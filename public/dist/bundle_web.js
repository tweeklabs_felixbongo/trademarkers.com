(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({"./app/js/web/web_app.js":[function(require,module,exports){
// @ts-check2
/// <reference2 path="/home/wang/projects/tks_project/tks/node_modules/@types/angular/index.d.ts" />
/// <reference path="./override.d.ts" />

'use strict';

/*global angular */

//var routes = require('./web_routes.js')

require('./web_controllers.js');
require('./web_menu.js');
require('./web_services.js');
require('./web_controllers_trademarks.js');
require('./web_studies.js');
require('./web_monitor.js');

require('../modules/modules.js');
var x = require('./temp2.json');

var modules = ['ngResource', 'ngRoute', 'ngCookies', 'ngSanitize', 'pascalprecht.translate', 'angulartics', 'angulartics.google.analytics', 'angulartics.google.tagmanager', 'mod.pager', 'mod.authorize', 'tks.controllers', 'tks.services', 'tks.services2', 'tks.filters', 'tks.directives', 'tks.directives2', 'mod.countries', 'mod.cards', 'tks.reg.controllers', 'tks.study.controllers', 'tks.menu', 'profile.directives', 'ui.bootstrap', 'angular-cookie-law', 'vcRecaptcha'];

angular.module('tks', modules).config(require('./web_routes.js')).config(["$translateProvider", function ($translateProvider) {

    var locale_en = require('./locale_en.json');
    var locale_zh_CN = require('./locale_zh-CN.json');
    console.log('locale_en', locale_en);

    $translateProvider.useSanitizeValueStrategy('escape');

    $translateProvider.translations('en', locale_en).translations('zh-CN', locale_zh_CN);

    $translateProvider.preferredLanguage('en');
}]).run(["$rootScope", "$location", "authService", "page_info", function ($rootScope, $location, authService, page_info) {
    console.log('haha');
    console.log('hello');
    $rootScope.$on("$routeChangeStart", function (event, next, current) {

        console.log('changing');
        var subfix = " - TradeMarkers®.com";
        var page_title = "Register Your Trademark Worldwide";
        //page_info.setTitle("Register Your Trademark Worldwide" + subfix)
        console.log(next.$$route);

        if (next.$$route !== undefined) {
            var originalPath = next.$$route.originalPath;
            console.log('original path', originalPath);
            if (originalPath === '/about') page_title = 'About Us';else if (originalPath === '/countries') page_title = "International Trademark Registration";else if (originalPath === '/contact') page_title = "Contact Us";else if (originalPath === '/classes') page_title = 'Nice Class Descriptions';else if (originalPath === '/search_classes') page_title = 'Search NICE Classes';else if (originalPath === '/terms') page_title = "Terms and Conditions";
        }

        page_info.setTitle(page_title + subfix);

        if (next.$$route !== undefined) {
            if (next.$$route.isPriv !== undefined) {
                if (next.$$route.isPriv) {
                    if (!authService.isSignedIn()) {
                        console.log('changing to signin');
                        $location.path("/signin");
                    }
                }
            }
        }
    });
}]).run(['DBStore', function (DBStore) {
    console.log('DBstore');
    DBStore.create();
}]).factory('authInterceptor', ['$q', '$location', 'Session', 'DBStore', function ($q, $location, Session, DBStore) {
    return {
        request: function request(config) {
            config.headers = config.headers || {};
            if (Session.getSessionID()) {
                config.headers.Authorization = 'Bearer ' + Session.getSessionID();
            }
            return config;
        },
        response: function response(_response) {
            console.log('resp', _response.status);
            return _response || $q.when(_response);
        },
        responseError: function responseError(response) {
            console.log('resp', response.status);
            if (response.status === 401) {
                if (response.data.indexOf("jwt expired") !== -1) {
                    Session.destroy();
                    $location.path('/signin');
                    return $q.reject(response);
                } else {
                    $location.path('/');
                    return $q.reject(response);
                }
            } else if (response.status === 404) {
                console.log(response);
                $location.path('/404/' + DBStore.add(response.data));
                return $q.reject(response);
            }
            //return response || $q.when(response);
            return $q.reject(response);
        }
    };
}]).config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
}]);

},{"../modules/modules.js":"/home/wang/projects/tks_project/tks/app/js/modules/modules.js","./locale_en.json":"/home/wang/projects/tks_project/tks/app/js/web/locale_en.json","./locale_zh-CN.json":"/home/wang/projects/tks_project/tks/app/js/web/locale_zh-CN.json","./temp2.json":"/home/wang/projects/tks_project/tks/app/js/web/temp2.json","./web_controllers.js":"/home/wang/projects/tks_project/tks/app/js/web/web_controllers.js","./web_controllers_trademarks.js":"/home/wang/projects/tks_project/tks/app/js/web/web_controllers_trademarks.js","./web_menu.js":"/home/wang/projects/tks_project/tks/app/js/web/web_menu.js","./web_monitor.js":"/home/wang/projects/tks_project/tks/app/js/web/web_monitor.js","./web_routes.js":"/home/wang/projects/tks_project/tks/app/js/web/web_routes.js","./web_services.js":"/home/wang/projects/tks_project/tks/app/js/web/web_services.js","./web_studies.js":"/home/wang/projects/tks_project/tks/app/js/web/web_studies.js"}],"/home/wang/projects/tks_project/tks/app/js/common/profile_directive.js":[function(require,module,exports){
"use strict";
/* global angular */
/* Directives */

var utilities = require('../modules/utilities.js');
var countries = require('countries');

angular.module('profile.directives', []).directive('appVersion', ['version', function (version) {
  return function (scope, elm, attrs) {
    elm.text(version);
  };
}]).directive('profile123', function () {
  console.log('in profile hello');
  return {
    restrict: 'E',
    scope: {
      name: '@',
      datasource: '=' // pass a json object
    },
    template: '<span>Hello {{name}}</span>'
  };
})

// to get name, country of inc, contact and email

.directive('profileNames', ['$http', 'countries', function ($http, countries) {
  return {
    restrict: 'E',
    scope: {
      name: '@',
      datasource: '='
    },
    templateUrl: '/partials/profileNames.html',
    link: function link(scope, el, attr) {

      if (!scope.datasource.inc_country)
        //          $http.get('/api2/get_geo?test_ip=174.54.165.206').success(function (geo) {
        //          $http.get('/api2/get_geo?test_ip=112.210.15.107').success(function (geo) {
        $http.get('/api2/get_geo').success(function (geo) {
          console.log('geo', geo);
          if (geo.country) scope.datasource.inc_country = geo.country;
          scope.datasource.nature = 'C';
        });

      scope.natures = [{ name: 'Company', code: 'C' }, { name: 'Individual', code: 'I' }];
      scope.countries = countries;
    }
  };
}]).directive('profileAddress', ['$http', '$templateCache', '$compile', function ($http, $templateCache, $compile) {
  console.log('in profile3 hello');
  return {
    restrict: 'E',
    scope: {
      name: '@',
      datasource: '=',
      country: '='
    },
    //      templateUrl: '/partials/profileAddress.html',
    //      templateUrl: '/api2/server_partials/address/{{code}}',

    link: function link(scope, el, attr) {

      /*
      scope.states2 = {
        "PH.AB": "test",
        "PH.CB": "cebu"
      }
      */

      scope.states = countries.get_states(scope.country);

      console.log('attr1', attr);
      console.log('ds', scope.datasource);
      console.log('c', scope.country);
      console.log('scope', scope);

      var url = "/api2/server_partials/address/" + scope.country;
      console.log(url);
      loadTemplate("/api2/server_partials/address/" + scope.country);
      console.log('country in watch1', scope.country);

      /*
       scope.$watch(attr.country, function (value) {
        if (value) {
          var url = "/api2/server_partials/address/" + value
          console.log(url)
          loadTemplate("/api2/server_partials/address/" + value);
          console.log('country in watch', value)
        }
      });
      */

      function loadTemplate(template) {
        $http.get(template /*, { cache: $templateCache }*/).success(function (templateContent) {

          //              console.log('dd', templateContent)
          console.log('el', el);
          el.replaceWith($compile(templateContent)(scope));
          console.log('loaded', scope);
          scope.$parent.ready2show = true; // flag to inform the main form to show, it's a hack
        });
      }

      console.log('scope', scope);

      //        scope.code = 'AS'
      /*
      if (!scope.datasource.country)
        $http.get('/api2/get_geo?test_ip=174.54.165.206').success(function (geo) {
          console.log('geo', geo)
          if (geo.country) {
            scope.datasource.country = geo.country
            scope.datasource.state = geo.region
            scope.datasource.city = geo.city
          }
        })
       console.log('datasource2', scope.datasource)
      */
      scope.countries = countries.get_countries();
    }
  };
}]);

},{"../modules/utilities.js":"/home/wang/projects/tks_project/tks/app/js/modules/utilities.js","countries":"/home/wang/projects/tks_project/tks/node_modules/countries/countries.js"}],"/home/wang/projects/tks_project/tks/app/js/common/tks_directives.js":[function(require,module,exports){
"use strict";
/* global angular, jQuery */
/* Directives */

var utilities = require('../modules/utilities.js');

function append_one_word_country_name(lst) {
    var rlst = [];
    for (var i = 0; i < lst.length; i++) {
        lst[i].simple_name = lst[i].name.replace(/ /g, '_').toLowerCase();
        rlst.push(lst[i]);
    }
    return rlst;
}

angular.module('tks.directives', []).directive('appVersion', ['version', function (version) {
    return function (scope, elm, attrs) {
        elm.text(version);
    };
}]).directive('hello2', function () {
    return {
        restrict: 'E',
        scope: {
            name: '@'
        },
        template: '<span>Hello {{name}}</span>'
    };
}).directive('trademarkRequest', function () {
    return {
        restrict: 'E',
        scope: {
            name: '@',
            tmreg: '=',
            profile: '='
        },
        templateUrl: '/partials/tmrequest.html',
        /*
         controller: function ($scope) {
         console.log('ctrl')
         console.log($scope)
         console.log('ctrl2')
         }*/
        link: function link($scope, element) {
            console.log('abc');
            console.log($scope.profile_name);
            console.log($scope);
            console.log('abcx');
        }
        /*
         link: function (scope, element) {
         console.log(scope.tmreg)
         scope.name2 = 'Jeff';
          var temp
         for (var i = 0; i <scope.tmreg.classes.length; i ++) {
         var c = scope.tmreg.classes[0]
         console.log(c)
         if (i === 0)
         temp = c.name + ' (' + c.details + ')'
         else {
         temp = temp + ', ' + c.name + ' (' + c.details + ')'
         }
         }
         scope.classes = temp
         }*/

    };
}).directive('tmPhoto', function () {
    return {
        restrict: 'E',
        scope: {
            psrc: '=',
            tmtype: '='
        },
        template: '<img ng-show="show(tmtype)" ng-src="{{psrc}}" width="100px;" />',
        link: function link(scope, el, attr) {
            scope.show = function (typ) {
                return ['l', 'wl', 'tml'].indexOf(typ) !== -1;
            };
        }
    };
}).directive('code2country', function () {
    return {
        restrict: 'E',
        scope: {
            code: '='
        },
        //      template: '<img src="{{code2src(code)}}" />',
        template: '<img ng-src="{{code2src(code)}}" />',
        link: function link(scope, el, attr) {
            scope.code2src = function (code) {
                console.log(code);
                return 'images/flags/' + code.toLowerCase() + '.png';
            };
        }
    };
}).directive('infoPopover', function () {
    return {
        restrict: 'E',
        scope: {
            info: '@info'
        },
        template: ' <img ng-src="images/info.gif" style="padding-left: 3px;" popover="{{info}}" popover-trigger="mouseenter" popover-placement="left"  />'
        /*
         link: function(scope, el, attr) {
         console.log(scope)
         }*/
    };
}).directive('order123', function () {
    return {
        restrict: 'E',
        scope: {
            details: '='
        },
        template: '<p><h3>details123</h3></p>',
        link: function link(scope, el, attr) {
            console.log('asdfasdf');
            console.log(scope);
        }
    };
}).directive('orderItemDetails', function () {
    return {
        restrict: 'E',
        scope: {
            details: '='
        },
        templateUrl: '/partials/order_item_details.html'
    };
}).directive('orderItemDetailsWithNotes', function () {
    // if notes is present, it is added first by admin
    return {
        restrict: 'E',
        scope: {
            details: '='
        },
        templateUrl: '/partials/order_item_details2.html'
    };
}).directive('loading', function () {
    return {
        restrict: 'E',
        scope: {
            when: '='
        },
        template: '<span ng-show="when"><img ng-src="/images/loading.gif" /></span>'
    };
}).directive('gChart', ['$location', function ($location) {
    return {
        restrict: 'A',
        scope: {
            gid: '=',
            gdata: '=',
            gready: '=',
            dtype: '=' // 'V' videos, 'S' submissions, 'A' assignments
        },
        link: function link(scope, elm, attrs) {

            function get_data() {

                console.log(scope.gdata);
                scope.gdata.fn.then(function (d) {
                    console.log('dd1');
                    console.log(d);
                    console.log('dd2');
                });
            }

            function drawRegionsMap() {
                var data = new google.visualization.DataTable();

                data.addColumn('string', 'Country');
                data.addColumn('number', 'Reference');
                console.log('bbb', scope.gdata);
                //    TMCountries.get().then(function(cnts) {
                //            console.log('ccc', data)
                //          data.addRows(Object.keys(cnts).length)
                data.addRows(Object.keys(scope.gdata).length);

                //            console.log('',Object.keys(cnts).length);

                var k = 0;
                for (var key in scope.gdata) {
                    if (scope.gdata.hasOwnProperty(key)) {
                        //               console.log(cnts[key])
                        data.setCell(k, 0, scope.gdata[key].name);
                        data.setCell(k, 1, scope.gdata[key].price);
                        k = k + 1;
                    }
                }

                var options = {};
                //options['colors'] = ['#FADC41', '#c44949', '#d74a12', '#0e9a0e', 'red', '#7c4b91', '#fadc41', '#0d6cca', '#7c4897'];
                //options['colors'] = [ '#0e9a0e', 'red', '#7c4b91', '#fadc41', '#0d6cca', '#7c4897'];
                options.colors = ['#008bd4', '#008bd4'];
                options.legend = 'none';
                //          options.region = 'ES'

                var chart = new google.visualization.GeoChart(document.getElementById(scope.gid));

                google.visualization.events.addListener(chart, 'regionClick', function (region) {
                    scope.$apply(function () {
                        $location.path('/trademark/' + region.region);
                        /*
                         var cntry = scope.gdata[region.region]
                         if (cntry !== undefined) {
                         if (cntry.link)
                         $location.path('/trademark/' + cntry.link);
                         else
                         $location.path('/trademark/' + region.region);
                         } */
                    });
                });

                scope.gready = true;

                /*
                 google.visualization.events.addListener(chart, 'ready', function () {
                 scope.$apply(function () {
                 console.log('map is ready')
                 scope.gready = true
                 }
                 });
                 */

                chart.draw(data, options);

                //        })
                /*
                 data.addRows(scope.gdata.length)
                   for (var k = 0; k < scope.gdata.length; k ++) {
                 var c = scope.gdata[k]
                 console.log(k,c, c.price, c.code )
                 data.setCell(k, 0, scope.gdata[k].name);
                 data.setCell(k, 1, scope.gdata[k].price);
                 }
                  var options = { };
                 //          options['colors'] = ['#FADC41', '#c44949', '#d74a12', '#0e9a0e', 'red', '#7c4b91', '#fadc41', '#0d6cca', '#7c4897'];
                 //          options['colors'] = [ '#0e9a0e', 'red', '#7c4b91', '#fadc41', '#0d6cca', '#7c4897'];
                 options['legend'] = 'none';
                 //          options.region = 'ES'
                   var chart = new google.visualization.GeoChart(document.getElementById(scope.gid));
                  google.visualization.events.addListener(chart, 'regionClick',
                 function(region) {
                 scope.$apply(function() {
                 $location.path('/trademark/' + region.region);
                 });
                 });
                  google.visualization.events.addListener(chart, 'ready', function() {
                 scope.$apply(function() {
                 console.log('map is ready')
                 scope.gready = true
                 });
                  });
                  chart.draw(data, options);
                 */
            }

            drawRegionsMap();
        }
    };
}])

//  show a list of countries in the region

.directive('regionList', function () {
    return {
        restrict: 'E',
        scope: {
            code: '@',
            cols: '='
        },
        templateUrl: '/partials/regionlist.html',
        link: function link(scope, el, attr) {

            var lst = append_one_word_country_name(utilities.get_countries_by_contnent(scope.code));
            var nums = Math.ceil(lst.length / 3);
            scope.countries1 = lst.slice(0, nums); // 0 13
            scope.countries2 = lst.slice(nums, 2 * nums); // 14
            scope.countries3 = lst.slice(2 * nums, 3 * nums);
        }
    };
}).directive('stripeForm', ['$window', function ($window) {

    var directive = {
        restrict: 'A'
    };
    directive.link = function (scope, element, attributes) {
        var form = angular.element(element);
        form.bind('submit', function () {
            var button = form.find('button');
            button.prop('disabled', true);
            $window.Stripe.createToken(form[0], function () {
                var args = arguments;
                scope.$apply(function () {
                    scope[attributes.stripeForm].apply(scope, args);
                });
                button.prop('disabled', false);
            });
        });
    };
    return directive;
}]).directive('passwordReset', ['authService', function (authService) {
    return {
        restrict: 'A',
        scope: {
            flag: '@'
        },
        templateUrl: '/partials/forgot_password.html',
        //templateUrl: '/partials/test123.html',
        //template: "<h1>hello</h1>",
        link: function link(scope, el, attr) {

            console.log('in reset password');

            scope.alerts = [];

            scope.closeAlert = function (index) {
                scope.alerts.splice(index, 1);
                scope.$parent.show_reset_form = false; // the parent should use this flag to show/hide related elements
                scope.alerts = [];
            };

            scope.info = {};
            scope.reset_password = function () {
                console.log('resetting');
                console.log(scope.info);
                console.log(scope);
                authService.request_password_reset(scope.info.email).then(function (result) {
                    console.log('back reset  result ', result);
                    scope.alerts = [];
                    if (result.status) scope.alerts.push({
                        type: 'success',
                        msg: 'We have sent an email with password reset instruction, please check your inbox'
                    });else scope.alerts.push({
                        type: 'warning',
                        msg: result.message
                    });
                });
            };
            console.log(scope);
        }
    };
}])

/*
 // this assumes the parent has alerts[] defined
 // $scope.alerts.push({
 type: 'warning',
 msg: "this is a message123"
 })
 */

.directive('alertLine', ['$timeout', function ($timeout) {
    return {
        restrict: 'E',
        template: '<alert ng-repeat="alert in alerts" type="{{alert.type}}" close="closeAlert($index)">{{alert.msg}}</alert>',
        link: function link(scope, el, attr) {
            // scope.alerts = [];
            scope.closeAlert = function (index) {
                scope.alerts.splice(index, 1); // alerts is defined in the parent controller
            };
            /*
             $timeout(function(){
             //        scope.alerts.splice(0, 1)
             scope.alerts = []
             }, 3000); // maybe '}, 3000, false);' to avoid calling apply*/
        }
    };
}]).directive('ticker', ['$timeout', function ($timeout) {
    return {
        // Restrict it to be an attribute in this case
        restrict: 'A',
        // responsible for registering DOM listeners as well as updating the DOM
        link: function link(scope, element, attrs) {
            //$(element).toolbar(scope.$eval(attrs.toolbarTip));
            console.log('ticker2');
            //jQuery('#webticker').webTicker()
            jQuery('#news').easyTicker({
                direction: 'up',
                easing: 'swing',
                speed: 'slow',
                interval: 3000,
                height: 'auto',
                visible: 2,
                mousePause: 1,
                controls: {
                    up: '',
                    down: '',
                    toggle: '',
                    playText: 'Play',
                    stopText: 'Stop'
                }
            });
        }
    };
}]).directive('coverflow', ['$location', function ($location) {
    return {
        // Restrict it to be an attribute in this case
        restrict: 'A',
        // responsible for registering DOM listeners as well as updating the DOM
        link: function link(scope, element, attrs) {

            scope.goto_trademark = function (path) {
                console.log('asdfasdf', path);
                $location.path(path);
            };

            scope.mypath = 'bbbbb';

            jQuery('#preview-coverflow').coverflow({
                index: 3,
                innerAngle: -55,
                innerOffset: 9,
                confirm: function confirm(event, cover, index) {
                    console.log('confirmed', cover);
                    scope.$apply(function () {
                        //$location.path('/trademark/US');
                        var img = jQuery(cover).children().andSelf().filter('img').last();
                        console.log('img', img.data('refsrc'));
                        $location.path(img.data('refsrc'));
                    });
                    /*
                     var img = jQuery(cover).children().andSelf().filter('img').last();
                     console.log('img', img.data('refsrc'))
                     $location.path('/trademark/');*/
                },
                select: function select(event, cover) {
                    var img = jQuery(cover).children().andSelf().filter('img').last();
                    jQuery('#photos-name').text('Register ' + img.data('name') || 'unknown');
                    scope.mypath = img.data('refsrc');
                }

            });
        }
    };
}]).directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
}).directive('userLink', ['$http', function ($http) {
    return {
        restrict: 'E',
        template: '<span class="glyphicon glyphicon-user" aria-hidden="true"></span>&nbsp;&nbsp;<a ng-href="account/{{account._id}}">{{account.name}}</a>',
        link: function link(scope, el, attrs) {
            attrs.$observe('usrid', function (value) {
                if (value) {
                    $http.get('/admin_api/account/' + value).then(function (data) {
                        scope.account = data.data;
                    });
                }
            });
        }
    };
}]).directive('profileLink', ['$http', function ($http) {
    return {
        restrict: 'E',
        scope: {
            profile_id: '='
        },
        template: '<span class="glyphicon glyphicon-file" aria-hidden="true"></span>&nbsp;<a ng-href="profile/{{account2._id}}">{{account2.profile_name}}</a>',
        link: function link(scope, el, attrs) {
            console.log('scp2', scope);
            attrs.$observe('profileId', function (value) {
                console.log('profile value', value);
                if (value) {
                    $http.get('/admin_api/profile/' + value).then(function (data) {
                        scope.account2 = data.data;
                    });
                }
            });
        }
    };
}]).directive('profileName', ['$http', 'authService', function ($http, authService) {
    return {
        restrict: 'E',
        scope: {
            profile_id: '='
        },
        template: '<a>{{account2.profile_name}}</a>',
        link: function link(scope, el, attrs) {
            console.log('scp2', scope);
            attrs.$observe('profileId', function (value) {
                console.log('profile value in profileName ', value);
                if (authService.isSignedIn()) {
                    if (value) {

                        $http.get('/api/profile/' + value).then(function success(data) {
                            scope.account2 = data.data;
                        }, function error(error) {
                            console.log('we got an error');
                            console.log(error);
                        });
                    }
                }
            });
        }
    };
}]).directive('jsonld', ['$filter', '$sce', function ($filter, $sce) {
    return {
        restrict: 'E',
        template: function template() {
            return '<script type="application/ld+json" ng-bind-html="onGetJson()"></script>';
        },
        scope: {
            json: '=json'
        },
        link: function link(scope, element, attrs) {
            scope.onGetJson = function () {
                return $sce.trustAsHtml($filter('json')(scope.json));
            };
        },
        replace: true
    };
}]);

},{"../modules/utilities.js":"/home/wang/projects/tks_project/tks/app/js/modules/utilities.js"}],"/home/wang/projects/tks_project/tks/app/js/common/tks_directives2.js":[function(require,module,exports){
"use strict";
/* global angular, jQuery */
/* Directives */

var utilities = require('../modules/utilities.js');

angular.module('tks.directives2', []).directive('trademarkType', ['$http', function ($http) {

    return {
        restrict: 'E',
        scope: {
            reg: '=',
            optns: '='
        },
        templateUrl: '/partials/trademark_type.html',
        link: function link(scope, el, attrs) {
            console.log('scp2', scope);

            scope.uploadFile = function (files) {

                console.log('upload file');
                console.log(files);
                var fd = new FormData();

                //Take the first selected file
                fd.append("logo_file", files[0]);
                console.log('fd', fd);

                $http.post('/api2/upload_file', fd, {
                    withCredentials: true,
                    headers: {
                        'Content-Type': undefined
                    },
                    transformRequest: angular.identity
                }).success(function (data) {
                    scope.reg.logo_url = data.url;
                    scope.upload_status = 'Uploaded';
                }).error(function (data) {
                    console.log('failed', data);
                });
            };
        }
    };
}]).directive('trademarkClasses', ['$http', function ($http) {
    return {
        restrict: 'E',
        scope: {
            reg: '=',
            optns: '='

        },
        templateUrl: '/partials/trademark_classes.html',
        link: function link(scope, el, attrs) {

            console.log('scp2', scope.optns);

            scope.labels = {
                //class_selection : "Select the classes in which you need to register your Trademark:"
                class_selection: "Select the classes which you would like your trademark monitored for:"
            };

            /*
            scope.options = {
                enable_class_detail : false
            }
            */

            scope.classes_selected = [];

            // generate an array for UI
            for (var x = 1; x <= 45; x++) {
                var c = x.toString();
                if (c.length === 1) c = '0' + c;
                scope.classes_selected.push({
                    code: c,
                    selected: false
                });
            }

            classes2array(scope.reg.classes);

            scope.class_change = function (s) {

                if (s.selected) {

                    scope.reg.classes.push({
                        name: s.code,
                        details: ''
                    });
                } else {

                    for (var i = 0; i < scope.reg.classes.length; i++) {
                        if (scope.reg.classes[i].name === s.code) scope.reg.classes.splice(i, 1);
                    }
                }
            };

            function classes2array(classes) {

                for (var k = 0; k < scope.classes_selected.length; k++) scope.classes_selected[k].selected = false;

                for (var i = 0; i < classes.length; i++) {
                    var indx = parseInt(classes[i].name);
                    scope.classes_selected[indx - 1].selected = true;
                }
            }
        }
    };
}]);

},{"../modules/utilities.js":"/home/wang/projects/tks_project/tks/app/js/modules/utilities.js"}],"/home/wang/projects/tks_project/tks/app/js/common/tks_filters.js":[function(require,module,exports){
'use strict';
/*global angular, dump, moment, io, console */

/* Filters */

angular.module('tks.filters', []).filter('reverse', function () {
  return function (input, uppercase) {
    var out = "";
    for (var i = 0; i < input.length; i++) {
      out = input.charAt(i) + out;
    }
    // conditional based on optional argument
    dump(input);
    if (uppercase) out = out.toUpperCase();

    dump(out);
    return out;
  };
}).filter('tm_classes', function () {
  return function (input) {
    if (typeof input === 'object') {
      var ret = '';
      for (var i = 0; i < input.length; i++) {
        if (ret === '') {
          if (input[i].details) ret = input[i].name + ' (' + input[i].details + ')';else ret = input[i].name;
        } else {
          if (input[i].details) ret = ret + '-' + input[i].name + ' (' + input[i].details + ')';else ret = ret + '-' + input[i].name;
        }
      }
      return ret;
    } else return 'n/a';
  };
})

// show a classes in a line without text
.filter('tm_classesSimple', function () {
  return function (input) {
    if (typeof input === 'object') {
      var ret = '';
      for (var i = 0; i < input.length; i++) {
        if (ret === '') {
          ret = input[i].name;
        } else {
          ret = ret + '-' + input[i].name;
        }
      }
      return ret;
    } else return 'n/a';
  };
}).filter('bool2text', function () {
  return function (bool) {
    if (bool) return 'Yes';else return 'No';
  };
}).filter('regtype2text', function () {
  return function (t) {
    if (t === 'w') return 'Wordmark';else if (t === 'l') return 'Logo';else if (t === 'wl') return 'Wordmark and logo';else if (t === 'tm') return 'Trademark';else if (t === 'tml') return 'Trademark with logo';else return 'Unknown';
  };
}).filter('poa2text', ['POA_INFO', function (POA_INFO) {
  return function (p, country) {
    if (p === -1) return 'This application does not require a Power of Attorney.';else if (p === 2) return 'A power of attorney is required before registration can start.';else {
      var label = POA_INFO[country];
      if (label !== undefined) {
        return label[p]; //TODO handle invalid P
      } else {
          return 'No POA label found for ' + p;
        }
    }
  };
}]).filter('country2text', ['countries', function (countries) {
  return function (p) {
    for (var i = 0; i < countries.length; i++) {
      if (countries[i].code === p) return countries[i].name;
    }
  };
}]).filter('orderStatus', function () {
  return function (p) {

    if (p === 1) return 'PENDING';else if (p === 2) return 'Completed';else if (p === 3) return 'Cancelled';else return 'Unknown';
  };
}).filter('ticketStatus', function () {
  return function (p) {

    if (p === 0) return 'ACTIVE';else if (p === 1) return 'CLOSED';else if (p === 2) return 'Cancelled';else return 'Unknown';
  };
}).filter('code2flagsrc', function () {
  return function (code) {
    if (code !== undefined) {

      console.log(code, typeof code);
      return 'images/flags/' + code.toLowerCase() + '.png';
    } /*else
      return 'images/'*/
  };
}).filter('commerce_use', ['$filter', function ($filter) {
  return function (code, use_date) {
    if (code !== undefined) {
      if (code === -1) return 'not applicable';else if (code === 0) return 'No';else if (code === 1) return 'Yes ( starting date : ' + $filter('date')(use_date, 'mediumDate') + ')';else if (code === 2) return 'No, but registered abroad';else return 'unknow code : ' + code;
    } else return 'unknown';
  };
}]).filter('study_type2text', function () {
  return function (t) {
    if (t === "0") return 'Basic';else if (t === "1") return 'Extensive';else if (t === -1) return '';else return 'Unknown';
  };
}).filter('admin_type', function () {
  return function (t) {
    if (t !== undefined) {
      if (t === 'A') return 'Admin';else if (t === 'S') return 'Staff';
    }
  };
}).filter('authors2text', function () {
  return function (t) {
    if (t !== undefined) {
      var ret;
      for (var i = 0; i < t.length; i++) {
        if (ret) ret = ret + ',' + t[i].name;else ret = t[i].name;
      }
      return ret;
    }
  };
}).filter('amt_in_case', function () {
  return function (t) {
    if (t !== undefined) {
      return '$ ' + t;
    }
  };
});

},{}],"/home/wang/projects/tks_project/tks/app/js/common/tks_services.js":[function(require,module,exports){
"use strict";
/* global angular */
var services2 = angular.module('tks.services2', []).value('version', '0.1');

services2.constant('STATIC_INFO', {

  requirements: 'In order to apply for a Trademark in USA you need to provide <br/>' + '1. Information through our Trademark Registration Order Form, and you need to<br/> ' + '2. Demonstrate use of the Trademark. No Power of Attorney is needed for USA.<br/> More to come',
  prices: 'Prices to be provided later...',
  priority_help: 'Claiming priority is part of the Paris Convention treaty.' + 'It means that if you have filed a trademark within the last 6 months in any of the signatory countries (most countries of the world), ' + 'you can use the filing date of this first registration as the filing date in the other signatory countries.  This right may be useful ' + 'to the owner of the trademark, to protect his intellectual property, in cases of legal conflicts with other parties.',
  class_help: 'When registering a trademark you need to specify the products and services that will be associated with your trademark.' + ' The large majority of countries of the world have adopted the International Classification of Nice. This system groups all products and ' + 'services into 45 classes - 34 for products, 11 for services - permitting you to specify precise and clear classes covering your trademark. ' + 'The protection that is offered to a registered trademark covers only the classes specified at the time of registration, therefore, allowing' + ' two identical trademarks to coexist in distinct classes.',
  commerce_use_help: '“Used in commerce” means that the mark is used in the ordinary course of trade. For example, for goods it would be used when it is placed in any manner on the goods, their containers or their displays. For services it is used when the trademark is used or displayed in advertising or during the sales process.'
}).constant('POA_INFO', {

  'IN': {
    0: 'I will be sending it within 120 days; file trademark immediately.',
    1: 'No, do not file application until received the Power of Attorney.'
  },
  'AR': {
    0: 'I will be sending it within 40 days; file trademark immediately.',
    1: 'No, do not file application until received the Power of Attorney.'
  },
  'AZ': {
    0: 'I will be sending it within 30 days; file trademark immediately.',
    1: 'No, do not file application until received the Power of Attorney.'
  },
  'BO': {
    0: 'I will be sending it within 30 days; file trademark immediately.',
    1: 'No, do not file application until received the Power of Attorney.'
  },
  'CA': {
    0: 'I will be sending it within 45 days; file trademark immediately.',
    1: 'No, do not file application until received the Power of Attorney.'
  },
  'CO': {
    0: 'I will be sending it within 60 days; file trademark immediately.',
    1: 'No, do not file application until received the Power of Attorney.'
  },
  'MX': {
    0: 'I will be sending it within 30 days; file trademark immediately($151)',
    1: 'No, do not file application until received the Power of Attorney.'
  },
  'BR': {
    0: 'I will be sending it within 55 days; file trademark immediately($163)',
    1: 'No, do not file application until received the Power of Attorney.'
  },
  'NO': {
    0: 'I will be sending it within 90 days; file trademark immediately',
    1: 'No, do not file application until received the Power of Attorney.'
  },
  'RU': {
    0: 'I will be sending it within 60 days; file trademark immediately',
    1: 'No, do not file application until received the Power of Attorney.'
  },
  'VN': {
    0: 'I will be sending it within 30 days; file trademark immediately ($50)',
    1: 'No, do not file application until received the Power of Attorney.'
  },
  'TW': {
    0: 'I will be sending it within 90 days; file trademark immediately',
    1: 'No, do not file application until received the Power of Attorney.'
  },
  'CN': { 0: '', 1: '' },
  'HELP': {
    'IN': 'In INDIA it is possible to file immediately your trademark application, without Power of Attorney, provided that you will send it within 120 Days.'
  },
  'POA_MUST': 'Please note that in order to file your trademark application, we need that you send us a Power of Attorney, below you can find a Template'

});

},{}],"/home/wang/projects/tks_project/tks/app/js/modules/mod_authorize.js":[function(require,module,exports){
"use strict";
/* global angular*/
angular.module('mod.authorize', []).value('version', '0.1')

// contains logged user info, uses sessionStorage, can be changed to use localStorage if needed

.service('Session', ["$window", "$cookieStore", function ($window, $cookieStore) {
  this.create = function (sessionId, userID, name, locale) {

    $cookieStore.put('sessionID', sessionId);
    $cookieStore.put('name', name);
    $cookieStore.put('userID', userID);
    $cookieStore.put('locale', locale);
  };

  this.destroy = function () {

    $cookieStore.remove('sessionID');
    $cookieStore.remove('name');
    $cookieStore.remove('userID');
    $cookieStore.remove('locale');
  };

  this.getSessionID = function () {
    return $cookieStore.get('sessionID');
  };

  this.getName = function () {
    return $cookieStore.get('name');
  };

  this.getUserID = function () {
    return $cookieStore.get('userID');
  };

  return this;
}])

// authorize services

.factory('authService', ['$http', '$q', '$timeout', 'Session', function ($http, $q, $timeout, Session) {

  var fasad = {};

  var authenticate = function authenticate(user) {
    var deferred = $q.defer();

    $http.post('/signin/', user).success(function (data, status) {
      console.log(data);
      console.log(status);
      if (data.sessionID !== null) Session.create(data.sessionID, data.userID, data.name, data.locale);
      deferred.resolve({ authenticated: data.sessionID !== null, message: data.msg, name: data.name });
    }).error(function (error) {
      deferred.resolve({ authenticated: false, message: 'Error occurred while authenticating.' });
    });

    return deferred.promise;
  };

  var isAuthenticated = function isAuthenticated() {
    var sid = Session.getSessionID();
    if (sid === undefined || sid === null) return false;else return true;
  };

  var register = function register(user) {
    var deferred = $q.defer();

    $http.post('/signup', user).success(function (data, status) {
      deferred.resolve({ registered: data.registered, message: data.message });
    }).error(function (error) {
      deferred.resolve({ registered: false, message: 'Sorry, error occurred while signing up, please try again.' });
    });

    return deferred.promise;
  };

  var logOut = function logOut() {
    Session.destroy();
  };

  var request_password_reset = function request_password_reset(email) {

    var deferred = $q.defer();

    $http.get('/api2/password_reset?email=' + email).success(function (data) {
      console.log(data);
      deferred.resolve({ status: data.status === 'ok', message: data.message });
    }).error(function (error) {
      deferred.resolve({ status: 'err', message: 'Sorry, error occurred while accessing server, please try again.' });
    });

    return deferred.promise;
  };

  fasad.signIn = function (user) {
    return authenticate(user);
  };
  fasad.signUp = function (user) {
    return register(user);
  };
  fasad.logOut = function () {
    return logOut();
  };
  fasad.isSignedIn = function () {
    return isAuthenticated();
  };
  fasad.userName = function () {
    return Session.getName();
  };

  fasad.request_password_reset = function (email) {
    return request_password_reset(email);
  };

  return fasad;
}]).constant('AUTH_EVENTS', {
  loginSuccess: 'auth-login-success',
  loginFailed: 'auth-login-failed',
  logoutSuccess: 'auth-logout-success',
  sessionTimeout: 'auth-session-timeout',
  notAuthenticated: 'auth-not-authenticated',
  notAuthorized: 'auth-not-authorized',
  cartUpdated: 'cart-updated'
})

// a simple local db store, stores key/value in sessionStorage, ideal for keepping some data like URL in the store,
// and use the key to pass to url

.service('DBStore', ['$window', function ($window) {

  this.create = function () {};

  this.destroy = function () {
    delete $window.sessionStorage.dbStore;
  };

  this.get = function (key) {
    var qkey = 'DB_' + key;
    var ret = $window.sessionStorage[qkey];
    delete $window.sessionStorage[qkey];
    return ret;
  };

  function generateQuickGuid() {
    return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
  }

  this.add = function (obj) {
    var key = generateQuickGuid();
    $window.sessionStorage['DB_' + key] = obj;
    return key;
  };

  return this;
}]);

},{}],"/home/wang/projects/tks_project/tks/app/js/modules/mod_cards.js":[function(require,module,exports){
"use strict";
/* global angular*/

angular.module('mod.cards', []).filter('range', function () {
  var filter = function filter(arr, lower, upper) {
    for (var i = lower; i <= upper; i++) arr.push(i);
    return arr;
  };
  return filter;
}).service('Cards', ['$http', '$q', function ($http, $q) {

  this.get_shopping_cart_dpm = function (cart_id) {
    var deferred = $q.defer();

    $http.get('/api_payment/get_shopping_cart_dpm/' + cart_id).success(function (data) {
      console.log('back from cart_dpm');
      console.log(data);
      deferred.resolve(data);
    }).error(function (error) {
      deferred.resolve(error);
    });
    return deferred.promise;
  };

  // this returns an blank card info, some suggested address

  this.get_user_info = function (cart_id) {
    var deferred = $q.defer();

    $http.get('/api_payment/get_user_info/' + cart_id).success(function (data) {
      console.log('back from cart_dpm');
      console.log(data);
      deferred.resolve(data);
    }).error(function (error) {
      deferred.resolve(error);
    });

    /*
          var info  = {
            "x_card_num": "6011000000000012",
            x_exp_date: "04/17",
            x_card_code: "782",
            x_first_name: "John",
            x_last_name: "Doe",
            x_address: "123 Main Street",
            x_city: "Boston",
            x_state: "MA",
            x_zip: "02142",
            x_country: "US"
          };
          */
    return deferred.promise;
  };

  return this;
}]);

},{}],"/home/wang/projects/tks_project/tks/app/js/modules/mod_countries.js":[function(require,module,exports){
"use strict";
/* globals angular */

var countries = require('countries');

angular.module('mod.countries', []).factory("countries", function () {
    console.log('123');
    var i = 1000;
    if (i === 1000) console.log('i am here and there');
    return countries.get_countries();
});

},{"countries":"/home/wang/projects/tks_project/tks/node_modules/countries/countries.js"}],"/home/wang/projects/tks_project/tks/app/js/modules/mod_pager.js":[function(require,module,exports){
"use strict";
/* global angular*/

angular.module('mod.pager', []).directive('pager2', ['$location', function ($location) {
  return {
    restrict: 'E',
    scope: {
      current: '=',
      pageCount: '=',
      showMorePages: '@'
    },
    templateUrl: '/partials/pager.html',
    link: function link(scope, iterStartElement, attrs) {

      scope.pagerPage = 0;
      scope.pagerSize = 10;

      scope.pager_count = function () {
        if (scope.pageCount !== undefined) return Math.ceil(scope.pageCount / scope.pagerSize);else return 0;
      };

      scope.first_page = function () {
        var ret = [],
            i,
            n;
        if (scope.pageCount != undefined) {
          // pageCount will be assigned
          n = 10;
          if (scope.pageCount < n) n = scope.pageCount;
          for (i = 0; i < n; i++) ret.push(i + 1);
        }
        return ret;
      };

      scope.pages = function () {
        var ret = [],
            i;
        for (i = 0; i < scope.pageCount; i++) ret.push(i + 1);
        return ret;
      };

      // try to see if page number has been set by caller, and adjust the current pager no
      scope.re_adjust_current_page = function () {

        if (scope.pageCount !== undefined) {
          if (scope.pagerSize > 0) {
            var n = Math.floor(scope.current / scope.pagerSize);
            if (n !== scope.pagerPage) scope.pagerPage = n;
          }
        }
      };

      scope.current_pager_page = function () {
        if (scope.pageCount !== undefined) {
          var ret = [],
              i;
          scope.re_adjust_current_page();
          var offset = scope.pagerPage * scope.pagerSize;
          for (i = offset; i < offset + scope.pagerSize; i++) {
            console.log('bb', i < scope.pageCount);
            if (i < scope.pageCount) ret.push(i + 1);
          }
          return ret;
        }
      };

      scope.all_pages = function () {
        var ret = [],
            i;
        for (i = 0; i < scope.pageCount; i++) ret.push(i + 1);
        return ret;
      };

      scope.set_page_number = function (n) {
        scope.current = n;
        if (scope.$parent.setDataSource !== undefined) {
          scope.$parent.setDataSource(scope.current);
          $location.search('page_no', n + 1);
        }
      };

      scope.nextPage = function () {

        console.log('sss', scope.pager_count());
        var n = scope.pagerPage + 1;

        if (n < scope.pageCount / scope.pagerSize) scope.pagerPage = n;

        console.log(scope.current, scope.pagerPage);
        console.log('next page');

        this.set_page_number(scope.pagerPage * scope.pagerSize);
      };

      scope.prevPage = function () {
        if (scope.pagerPage > 0) scope.pagerPage = scope.pagerPage - 1;
        console.log('prev page');
        this.set_page_number(scope.pagerPage * scope.pagerSize);
      };

      scope.myclass = function (n) {
        if (n === scope.current) return 'active';else return '';
      };

      scope.show = function () {
        return scope.pageCount > 1;
      };

      scope.showMore = function () {
        return scope.showMorePages;
      };

      scope.set_show_more = function () {
        scope.showMorePages = !scope.showMorePages;
      };
    }
  };
}]);

},{}],"/home/wang/projects/tks_project/tks/app/js/modules/modules.js":[function(require,module,exports){
'use strict';

module.exports = [require('../common/tks_services.js'), require('../common/tks_filters.js'), require('../common/tks_directives.js'), require('../common/tks_directives2.js'), require('../common/profile_directive.js'), require('./mod_countries.js'), require('./mod_authorize.js'), require('./mod_pager.js'), require('./mod_cards.js')];

},{"../common/profile_directive.js":"/home/wang/projects/tks_project/tks/app/js/common/profile_directive.js","../common/tks_directives.js":"/home/wang/projects/tks_project/tks/app/js/common/tks_directives.js","../common/tks_directives2.js":"/home/wang/projects/tks_project/tks/app/js/common/tks_directives2.js","../common/tks_filters.js":"/home/wang/projects/tks_project/tks/app/js/common/tks_filters.js","../common/tks_services.js":"/home/wang/projects/tks_project/tks/app/js/common/tks_services.js","./mod_authorize.js":"/home/wang/projects/tks_project/tks/app/js/modules/mod_authorize.js","./mod_cards.js":"/home/wang/projects/tks_project/tks/app/js/modules/mod_cards.js","./mod_countries.js":"/home/wang/projects/tks_project/tks/app/js/modules/mod_countries.js","./mod_pager.js":"/home/wang/projects/tks_project/tks/app/js/modules/mod_pager.js"}],"/home/wang/projects/tks_project/tks/app/js/modules/utilities.js":[function(require,module,exports){
"use strict";
var countries = require('countries');
var input_formats = require("../../../data/formats.json");
var oapi = require("../../../data/oapi.json");
var aripo = require("../../../data/aripo.json");

function getIEVersion(agent) {
  var reg = /MSIE\s?(\d+)(?:\.(\d+))?/i;
  var matches = agent.match(reg);
  if (matches !== null) {
    return { major: matches[1], minor: matches[2] };
  }
  return { major: "-1", minor: "-1" };
}

function supportedCountriesProc() {

  var ret = {};
  var cnt = 0;
  for (var key in input_formats) {
    if (input_formats.hasOwnProperty(key)) {

      if (countries.get_country(key) !== undefined) {
        ret[key] = { name: countries.get_country(key).name, price: cnt };
        cnt = cnt + 10;
      } else {
        console.log('can not find in country ', key);
      }
    }
  }

  for (key in oapi) {
    if (oapi.hasOwnProperty(key)) {
      if (countries.get_country(key) !== undefined) {
        ret[key] = { name: countries.get_country(key).name, price: 100, link: 'OAPI' };
      }
    }
  }

  // countries in aripo has option to either register as specific country or under ARIP treaty
  for (key in aripo) {
    if (aripo.hasOwnProperty(key)) {
      if (countries.get_country(key) !== undefined) {
        ret[key] = { name: countries.get_country(key).name, price: 100, others: 'ARIPO' };
      }
    }
  }

  return ret;
}

// framework can be : 'angular', 'bootstrap3'

exports.supportedBrowser = function (userAgent, framework) {

  var user_agent = userAgent.toLowerCase();
  // Chrome supported in all platforms

  //  console.log('user agent', user_agent, !!user_agent.match(/trident.*rv[ :]*11\./))

  if (/chrome/.test(user_agent)) return true;else if (/firefox/.test(user_agent)) return true;else if (/safari/.test(user_agent)) return true;else if (/mozilla/.test(user_agent)) return true;else if (/msie 10/.test(user_agent)) return true;else if (!!user_agent.match(/trident.*rv[ :]*11\./)) return true;else return false;
};

exports.supportedCountries = function () {

  return supportedCountriesProc();
};

exports.get_countries_by_contnent = function (code) {

  var clst = countries.get_countries_by_contnent(code);
  var slst = supportedCountriesProc();

  return clst.filter(function (e) {
    return slst[e.code] !== undefined;
  });
};

},{"../../../data/aripo.json":"/home/wang/projects/tks_project/tks/data/aripo.json","../../../data/formats.json":"/home/wang/projects/tks_project/tks/data/formats.json","../../../data/oapi.json":"/home/wang/projects/tks_project/tks/data/oapi.json","countries":"/home/wang/projects/tks_project/tks/node_modules/countries/countries.js"}],"/home/wang/projects/tks_project/tks/app/js/web/address_to_profile.js":[function(require,module,exports){
"use strict";

// convert address line to a profile record

exports.parse = function (address_lines) {
    var profile = {
        address: "",
        city: "",
        contact: "",
        country: "",
        email: "",
        inc_country: "",
        nature: "I",
        phone: "",
        profile_name: '',
        state: "",
        street: "",
        zip: ""
    };

    var lines = address_lines.split("\n");
    var n = lines.length;

    if (n > 3) {
        profile.country = get_country_code(lines[n - 1]);
        profile.zip = get_zipcode_proc(lines[n - 2]);
        profile.city = lines[n - 2];
        profile.street = lines[n - 3];
        profile.address = lines[n - 4];
    }

    return profile;
};

var country_codes = {
    "United Kingdom": { code: 'GB' }
};
function get_country_code(country_name) {
    var temp = country_codes[country_name];
    if (temp) return temp.code;else return undefined;
}

var uk_zip_reg = /(?:[A-Za-z]\d ?\d[A-Za-z]{2})|(?:[A-Za-z][A-Za-z\d]\d ?\d[A-Za-z]{2})|(?:[A-Za-z]{2}\d{2} ?\d[A-Za-z]{2})|(?:[A-Za-z]\d[A-Za-z] ?\d[A-Za-z]{2})|(?:[A-Za-z]{2}\d[A-Za-z] ?\d[A-Za-z]{2})/;

// returns a valid zip code, or undefined
// UK only for now

function get_zipcode_proc(line) {
    var ret;
    var r = uk_zip_reg.exec(line);
    if (r) {
        ret = r[0];
    }
    return ret;
}

},{}],"/home/wang/projects/tks_project/tks/app/js/web/controllers/Es6controller.js":[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var Es6controller =

/*@ngInject;*/

["$scope", function Es6controller($scope) {
    _classCallCheck(this, Es6controller);

    this.ctx = $scope;

    this.ctx.msg = 'hello ES612';

    this.ctx.click_me = function (msg) {
        console.log('click me ');
        console.log(msg);
        console.log('click me end');
    };
}];

exports['default'] = Es6controller;
module.exports = exports['default'];

},{}],"/home/wang/projects/tks_project/tks/app/js/web/controllers/UserController.js":[function(require,module,exports){
"use strict";

Object.defineProperty(exports, '__esModule', {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var UserController =

/*@ngInject;*/

["$scope", "$location", "$rootScope", "$translate", "register_trademark", "authService", "AUTH_EVENTS", "Cart", function UserController($scope, $location, $rootScope, $translate, register_trademark, authService, AUTH_EVENTS, Cart) {
    _classCallCheck(this, UserController);

    $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.cartCount = 0;

    $scope.userName = null;

    $scope.getUserName = function () {
        return authService.userName();
    };

    Cart.getRemoteCartCount().then(function (count) {
        $scope.cartCount = count;
    });

    $scope.$on(AUTH_EVENTS.loginSuccess, function (e) {
        $scope.userName = authService.userName();
    });

    $scope.$on(AUTH_EVENTS.logoutSuccess, function (e) {
        $scope.userName = authService.userName();
    });

    $scope.$on(AUTH_EVENTS.cartUpdated, function (e) {
        Cart.getRemoteCartCount().then(function (count) {
            $scope.cartCount = count;
        });
    });

    $scope.isLoggedIn = function () {
        return authService.isSignedIn();
    };

    $scope.logOut = function () {
        authService.logOut();
        $rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);
        $rootScope.$broadcast(AUTH_EVENTS.cartUpdated);
        $location.path('/');
    };

    $scope.go_case = function (case_no) {
        console.log('case_no', case_no);
    };

    $scope.msg = 'hello me';

    $scope.authenticate = function (user) {

        authService.signIn(user).then(function (status) {
            if (status.authenticated === true) {
                console.log('u r in');
                /*
                 Cart.getRemoteCartCount().then(function (count) {
                 if (count > 0)
                 $location.path('/shopping_cart')
                 else
                 $location.path('/dashboard')
                 })
                 */

                Cart.store_to_server().then(function (status) {
                    console.log('store to server status', status);
                    Cart.clean_local_store();

                    register_trademark.init_item();
                    $rootScope.$broadcast(AUTH_EVENTS.cartUpdated);

                    $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                    Cart.getRemoteCartCount().then(function (count) {
                        if (count > 0) $location.path('/shopping_cart');else $location.path('/dashboard');
                    });
                });
            } else {
                $scope.alerts = [];
                $scope.alerts.push({ type: 'warning', msg: status.message });
            }
        });
    };

    $scope.register = function (newUser) {

        if (newUser.password === newUser.password2) {
            $scope.alerts = [];
            authService.signUp(newUser).then(function (status) {
                if (status.registered) {
                    $scope.authenticate({ email: newUser.email, password: newUser.password });
                } else {
                    $scope.alerts = [];
                    $scope.alerts.push({ type: 'warning', msg: status.message });
                }
            });
        } else {
            $scope.alerts = [];
            $scope.alerts.push({ type: 'warning', msg: "Password and retyped password not equal" });
        }
    };

    $scope.forgot_password = function (user) {
        if (user === undefined || !user.email) {
            $scope.alerts = [];
            $scope.alerts.push({ type: 'warning', msg: 'Please enter an email address first' });
        } else {

            authService.request_password_reset(user.email).then(function (result) {
                $scope.alerts = [];
                if (result.status) $scope.alerts.push({
                    type: 'success',
                    msg: 'We have sent an email with password reset instruction, please check your inbox'
                });else $scope.alerts.push({ type: 'warning', msg: result.message });
            });
        }
    };

    $scope.set_language = function (lang_code) {
        $translate.use(lang_code);
    };
}];

exports['default'] = UserController;
module.exports = exports['default'];

},{}],"/home/wang/projects/tks_project/tks/app/js/web/locale_en.json":[function(require,module,exports){
module.exports={
  "G_GO":"Go",
  "G_SEND":"Send",
  "G_COPYRIGHT":"All rights reserved.",
  "M_LOGIN_HERE":"Login Here",
  "M_LOGIN":"Login",
  "M_SIGNUP":"New User",
  "M_RECOVER_PWD":"Recover Password",
  "M_SHOPPING_CART":"Shopping Cart",
  "M_CASE_NUMBER":"Please enter case number, if applicable:",
  "M_APPLY":"Apply",
  "M_HOME":"Home",
  "M_HOME_BOX1":"TRADEMARKERS are specialists in handling the registration of your trademarks worldwide.",
  "M_HOME_BOX2":" Your one stop shop to have your brand registered by experienced attorneys in 195 countries and jurisdictions.",
  "M_HOME_REG_IN":"I would like to register a trademark in :",
  "M_TRADEMARK_REGISTRATION" :"Trademark Registration",
  "M_CONTACT":"Contact",
  "M_ABOUT":"About",
  "M_TM_CLASSES":"Trademark Classes",
  "M_TERMS":"Terms and Conditions",
  "M_SEARCH_NICE_CLASSES":"Search NICE Classes",
  "M_CONTACT_US":"Contact us",
  "M_CONTACT_NAME":"Name",
  "M_CONTACT_EMAIL":"Email Address",
  "M_CONTACT_TEL":"Telephone",
  "M_CONTACT_MSG":"Message",
  "M_COTACT_OUR_EMAIL":"Email Us",
  "M_ABOUT_1":"Trademarkers.com is a leading international brand protection company that specializes in global trademark and domain name registration. Our accredited and internationally respected lawyers have helped numerous customers protect their brands and intellectual property worldwide.",
  "M_ABOUT_2":"Our clients range from individuals who wish to protect one idea in one country, to intellectual property attorneys with multiple clients, to large businesses trademarking their brands throughout entire industries globally.",
  "M_ABOUT_3":"We have a team of lawyers, with years of experience in Trademark Registrations, which allows us to assure you:",
  "M_ABOUT_3_1":"Competent professionals will perform all necessary procedures for the registration of your trademark",
  "M_ABOUT_3_2":"You will receive timely information regarding updates to the process from dedicated account representatives",
  "M_ABOUT_3_3":"If objections arise in the registration process (opposition, refusal, etc.), knowledgeable lawyers will advise you on the appropriate course of action",
  "M_ABOUT_3_4":"You can follow the interim status of your trademark in real­time, from application to registration, through a secure member’s section",
  "M_ABOUT_3_5":"All information provided to us will be kept in absolute confidentiality ­ please view our confidentiality policy",
  "M_ABOUT_6":"",
  "M_ABOUT_M":"About Trademarkers.com",
  "M_ABOUT_WHY":"Why Trademarkers.com ?",
  "M_TRADEMARK_STEP1":"Step 1: Trademark Comprehensive Study:",
  "M_TRADEMARK_STEP2":"Step 2: Trademark Registration Request:",
  "M_TRADEMARK_START1":"Start Study",
  "M_TRADEMARK_START2":"Start Registration",
  "M_TRADEMARK_1":"Trademark search report with Attorney's analysis about registration probabilities. This report is optional but highly recommended.",
  "M_SIDEPANEL_REG_PRICE":"Registration Prices"


}
},{}],"/home/wang/projects/tks_project/tks/app/js/web/locale_zh-CN.json":[function(require,module,exports){
module.exports={
    "G_COPYRIGHT":"版权所有",
    "G_GO":"开始",
    "G_SEND":"发送",
    "M_LOGIN_HERE":"请在此登入",
    "M_LOGIN":"登入",
    "M_SIGNUP":"注册",
    "M_RECOVER_PWD":"忘记密码",
    "M_SHOPPING_CART":"购物车",
    "M_CASE_NUMBER":"如果您有CASE NUMBER，请在此输入 :",
    "M_APPLY":"赎回",
    "M_HOME":"首页",
    "M_HOME_BOX1":"Trademarkers可在世界各地为您办理商标注册。",
    "M_HOME_BOX2":"我们经验丰富的商标律师分布在一百多个国家跟地区、为您提供一站式的服务。",
    "M_HOME_REG_IN":"请帮我在以下的国家登记商标 :",
    "M_TRADEMARK_REGISTRATION" :"商标登记",
    "M_CONTACT":"联系",
    "M_ABOUT":"关于我们",
    "M_TM_CLASSES":"商标类",
    "M_TERMS":"使用条款",
    "M_SEARCH_NICE_CLASSES":"搜索NICE商标类",
    "M_CONTACT_US":"联系我们",
    "M_CONTACT_NAME":"姓名",
    "M_CONTACT_EMAIL":"电邮",
    "M_CONTACT_TEL":"电话",
    "M_CONTACT_MSG":"内容",
    "M_COTACT_OUR_EMAIL":"电邮",
    "M_ABOUT_1":"Trademarkers.com是一家全球领先的国际品牌保护公司，专门从事全球性的商标和域名注册。我们认可的和在国际上受到尊重的律师已经帮助众多客户在全球范围内保护其品牌和知识产权。",
    "M_ABOUT_2":"我们的客户从在一个国家保护自己的构想个人、到有众多客户的知识产权律师、到在全球范围内全方位保护他们的品牌大型商业机构、应有尽有.",
    "M_ABOUT_3":"我们有一个律师团队，拥有多年的商标注册的经验，这使得我们可以向您保证：",
    "M_ABOUT_3_1":"合格的专业人员将为您商标注册进行所有必要的手续",
    "M_ABOUT_3_2":"您将有分配给您的客户代表，从他那您会收到即时的关于您的商标的任何信息",
    "M_ABOUT_3_3":"如果在登记过程中有反对意见出现（反对，拒绝等），经验丰富的律师会建议您适当的补救方法",
    "M_ABOUT_3_4":"您可以实时的掌控您的申请、从申请到登记",
    "M_ABOUT_3_5":"您所提供的所有信息都将被绝对保密，请查看我们的保密政策",
    "M_ABOUT_M":"Trademarkers.com简介",
    "M_ABOUT_WHY":"为什么要选Trademarkers.com ?",
    "M_TRADEMARK_STEP1":"第1步：商标综合研究",
    "M_TRADEMARK_STEP2":"第2步：商标注册申请：",
    "M_TRADEMARK_START1":"开始研究",
    "M_TRADEMARK_START2":"开始申请",
    "M_TRADEMARK_1":"商标检索报告与律师的有关注册概率分析。这份报告是可选的，但强烈建议。",
    "M_SIDEPANEL_REG_PRICE":"商标登记价格"
}






},{}],"/home/wang/projects/tks_project/tks/app/js/web/price_calc.js":[function(require,module,exports){

// price calculator

'use strict';

function get_price_object_by_type(type, price) {
    if (price) {
        if (price instanceof Array) {
            if (type === 'wl' || type === 'tml') return price[1];else return price[0];
        } else {
            return price;
        }
    } else return undefined;
}

function get_amount_proc(item, prices) {
    var price = get_price_object_by_type(item.type, prices);
    console.log('price222', price);
    var amt = price.reg1;
    if (!price.mc) {
        for (var j = 1; j < item.classes.length; j++) {
            amt = amt + price.reg2;
        }
    } else {
        for (var k = price.mc_num; k < item.classes.length; k++) {
            amt = amt + price.reg2;
        }
    }
    if (item.claim_priority && price.priority_filing) amt = amt + price.priority_filing;
    return amt;
}

exports.get_amount = function (item, prices) {
    return get_amount_proc(item, prices);
};

},{}],"/home/wang/projects/tks_project/tks/app/js/web/profile_edit.js":[function(require,module,exports){

"use strict";

exports.Profile_Edit = function ($scope, $http, $routeParams, $location, $route) {

  console.log('profiledit');

  $scope.country = 'CA';

  $scope.profile = {
    profile_name: 'frank',
    nature: 'C',
    inc_country: 'CA',
    contact: 'angel',
    email: 'abcde@sample.com',
    city: 'cebu'
  };

  $scope.set_country = function (code) {
    $scope.country = code;
    console.log(code);
    $route.reload();
  };
};

},{}],"/home/wang/projects/tks_project/tks/app/js/web/temp2.json":[function(require,module,exports){
module.exports={
  "HEADLINE": "中文的信息"
}
},{}],"/home/wang/projects/tks_project/tks/app/js/web/web_cart_store.js":[function(require,module,exports){
// @ts-check
"use strict";

exports.Cart_Store = function ($window) {

  this.create = function () {};

  this.destroy = function () {
    delete $window.sessionStorage.cart_items;
    delete $window.sessionStorage.cart_count;
    delete $window.sessionStorage.cart_profiles;
  };

  // this function normialize the profiles into another table

  this.add = function (item) {

    console.log('add item in Cart_store');
    item.$$temp_key = generateQuickGuid(); // this key should be removed before posting to server

    console.log('item in store', item);

    if (item.profile_id) {

      console.log('use profile_id');
    } else if (item.profile) {
      // fw be sure to check the profile is only a reference to an existing profile

      var temp = JSON.stringify(item.profile);
      var profile = JSON.parse(temp);
      var profile_id = generateQuickGuid();

      item.profile_id = profile_id;
      delete item.profile;

      var profiles;

      if ($window.sessionStorage.cart_profiles) profiles = JSON.parse($window.sessionStorage.cart_profiles);else profiles = {};

      profiles[profile_id] = profile;

      $window.sessionStorage.cart_profiles = JSON.stringify(profiles);
    }

    var items;

    if ($window.sessionStorage.cart_items) {
      items = JSON.parse($window.sessionStorage.cart_items);
    } else items = [];

    items.push(item);

    $window.sessionStorage['cart_items'] = JSON.stringify(items);
    $window.sessionStorage['cart_count'] = items.length;
  };

  // delete item does not delete the related profile

  this.delete_item = function (item) {

    var items;
    if ($window.sessionStorage.cart_items) {
      items = JSON.parse($window.sessionStorage.cart_items);
    }

    if (items) {
      var i;
      for (i = 0; i < items.length; i++) {
        if (items[i].$$temp_key === item.$$temp_key) break;
      }
      if (i < items.length) {
        items.splice(i, 1);
        $window.sessionStorage['cart_items'] = JSON.stringify(items);
        $window.sessionStorage['cart_count'] = items.length;
      }
    }
  };

  this.update_item = function (item) {

    var items;
    if ($window.sessionStorage.cart_items) {
      items = JSON.parse($window.sessionStorage.cart_items);
    }

    if (items) {
      var i;
      for (i = 0; i < items.length; i++) {
        if (items[i].$$temp_key === item.$$temp_key) break;
      }
      if (i < items.length) {
        items[i] = item;
        $window.sessionStorage['cart_items'] = JSON.stringify(items);
        $window.sessionStorage['cart_count'] = items.length;
      }
    }
  };

  this.getCount = function () {
    if ($window.sessionStorage.cart_count) return $window.sessionStorage.cart_count;else return 0;
  };

  this.getCart = function () {
    var profiles = getProfilesLocal();
    var items;
    if ($window.sessionStorage.cart_items) {
      items = JSON.parse($window.sessionStorage.cart_items);
    } else items = [];
    var amount = 0;

    for (var i = 0; i < items.length; i++) {
      if (items[i].selected) amount = amount + items[i].amount;
      if (items[i].profile_id) items[i].$profile_name = profiles[items[i].profile_id].profile_name;
    }

    return {
      items: items,
      amount: amount,
      _id: 'local'
    };
  };

  // create a profile in the local storage, used by case_gen for now

  this.create_profile = function (profile) {
    console.log('create_profile', profile);

    var temp = JSON.stringify(profile);
    var profile = JSON.parse(temp);
    var profile_id = generateQuickGuid();

    var profiles;

    if ($window.sessionStorage.cart_profiles) profiles = JSON.parse($window.sessionStorage.cart_profiles);else profiles = {};

    profiles[profile_id] = profile;

    $window.sessionStorage.cart_profiles = JSON.stringify(profiles);

    return profile_id;
  };

  this.getProfiles = function () {
    if ($window.sessionStorage.cart_profiles) {
      var profiles = JSON.parse($window.sessionStorage.cart_profiles);
      var ret = {};
      for (var key in profiles) {
        if (profiles.hasOwnProperty(key)) {
          ret[key] = profiles[key].profile_name;
        }
      }
      return ret;
    } else return undefined;
  };

  this.getProfile = function (profile_id) {
    if ($window.sessionStorage.cart_profiles) {
      var profiles = JSON.parse($window.sessionStorage.cart_profiles);
      return profiles[profile_id];
    } else return undefined;

    /*
    return {
      profile_name : 'Jack'
    }*/
  };

  this.getAllProfiles = function () {
    return getProfilesLocal();
  };

  this.getAllProfilesID = function () {
    var pfiles = getProfilesLocal();
    console.log('pfiles', pfiles);
    return Object.keys(pfiles);
  };

  function generateQuickGuid() {
    return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
  }

  function getProfilesLocal() {
    if ($window.sessionStorage.cart_profiles) {
      return JSON.parse($window.sessionStorage.cart_profiles);
    } else return {};
  }

  return this;
};

},{}],"/home/wang/projects/tks_project/tks/app/js/web/web_controllers.js":[function(require,module,exports){
"use strict";
/*global angular, console */
/* jshint esnext: true */

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

//var co = require('co')

var _controllersEs6controllerJs = require('./controllers/Es6controller.js');

var _controllersEs6controllerJs2 = _interopRequireDefault(_controllersEs6controllerJs);

var _controllersUserControllerJs = require('./controllers/UserController.js');

var _controllersUserControllerJs2 = _interopRequireDefault(_controllersUserControllerJs);

var countries = require('countries');
var utilities = require('../modules/utilities.js');
require('setimmediate');

function append_one_word_country_name(lst) {
    var rlst = [];
    for (var i = 0; i < lst.length; i++) {
        lst[i].simple_name = lst[i].name.replace(/ /g, '_').toLowerCase();
        rlst.push(lst[i]);
    }
    return rlst;
}

/*
 class Es6controller {

 constructor($scope) {

 this.ctx = $scope

 this.ctx.msg = 'hello ES612'

 this.ctx.click_me = function(msg) {
 console.log('click me ')
 console.log(msg)
 console.log('click me end')
 }

 }

 }
 */

//console.log('es66', Es6controller)
//Es6controller.$inject = ["$scope"]

angular.module('tks.controllers', []).controller('es6Ctrl', _controllersEs6controllerJs2['default']).controller('es26Ctrl', ["$scope", function ($scope) {
    $scope.msg = 'hello';
}]).controller('pageCtrl', ["$scope", "page_info", function ($scope, page_info) {
    $scope.page_info = page_info;

    console.log('asfdasdfasdf', page_info);
}]).controller('caseGenCtrl', ["$scope", "$rootScope", "$q", "$window", "$location", "AUTH_EVENTS", "Cart", "Case_gen", "case_obj", require('./web_controllers_case').case_gen]).controller('caseGenCtrl2', ["$q", "$scope", "$rootScope", "$window", "$location", "AUTH_EVENTS", "user_profile", "authService", "Cart", "Cart_Store", "Case_gen", "case_obj", require('./web_controllers_case').case_gen2])

//.controller('homeCtrl', function ($scope, $interval, $location, TMCountries, news_feeder, $translate) {

.controller('homeCtrl', ["$scope", "$interval", "$location", "TMCountries", "news_feeder", "$translate", function ($scope, $interval, $location, TMCountries, news_feeder, $translate) {

    $translate('HEADLINE').then(function (translation) {
        console.log('translate', translation);
    });
    $scope.countries1 = append_one_word_country_name(countries.get_countries(['US', 'MX', 'BR']));
    $scope.countries2 = append_one_word_country_name(countries.get_countries(['EU', 'CH', 'NO']));
    $scope.countries3 = append_one_word_country_name(countries.get_countries(['CN', 'RU', 'VN']));

    console.table($scope.countries1);

    $scope.jsonId = {
        "@context": "http://schema.org",
        "@type": "Place",
        "geo": {
            "@type": "GeoCoordinates",
            "latitude": "40.75",
            "longitude": "73.98"
        },
        "name": "Empire State Building"
    };

    $scope.items = [{ ID: '000001', Title: 'Chicago' }, { ID: '000002', Title: 'New York' }, { ID: '000003', Title: 'Washington' }];

    $scope.thanks = 'thanks';
    $scope.stats = utilities.supportedCountries();
    $scope.map_ready = false;

    $scope.quicklinks = [{ code: 'US', name: 'United States', url: '/trademark-registration-in-united_states' }, { code: 'EU', name: 'Europe', url: '/trademark-registration-in-european_union' }, { code: 'AF', name: 'Africa', url: '/region/AF' }, { code: 'OC', name: 'Oceania', url: '/region/OC' }, { code: 'NA', name: 'North America', url: '/region/NA' }, { code: 'AS', name: 'Asia', url: '/region/AS' }];

    $scope.selectedLink = $scope.quicklinks[0];

    $scope.goto_case = function (case_code) {
        console.log('case code is ', case_code);
        $location.path('/case/' + case_code);
    };

    /*
     $scope.$watch('selectedLink2', function (newVal, oldVal) {
     console.log('changed', newVal, oldVal)
     if (newVal) {
     $location.path(newVal.url)
     }
     });
     */

    /*
     $scope.feeds = news_feeds2
      function get_feeds() {
     news_feeder.get_feeds(5).then(function (feeds) {
     console.log('feed return', feeds)
     for (var i = 0; i < feeds.length; i++) {
     console.log(i, feeds[i])
     $scope.feeds.push(feeds[i])
     }
     })
     }
      function update_current_feed() {
      console.log('updatef feed')
     if ($scope.feeds.length > 0) {
     $scope.current_feed = $scope.feeds[0]
     $scope.feeds.shift()
     } else
     get_feeds()
       }
     */

    //get_feeds()

    //$interval(update_current_feed, 5000)

    //$interval(get_feeds, 5000);
}]).controller('attorneysCtrl', ["bio_data", function (bio_data) {
    this.obj = bio_data;
}]).controller('mylogsCtrl', ["$scope", "$http", function ($scope, $http) {

    $scope.msg = 'my logs';

    $http.get('/api2/mylogs/').success(function (data) {
        $scope.logs = data;
        $scope.ready = true;
    });
}]).controller('regionCtrl', ["$scope", '$routeParams', "TMCountries", "page_info", function ($scope, $routeParams, TMCountries, page_info) {

    console.log($routeParams);
    $scope.code = $routeParams.code;

    var tab = {
        'EU': 'Europe',
        'AF': 'Africa',
        'OC': 'Oceania',
        'NA': 'North America',
        'AS': 'Asia',
        'ME': 'Middle East',
        'SA': 'South America',
        'CA': 'Central America'
    };

    console.log('tab code ', tab[$scope.code]);

    if (tab[$scope.code]) {
        var seo_string = "Trademark Registration in " + tab[$scope.code] + " - TradeMarkers®.com";
        page_info.setTitle(seo_string);
        page_info.set_keywords(seo_string);
        page_info.set_description(seo_string);
    }

    /*
     //    var lst = countries.get_countries_by_contnent($routeParams.code)
     var lst = utilities.get_countries_by_contnent($routeParams.code)
      var nums = Math.ceil(lst.length / 3)
      console.log('lst', lst)
     console.log(nums, lst.length)
     //    $scope.countries1 = countries.get_countries(['US', 'MX', 'BR'])
     $scope.countries1 = lst.slice(0, nums)   // 0 13
     $scope.countries2 = lst.slice(nums , 2* nums  ) // 14
     $scope.countries3 = lst.slice(2*nums , 3* nums  )
      $scope.thanks = 'thanks'
     */
}]).controller('404Ctrl', ['$scope', '$routeParams', 'DBStore', function ($scope, $routeParams, DBStore) {
    console.log($routeParams);

    if ($routeParams.key) {
        $scope.response = DBStore.get($routeParams.key);
    } else $scope.response = 'Sorry, page not found';
}]).controller('HeaderController', ["$scope", "$location", "$rootScope", "authService", "Cart", "AUTH_EVENTS", 'TM_MENU', function ($scope, $location, $rootScope, authService, Cart, AUTH_EVENTS, TM_MENU) {

    $scope.tm_menu = TM_MENU;
    console.log($scope.tm_menu);

    console.log('AAAAAAAAAAAAAAAAA', $location.path());

    $scope.init = function () {
        console.log('initting');
        if (authService.isSignedIn()) {
            if ($scope.userName === null) $scope.userName = authService.userName();
            if ($scope.cartCount === 0 && authService.isSignedIn()) Cart.getRemoteCartCount().then(function (count) {
                $scope.cartCount = count;
            });
        }
    };

    $scope.isBrowserSupported = function () {
        return utilities.supportedBrowser(navigator.userAgent, 'angular');
    };

    $scope.isActive = function (viewLocation) {
        console.log('isactive', viewLocation === $location.path(), $location.path());
        return viewLocation === $location.path();
    };

    $scope.isLoggedIn = function () {
        return authService.isSignedIn();
    };

    $scope.logOut = function () {
        authService.logOut();
        $rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);
        $rootScope.$broadcast(AUTH_EVENTS.cartUpdated);
        $location.path('/');
    };

    $scope.userName = null;
    $scope.cartCount = 0;
    Cart.getRemoteCartCount().then(function (count) {
        $scope.cartCount = count;
    });

    $scope.$on(AUTH_EVENTS.loginSuccess, function (e) {
        $scope.userName = authService.userName();
    });

    $scope.$on(AUTH_EVENTS.logoutSuccess, function (e) {
        $scope.userName = authService.userName();
    });

    $scope.$on(AUTH_EVENTS.cartUpdated, function (e) {
        Cart.getRemoteCartCount().then(function (count) {
            $scope.cartCount = count;
        });
    });

    $scope.init();

    $scope.goto_case = function (case_code) {
        console.log('case code is ', case_code);
        if (case_code !== undefined) $location.path('/case/' + case_code);
    };
}]).controller('classesCtrl', ['$scope', '$http', function ($scope, $http) {

    $scope.ready = true;

    $http.get('/api2/cr_classes/').success(function (data) {
        console.log('listing classes');
        $scope.classes = data;
        $scope.ready = true;
    });
}]).controller('cartController', ['$scope', '$http', '$rootScope', '$location', 'Cart', 'AUTH_EVENTS', '$modal', require('./web_controllers_cart').cart]).controller('cartThanksController', ['$scope', '$routeParams', function ($scope, $routeParams) {

    $scope.order_key = $routeParams.order_key;
}]).controller('ModalInstanceCtrl2', ['$scope', '$modalInstance', 'details', function ($scope, $modalInstance, details) {
    $scope.details = details;

    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.cancel = function () {
        //        $modalInstance.dismiss('cancel');
    };
}]).controller('orderDetailController', ['$scope', '$http', '$rootScope', '$routeParams', 'Cart', 'Session', 'AUTH_EVENTS', 'Orders', function ($scope, $http, $rootScope, $routeParams, Cart, Session, AUTH_EVENTS, Orders) {

    $scope.ref = $routeParams.id;

    $http.get('/api/order/' + $routeParams.id).success(function (data) {
        console.log('order', data);
        $scope.details = data.data;
    });

    /*
    $scope.get_receipt = function (order_id) {
        console.log(order_id)
        $http.get('/api/receipt/' + order_id).success(function (data) {
            console.log(data)
        })
    }
    */
}]).controller('ordersController', ['$scope', '$http', '$rootScope', '$routeParams', 'Cart', 'Session', 'AUTH_EVENTS', 'Orders', function ($scope, $http, $rootScope, $routeParams, Cart, Session, AUTH_EVENTS, Orders) {

    $scope.pageSize = 10;

    $scope.setDataSource = function (page_no) {
        console.log(page_no);
        $scope.orders = Orders.query({ page_no: page_no, page_size: 10 }, function () {});
    };

    $scope.setNumberOfPages = function () {
        console.log('init');
        $http.get('/api/orders_count').success(function (data) {
            $scope.numberOfPages2 = Math.ceil(data.count / $scope.pageSize);
            console.log($scope.numberOfPages2);
        });
    };

    $scope.currentPage = $routeParams.page_no ? $routeParams.page_no - 1 : 0;
    $scope.setDataSource($scope.currentPage); // good for first page
}]).controller('registrationsController', ['$scope', '$http', '$rootScope', '$routeParams', 'Cart', 'Session', 'AUTH_EVENTS', 'Registrations', function ($scope, $http, $rootScope, $routeParams, Cart, Session, AUTH_EVENTS, Registrations) {

    $scope.pageSize = 10;

    $scope.setDataSource = function (page_no) {
        console.log(page_no);
        $scope.registrations = Registrations.query({ page_no: page_no, page_size: 10 }, function () {});
    };

    $scope.setNumberOfPages = function () {
        console.log('init');
        $http.get('/api/registrations_count').success(function (data) {
            $scope.numberOfPages2 = Math.ceil(data.count / $scope.pageSize);
            console.log($scope.numberOfPages2);
        });
    };

    $scope.currentPage = $routeParams.page_no ? $routeParams.page_no - 1 : 0;
    $scope.setDataSource($scope.currentPage); // good for first page
}]).controller('registrationDetailController', ['$scope', '$http', '$rootScope', '$routeParams', 'Cart', 'Session', 'AUTH_EVENTS', 'Registrations', function ($scope, $http, $rootScope, $routeParams, Cart, Session, AUTH_EVENTS, Registrations) {

    console.log('xx', $routeParams.id);
    $scope.ready = false;

    if ($routeParams.id) {
        $scope.ref = $routeParams.id;

        $http.get('/api/registration/' + $scope.ref).success(function (data) {
            console.log(data);
            $scope.details = data.data;
            $scope.ready = true;
        });
    }
}]).controller('help_info', ['$scope', '$routeParams', '$location', '$modal', '$interval', 'news_feeder', function ($scope, $routeParams, $location, $modal, $interval, news_feeder) {

    console.log('helpinfo', $location.path());
    var country;
    if ($routeParams.country) country = $routeParams.country;else {
        var temp = countries.get_country_by_regex(/^\/trademark-registration-in-(\w+)/i, $location.path());
        if (temp) country = temp.code;
    }

    if (country) $scope.country_flag = country.toLowerCase() + '.png';

    $scope.popup = function (info) {
        console.log($routeParams);
        var info2 = info + country;
        $modal.open({
            templateUrl: 'partials/modal_info.html',
            windowClass: 'app-modal-window',
            controller: 'ModalInfoCtrl', resolve: {
                info_key: function info_key() {
                    return info2;
                }
            }
        });
    };

    $scope.feeds = [];

    function get_feeds() {
        news_feeder.get_feeds(1).then(function (feed) {
            $scope.feeds.push(feed);
            if ($scope.feeds.length > 3) $scope.feeds.shift();
        });
    }

    get_feeds();

    //    $interval(get_feeds, 10000);  //fw! uncomment when deployed
}]).controller('UserController', _controllersUserControllerJs2['default']).controller('dashboardCtrl', ['$scope', '$http', 'authService', 'Session', function ($scope, $http, authService, Session) {

    $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $http.get('/api/myaccount/').success(function (data) {
        console.log(data);
        $scope.user = data;
        $scope.ready = true;
    });

    $scope.reset_password = function () {

        console.log('reset password', Session.getUserID());

        authService.request_password_reset(Session.getUserID()).then(function (result) {
            $scope.alerts = [];
            if (result.status) $scope.alerts.push({
                type: 'success',
                msg: 'We have sent an email with password reset instruction, please check your inbox'
            });else $scope.alerts.push({ type: 'warning', msg: result.message });
        });
    };
}]).controller('search_classesController', ['$scope', 'Search_classes', function ($scope, Search_classes) {

    $scope.msg = 'result empty yet';

    $scope.search_now = function (search) {
        $scope.hasResult = false;
        Search_classes.search(search).then(function (result) {
            console.log(result);
            if (result.status) {
                $scope.result = result.result;
                $scope.hasResult = true;
            }
        });
    };
}])

// Please note that Instance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.

.controller('ModalInstanceCtrl', ["$scope", "$modalInstance", "Search_classes", function ($scope, $modalInstance, Search_classes) {

    //  $scope.items = items;
    $scope.selected = {
        //    item: $scope.items[0]
    };

    $scope.ok = function () {
        console.log($scope.result);

        if ($scope.result === undefined) $modalInstance.close([]);else {
            var classes_selected = [];
            for (var i = 0; i < $scope.result.length; i++) {
                var item = $scope.result[i];

                var selected = item.basics.filter(function (element) {
                    return element.selected;
                });
                console.log(selected, selected.length);
                if (selected.length > 0) {
                    // use only the first item selected, as all belong to the same class
                    classes_selected.push({
                        code: item.code,
                        basic_number: selected[0].bnumber,
                        basic_desc: selected[0].bdesc
                    });
                }
            }
            $modalInstance.close(classes_selected);
        }
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.search_now = function (search_text) {
        console.log('searc now');
        Search_classes.search(search_text).then(function (result) {
            console.log(result);
            if (result.status) {
                $scope.result = result.result;
                $scope.hasResult = true;
            }
        });
    };
}]).controller('ModalInfoCtrl', ["$scope", "$sce", "$modalInstance", 'info_key', "help_info", function ($scope, $sce, $modalInstance, info_key, help_info) {

    //  $scope.items = items;
    //  $scope.selected = {
    //    item: $scope.items[0]
    //  };

    console.log('opening', info_key);
    help_info.get(info_key).then(function (status) {
        console.log('ret');
        console.log(status);
        var info = status.result;
        //    $scope.result = status.result
        $scope.info_body = $sce.trustAsHtml(info.body);
        $scope.result = info;
    });

    $scope.ok = function () {
        console.log($scope.result);

        if ($scope.result === undefined) $modalInstance.close([]);else {
            var classes_selected = [];
            for (var i = 0; i < $scope.result.length; i++) {
                var item = $scope.result[i];

                var selected = item.basics.filter(function (element) {
                    return element.selected;
                });
                console.log(selected, selected.length);
                if (selected.length > 0) {
                    // use only the first item selected, as all belong to the same class
                    classes_selected.push({
                        code: item.code,
                        basic_number: selected[0].bnumber,
                        basic_desc: selected[0].bdesc
                    });
                }
            }
            $modalInstance.close(classes_selected);
        }
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]).controller('contactCtrl', ['$scope', '$http', function ($scope, $http) {

    $scope.didClicked = false;

    $scope.contact_proc = function (contact) {
        $scope.didClicked = true;
        console.log('once ', contact, $scope.didClicked);
        $http.post("/contactus", contact).success(function (data) {
            console.log(data);
            if (data.status === 'ok') {
                $scope.show_msg = "Thanks for contacting us.";
            } else {
                alert(data.msg);
            }
        });
    };
}]).controller('accountController', ['$scope', '$http', '$timeout', 'countries', function ($scope, $http, $timeout, countries) {

    $scope.countries = countries;
    $http.get('/api/myaccount/').success(function (data) {
        console.log(data);
        $scope.user = data;
        $scope.ready = true;
    });

    $scope.update_user = function (user) {
        $http.post("/api/update_user", user).success(function (data) {

            $scope.alerts = [{ type: 'success', msg: 'Your account has been updated' }];
            $timeout(function () {
                $scope.alerts = [];
            }, 1500);
        });
    };
}]).controller('profileController', ['$scope', '$http', '$routeParams', '$location', '$route', '$timeout', 'registry_initData', 'user_profile', function ($scope, $http, $routeParams, $location, $route, $timeout, registry_initData, user_profile) {

    console.log('init data ', registry_initData);

    $scope.profile = registry_initData.profile;

    $scope.next = function (profile) {

        var err_msg;

        if (!profile.profile_name) err_msg = 'Please provide a name for this profile';
        if (!err_msg && !profile.contact) err_msg = 'Please provide a contact person';
        if (!err_msg && !profile.email) err_msg = 'Please provide a valid email address so we can contact you';

        if (err_msg) $scope.alerts = [{ type: 'warning', msg: err_msg }];else {

            console.log('posting profile ', profile);

            user_profile.post(profile).then(function (status) {
                console.log(status);
                if (status.id) // this happens only for new profile
                    $location.path('/profile_address/' + status.id);else {
                    $scope.alerts = [{ type: 'success', msg: 'Your profile has been updated' }];
                    $timeout(function () {
                        $scope.alerts = [];
                    }, 1500);
                }
            });

            /*
             delete reg.profile_id
             register_trademark.save(reg)
             console.log('reg0',reg)
             $location.path('/trademark_address/' + $routeParams.country)
             */
        }
    };
}]).controller('profileListController', ['$scope', '$http', '$route', '$location', '$routeParams', 'countries', function ($scope, $http, $route, $location, $routeParams, countries) {

    $http.get('/api/profiles/').success(function (data) {
        console.log(data);
        $scope.profiles = data;
    });

    $scope.del_proc = function (rec) {

        console.log(rec);
        if (confirm('Do you really want to delete this profile : ' + rec.profile_name)) {

            $http.post("/api/delete_profile", rec).success(function (data) {
                console.log(data.status);
                if (data.status) $route.reload();else alert(data.msg);
            });

            /*
             $http.post('/api/delete_profile', rec._id).success(function (data) {
             console.log(data)
             if (data.status === 'ok') {
             $scope.get_users()
             } else {
             alert(data.msg)
             }
             });*/
        }
    };
}]).controller('reg_allCtrl', ['$scope', '$http', '$routeParams', 'TM_MENU', function ($scope, $http, $routeParams, TM_MENU) {

    $scope.countries = TM_MENU.main_menu;
    console.log(TM_MENU.main_menu);
}]).controller('prices_controller', ['$scope', '$http', '$routeParams', "$sce", 'help_info', 'TM_MENU', function ($scope, $http, $routeParams, $sce, help_info, TM_MENU) {

    console.log('countries');
    $scope.country = $routeParams.country;
    $scope.countries = TM_MENU.main_menu;
    console.log(TM_MENU.main_menu);

    $scope.country2 = $scope.country;

    if ($scope.country === 'NO') // when country is Norway(NO), it will be 'falsey' in the html, use country2 to test it in html to by pass this
        $scope.country2 = 'NO_';

    var info_key = "TM_PRICE_" + $routeParams.country;

    help_info.get(info_key).then(function (status) {
        console.log('ret');
        console.log(status);
        var info = status.result;
        $scope.info_body = $sce.trustAsHtml(info.body);
        $scope.result = info;
    });
}]).controller('dpm_test', ['$scope', '$http', '$routeParams', function ($scope, $http, $routeParams) {

    console.log('dpm test');

    $http.get('/api/get_dpm_test').success(function (data) {
        console.log(data);
        $scope.dpm = data;
    });
}]).controller('check_out_controller', ['$scope', '$http', '$routeParams', '$locale', 'countries', 'Cards', function ($scope, $http, $routeParams, $locale, countries, Cards) {

    $scope.currentYear = new Date().getFullYear();
    $scope.currentMonth = new Date().getMonth() + 1;
    $scope.months = $locale.DATETIME_FORMATS.MONTH;
    $scope.countries = countries;

    $scope.formData = {};

    var set_expiry = function set_expiry() {
        if ($scope.formData.exp_year && $scope.formData.exp_month) {
            $scope.formData.x_exp_date = $scope.formData.exp_month + '/' + $scope.formData.exp_year;
        }
    };

    Cards.get_user_info($routeParams.cart_id).then(function (info) {
        $scope.formData = info; // see above for data returned
    });

    Cards.get_shopping_cart_dpm($routeParams.cart_id).then(function (data) {

        console.log('haha back from dpm ', data);
        $scope.dpm = data;
    });

    $scope.$watch('formData.exp_month', function (newVal, oldVal) {
        console.log('changed', newVal, oldVal);
        set_expiry();
    });

    $scope.$watch('formData.exp_year', function (newVal, oldVal) {
        console.log('changed', newVal, oldVal);
        set_expiry();
    });

    $scope.processForm = function () {
        console.log('processed');
        $scope.msg = 'Contacting Payment Gateway, Please wait...';
    };
}]).controller('post_payment_controller', ['$scope', '$http', '$routeParams', function ($scope, $http, $routeParams) {

    console.log('dpm test', $routeParams);

    $http.get('/api/get_gw_response/' + $routeParams.key).success(function (data) {
        console.log(data);
        $scope.resp = data;
    });
}])

// key in this route is the id for the order
.controller('post_payment2_controller', ['$scope', '$http', '$routeParams', function ($scope, $http, $routeParams) {
    $scope.order_key = $routeParams.key;
}]).controller('check_out_stripe_controller', ['$scope', '$http', '$routeParams', '$location', '$rootScope', '$locale', '$window', 'Cards', 'AUTH_EVENTS', require('./web_controllers_stripe').stripe]).controller('eu_cookies_controller', ['$http', '$routeParams', '$location', '$route', '$locale', '$window', 'Cards', 'AUTH_EVENTS', require('./web_controllers_eu_cookies').eu_cookies]).controller('before_check_out', ['$scope', '$routeParams', '$location', '$rootScope', 'authService', 'Cart', 'valid_emails', 'AUTH_EVENTS', require('./web_controllers_check_out').b4check_out]).controller('before_check_out_non_member', ['$scope', '$routeParams', '$location', '$rootScope', 'authService', 'Cart', 'AUTH_EVENTS', require('./web_controllers_check_out').b4check_out_non_member]).controller('profile_edit', ['$scope', '$http', '$routeParams', '$location', '$route', require('./profile_edit').Profile_Edit]);

/*
 .controller('CarouselDemoCtrl', function ($scope) {
 $scope.coverflow = {};
 $scope.images = [
 '/angular-coverflow/covers/1.jpg',
 '/angular-coverflow/covers/2.jpg',
 '/angular-coverflow/covers/3.jpg',
 '/angular-coverflow/covers/4.jpg',
 '/angular-coverflow/covers/5.jpg',
 '/angular-coverflow/covers/6.jpg',
 '/angular-coverflow/covers/7.jpg',
 '/angular-coverflow/covers/8.jpg',
 '/angular-coverflow/covers/9.jpg',
 '/angular-coverflow/covers/10.jpg',
 '/angular-coverflow/covers/11.jpg',
 '/angular-coverflow/covers/12.jpg',
 '/angular-coverflow/covers/13.jpg',
 '/angular-coverflow/covers/14.jpg',
 '/angular-coverflow/covers/15.jpg',
 '/angular-coverflow/covers/16.jpg',
 '/angular-coverflow/covers/17.jpg',
 '/angular-coverflow/covers/18.jpg',
 '/angular-coverflow/covers/19.jpg',
 '/angular-coverflow/covers/20.jpg'
 ];
 $scope.imageSelected = function () {
 console.log('img clk')
 }
 })*/

},{"../modules/utilities.js":"/home/wang/projects/tks_project/tks/app/js/modules/utilities.js","./controllers/Es6controller.js":"/home/wang/projects/tks_project/tks/app/js/web/controllers/Es6controller.js","./controllers/UserController.js":"/home/wang/projects/tks_project/tks/app/js/web/controllers/UserController.js","./profile_edit":"/home/wang/projects/tks_project/tks/app/js/web/profile_edit.js","./web_controllers_cart":"/home/wang/projects/tks_project/tks/app/js/web/web_controllers_cart.js","./web_controllers_case":"/home/wang/projects/tks_project/tks/app/js/web/web_controllers_case.js","./web_controllers_check_out":"/home/wang/projects/tks_project/tks/app/js/web/web_controllers_check_out.js","./web_controllers_eu_cookies":"/home/wang/projects/tks_project/tks/app/js/web/web_controllers_eu_cookies.js","./web_controllers_stripe":"/home/wang/projects/tks_project/tks/app/js/web/web_controllers_stripe.js","countries":"/home/wang/projects/tks_project/tks/node_modules/countries/countries.js","setimmediate":"/home/wang/projects/tks_project/tks/node_modules/setimmediate/setImmediate.js"}],"/home/wang/projects/tks_project/tks/app/js/web/web_controllers_cart.js":[function(require,module,exports){
"use strict";

exports.cart = function ($scope, $http, $rootScope, $location, Cart, AUTH_EVENTS, $modal) {

  //  $scope.cart = Cart.getCart()
  //    $scope.name = Session.getName()

  /*
  $scope.alert = function (m) {
    alert(m)
  }
  */

  $scope.isCartEmpty = function (cart) {
    if (!cart) return true;else {
      return cart.items.length === 0;
    }
  };

  $scope.get_remote_cart = function () {

    Cart.getRemoteCart().then(function (data) {
      $scope.cart = data.cart;
      $rootScope.$broadcast(AUTH_EVENTS.cartUpdated);
    });
  };

  $scope.get_remote_cart();

  $scope.delete_item = function (cart_id, rec) {

    if (confirm('Do you really want to delete this item?')) {

      Cart.delete_item(cart_id, rec).then(function (data) {
        $scope.get_remote_cart();
      });
    }
  };

  $scope.upload_local_cart = function (cart_id) {
    console.log('cart_id', cart_id);

    if (confirm('Do you really want to upload this local cart?')) {

      Cart.store_to_server().then(function (status) {
        console.log('store to server status2', status);
        /*
        Cart.clean_local_store()
         register_trademark.init_item()
        $rootScope.$broadcast(AUTH_EVENTS.cartUpdated);
         $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
        Cart.getRemoteCartCount().then(function (count) {
          if (count > 0)
            $location.path('/shopping_cart')
          else
            $location.path('/dashboard')
        })
        */
      });
      /*
      Cart.delete_item(cart_id, rec).then(function (data) {
        $scope.get_remote_cart()
      })*/
    }
  };

  $scope.selection_change = function (cart_id, rec) {
    Cart.selection_change(cart_id, rec).then(function (status) {
      $scope.get_remote_cart();
    });
  };

  $scope.show_details = function (rec) {
    console.log('show_details', rec, $modal);
    console.log('show_$modal', $modal);
    var modalInstance = $modal.open({
      templateUrl: 'partials/shopping_cart_details.html',
      windowClass: 'app-modal-window',
      controller: 'ModalInstanceCtrl2',
      resolve: {
        details: function details() {
          return rec;
        }
      }
    });
  };

  // happens
  $scope.local_store_to_server = function () {
    console.log('local store to server');
    Cart.store_to_server().then(function (status) {
      console.log('store to server status', status);
      Cart.clean_local_store();
    });
  };
};

},{}],"/home/wang/projects/tks_project/tks/app/js/web/web_controllers_case.js":[function(require,module,exports){
"use strict";

var countries = require('countries');

// CONTROLLERS

// controller for case1

exports.case_gen = function ($scope, $rootScope, $q, $window, $location, AUTH_EVENTS, Cart, Case_gen, case_obj) {

    var available_countries = ['US', 'CA', 'BR', 'RU', 'IN', 'CN', 'ZA', 'NO', 'CH'];
    $scope.alerts = [];
    $scope.case_obj = case_obj;

    $scope.countries = countries.get_countries(available_countries);

    $scope.total_amount = 0;
    $scope.net_amount = 0;
    $scope.discount = 10;
    $scope.discount_amount = 100;
    $scope.tm_type = get_type(case_obj);

    if (case_obj.status === 'found') {

        // get prices for countries available

        Case_gen.get_items(case_obj, available_countries).then(function (items) {

            items.map(function (item) {

                for (var i = 0; i < $scope.countries.length; i++) {
                    if ($scope.countries[i].code === item.country) {
                        $scope.countries[i].amount_orig = item.amount_orig;
                        $scope.countries[i].selected = false;
                    }
                }
            });
        });

        // watch user's selection and adjust the amounts

        $scope.$watch('countries', function (newV, oldV) {

            var i,
                countries_cnt = 0;
            $scope.total_amount = 0;

            for (i = 0; i < $scope.countries.length; i++) {
                if ($scope.countries[i].selected) {
                    $scope.countries[i].amount = $scope.countries[i].amount_orig;
                    countries_cnt = countries_cnt + 1;
                    $scope.total_amount = $scope.total_amount + $scope.countries[i].amount;
                } else delete $scope.countries[i].amount;
            }
            if (countries_cnt > 1) $scope.discount = 20;else $scope.discount = 10;
            $scope.discount_amount = Math.round($scope.total_amount * $scope.discount / 100);
            $scope.net_amount = $scope.total_amount - $scope.discount_amount;
        }, true);
    }

    function get_selection() {
        var ret = [];
        for (var i = 0; i < $scope.countries.length; i++) {
            if ($scope.countries[i].selected) {
                ret.push($scope.countries[i].code);
            }
        }
        return ret;
    }

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.submit = function () {

        if (get_selection().length == 0) {
            $scope.alerts = [];
            $scope.alerts.push({
                type: 'warning',
                msg: 'Please select at least one from the countries listed above'
            });
        } else {

            save_selection_to_cart();
        }
    };

    // fw assumes mark always exist

    function get_type(case_o) {
        if (case_o.image_url) return 'wl';else return 'w'; // 'w', 'l', 'wl'   word only/logo/word and logo
    }

    function save_selection_to_cart() {

        Case_gen.fetch_images(window.encodeURIComponent(case_obj.image_url)).then(function (new_url) {

            return { new_url: new_url };
        }).then(function (jret) {

            var profile = Case_gen.get_default_profile(case_obj); // parse the info in case_obj into profile
            return Case_gen.create_profile(profile).then(function (profile_id) {

                jret.profile_id = profile_id;
                return jret;
            });
        }).then(function (jret) {

            return Case_gen.get_items(case_obj, get_selection()).then(function (items) {

                items.map(function (item) {

                    item.disc = $scope.discount;
                    item.amount = Math.round(item.amount_orig * (100 - item.disc) / 100);

                    delete item.profile;
                    item.profile_id = jret.profile_id;
                    if (jret.new_url !== undefined) item.logo_url = jret.new_url;
                });

                return items;
            });
        }).then(function (items) {
            console.log('?', items);

            var posts = [];

            items.map(function (item) {
                posts.push(Cart.post_to_server(item));
            });

            $q.all(posts).then(function (results) {
                console.log('posted results', results);
                // everything has been posted to cart at this point
                $rootScope.$broadcast(AUTH_EVENTS.cartUpdated);
                $location.path('/shopping_cart');
            });
        });
    }

    /*
    Case_gen.get_items(case_obj, selected)  // get price first
         .then(function (items) {
             items.map(function (item) {
                 item.disc = $scope.discount
                item.amount = Math.round(item.amount_orig * (100 - item.disc) / 100)
             })
             Case_gen.fetch_images(window.encodeURIComponent(case_obj.image_url)).then(function (new_url) {
                 var profile = Case_gen.get_default_profile(case_obj)
                 Case_gen.create_profile(profile).then(function (profile_id) {
                     var posts = []
                     items.map(function (item) {
                        delete item.profile
                        item.profile_id = profile_id
                        if (new_url)
                            item.logo_url = new_url
                        posts.push(Cart.post_to_server(item))
                    })
                     $q.all(posts).then(function (results) {
                        console.log('posted results', results)
                        // everything has been posted to cart at this point
                        $rootScope.$broadcast(AUTH_EVENTS.cartUpdated);
                        $location.path('/shopping_cart')
                    })
                })
            })
        })
     }
    */
};

// controller for case2
/*
 exports.case_gen2 = function ($q, $scope, $rootScope, $window, $location, AUTH_EVENTS, user_profile, authService, Cart,
 Cart_Store, Case_gen, case_obj) {

 $scope.alerts = [];
 $scope.case_obj = case_obj

 $scope.total_amount = 0
 $scope.tm_type = get_type(case_obj)

 $scope.profile_name = ''
 $scope.profile_email = ''

 var selection_obj = JSON.parse($window.sessionStorage.case_obj)

 Case_gen.get_items(selection_obj.case_obj, selection_obj.selection)

 .then(function (items) {
 console.log('items', items)
 var disc = items.length > 1 ? 20 : 10
 items.map(function (item) {
 console.log(item)
 item.disc = disc
 item.amount = Math.round(item.amount_orig * (100 - disc) / 100)
 $scope.total_amount = $scope.total_amount + item.amount

 })
 console.log('item2', items, $scope.total_amount)
 $scope.items = items

 })

 $scope.closeAlert = function (index) {
 $scope.alerts.splice(index, 1);
 };

 $scope.back = function () {
 $location.path('/case/' + $scope.case_obj.case_code)
 }

 $scope.submit = function () {

 var msg = ''

 if (!$scope.profile_name)
 msg = 'Please provide a name'
 else if (!$scope.profile_email)
 msg = 'Pleaes provide an email address'

 msg = ''

 if (msg) {
 $scope.alerts = []
 $scope.alerts.push({
 type: 'warning',
 msg: msg
 });
 } else {

 if ($scope.items.length > 0) {
 console.log('items222', $scope.items)

 delete_cached_case_obj($window)

 Case_gen.fetch_images(window.encodeURIComponent(case_obj.image_url)).then(function (new_url) {

 var profile = Case_gen.get_default_profile(case_obj)

 Case_gen.create_profile(profile).then(function (profile_id) {
 var posts = []

 $scope.items.map(function (item) {
 delete item.profile
 item.profile_id = profile_id
 if (new_url)
 item.logo_url = new_url
 posts.push(Cart.post_to_server(item))
 })

 $q.all(posts).then(function (results) {
 console.log('posted results', results)
 // everything has been posted to cart at this point
 $rootScope.$broadcast(AUTH_EVENTS.cartUpdated);
 $location.path('/shopping_cart')
 })

 })

 })
 }
 }
 }

 // fw assumes mark always exist

 function get_type(case_o) {
 if (case_o.image_url)
 return 'wl'
 else
 return 'w'; // 'w', 'l', 'wl'   word only/logo/word and logo
 }

 }
 */
/*
 // ROUTE RESOLVE

 // from browser
 function get_cached_case_obj($window) {
 var ret
 if ($window.sessionStorage && $window.sessionStorage.case_obj)
 ret = JSON.parse($window.sessionStorage.case_obj)
 return ret
 }

 function delete_cached_case_obj($window) {
 if ($window.sessionStorage.case_obj)
 delete $window.sessionStorage.case_obj
 }

 */

// case_obj from server
function get_case_obj($q, $route, Case_gen) {
    var deferred = $q.defer();
    if ($route.current.params.id) {
        var obj = Case_gen.get_case_obj($route.current.params.id);
        deferred.resolve(obj);
    } else deferred.reject({ msg: 'no case no provided' });
    return deferred.promise;
}

// resolve for case1

exports.b4case_gen = function ($q, $route, $location, $window, Case_gen) {
    var case_code = $route.current.params.id;
    var new_case_code = get_case_code(case_code); // let's normaize the case code if possible
    if (new_case_code !== case_code) // normlized one is different, do a redirect
        $location.path('/case/' + new_case_code);else {
        return get_case_obj($q, $route, Case_gen); // make the case_obj available to controller
    }
};

function get_case_code(old_code) {
    var temp = old_code.toUpperCase();
    if (temp.length === 7 && temp.split('-').length - 1 == 0) // M1234AA? let's normalize it
        return temp.substr(0, 1) + '-' + temp.substr(1, 4) + '-' + temp.substr(5, 2);else return temp;
}

/*

 // resolve for case2
 exports.b4case_gen2 = function ($q, $route, $location, $window, Case_gen) {
 if ($window.sessionStorage.case_obj)
 return get_case_obj($q, $route, Case_gen)
 else
 $location.path('/case/' + $route.current.params.id)
 }
 */

},{"countries":"/home/wang/projects/tks_project/tks/node_modules/countries/countries.js"}],"/home/wang/projects/tks_project/tks/app/js/web/web_controllers_check_out.js":[function(require,module,exports){
"use strict";

// this returns info about the non log in user, emails returns existing email

exports.b4check_out_initial_check = function (Cart) {
  return Cart.getExistingEmails(); // return a promise for 'resolve' in ng-route
};

// if signin, it redirects to 'check_out_stripe'
// else check if emails of profile exist, if yes, prompt for login
// else prompt for a password to do a quick sign up

exports.b4check_out = function ($scope, $routeParams, $location, $rootScope, authService, Cart, valid_emails, AUTH_EVENTS) {

  console.log('b4data', valid_emails);

  if (authService.isSignedIn()) {
    $location.path('/check_out_stripe/' + $routeParams.cart_id);
  } else {
    $scope.existing_emails = valid_emails.length > 0;
    if (valid_emails.length > 0) $scope.user = { email: valid_emails[0] }; // set user email for login
    else {
        // let's obtain the first profile as default to sign up
        $scope.profile = Cart.getDefaultLocalProfile();
        console.log('default profile', $scope.profile);
        $scope.user = {
          email: $scope.profile.email,
          name: $scope.profile.profile_name,
          profile: $scope.profile
        };
      }
  }

  $scope.authenticate = function (user) {

    authService.signIn(user).then(function (status) {
      if (status.authenticated === true) {

        Cart.store_to_server().then(function (status) {
          console.log('store to server status', status);
          Cart.clean_local_store();

          $rootScope.$broadcast(AUTH_EVENTS.cartUpdated);

          $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
          Cart.getRemoteCartCount().then(function (count) {
            if (count > 0) Cart.getRemoteCart().then(function (data) {
              $location.path('/check_out_stripe/' + data.cart._id);
            });else $location.path('/dashboard');
          });
        });
      } else {
        $scope.alerts = [];
        $scope.alerts.push({ type: 'warning', msg: status.message });
      }
    });
  };

  $scope.register2 = function (user) {
    $scope.alerts = [];
    console.log('reg2');
    console.log(user);

    authService.signUp(user).then(function (status) {
      if (status.registered) {
        $scope.authenticate({ email: user.email, password: user.password });
      } else {
        $scope.alerts = [];
        $scope.alerts.push({ type: 'warning', msg: status.message });
      }
    });
  };
};

// not in use

exports.b4check_out_non_member = function ($scope, $routeParams, $location, $rootScope, authService, Cart, AUTH_EVENTS) {
  console.log('non member');

  $scope.alerts = [];

  $scope.closeAlert = function (index) {
    $scope.alerts.splice(index, 1);
  };

  Cart.getRemoteCart().then(function (items) {
    console.log(items);
    $scope.data = items;
  });

  $scope.register2 = function (user) {
    $scope.alerts = [];
    console.log('reg2');
    console.log(user);

    authService.signUp(user).then(function (status) {
      if (status.registered) {
        $scope.authenticate({ email: user.email, password: user.password });
      } else {
        $scope.alerts = [];
        $scope.alerts.push({ type: 'warning', msg: status.message });
      }
    });
  };

  $scope.authenticate = function (user) {

    authService.signIn(user).then(function (status) {
      if (status.authenticated === true) {

        Cart.store_to_server().then(function (status) {
          console.log('store to server status', status);
          Cart.clean_local_store();

          $rootScope.$broadcast(AUTH_EVENTS.cartUpdated);

          $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
          Cart.getRemoteCartCount().then(function (count) {
            if (count > 0) Cart.getRemoteCart().then(function (data) {
              $location.path('/check_out_stripe/' + data.cart._id);
            });else $location.path('/dashboard');
          });
        });
      } else {
        $scope.alerts = [];
        $scope.alerts.push({ type: 'warning', msg: status.message });
      }
    });
  };
};

},{}],"/home/wang/projects/tks_project/tks/app/js/web/web_controllers_eu_cookies.js":[function(require,module,exports){
"use strict";

var countries = require('countries');

var europe_countries = countries.get_countries_by_contnent("EU").map(function (item) {
    return item.code;
});

exports.eu_cookies = function ($http, $routeParams, $location, $route, $locale, $window, Cards, AUTH_EVENTS) {
    var _this = this;

    this.msg = 'testing EU COOKIE';

    this.show_cookies_reminder = false; // initial false

    // let url = '/api2/get_geo?test_ip=62.210.71.225'
    //  let url = '/api2/get_geo?test_ip=45.74.4.102'
    var url = '/api2/get_geo';

    console.log('checking cookies');

    // 123.123.123.123 cn
    // 62.210.71.225 fr

    $http.get(url).success(function (geo) {
        console.log('geo', geo);
        if (geo.country) {

            console.log('checking');
            console.log(europe_countries.indexOf(geo.country));
            _this.show_cookies_reminder = europe_countries.indexOf(geo.country) != -1;
            console.log(_this);
        }
    });
};

},{"countries":"/home/wang/projects/tks_project/tks/node_modules/countries/countries.js"}],"/home/wang/projects/tks_project/tks/app/js/web/web_controllers_stripe.js":[function(require,module,exports){
"use strict";

var countries = require('countries');

exports.stripe = function ($scope, $http, $routeParams, $location, $rootScope, $locale, $window, Cards, AUTH_EVENTS) {

  $scope.currentYear = new Date().getFullYear();
  $scope.currentMonth = new Date().getMonth() + 1;
  $scope.months = $locale.DATETIME_FORMATS.MONTH;
  $scope.countries = countries.get_countries();

  $scope.formData = {};

  Cards.get_user_info($routeParams.cart_id).then(function (info) {

    console.log('info', info);
    if (info.x_state) info.x_state = countries.get_state_name(info.x_state);
    $scope.formData = info; // see above for data returned
  });

  $http.get("/api/get_cart_amount/" + $routeParams.cart_id).success(function (data) {

    console.log('total amt', data);
    $scope.cart_total = data.total;
  });

  $http.get("/api_payment/get_stripe_public_key").success(function (data) {
    $scope.stripe_pk = data.stripe_pk;
  });

  $scope.$watch('formData.exp_month', function (newVal, oldVal) {
    $scope.formData.x_exp_mon2 = parseInt(newVal);
  });

  $scope.$watch('formData.exp_year', function (newVal, oldVal) {
    $scope.formData.x_exp_year2 = parseInt(newVal);
  });

  $scope.processForm = function (status, response) {

    if (response.hasOwnProperty("error")) {
      $scope.alerts = [];
      $scope.alerts.push({ type: 'warning', msg: response.error.message });
    } else {

      $scope.msg = 'Contacting Payment Gateway, Please wait...';

      $http.post("/api_payment/check_out_with_stripe/" + $routeParams.cart_id, response).success(function (data) {
        console.log(data);
        if (data.ok) {
          $rootScope.$broadcast(AUTH_EVENTS.cartUpdated);
          $location.path('/post_payment2/' + data.order_key);
        } else {
          $scope.msg = null;
          $scope.alerts = [];
          $scope.alerts.push({ type: 'warning', msg: data.err.message });
        }
      });
    }
  };
};

},{"countries":"/home/wang/projects/tks_project/tks/node_modules/countries/countries.js"}],"/home/wang/projects/tks_project/tks/app/js/web/web_controllers_trademarks.js":[function(require,module,exports){
// @ts-check
"use strict";
/*global angular, console */
// this is defined as angular module, so should be included in web_app.js

var countries = require('countries');
var organizations = require("../../../data/organizations.json");
var oapi = require("../../../data/oapi.json");
var aripo = require("../../../data/aripo.json");
var eu = require("../../../data/eu.json");

angular.module('tks.reg.controllers', [])

// trademark start here, either goes to a 'study' or 'register'

.controller('trademark', ['$scope', '$routeParams', '$location', '$translate', 'register_trademark', 'country_info', 'page_info', function ($scope, $routeParams, $location, $translate, register_trademark, country_info, page_info) {

    console.log('info123', country_info);

    var seo_string = country_info.country.name + " Trademark Registration, Register Trademark " + country_info.country.name;

    page_info.setTitle(seo_string);
    page_info.set_keywords(seo_string);
    page_info.set_description(seo_string);

    var country_name = countries.get_country($routeParams.country).name;

    $scope.country_info = country_info;
    $scope.country_info.study_disabled = 'disabled';

    console.log('show me country_info', country_info);

    if (oapi[$routeParams.country] === undefined) {

        $scope.country = $routeParams.country;
        $scope.ready = register_trademark.isImplemented($scope.country);

        if (organizations[$scope.country]) $scope.others = organizations[$scope.country][0];else {
            // not an organization, check if the country has related organization
            if (aripo[$routeParams.country] !== undefined) $scope.others = organizations.ARIPO[1].replace(/_country_/g, country_name);else if (eu[$routeParams.country] !== undefined) $scope.others = organizations.EU[1].replace(/_country_/g, country_name);
        }
    } else {
        $location.path('/trademark/OAPI');
    }

    //  $scope.study_disabled = country_info.study_available ? "": "disabled"

    console.log('study_disable', $scope.study_disabled);
    $scope.status = "ready";

    $scope.locale = function () {
        console.log('asdfasdf', $translate.use());
        return $translate.use();
    };
}]).controller('trademarkSEO', ["$scope", "$routeParams", "$location", "$translate", "register_trademark", "country_info", "page_info", function ($scope, $routeParams, $location, $translate, register_trademark, country_info, page_info) {

    console.log('trademarkSEO', $location.path());
    console.dir('trademarkSEO', $routeParams);
    console.log('country info', country_info);

    // var seo_string = country_info.country.name + " Trademark Registration, Register Trademark " + country_info.country.name

    var seo_string = "Trademark Registration in " + country_info.country.name + " - TradeMarkers®.com";

    console.log('seo_string', seo_string);
    page_info.setTitle(seo_string);
    page_info.set_by_country_code(country_info.country.code);
    console.log('con info ', country_info);

    /*
    page_info.set_keywords(seo_string)
    page_info.set_description(seo_string)
    */

    var country_name = country_info.country.name;
    var country_code = country_info.country.code;

    $scope.country_info = country_info;
    $scope.study_disabled = country_info.study_available ? "" : "disabled";
    $scope.trademark_disabled = country_info.trademark_available ? "" : "disabled";

    $scope.locale = function () {

        console.log('asdfasdf', $translate.use());
        return $translate.use();
    };

    if (oapi[country_code] === undefined) {

        $scope.country = country_code;
        $scope.ready = register_trademark.isImplemented($scope.country);

        if (organizations[$scope.country]) $scope.others = organizations[$scope.country][0];else {
            // not an organization, check if the country has related organization
            if (aripo[$scope.country] !== undefined) $scope.others = organizations.ARIPO[1].replace(/_country_/g, country_name);else if (eu[$scope.country] !== undefined) $scope.others = organizations.EU[1].replace(/_country_/g, country_name);
        }
    } else {
        $location.path('/trademark/OAPI');
    }
}])

/*
 .controller('trademarkSEO', ['$scope', '$routeParams', '$location', 'register_trademark', 'country_info', 'page_info',
 function ($scope, $routeParams, $location, register_trademark, country_info, page_info) {
  console.log('trademarkSEO', $location.path())
 console.dir('trademarkSEO', $routeParams)
 console.log('country info', country_info)
  var seo_string = country_info.country.name + " Trademark Registration, Register Trademark " + country_info.country.name
  console.log('seo_string', seo_string)
 page_info.setTitle(seo_string)
 page_info.set_keywords(seo_string)
 page_info.set_description(seo_string)
   var country_name = country_info.country.name
 var country_code = country_info.country.code
  $scope.country_info = country_info
 $scope.study_disabled = country_info.study_available ? "": "disabled"
 $scope.trademark_disabled = country_info.trademark_available ? "": "disabled"
   if (oapi[country_code] === undefined) {
  $scope.country = country_code
 $scope.ready = register_trademark.isImplemented($scope.country)
  if (organizations[$scope.country])
 $scope.others = organizations[$scope.country][0]
 else {
 // not an organization, check if the country has related organization
 if (aripo[$scope.country] !== undefined)
 $scope.others = organizations.ARIPO[1].replace(/_country_/g, country_name)
 else if (eu[$scope.country] !== undefined)
 $scope.others = organizations.EU[1].replace(/_country_/g, country_name)
 }
  } else {
 $location.path('/trademark/OAPI')
 }
 }
 ])
  */

// starting of registration process
.controller('register1', ['$scope', '$http', '$location', '$routeParams', 'register_trademark', 'country_info', function ($scope, $http, $location, $routeParams, register_trademark, country_info) {

    $scope.country = $routeParams.country;
    $scope.country_info = country_info;

    $scope.start_registration = function (country) {
        console.log('starting');
        //        if (authService.isSignedIn()) {
        register_trademark.set_input_format(country);
        register_trademark.init_item('registration'); // line buffer got initialized only here to allow 'back' to work
        $location.path('/register2/' + country);
    };
}])

// get type of registration, word mark, photos, classes
// this controller shared by register2.html and study2.html

.controller('register2', ['$scope', '$http', '$location', '$rootScope', '$routeParams', '$modal', 'countries', 'register_trademark', 'STATIC_INFO', 'registry_initData', 'country_info', function ($scope, $http, $location, $rootScope, $routeParams, $modal, countries, register_trademark, STATIC_INFO, registry_initData, country_info) {

    $scope.countries = countries;

    $scope.classes_selected = [];
    $scope.CLASS_HELP = STATIC_INFO.class_help;

    // generate NICE classes block for selection
    for (var x = 1; x <= 45; x++) {
        var c = x.toString();
        if (c.length === 1) c = '0' + c;
        $scope.classes_selected.push({
            code: c,
            selected: false
        });
    }

    // assign to classes_selected for data entry
    var classes2array = function classes2array(classes) {

        for (var k = 0; k < $scope.classes_selected.length; k++) $scope.classes_selected[k].selected = false;

        for (var i = 0; i < classes.length; i++) {
            var indx = parseInt(classes[i].name);
            $scope.classes_selected[indx - 1].selected = true;
        }
    };
    /*
     var temp = register_trademark.get()
     if (temp)
     classes2array(temp.classes)
     */

    $scope.country = $routeParams.country;
    $scope.country_info = country_info;

    $scope.input_format = registry_initData.input_format;
    $scope.reg = registry_initData.line;

    if ($scope.reg.type === '') {
        $scope.reg.type = $scope.input_format.trade_type;
        console.log('redo');
    }

    if ($scope.reg) classes2array($scope.reg.classes);

    var chk_reg = function chk_reg(reg, cb) {

        var required_logo = function required_logo(tm_type) {
            var logo_types = ['l', 'wl', 'tml'];
            return logo_types.indexOf(tm_type) !== -1;
        };

        var err = null;
        if (reg.type !== 'l') {
            if (reg.wordmark === '') err = 'Wordmark should not be blank';
        }
        if (!err) {
            // check for logo

            console.log('checking type reg.type', reg.type);
            //if (reg.type !== 'w') || reg.type // if not word only, then check logo
            if (required_logo(reg.type)) {
                if (!reg.logo_url) err = 'Please provide a logo file';
            }
        }

        if (!err) {
            if (reg.classes.length === 0) err = 'You should choose at least one class';else {
                console.log('hi');
                /*
                 for (var i = 0; i < reg.classes.length; i++) {
                  if (!reg.classes[i].details) {
                 err = 'class ' + reg.classes[i].name + ' should have a description'
                 break
                 }
                 }*/
            }
        }

        if (err) cb(new Error(err));else cb(null);
    };

    $scope.submit = function (reg) {

        chk_reg(reg, function (err) {
            if (err) {
                $scope.alerts = [];
                $scope.alerts.push({
                    type: 'warning',
                    msg: err.message
                });
            } else {
                reg.country = $routeParams.country;
                reg.amount = register_trademark.getAmount(reg.country);
                register_trademark.save(reg);
                $location.path('/register3/' + reg.country);
            }
        });
    };

    $scope.submit_study = function (reg) {

        chk_reg(reg, function (err) {
            if (err) {
                $scope.alerts = [];
                $scope.alerts.push({
                    type: 'warning',
                    msg: err.message
                });
            } else {
                reg.country = $routeParams.country;
                reg.amount = register_trademark.getAmount(reg.country);
                register_trademark.save(reg);
                //            $location.path('/study3/' + reg.country)
                $location.path('/trademark_contact/' + $scope.country);
            }
        });
    };

    $scope.study_type_change = function (value) {
        console.log('study type', value);
    };

    $scope.getAmount = function () {
        return register_trademark.getAmount($scope.country);
    };

    $scope.upload_result = function (content) {
        console.log('upload_result');
        console.log(content);
        $scope.upload_status = content;
        //    refresh_video($http, $scope, $routeParams.code)
    };

    //http://stackoverflow.com/questions/17216806/angularjs-uploading-an-image-with-ng-upload

    $scope.file_size = 100;
    //      $scope.upload_status

    $scope.uploadFile = function (files) {

        console.log(typeof files[0].type, files[0].type, files[0].type.substring(0, 5), 'image' === files[0].type.substring(0, 5));

        if ('image' === files[0].type.substring(0, 5)) {

            var fd = new FormData();

            //Take the first selected file
            fd.append("logo_file", files[0]);

            $scope.upload_status = 'uploading...';
            $http.post('/upload_file', fd, {
                withCredentials: true,
                headers: {
                    'Content-Type': undefined
                },
                transformRequest: angular.identity
            }).success(function (data) {
                $scope.reg.logo_url = data.url;
                $scope.upload_status = 'Uploaded';
                console.log('success', data);
            }).error(function (data) {
                console.log('failed', data);
            });
        } else {
            alert(files[0].name + ' is not an image file');
        }
    };

    $scope.class_change = function (s) {

        if (s.selected) {

            if (!$scope.reg.classes.some(function (element, index, array) {
                // if not yet selected
                return element.name === s.code;
            })) {
                $scope.reg.classes.push({
                    name: s.code,
                    details: ''
                });
            }
        } else {
            for (var i = 0; i < $scope.reg.classes.length; i++) {
                if ($scope.reg.classes[i].name === s.code) $scope.reg.classes.splice(i, 1);
            }
        }
    };

    // open a modal dialog to search classes from NICE website

    $scope.open = function () {

        var modalInstance = $modal.open({
            templateUrl: 'partials/search_class_modal.html',
            controller: 'ModalInstanceCtrl',
            resolve: {
                items: function items() {
                    return $scope.items;
                }
            }
        });

        modalInstance.result.then(function (selectedClasses) {
            if (selectedClasses.length > 0) {

                console.log('selected classes', selectedClasses);
                $scope.reg.classes = [];
                for (var i = 0; i < selectedClasses.length; i++) {
                    console.log(selectedClasses[i]);
                    var temp = selectedClasses[i].basic_desc + ' (' + selectedClasses[i].basic_number + ')';
                    $scope.reg.classes.push({
                        name: selectedClasses[i].code,
                        details: temp
                    });
                }
                classes2array($scope.reg.classes);
            }
            // $scope.selected = selectedItem;
        }, function () {
            //          $log.info('Modal dismissed at: ' + new Date());
        });
    };

    //    }
}])

// get POA and commerce use
.controller('register3', ['$scope', '$http', '$location', '$rootScope', '$routeParams', '$modal', 'countries', 'register_trademark', 'POA_INFO', 'STATIC_INFO', 'authService', function ($scope, $http, $location, $rootScope, $routeParams, $modal, countries, register_trademark, POA_INFO, STATIC_INFO, authService) {

    console.log(POA_INFO);

    console.log('reg3');

    $scope.countries = countries;

    $scope.country = $routeParams.country;

    $scope.reg = register_trademark.get();

    $scope.POA_INFO = POA_INFO[$scope.country]; // could be 'undefined' if the country does not need POA
    $scope.POA_HELP = POA_INFO.HELP[$scope.country];
    $scope.CLAIM_HELP = STATIC_INFO.priority_help;
    $scope.COMMERCE_USE_HELP = STATIC_INFO.commerce_use_help;
    $scope.POA_MUST = POA_INFO.POA_MUST;

    console.log($scope.reg);
    console.log('t', typeof $scope.reg.POA);

    $scope.submit = function (reg) {
        reg.country = $routeParams.country;
        register_trademark.save(reg);

        $location.path('/trademark_contact/' + $scope.country);

        /*
         if (authService.isSignedIn())
         $location.path('/register4/' + $scope.country)
         else
         $location.path('/register5/' + $scope.country)
         */
    };

    $scope.back = function (reg) {
        register_trademark.save(reg);
        $location.path('/register2/' + reg.country);
    };

    $scope.newValue = function (value) {
        if (!value) $scope.reg.claim_priority = false;
    };

    $scope.getAmount = function () {
        return register_trademark.getAmount($scope.country);
    };
}])

// get profile

.controller('register4', ['$scope', '$http', '$location', '$rootScope', '$routeParams', '$modal', 'countries', 'register_trademark', 'DBStore', function ($scope, $http, $location, $rootScope, $routeParams, $modal, countries, register_trademark, DBStore) {

    $scope.alerts = [];

    $http.get('/api/profiles/').success(function (data) {
        $scope.profiles = data;
    });

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.country = $routeParams.country;
    $scope.reg = register_trademark.get();

    $scope.new_profile = function () {
        console.log($location);
        console.log($location.path());
        var uuid = DBStore.add($location.path());
        $location.path('/profile/new/' + uuid);
    };

    $scope.submit = function (reg) {
        reg.country = $routeParams.country;

        register_trademark.save(reg);
        console.log(reg);
        $scope.profile_name = reg.profile_id;
        $location.path('/register5/' + $scope.country);
    };

    $scope.back = function (reg) {
        register_trademark.save(reg);
        $location.path('/register3/' + reg.country);
    };
}])

// final confirmation

.controller('register5', ['$scope', '$http', '$location', '$rootScope', '$routeParams', 'register_trademark', 'Cart', 'AUTH_EVENTS', 'registry_initData', 'authService', function ($scope, $http, $location, $rootScope, $routeParams, register_trademark, Cart, AUTH_EVENTS, registry_initData, authService) {

    console.log('reg5 init data', registry_initData);

    $scope.country = $routeParams.country;

    $scope.reg = registry_initData.line;
    if (registry_initData.profile) $scope.profile = registry_initData.profile;else $scope.profile = $scope.reg.profile; // no profile_id, use embedded profile

    console.log('reg5', $scope.reg);

    $scope.back = function (reg) {
        reg.country = $routeParams.country;
        if (reg.profile_id) $location.path('/trademark_contact/' + $routeParams.country);else $location.path('/trademark_address/' + $routeParams.country);
    };

    $scope.submit = function (reg) {
        console.log('reg', reg);
        register_trademark.save(reg);
        Cart.post_to_server(register_trademark.get()).then(function (status) {
            console.log('cart post to server status');
            $rootScope.$broadcast(AUTH_EVENTS.cartUpdated);
            Cart.getExistingEmails().then(function (emails) {
                if (emails.length > 0) {
                    $location.path('/before_check_out/' + Cart.getRemoteCart()._id);
                } else $location.path('/shopping_cart');
            });
        });
    };
}]).controller('trademark_contact', ['$scope', '$http', '$location', '$rootScope', '$routeParams', '$modal', 'countries', 'register_trademark', 'registry_initData', function ($scope, $http, $location, $rootScope, $routeParams, $modal, countries, register_trademark, registry_initData) {

    $scope.reg = registry_initData.line;

    $scope.country = $routeParams.country;
    $scope.reg = register_trademark.get();

    console.log('init', registry_initData);
    $scope.profiles = registry_initData.profiles;

    $scope.next = function (reg) {

        var err_msg;

        if (!reg.profile.profile_name) err_msg = 'Please provide a name for this profile';
        if (!err_msg && !reg.profile.contact) err_msg = 'Please provide a contact person';
        if (!err_msg && !reg.profile.email) err_msg = 'Please provide a valid email address so we can contact you';

        if (err_msg) $scope.alerts = [{ type: 'warning', msg: err_msg }];else {
            delete reg.profile_id;
            register_trademark.save(reg);
            console.log('reg0', reg);
            $location.path('/trademark_address/' + $routeParams.country);
        }
    };

    $scope.back = function (reg) {
        register_trademark.save(reg);
        console.table(reg);
        var back_to = reg.service === 'study' ? '/study2/' : '/register3/';
        $location.path(back_to + $routeParams.country);
    };

    $scope.use_existing_profile = function (profile_id, reg) {
        console.log('use_existing', reg);
        console.log('profile_id', profile_id);
        $scope.reg.profile_id = profile_id;
        register_trademark.save(reg);
        console.log('profile -id new', register_trademark.get());
        var next_to = $scope.reg.service === 'study' ? '/study3/' : '/register5/';
        $location.path(next_to + $routeParams.country);
    };
}]).controller('trademark_address', ['$scope', '$http', '$location', '$route', '$window', 'registry_initData', function ($scope, $http, $location, $route, $window, registry_initData) {

    $scope.profile = registry_initData.profile;
    $scope.side_panel_account = registry_initData.side_panel_account;

    console.log('init', registry_initData);
    console.log('init2', JSON.stringify(registry_initData));

    $scope.countries = countries.get_countries();
    if (registry_initData.next_prompt) $scope.next_prompt = registry_initData.next_prompt;else $scope.next_prompt = 'Next';

    if ($window.sessionStorage.new_country) {
        $scope.profile.country = $window.sessionStorage.new_country;
        delete $window.sessionStorage.new_country;
    }

    if (!$scope.profile.country) {
        // let's determine
        //        $http.get('/api2/get_geo?test_ip=174.54.165.206').success(function (geo) {
        //        $http.get('/api2/get_geo?test_ip=112.210.15.107').success(function (geo) {
        $http.get('/api2/get_geo').success(function (geo) {
            console.log('geo', geo);
            if (geo.country) {
                $scope.profile.country = geo.country;
                $scope.profile.state = geo.region;
                $scope.profile.city = geo.city;
            }
        });
    }

    $scope.items = [{ name: 'Canada', code: 'CA' }, { name: 'China', code: 'CN' }, { name: 'Hong Kong', code: 'HK' }, { name: 'Philippines', code: 'PH' }, { name: 'Singapore', code: 'SG' }, { name: 'Thailand', code: 'TH' }, { name: 'United States', code: 'US' }];

    $scope.setCountry = function (code) {
        console.log('dd', code);
        if (code !== 'ALL') $scope.profile.country = code;else $scope.showC = true;
    };

    $scope.status = {
        isopen: false
    };

    $scope.toggled = function (open) {
        console.log('Dropdown is now: ', open);
    };

    $scope.$watch('profile.country', function (newVal, oldVal) {
        console.log('changed', newVal, oldVal);

        if (newVal === oldVal) {
            console.log('no change');
            return;
        }
        //  register_trademark.save_profile($scope.profile)
        $window.sessionStorage.new_country = newVal;
        $route.reload();
    });

    $scope.next = function (profile) {

        var err_msg;

        if (!profile.address) err_msg = 'Address line should not be blank';
        if (!err_msg && !profile.country) err_msg = 'Please select a country by clicking the link at lower right of this form';
        if (!err_msg && !profile.city) {
            var cn = profile.country;
            console.log(cn, cn !== 'HK', cn !== 'SG', cn !== 'HK' || cn !== 'SG');
            if (!(profile.country === 'HK' || profile.country === 'SG')) {
                err_msg = 'City should not be blank';
            }
        }
        if (!err_msg && !profile.state) err_msg = 'State should not be blank';

        if (!err_msg && !profile.phone) err_msg = 'Phone should not be blank';

        if (err_msg) $scope.alerts = [{ type: 'warning', msg: err_msg }];else {

            if (registry_initData.next_proc) {
                registry_initData.next_proc(profile);
            } else $location.path(registry_initData.next_path);
        }
    };

    $scope.back = function (profile) {
        $location.path(registry_initData.back_path);
    };
}]);

},{"../../../data/aripo.json":"/home/wang/projects/tks_project/tks/data/aripo.json","../../../data/eu.json":"/home/wang/projects/tks_project/tks/data/eu.json","../../../data/oapi.json":"/home/wang/projects/tks_project/tks/data/oapi.json","../../../data/organizations.json":"/home/wang/projects/tks_project/tks/data/organizations.json","countries":"/home/wang/projects/tks_project/tks/node_modules/countries/countries.js"}],"/home/wang/projects/tks_project/tks/app/js/web/web_menu.js":[function(require,module,exports){
// @ts-check
"use strict";

/*global angular */

var countries = require('countries');
var utilities = require('../modules/utilities.js');

function get_countries_by_continent(cont_code) {
  var temp = utilities.supportedCountries();

  return countries.get_countries_by_contnent(cont_code).filter(function (value) {
    return temp[value.code] !== undefined;
  });
}

function append_one_word_country_name(lst) {
  var rlst = [];
  for (var i = 0; i < lst.length; i++) {
    lst[i].simple_name = lst[i].name.replace(/ /g, '_').toLowerCase();
    rlst.push(lst[i]);
  }
  return rlst;
}

angular.module('tks.menu', []).factory("TM_MENU", [function () {

  return {

    main_menu: countries.get_countries(['EU', 'US', 'CA', 'RU', 'CH', 'NO', 'SG', 'HK', 'CN', 'TW', 'TH', 'PH', 'KR', 'JP', 'VN', 'IN', 'AR', 'MX', 'BR', 'AU', 'AZ', 'BH', 'BY', 'BZ', 'BO', 'CO']),

    major_countries: append_one_word_country_name(countries.get_countries(['US', 'EU', 'CA', 'CN', 'SG', 'HK', 'TW', 'PH'])),
    asia: get_countries_by_continent('AS'),
    europe: get_countries_by_continent('EU'),
    n_america: get_countries_by_continent('NA'),
    s_america: get_countries_by_continent('SA'),
    c_america: get_countries_by_continent('CA'),
    africa: get_countries_by_continent('AF'),
    //    middle_east: countries.get_countries(['BH', 'CA']),
    middle_east: get_countries_by_continent('ME'),
    oceania: get_countries_by_continent('OC'),

    rest_menu: [{ name: 'Afghanistan', code: 'AF' }, { name: 'Åland Islands', code: 'AX' }, { name: 'Albania', code: 'AL' }]

  };
}]);

},{"../modules/utilities.js":"/home/wang/projects/tks_project/tks/app/js/modules/utilities.js","countries":"/home/wang/projects/tks_project/tks/node_modules/countries/countries.js"}],"/home/wang/projects/tks_project/tks/app/js/web/web_monitor.js":[function(require,module,exports){
"use strict";
/*global angular, console */

angular.module('tks.monitor.controllers', [])

// starting of monitor process

.controller('monitor1', ["$scope", "$http", "$location", "$routeParams", function ($scope, $http, $location, $routeParams) {

    $scope.country = $routeParams.country;
    $scope.start_monitor = function (country) {
        $location.path('/monitor2/' + country);
    };
}]).controller('monitor2', ["$scope", "$http", "$location", "$routeParams", function ($scope, $http, $location, $routeParams) {

    $scope.reg = {
        type: 'w',
        classes: [{ "name": "42", "details": "class42" }, { "name": "01", "details": "class01" }, {
            "name": "15",
            "details": "class15"
        }]
    };

    $scope.country = $routeParams.country;
    $scope.start_monitor = function (country) {
        $location.path('/monitor2/' + country);
    };
}]);

},{}],"/home/wang/projects/tks_project/tks/app/js/web/web_routes.js":[function(require,module,exports){
// @ts-check

"use strict";

var transaction_procs = require('./web_services_transaction_procs.js');
var utilities = require('../modules/utilities.js');

var loginReq = {
    loginRequired: ['$location', '$q', 'authService', function ($location, $q, authService) {
        var deferred = $q.defer();

        if (!authService.isSignedIn()) {
            deferred.reject();
            $location.path('/signin');
        } else deferred.resolve();

        return deferred.promise;
    }]
};

var get_country_info = ['$route', '$http', '$q', '$translate', transaction_procs.get_country_info];
var get_country_info_SEO = ['$route', '$http', '$q', '$translate', transaction_procs.get_country_info_SEO];

var pre_attorneys = {
    bio_data: ['$route', 'attorneysService', function ($route, attorneysService) {
        //        console.log('atty ', $route.current.params.name)
        return attorneysService.get($route.current.params.name);
    }]
};

var pre_registry2 = {
    registry_initData: ['$route', '$q', 'register_trademark', function ($route, $q, register_trademark) {
        return transaction_procs.b4registry2($route, $q, register_trademark);
    }],
    country_info: get_country_info
};

var pre_registry5 = {
    registry_initData: ['$route', '$q', 'register_trademark', 'Cart', function ($route, $q, register_trademark, Cart) {
        return transaction_procs.b4registry5($route, $q, register_trademark, Cart);
    }]
};

var pre_trademark_contact = {
    registry_initData: ['$route', '$q', 'register_trademark', 'Cart', function ($route, $q, register_trademark, Cart) {
        return transaction_procs.b4trademark_contact($route, $q, register_trademark, Cart);
    }]
};

var pre_trademark_address = {
    registry_initData: ['$route', '$q', 'register_trademark', 'Cart', function ($route, $q, register_trademark, Cart) {
        return transaction_procs.b4trademark_address($route, $q, register_trademark, Cart);
    }]
};

var pre_account_address = {
    registry_initData: ['$route', '$q', '$location', 'user_account', function ($route, $q, $location, user_account) {
        return transaction_procs.b4account_address($route, $q, $location, user_account);
    }]
};

var pre_profile_address = {
    registry_initData: ['$route', '$q', '$location', 'user_profile', function ($route, $q, $location, user_profile) {
        return transaction_procs.b4profile_address($route, $q, $location, user_profile);
    }]
};

var pre_profile = {
    registry_initData: ['$route', '$q', '$location', 'user_profile', function ($route, $q, $location, user_profile) {
        return transaction_procs.b4profile($route, $q, $location, user_profile);
    }]
};

var pre_before_checkout = {
    valid_emails: ['Cart', function (Cart) {
        return require('./web_controllers_check_out').b4check_out_initial_check(Cart);
    }]
};

var pre_case_gen = {
    case_obj: ['$q', '$route', '$location', '$window', 'Case_gen', function ($q, $route, $location, $window, Case_gen) {
        return require('./web_controllers_case').b4case_gen($q, $route, $location, $window, Case_gen);
    }]

};

var pre_case_gen2 = {
    case_obj: ['$q', '$route', '$location', '$window', 'Case_gen', function ($q, $route, $location, $window, Case_gen) {
        return require('./web_controllers_case').b4case_gen2($q, $route, $location, $window, Case_gen);
    }]
};

module.exports = ['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $routeProvider.when('/', { templateUrl: 'partials/home.html', controller: 'homeCtrl' }).when('/whyus', { templateUrl: 'partials/whyus.html', controller: 'homeCtrl' }).when('/mylogs', { templateUrl: 'partials/mylogs.html', controller: 'mylogsCtrl' }).when('/reg_all', { templateUrl: 'partials/reg_all.html', controller: 'reg_allCtrl' }).when('/404/:key?', { templateUrl: 'partials/404.html', controller: '404Ctrl' }).when('/region/:code', { templateUrl: '../../partials/region.html', controller: 'regionCtrl' }).when('/countries', { templateUrl: '../../partials/countries.html', controller: 'regionCtrl' }).when('/trademark/:country', {
        templateUrl: '../../partials/trademark.html', controller: 'trademark', resolve: {
            country_info: get_country_info
        }
    }).

    //    when('/test_stripe', {templateUrl: '../../partials/test_stripe.html', controller: 'IndexController'}).

    // registration of trademark 5 steps
    when('/register1/:country', {
        templateUrl: '../../partials/register1.html', controller: 'register1', resolve: {
            country_info: get_country_info
        }
    }).when('/register2/:country', {
        templateUrl: '../../partials/register2.html', controller: 'register2',
        resolve: pre_registry2
    }).when('/register3/:country', { templateUrl: '../../partials/register3.html', controller: 'register3' }).when('/register4/:country', { templateUrl: '../../partials/register4.html', controller: 'register4' }).when('/register5/:country', {
        templateUrl: '../../partials/register5.html', controller: 'register5',
        resolve: pre_registry5
    }).when('/trademark_contact/:country', {
        templateUrl: '../../partials/trademark_contact.html',
        controller: 'trademark_contact',
        resolve: pre_trademark_contact
    }).when('/trademark_address/:country', {
        templateUrl: '../../partials/trademark_address.html',
        controller: 'trademark_address',
        resolve: pre_trademark_address
    }).

    // study of trademark ? steps
    when('/study1/:country', { templateUrl: '../../partials/study1.html', controller: 'study1' }).when('/study2/:country', {
        templateUrl: '../../partials/study2.html',
        controller: 'register2',
        resolve: pre_registry2
    }).when('/study3/:country', {
        templateUrl: '../../partials/study3.html',
        controller: 'study3',
        resolve: pre_registry5
    }).when('/monitor1/:country', { templateUrl: '../../partials/monitor1.html', controller: 'monitor1' }).when('/monitor2/:country', {
        templateUrl: '../../partials/monitor2.html',
        controller: 'monitor2',
        resolve: pre_registry2
    }).when('/prices/:country?', { templateUrl: 'partials/prices.html', controller: 'prices_controller' }).when('/shopping_cart', { templateUrl: 'partials/shopping_cart.html', controller: 'cartController' }).when('/shopping_cart_thanks/:order_key', {
        templateUrl: 'partials/shopping_cart_thanks.html',
        controller: 'cartThanksController'
    }).

    // this is only for test
    when('/profile_edit', { templateUrl: 'partials/profile_edit.html', controller: 'profile_edit' }).

    //    when('/check_out/:cart_id', {templateUrl: 'partials/check_out.html', controller: 'check_out_controller'}).
    when('/before_check_out/:cart_id', {
        templateUrl: 'partials/before_check_out.html',
        controller: 'before_check_out',
        resolve: pre_before_checkout
    }).when('/check_out_non_member', {
        templateUrl: 'partials/before_check_out_non_member.html',
        controller: 'before_check_out_non_member'
    }).when('/check_out_stripe/:cart_id', {
        templateUrl: 'partials/check_out_stripe.html',
        controller: 'check_out_stripe_controller',
        isPriv: true,
        resolve: loginReq
    }).when('/post_payment/:key', { templateUrl: 'partials/post_payment.html', controller: 'post_payment_controller' }).when('/post_payment2/:key', {
        templateUrl: 'partials/post_payment2.html',
        controller: 'post_payment2_controller'
    }).when('/orders', {
        templateUrl: 'partials/orders.html',
        controller: 'ordersController',
        isPriv: true,
        resolve: loginReq,
        reloadOnSearch: false
    }).when('/order/:id', {
        templateUrl: 'partials/order.html',
        controller: 'orderDetailController',
        isPriv: true,
        resolve: loginReq,
        reloadOnSearch: false
    }).when('/registrations', {
        templateUrl: 'partials/registrations.html', controller: 'registrationsController',
        reloadOnSearch: false, isPriv: true, resolve: loginReq
    }).when('/registration/:id', {
        templateUrl: 'partials/registration.html',
        controller: 'registrationDetailController',
        isPriv: true,
        resolve: loginReq
    }).when('/signup', { templateUrl: 'partials/signup.html', controller: 'UserController' }).when('/signin', { templateUrl: 'partials/signin.html', controller: 'UserController' }).when('/signout', { templateUrl: 'partials/comming.html', controller: 'UserController' }).when('/recoverpassword', { templateUrl: 'partials/resetpassword.html', controller: 'UserController' }).when('/case/:id', {
        templateUrl: '/partials/case.html', controller: 'caseGenCtrl',
        resolve: pre_case_gen

    }).
    /*
    when('/case2/:id', {
        templateUrl: '/partials/case2.html', controller: 'caseGenCtrl2',
        resolve: pre_case_gen2
     }). */
    when('/dashboard', {
        templateUrl: 'partials/dashboard.html',
        controller: 'dashboardCtrl',
        isPriv: true,
        resolve: loginReq
    }).when('/account', {
        templateUrl: 'partials/account.html',
        controller: 'accountController',
        isPriv: true,
        resolve: loginReq
    }).when('/account_address/:country', {
        templateUrl: '../../partials/trademark_address.html', controller: 'trademark_address',
        resolve: pre_account_address
    }).when('/profile/:id/:backlink?', {
        templateUrl: 'partials/profile.html',
        controller: 'profileController',
        resolve: pre_profile
    }).when('/profile_address/:id', {
        templateUrl: '../../partials/trademark_address.html', controller: 'trademark_address',
        resolve: pre_profile_address
    }).when('/profiles', {
        templateUrl: 'partials/profiles.html',
        controller: 'profileListController',
        isPriv: true,
        resolve: loginReq
    }).

    // static information of the site
    when('/about', { templateUrl: 'partials/about.html', controller: 'homeCtrl' }).when('/attorneys', { templateUrl: 'partials/attorneys_list.html', controller: 'homeCtrl' }).when('/attorneys/:name', {
        templateUrl: 'partials/attorneys.html',
        controller: 'attorneysCtrl as bio',
        resolve: pre_attorneys
    }).when('/terms', { templateUrl: 'partials/terms.html', controller: 'homeCtrl' }).when('/services', { templateUrl: 'partials/services.html', controller: 'homeCtrl' }).when('/cookies', { templateUrl: 'partials/privacy.html', controller: 'homeCtrl' }).when('/resources', { templateUrl: 'partials/resources.html', controller: 'homeCtrl' }).when('/careers', { templateUrl: 'partials/careers.html', controller: 'homeCtrl' }).when('/service_contract', { templateUrl: 'partials/service_contract.html', controller: 'homeCtrl' }).when('/privacy', { templateUrl: 'partials/privacy.html', controller: 'homeCtrl' }).when('/contact', { templateUrl: 'partials/contact.html', controller: 'contactCtrl' }).when('/classes', { templateUrl: 'partials/classes.html', controller: 'classesCtrl' }).when('/search_classes', { templateUrl: 'partials/search_classes.html', controller: 'search_classesController' }).when('/supported_browsers', { templateUrl: 'partials/supported_browsers.html', controller: 'signupCtrl' }).when('/es6', { templateUrl: 'partials/es6.html', controller: 'es6Ctrl' });

    //console.table(utilities.supportedCountries())

    var scountries = utilities.supportedCountries();

    console.log('scountries', scountries);
    for (var code in scountries) {
        if (true) {
            var sname = scountries[code].name.replace(/ /g, '_').toLowerCase();
            $routeProvider.when('/trademark-registration-in-' + sname, {
                templateUrl: '../../partials/trademark.html', controller: 'trademarkSEO', resolve: {
                    country_info: get_country_info_SEO
                }
            });
        }
    }

    /*
     $routeProvider.when('/trademark-registration-in-turkey', {templateUrl: '../../partials/trademark.html', controller: 'trademarkSEO', resolve: {
     country_info: get_country_info_SEO
     }}).
     */

    $routeProvider.otherwise({ redirectTo: '/' });

    $locationProvider.html5Mode(true);

    // $locationProvider.html5mode({ enabled: true, requireBase: false });
}];

},{"../modules/utilities.js":"/home/wang/projects/tks_project/tks/app/js/modules/utilities.js","./web_controllers_case":"/home/wang/projects/tks_project/tks/app/js/web/web_controllers_case.js","./web_controllers_check_out":"/home/wang/projects/tks_project/tks/app/js/web/web_controllers_check_out.js","./web_services_transaction_procs.js":"/home/wang/projects/tks_project/tks/app/js/web/web_services_transaction_procs.js"}],"/home/wang/projects/tks_project/tks/app/js/web/web_services.js":[function(require,module,exports){
"use strict";
/* global angular*/

var services_procs = require('./web_services_procs');
var cart_procs = require('./web_services_cart');
var transaction = require('./web_services_transaction');
var case_gen = require('./web_services_case');
var cart_store = require('./web_cart_store');

var services = angular.module('tks.services', []).value('version', '0.1');

services.factory('register_trademark', ['$http', '$q', transaction.register_trademark]);

services.service('Cart', ["$http", "$q", "authService", "Cart_Store", cart_procs.Cart]).service('Cart_Store', ["$window", cart_store.Cart_Store]).service('Search_classes', ["$http", "$q", services_procs.Search_classes]).service('help_info', ['$http', '$q', services_procs.help_info]).service('page_info', [services_procs.page_info]).service('Case_gen', ["$http", "$q", "register_trademark", "Cart_Store", "user_profile", "authService", case_gen.Case_gen]).factory('Registrations', ['$resource', function ($resource) {
  return $resource('/api/registrations');
}]).factory('Orders', ['$resource', function ($resource) {
  return $resource('/api/orders');
}]).service('TMCountries', ['$http', '$q', services_procs.TMCountries]).service('user_account', ['$http', '$q', services_procs.user_account]).service('user_profile', ['$http', '$q', services_procs.user_profile]).service('attorneysService', ['$http', '$q', services_procs.attorneys_service_proc]).factory('news_feeder', ['$http', '$q', services_procs.news_feeder_proc]);

},{"./web_cart_store":"/home/wang/projects/tks_project/tks/app/js/web/web_cart_store.js","./web_services_cart":"/home/wang/projects/tks_project/tks/app/js/web/web_services_cart.js","./web_services_case":"/home/wang/projects/tks_project/tks/app/js/web/web_services_case.js","./web_services_procs":"/home/wang/projects/tks_project/tks/app/js/web/web_services_procs.js","./web_services_transaction":"/home/wang/projects/tks_project/tks/app/js/web/web_services_transaction.js"}],"/home/wang/projects/tks_project/tks/app/js/web/web_services_cart.js":[function(require,module,exports){
"use strict";
/*global angular */

exports.Cart = function ($http, $q, authService, Cart_Store) {

  this.getRemoteCart = function () {
    console.log('get remote cart');
    var deferred = $q.defer();

    if (authService.isSignedIn()) {

      $http.get('/api/get_cart').success(function (data) {
        console.log('data.cart', data.cart);
        deferred.resolve({ status: 'ok', message: 'succeeded', cart: data.cart });
      }).error(function (error) {
        deferred.resolve({ status: 'err', message: 'Sorry, error occurred while accessing server, please try again.' });
      });
    } else deferred.resolve({ status: 'ok', message: 'succeeded', cart: Cart_Store.getCart() });

    return deferred.promise;
  };

  this.check_out = function (cart_id) {

    var deferred = $q.defer();

    $http.post('/api/checkout/' + cart_id, { payment: 'paypal' }).success(function (data) {
      console.log(data);
      deferred.resolve({ status: data.status, message: data.message, order_key: data.order_key });
    }).error(function (error) {
      deferred.resolve({ status: 'failed', message: 'Sorry, error occurred while posting, please try again.' });
    });

    return deferred.promise;
  };

  this.getRemoteCartCount = function () {
    var deferred = $q.defer();

    console.log('getRemoteChartCount');
    if (authService.isSignedIn()) {
      $http.get('/api/get_cart_count').success(function (data) {
        deferred.resolve(data.cart_count);
      }).error(function (error) {
        deferred.resolve({ status: 'err', message: 'Sorry, error occurred while accessing server, please try again.' });
      });
    } else {

      deferred.resolve(Cart_Store.getCount());
    }

    return deferred.promise;
  };

  this.getProfiles = function () {
    var deferred = $q.defer();
    if (authService.isSignedIn()) {
      $http.get('/api/get_profiles').success(function (profiles) {
        deferred.resolve(profiles);
      });
    } else {
      deferred.resolve(Cart_Store.getProfiles());
    }
    return deferred.promise;
  };

  this.getProfile = function (profile_id) {
    var deferred = $q.defer();
    if (profile_id) {
      if (authService.isSignedIn()) {
        $http.get('/api/get_profile/' + profile_id).success(function (profile) {
          deferred.resolve(profile);
        });
      } else {
        deferred.resolve(Cart_Store.getProfile(profile_id));
      }
    } else {
      deferred.resolve(undefined);
    }
    return deferred.promise;
  };

  // this function use email address of profiles in the local store to verify with server, see if
  // exists in the server
  // this works only for not sign in, if sign in, email return empty

  this.getExistingEmails = function () {

    var deferred = $q.defer();
    //    console.log('profiles', Cart_Store.getAllProfiles())

    if (!authService.isSignedIn()) {
      var profiles = Cart_Store.getAllProfiles();
      var emails = [];

      for (var key in profiles) {
        if (profiles.hasOwnProperty(key)) {
          console.log('kk', profiles[key]);
          emails.push(profiles[key].email);
        }
      }

      $http.post('/api2/valid_emails', emails).success(function (data) {
        deferred.resolve(data);
      }).error(function (error) {
        deferred.resolve({ status: false, message: 'Sorry, error occurred while checking, please try again.' });
      });
    } else deferred.resolve([]);

    return deferred.promise;
  };

  // get the first local profile for sign up
  this.getDefaultLocalProfile = function () {

    var profiles = Cart_Store.getAllProfiles();
    for (var key in profiles) {
      if (profiles.hasOwnProperty(key)) {
        return profiles[key];
      }
    }
  };

  // cart_id not needed if not logged

  this.delete_item = function (cart_id, item) {

    var deferred = $q.defer();
    if (authService.isSignedIn()) {
      $http.get('/api/delete_cart_item/' + cart_id + '/' + item._id).success(function (data) {
        deferred.resolve({ status: true });
      });
    } else {
      Cart_Store.delete_item(item);
      deferred.resolve({ status: true });
    }
    return deferred.promise;
  };

  // cart_id might be 'local' if not logged in

  this.selection_change = function (cart_id, item) {
    console.log('change_item', cart_id, item);
    var deferred = $q.defer();
    if (cart_id === 'local') {
      Cart_Store.update_item(item);
      deferred.resolve(true);
    } else if (authService.isSignedIn()) {
      $http.post('/api/update_cart_item/' + cart_id + '/' + item._id, item).success(function (data) {
        deferred.resolve(true);
      });
    }
    return deferred.promise;
  };

  this.post_to_server = function (item) {

    var deferred = $q.defer();
    if (authService.isSignedIn()) {
      console.log('show item', item);
      $http.post('/api/post_trademark', item).success(function (data) {
        console.log(data);
        deferred.resolve({ registered: data.status, message: data.message });
      }).error(function (error) {
        deferred.resolve({ registered: false, message: 'Sorry, error occurred while posting, please try again.' });
      });
    } else {
      Cart_Store.add(item);
      deferred.resolve({ registered: true, message: 'true' });
    }

    return deferred.promise;
  };

  // save the local store to server, used this for not logged in selections right after loggin

  this.store_to_server_not_to_use = function () {

    var deferred = $q.defer();

    if (Cart_Store.getCount() > 0) {
      var temp = Cart_Store.getCart();
      console.log('temp', temp.items);
      var s = angular.toJson(temp.items); // this removes all properties with $$
      var items = JSON.parse(s); // convert to json

      $http.post('/api/batch_post', items).success(function (data) {
        deferred.resolve(data);
      }).error(function (error) {
        deferred.resolve({ status: false, message: 'Sorry, error occurred while posting, please try again.' });
      });
    } else {
      console.log('nothing from store');
      deferred.resolve({ status: true });
    }

    return deferred.promise;
  };

  this.store_to_server = function () {

    var deferred = $q.defer();

    if (Cart_Store.getCount() > 0) {
      var temp = Cart_Store.getCart();
      var profiles = Cart_Store.getAllProfiles();

      var s = angular.toJson(temp.items); // this removes all properties with $$
      var items = JSON.parse(s); // convert to json

      // let's remove profile object as we already have the profile_id
      for (var i = 0; i < items.length; i++) {
        delete items[i].profile;
      }
      console.table(items);

      $http.post('/api/batch_post2', { profiles: profiles, items: items }).success(function (data) {
        deferred.resolve(data);
      }).error(function (error) {
        deferred.resolve({ status: false, message: 'Sorry, error occurred while posting, please try again.' });
      });
    } else {
      console.log('nothing from store');
      deferred.resolve({ status: true });
    }

    return deferred.promise;
  };

  this.clean_local_store = function () {
    console.log('clean the local store');
    Cart_Store.destroy();
  };

  return this;
};

},{}],"/home/wang/projects/tks_project/tks/app/js/web/web_services_case.js":[function(require,module,exports){
"use strict";

var price_calc = require("./price_calc.js");
var address = require("./address_to_profile");

exports.Case_gen = function ($http, $q, register_trademark, Cart_Store, user_profile, authService) {

    function get_type(case_o) {
        if (case_o.image_url) return 'wl';else return 'w'; // 'w', 'l', 'wl'   word only/logo/word and logo
    }

    function get_classes(case_o) {

        var ret = [];
        var classes = case_o.classes.split(",");
        for (var i = 0; i < classes.length; i++) {
            ret.push({ name: classes[i], details: "" });
        }
        return ret;
        //  return [{"name":"01","details":"w1"}]
    }

    function get_price(country) {

        var deferred = $q.defer();
        $http.get('/api/get_price/' + country).success(function (data) {
            // customized price will be used if user logged in
            console.log('get price 123 in ', data);
            deferred.resolve(data);
        }).error(function (error) {
            deferred.reject({ msg: error });
        });
        return deferred.promise;
    }

    function set_item(case_obj, country) {
        var deferred = $q.defer();

        var rec = register_trademark.init_item_ex("registration", country);
        rec.country = country;
        rec.wordmark = case_obj.mark;
        rec.image_url = case_obj.image_url;
        rec.type = get_type(case_obj);
        rec.classes = get_classes(case_obj);

        console.log('rec', case_obj);

        get_price(country).then(function (price) {
            //rec.price = price
            rec.amount_orig = price_calc.get_amount(rec, price);
            deferred.resolve(rec);
        });

        return deferred.promise;
    }

    return {

        get_case_obj: function get_case_obj(case_code) {

            var deferred = $q.defer();

            $http.get('/api2/get_case/' + case_code).success(function (data) {
                deferred.resolve(data);
            });

            return deferred.promise;
        },

        get_price: function get_price(country) {

            var deferred = $q.defer();
            $http.get('/api/get_price/' + country).success(function (data) {
                // customized price will be used if user logged in
                console.log('get price 123 in ', data);
                deferred.resolve(data);
            }).error(function (error) {
                deferred.reject({ msg: error });
            });
            return deferred.promise;
        },

        // based on input, return the items ordered

        get_items: function get_items(case_obj, selectedCountires) {

            var ret = [];
            selectedCountires.forEach(function (country) {
                var p = set_item(case_obj, country);
                ret.push(p);
            });

            return $q.all(ret).then(function (results) {
                console.log('results123', results);
                return results;
            });
        },

        // request server to fetch from remote the image
        // and return the new url in the local server

        fetch_images: function fetch_images(image_url) {
            var deferred = $q.defer();

            console.log('image_url', image_url);
            if (image_url !== 'null' && image_url) {
                $http.get('/api2/fetch_image/' + image_url).success(function (server_url) {
                    deferred.resolve(server_url.url);
                });
            } else deferred.resolve(undefined);

            return deferred.promise;
        },

        // return a default profile based on address

        get_default_profile: function get_default_profile(case_obj) {
            //  return address_to_profile(case_obj)
            var p = address.parse(case_obj.address);
            p.profile_name = case_obj.case_code;
            return p;
        },

        // create a profile in local store or in the server(if signed)
        // return profile_id

        create_profile: function create_profile(profile) {
            var deferred = $q.defer();
            if (!authService.isSignedIn()) {
                var profile_id;
                var pIDs = Cart_Store.getAllProfilesID(); // let's try to get existing profile first
                if (pIDs.length > 0) profile_id = pIDs[0];else profile_id = Cart_Store.create_profile(profile);
                deferred.resolve(profile_id);
            } else {

                // let's see if there is an existing profile in the server
                user_profile.get_all_profilesID().then(function (profilesID) {
                    if (profilesID.length > 0) {
                        deferred.resolve(profilesID[0]);
                    } else {
                        user_profile.post(profile).then(function (status) {
                            deferred.resolve(status.id);
                        });
                    }
                });
            }
            return deferred.promise;
        }

    };
};

// some more specific logic is needed here
/*
 function address_to_profile(case_obj) {

 console.log('case_obj', case_obj)
 var profile = {
 address: "ceb",
 city: "ceb",
 contact: "",
 country: "UK",
 email: "",
 inc_country: "UK",
 nature: "I",
 phone: "",
 profile_name: case_obj.case_code,
 state: "",
 street: "",
 zip: ""
 }
 return profile
 }
 */

},{"./address_to_profile":"/home/wang/projects/tks_project/tks/app/js/web/address_to_profile.js","./price_calc.js":"/home/wang/projects/tks_project/tks/app/js/web/price_calc.js"}],"/home/wang/projects/tks_project/tks/app/js/web/web_services_procs.js":[function(require,module,exports){
// @ts-check
"use strict";

var countries = require("../../../data/countries/tm_countries.json");

exports.news_feeder_proc = function ($http, $q) {

    var feeds = [];
    return {

        get_feeds: function get_feeds(news_num) {
            var deferred = $q.defer();
            console.log('get feeds ', news_num);

            if (feeds.length > 0) {
                deferred.resolve(feeds[0]);
                feeds.shift();
            } else {

                $http.get('/api_info/get_feeds/10').success(function (data) {
                    feeds = data;

                    console.log('feeds', feeds);
                    console.log('feeds0', feeds[0]);
                    //    deferred.resolve(feeds[0])
                    deferred.resolve(feeds);
                    //  feeds.shift()
                }).error(function (error) {
                    console.log(error);
                });
            }
            return deferred.promise;
        }
    };
};

exports.attorneys_service_proc = function ($http, $q) {

    this.get = function (name) {

        var deferred = $q.defer();

        $http.get('/api_info/get_attorneys/' + name).success(function (data) {
            deferred.resolve(data);
        }).error(function (error) {
            deferred.resolve({});
        });

        return deferred.promise;
    };

    return this;
};

exports.Search_classes = function ($http, $q) {

    this.search = function (search_text) {
        console.log('search classes');
        var deferred = $q.defer();

        $http.get('/api2/search_classes?q=' + search_text).success(function (data) {
            console.log(data);
            deferred.resolve({ status: data.status === 'ok', message: data.message, result: data.result });
        }).error(function (error) {
            deferred.resolve({
                status: false,
                message: 'Sorry, error occurred while accessing server, please try again.'
            });
        });
        return deferred.promise;
    };
    return this;
};

exports.help_info = function ($http, $q) {
    this.get = function (info_key) {
        var deferred = $q.defer();

        $http.get('/api2/get_help_info/' + info_key).success(function (data) {
            console.log('b');
            console.log(data);
            deferred.resolve({ status: data.status === 'ok', message: data.message, result: data.info });
        }).error(function (error) {
            deferred.resolve({
                status: false,
                message: 'Sorry, error occurred while accessing server, please try again.'
            });
        });

        return deferred.promise;
    };
};

exports.TMCountries = function ($http, $q) {
    this.get = function () {

        var deferred = $q.defer();

        $http.get('/api2/get_countries').success(function (data) {
            deferred.resolve(data);
        }).error(function (error) {
            deferred.resolve({});
        });

        return deferred.promise;
    };

    return this;
};

exports.user_account = function ($http, $q) {

    this.get = function () {

        var deferred = $q.defer();

        $http.get('/api/myaccount/').success(function (data) {
            deferred.resolve(data);
        });

        return deferred.promise;
    };

    this.post = function (account) {

        var deferred = $q.defer();
        console.log('saving account ', account);
        $http.post("/api/update_user", account).success(function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    };

    return this;
};

exports.user_profile = function ($http, $q) {

    this.get = function (profile_id) {

        var deferred = $q.defer();

        if (profile_id === 'new') {
            deferred.resolve({});
        } else $http.get('/api/profile/' + profile_id).success(function (data) {
            console.log('user_profile111', data);
            console.log(JSON.stringify(data));
            deferred.resolve(data);
        });

        return deferred.promise;
    };

    this.post = function (profile) {

        var deferred = $q.defer();
        $http.post("/api/update_profile", profile).success(function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    };

    this.get_all_profilesID = function () {
        var deferred = $q.defer();
        $http.get('/api/get_profiles').success(function (profiles) {
            deferred.resolve(Object.keys(profiles));
        });
        return deferred.promise;
    };

    return this;
};

exports.page_info = function () {

    var _title = 'Trademarkers.com';
    var _keywords = "Register Trademarks";
    var _desc = "Register Trademarks";

    var _jsond_id = {

        "@context": "http://www.schema.org",
        "@type": "Organization",
        "@id": "https://www.trademarkers.com",
        "name": "Trademarkers LLC123",
        "url": "https://www.trademarkers.com",
        "logo": "https://www.trademarkers.com/images/logo2.png",
        "image": "https://www.trademarkers.com/images/logo2.png",
        "description": "TRADEMARKERS are specialists in handling the registration of your trademarks worldwide"

    };

    return {

        title: function title() {
            return _title;
        },

        setTitle: function setTitle(newTitle) {
            _title = newTitle;
        },

        description: function description() {
            return _desc;
        },

        keywords: function keywords() {
            return _keywords;
        },

        json_id: function json_id() {
            return _jsond_id;
        },

        set_keywords: function set_keywords(kwords) {
            _keywords = kwords;
        },

        set_description: function set_description(desc) {
            _desc = desc;
        },

        set_by_country_code: function set_by_country_code(country) {
            //_title = newTitle;
            var seo = 'Trademark Registration in ' + country + '.TradeMarkers Provides Quick and Easy Trademark Filing, Monitoring and Study Services. File Today!';
            console.log('seo goes here', country);
            var info = countries[country];
            if (info) {

                var agency = info.agency_short ? info.agency_short : info.agency_name;

                _desc = "Trademark Registration in " + info.name + ". TradeMarkers Provides Quick and Easy Trademark Filing, Monitoring and Study Services. File with " + agency + " Today!";
                _keywords = "Trademark Registration in " + info.name + ", Trademark Registration in " + agency + ", International Trademark Registration, Trademark Filing Worldwide, Trademark Registration Globally";
            } else {

                _title = 'Trademarkers.com';
                _keywords = "Register Trademarks";
                _desc = "Register Trademarks";
            }
        }

    };
};

},{"../../../data/countries/tm_countries.json":"/home/wang/projects/tks_project/tks/data/countries/tm_countries.json"}],"/home/wang/projects/tks_project/tks/app/js/web/web_services_transaction.js":[function(require,module,exports){
// @ts-check
"use strict";

var input_formats = require("../../../data/formats.json");

console.log(input_formats);

for (var key in input_formats) {
  if (input_formats.hasOwnProperty(key)) {
    console.log(key + " -> " + input_formats[key]);
    input_formats[key].POA_required = -1;
  }
}

exports.register_trademark = function ($http, $q) {

  // POA_required : -1, not needed, (0,1,2... needed, and different condition for different countries)

  var tm_item = { type: "", wordmark: "", last6mon: false, POA: 0, classes: [] }; // w, l, wl, tm (trademark ), tml (trademark with logo)
  var input_format = { input_type: 1, POA_required: false, word_label: 'Trademark', country: 'US' };
  var price = null;

  var myService = {

    isImplemented: function isImplemented(country) {
      return input_formats[country] !== undefined;
    },

    save: function save(item) {
      tm_item = item;
    },

    save_profile: function save_profile(profile) {
      tm_item.profile = profile;
    },

    get: function get() {
      console.log('get', tm_item);
      return tm_item;
    },

    getPriceObjectByType: function getPriceObjectByType(type) {
      if (price) {
        var p2 = type === 'wl' || type === 'tml';
        if (price instanceof Array) {
          if (p2) return price[1];else return price[0];
        } else {
          return price;
        }
      } else return undefined;
    },

    getStudyAmount: function getStudyAmount() {
      var p = this.getPriceObjectByType(tm_item.type);
      if (p) {
        var amt = p.study1;
        for (var j = 1; j < tm_item.classes.length; j++) {
          amt = amt + p.study2;
        }
        return amt;
      } else return -1;
    },

    getRegAmount: function getRegAmount() {
      var p = this.getPriceObjectByType(tm_item.type);

      console.log('getRegAmount', p);
      console.log('item', tm_item);
      if (p) {
        var amt = p.reg1;
        if (!p.mc) {
          for (var j = 1; j < tm_item.classes.length; j++) {
            amt = amt + p.reg2;
          }
        } else {
          // multiple classes in the price 1
          for (var k = p.mc_num; k < tm_item.classes.length; k++) {
            amt = amt + p.reg2;
          }
        }

        if (tm_item.claim_priority && p.priority_filing) amt = amt + p.priority_filing;

        return amt;
      } else return -1;
    },

    getAmount: function getAmount(country) {
      console.log('get_amount', tm_item);
      if (tm_item.classes.length === 0) return 0;else {

        var country_price, unit_price, amt;

        if (tm_item.service === 'registration') {
          return this.getRegAmount();

          /*
           if (["EU", "CH", "NO"].indexOf(country) !== -1) {
           country_price = prices[country]
           unit_price = country_price[tm_item.type]
           amt = unit_price.price1
           for (var i = 1; i < tm_item.classes.length; i++) {
           amt = amt + unit_price.price2
           }
           if (amt === undefined)
           amt = 0
           return amt
           } else {
           return this.getRegAmount()
           }*/
        } else if (tm_item.service === 'study') {
            return this.getStudyAmount();
          } else return 0;
      }
    },

    init_item: function init_item(service) {

      console.log('initinggg', service);

      if (service === 'registration') tm_item = {
        service: service, type: "", wordmark: "", last6mon: false, POA: input_format.POA_required,
        classes: [], commerce_use: input_format.commerce_use, selected: true
      };else if (service === 'study') {
        tm_item = {
          service: service, type: "", wordmark: "", classes: [], study_summary: "", study_type: input_format.study_type, selected: true
        };
      }

      tm_item.profile = {}; // this will be populated later

      console.log(tm_item);
    },

    // not using any internal variable to init
    init_item_ex: function init_item_ex(service, country) {

      var ret_item = {};
      var input_format2 = input_formats[country];

      if (service === 'registration') ret_item = {
        service: service, type: "", wordmark: "", last6mon: false, POA: input_format2.POA_required,
        classes: [], commerce_use: input_format2.commerce_use, selected: true
      };else if (service === 'study') {
        ret_item = {
          service: service, type: "", wordmark: "", classes: [], study_summary: "", study_type: input_format2.study_type, selected: true
        };
      }

      ret_item.profile = {}; // this will be populated later

      return ret_item;
    },

    get_country_info: function get_country_info(country) {

      var deferred = $q.defer();

      $http.get('/api_info/get_country_info/' + country).success(function (country_info) {
        console.log('get country info service', country_info);
        deferred.resolve(country_info);
      }).error(function (error) {
        console.log('error in get_country_info');
      });

      return deferred.promise;
    },

    // set's format and prices for the particular country
    // price is returned as well as promise, this can be used in ngRoute's resolve

    set_input_format: function set_input_format(country) {
      input_format = input_formats[country];
    },

    set_price: function set_price(country) {

      var deferred = $q.defer();
      $http.get('/api/get_price/' + country).success(function (data) {
        // customized price will be used if user logged in
        console.log('get price in ', data);
        price = data;
        deferred.resolve(data);
      }).error(function (error) {
        price = null;
      });
      return deferred.promise;
    },

    get_input_format: function get_input_format() {
      return input_format;
    }

    /* moved to web_services_cart
     post_to_server: function () {
       var deferred = $q.defer();
       $http.post('/api/post_trademark', tm_item).
        success(function (data) {
          myService.init_item()
          deferred.resolve({registered: data.status, message: data.message});
        }).
        error(function (error) {
          deferred.resolve({registered: false, message: 'Sorry, error occurred while posting, please try again.'});
        });
       return deferred.promise;
     }*/

  };

  return myService;
};

},{"../../../data/formats.json":"/home/wang/projects/tks_project/tks/data/formats.json"}],"/home/wang/projects/tks_project/tks/app/js/web/web_services_transaction_procs.js":[function(require,module,exports){
// @ts-check
"use strict";

var countries = require('countries');

function get_country_info_proc($http, $q, country_code, locale) {

    var deferred = $q.defer();
    var country = countries.get_country(country_code);

    console.log('get_country_info12345 for ', country_code);
    $http.get('/api_info/get_country_info/' + country_code + '?locale=' + locale).success(function (country_info) {
        console.log('get country info service', country_info);
        country_info.country = country;
        deferred.resolve(country_info);
    }).error(function (error) {
        console.log('error in get_country_info');
        deferred.reject({ msg: 'error in get_country_info' });
    });

    return deferred.promise;
}

exports.get_country_info = function ($route, $http, $q, $translate) {
    return get_country_info_proc($http, $q, $route.current.params.country, $translate.use());
};

// get country info directly from path, ex trademark-registration-in-turnkey

exports.get_country_info_SEO = function ($route, $http, $q, $translate) {

    var country = countries.get_country_by_regex(/^\/trademark-registration-in-(\w+)/i, $route.current.originalPath);
    if (country) {
        return get_country_info_proc($http, $q, country.code, $translate.use());
    }
    /*
    var deferred = $q.defer();
     var country
     country = countries.get_country_by_regex(/^\/trademark-registration-in-(\w+)/i, $route.current.originalPath)
     if (country) {
         console.log('country code', country.code)
        $http.get('/api_info/get_country_info/' + country.code).success(function (country_info) {
            console.log('get country info service seo', country_info)
            country_info.country = country
            deferred.resolve(country_info);
        }).error(function (error) {
            console.log('error in get_country_info')
        });
    }
     return deferred.promise;
    */
};

exports.b4registry2 = function ($route, $q, register_trademark) {

    var country = $route.current.params.country;

    register_trademark.set_input_format(country);
    var price = register_trademark.set_price(country);
    var format = register_trademark.set_price(country);

    return $q.all([price, register_trademark.get_input_format()]).then(function (results) {
        return {
            price: results[0],
            input_format: results[1],
            line: register_trademark.get() // line buffer for the transaction
        };
    });
};

exports.b4registry5 = function ($route, $q, register_trademark, Cart) {

    var country = $route.current.params.country;
    var profile_id = register_trademark.get().profile_id; // might be undefined

    return $q.all([Cart.getProfile(profile_id)]).then(function (results) {

        console.log('b4 reg5 or study3', results);
        return {
            line: register_trademark.get(), // line buffer for the transaction
            profile: results[0]
        };
    });
};

exports.b4trademark_contact = function ($route, $q, register_trademark, Cart) {

    var country = $route.current.params.country;

    return $q.all([register_trademark.get(), Cart.getProfiles()]).then(function (results) {
        console.log(results.length);
        console.log('b4trrade', results);
        return {
            line: results[0], // line buffer for the transaction
            profiles: results[1]
            /*
             profiles : {
             '123': 'Jack',
             'abc': 'Dick'
             }*/
        };
    });
};

exports.b4trademark_address = function ($route, $q, register_trademark, Cart) {

    var country = $route.current.params.country;
    var reg = register_trademark.get();

    console.log('reg in b4trademark_address', reg);

    // var back_to = reg.service === 'study' ? '/study2/' : '/register3/'
    var back_to = '/trademark_contact/';
    var next_to = reg.service === 'study' ? '/study3/' : '/register5/';

    return $q.all([Cart.getProfiles()]).then(function (results) {
        return {
            line: reg, // line buffer for the transaction
            profile: reg.profile,
            profiles: results[0],
            back_path: back_to + country,
            next_path: next_to + country
        };
    });
};

/*
 profiles : {
 '123': 'Jack',
 'abc': 'Dick'
 }*/
exports.b4account_address = function ($route, $q, $location, user_account) {

    var back_to = '/account/';
    var next_to = 'next'; // this will not be used as next_proc is provided below

    return $q.all([user_account.get()]).then(function (results) {
        console.log('results', results);
        return {
            profile: results[0],
            back_path: back_to,
            next_path: next_to,
            next_prompt: 'Save',
            side_panel_account: true,
            next_proc: function next_proc(account) {
                user_account.post(account).then(function (status) {
                    console.log(status);
                    $location.path('/account/');
                });
            }
        };
    });
};

exports.b4profile_address = function ($route, $q, $location, user_profile) {

    var back_to = '/profile/' + $route.current.params.id;

    return $q.all([user_profile.get($route.current.params.id)]).then(function (results) {
        console.log('results 4 profile', results);
        return {
            profile: results[0],
            back_path: back_to,
            next_path: '',
            next_prompt: 'Save',
            side_panel_account: true,
            next_proc: function next_proc(account) {
                user_profile.post(account).then(function (status) {
                    console.log(status);
                    $location.path(back_to);
                });
            }
        };
    });
};

exports.b4profile = function ($route, $q, $location, user_profile) {

    return $q.all([user_profile.get($route.current.params.id)]).then(function (results) {
        console.log('results 4 profile', results);
        return {
            profile: results[0]
        };
    });
};

},{"countries":"/home/wang/projects/tks_project/tks/node_modules/countries/countries.js"}],"/home/wang/projects/tks_project/tks/app/js/web/web_studies.js":[function(require,module,exports){
"use strict";
/*global angular, console */

angular.module('tks.study.controllers', [])

// starting of study process

.controller('study1', ['$scope', '$http', '$location', '$routeParams', 'register_trademark', function ($scope, $http, $location, $routeParams, register_trademark) {

  $scope.country = $routeParams.country;

  $scope.start_study = function (country) {

    console.log('study ', country);
    register_trademark.set_input_format(country);
    register_trademark.init_item('study');
    $location.path('/study2/' + country);
  };
}]).controller('study3', ['$scope', '$http', '$location', '$rootScope', '$routeParams', 'register_trademark', 'AUTH_EVENTS', 'Cart', 'registry_initData', function ($scope, $http, $location, $rootScope, $routeParams, register_trademark, AUTH_EVENTS, Cart, registry_initData) {

  $scope.country = $routeParams.country;

  $scope.reg = register_trademark.get();

  console.log('study3', registry_initData);

  if (registry_initData.profile) $scope.profile = registry_initData.profile;else $scope.profile = $scope.reg.profile; // no profile_id, use embedded profile

  $scope.back = function (reg) {
    reg.country = $routeParams.country;
    //        $location.path('/study2/' + $scope.country)
    if (reg.profile_id) $location.path('/trademark_contact/' + $routeParams.country);else $location.path('/trademark_address/' + $routeParams.country);
  };

  $scope.submit = function (reg) {
    register_trademark.save(reg);

    Cart.post_to_server(register_trademark.get()).then(function (status) {
      console.log('cart post to server status');
      $rootScope.$broadcast(AUTH_EVENTS.cartUpdated);

      Cart.getExistingEmails().then(function (emails) {
        if (emails.length > 0) {
          $location.path('/before_check_out/' + Cart.getRemoteCart()._id);
        } else $location.path('/shopping_cart');
      });
    });
  };
}]);

},{}],"/home/wang/projects/tks_project/tks/data/aripo.json":[function(require,module,exports){
module.exports={
  "BW":"Botswana",
  "LS":"Lesotho",
  "LR":"Liberia",
  "MW":"Malawi",
  "NA":"Namibia",
  "SZ":"Swaziland",
  "TZ":"Tanzania",
  "UG":"Uganda",
  "ZW":"Zimbabwe"
}
},{}],"/home/wang/projects/tks_project/tks/data/countries/tm_countries.json":[function(require,module,exports){
module.exports={
  "CH": {
    "name": "Switzerland",
    "agency_name": "Swiss Federal Institute of Intellectual Property",
    "agency_short": "IPI",
    "opposition_period": "three months",
    "registration_period": "within 2 weeks",
    "madrid_protocol": true
  },
  "NO": {
    "name": "Norway",
    "agency_name": "Norwegian Industrial Property Office",
    "agency_short": "NIPO",
    "opposition_period": "three months",
    "registration_period": "3-6 months",
    "madrid_protocol": true
  },
  "CN": {
    "name": "China",
    "agency_name": "State Administration for Industry and Commerce Trademark Office",
    "agency_short": "CTMO",
    "opposition_period": "three months",
    "registration_period": "15 months",
    "madrid_protocol": true
  },
  "RU": {
    "name": "The Russian Federation",
    "agency_name": "Federal Services for Intellectual property",
    "agency_short": "ROSPATENT",
    "opposition_period": "5 years",
    "registration_period": "12 to 14 months",
    "madrid_protocol": true
  },
  "US": {
    "name": "the United States of America",
    "agency_name": "U.S. Patent and Trademark Office",
    "agency_short": "USPTO",
    "opposition_period": "30 days",
    "registration_period": "9 to 16 months",
    "madrid_protocol": true
  },
  "MX": {
    "name": "Mexico",
    "agency_name": "Mexican Institute of Industrial Property",
    "agency_short": "MIIP",
    "opposition_period": "4 to 6 months",
    "registration_period": "4 to 6 months",
    "madrid_protocol": true
  },
  "BR": {
    "name": "Brazil",
    "agency_name": "Brazilian Patent and Trademark Office",
    "agency_short": "BPTO",
    "opposition_period": "60 days",
    "registration_period": "three years",
    "madrid_protocol": true
  },
  "IT": {
    "name": "Italy",
    "agency_name": "Patent and Trademark Office",
    "agency_short": "UIBM",
    "opposition_period": "3 months",
    "registration_period": "6 months",
    "madrid_protocol": true
  },
  "LV": {
    "name": "Latvia",
    "agency_name": "Patent Office of the republic of Latvia",
    "agency_short": "PO",
    "opposition_period": "3 months",
    "registration_period": "8-10 months",
    "madrid_protocol": true
  },
  "DE": {
    "name": "Germany",
    "agency_name": "Trademark Department of the German Patent and Trademark Office",
    "agency_short": "DPMA",
    "opposition_period": "3 months",
    "registration_period": "6-9 months",
    "madrid_protocol": true
  },
  "ES": {
    "name": "Spain",
    "agency_name": "Spanish Patent and Trade Mark Office",
    "agency_short": "SPTO",
    "opposition_period": "2 months",
    "registration_period": "6-12 months",
    "madrid_protocol": true
  },
  "FR": {
    "name": "France",
    "agency_name": "National Institute of Industrial Property",
    "agency_short": "I.N.P.I",
    "opposition_period": "2 months",
    "registration_period": "4-6 months",
    "madrid_protocol": true
  },
  "PT": {
    "name": "Portugal",
    "agency_name": "Portuguese Institute of industrial property",
    "agency_short": "INPI",
    "opposition_period": "2 months",
    "registration_period": "4-6 months",
    "madrid_protocol": true
  },
  "GB": {
    "name": "United Kingdom",
    "agency_name": "Intellectual Property Office",
    "agency_short": "IP",
    "opposition_period": "2 months",
    "registration_period": "4-6 months",
    "madrid_protocol": true
  },
  "SE": {
    "name": "Sweden",
    "agency_name": "Swedish Patent and Registration Office",
    "agency_short": "PRV",
    "opposition_period": "3 months",
    "registration_period": "3-6 months",
    "madrid_protocol": true
  },
  "PL": {
    "name": "Poland",
    "agency_name": "Polish Patent Office",
    "agency_short": "UPRP",
    "opposition_period": "6 months",
    "registration_period": "2 years",
    "madrid_protocol": true
  },
  "CZ": {
    "name": "Czech Republic",
    "agency_name": "Industrial Property Office",
    "agency_short": "UPV",
    "opposition_period": "3 months",
    "registration_period": "6-10 months",
    "madrid_protocol": true
  },
  "EE": {
    "name": "Estonia",
    "agency_name": "Estonian Patent Office",
    "agency_short": "EPA",
    "opposition_period": "2 months",
    "registration_period": "16-18 months",
    "madrid_protocol": true
  },
  "FI": {
    "name": "Finland",
    "agency_name": "Finnish Patent and Registration Office",
    "agency_short": "PRH",
    "opposition_period": "2 months",
    "registration_period": "4-5 months",
    "madrid_protocol": true
  },
  "AT": {
    "name": "Austria",
    "agency_name": "Austrian Patent Office",
    "agency_short": "APO",
    "opposition_period": "2 months",
    "registration_period": "3-4 months",
    "madrid_protocol": true
  },
  "DK": {
    "name": "Denmark",
    "agency_name": "Danish Patent and Trademark Office",
    "agency_short": "DPTO",
    "opposition_period": "2 months",
    "registration_period": "3 months",
    "madrid_protocol": true
  },
  "MC": {
    "name": "Monaco",
    "agency_name": "Monaco Intellectual Property",
    "agency_short": "MIPRO",
    "opposition_period": "3 months",
    "registration_period": "3 months",
    "madrid_protocol": true
  },
  "MT": {
    "name": "Malta",
    "agency_name": "Industrial Property Registrations Directorate",
    "agency_short": "IPR",
    "opposition_period": "9 months",
    "registration_period": "9 months",
    "madrid_protocol": false
  },
  "IE": {
    "name": "Ireland",
    "agency_name": "Irish Patents Office",
    "agency_short": "IPO",
    "opposition_period": "3 months",
    "registration_period": "2 months",
    "madrid_protocol": true
  },
  "CA": {
    "name": "Canada",
    "agency_name": "Trademarks Office of the Canadian Intellectual Property Office",
    "agency_short": "CIPO",
    "opposition_period": "2 months",
    "registration_period": "14-18 months",
    "madrid_protocol": false
  },
  "CU": {
    "name": "Cuba",
    "agency_name": "Cuban Intellectual Property Office",
    "agency_short": "OCPI",
    "opposition_period": "2 months",
    "registration_period": "18 months",
    "madrid_protocol": true
  },
  "UA": {
    "name": "Ukraine",
    "agency_name": "Ukrainian Institute of Industrial Property",
    "agency_short": "UIPV",
    "opposition_period": "12-15 months",
    "registration_period": "12-15 months",
    "madrid_protocol": true
  },
  "UY": {
    "name": "Uruguay",
    "agency_name": "National Direction of Industrial Property",
    "agency_short": "DNPI",
    "opposition_period": "30 days",
    "registration_period": "12-14 months",
    "madrid_protocol": false
  },
  "UZ": {
    "name": "Uzbekistan",
    "agency_name": "Agency on Intellectual Property of the Republic of Uzbekistan",
    "agency_short": "",
    "opposition_period": "12 months",
    "registration_period": "12 months",
    "madrid_protocol": true
  },
  "VE": {
    "name": "Venezuela",
    "agency_name": "Industrial Property Registry - Autonomous Service of Intellectual Property",
    "agency_short": "SAPI",
    "opposition_period": "30 working days",
    "registration_period": "12 months",
    "madrid_protocol": false
  },
  "WB": {
    "name": "Westbank",
    "agency_name": "Office of the Registrar in the West Bank",
    "agency_short": "Office of the Registrar in the West Bank",
    "opposition_period": "3 months",
    "registration_period": "16-18 months",
    "madrid_protocol": false
  },
  "YE": {
    "name": "Yemen",
    "agency_name": "Sector, General Administration of Intellectual Property Protection",
    "agency_short": "YIPO",
    "opposition_period": "3 months",
    "registration_period": "12 months",
    "madrid_protocol": false
  },
  "ZW": {
    "name": "Zimbabwe",
    "agency_name": "the African Regional Industrial Property Organization",
    "agency_short": "ARIPO",
    "opposition_period": "3 months",
    "registration_period": "8-16 months",
    "madrid_protocol": false
  },
  "PK": {
    "name": "Pakistan",
    "agency_name": "Intellectual Property Organization of Pakistan, Trade Marks Registry",
    "agency_short": "IPO",
    "opposition_period": "2 months",
    "registration_period": "9-12 months",
    "madrid_protocol": false
  },
  "PA": {
    "name": "Panama",
    "agency_name": "Panama Industrial Property Office",
    "agency_short": "DIGERPI",
    "opposition_period": "2 months",
    "registration_period": "12-18 months",
    "madrid_protocol": false
  },
  "PY": {
    "name": "Paraguay",
    "agency_name": "National Directorate of Intellectual Property",
    "agency_short": "",
    "opposition_period": "60 working days",
    "registration_period": "8 months",
    "madrid_protocol": false
  },
  "PH": {
    "name": "Philippines",
    "agency_name": "Intellectual Property Office of Philippines",
    "agency_short": "IPOPHIL",
    "opposition_period": "30 working days",
    "registration_period": "4-12 months",
    "madrid_protocol": true
  },
  "PR": {
    "name": "Puerto Rico",
    "agency_name": "the Department of Economic development and Commerce",
    "agency_short": "",
    "opposition_period": "30 working days",
    "registration_period": "15-20 months",
    "madrid_protocol": false
  },
  "QA": {
    "name": "Qatar",
    "agency_name": "Industrial Property Office Trademarks, Industrial Designs & G.I. Competent administration Ministry of Business and Trade, Department of Commercial Registration and Licensing",
    "agency_short": "",
    "opposition_period": "4 months",
    "registration_period": "8-12 months",
    "madrid_protocol": false
  },
  "RO": {
    "name": "Romania",
    "agency_name": "State Office for Inventions and Trademarks",
    "agency_short": "OSIM",
    "opposition_period": "2 months",
    "registration_period": "8-12 months",
    "madrid_protocol": true
  },
  "RW": {
    "name": "Rwanda",
    "agency_name": "Rwanda Development Board (RDB) Office of the Registrar General",
    "agency_short": "RDB",
    "opposition_period": "30 working days",
    "registration_period": "12 months",
    "madrid_protocol": true
  },
  "SA": {
    "name": "Saudi Arabia",
    "agency_name": "General Directorate of Industrial Property King Abdullaziz City for Science and Technology",
    "agency_short": "KACST",
    "opposition_period": "90 working days",
    "registration_period": "12-14 months",
    "madrid_protocol": false
  },
  "RS": {
    "name": "Serbia",
    "agency_name": "Intellectual Property Office Republic of Serbia",
    "agency_short": "ZIS",
    "opposition_period": "30 working days",
    "registration_period": "3-6 months",
    "madrid_protocol": true
  },
  "SG": {
    "name": "Singapore",
    "agency_name": "Intellectual Property Office of Singapore",
    "agency_short": "IPOS",
    "opposition_period": "two months",
    "registration_period": "6 to 12 months",
    "madrid_protocol": true
  },
  "SK": {
    "name": "Slovakia",
    "agency_name": "Industrial Property Office of the Slovak Republic",
    "agency_short": "INDPROP",
    "opposition_period": "3 months",
    "registration_period": "4-5 months",
    "madrid_protocol": true
  },
  "SI": {
    "name": "Slovenia",
    "agency_name": "Slovenian Intellectual Property Office, Ministry of Economy",
    "agency_short": "SIPO",
    "opposition_period": "3 months",
    "registration_period": "4-5 months",
    "madrid_protocol": true
  },
  "ZA": {
    "name": "South Africa",
    "agency_name": "Companies and Intellectual Property Commission",
    "agency_short": "CIPC",
    "opposition_period": "3 months",
    "registration_period": "2-3 years",
    "madrid_protocol": false
  },
  "AL": {
    "name": "Albania",
    "agency_name": "Albanian Directorate of Patents and Marks",
    "agency_short": "ALPTO",
    "opposition_period": "3 months",
    "registration_period": "6-9 months",
    "madrid_protocol": true
  },
  "AG": {
    "name": "Antigua and Barbuda",
    "agency_name": "Antigua & Barbuda Intellectual Property & Commerce Office",
    "agency_short": "",
    "opposition_period": "3 months",
    "registration_period": "2 years",
    "madrid_protocol": true
  },
  "AM": {
    "name": "Armenia",
    "agency_name": "Intellectual Property Agency",
    "agency_short": "",
    "opposition_period": "?",
    "registration_period": "6-12 months",
    "madrid_protocol": true
  },
  "VN": {
    "name": "Vietnam",
    "agency_name": "National Office of Industrial Property of Vietnam",
    "agency_short": "NOIP",
    "opposition_period": "12 months",
    "registration_period": "12 months",
    "madrid_protocol": true
  },
  "GR": {
    "name": "Greece",
    "agency_name": "Industrial Property Organization",
    "agency_short": "OBI",
    "opposition_period": "3 months",
    "registration_period": "3 months",
    "madrid_protocol": true
  },
  "LT": {
    "name": "Lithuania",
    "agency_name": "State Patent Bureau of the Republic of Lithuania",
    "agency_short": "SPB",
    "opposition_period": "10-18 months",
    "registration_period": "3 months",
    "madrid_protocol": true
  },
  "PE": {
    "name": "Peru",
    "agency_name": "National Institute for the Defense of Competition and Protection of Industrial Property",
    "agency_short": "INDECOPI",
    "opposition_period": "30 working days",
    "registration_period": "5-3 months",
    "madrid_protocol": false
  },
  "AU": {
    "name": "Australia",
    "agency_name": "IP Australia Department of Industry",
    "agency_short": "",
    "opposition_period": "2 months",
    "registration_period": "8-12 months",
    "madrid_protocol": true
  },
  "AZ": {
    "name": "Azerbaijan",
    "agency_name": "State Committee on Standardization, Metrology and Patents of the Republic of Azerbaijan",
    "agency_short": "",
    "opposition_period": "2 months",
    "registration_period": "8-12 months",
    "madrid_protocol": true
  },
  "BY": {
    "name": "Belarus",
    "agency_name": "National Center of Intellectual Property State Committee on Science and Technologies",
    "agency_short": "NCIP",
    "opposition_period": "?",
    "registration_period": "3 years",
    "madrid_protocol": true
  },
  "BT": {
    "name": "Bhutan",
    "agency_name": "Industrial Property Division  Ministry of Economic Affairs",
    "agency_short": "IPD",
    "opposition_period": "3 months",
    "registration_period": "6 -7 months",
    "madrid_protocol": true
  },
  "BA": {
    "name": "Bosnia and Herzegovina",
    "agency_name": "Institute for Intellectual Property of Bosnia and Herzegovina",
    "agency_short": "IPR",
    "opposition_period": "3 months",
    "registration_period": "1 - 2 years",
    "madrid_protocol": true
  },
  "BG": {
    "name": "Bulgaria",
    "agency_name": "Patent Office of the Republic of Bulgaria",
    "agency_short": "BPO",
    "opposition_period": "3 months",
    "registration_period": "6-9 months",
    "madrid_protocol": true
  },
  "CO": {
    "name": "Colombia",
    "agency_name": "Ministry of Economic Development Superintendence of Industry and Commerce Industrial Property Delegation",
    "agency_short": "SIC",
    "opposition_period": "1 month",
    "registration_period": "4-6 months",
    "madrid_protocol": true
  },
  "HR": {
    "name": "Croatia",
    "agency_name": "Croatian State Intellectual Property Office",
    "agency_short": "CISPO",
    "opposition_period": "3 months",
    "registration_period": "9-12 months",
    "madrid_protocol": true
  },
  "CY": {
    "name": "Cyprus",
    "agency_name": "Department of Registrar of Companies and Official Receiver Ministry of Commerce Industry and Tourism",
    "agency_short": "",
    "opposition_period": "2 months",
    "registration_period": "2-3 years",
    "madrid_protocol": true
  },
  "KP": {
    "name": "North Korea",
    "agency_name": "Trademarks Industrial Designs and Geographical Indications Office of the Democratic People’s Republic of Korea",
    "agency_short": "",
    "opposition_period": "6 months",
    "registration_period": "12 months",
    "madrid_protocol": true
  },
  "EG": {
    "name": "Egypt",
    "agency_name": "Commercial Registry Administration Ministry of Trade and Industry Trademarks Office",
    "agency_short": "",
    "opposition_period": "2 months",
    "registration_period": "2-3 years",
    "madrid_protocol": true
  },
  "GE": {
    "name": "Georgia",
    "agency_name": "National Intellectual Property Center",
    "agency_short": "Sakpatenti",
    "opposition_period": "3 months",
    "registration_period": "12 months",
    "madrid_protocol": true
  },
  "HU": {
    "name": "Hungary",
    "agency_name": "Hungarian Intellectual Property Office",
    "agency_short": "HIPO",
    "opposition_period": "3 months",
    "registration_period": "7-9 months",
    "madrid_protocol": true
  },
  "IS": {
    "name": "Iceland",
    "agency_name": "Icelandic Patent Office",
    "agency_short": "",
    "opposition_period": "2 months",
    "registration_period": "2-4 months",
    "madrid_protocol": true
  },
  "IN": {
    "name": "India",
    "agency_name": "Controller General of Patents Designs & Trade Marks",
    "agency_short": "",
    "opposition_period": "4 months",
    "registration_period": "12-18 months",
    "madrid_protocol": true
  },
  "IR": {
    "name": "Iran",
    "agency_name": "ndustrial Property Office",
    "agency_short": "",
    "opposition_period": "1 month",
    "registration_period": " 8 months",
    "madrid_protocol": true
  },
  "IL": {
    "name": "Israel",
    "agency_name": "Ministry of Justice Patent Office Trademarks Department",
    "agency_short": "",
    "opposition_period": "3 months",
    "registration_period": "18 months",
    "madrid_protocol": true
  },
  "JP": {
    "name": "Japan",
    "agency_name": "International Affairs Division General Affairs Department Japan Patent Office",
    "agency_short": "",
    "opposition_period": "2 months",
    "registration_period": "7-8 months",
    "madrid_protocol": true
  },
  "KZ": {
    "name": "Kazakhstan",
    "agency_name": "Committee for Intellectual Property Rights",
    "agency_short": "",
    "opposition_period": "5 years",
    "registration_period": "10 months",
    "madrid_protocol": true
  },
  "KE": {
    "name": "Kenya",
    "agency_name": "Kenya Industrial Property Institute",
    "agency_short": "KIPI",
    "opposition_period": "2 months",
    "registration_period": "15 months",
    "madrid_protocol": true
  },
  "KG": {
    "name": "Kyrgyzstan",
    "agency_name": "Kyrgyzpatent",
    "agency_short": "",
    "opposition_period": "?",
    "registration_period": "6-14 months",
    "madrid_protocol": true
  },
  "LS": {
    "name": "Lesotho",
    "agency_name": "Ministry of Law and Constitutional Affairs",
    "agency_short": "",
    "opposition_period": "3 months",
    "registration_period": "24 months",
    "madrid_protocol": true
  },
  "LY": {
    "name": "Libya",
    "agency_name": "General Corporations and Commercial Registries Directorate Ministry of Industrial Economic and Trade",
    "agency_short": "",
    "opposition_period": "3 months",
    "registration_period": "3 years",
    "madrid_protocol": false
  },
  "LI": {
    "name": "Liechtenstein",
    "agency_name": "Amt für Volkswirtschaft",
    "agency_short": "AVW",
    "opposition_period": "?",
    "registration_period": "4-6 months",
    "madrid_protocol": true
  },
  "MG": {
    "name": "Madagascar",
    "agency_name": "Malagasy Office of Industrial Property",
    "agency_short": "OMAPI",
    "opposition_period": "?",
    "registration_period": "10 months",
    "madrid_protocol": true
  },
  "MN": {
    "name": "Mongolia",
    "agency_name": "Intellectual Property Office",
    "agency_short": "IPOM",
    "opposition_period": "1 year",
    "registration_period": "6-9 months",
    "madrid_protocol": true
  },
  "ME": {
    "name": "Montenegro",
    "agency_name": "Intellectual Property Office Montenegro",
    "agency_short": "",
    "opposition_period": "3 months",
    "registration_period": "12 months",
    "madrid_protocol": true
  },
  "MA": {
    "name": "Morocco",
    "agency_name": "Moroccan Industrial and Commercial Property Office",
    "agency_short": "OMPIC",
    "opposition_period": "2 months",
    "registration_period": "6 months",
    "madrid_protocol": true
  },
  "MZ": {
    "name": "Mozambique",
    "agency_name": "Industrial Property Institute",
    "agency_short": "IPI",
    "opposition_period": "2 months",
    "registration_period": "4 months",
    "madrid_protocol": true
  },
  "NA": {
    "name": "Namibia",
    "agency_name": "Ministry of Trade and Industry",
    "agency_short": "Directorate Commerce",
    "opposition_period": "2 months",
    "registration_period": "5 years",
    "madrid_protocol": true
  },
  "NZ": {
    "name": "New Zealand",
    "agency_name": "New Zealand Intellectual Property Office",
    "agency_short": "IPONZ",
    "opposition_period": "3 months",
    "registration_period": "6-12 months",
    "madrid_protocol": true
  },
  "OM": {
    "name": "Oman",
    "agency_name": "Intellectual Property Office of Oman",
    "agency_short": "",
    "opposition_period": "3 months",
    "registration_period": "6-12 months",
    "madrid_protocol": true
  },
  "KR": {
    "name": "South Korea",
    "agency_name": "Korean Intellectual Property Office",
    "agency_short": "KIPO",
    "opposition_period": "2 months",
    "registration_period": "9-12 months",
    "madrid_protocol": true
  },
  "MD": {
    "name": "Republic of Moldova",
    "agency_name": "State Agency of Intellectual Property",
    "agency_short": "AGEPI",
    "opposition_period": "3 months",
    "registration_period": "12-14 months",
    "madrid_protocol": true
  },
  "SM": {
    "name": "San Marino",
    "agency_name": "Ufficio di Stato Brevetti e Marchi",
    "agency_short": "USBM",
    "opposition_period": "3 months",
    "registration_period": "12-18 months",
    "madrid_protocol": true
  },
  "LK": {
    "name": "Sri Lanka",
    "agency_name": "National Intellectual Property Office of Sri Lanka",
    "agency_short": "NIPO",
    "opposition_period": "3 months",
    "registration_period": "6 months",
    "madrid_protocol": false
  },
  "SZ": {
    "name": "Swaziland",
    "agency_name": "Ministry of Commerce Industry and Trade",
    "agency_short": "",
    "opposition_period": "8 months",
    "registration_period": "9-12 months",
    "madrid_protocol": true
  },
  "SY": {
    "name": "Syria",
    "agency_name": "Trademark Office (Ministry of Interior Trade and Consumer Protection)",
    "agency_short": "Trademark Office",
    "opposition_period": "3 months",
    "registration_period": "8-12 months",
    "madrid_protocol": true
  },
  "TW": {
    "name": "Taiwan",
    "agency_name": "Intellectual Office",
    "agency_short": "MOEA",
    "opposition_period": "3 months",
    "registration_period": "10-12 months",
    "madrid_protocol": false
  },
  "TJ": {
    "name": "Tajikistan",
    "agency_name": "National Patent and Information Centre",
    "agency_short": "NCPI",
    "opposition_period": "?",
    "registration_period": "18 months",
    "madrid_protocol": true
  },
  "MK": {
    "name": "Macedonia",
    "agency_name": "State Office of Industrial Property",
    "agency_short": "SOIP",
    "opposition_period": "3 months",
    "registration_period": "6-12 months",
    "madrid_protocol": true
  },
  "TN": {
    "name": "Tunisia",
    "agency_name": "National Institute for Standardization and Industrial Property",
    "agency_short": "INNORPI",
    "opposition_period": "2 months",
    "registration_period": "2 years",
    "madrid_protocol": true
  },
  "TR": {
    "name": "Turkey",
    "agency_name": "National Patent Office",
    "agency_short": "TPE",
    "opposition_period": "3 months",
    "registration_period": "10-12 months",
    "madrid_protocol": true
  },
  "TM": {
    "name": "Turkmenistan",
    "agency_name": "Patent Department of Ministry of Economy and Finance of Turkmenistan",
    "agency_short": "",
    "opposition_period": "?",
    "registration_period": "18 months",
    "madrid_protocol": true
  },
  "UG": {
    "name": "Uganda",
    "agency_name": "Trademarks Registry Intellectual Property Department in the Uganda Registration Services Bureau",
    "agency_short": "",
    "opposition_period": "2 months",
    "registration_period": "3 months",
    "madrid_protocol": true
  },
  "AF": {
    "name": "Afghanistan",
    "agency_name": "Afghanistan Central Business Registry and Intellectual Property Ministry of Commerce and Industry",
    "agency_short": "ACBR",
    "opposition_period": "1 month",
    "registration_period": "3-6 months",
    "madrid_protocol": true
  },
  "DZ": {
    "name": "Algeria",
    "agency_name": "National Office of Copyrights and Related Rights Ministry of Communication and Culture",
    "agency_short": "ONDA",
    "opposition_period": "?",
    "registration_period": "24 months",
    "madrid_protocol": true
  },
  "AD": {
    "name": "Andorra",
    "agency_name": "Trademarks Office of the Principality of Andorra Department of Economy Ministry of Economy and Land Management",
    "agency_short": "OMPA",
    "opposition_period": "?",
    "registration_period": "18 months",
    "madrid_protocol": true
  },
  "AO": {
    "name": "Angola",
    "agency_name": "Angolan Institute of Industrial Property Ministry of Geology Mines and Industry",
    "agency_short": "MINGMI",
    "opposition_period": "2 months",
    "registration_period": "24-28 months",
    "madrid_protocol": true
  },
  "AR": {
    "name": "Argentina",
    "agency_name": "National Institute of Industrial Property",
    "agency_short": "INPI",
    "opposition_period": "1 month",
    "registration_period": "18 months",
    "madrid_protocol": false
  },
  "AW": {
    "name": "Aruba",
    "agency_name": "Bureau of Intellectual Property in Aruba",
    "agency_short": "",
    "opposition_period": "6 months",
    "registration_period": "2 months",
    "madrid_protocol": true
  },
  "BS": {
    "name": "Bahamas",
    "agency_name": "Registrar General's Department",
    "agency_short": "",
    "opposition_period": "1 month",
    "registration_period": "4 years",
    "madrid_protocol": false
  },
  "BD": {
    "name": "Bangladesh",
    "agency_name": "Department of Patents Designs and Trademarks Ministry of Industries",
    "agency_short": "DPDT",
    "opposition_period": "1 month",
    "registration_period": "6 years",
    "madrid_protocol": false
  },
  "BZ": {
    "name": "Belize",
    "agency_name": "Belize Intellectual Property Office",
    "agency_short": "BELIPO",
    "opposition_period": "3 months",
    "registration_period": "6 months",
    "madrid_protocol": false
  },
  "BO": {
    "name": "Bolivia",
    "agency_name": "Servicio Nacional de Propriedad Intelectual",
    "agency_short": "SENAPI",
    "opposition_period": "1 month",
    "registration_period": "4 months",
    "madrid_protocol": true
  },
  "BN": {
    "name": "Brunei",
    "agency_name": "Brunei Intellectual Property Office",
    "agency_short": "BruIPO",
    "opposition_period": "3 months",
    "registration_period": "1-2 years",
    "madrid_protocol": false
  },
  "BI": {
    "name": "Burundi",
    "agency_name": "Ministry of Trade Industry and Tourism",
    "agency_short": "",
    "opposition_period": "1 month",
    "registration_period": "6 months",
    "madrid_protocol": false
  },
  "KH": {
    "name": "Cambodia",
    "agency_name": "Department of Industrial Property Ministry of Industry and Handicraft",
    "agency_short": "DIP",
    "opposition_period": "3 months",
    "registration_period": "1 year",
    "madrid_protocol": false
  },
  "CV": {
    "name": "Cape Verde",
    "agency_name": "Industrial Property Institute",
    "agency_short": "IPICV",
    "opposition_period": "3 months",
    "registration_period": "6 months",
    "madrid_protocol": false
  },
  "CL": {
    "name": "Chile",
    "agency_name": "National Institute of Industrial Property",
    "agency_short": "INAPI",
    "opposition_period": "1 month",
    "registration_period": "8 months",
    "madrid_protocol": false
  },
  "CR": {
    "name": "Costa Rica",
    "agency_name": "Registro de la Propiedad Industrial Registro Nacional  Ministerio de Justicia y Paz",
    "agency_short": "RNPDIGITAL",
    "opposition_period": "2 months",
    "registration_period": "5-6 months",
    "madrid_protocol": false
  },
  "DM": {
    "name": "Dominica",
    "agency_name": "Companies and Intellectual Property Office Ministry of Tourism and legal Affairs",
    "agency_short": "CIPO",
    "opposition_period": "2 months",
    "registration_period": "5 months - 1 year",
    "madrid_protocol": false
  },
  "DO": {
    "name": "Dominican Republic",
    "agency_name": "National Office of Industrial Property (ONAPI) State Secretariat for Industry and Commerce",
    "agency_short": "",
    "opposition_period": "45 days",
    "registration_period": "1 month",
    "madrid_protocol": false
  },
  "EC": {
    "name": "Ecuador",
    "agency_name": "Instituto Ecuatoriano de Propiedad Industrial Intelectual",
    "agency_short": "I.E.P.I.",
    "opposition_period": "1 month",
    "registration_period": "2 months",
    "madrid_protocol": false
  },
  "SV": {
    "name": "El Salvador",
    "agency_name": "National Center of Registries",
    "agency_short": "CNR",
    "opposition_period": "2 months",
    "registration_period": "7-9 months",
    "madrid_protocol": false
  },
  "ET": {
    "name": "Ethiopia",
    "agency_name": "Ethiopian Intellectual Property Office",
    "agency_short": "EIPO",
    "opposition_period": "2 months",
    "registration_period": "4 months",
    "madrid_protocol": false
  },
  "GZ": {
    "name": "Gaza",
    "agency_name": "Abu-Ghazaleh Intellectual Property",
    "agency_short": "AGIP",
    "opposition_period": "3 months",
    "registration_period": "18-24 months",
    "madrid_protocol": false
  },
  "GH": {
    "name": "Ghana",
    "agency_name": "Registrar General's Department Ministry of Justice",
    "agency_short": "",
    "opposition_period": "2 months",
    "registration_period": "1 year",
    "madrid_protocol": false
  },
  "GD": {
    "name": "Grenada",
    "agency_name": "Intellectual Property Office",
    "agency_short": "CAIPO",
    "opposition_period": "1 months",
    "registration_period": "1 year",
    "madrid_protocol": false
  },
  "GT": {
    "name": "Guatemala",
    "agency_name": "Registry of Intellectual Property Ministry of Economic Affairs",
    "agency_short": "",
    "opposition_period": "2 months",
    "registration_period": "2-3 months",
    "madrid_protocol": false
  },
  "GY": {
    "name": "Guyana",
    "agency_name": "Deeds Registry Ministry of Legal Affairs",
    "agency_short": "",
    "opposition_period": "1 month",
    "registration_period": "1 year",
    "madrid_protocol": false
  },
  "HT": {
    "name": "Haiti",
    "agency_name": "Intellectual Property Service Ministry of Trade and Industry",
    "agency_short": "MCI",
    "opposition_period": "2 months",
    "registration_period": "8-12 months",
    "madrid_protocol": false
  },
  "HN": {
    "name": "Honduras",
    "agency_name": "Directorate General of Intellectual Property",
    "agency_short": "DIGEPIH",
    "opposition_period": "1 month",
    "registration_period": "3 to 4 months",
    "madrid_protocol": false
  },
  "HK": {
    "name": "Hong Kong",
    "agency_name": "Intellectual Property Department",
    "agency_short": "IPD",
    "opposition_period": "3 months",
    "registration_period": "9 to 12 months",
    "madrid_protocol": false
  },
  "ID": {
    "name": "Indonesia",
    "agency_name": "Directorate General of Intellectual Property Rights Ministry of Law and Human Rights",
    "agency_short": "DGIP",
    "opposition_period": "3 months",
    "registration_period": "1.5 years",
    "madrid_protocol": false
  },
  "IQ": {
    "name": "Iraq",
    "agency_name": "Industrial Property Department Central Organization for Standardization & Quality Control Ministry of Planning",
    "agency_short": "COSQC",
    "opposition_period": "3 months",
    "registration_period": "1.5 to 2 years",
    "madrid_protocol": false
  },
  "JM": {
    "name": "Jamaica",
    "agency_name": "Jamaica Intellectual Property Office",
    "agency_short": "JIPO",
    "opposition_period": "2 months",
    "registration_period": "12 to 18 months",
    "madrid_protocol": false
  },
  "JO": {
    "name": "Jordan",
    "agency_name": "Industrial Property Protection Directorate Ministry of Industry and Trade",
    "agency_short": "MIT",
    "opposition_period": "3 months",
    "registration_period": "8-12 months",
    "madrid_protocol": false
  },
  "KO": {
    "name": "Kosovo",
    "agency_name": "Kosovo Intellectual Property",
    "agency_short": "",
    "opposition_period": "3 months",
    "registration_period": "12 months",
    "madrid_protocol": false
  },
  "KW": {
    "name": "Kuwait",
    "agency_name": "Ministry of Trade and Industry Trademarks and Patents Department",
    "agency_short": "",
    "opposition_period": "1 month",
    "registration_period": "8-10 months",
    "madrid_protocol": false
  },
  "LB": {
    "name": "Lebanon",
    "agency_name": "Office of Intellectual Property Department of Intellectual Property",
    "agency_short": "",
    "opposition_period": "?",
    "registration_period": "4-6 months",
    "madrid_protocol": false
  },
  "MO": {
    "name": "Macau",
    "agency_name": "Macau Economic Services - Intellectual Property Department",
    "agency_short": "",
    "opposition_period": "2 months",
    "registration_period": "6-8 months",
    "madrid_protocol": false
  },
  "MY": {
    "name": "Malaysia",
    "agency_name": "Intellectual Property Corporation of Malaysia",
    "agency_short": "MyIPO",
    "opposition_period": "2 months",
    "registration_period": "2 years",
    "madrid_protocol": false
  },
  "MV": {
    "name": "Maldives",
    "agency_name": "Intellectual Property Unit Ministry of Economic Development",
    "agency_short": "",
    "opposition_period": "?",
    "registration_period": "3-4 weeks",
    "madrid_protocol": false
  },
  "MU": {
    "name": "Mauritius",
    "agency_name": "Industrial Property Office",
    "agency_short": "IPO",
    "opposition_period": "2 months",
    "registration_period": "12 months",
    "madrid_protocol": false
  },
  "MM": {
    "name": "Myanmar",
    "agency_name": "Department of Technical and Vocational Education Ministry of Science and Technology",
    "agency_short": "",
    "opposition_period": "?",
    "registration_period": "3 months",
    "madrid_protocol": false
  },
  "NP": {
    "name": "Nepal",
    "agency_name": "Department of Industry Ministry of Industry",
    "agency_short": "DOIND",
    "opposition_period": "3 months",
    "registration_period": "2-3 months",
    "madrid_protocol": false
  },
  "NI": {
    "name": "Nicaragua",
    "agency_name": "Intellectual Property Registry Ministry of Development Industry and Trade",
    "agency_short": "RPI",
    "opposition_period": "2 months",
    "registration_period": "10 months",
    "madrid_protocol": false
  },
  "NG": {
    "name": "Nigeria",
    "agency_name": "Trademarks",
    "agency_short": " Patents and Designs Registry Federal Ministry of Trade and Investment\"",
    "opposition_period": "''",
    "registration_period": "2 months",
    "madrid_protocol": false
  },
  "ST": {
    "name": "Sao Tome and Principe",
    "agency_name": "Industrial Property Institute",
    "agency_short": "SENAPI",
    "opposition_period": "3 months",
    "registration_period": "3 months - 1 year",
    "madrid_protocol": false
  },
  "SC": {
    "name": "Seychelles",
    "agency_name": "Registration Division Department of Legal Affairs President's Office",
    "agency_short": "",
    "opposition_period": "2 months",
    "registration_period": "12-24 months",
    "madrid_protocol": false
  },
  "SR": {
    "name": "Suriname",
    "agency_name": "Bureau of Intellectual Property Ministry of Justice and Police",
    "agency_short": "",
    "opposition_period": "6 months",
    "registration_period": "18 months",
    "madrid_protocol": false
  },
  "TZ": {
    "name": "Tanzania",
    "agency_name": "Trade and Service Marks Office",
    "agency_short": "",
    "opposition_period": "2 months",
    "registration_period": "12-18 months",
    "madrid_protocol": false
  },
  "TH": {
    "name": "Thailand",
    "agency_name": "Department of Intellectual Property Ministry of Commerce",
    "agency_short": "DIP",
    "opposition_period": "3 months",
    "registration_period": "8-24 months",
    "madrid_protocol": false
  },
  "TT": {
    "name": "Trinidad and Tobago",
    "agency_name": "Intellectual Property Office Ministry of Legal Affairs",
    "agency_short": "IPO",
    "opposition_period": "3 months",
    "registration_period": "8 months-2 years",
    "madrid_protocol": false
  },
  "TC": {
    "name": "Turks and Caicos Islands",
    "agency_name": "Intellectual Property Office",
    "agency_short": "IPO",
    "opposition_period": "1 month",
    "registration_period": "8-12 months",
    "madrid_protocol": false
  },
  "AE": {
    "name": "United Arab Emirates",
    "agency_name": "Department of Industrial Property Ministry of Economy",
    "agency_short": "",
    "opposition_period": "1 month",
    "registration_period": "12-15 months",
    "madrid_protocol": false
  },
  "EU": {
    "name": "the European Union",
    "agency_name": "European Union Intellectual Property Office",
    "agency_short": "EUIPO",
    "opposition_period": "three months",
    "registration_period": "seven months",
    "madrid_protocol": true
  },

  "TL": {
    "name": "East Timor",
    "agency_name": "Intellectual Property Office",
    "agency_short": "IPO",
    "opposition_period": "1 month",
    "registration_period": "8-12 months",
    "madrid_protocol": false
  },


  "SB": {
    "name": "Solomon Islands",
    "agency_name": "Intellectual Property Office",
    "agency_short": "IPO",
    "opposition_period": "1 month",
    "registration_period": "8-12 months",
    "madrid_protocol": false
  },


  "JE": {
    "name": "Jersey",
    "agency_name": "Intellectual Property Office",
    "agency_short": "IPO",
    "opposition_period": "1 month",
    "registration_period": "8-12 months",
    "madrid_protocol": false
  },

  

  "BX": {
    "name": "Benelux",
    "agency_name": "Intellectual Property Office",
    "agency_short": "IPO",
    "opposition_period": "1 month",
    "registration_period": "8-12 months",
    "madrid_protocol": false
  },

  


  "SX": {
    "name": "Sint Maarten",
    "agency_name": "Intellectual Property Office",
    "agency_short": "IPO",
    "opposition_period": "1 month",
    "registration_period": "8-12 months",
    "madrid_protocol": false
  },


  "KRD": {
    "name": "Kurdistan Region",
    "agency_name": "Intellectual Property Office",
    "agency_short": "IPO",
    "opposition_period": "1 month",
    "registration_period": "8-12 months",
    "madrid_protocol": false
  },


  "EAZ": {
    "name": "Zanzibar",
    "agency_name": "Intellectual Property Office",
    "agency_short": "IPO",
    "opposition_period": "1 month",
    "registration_period": "8-12 months",
    "madrid_protocol": false
  },


  "MAF": {
    "name": "Saint Martin",
    "agency_name": "Intellectual Property Office",
    "agency_short": "IPO",
    "opposition_period": "1 month",
    "registration_period": "8-12 months",
    "madrid_protocol": false
  },



  "NE": {
    "name": "Nigeria",
    "agency_name": "Intellectual Property Office",
    "agency_short": "IPO",
    "opposition_period": "1 month",
    "registration_period": "8-12 months",
    "madrid_protocol": false
  },



  "CI": {
    "name": "Ivory Coast",
    "agency_name": "Intellectual Property Office",
    "agency_short": "IPO",
    "opposition_period": "1 month",
    "registration_period": "8-12 months",
    "madrid_protocol": false
  },



  "NL": {
    "name": "Netherlands",
    "agency_name": "Intellectual Property Office",
    "agency_short": "IPO",
    "opposition_period": "1 month",
    "registration_period": "8-12 months",
    "madrid_protocol": false
  },



  "LU": {
    "name": "Luxembourg",
    "agency_name": "Intellectual Property Office",
    "agency_short": "IPO",
    "opposition_period": "1 month",
    "registration_period": "8-12 months",
    "madrid_protocol": false
  },


  "VCT": {
    "name": "Saint Vincent and the Grenadines",
    "agency_name": "Intellectual Property Office",
    "agency_short": "IPO",
    "opposition_period": "1 month",
    "registration_period": "8-12 months",
    "madrid_protocol": false
  }






}
},{}],"/home/wang/projects/tks_project/tks/data/eu.json":[function(require,module,exports){
module.exports={
  "AT":"Austria",
  "BG":"Bulgaria",
  "BE":"Belgium",
  "CZ":"Czech Republic",
  "CY":"Cyprus",
  "DK":"Denmark",
  "EE":"Estonia",
  "FI":"Finland",
  "FR":"France",
  "DE":"Germany",
  "GR":"Greece",
  "HU":"Hungary",
  "IE":"Ireland",
  "IT":"Italy",
  "LV":"Latvia",
  "LT":"Lithuania",
  "LU":"Luxembourg",
  "MT":"Malta",
  "NL":"Netherlands",
  "PL":"Poland",
  "PT":"Portugal",
  "RO":"Romania",
  "SL":"Slovakia",
  "SI":"Slovenia",
  "ES":"Spain",
  "SE":"Sweden",
  "GB":"United Kingdom"
}
},{}],"/home/wang/projects/tks_project/tks/data/formats.json":[function(require,module,exports){
module.exports={
  "US": {"input_type": 0, "POA_required": -1, "trade_type": "w", "word_label": "Word mark",  "commerce_use": 0,  "study_type": -1, "study_type2": "0"},
  "IN": {"input_type": 0, "POA_required":-1, "trade_type": "w", "word_label": "Word mark",  "commerce_use": 0, "study_type": -1},
  "SG": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark", "commerce_use": -1, "study_type": -1},
  "GR": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark", "commerce_use": -1, "study_type": -1},
  "LT": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark", "commerce_use": -1, "study_type": -1},
  "HK": {"input_type": 0, "POA_required": -1, "trade_type": "w", "word_label": "Word mark",  "commerce_use": -1, "study_type": -1},
  "JP": {"input_type": 0, "POA_required": -1, "trade_type": "w", "word_label": "Word mark",  "commerce_use": 0, "study_type": -1},
  "TW": {"input_type": 0, "POA_required": 1, "trade_type": "w", "word_label": "Word mark",  "commerce_use": 0, "study_type": -1},
  "AR": {"input_type": 1, "POA_required": 1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "AZ": {"input_type": 1, "POA_required": 1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "AU": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark", "commerce_use": -1, "study_type": -1},
  "NZ": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "GB": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "DE": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "IE": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "AS": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "FJ": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "PG": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "WS": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "TO": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "BH": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "BY": {"input_type": 1, "POA_required": 2, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "BZ": {"input_type": 1, "POA_required": 2, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "BO": {"input_type": 1, "POA_required": 1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "BE": {"input_type": 1, "POA_required": 1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "CA": {"input_type": 0, "POA_required": 1, "trade_type": "w", "word_label": "Word mark",  "commerce_use": 0, "study_type": -1},
  "CO": {"input_type": 1, "POA_required": 1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "EU": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "CH": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "SZ": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark", "commerce_use": -1, "study_type": -1},
  "NO": {"input_type": 1, "POA_required": 1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "MX": {"input_type": 0, "POA_required": 1, "trade_type": "w", "word_label": "Workmark",  "commerce_use": -1, "study_type": -1},
  "BR": {"input_type": 1, "POA_required": 1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "CN": {"input_type": 0, "POA_required": -1, "trade_type": "w", "word_label": "Word mark",  "commerce_use": -1, "study_type": -1},
  "RU": {"input_type": 1, "POA_required": 1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "KR": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark", "commerce_use": -1, "study_type": -1},
  "TH": {"input_type": 1, "POA_required": 1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "PH": {"input_type": 1, "POA_required": 1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "VN": {"input_type": 1, "POA_required": 1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "AF": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "AM": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "BD": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "BT": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "BN": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "KH": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "TL": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "GE": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "ID": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "IQ": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "IL": {"input_type": 0, "POA_required": 1, "trade_type": "w", "word_label": "Word mark",  "commerce_use": 0, "study_type": -1, "default":"w"},
  "JO": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "KW": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "KG": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "LA": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "LB": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "MO": {"input_type": 0, "POA_required": 1, "trade_type": "w", "word_label": "Word mark",  "commerce_use": 0, "study_type": -1, "default":"w"},
  "MY": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "MV": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "MN": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "MM": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "NP": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "OM": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "PK": {"input_type": 0, "POA_required": 1, "trade_type": "w", "word_label": "Word mark",  "commerce_use": 0, "study_type": -1, "default":"w"},
  "PS": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "QA": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "SA": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "LK": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "TJ": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "TR": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "TM": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "AE": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "UZ": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "YE": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "TG": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "AL": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "AD": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "AT": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "BA": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "BG": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "HR": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "CY": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "CZ": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "DK": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "EE": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "FI": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "FR": {"input_type": 0, "POA_required": 1, "trade_type": "w", "word_label": "Word mark",  "commerce_use": 0, "study_type": -1, "default":"w"},
  "GI": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "GG": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "HU": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "IS": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "IT": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "LV": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "LI": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "MK": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "MT": {"input_type": 0, "POA_required": 1, "trade_type": "w", "word_label": "Word mark",  "commerce_use": 0, "study_type": -1, "default":"w"},
  "MD": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "MC": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "PL": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "PT": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "RO": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "SM": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "SK": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "SI": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "ES": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "SE": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "UA": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "AI": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "AG": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "AW": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "BB": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "BM": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "VG": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "KY": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "CL": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "CR": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "CW": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "DM": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "DO": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "EC": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "SV": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "GD": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "GT": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "GY": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "HT": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "HN": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "JM": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "MS": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "NI": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "PA": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "PY": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "PE": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "PR": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "VC": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "KN": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "LC": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "SR": {"input_type": 0, "POA_required": 1, "trade_type": "w", "word_label": "Word mark",  "commerce_use": 0, "study_type": -1, "default":"w"},
  "TT": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "UY": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "VE": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "DZ": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "AO": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "BI": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "CV": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "CG": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "DJ": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "EG": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "ER": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "ET": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "GM": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "GH": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "KE": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "LR": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "LY": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "MG": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "MW": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "MU": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "MA": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "MZ": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "NA": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "RW": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "ST": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "SC": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "SL": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "ZA": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "TZ": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "TN": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "UG": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "ZM": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "ZW": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "RS": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "ME": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "BS": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "KO": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "TC": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "SB": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},  
  "LS": {"input_type": 0, "POA_required": -1, "trade_type": "w", "word_label": "Word mark",  "commerce_use": -1,  "study_type": -1, "study_type2": "0"},
  "JE": {"input_type": 0, "POA_required": -1, "trade_type": "w", "word_label": "Word mark",  "commerce_use": -1,  "study_type": -1, "study_type2": "0"},
  "BX": {"input_type": 0, "POA_required": -1, "trade_type": "w", "word_label": "Word mark",  "commerce_use": -1,  "study_type": -1, "study_type2": "0"},
  "SX": {"input_type": 0, "POA_required": -1, "trade_type": "w", "word_label": "Word mark",  "commerce_use": -1,  "study_type": -1, "study_type2": "0"},
  "NE": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "CI": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "NL": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "LU": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "OAPI": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "KRD": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "EAZ": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "VCT": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "MAF": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "ARIPO": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"}
}
},{}],"/home/wang/projects/tks_project/tks/data/oapi.json":[function(require,module,exports){
module.exports={
  "BJ":"Benin",
  "BF":"Burkina Faso",
  "CM":"Cameroon",
  "CF":"Central African",
  "TD":"Chad",
  "KM":"Comoros",
  "CD":"Congo Republic",
  "GQ":"Equatorial Guinea",
  "GA":"Gabon",
  "GN":"Guinea",
  "GW":"Guinea-Bissau",
  "ML":"Mali",
  "MR":"Mauritania",
  "NG":"Nigeria",
  "SN":"Senegal",
  "TO":"Tonga"
}
},{}],"/home/wang/projects/tks_project/tks/data/organizations.json":[function(require,module,exports){
module.exports={
  "OAPI":["OAPI treaty includes trademark protection in the following countries: <b>Benin, Burkina Faso, Cameroon, Central African Republic, Chad, Comoros, Congo Republic, Equatorial Guinea, Gabon, Guinea, Guinea-Bissau, Ivory Coast, Mali, Mauritania, Niger, Senegal, and Togo.</b>"],
  "ARIPO":  [ "ARIPO treaty  includes trademark protection in the following countries: <b>Botswana, Lesotho, Liberia, Malawi, Namibia, Swaziland, Tanzania, Uganda and Zimbabwe.</b>",
    "In order to obtain trademark protection in _country_ you can register your trademark in two ways: <br/> <b>First option</b> is that you request registration for the ARIPO trademark; ARIPO treaty includes trademark protection in <b>Botswana, Lesotho, Liberia, Malawi, Namibia, Rwanda, Swaziland, Tanzania, Uganda and Zimbabwe.</b> To proceed with trademark application in ARIPO, click <a href='/trademark/ARIPO'>here</a>.<br/>  <b>Second option</b> is that you register directly your trademark in _country_. If you want to proceed this way please follow the steps described below."],
  "EU":[
          "The Community Trademark grants protection over the entire European Union with one registration. This Trademark Registration includes protection in: <b>Austria, Bulgaria, Belgium, Czech Republic, Cyprus, Denmark, Estonia, Finland, France, Germany, Greece, Hungary, Ireland, Italy, Latvia, Lithuania, Luxembourg, Malta, The Netherlands, Poland, Portugal, Romania, Slovakia, Slovenia, Spain, Sweden and United Kingdom.</b>",
          "<p>In order to obtain trademark protection in <b>_country_</b> you can register your trademark in two ways:</p><p><b>First option</b> is that you request registration in the entire European Union with one single application; this can be done via the Community Trademark agreement which grants trademark protection in the 27 country members of the EU. <a href='/trademark/EU'>Click here</a>.</p><p><b>Second option</b> is that you register directly your trademark in <b>_country_</b>. If you want to proceed this way please follow the steps described below:</p>"
  ]
}
},{}],"/home/wang/projects/tks_project/tks/node_modules/browserify/node_modules/process/browser.js":[function(require,module,exports){
// shim for using process in browser

var process = module.exports = {};

process.nextTick = (function () {
    var canSetImmediate = typeof window !== 'undefined'
    && window.setImmediate;
    var canPost = typeof window !== 'undefined'
    && window.postMessage && window.addEventListener
    ;

    if (canSetImmediate) {
        return function (f) { return window.setImmediate(f) };
    }

    if (canPost) {
        var queue = [];
        window.addEventListener('message', function (ev) {
            var source = ev.source;
            if ((source === window || source === null) && ev.data === 'process-tick') {
                ev.stopPropagation();
                if (queue.length > 0) {
                    var fn = queue.shift();
                    fn();
                }
            }
        }, true);

        return function nextTick(fn) {
            queue.push(fn);
            window.postMessage('process-tick', '*');
        };
    }

    return function nextTick(fn) {
        setTimeout(fn, 0);
    };
})();

process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;

process.binding = function (name) {
    throw new Error('process.binding is not supported');
}

// TODO(shtylman)
process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};

},{}],"/home/wang/projects/tks_project/tks/node_modules/countries/countries.json":[function(require,module,exports){
module.exports=[ 
  {"name":"Afghanistan","code":"AF","continent":"ME"},
  {"name":"Åland Islands","code":"AX","continent":"EU"},
  {"name":"Albania","code":"AL","continent":"EU"},
  {"name":"Algeria","code":"DZ","continent":"AF"},
  {"name":"American Samoa","code":"AS","continent":"OC"},
  {"name":"Andorra","code":"AD","continent":"EU"},
  {"name":"Angola","code":"AO","continent":"AF"},
  {"name":"Anguilla","code":"AI","continent":"AF"},
  {"name":"Antarctica","code":"AQ","continent":"AN"},
  {"name":"Antigua and Barbuda","code":"AG","continent":"CA"},
  {"name":"Argentina","code":"AR","continent":"SA"},
  {"name":"Armenia","code":"AM","continent":"EU"},
  {"name":"Aruba","code":"AW","continent":"CA"},
  {"name":"Australia","code":"AU","continent":"OC"},
  {"name":"Austria","code":"AT","continent":"EU"},
  {"name":"Azerbaijan","code":"AZ","continent":"EU"},
  {"name":"Bahamas","code":"BS","continent":"CA"},
  {"name":"Bahrain","code":"BH","continent":"ME"},
  {"name":"Bangladesh","code":"BD","continent":"AS"},
  {"name":"Barbados","code":"BB","continent":"CA"},
  {"name":"Belarus","code":"BY","continent":"EU"},
  {"name":"Belgium","code":"BE","continent":"EU"},
  {"name":"Benelux","code":"BX","continent":"EU"},
  {"name":"Belize","code":"BZ","continent":"CA"},
  {"name":"Benin","code":"BJ","continent":"AF"},
  {"name":"Bermuda","code":"BM","continent":"AS"},
  {"name":"Bhutan","code":"BT","continent":"AS"},
  {"name":"Bolivia","code":"BO","continent":"SA"},
  {"name":"Bosnia","code":"BA","continent":"EU"},
  {"name":"Botswana","code":"BW","continent":"AF"},
  {"name":"Bouvet Island","code":"BV","continent":"AN"},
  {"name":"Brazil","code":"BR","continent":"SA"},
  {"name":"British Indian","code":"IO","continent":"AS"},
  {"name":"Brunei Darussalam","code":"BN","continent":"AS"},
  {"name":"Bulgaria","code":"BG","continent":"EU"},
  {"name":"Burkina Faso","code":"BF","continent":"AF"},
  {"name":"Burundi","code":"BI","continent":"AF"},
  {"name":"Cambodia","code":"KH","continent":"AS"},
  {"name":"Cameroon","code":"CM","continent":"AF"},
  {"name":"Canada","code":"CA","continent":"NA"},
  {"name":"Cape Verde","code":"CV","continent":"AF"},
  {"name":"Cayman Islands","code":"KY","continent":"CA"},
  {"name":"Central African Republic","code":"CF","continent":"AF"},
  {"name":"Chad","code":"TD","continent":"AF"},
  {"name":"Chile","code":"CL","continent":"SA"},
  {"name":"China","code":"CN","continent":"AS"},
  {"name":"Christmas Island","code":"CX","continent":"AS"},
  {"name":"Cocos (Keeling)","code":"CC","continent":"AS"},
  {"name":"Colombia","code":"CO","continent":"SA"},
  {"name":"Comoros","code":"KM","continent":"AF"},
  {"name":"Republic of the Congo","code":"CG","continent":"AF"},
  {"name":"Democratic Republic of the Congo","code":"CD","continent":"AF"},
  {"name":"Cook Islands","code":"CK","continent":"OC"},
  {"name":"Costa Rica","code":"CR","continent":"CA"},
  {"name": "Ivory Coast", "code": "CI","continent":"AF"},
  {"name":"Croatia","code":"HR","continent":"EU"},
  {"name":"Cuba","code":"CU","continent":"CA"},
  {"name":"Cyprus","code":"CY","continent":"EU"},
  {"name":"Czech Republic","code":"CZ","continent":"EU"},
  {"name":"Denmark","code":"DK","continent":"EU"},
  {"name":"Djibouti","code":"DJ","continent":"AF"},
  {"name":"Dominica","code":"DM","continent":"CA"},
  {"name":"Dominican Republic","code":"DO","continent":"CA"},
  {"name":"Ecuador","code":"EC","continent":"SA"},
  {"name":"Egypt","code":"EG","continent":"AF"},
  {"name":"El Salvador","code":"SV","continent":"CA"},
  {"name":"Equatorial Guinea","code":"GQ","continent":"AF"},
  {"name":"Eritrea","code":"ER","continent":"AF"},
  {"name":"Estonia","code":"EE","continent":"EU"},
  {"name":"Ethiopia","code":"ET","continent":"AF"},
  {"name":"European Union","code":"EU","continent":"EU"},
  {"name":"Falkland Islands","code":"FK","continent":"SA"},
  {"name":"Faroe Islands","code":"FO","continent":"EU"},
  {"name":"Fiji","code":"FJ","continent":"OC"},
  {"name":"Finland","code":"FI","continent":"EU"},
  {"name":"France","code":"FR","continent":"EU"},
  {"name":"French Guiana","code":"GF","continent":"AF"},
  {"name":"French Polynesia","code":"PF","continent":"AF"},
  {"name":"French Southern","code":"TF","continent":"AN"},
  {"name":"Gabon","code":"GA","continent":"AF"},
  {"name":"Gambia","code":"GM","continent":"AF"},
  {"name":"Georgia","code":"GE","continent":"EU"},
  {"name":"Germany","code":"DE","continent":"EU"},
  {"name":"Ghana","code":"GH","continent":"AF"},
  {"name":"Gibraltar","code":"GI","continent":"EU"},
  {"name":"Greece","code":"GR","continent":"EU"},
  {"name":"Greenland","code":"GL","continent":"CA"},
  {"name":"Grenada","code":"GD","continent":"CA"},
  {"name":"Guadeloupe","code":"GP","continent":"CA"},
  {"name":"Guam","code":"GU","continent":"OC"},
  {"name":"Guatemala","code":"GT","continent":"CA"},
  {"name":"Guernsey","code":"GG","continent":"EU"},
  {"name":"Guinea","code":"GN","continent":"AF"},
  {"name":"Guinea-Bissau","code":"GW","continent":"AF"},
  {"name":"Guyana","code":"GY","continent":"SA"},
  {"name":"Haiti","code":"HT","continent":"CA"},
  {"name":"Heard and Mcdonald","code":"HM","continent":"AN"},
  {"name":"Holy See","code":"VA","continent":"EU"},
  {"name":"Honduras","code":"HN","continent":"CA"},
  {"name":"Hong Kong","code":"HK","continent":"AS"},
  {"name":"Hungary","code":"HU","continent":"EU"},
  {"name":"Iceland","code":"IS","continent":"EU"},
  {"name":"India","code":"IN","continent":"AS"},
  {"name":"Indonesia","code":"ID","continent":"AS"},
  {"name":"Iran","code":"IR","continent":"AS"},
  {"name":"Iraq","code":"IQ","continent":"ME"},
  {"name":"Ireland","code":"IE","continent":"EU"},
  {"name":"Isle of Man","code":"IM","continent":"EU"},
  {"name":"Israel","code":"IL","continent":"ME"},
  {"name":"Italy","code":"IT","continent":"EU"},
  {"name":"Jamaica","code":"JM","continent":"CA"},
  {"name":"Japan","code":"JP","continent":"AS"},
  {"name":"Jersey","code":"JE","continent":"EU"},
  {"name":"Jordan","code":"JO","continent":"ME"},
  {"name":"Kazakhstan","code":"KZ","continent":"AS"},
  {"name":"Kenya","code":"KE","continent":"AF"},
  {"name":"Kiribati","code":"KI","continent":"OC"},
  {"name":"North Korea","code":"KP","continent":"AS"},
  {"name":"South Korea","code":"KR","continent":"AS"},
  {"name":"Kosovo","code":"KO","continent":"EU"},
  {"name":"Kuwait","code":"KW","continent":"ME"},
  {"name":"Kyrgyzstan","code":"KG","continent":"AS"},
  {"name":"Laos","code":"LA","continent":"AS"},
  {"name":"Latvia","code":"LV","continent":"EU"},
  {"name":"Lebanon","code":"LB","continent":"ME"},
  {"name":"Lesotho","code":"LS","continent":"AF"},
  {"name":"Liberia","code":"LR","continent":"AF"},
  {"name":"Libyan","code":"LY","continent":"AF"},
  {"name":"Liechtenstein","code":"LI","continent":"EU"},
  {"name":"Lithuania","code":"LT","continent":"EU"},
  {"name":"Luxembourg","code":"LU","continent":"EU"},
  {"name":"Macao","code":"MO","continent":"AS"},
  {"name":"Macedonia","code":"MK","continent":"EU"},
  {"name":"Madagascar","code":"MG","continent":"AF"},
  {"name":"Malawi","code":"MW","continent":"AF"},
  {"name":"Malaysia","code":"MY","continent":"AS"},
  {"name":"Maldives","code":"MV","continent":"AS"},
  {"name":"Mali","code":"ML","continent":"AF"},
  {"name":"Malta","code":"MT","continent":"EU"},
  {"name":"Marshall Islands","code":"MH","continent":"OC"},
  {"name":"Martinique","code":"MQ","continent":"CA"},
  {"name":"Mauritania","code":"MR","continent":"AF"},
  {"name":"Mauritius","code":"MU","continent":"AF"},
  {"name":"Mayotte","code":"YT","continent":"AF"},
  {"name":"Mexico","code":"MX","continent":"NA"},
  {"name":"Micronesia","code":"FM","continent":"OC"},
  {"name":"Moldova","code":"MD","continent":"EU"},
  {"name":"Monaco","code":"MC","continent":"EU"},
  {"name":"Mongolia","code":"MN","continent":"AS"},
  {"name":"Montserrat","code":"MS","continent":"CA"},
  {"name":"Morocco","code":"MA","continent":"AF"},
  {"name":"Mozambique","code":"MZ","continent":"AF"},
  {"name":"Myanmar","code":"MM","continent":"AS"},
  {"name":"Namibia","code":"NA","continent":"AF"},
  {"name":"Nauru","code":"NR","continent":"OC"},
  {"name":"Nepal","code":"NP","continent":"AS"},
  {"name":"Netherlands","code":"NL","continent":"EU"},
  {"name":"Netherlands Antilles","code":"AN","continent":"EU"},
  {"name":"New Caledonia","code":"NC","continent":"OC"},
  {"name":"New Zealand","code":"NZ","continent":"OC"},
  {"name":"Nicaragua","code":"NI","continent":"CA"},
  {"name":"Niger","code":"NE","continent":"AF"},
  {"name":"Nigeria","code":"NG","continent":"AF"},
  {"name":"Niue","code":"NU","continent":"OC"},
  {"name":"Norfolk Island","code":"NF","continent":"OC"},
  {"name":"Northern Mariana","code":"MP","continent":"OC"},
  {"name":"Norway","code":"NO","continent":"EU"},
  {"name":"Oman","code":"OM","continent":"ME"},
  {"name":"Pakistan","code":"PK","continent":"AS"},
  {"name":"Palau","code":"PW","continent":"OC"},
  {"name":"Palestine","code":"PS","continent":"ME"},
  {"name":"Panama","code":"PA","continent":"CA"},
  {"name":"Papua New Guinea","code":"PG","continent":"AF"},
  {"name":"Paraguay","code":"PY","continent":"SA"},
  {"name":"Peru","code":"PE","continent":"SA"},
  {"name":"Philippines","code":"PH","continent":"AS"},
  {"name":"Pitcairn","code":"PN","continent":"OC"},
  {"name":"Poland","code":"PL","continent":"EU"},
  {"name":"Portugal","code":"PT","continent":"EU"},
  {"name":"Puerto Rico","code":"PR","continent":"CA"},
  {"name":"Qatar","code":"QA","continent":"ME"},
  {"name":"Reunion","code":"RE","continent":"AF"},
  {"name":"Romania","code":"RO","continent":"EU"},
  {"name":"Russian Federation","code":"RU","continent":"EU"},
  {"name":"RWANDA","code":"RW","continent":"AF"},
  {"name":"Saint Helena","code":"SH","continent":"AF"},
  {"name":"Saint Kitts and Nevis","code":"KN","continent":"CA"},
  {"name":"Saint Lucia","code":"LC","continent":"CA"},
  {"name":"Saint Pierre and Miquelon","code":"PM","continent":"CA"},
  {"name":"Saint Vincent","code":"VC","continent":"CA"},
  {"name":"Samoa","code":"WS","continent":"OC"},
  {"name":"San Marino","code":"SM","continent":"EU"},
  {"name":"Sao Tome and Principe","code":"ST","continent":"AF"},
  {"name":"Saudi Arabia","code":"SA","continent":"ME"},
  {"name":"Senegal","code":"SN","continent":"AF"},
  {"name":"Serbia and Montenegro","code":"CS","continent":"EU"},
  {"name":"Serbia","code":"RS","continent":"EU"},
  {"name":"Montenegro","code":"ME","continent":"EU"},
  {"name":"Seychelles","code":"SC","continent":"AF"},
  {"name":"Sierra Leone","code":"SL","continent":"AF"},
  {"name":"Sint Maarten","code":"SX","continent":"EU"},
  {"name":"Singapore","code":"SG","continent":"AS"},
  {"name":"Slovakia","code":"SK","continent":"EU"},
  {"name":"Slovenia","code":"SI","continent":"EU"},
  {"name":"Solomon Islands","code":"SB","continent":"OC"},
  {"name":"Somalia","code":"SO","continent":"AF"},
  {"name":"South Africa","code":"ZA","continent":"AF"},
  {"name":"South Georgia","code":"GS","continent":"EU"},
  {"name":"Spain","code":"ES","continent":"EU"},
  {"name":"Sri Lanka","code":"LK","continent":"AS"},
  {"name":"Sudan","code":"SD","continent":"AF"},
  {"name":"Suri name","code":"SR","continent":"SA"},
  {"name":"Svalbard and Jan Mayen","code":"SJ","continent":"EU"},
  {"name":"Swaziland","code":"SZ","continent":"AF"},
  {"name":"Sweden","code":"SE","continent":"EU"},
  {"name":"Switzerland","code":"CH","continent":"EU"},
  {"name":"Syrian Arab Republic","code":"SY","continent":"AS"},
  {"name":"Taiwan","code":"TW","continent":"AS"},
  {"name":"Tajikistan","code":"TJ","continent":"AS"},
  {"name":"Tanzania","code":"TZ","continent":"AF"},
  {"name":"Thailand","code":"TH","continent":"AS"},
  {"name":"East Timor","code":"TL","continent":"AS"},
  {"name":"Togo","code":"TG","continent":"AF"},
  {"name":"Tokelau","code":"TK","continent":"OC"},
  {"name":"Tonga","code":"TO","continent":"OC"},
  {"name":"Trinidad and Tobago","code":"TT","continent":"CA"},
  {"name":"Tunisia","code":"TN","continent":"AF"},
  {"name":"Turkey","code":"TR","continent":"ME"},
  {"name":"Turkmenistan","code":"TM","continent":"AS"},
  {"name":"Turks and Caicos Islands","code":"TC","continent":"CA"},
  {"name":"Tuvalu","code":"TV","continent":"OC"},
  {"name":"Uganda","code":"UG","continent":"AF"},
  {"name":"Ukraine","code":"UA","continent":"EU"},
  {"name":"United Arab Emirates","code":"AE","continent":"ME"},
  {"name":"United Kingdom","code":"GB","continent":"EU"},
  {"name":"United States","code":"US","continent":"NA"},
  {"name":"Uruguay","code":"UY","continent":"SA"},
  {"name":"Uzbekistan","code":"UZ","continent":"AS"},
  {"name":"Vanuatu","code":"VU","continent":"OC"},
  {"name":"Venezuela","code":"VE","continent":"SA"},
  {"name":"Vietnam","code":"VN","continent":"AS"},
  {"name":"British Virgin Islands","code":"VG","continent":"CA"},
  {"name":"Virgin Islands, U.S.","code":"VI","continent":"CA"},
  {"name":"Wallis and Futuna","code":"WF","continent":"OC"},
  {"name":"Western Sahara","code":"EH","continent":"AF"},
  {"name":"Yemen","code":"YE","continent":"ME"},
  {"name":"Zambia","code":"ZM","continent":"AF"},
  {"name":"Zimbabwe","code":"ZW","continent":"AF"},
  {"name":"ARIPO Treaty","code":"ARIPO","continent":"AF"},
  {"name":"OAPI Treaty","code":"OAPI","continent":"AF"},
  {"name":"Kurdistan Region","code":"KRD","continent":"ME"},
  {"name":"Zanzibar","code":"EAZ","continent":"AF"},
  {"name":"Saint Martin","code":"MAF","continent":"EU"}, 
  {"name":"Saint Vincent and the Grenadines","code":"VCT","continent":"OC"}
]

},{}],"/home/wang/projects/tks_project/tks/node_modules/countries/countries.js":[function(require,module,exports){
var countries = require('./countries.json')
var states = require('./states.json')

//console.log(countries)
/*
 AF	Africa
 AN	Antarctica
 AS	Asia
 EU	Europe
 NA	North america
 OC	Oceania
 SA	South america
 */

function all_countries() {
  return countries
}

function get_country(code) {
  for (var i = 0; i < countries.length; i++) {
    if (countries[i].code === code) {
      return countries[i]
    }
  }
}

var country_alias = {
  "THE GAMBIA": "GAMBIA",
  "GUINEA BISSAU": "GUINEA-BISSAU",
  "SAINT VINCENT AND GRENADINES": "SAINT VINCENT",
  "PALESTINIAN TERRITORY": "PALESTINE"
}

function get_country_by_name(name) {
  var kname = name.replace(/-|_|%20/g, ' ').toUpperCase() // handle American-Samoa or Am

  console.log(country_alias[kname], kname)
  if (country_alias[kname])
    kname = country_alias[kname]


  for (var i = 0; i < countries.length; i++) {
    if (countries[i].name.toUpperCase() === kname)
      return countries[i]
  }
}

// get_country_by_regex(/^\/registration-in-(\w+)/i, '/registration-in-China')

function get_country_by_regex(regx, string) {

  var rslt = regx.exec(string)
  if (rslt === null)
    return undefined
  else {
    return get_country_by_name(rslt[1].replace(/_/g, ' '))
  }
}




// if param is undefined, all return
// if param is an array, only those in the list returned
function get_countries(param) {
  if (param === undefined)
    return countries
  else {
    var ret = []
    for (var i = 0; i < param.length; i++) {
      var country = get_country(param[i])
      if (country !== undefined)
        ret.push(country)
    }
    return ret
  }
}

function get_countries_by_contnent(code) {
  return countries.filter(function (value, index, ar) {
    return value.continent === code
  })
}


//console.log(get_countries(['US', 'CN','xx']))

exports.get_countries = function (aParam) {
  return get_countries(aParam)
}


exports.get_country = function (aParam) {
  return get_country(aParam)
}


exports.get_country_by_name = function (name) {
  return get_country_by_name(name)
}


exports.get_country_by_regex = function (regx, string) {
  return get_country_by_regex(regx, string)
}


exports.get_countries_by_contnent = function (code) {
  return get_countries_by_contnent(code)
}

// returns states of the country_code

exports.get_states = function (code) {
  return states[code]
}


exports.get_state_name = function (code) {
  var country = code.substring(0, 2)
  console.log('country', country)
  if (states[country])
    return states[country][code]
  else
    return undefined
}





},{"./countries.json":"/home/wang/projects/tks_project/tks/node_modules/countries/countries.json","./states.json":"/home/wang/projects/tks_project/tks/node_modules/countries/states.json"}],"/home/wang/projects/tks_project/tks/node_modules/countries/states.json":[function(require,module,exports){
module.exports={
  "US": {
    "US.AL": "Alabama",
    "US.AK": "Alaska",
    "US.AZ": "Arizona",
    "US.AR": "Arkansas",
    "US.CA": "California",
    "US.CO": "Colorado",
    "US.CT": "Connecticut",
    "US.DE": "Delaware",
    "US.DC": "District of Columbia",
    "US.FL": "Florida",
    "US.GA": "Georgia",
    "US.HI": "Hawaii",
    "US.ID": "Idaho",
    "US.IL": "Illinois",
    "US.IN": "Indiana",
    "US.IA": "Iowa",
    "US.KS": "Kansas",
    "US.KY": "Kentucky",
    "US.LA": "Louisiana",
    "US.ME": "Maine",
    "US.MD": "Maryland",
    "US.MA": "Massachusetts",
    "US.MI": "Michigan",
    "US.MN": "Minnesota",
    "US.MS": "Mississippi",
    "US.MO": "Missouri",
    "US.MT": "Montana",
    "US.NE": "Nebraska",
    "US.NV": "Nevada",
    "US.NH": "New Hampshire",
    "US.NJ": "New Jersey",
    "US.NM": "New Mexico",
    "US.NY": "New York",
    "US.NC": "North Carolina",
    "US.ND": "North Dakota",
    "US.OH": "Ohio",
    "US.OK": "Oklahoma",
    "US.OR": "Oregon",
    "US.PA": "Pennsylvania",
    "US.RI": "Rhode Island",
    "US.SC": "South Carolina",
    "US.SD": "South Dakota",
    "US.TN": "Tennessee",
    "US.TX": "Texas",
    "US.UT": "Utah",
    "US.VT": "Vermont",
    "US.VA": "Virginia",
    "US.WA": "Washington",
    "US.WV": "West Virginia",
    "US.WI": "Wisconsin",
    "US.WY": "Wyoming"
  },
  "CN" : {
    "CN.AH": "Anhui",
    "CN.BJ": "Beijing",
    "CN.CQ": "Chongqing",
    "CN.FJ": "Fujian",
    "CN.GS": "Gansu",
    "CN.GD": "Guandong",
    "CN.GX": "Guangxi",
    "CN.HA": "Hainan",
    "CN.HB": "Hebei",
    "CN.HL": "Heilongjian",
    "CN.HA": "Henan",
    "CN.HB": "Hubei",
    "CN.HN": "Hunan",
    "CN.JS": "Jiangsu",
    "CN.JX": "Jiangxi",
    "CN.JL": "Jilin",
    "CN.LN": "Liaoning",
    "CN.NM": "Nei Mongol",
    "CN.NX": "Ningxia",
    "CN.QH": "Qinghai",
    "CN.SA": "Shaanxi",
    "CN.SD": "Shandong",
    "CN.SH": "Shanghai",
    "CN.SX": "Shanxi",
    "CN.SC": "Sichuan",
    "CN.TJ": "Tianjin",
    "CN.XJ": "Xinjuan",
    "CN.XZ": "Xizang",
    "CN.YN": "Yunnan",
    "CN.ZJ": "Zhejian"
  },
  "HK" : {
    "HK.CW": "Central and Western",
    "HK.EA": "Eastern",
    "HK.IS": "Islands",
    "HK.KC": "Kowloon City",
    "HK.KI": "Kwai Tsing",
    "HK.KU": "Kwun Tong",
    "HK.NO": "North",
    "HK.SK": "Sai Kung",
    "HK.SS": "Sham Shui Po",
    "HK.ST": "Sha Tin",
    "HK.SO": "Southern",
    "HK.TP": "Tai Po",
    "HK.TW": "Tsuen Wan",
    "HK.TM": "Tuen Mun",
    "HK.WC": "Wan Chai",
    "HK.WT": "Wong Tai Sin",
    "HK.YT": "Yau Tsim Mong",
    "HK.YL": "Yuen Long"
  },
  "PH" : {
    "PH.AB": "Abra",
    "PH.AN": "Agusan del Norte",
    "PH.AS": "Agusan del Sur",
    "PH.AK": "Aklan",
    "PH.AL": "Albay",
    "PH.AQ": "Antique",
    "PH.AP": "Apayao",
    "PH.AU": "Aurora",
    "PH.BS": "Basilan",
    "PH.BA": "Bataan",
    "PH.BN": "Batanes",
    "PH.BT": "Batangas",
    "PH.BG": "Benguet",
    "PH.BI": "Biliran",
    "PH.BO": "Bohol",
    "PH.BK": "Bukidnon",
    "PH.BU": "Bulacan",
    "PH.CG": "Cagayan",
    "PH.CN": "Camarines Norte",
    "PH.CS": "Camarines Sur",
    "PH.CM": "Camiguin",
    "PH.CP": "Capiz",
    "PH.CT": "Catanduanes",
    "PH.CV": "Cavite",
    "PH.CB": "Cebu",
    "PH.CL": "Compostela Valley",
    "PH.NC": "Cotabato",
    "PH.DV": "Davao del Norte",
    "PH.DS": "Davao del Sur",
    "PH.DO": "Davao Oriental",
    "PH.DI": "Dinagat Islands",
    "PH.ES": "Eastern Samar",
    "PH.GU": "Guimaras",
    "PH.IF": "Ifugao",
    "PH.IN": "Ilocos Norte",
    "PH.IS": "Ilocos Sur",
    "PH.II": "Iloilo",
    "PH.IB": "Isabela",
    "PH.KA": "Kalinga",
    "PH.LG": "Laguna",
    "PH.LN": "Lanao del Norte",
    "PH.LS": "Lanao del Sur",
    "PH.LU": "La Union",
    "PH.LE": "Leyte",
    "PH.MG": "Maguindanao",
    "PH.MQ": "Marinduque",
    "PH.MB": "Masbate",
    "PH.MM": "Metropolitan Manila",
    "PH.MD": "Misamis Occidental",
    "PH.MN": "Misamis Oriental",
    "PH.MT": "Mountain",
    "PH.ND": "Negros Occidental",
    "PH.NR": "Negros Oriental",
    "PH.NS": "Northern Samar",
    "PH.NE": "Nueva Ecija",
    "PH.NV": "Nueva Vizcaya",
    "PH.MC": "Occidental Mindoro",
    "PH.MR": "Oriental Mindoro",
    "PH.PL": "Palawan",
    "PH.PM": "Pampanga",
    "PH.PN": "Pangasinan",
    "PH.QZ": "Quezon",
    "PH.QR": "Quirino",
    "PH.RI": "Rizal",
    "PH.RO": "Romblon",
    "PH.SM": "Samar",
    "PH.SG": "Sarangani",
    "PH.SQ": "Siquijor",
    "PH.SR": "Sorsogon",
    "PH.SC": "South Cotabato",
    "PH.SL": "Southern Leyte",
    "PH.SK": "Sultan Kudarat",
    "PH.SU": "Sulu",
    "PH.ST": "Surigao del Norte",
    "PH.SS": "Surigao del Sur",
    "PH.TR": "Tarlac",
    "PH.TT": "Tawi-Tawi",
    "PH.ZM": "Zambales",
    "PH.ZN": "Zamboanga del Norte",
    "PH.ZS": "Zamboanga del Sur",
    "PH.ZY": "Zamboanga-Sibugay"
  },
  "CA" : {
    "CA.AB": "Alberta",
    "CA.BC": "British Columbia",
    "CA.MB": "Manitoba",
    "CA.NB": "New Brunswick",
    "CA.NF": "Newfoundland and Labrador",
    "CA.NT": "Northwest Territories",
    "CA.NS": "Nova Scotia",
    "CA.NU": "Nunavut",
    "CA.ON": "Ontario",
    "CA.PE": "Prince Edward Island",
    "CA.QC": "Quebec",
    "CA.SK": "Saskatchewan",
    "CA.YT": "Yukon Territory"
  },
  "TH" : {
    "TH.AC": "Amnat Charoen",
    "TH.AT": "Ang Thong",
    "TH.BM": "Bangkok Metropolis",
    "TH.BR": "Buri Ram",
    "TH.CC": "Chachoengsao",
    "TH.CN": "Chai Nat",
    "TH.CY": "Chaiyaphum",
    "TH.CT": "Chanthaburi",
    "TH.CM": "Chiang Mai",
    "TH.CR": "Chiang Rai",
    "TH.CB": "Chon Buri",
    "TH.CP": "Chumphon",
    "TH.KL": "Kalasin",
    "TH.KP": "Kamphaeng Phet",
    "TH.KN": "Kanchanaburi",
    "TH.KK": "Khon Kaen",
    "TH.KR": "Krabi",
    "TH.LG": "Lampang",
    "TH.LN": "Lamphun",
    "TH.LE": "Loei",
    "TH.LB": "Lop Buri",
    "TH.MH": "Mae Hong Son",
    "TH.MS": "Maha Sarakham",
    "TH.MD": "Mukdahan",
    "TH.NN": "Nakhon Nayok",
    "TH.NP": "Nakhon Pathom",
    "TH.NF": "Nakhon Phanom",
    "TH.NR": "Nakhon Ratchasima",
    "TH.NS": "Nakhon Sawan",
    "TH.NT": "Nakhon Si Thammarat",
    "TH.NA": "Nan",
    "TH.NW": "Narathiwat",
    "TH.NB": "Nong Bua Lam Phu",
    "TH.NK": "Nong Khai",
    "TH.NO": "Nonthaburi",
    "TH.PT": "Pathum Thani",
    "TH.PI": "Pattani",
    "TH.PG": "Phangnga",
    "TH.PL": "Phatthalung",
    "TH.PY": "Phayao",
    "TH.PH": "Phetchabun",
    "TH.PE": "Phetchaburi",
    "TH.PC": "Phichit",
    "TH.PS": "Phitsanulok",
    "TH.PR": "Phrae",
    "TH.PA": "Phra Nakhon Si Ayutthaya",
    "TH.PU": "Phuket",
    "TH.PB": "Prachin Buri",
    "TH.PK": "Prachuap Khiri Khan",
    "TH.RN": "Ranong",
    "TH.RT": "Ratchaburi",
    "TH.RY": "Rayong",
    "TH.RE": "Roi Et",
    "TH.SK": "Sa Kaeo",
    "TH.SN": "Sakon Nakhon",
    "TH.SP": "Samut Prakan",
    "TH.SS": "Samut Sakhon",
    "TH.SM": "Samut Songkhram",
    "TH.SR": "Saraburi",
    "TH.SA": "Satun",
    "TH.SB": "Sing Buri",
    "TH.SI": "Si Sa Ket",
    "TH.SG": "Songkhla",
    "TH.SO": "Sukhothai",
    "TH.SH": "Suphan Buri",
    "TH.ST": "Surat Thani",
    "TH.SU": "Surin",
    "TH.TK": "Tak",
    "TH.TG": "Trang",
    "TH.TT": "Trat",
    "TH.UR": "Ubon Ratchathani",
    "TH.UN": "Udon Thani",
    "TH.UT": "Uthai Thani",
    "TH.UD": "Uttaradit",
    "TH.YL": "Yala",
    "TH.YS": "Yasothon"
  }
}

},{}],"/home/wang/projects/tks_project/tks/node_modules/setimmediate/setImmediate.js":[function(require,module,exports){
(function (process){
(function (global, undefined) {
    "use strict";

    if (global.setImmediate) {
        return;
    }

    var nextHandle = 1; // Spec says greater than zero
    var tasksByHandle = {};
    var currentlyRunningATask = false;
    var doc = global.document;
    var setImmediate;

    function addFromSetImmediateArguments(args) {
        tasksByHandle[nextHandle] = partiallyApplied.apply(undefined, args);
        return nextHandle++;
    }

    // This function accepts the same arguments as setImmediate, but
    // returns a function that requires no arguments.
    function partiallyApplied(handler) {
        var args = [].slice.call(arguments, 1);
        return function() {
            if (typeof handler === "function") {
                handler.apply(undefined, args);
            } else {
                (new Function("" + handler))();
            }
        };
    }

    function runIfPresent(handle) {
        // From the spec: "Wait until any invocations of this algorithm started before this one have completed."
        // So if we're currently running a task, we'll need to delay this invocation.
        if (currentlyRunningATask) {
            // Delay by doing a setTimeout. setImmediate was tried instead, but in Firefox 7 it generated a
            // "too much recursion" error.
            setTimeout(partiallyApplied(runIfPresent, handle), 0);
        } else {
            var task = tasksByHandle[handle];
            if (task) {
                currentlyRunningATask = true;
                try {
                    task();
                } finally {
                    clearImmediate(handle);
                    currentlyRunningATask = false;
                }
            }
        }
    }

    function clearImmediate(handle) {
        delete tasksByHandle[handle];
    }

    function installNextTickImplementation() {
        setImmediate = function() {
            var handle = addFromSetImmediateArguments(arguments);
            process.nextTick(partiallyApplied(runIfPresent, handle));
            return handle;
        };
    }

    function canUsePostMessage() {
        // The test against `importScripts` prevents this implementation from being installed inside a web worker,
        // where `global.postMessage` means something completely different and can't be used for this purpose.
        if (global.postMessage && !global.importScripts) {
            var postMessageIsAsynchronous = true;
            var oldOnMessage = global.onmessage;
            global.onmessage = function() {
                postMessageIsAsynchronous = false;
            };
            global.postMessage("", "*");
            global.onmessage = oldOnMessage;
            return postMessageIsAsynchronous;
        }
    }

    function installPostMessageImplementation() {
        // Installs an event handler on `global` for the `message` event: see
        // * https://developer.mozilla.org/en/DOM/window.postMessage
        // * http://www.whatwg.org/specs/web-apps/current-work/multipage/comms.html#crossDocumentMessages

        var messagePrefix = "setImmediate$" + Math.random() + "$";
        var onGlobalMessage = function(event) {
            if (event.source === global &&
                typeof event.data === "string" &&
                event.data.indexOf(messagePrefix) === 0) {
                runIfPresent(+event.data.slice(messagePrefix.length));
            }
        };

        if (global.addEventListener) {
            global.addEventListener("message", onGlobalMessage, false);
        } else {
            global.attachEvent("onmessage", onGlobalMessage);
        }

        setImmediate = function() {
            var handle = addFromSetImmediateArguments(arguments);
            global.postMessage(messagePrefix + handle, "*");
            return handle;
        };
    }

    function installMessageChannelImplementation() {
        var channel = new MessageChannel();
        channel.port1.onmessage = function(event) {
            var handle = event.data;
            runIfPresent(handle);
        };

        setImmediate = function() {
            var handle = addFromSetImmediateArguments(arguments);
            channel.port2.postMessage(handle);
            return handle;
        };
    }

    function installReadyStateChangeImplementation() {
        var html = doc.documentElement;
        setImmediate = function() {
            var handle = addFromSetImmediateArguments(arguments);
            // Create a <script> element; its readystatechange event will be fired asynchronously once it is inserted
            // into the document. Do so, thus queuing up the task. Remember to clean up once it's been called.
            var script = doc.createElement("script");
            script.onreadystatechange = function () {
                runIfPresent(handle);
                script.onreadystatechange = null;
                html.removeChild(script);
                script = null;
            };
            html.appendChild(script);
            return handle;
        };
    }

    function installSetTimeoutImplementation() {
        setImmediate = function() {
            var handle = addFromSetImmediateArguments(arguments);
            setTimeout(partiallyApplied(runIfPresent, handle), 0);
            return handle;
        };
    }

    // If supported, we should attach to the prototype of global, since that is where setTimeout et al. live.
    var attachTo = Object.getPrototypeOf && Object.getPrototypeOf(global);
    attachTo = attachTo && attachTo.setTimeout ? attachTo : global;

    // Don't get fooled by e.g. browserify environments.
    if ({}.toString.call(global.process) === "[object process]") {
        // For Node.js before 0.9
        installNextTickImplementation();

    } else if (canUsePostMessage()) {
        // For non-IE10 modern browsers
        installPostMessageImplementation();

    } else if (global.MessageChannel) {
        // For web workers, where supported
        installMessageChannelImplementation();

    } else if (doc && "onreadystatechange" in doc.createElement("script")) {
        // For IE 6–8
        installReadyStateChangeImplementation();

    } else {
        // For older browsers
        installSetTimeoutImplementation();
    }

    attachTo.setImmediate = setImmediate;
    attachTo.clearImmediate = clearImmediate;
}(new Function("return this")()));

}).call(this,require('_process'))
//# sourceMappingURL=data:application/json;charset:utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9zZXRpbW1lZGlhdGUvc2V0SW1tZWRpYXRlLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gKGdsb2JhbCwgdW5kZWZpbmVkKSB7XG4gICAgXCJ1c2Ugc3RyaWN0XCI7XG5cbiAgICBpZiAoZ2xvYmFsLnNldEltbWVkaWF0ZSkge1xuICAgICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdmFyIG5leHRIYW5kbGUgPSAxOyAvLyBTcGVjIHNheXMgZ3JlYXRlciB0aGFuIHplcm9cbiAgICB2YXIgdGFza3NCeUhhbmRsZSA9IHt9O1xuICAgIHZhciBjdXJyZW50bHlSdW5uaW5nQVRhc2sgPSBmYWxzZTtcbiAgICB2YXIgZG9jID0gZ2xvYmFsLmRvY3VtZW50O1xuICAgIHZhciBzZXRJbW1lZGlhdGU7XG5cbiAgICBmdW5jdGlvbiBhZGRGcm9tU2V0SW1tZWRpYXRlQXJndW1lbnRzKGFyZ3MpIHtcbiAgICAgICAgdGFza3NCeUhhbmRsZVtuZXh0SGFuZGxlXSA9IHBhcnRpYWxseUFwcGxpZWQuYXBwbHkodW5kZWZpbmVkLCBhcmdzKTtcbiAgICAgICAgcmV0dXJuIG5leHRIYW5kbGUrKztcbiAgICB9XG5cbiAgICAvLyBUaGlzIGZ1bmN0aW9uIGFjY2VwdHMgdGhlIHNhbWUgYXJndW1lbnRzIGFzIHNldEltbWVkaWF0ZSwgYnV0XG4gICAgLy8gcmV0dXJucyBhIGZ1bmN0aW9uIHRoYXQgcmVxdWlyZXMgbm8gYXJndW1lbnRzLlxuICAgIGZ1bmN0aW9uIHBhcnRpYWxseUFwcGxpZWQoaGFuZGxlcikge1xuICAgICAgICB2YXIgYXJncyA9IFtdLnNsaWNlLmNhbGwoYXJndW1lbnRzLCAxKTtcbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaWYgKHR5cGVvZiBoYW5kbGVyID09PSBcImZ1bmN0aW9uXCIpIHtcbiAgICAgICAgICAgICAgICBoYW5kbGVyLmFwcGx5KHVuZGVmaW5lZCwgYXJncyk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIChuZXcgRnVuY3Rpb24oXCJcIiArIGhhbmRsZXIpKSgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHJ1bklmUHJlc2VudChoYW5kbGUpIHtcbiAgICAgICAgLy8gRnJvbSB0aGUgc3BlYzogXCJXYWl0IHVudGlsIGFueSBpbnZvY2F0aW9ucyBvZiB0aGlzIGFsZ29yaXRobSBzdGFydGVkIGJlZm9yZSB0aGlzIG9uZSBoYXZlIGNvbXBsZXRlZC5cIlxuICAgICAgICAvLyBTbyBpZiB3ZSdyZSBjdXJyZW50bHkgcnVubmluZyBhIHRhc2ssIHdlJ2xsIG5lZWQgdG8gZGVsYXkgdGhpcyBpbnZvY2F0aW9uLlxuICAgICAgICBpZiAoY3VycmVudGx5UnVubmluZ0FUYXNrKSB7XG4gICAgICAgICAgICAvLyBEZWxheSBieSBkb2luZyBhIHNldFRpbWVvdXQuIHNldEltbWVkaWF0ZSB3YXMgdHJpZWQgaW5zdGVhZCwgYnV0IGluIEZpcmVmb3ggNyBpdCBnZW5lcmF0ZWQgYVxuICAgICAgICAgICAgLy8gXCJ0b28gbXVjaCByZWN1cnNpb25cIiBlcnJvci5cbiAgICAgICAgICAgIHNldFRpbWVvdXQocGFydGlhbGx5QXBwbGllZChydW5JZlByZXNlbnQsIGhhbmRsZSksIDApO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdmFyIHRhc2sgPSB0YXNrc0J5SGFuZGxlW2hhbmRsZV07XG4gICAgICAgICAgICBpZiAodGFzaykge1xuICAgICAgICAgICAgICAgIGN1cnJlbnRseVJ1bm5pbmdBVGFzayA9IHRydWU7XG4gICAgICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICAgICAgdGFzaygpO1xuICAgICAgICAgICAgICAgIH0gZmluYWxseSB7XG4gICAgICAgICAgICAgICAgICAgIGNsZWFySW1tZWRpYXRlKGhhbmRsZSk7XG4gICAgICAgICAgICAgICAgICAgIGN1cnJlbnRseVJ1bm5pbmdBVGFzayA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIGNsZWFySW1tZWRpYXRlKGhhbmRsZSkge1xuICAgICAgICBkZWxldGUgdGFza3NCeUhhbmRsZVtoYW5kbGVdO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGluc3RhbGxOZXh0VGlja0ltcGxlbWVudGF0aW9uKCkge1xuICAgICAgICBzZXRJbW1lZGlhdGUgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhciBoYW5kbGUgPSBhZGRGcm9tU2V0SW1tZWRpYXRlQXJndW1lbnRzKGFyZ3VtZW50cyk7XG4gICAgICAgICAgICBwcm9jZXNzLm5leHRUaWNrKHBhcnRpYWxseUFwcGxpZWQocnVuSWZQcmVzZW50LCBoYW5kbGUpKTtcbiAgICAgICAgICAgIHJldHVybiBoYW5kbGU7XG4gICAgICAgIH07XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gY2FuVXNlUG9zdE1lc3NhZ2UoKSB7XG4gICAgICAgIC8vIFRoZSB0ZXN0IGFnYWluc3QgYGltcG9ydFNjcmlwdHNgIHByZXZlbnRzIHRoaXMgaW1wbGVtZW50YXRpb24gZnJvbSBiZWluZyBpbnN0YWxsZWQgaW5zaWRlIGEgd2ViIHdvcmtlcixcbiAgICAgICAgLy8gd2hlcmUgYGdsb2JhbC5wb3N0TWVzc2FnZWAgbWVhbnMgc29tZXRoaW5nIGNvbXBsZXRlbHkgZGlmZmVyZW50IGFuZCBjYW4ndCBiZSB1c2VkIGZvciB0aGlzIHB1cnBvc2UuXG4gICAgICAgIGlmIChnbG9iYWwucG9zdE1lc3NhZ2UgJiYgIWdsb2JhbC5pbXBvcnRTY3JpcHRzKSB7XG4gICAgICAgICAgICB2YXIgcG9zdE1lc3NhZ2VJc0FzeW5jaHJvbm91cyA9IHRydWU7XG4gICAgICAgICAgICB2YXIgb2xkT25NZXNzYWdlID0gZ2xvYmFsLm9ubWVzc2FnZTtcbiAgICAgICAgICAgIGdsb2JhbC5vbm1lc3NhZ2UgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICBwb3N0TWVzc2FnZUlzQXN5bmNocm9ub3VzID0gZmFsc2U7XG4gICAgICAgICAgICB9O1xuICAgICAgICAgICAgZ2xvYmFsLnBvc3RNZXNzYWdlKFwiXCIsIFwiKlwiKTtcbiAgICAgICAgICAgIGdsb2JhbC5vbm1lc3NhZ2UgPSBvbGRPbk1lc3NhZ2U7XG4gICAgICAgICAgICByZXR1cm4gcG9zdE1lc3NhZ2VJc0FzeW5jaHJvbm91cztcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIGluc3RhbGxQb3N0TWVzc2FnZUltcGxlbWVudGF0aW9uKCkge1xuICAgICAgICAvLyBJbnN0YWxscyBhbiBldmVudCBoYW5kbGVyIG9uIGBnbG9iYWxgIGZvciB0aGUgYG1lc3NhZ2VgIGV2ZW50OiBzZWVcbiAgICAgICAgLy8gKiBodHRwczovL2RldmVsb3Blci5tb3ppbGxhLm9yZy9lbi9ET00vd2luZG93LnBvc3RNZXNzYWdlXG4gICAgICAgIC8vICogaHR0cDovL3d3dy53aGF0d2cub3JnL3NwZWNzL3dlYi1hcHBzL2N1cnJlbnQtd29yay9tdWx0aXBhZ2UvY29tbXMuaHRtbCNjcm9zc0RvY3VtZW50TWVzc2FnZXNcblxuICAgICAgICB2YXIgbWVzc2FnZVByZWZpeCA9IFwic2V0SW1tZWRpYXRlJFwiICsgTWF0aC5yYW5kb20oKSArIFwiJFwiO1xuICAgICAgICB2YXIgb25HbG9iYWxNZXNzYWdlID0gZnVuY3Rpb24oZXZlbnQpIHtcbiAgICAgICAgICAgIGlmIChldmVudC5zb3VyY2UgPT09IGdsb2JhbCAmJlxuICAgICAgICAgICAgICAgIHR5cGVvZiBldmVudC5kYXRhID09PSBcInN0cmluZ1wiICYmXG4gICAgICAgICAgICAgICAgZXZlbnQuZGF0YS5pbmRleE9mKG1lc3NhZ2VQcmVmaXgpID09PSAwKSB7XG4gICAgICAgICAgICAgICAgcnVuSWZQcmVzZW50KCtldmVudC5kYXRhLnNsaWNlKG1lc3NhZ2VQcmVmaXgubGVuZ3RoKSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICAgICAgaWYgKGdsb2JhbC5hZGRFdmVudExpc3RlbmVyKSB7XG4gICAgICAgICAgICBnbG9iYWwuYWRkRXZlbnRMaXN0ZW5lcihcIm1lc3NhZ2VcIiwgb25HbG9iYWxNZXNzYWdlLCBmYWxzZSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBnbG9iYWwuYXR0YWNoRXZlbnQoXCJvbm1lc3NhZ2VcIiwgb25HbG9iYWxNZXNzYWdlKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHNldEltbWVkaWF0ZSA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyIGhhbmRsZSA9IGFkZEZyb21TZXRJbW1lZGlhdGVBcmd1bWVudHMoYXJndW1lbnRzKTtcbiAgICAgICAgICAgIGdsb2JhbC5wb3N0TWVzc2FnZShtZXNzYWdlUHJlZml4ICsgaGFuZGxlLCBcIipcIik7XG4gICAgICAgICAgICByZXR1cm4gaGFuZGxlO1xuICAgICAgICB9O1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGluc3RhbGxNZXNzYWdlQ2hhbm5lbEltcGxlbWVudGF0aW9uKCkge1xuICAgICAgICB2YXIgY2hhbm5lbCA9IG5ldyBNZXNzYWdlQ2hhbm5lbCgpO1xuICAgICAgICBjaGFubmVsLnBvcnQxLm9ubWVzc2FnZSA9IGZ1bmN0aW9uKGV2ZW50KSB7XG4gICAgICAgICAgICB2YXIgaGFuZGxlID0gZXZlbnQuZGF0YTtcbiAgICAgICAgICAgIHJ1bklmUHJlc2VudChoYW5kbGUpO1xuICAgICAgICB9O1xuXG4gICAgICAgIHNldEltbWVkaWF0ZSA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyIGhhbmRsZSA9IGFkZEZyb21TZXRJbW1lZGlhdGVBcmd1bWVudHMoYXJndW1lbnRzKTtcbiAgICAgICAgICAgIGNoYW5uZWwucG9ydDIucG9zdE1lc3NhZ2UoaGFuZGxlKTtcbiAgICAgICAgICAgIHJldHVybiBoYW5kbGU7XG4gICAgICAgIH07XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gaW5zdGFsbFJlYWR5U3RhdGVDaGFuZ2VJbXBsZW1lbnRhdGlvbigpIHtcbiAgICAgICAgdmFyIGh0bWwgPSBkb2MuZG9jdW1lbnRFbGVtZW50O1xuICAgICAgICBzZXRJbW1lZGlhdGUgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhciBoYW5kbGUgPSBhZGRGcm9tU2V0SW1tZWRpYXRlQXJndW1lbnRzKGFyZ3VtZW50cyk7XG4gICAgICAgICAgICAvLyBDcmVhdGUgYSA8c2NyaXB0PiBlbGVtZW50OyBpdHMgcmVhZHlzdGF0ZWNoYW5nZSBldmVudCB3aWxsIGJlIGZpcmVkIGFzeW5jaHJvbm91c2x5IG9uY2UgaXQgaXMgaW5zZXJ0ZWRcbiAgICAgICAgICAgIC8vIGludG8gdGhlIGRvY3VtZW50LiBEbyBzbywgdGh1cyBxdWV1aW5nIHVwIHRoZSB0YXNrLiBSZW1lbWJlciB0byBjbGVhbiB1cCBvbmNlIGl0J3MgYmVlbiBjYWxsZWQuXG4gICAgICAgICAgICB2YXIgc2NyaXB0ID0gZG9jLmNyZWF0ZUVsZW1lbnQoXCJzY3JpcHRcIik7XG4gICAgICAgICAgICBzY3JpcHQub25yZWFkeXN0YXRlY2hhbmdlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIHJ1bklmUHJlc2VudChoYW5kbGUpO1xuICAgICAgICAgICAgICAgIHNjcmlwdC5vbnJlYWR5c3RhdGVjaGFuZ2UgPSBudWxsO1xuICAgICAgICAgICAgICAgIGh0bWwucmVtb3ZlQ2hpbGQoc2NyaXB0KTtcbiAgICAgICAgICAgICAgICBzY3JpcHQgPSBudWxsO1xuICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIGh0bWwuYXBwZW5kQ2hpbGQoc2NyaXB0KTtcbiAgICAgICAgICAgIHJldHVybiBoYW5kbGU7XG4gICAgICAgIH07XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gaW5zdGFsbFNldFRpbWVvdXRJbXBsZW1lbnRhdGlvbigpIHtcbiAgICAgICAgc2V0SW1tZWRpYXRlID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXIgaGFuZGxlID0gYWRkRnJvbVNldEltbWVkaWF0ZUFyZ3VtZW50cyhhcmd1bWVudHMpO1xuICAgICAgICAgICAgc2V0VGltZW91dChwYXJ0aWFsbHlBcHBsaWVkKHJ1bklmUHJlc2VudCwgaGFuZGxlKSwgMCk7XG4gICAgICAgICAgICByZXR1cm4gaGFuZGxlO1xuICAgICAgICB9O1xuICAgIH1cblxuICAgIC8vIElmIHN1cHBvcnRlZCwgd2Ugc2hvdWxkIGF0dGFjaCB0byB0aGUgcHJvdG90eXBlIG9mIGdsb2JhbCwgc2luY2UgdGhhdCBpcyB3aGVyZSBzZXRUaW1lb3V0IGV0IGFsLiBsaXZlLlxuICAgIHZhciBhdHRhY2hUbyA9IE9iamVjdC5nZXRQcm90b3R5cGVPZiAmJiBPYmplY3QuZ2V0UHJvdG90eXBlT2YoZ2xvYmFsKTtcbiAgICBhdHRhY2hUbyA9IGF0dGFjaFRvICYmIGF0dGFjaFRvLnNldFRpbWVvdXQgPyBhdHRhY2hUbyA6IGdsb2JhbDtcblxuICAgIC8vIERvbid0IGdldCBmb29sZWQgYnkgZS5nLiBicm93c2VyaWZ5IGVudmlyb25tZW50cy5cbiAgICBpZiAoe30udG9TdHJpbmcuY2FsbChnbG9iYWwucHJvY2VzcykgPT09IFwiW29iamVjdCBwcm9jZXNzXVwiKSB7XG4gICAgICAgIC8vIEZvciBOb2RlLmpzIGJlZm9yZSAwLjlcbiAgICAgICAgaW5zdGFsbE5leHRUaWNrSW1wbGVtZW50YXRpb24oKTtcblxuICAgIH0gZWxzZSBpZiAoY2FuVXNlUG9zdE1lc3NhZ2UoKSkge1xuICAgICAgICAvLyBGb3Igbm9uLUlFMTAgbW9kZXJuIGJyb3dzZXJzXG4gICAgICAgIGluc3RhbGxQb3N0TWVzc2FnZUltcGxlbWVudGF0aW9uKCk7XG5cbiAgICB9IGVsc2UgaWYgKGdsb2JhbC5NZXNzYWdlQ2hhbm5lbCkge1xuICAgICAgICAvLyBGb3Igd2ViIHdvcmtlcnMsIHdoZXJlIHN1cHBvcnRlZFxuICAgICAgICBpbnN0YWxsTWVzc2FnZUNoYW5uZWxJbXBsZW1lbnRhdGlvbigpO1xuXG4gICAgfSBlbHNlIGlmIChkb2MgJiYgXCJvbnJlYWR5c3RhdGVjaGFuZ2VcIiBpbiBkb2MuY3JlYXRlRWxlbWVudChcInNjcmlwdFwiKSkge1xuICAgICAgICAvLyBGb3IgSUUgNuKAkzhcbiAgICAgICAgaW5zdGFsbFJlYWR5U3RhdGVDaGFuZ2VJbXBsZW1lbnRhdGlvbigpO1xuXG4gICAgfSBlbHNlIHtcbiAgICAgICAgLy8gRm9yIG9sZGVyIGJyb3dzZXJzXG4gICAgICAgIGluc3RhbGxTZXRUaW1lb3V0SW1wbGVtZW50YXRpb24oKTtcbiAgICB9XG5cbiAgICBhdHRhY2hUby5zZXRJbW1lZGlhdGUgPSBzZXRJbW1lZGlhdGU7XG4gICAgYXR0YWNoVG8uY2xlYXJJbW1lZGlhdGUgPSBjbGVhckltbWVkaWF0ZTtcbn0obmV3IEZ1bmN0aW9uKFwicmV0dXJuIHRoaXNcIikoKSkpO1xuIl19
},{"_process":"/home/wang/projects/tks_project/tks/node_modules/browserify/node_modules/process/browser.js"}]},{},["./app/js/web/web_app.js"])
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCIvaG9tZS93YW5nL3Byb2plY3RzL3Rrc19wcm9qZWN0L3Rrcy9hcHAvanMvd2ViL3dlYl9hcHAuanMiLCIvaG9tZS93YW5nL3Byb2plY3RzL3Rrc19wcm9qZWN0L3Rrcy9hcHAvanMvY29tbW9uL3Byb2ZpbGVfZGlyZWN0aXZlLmpzIiwiL2hvbWUvd2FuZy9wcm9qZWN0cy90a3NfcHJvamVjdC90a3MvYXBwL2pzL2NvbW1vbi90a3NfZGlyZWN0aXZlcy5qcyIsIi9ob21lL3dhbmcvcHJvamVjdHMvdGtzX3Byb2plY3QvdGtzL2FwcC9qcy9jb21tb24vdGtzX2RpcmVjdGl2ZXMyLmpzIiwiL2hvbWUvd2FuZy9wcm9qZWN0cy90a3NfcHJvamVjdC90a3MvYXBwL2pzL2NvbW1vbi90a3NfZmlsdGVycy5qcyIsIi9ob21lL3dhbmcvcHJvamVjdHMvdGtzX3Byb2plY3QvdGtzL2FwcC9qcy9jb21tb24vdGtzX3NlcnZpY2VzLmpzIiwiL2hvbWUvd2FuZy9wcm9qZWN0cy90a3NfcHJvamVjdC90a3MvYXBwL2pzL21vZHVsZXMvbW9kX2F1dGhvcml6ZS5qcyIsIi9ob21lL3dhbmcvcHJvamVjdHMvdGtzX3Byb2plY3QvdGtzL2FwcC9qcy9tb2R1bGVzL21vZF9jYXJkcy5qcyIsIi9ob21lL3dhbmcvcHJvamVjdHMvdGtzX3Byb2plY3QvdGtzL2FwcC9qcy9tb2R1bGVzL21vZF9jb3VudHJpZXMuanMiLCIvaG9tZS93YW5nL3Byb2plY3RzL3Rrc19wcm9qZWN0L3Rrcy9hcHAvanMvbW9kdWxlcy9tb2RfcGFnZXIuanMiLCIvaG9tZS93YW5nL3Byb2plY3RzL3Rrc19wcm9qZWN0L3Rrcy9hcHAvanMvbW9kdWxlcy9tb2R1bGVzLmpzIiwiL2hvbWUvd2FuZy9wcm9qZWN0cy90a3NfcHJvamVjdC90a3MvYXBwL2pzL21vZHVsZXMvdXRpbGl0aWVzLmpzIiwiL2hvbWUvd2FuZy9wcm9qZWN0cy90a3NfcHJvamVjdC90a3MvYXBwL2pzL3dlYi9hZGRyZXNzX3RvX3Byb2ZpbGUuanMiLCIvaG9tZS93YW5nL3Byb2plY3RzL3Rrc19wcm9qZWN0L3Rrcy9hcHAvanMvd2ViL2NvbnRyb2xsZXJzL0VzNmNvbnRyb2xsZXIuanMiLCIvaG9tZS93YW5nL3Byb2plY3RzL3Rrc19wcm9qZWN0L3Rrcy9hcHAvanMvd2ViL2NvbnRyb2xsZXJzL1VzZXJDb250cm9sbGVyLmpzIiwiL2hvbWUvd2FuZy9wcm9qZWN0cy90a3NfcHJvamVjdC90a3MvYXBwL2pzL3dlYi9sb2NhbGVfZW4uanNvbiIsIi9ob21lL3dhbmcvcHJvamVjdHMvdGtzX3Byb2plY3QvdGtzL2FwcC9qcy93ZWIvbG9jYWxlX3poLUNOLmpzb24iLCIvaG9tZS93YW5nL3Byb2plY3RzL3Rrc19wcm9qZWN0L3Rrcy9hcHAvanMvd2ViL3ByaWNlX2NhbGMuanMiLCIvaG9tZS93YW5nL3Byb2plY3RzL3Rrc19wcm9qZWN0L3Rrcy9hcHAvanMvd2ViL3Byb2ZpbGVfZWRpdC5qcyIsIi9ob21lL3dhbmcvcHJvamVjdHMvdGtzX3Byb2plY3QvdGtzL2FwcC9qcy93ZWIvdGVtcDIuanNvbiIsIi9ob21lL3dhbmcvcHJvamVjdHMvdGtzX3Byb2plY3QvdGtzL2FwcC9qcy93ZWIvd2ViX2NhcnRfc3RvcmUuanMiLCIvaG9tZS93YW5nL3Byb2plY3RzL3Rrc19wcm9qZWN0L3Rrcy9hcHAvanMvd2ViL3dlYl9jb250cm9sbGVycy5qcyIsIi9ob21lL3dhbmcvcHJvamVjdHMvdGtzX3Byb2plY3QvdGtzL2FwcC9qcy93ZWIvd2ViX2NvbnRyb2xsZXJzX2NhcnQuanMiLCIvaG9tZS93YW5nL3Byb2plY3RzL3Rrc19wcm9qZWN0L3Rrcy9hcHAvanMvd2ViL3dlYl9jb250cm9sbGVyc19jYXNlLmpzIiwiL2hvbWUvd2FuZy9wcm9qZWN0cy90a3NfcHJvamVjdC90a3MvYXBwL2pzL3dlYi93ZWJfY29udHJvbGxlcnNfY2hlY2tfb3V0LmpzIiwiL2hvbWUvd2FuZy9wcm9qZWN0cy90a3NfcHJvamVjdC90a3MvYXBwL2pzL3dlYi93ZWJfY29udHJvbGxlcnNfZXVfY29va2llcy5qcyIsIi9ob21lL3dhbmcvcHJvamVjdHMvdGtzX3Byb2plY3QvdGtzL2FwcC9qcy93ZWIvd2ViX2NvbnRyb2xsZXJzX3N0cmlwZS5qcyIsIi9ob21lL3dhbmcvcHJvamVjdHMvdGtzX3Byb2plY3QvdGtzL2FwcC9qcy93ZWIvd2ViX2NvbnRyb2xsZXJzX3RyYWRlbWFya3MuanMiLCIvaG9tZS93YW5nL3Byb2plY3RzL3Rrc19wcm9qZWN0L3Rrcy9hcHAvanMvd2ViL3dlYl9tZW51LmpzIiwiL2hvbWUvd2FuZy9wcm9qZWN0cy90a3NfcHJvamVjdC90a3MvYXBwL2pzL3dlYi93ZWJfbW9uaXRvci5qcyIsIi9ob21lL3dhbmcvcHJvamVjdHMvdGtzX3Byb2plY3QvdGtzL2FwcC9qcy93ZWIvd2ViX3JvdXRlcy5qcyIsIi9ob21lL3dhbmcvcHJvamVjdHMvdGtzX3Byb2plY3QvdGtzL2FwcC9qcy93ZWIvd2ViX3NlcnZpY2VzLmpzIiwiL2hvbWUvd2FuZy9wcm9qZWN0cy90a3NfcHJvamVjdC90a3MvYXBwL2pzL3dlYi93ZWJfc2VydmljZXNfY2FydC5qcyIsIi9ob21lL3dhbmcvcHJvamVjdHMvdGtzX3Byb2plY3QvdGtzL2FwcC9qcy93ZWIvd2ViX3NlcnZpY2VzX2Nhc2UuanMiLCIvaG9tZS93YW5nL3Byb2plY3RzL3Rrc19wcm9qZWN0L3Rrcy9hcHAvanMvd2ViL3dlYl9zZXJ2aWNlc19wcm9jcy5qcyIsIi9ob21lL3dhbmcvcHJvamVjdHMvdGtzX3Byb2plY3QvdGtzL2FwcC9qcy93ZWIvd2ViX3NlcnZpY2VzX3RyYW5zYWN0aW9uLmpzIiwiL2hvbWUvd2FuZy9wcm9qZWN0cy90a3NfcHJvamVjdC90a3MvYXBwL2pzL3dlYi93ZWJfc2VydmljZXNfdHJhbnNhY3Rpb25fcHJvY3MuanMiLCIvaG9tZS93YW5nL3Byb2plY3RzL3Rrc19wcm9qZWN0L3Rrcy9hcHAvanMvd2ViL3dlYl9zdHVkaWVzLmpzIiwiL2hvbWUvd2FuZy9wcm9qZWN0cy90a3NfcHJvamVjdC90a3MvZGF0YS9hcmlwby5qc29uIiwiL2hvbWUvd2FuZy9wcm9qZWN0cy90a3NfcHJvamVjdC90a3MvZGF0YS9jb3VudHJpZXMvdG1fY291bnRyaWVzLmpzb24iLCIvaG9tZS93YW5nL3Byb2plY3RzL3Rrc19wcm9qZWN0L3Rrcy9kYXRhL2V1Lmpzb24iLCIvaG9tZS93YW5nL3Byb2plY3RzL3Rrc19wcm9qZWN0L3Rrcy9kYXRhL2Zvcm1hdHMuanNvbiIsIi9ob21lL3dhbmcvcHJvamVjdHMvdGtzX3Byb2plY3QvdGtzL2RhdGEvb2FwaS5qc29uIiwiL2hvbWUvd2FuZy9wcm9qZWN0cy90a3NfcHJvamVjdC90a3MvZGF0YS9vcmdhbml6YXRpb25zLmpzb24iLCIvaG9tZS93YW5nL3Byb2plY3RzL3Rrc19wcm9qZWN0L3Rrcy9ub2RlX21vZHVsZXMvYnJvd3NlcmlmeS9ub2RlX21vZHVsZXMvcHJvY2Vzcy9icm93c2VyLmpzIiwiL2hvbWUvd2FuZy9wcm9qZWN0cy90a3NfcHJvamVjdC90a3Mvbm9kZV9tb2R1bGVzL2NvdW50cmllcy9jb3VudHJpZXMuanNvbiIsIi9ob21lL3dhbmcvcHJvamVjdHMvdGtzX3Byb2plY3QvdGtzL25vZGVfbW9kdWxlcy9jb3VudHJpZXMvY291bnRyaWVzLmpzIiwiL2hvbWUvd2FuZy9wcm9qZWN0cy90a3NfcHJvamVjdC90a3Mvbm9kZV9tb2R1bGVzL2NvdW50cmllcy9zdGF0ZXMuanNvbiIsIi9ob21lL3dhbmcvcHJvamVjdHMvdGtzX3Byb2plY3QvdGtzL25vZGVfbW9kdWxlcy9zZXRpbW1lZGlhdGUvc2V0SW1tZWRpYXRlLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7OztBQ0lBLFlBQVksQ0FBQzs7Ozs7O0FBTWIsT0FBTyxDQUFDLHNCQUFzQixDQUFDLENBQUE7QUFDL0IsT0FBTyxDQUFDLGVBQWUsQ0FBQyxDQUFBO0FBQ3hCLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxDQUFBO0FBQzVCLE9BQU8sQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFBO0FBQzFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxDQUFBO0FBQzNCLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxDQUFBOztBQUUzQixPQUFPLENBQUMsdUJBQXVCLENBQUMsQ0FBQTtBQUNoQyxJQUFJLENBQUMsR0FBRyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUE7O0FBSS9CLElBQUksT0FBTyxHQUNQLENBQ0ksWUFBWSxFQUFFLFNBQVMsRUFBRSxXQUFXLEVBQUUsWUFBWSxFQUFFLHdCQUF3QixFQUM1RSxhQUFhLEVBQUUsOEJBQThCLEVBQUUsK0JBQStCLEVBQzlFLFdBQVcsRUFBRSxlQUFlLEVBQUUsaUJBQWlCLEVBQUUsY0FBYyxFQUFFLGVBQWUsRUFDaEYsYUFBYSxFQUFFLGdCQUFnQixFQUFFLGlCQUFpQixFQUFFLGVBQWUsRUFBRSxXQUFXLEVBQ2hGLHFCQUFxQixFQUFFLHVCQUF1QixFQUFFLFVBQVUsRUFBRSxvQkFBb0IsRUFDaEYsY0FBYyxFQUFFLG9CQUFvQixFQUFDLGFBQWEsQ0FDckQsQ0FBQTs7QUFJTCxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxPQUFPLENBQUMsQ0FDekIsTUFBTSxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQ2xDLE1BQU0sQ0FBQyxVQUFVLGtCQUFrQixFQUFFOztBQUVsQyxRQUFJLFNBQVMsR0FBRyxPQUFPLENBQUMsa0JBQWtCLENBQUMsQ0FBQTtBQUMzQyxRQUFJLFlBQVksR0FBRyxPQUFPLENBQUMscUJBQXFCLENBQUMsQ0FBQTtBQUNqRCxXQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxTQUFTLENBQUMsQ0FBQTs7QUFFbkMsc0JBQWtCLENBQUMsd0JBQXdCLENBQUMsUUFBUSxDQUFDLENBQUM7O0FBRXRELHNCQUFrQixDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLENBQzNDLFlBQVksQ0FBQyxPQUFPLEVBQUUsWUFBWSxDQUFDLENBQUM7O0FBRXpDLHNCQUFrQixDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxDQUFDO0NBRTlDLENBQUMsQ0FDRCxHQUFHLENBQUMsQ0FBQyxZQUFZLEVBQUUsV0FBVyxFQUFFLGFBQWEsRUFBRSxXQUFXLEVBQUUsVUFBVSxVQUFVLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBRSxTQUFTLEVBQUU7QUFDbEgsV0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQTtBQUNuQixXQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFBO0FBQ3BCLGNBQVUsQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEVBQUUsVUFBVSxLQUFLLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRTs7QUFFaEUsZUFBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQTtBQUN2QixZQUFJLE1BQU0sR0FBRyxzQkFBc0IsQ0FBQTtBQUNuQyxZQUFJLFVBQVUsR0FBRyxtQ0FBbUMsQ0FBQTs7QUFFcEQsZUFBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUE7O0FBR3pCLFlBQUksSUFBSSxDQUFDLE9BQU8sS0FBSyxTQUFTLEVBQUU7QUFDNUIsZ0JBQUksWUFBWSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFBO0FBQzVDLG1CQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsRUFBRSxZQUFZLENBQUMsQ0FBQTtBQUMxQyxnQkFBSSxZQUFZLEtBQUssUUFBUSxFQUN6QixVQUFVLEdBQUcsVUFBVSxDQUFBLEtBQ3RCLElBQUksWUFBWSxLQUFLLFlBQVksRUFDbEMsVUFBVSxHQUFHLHNDQUFzQyxDQUFBLEtBQ2xELElBQUksWUFBWSxLQUFLLFVBQVUsRUFDaEMsVUFBVSxHQUFHLFlBQVksQ0FBQSxLQUN4QixJQUFJLFlBQVksS0FBSyxVQUFVLEVBQ2hDLFVBQVUsR0FBRyx5QkFBeUIsQ0FBQSxLQUNyQyxJQUFJLFlBQVksS0FBSyxpQkFBaUIsRUFDdkMsVUFBVSxHQUFHLHFCQUFxQixDQUFBLEtBQ2pDLElBQUksWUFBWSxLQUFLLFFBQVEsRUFDOUIsVUFBVSxHQUFHLHNCQUFzQixDQUFBO1NBQzFDOztBQUdELGlCQUFTLENBQUMsUUFBUSxDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUMsQ0FBQTs7QUFJdkMsWUFBSSxJQUFJLENBQUMsT0FBTyxLQUFLLFNBQVMsRUFBRTtBQUM1QixnQkFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sS0FBSyxTQUFTLEVBQUU7QUFDbkMsb0JBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUU7QUFDckIsd0JBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxFQUFFLEVBQUU7QUFDM0IsK0JBQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLENBQUMsQ0FBQTtBQUNqQyxpQ0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztxQkFDN0I7aUJBQ0o7YUFDSjtTQUNKO0tBQ0osQ0FBQyxDQUFDO0NBQ04sQ0FBQyxDQUFDLENBQ0YsR0FBRyxDQUFDLENBQUMsU0FBUyxFQUFFLFVBQVUsT0FBTyxFQUFFO0FBQ2hDLFdBQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUE7QUFDdEIsV0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFBO0NBQ25CLENBQUMsQ0FBQyxDQUdGLE9BQU8sQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLElBQUksRUFBRSxXQUFXLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsRUFBRSxTQUFTLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRTtBQUM3RyxXQUFPO0FBQ0gsZUFBTyxFQUFFLGlCQUFVLE1BQU0sRUFBRTtBQUN2QixrQkFBTSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUMsT0FBTyxJQUFJLEVBQUUsQ0FBQztBQUN0QyxnQkFBSSxPQUFPLENBQUMsWUFBWSxFQUFFLEVBQUU7QUFDeEIsc0JBQU0sQ0FBQyxPQUFPLENBQUMsYUFBYSxHQUFHLFNBQVMsR0FBRyxPQUFPLENBQUMsWUFBWSxFQUFFLENBQUE7YUFDcEU7QUFDRCxtQkFBTyxNQUFNLENBQUM7U0FDakI7QUFDRCxnQkFBUSxFQUFFLGtCQUFVLFNBQVEsRUFBRTtBQUMxQixtQkFBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsU0FBUSxDQUFDLE1BQU0sQ0FBQyxDQUFBO0FBQ3BDLG1CQUFPLFNBQVEsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVEsQ0FBQyxDQUFDO1NBQ3hDO0FBQ0QscUJBQWEsRUFBRSx1QkFBVSxRQUFRLEVBQUU7QUFDL0IsbUJBQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQTtBQUNwQyxnQkFBSSxRQUFRLENBQUMsTUFBTSxLQUFLLEdBQUcsRUFBRTtBQUN6QixvQkFBSSxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRTtBQUM3QywyQkFBTyxDQUFDLE9BQU8sRUFBRSxDQUFBO0FBQ2pCLDZCQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFBO0FBQ3pCLDJCQUFPLEVBQUUsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUM7aUJBQzlCLE1BQU07QUFDSCw2QkFBUyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQTtBQUNuQiwyQkFBTyxFQUFFLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2lCQUM5QjthQUNKLE1BQU0sSUFBSSxRQUFRLENBQUMsTUFBTSxLQUFLLEdBQUcsRUFBRTtBQUNoQyx1QkFBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQTtBQUNyQix5QkFBUyxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQTtBQUNwRCx1QkFBTyxFQUFFLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFBO2FBQzdCOztBQUVELG1CQUFPLEVBQUUsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUE7U0FDN0I7S0FDSixDQUFBO0NBQ0osQ0FBQyxDQUFDLENBRUYsTUFBTSxDQUFDLENBQUMsZUFBZSxFQUFFLFVBQVUsYUFBYSxFQUFFO0FBQy9DLGlCQUFhLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO0NBQ3RELENBQUMsQ0FBQyxDQUFBOzs7QUMzSVAsWUFBWSxDQUFDOzs7O0FBSWIsSUFBSSxTQUFTLEdBQUcsT0FBTyxDQUFDLHlCQUF5QixDQUFDLENBQUE7QUFDbEQsSUFBSSxTQUFTLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFBOztBQUVwQyxPQUFPLENBQUMsTUFBTSxDQUFDLG9CQUFvQixFQUFFLEVBQUUsQ0FBQyxDQUN0QyxTQUFTLENBQUMsWUFBWSxFQUFFLENBQUMsU0FBUyxFQUFFLFVBQVUsT0FBTyxFQUFFO0FBQ3JELFNBQU8sVUFBVSxLQUFLLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRTtBQUNsQyxPQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0dBQ25CLENBQUM7Q0FDSCxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsWUFBWSxFQUFFLFlBQVk7QUFDdEMsU0FBTyxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFBO0FBQy9CLFNBQU87QUFDTCxZQUFRLEVBQUUsR0FBRztBQUNiLFNBQUssRUFBRTtBQUNMLFVBQUksRUFBRSxHQUFHO0FBQ1QsZ0JBQVUsRUFBRyxHQUFHO0tBQ2pCO0FBQ0QsWUFBUSxFQUFFLDZCQUE2QjtHQUN4QyxDQUFBO0NBQ0YsQ0FBQzs7OztDQUlELFNBQVMsQ0FBQyxjQUFjLEVBQUUsQ0FBQyxPQUFPLEVBQUUsV0FBVyxFQUFFLFVBQVUsS0FBSyxFQUFFLFNBQVMsRUFBRTtBQUM1RSxTQUFPO0FBQ0wsWUFBUSxFQUFFLEdBQUc7QUFDYixTQUFLLEVBQUU7QUFDTCxVQUFJLEVBQUUsR0FBRztBQUNULGdCQUFVLEVBQUUsR0FBRztLQUNoQjtBQUNELGVBQVcsRUFBRSw2QkFBNkI7QUFDMUMsUUFBSSxFQUFFLGNBQVUsS0FBSyxFQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUU7O0FBRS9CLFVBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLFdBQVc7OztBQUcvQixhQUFLLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLEdBQUcsRUFBRTtBQUNoRCxpQkFBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDLENBQUE7QUFDdkIsY0FBSSxHQUFHLENBQUMsT0FBTyxFQUNiLEtBQUssQ0FBQyxVQUFVLENBQUMsV0FBVyxHQUFHLEdBQUcsQ0FBQyxPQUFPLENBQUE7QUFDNUMsZUFBSyxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFBO1NBQzlCLENBQUMsQ0FBQTs7QUFFSixXQUFLLENBQUMsT0FBTyxHQUFHLENBQ2QsRUFBQyxJQUFJLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUMsRUFDNUIsRUFBQyxJQUFJLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUMsQ0FDaEMsQ0FBQTtBQUNELFdBQUssQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFBO0tBQzVCO0dBQ0YsQ0FBQTtDQUNGLENBQUMsQ0FBQyxDQUVGLFNBQVMsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxVQUFVLEVBQUUsVUFBVSxLQUFLLEVBQUUsY0FBYyxFQUFFLFFBQVEsRUFBRTtBQUM5RyxTQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLENBQUE7QUFDaEMsU0FBTztBQUNMLFlBQVEsRUFBRSxHQUFHO0FBQ2IsU0FBSyxFQUFFO0FBQ0wsVUFBSSxFQUFFLEdBQUc7QUFDVCxnQkFBVSxFQUFHLEdBQUc7QUFDaEIsYUFBTyxFQUFHLEdBQUc7S0FDZDs7OztBQUlELFFBQUksRUFBRSxjQUFVLEtBQUssRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFOzs7Ozs7Ozs7QUFTL0IsV0FBSyxDQUFDLE1BQU0sR0FBRyxTQUFTLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQTs7QUFFbEQsYUFBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUMsSUFBSSxDQUFDLENBQUE7QUFDekIsYUFBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFBO0FBQ25DLGFBQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQTtBQUMvQixhQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQTs7QUFHM0IsVUFBSSxHQUFHLEdBQUcsZ0NBQWdDLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQTtBQUMxRCxhQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFBO0FBQ2hCLGtCQUFZLENBQUMsZ0NBQWdDLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0FBQy9ELGFBQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFBOzs7Ozs7Ozs7Ozs7O0FBZS9DLGVBQVMsWUFBWSxDQUFDLFFBQVEsRUFBRTtBQUM5QixhQUFLLENBQUMsR0FBRyxDQUFDLFFBQVEsaUNBQWlDLENBQ2hELE9BQU8sQ0FBQyxVQUFTLGVBQWUsRUFBRTs7O0FBR2pDLGlCQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBQyxFQUFFLENBQUMsQ0FBQTtBQUNwQixZQUFFLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO0FBQ2pELGlCQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxLQUFLLENBQUMsQ0FBQTtBQUM1QixlQUFLLENBQUMsT0FBTyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUE7U0FDaEMsQ0FBQyxDQUFDO09BQ047O0FBRUQsYUFBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUMsS0FBSyxDQUFDLENBQUE7Ozs7Ozs7Ozs7Ozs7OztBQWlCMUIsV0FBSyxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUMsYUFBYSxFQUFFLENBQUE7S0FDNUM7R0FDRixDQUFBO0NBQ0YsQ0FBQyxDQUFDLENBQUE7OztBQ3RJTCxZQUFZLENBQUM7Ozs7QUFJYixJQUFJLFNBQVMsR0FBRyxPQUFPLENBQUMseUJBQXlCLENBQUMsQ0FBQTs7QUFHbEQsU0FBUyw0QkFBNEIsQ0FBQyxHQUFHLEVBQUU7QUFDdkMsUUFBSSxJQUFJLEdBQUcsRUFBRSxDQUFBO0FBQ2IsU0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7QUFDakMsV0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsR0FBRyxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUE7QUFDakUsWUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtLQUNwQjtBQUNELFdBQU8sSUFBSSxDQUFBO0NBQ2Q7O0FBR0QsT0FBTyxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRSxFQUFFLENBQUMsQ0FDaEMsU0FBUyxDQUFDLFlBQVksRUFBRSxDQUFDLFNBQVMsRUFDOUIsVUFBVSxPQUFPLEVBQUU7QUFDZixXQUFPLFVBQVUsS0FBSyxFQUFFLEdBQUcsRUFBRSxLQUFLLEVBQUU7QUFDaEMsV0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztLQUNyQixDQUFDO0NBQ0wsQ0FDSixDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFBRSxZQUFZO0FBQy9CLFdBQU87QUFDSCxnQkFBUSxFQUFFLEdBQUc7QUFDYixhQUFLLEVBQUU7QUFDSCxnQkFBSSxFQUFFLEdBQUc7U0FDWjtBQUNELGdCQUFRLEVBQUUsNkJBQTZCO0tBQzFDLENBQUE7Q0FDSixDQUFDLENBQUMsU0FBUyxDQUFDLGtCQUFrQixFQUFFLFlBQVk7QUFDekMsV0FBTztBQUNILGdCQUFRLEVBQUUsR0FBRztBQUNiLGFBQUssRUFBRTtBQUNILGdCQUFJLEVBQUUsR0FBRztBQUNULGlCQUFLLEVBQUUsR0FBRztBQUNWLG1CQUFPLEVBQUUsR0FBRztTQUNmO0FBQ0QsbUJBQVcsRUFBRSwwQkFBMEI7Ozs7Ozs7QUFPdkMsWUFBSSxFQUFFLGNBQVUsTUFBTSxFQUFFLE9BQU8sRUFBRTtBQUM3QixtQkFBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtBQUNsQixtQkFBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUE7QUFDaEMsbUJBQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUE7QUFDbkIsbUJBQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUE7U0FDdEI7Ozs7Ozs7Ozs7Ozs7Ozs7OztLQW1CSixDQUFBO0NBQ0osQ0FBQyxDQUFDLFNBQVMsQ0FBQyxTQUFTLEVBQUUsWUFBWTtBQUNoQyxXQUFPO0FBQ0gsZ0JBQVEsRUFBRSxHQUFHO0FBQ2IsYUFBSyxFQUFFO0FBQ0gsZ0JBQUksRUFBRSxHQUFHO0FBQ1Qsa0JBQU0sRUFBRSxHQUFHO1NBQ2Q7QUFDRCxnQkFBUSxFQUFFLGlFQUFpRTtBQUMzRSxZQUFJLEVBQUUsY0FBVSxLQUFLLEVBQUUsRUFBRSxFQUFFLElBQUksRUFBRTtBQUM3QixpQkFBSyxDQUFDLElBQUksR0FBRyxVQUFVLEdBQUcsRUFBRTtBQUN4Qix1QkFBUSxDQUFDLEdBQUcsRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO2FBQ2xELENBQUE7U0FDSjtLQUNKLENBQUE7Q0FDSixDQUFDLENBQUMsU0FBUyxDQUFDLGNBQWMsRUFBRSxZQUFZO0FBQ3JDLFdBQU87QUFDSCxnQkFBUSxFQUFFLEdBQUc7QUFDYixhQUFLLEVBQUU7QUFDSCxnQkFBSSxFQUFFLEdBQUc7U0FDWjs7QUFFRCxnQkFBUSxFQUFFLHFDQUFxQztBQUMvQyxZQUFJLEVBQUUsY0FBVSxLQUFLLEVBQUUsRUFBRSxFQUFFLElBQUksRUFBRTtBQUM3QixpQkFBSyxDQUFDLFFBQVEsR0FBRyxVQUFVLElBQUksRUFBRTtBQUM3Qix1QkFBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQTtBQUNqQix1QkFBTyxlQUFlLEdBQUcsSUFBSSxDQUFDLFdBQVcsRUFBRSxHQUFHLE1BQU0sQ0FBQTthQUN2RCxDQUFBO1NBQ0o7S0FDSixDQUFBO0NBQ0osQ0FBQyxDQUFDLFNBQVMsQ0FBQyxhQUFhLEVBQUUsWUFBWTtBQUNwQyxXQUFPO0FBQ0gsZ0JBQVEsRUFBRSxHQUFHO0FBQ2IsYUFBSyxFQUFFO0FBQ0gsZ0JBQUksRUFBRSxPQUFPO1NBQ2hCO0FBQ0QsZ0JBQVEsRUFBRSx3SUFBd0k7Ozs7O0tBS3JKLENBQUE7Q0FDSixDQUFDLENBQUMsU0FBUyxDQUFDLFVBQVUsRUFBRSxZQUFZO0FBQ2pDLFdBQU87QUFDSCxnQkFBUSxFQUFFLEdBQUc7QUFDYixhQUFLLEVBQUU7QUFDSCxtQkFBTyxFQUFFLEdBQUc7U0FDZjtBQUNELGdCQUFRLEVBQUUsNEJBQTRCO0FBQ3RDLFlBQUksRUFBRSxjQUFVLEtBQUssRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFO0FBQzdCLG1CQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFBO0FBQ3ZCLG1CQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFBO1NBQ3JCO0tBQ0osQ0FBQTtDQUNKLENBQUMsQ0FBQyxTQUFTLENBQUMsa0JBQWtCLEVBQUUsWUFBWTtBQUN6QyxXQUFPO0FBQ0gsZ0JBQVEsRUFBRSxHQUFHO0FBQ2IsYUFBSyxFQUFFO0FBQ0gsbUJBQU8sRUFBRSxHQUFHO1NBQ2Y7QUFDRCxtQkFBVyxFQUFFLG1DQUFtQztLQUNuRCxDQUFDO0NBQ0wsQ0FBQyxDQUVELFNBQVMsQ0FBQywyQkFBMkIsRUFBRSxZQUFZOztBQUNoRCxXQUFPO0FBQ0gsZ0JBQVEsRUFBRSxHQUFHO0FBQ2IsYUFBSyxFQUFFO0FBQ0gsbUJBQU8sRUFBRSxHQUFHO1NBQ2Y7QUFDRCxtQkFBVyxFQUFFLG9DQUFvQztLQUNwRCxDQUFDO0NBQ0wsQ0FBQyxDQUVELFNBQVMsQ0FBQyxTQUFTLEVBQUUsWUFBWTtBQUM5QixXQUFPO0FBQ0gsZ0JBQVEsRUFBRSxHQUFHO0FBQ2IsYUFBSyxFQUFFO0FBQ0gsZ0JBQUksRUFBRSxHQUFHO1NBQ1o7QUFDRCxnQkFBUSxFQUFFLGtFQUFrRTtLQUMvRSxDQUFBO0NBQ0osQ0FBQyxDQUVELFNBQVMsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxXQUFXLEVBQzdCLFVBQVUsU0FBUyxFQUFFO0FBQ2pCLFdBQU87QUFDSCxnQkFBUSxFQUFFLEdBQUc7QUFDYixhQUFLLEVBQUU7QUFDSCxlQUFHLEVBQUUsR0FBRztBQUNSLGlCQUFLLEVBQUUsR0FBRztBQUNWLGtCQUFNLEVBQUUsR0FBRztBQUNYLGlCQUFLLEVBQUUsR0FBRztTQUNiO0FBQ0QsWUFBSSxFQUFFLGNBQVUsS0FBSyxFQUFFLEdBQUcsRUFBRSxLQUFLLEVBQUU7O0FBRS9CLHFCQUFTLFFBQVEsR0FBRzs7QUFFaEIsdUJBQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFBO0FBQ3hCLHFCQUFLLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEVBQUU7QUFDN0IsMkJBQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUE7QUFDbEIsMkJBQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUE7QUFDZCwyQkFBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtpQkFDckIsQ0FBQyxDQUFBO2FBQ0w7O0FBRUQscUJBQVMsY0FBYyxHQUFHO0FBQ3RCLG9CQUFJLElBQUksR0FBRyxJQUFJLE1BQU0sQ0FBQyxhQUFhLENBQUMsU0FBUyxFQUFFLENBQUM7O0FBRWhELG9CQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFBRSxTQUFTLENBQUMsQ0FBQztBQUNwQyxvQkFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsV0FBVyxDQUFDLENBQUM7QUFDdEMsdUJBQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQTs7OztBQUkvQixvQkFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQTs7OztBQUk3QyxvQkFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFBO0FBQ1QscUJBQUssSUFBSSxHQUFHLElBQUksS0FBSyxDQUFDLEtBQUssRUFBRTtBQUN6Qix3QkFBSSxLQUFLLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsRUFBRTs7QUFFakMsNEJBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO0FBQzFDLDRCQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQztBQUMzQyx5QkFBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUE7cUJBQ1o7aUJBQ0o7O0FBRUQsb0JBQUksT0FBTyxHQUFHLEVBQUUsQ0FBQzs7O0FBR2pCLHVCQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsU0FBUyxFQUFFLFNBQVMsQ0FBQyxDQUFDO0FBQ3hDLHVCQUFPLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQzs7O0FBSXhCLG9CQUFJLEtBQUssR0FBRyxJQUFJLE1BQU0sQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7O0FBRWxGLHNCQUFNLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLGFBQWEsRUFDeEQsVUFBVSxNQUFNLEVBQUU7QUFDZCx5QkFBSyxDQUFDLE1BQU0sQ0FBQyxZQUFZO0FBQ3JCLGlDQUFTLENBQUMsSUFBSSxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7Ozs7Ozs7OztxQkFTakQsQ0FBQyxDQUFDO2lCQUNOLENBQUMsQ0FBQzs7QUFFUCxxQkFBSyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUE7Ozs7Ozs7Ozs7O0FBV25CLHFCQUFLLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OzthQXlDN0I7O0FBRUQsMEJBQWMsRUFBRSxDQUFBO1NBRW5CO0tBQ0osQ0FBQztDQUNMLENBQ0osQ0FBQzs7OztDQUlELFNBQVMsQ0FBQyxZQUFZLEVBQUUsWUFBWTtBQUNqQyxXQUFPO0FBQ0gsZ0JBQVEsRUFBRSxHQUFHO0FBQ2IsYUFBSyxFQUFFO0FBQ0gsZ0JBQUksRUFBRSxHQUFHO0FBQ1QsZ0JBQUksRUFBRSxHQUFHO1NBQ1o7QUFDRCxtQkFBVyxFQUFFLDJCQUEyQjtBQUN4QyxZQUFJLEVBQUUsY0FBVSxLQUFLLEVBQUUsRUFBRSxFQUFFLElBQUksRUFBRTs7QUFFN0IsZ0JBQUksR0FBRyxHQUFHLDRCQUE0QixDQUFDLFNBQVMsQ0FBQyx5QkFBeUIsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQTtBQUN2RixnQkFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFBO0FBQ3BDLGlCQUFLLENBQUMsVUFBVSxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFBO0FBQ3JDLGlCQUFLLENBQUMsVUFBVSxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQTtBQUM1QyxpQkFBSyxDQUFDLFVBQVUsR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxJQUFJLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFBO1NBRW5EO0tBQ0osQ0FBQTtDQUNKLENBQUMsQ0FFRCxTQUFTLENBQUMsWUFBWSxFQUFFLENBQUMsU0FBUyxFQUMvQixVQUFVLE9BQU8sRUFBRTs7QUFFZixRQUFJLFNBQVMsR0FBRztBQUNaLGdCQUFRLEVBQUUsR0FBRztLQUNoQixDQUFDO0FBQ0YsYUFBUyxDQUFDLElBQUksR0FBRyxVQUFVLEtBQUssRUFBRSxPQUFPLEVBQUUsVUFBVSxFQUFFO0FBQ25ELFlBQUksSUFBSSxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7QUFDcEMsWUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsWUFBWTtBQUM1QixnQkFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztBQUNqQyxrQkFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQUM7QUFDOUIsbUJBQU8sQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxZQUFZO0FBQzVDLG9CQUFJLElBQUksR0FBRyxTQUFTLENBQUM7QUFDckIscUJBQUssQ0FBQyxNQUFNLENBQUMsWUFBWTtBQUNyQix5QkFBSyxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO2lCQUNuRCxDQUFDLENBQUM7QUFDSCxzQkFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLENBQUM7YUFDbEMsQ0FBQyxDQUFDO1NBQ04sQ0FBQyxDQUFDO0tBQ04sQ0FBQztBQUNGLFdBQU8sU0FBUyxDQUFDO0NBRXBCLENBQ0osQ0FBQyxDQUVELFNBQVMsQ0FBQyxlQUFlLEVBQUUsQ0FBQyxhQUFhLEVBQ3RDLFVBQVUsV0FBVyxFQUFFO0FBQ25CLFdBQU87QUFDSCxnQkFBUSxFQUFFLEdBQUc7QUFDYixhQUFLLEVBQUU7QUFDSCxnQkFBSSxFQUFFLEdBQUc7U0FDWjtBQUNELG1CQUFXLEVBQUUsZ0NBQWdDOzs7QUFHN0MsWUFBSSxFQUFFLGNBQVUsS0FBSyxFQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUU7O0FBRzdCLG1CQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLENBQUE7O0FBRWhDLGlCQUFLLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQzs7QUFFbEIsaUJBQUssQ0FBQyxVQUFVLEdBQUcsVUFBVSxLQUFLLEVBQUU7QUFDaEMscUJBQUssQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztBQUM5QixxQkFBSyxDQUFDLE9BQU8sQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFBO0FBQ3JDLHFCQUFLLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQzthQUVyQixDQUFDOztBQUVGLGlCQUFLLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQTtBQUNmLGlCQUFLLENBQUMsY0FBYyxHQUFHLFlBQVk7QUFDL0IsdUJBQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUE7QUFDeEIsdUJBQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFBO0FBQ3ZCLHVCQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFBO0FBQ2xCLDJCQUFXLENBQUMsc0JBQXNCLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxNQUFNLEVBQUU7QUFDeEUsMkJBQU8sQ0FBQyxHQUFHLENBQUMscUJBQXFCLEVBQUUsTUFBTSxDQUFDLENBQUE7QUFDMUMseUJBQUssQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFBO0FBQ2pCLHdCQUFJLE1BQU0sQ0FBQyxNQUFNLEVBQ2IsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7QUFDZCw0QkFBSSxFQUFFLFNBQVM7QUFDZiwyQkFBRyxFQUFFLGdGQUFnRjtxQkFDeEYsQ0FBQyxDQUFBLEtBRUYsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7QUFDZCw0QkFBSSxFQUFFLFNBQVM7QUFDZiwyQkFBRyxFQUFFLE1BQU0sQ0FBQyxPQUFPO3FCQUN0QixDQUFDLENBQUE7aUJBQ1QsQ0FBQyxDQUFBO2FBQ0wsQ0FBQTtBQUNELG1CQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFBO1NBQ3JCO0tBQ0osQ0FBQTtDQUNKLENBQ0osQ0FBQzs7Ozs7Ozs7OztDQVVELFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxVQUFVLEVBQUUsVUFBVSxRQUFRLEVBQUU7QUFDckQsV0FBTztBQUNILGdCQUFRLEVBQUUsR0FBRztBQUNiLGdCQUFRLEVBQUUsMkdBQTJHO0FBQ3JILFlBQUksRUFBRSxjQUFVLEtBQUssRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFOztBQUU3QixpQkFBSyxDQUFDLFVBQVUsR0FBRyxVQUFVLEtBQUssRUFBRTtBQUNoQyxxQkFBSyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO2FBQ2pDLENBQUM7Ozs7OztTQU1MO0tBQ0osQ0FBQTtDQUNKLENBQUMsQ0FBQyxDQUVGLFNBQVMsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxVQUFVLEVBQUUsVUFBVSxRQUFRLEVBQUU7QUFDbEQsV0FBTzs7QUFFSCxnQkFBUSxFQUFFLEdBQUc7O0FBRWIsWUFBSSxFQUFFLGNBQVUsS0FBSyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUU7O0FBRW5DLG1CQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFBOztBQUV0QixrQkFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLFVBQVUsQ0FBQztBQUN2Qix5QkFBUyxFQUFFLElBQUk7QUFDZixzQkFBTSxFQUFFLE9BQU87QUFDZixxQkFBSyxFQUFFLE1BQU07QUFDYix3QkFBUSxFQUFFLElBQUk7QUFDZCxzQkFBTSxFQUFFLE1BQU07QUFDZCx1QkFBTyxFQUFFLENBQUM7QUFDViwwQkFBVSxFQUFFLENBQUM7QUFDYix3QkFBUSxFQUFFO0FBQ04sc0JBQUUsRUFBRSxFQUFFO0FBQ04sd0JBQUksRUFBRSxFQUFFO0FBQ1IsMEJBQU0sRUFBRSxFQUFFO0FBQ1YsNEJBQVEsRUFBRSxNQUFNO0FBQ2hCLDRCQUFRLEVBQUUsTUFBTTtpQkFDbkI7YUFDSixDQUFDLENBQUM7U0FDTjtLQUNKLENBQUM7Q0FDTCxDQUFDLENBQUMsQ0FFRixTQUFTLENBQUMsV0FBVyxFQUFFLENBQUMsV0FBVyxFQUFFLFVBQVUsU0FBUyxFQUFFO0FBQ3ZELFdBQU87O0FBRUgsZ0JBQVEsRUFBRSxHQUFHOztBQUViLFlBQUksRUFBRSxjQUFVLEtBQUssRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFOztBQUVuQyxpQkFBSyxDQUFDLGNBQWMsR0FBRyxVQUFVLElBQUksRUFBRTtBQUNuQyx1QkFBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQUE7QUFDN0IseUJBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7YUFDdkIsQ0FBQTs7QUFFRCxpQkFBSyxDQUFDLE1BQU0sR0FBRyxPQUFPLENBQUE7O0FBRXRCLGtCQUFNLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxTQUFTLENBQ2xDO0FBQ0kscUJBQUssRUFBRSxDQUFDO0FBQ1IsMEJBQVUsRUFBRSxDQUFDLEVBQUU7QUFDZiwyQkFBVyxFQUFFLENBQUM7QUFDZCx1QkFBTyxFQUFFLGlCQUFVLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFO0FBQ3BDLDJCQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUMsQ0FBQTtBQUMvQix5QkFBSyxDQUFDLE1BQU0sQ0FBQyxZQUFZOztBQUVyQiw0QkFBSSxHQUFHLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDLE9BQU8sRUFBRSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztBQUNsRSwrQkFBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFBO0FBQ3RDLGlDQUFTLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQTtxQkFDckMsQ0FBQyxDQUFDOzs7OztpQkFLTjtBQUNELHNCQUFNLEVBQUUsZ0JBQVUsS0FBSyxFQUFFLEtBQUssRUFBRTtBQUM1Qix3QkFBSSxHQUFHLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDLE9BQU8sRUFBRSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztBQUNsRSwwQkFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxTQUFTLENBQUMsQ0FBQztBQUN6RSx5QkFBSyxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFBO2lCQUNwQzs7YUFHSixDQUNKLENBQUE7U0FDSjtLQUNKLENBQUM7Q0FDTCxDQUFDLENBQUMsQ0FFRixTQUFTLENBQUMsU0FBUyxFQUFFLFlBQVk7QUFDOUIsV0FBTyxVQUFVLEtBQUssRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFO0FBQ3BDLGVBQU8sQ0FBQyxJQUFJLENBQUMsa0JBQWtCLEVBQUUsVUFBVSxLQUFLLEVBQUU7QUFDOUMsZ0JBQUksS0FBSyxDQUFDLEtBQUssS0FBSyxFQUFFLEVBQUU7QUFDcEIscUJBQUssQ0FBQyxNQUFNLENBQUMsWUFBWTtBQUNyQix5QkFBSyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7aUJBQzlCLENBQUMsQ0FBQzs7QUFFSCxxQkFBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO2FBQzFCO1NBQ0osQ0FBQyxDQUFDO0tBQ04sQ0FBQztDQUNMLENBQUMsQ0FFRCxTQUFTLENBQUMsVUFBVSxFQUFFLENBQUMsT0FBTyxFQUFFLFVBQVUsS0FBSyxFQUFFO0FBQzlDLFdBQU87QUFDSCxnQkFBUSxFQUFFLEdBQUc7QUFDYixnQkFBUSxFQUFFLHdJQUF3STtBQUNsSixZQUFJLEVBQUUsY0FBVSxLQUFLLEVBQUUsRUFBRSxFQUFFLEtBQUssRUFBRTtBQUM5QixpQkFBSyxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsVUFBVSxLQUFLLEVBQUU7QUFDckMsb0JBQUksS0FBSyxFQUFFO0FBQ1AseUJBQUssQ0FBQyxHQUFHLENBQUMscUJBQXFCLEdBQUcsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsSUFBSSxFQUFFO0FBQzFELDZCQUFLLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUE7cUJBRTVCLENBQUMsQ0FBQTtpQkFDTDthQUNKLENBQUMsQ0FBQztTQUNOO0tBQ0osQ0FBQTtDQUNKLENBQUMsQ0FBQyxDQUVGLFNBQVMsQ0FBQyxhQUFhLEVBQUUsQ0FBQyxPQUFPLEVBQUUsVUFBVSxLQUFLLEVBQUU7QUFDakQsV0FBTztBQUNILGdCQUFRLEVBQUUsR0FBRztBQUNiLGFBQUssRUFBRTtBQUNILHNCQUFVLEVBQUUsR0FBRztTQUNsQjtBQUNELGdCQUFRLEVBQUUsNElBQTRJO0FBQ3RKLFlBQUksRUFBRSxjQUFVLEtBQUssRUFBRSxFQUFFLEVBQUUsS0FBSyxFQUFFO0FBQzlCLG1CQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsQ0FBQTtBQUMxQixpQkFBSyxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsVUFBVSxLQUFLLEVBQUU7QUFDekMsdUJBQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxFQUFFLEtBQUssQ0FBQyxDQUFBO0FBQ25DLG9CQUFJLEtBQUssRUFBRTtBQUNQLHlCQUFLLENBQUMsR0FBRyxDQUFDLHFCQUFxQixHQUFHLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksRUFBRTtBQUMxRCw2QkFBSyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFBO3FCQUU3QixDQUFDLENBQUE7aUJBQ0w7YUFFSixDQUFDLENBQUM7U0FDTjtLQUNKLENBQUE7Q0FDSixDQUFDLENBQUMsQ0FFRixTQUFTLENBQUMsYUFBYSxFQUFFLENBQUMsT0FBTyxFQUFFLGFBQWEsRUFBRSxVQUFVLEtBQUssRUFBRSxXQUFXLEVBQUU7QUFDN0UsV0FBTztBQUNILGdCQUFRLEVBQUUsR0FBRztBQUNiLGFBQUssRUFBRTtBQUNILHNCQUFVLEVBQUUsR0FBRztTQUNsQjtBQUNELGdCQUFRLEVBQUUsa0NBQWtDO0FBQzVDLFlBQUksRUFBRSxjQUFVLEtBQUssRUFBRSxFQUFFLEVBQUUsS0FBSyxFQUFFO0FBQzlCLG1CQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsQ0FBQTtBQUMxQixpQkFBSyxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsVUFBVSxLQUFLLEVBQUU7QUFDekMsdUJBQU8sQ0FBQyxHQUFHLENBQUMsK0JBQStCLEVBQUUsS0FBSyxDQUFDLENBQUE7QUFDbkQsb0JBQUksV0FBVyxDQUFDLFVBQVUsRUFBRSxFQUFFO0FBQzFCLHdCQUFJLEtBQUssRUFBRTs7QUFFUCw2QkFBSyxDQUFDLEdBQUcsQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsT0FBTyxDQUFDLElBQUksRUFBRTtBQUMzRCxpQ0FBSyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFBO3lCQUM3QixFQUFFLFNBQVMsS0FBSyxDQUFDLEtBQUssRUFBRTtBQUNyQixtQ0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFBO0FBQzlCLG1DQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFBO3lCQUNyQixDQUFDLENBQUE7cUJBRUw7aUJBRUo7YUFHSixDQUFDLENBQUM7U0FDTjtLQUNKLENBQUE7Q0FDSixDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxFQUFFLENBQUMsU0FBUyxFQUFFLE1BQU0sRUFBRSxVQUFVLE9BQU8sRUFBRSxJQUFJLEVBQUU7QUFDakUsV0FBTztBQUNILGdCQUFRLEVBQUUsR0FBRztBQUNiLGdCQUFRLEVBQUUsb0JBQVk7QUFDbEIsbUJBQU8seUVBQXlFLENBQUM7U0FDcEY7QUFDRCxhQUFLLEVBQUU7QUFDSCxnQkFBSSxFQUFFLE9BQU87U0FDaEI7QUFDRCxZQUFJLEVBQUUsY0FBVSxLQUFLLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRTtBQUNuQyxpQkFBSyxDQUFDLFNBQVMsR0FBRyxZQUFZO0FBQzFCLHVCQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO2FBQ3hELENBQUE7U0FDSjtBQUNELGVBQU8sRUFBRSxJQUFJO0tBQ2hCLENBQUM7Q0FDTCxDQUFDLENBQUMsQ0FBQzs7O0FDdGtCUixZQUFZLENBQUM7Ozs7QUFJYixJQUFJLFNBQVMsR0FBRyxPQUFPLENBQUMseUJBQXlCLENBQUMsQ0FBQTs7QUFFbEQsT0FBTyxDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsRUFBRSxFQUFFLENBQUMsQ0FFaEMsU0FBUyxDQUFDLGVBQWUsRUFBRSxDQUFDLE9BQU8sRUFBRSxVQUFVLEtBQUssRUFBRTs7QUFFbkQsV0FBTztBQUNILGdCQUFRLEVBQUUsR0FBRztBQUNiLGFBQUssRUFBRTtBQUNILGVBQUcsRUFBRSxHQUFHO0FBQ1IsaUJBQUssRUFBRyxHQUFHO1NBQ2Q7QUFDRCxtQkFBVyxFQUFFLCtCQUErQjtBQUM1QyxZQUFJLEVBQUUsY0FBVSxLQUFLLEVBQUUsRUFBRSxFQUFFLEtBQUssRUFBRTtBQUM5QixtQkFBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUE7O0FBRTFCLGlCQUFLLENBQUMsVUFBVSxHQUFHLFVBQVUsS0FBSyxFQUFFOztBQUVoQyx1QkFBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsQ0FBQTtBQUMxQix1QkFBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtBQUNsQixvQkFBSSxFQUFFLEdBQUcsSUFBSSxRQUFRLEVBQUUsQ0FBQzs7O0FBR3hCLGtCQUFFLENBQUMsTUFBTSxDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztBQUNqQyx1QkFBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUE7O0FBRXJCLHFCQUFLLENBQUMsSUFBSSxDQUFDLG1CQUFtQixFQUFFLEVBQUUsRUFBRTtBQUNoQyxtQ0FBZSxFQUFFLElBQUk7QUFDckIsMkJBQU8sRUFBRTtBQUNMLHNDQUFjLEVBQUUsU0FBUztxQkFDNUI7QUFDRCxvQ0FBZ0IsRUFBRSxPQUFPLENBQUMsUUFBUTtpQkFDckMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUksRUFBRTtBQUN2Qix5QkFBSyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQTtBQUM3Qix5QkFBSyxDQUFDLGFBQWEsR0FBRyxVQUFVLENBQUE7aUJBQ25DLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBVSxJQUFJLEVBQUU7QUFDckIsMkJBQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFBO2lCQUM5QixDQUFDLENBQUM7YUFDTixDQUFBO1NBRUo7S0FDSixDQUFBO0NBQ0osQ0FBQyxDQUFDLENBRUYsU0FBUyxDQUFDLGtCQUFrQixFQUFFLENBQUMsT0FBTyxFQUFFLFVBQVUsS0FBSyxFQUFFO0FBQ3RELFdBQU87QUFDSCxnQkFBUSxFQUFFLEdBQUc7QUFDYixhQUFLLEVBQUU7QUFDSCxlQUFHLEVBQUUsR0FBRztBQUNSLGlCQUFLLEVBQUcsR0FBRzs7U0FFZDtBQUNELG1CQUFXLEVBQUUsa0NBQWtDO0FBQy9DLFlBQUksRUFBRSxjQUFVLEtBQUssRUFBRSxFQUFFLEVBQUUsS0FBSyxFQUFFOztBQUU5QixtQkFBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFBOztBQUdoQyxpQkFBSyxDQUFDLE1BQU0sR0FBRzs7QUFFWCwrQkFBZSxFQUFHLHVFQUF1RTthQUM1RixDQUFBOzs7Ozs7OztBQVNELGlCQUFLLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFBOzs7QUFHM0IsaUJBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxFQUFFLEVBQUU7QUFDMUIsb0JBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQTtBQUNwQixvQkFBSSxDQUFDLENBQUMsTUFBTSxLQUFLLENBQUMsRUFDZCxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUMsQ0FBQTtBQUNmLHFCQUFLLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDO0FBQ3hCLHdCQUFJLEVBQUUsQ0FBQztBQUNQLDRCQUFRLEVBQUUsS0FBSztpQkFDbEIsQ0FBQyxDQUFBO2FBQ0w7O0FBRUQseUJBQWEsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFBOztBQUVoQyxpQkFBSyxDQUFDLFlBQVksR0FBRyxVQUFVLENBQUMsRUFBRTs7QUFFOUIsb0JBQUksQ0FBQyxDQUFDLFFBQVEsRUFBRTs7QUFFWix5QkFBSyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO0FBQ25CLDRCQUFJLEVBQUUsQ0FBQyxDQUFDLElBQUk7QUFDWiwrQkFBTyxFQUFFLEVBQUU7cUJBQ2QsQ0FBQyxDQUFBO2lCQUVMLE1BQU07O0FBRUgseUJBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7QUFDL0MsNEJBQUksS0FBSyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLENBQUMsQ0FBQyxJQUFJLEVBQ3BDLEtBQUssQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUE7cUJBQ3JDO2lCQUNKO2FBRUosQ0FBQTs7QUFFRCxxQkFBUyxhQUFhLENBQUMsT0FBTyxFQUFFOztBQUU1QixxQkFBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQ2xELEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFBOztBQUU5QyxxQkFBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7QUFDckMsd0JBQUksSUFBSSxHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUE7QUFDcEMseUJBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQTtpQkFDbkQ7YUFDSjtTQUVKO0tBQ0osQ0FBQTtDQUNKLENBQUMsQ0FBQyxDQUFBOzs7QUN6SFAsWUFBWSxDQUFDOzs7OztBQUtiLE9BQU8sQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLEVBQUUsQ0FBQyxDQUMvQixNQUFNLENBQUMsU0FBUyxFQUFFLFlBQVk7QUFDNUIsU0FBTyxVQUFVLEtBQUssRUFBRSxTQUFTLEVBQUU7QUFDakMsUUFBSSxHQUFHLEdBQUcsRUFBRSxDQUFDO0FBQ2IsU0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7QUFDckMsU0FBRyxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDO0tBQzdCOztBQUVELFFBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQTtBQUNYLFFBQUksU0FBUyxFQUNYLEdBQUcsR0FBRyxHQUFHLENBQUMsV0FBVyxFQUFFLENBQUM7O0FBRTFCLFFBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQTtBQUNULFdBQU8sR0FBRyxDQUFDO0dBQ1osQ0FBQztDQUNILENBQUMsQ0FBQyxNQUFNLENBQUMsWUFBWSxFQUFFLFlBQVk7QUFDbEMsU0FBTyxVQUFVLEtBQUssRUFBRTtBQUN0QixRQUFJLE9BQU8sS0FBSyxBQUFDLEtBQUssUUFBUSxFQUFFO0FBQzlCLFVBQUksR0FBRyxHQUFHLEVBQUUsQ0FBQTtBQUNaLFdBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO0FBQ3JDLFlBQUksR0FBRyxLQUFLLEVBQUUsRUFBRTtBQUNkLGNBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sRUFDbEIsR0FBRyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFBLEtBRW5ELEdBQUcsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFBO1NBQ3RCLE1BQ0k7QUFDSCxjQUFJLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEVBQ2xCLEdBQUcsR0FBRyxHQUFHLEdBQUcsR0FBRyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFBLEtBRS9ELEdBQUcsR0FBRyxHQUFHLEdBQUcsR0FBRyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUE7U0FDbEM7T0FDRjtBQUNELGFBQU8sR0FBRyxDQUFBO0tBQ1gsTUFDQyxPQUFPLEtBQUssQ0FBQTtHQUNmLENBQUE7Q0FDRixDQUFDOzs7Q0FHRCxNQUFNLENBQUMsa0JBQWtCLEVBQUUsWUFBWTtBQUN0QyxTQUFPLFVBQVUsS0FBSyxFQUFFO0FBQ3RCLFFBQUksT0FBTyxLQUFLLEFBQUMsS0FBSyxRQUFRLEVBQUU7QUFDOUIsVUFBSSxHQUFHLEdBQUcsRUFBRSxDQUFBO0FBQ1osV0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7QUFDckMsWUFBSSxHQUFHLEtBQUssRUFBRSxFQUFFO0FBQ1osYUFBRyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUE7U0FDdEIsTUFDSTtBQUNELGFBQUcsR0FBRyxHQUFHLEdBQUcsR0FBRyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUE7U0FDbEM7T0FDRjtBQUNELGFBQU8sR0FBRyxDQUFBO0tBQ1gsTUFDQyxPQUFPLEtBQUssQ0FBQTtHQUNmLENBQUE7Q0FDRixDQUFDLENBR0QsTUFBTSxDQUFDLFdBQVcsRUFBRSxZQUFZO0FBQy9CLFNBQU8sVUFBVSxJQUFJLEVBQUU7QUFDckIsUUFBSSxJQUFJLEVBQ04sT0FBTyxLQUFLLENBQUEsS0FFWixPQUFPLElBQUksQ0FBQTtHQUNkLENBQUE7Q0FDRixDQUFDLENBQUMsTUFBTSxDQUFDLGNBQWMsRUFBRSxZQUFZO0FBQ3BDLFNBQU8sVUFBVSxDQUFDLEVBQUU7QUFDbEIsUUFBSSxDQUFDLEtBQUssR0FBRyxFQUNYLE9BQU8sVUFBVSxDQUFBLEtBQ2QsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUNoQixPQUFPLE1BQU0sQ0FBQSxLQUNWLElBQUksQ0FBQyxLQUFLLElBQUksRUFDakIsT0FBTyxtQkFBbUIsQ0FBQSxLQUN2QixJQUFJLENBQUMsS0FBSyxJQUFJLEVBQ2pCLE9BQU8sV0FBVyxDQUFBLEtBQ2YsSUFBSSxDQUFDLEtBQUssS0FBSyxFQUNsQixPQUFPLHFCQUFxQixDQUFBLEtBRTVCLE9BQU8sU0FBUyxDQUFBO0dBQ25CLENBQUE7Q0FDRixDQUFDLENBQUMsTUFBTSxDQUFDLFVBQVUsRUFBRSxDQUFDLFVBQVUsRUFBQyxVQUFVLFFBQVEsRUFBRTtBQUNwRCxTQUFPLFVBQVUsQ0FBQyxFQUFFLE9BQU8sRUFBRTtBQUMzQixRQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsRUFDVixPQUFPLHdEQUF3RCxDQUFBLEtBQzVELElBQUksQ0FBQyxLQUFLLENBQUMsRUFDZCxPQUFPLGdFQUFnRSxDQUFBLEtBQ3BFO0FBQ0gsVUFBSSxLQUFLLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFBO0FBQzdCLFVBQUksS0FBSyxLQUFLLFNBQVMsRUFBRTtBQUN2QixlQUFPLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQTtPQUNoQixNQUFNO0FBQ0wsaUJBQU8seUJBQXlCLEdBQUcsQ0FBQyxDQUFDO1NBQ3RDO0tBQ0Y7R0FDRixDQUFBO0NBQ0YsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLGNBQWMsRUFBRSxDQUFDLFdBQVcsRUFBQyxVQUFVLFNBQVMsRUFBRTtBQUMzRCxTQUFPLFVBQVUsQ0FBQyxFQUFFO0FBQ2xCLFNBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxTQUFTLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRyxFQUFFO0FBQzFDLFVBQUksU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksS0FBSyxDQUFDLEVBQ3pCLE9BQU8sU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQTtLQUMzQjtHQUVGLENBQUE7Q0FDRixDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLFlBQVk7QUFDcEMsU0FBTyxVQUFVLENBQUMsRUFBRTs7QUFFbEIsUUFBSSxDQUFDLEtBQUssQ0FBQyxFQUNULE9BQU8sU0FBUyxDQUFBLEtBQ2IsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUNkLE9BQU8sV0FBVyxDQUFBLEtBQ2YsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUNkLE9BQU8sV0FBVyxDQUFBLEtBRWxCLE9BQU8sU0FBUyxDQUFBO0dBQ25CLENBQUE7Q0FDRixDQUFDLENBRUQsTUFBTSxDQUFDLGNBQWMsRUFBRSxZQUFZO0FBQ2xDLFNBQU8sVUFBVSxDQUFDLEVBQUU7O0FBRWxCLFFBQUksQ0FBQyxLQUFLLENBQUMsRUFDVCxPQUFPLFFBQVEsQ0FBQSxLQUNaLElBQUksQ0FBQyxLQUFLLENBQUMsRUFDZCxPQUFPLFFBQVEsQ0FBQSxLQUNaLElBQUksQ0FBQyxLQUFLLENBQUMsRUFDZCxPQUFPLFdBQVcsQ0FBQSxLQUVsQixPQUFPLFNBQVMsQ0FBQTtHQUNuQixDQUFBO0NBQ0YsQ0FBQyxDQUVGLE1BQU0sQ0FBQyxjQUFjLEVBQUUsWUFBWTtBQUNqQyxTQUFPLFVBQVUsSUFBSSxFQUFFO0FBQ3JCLFFBQUksSUFBSSxLQUFLLFNBQVMsRUFBRTs7QUFFdEIsYUFBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsT0FBTyxJQUFJLENBQUMsQ0FBQTtBQUM5QixhQUFPLGVBQWUsR0FBRyxJQUFJLENBQUMsV0FBVyxFQUFFLEdBQUcsTUFBTSxDQUFBO0tBQ3JEOztHQUVGLENBQUE7Q0FDRixDQUFDLENBQUMsTUFBTSxDQUFDLGNBQWMsRUFBRSxDQUFDLFNBQVMsRUFBQyxVQUFVLE9BQU8sRUFBRTtBQUN0RCxTQUFPLFVBQVUsSUFBSSxFQUFFLFFBQVEsRUFBRTtBQUMvQixRQUFJLElBQUksS0FBSyxTQUFTLEVBQUU7QUFDcEIsVUFBSSxJQUFJLEtBQUssQ0FBQyxDQUFDLEVBQ2IsT0FBTyxnQkFBZ0IsQ0FBQSxLQUNwQixJQUFJLElBQUksS0FBSyxDQUFDLEVBQ2pCLE9BQU8sSUFBSSxDQUFBLEtBQ1IsSUFBSSxJQUFJLEtBQUssQ0FBQyxFQUNqQixPQUFPLHdCQUF3QixHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxRQUFRLEVBQUUsWUFBWSxDQUFDLEdBQUUsR0FBRyxDQUFBLEtBQzNFLElBQUksSUFBSSxLQUFLLENBQUMsRUFDakIsT0FBTywyQkFBMkIsQ0FBQSxLQUVsQyxPQUFPLGdCQUFnQixHQUFHLElBQUksQ0FBQTtLQUNuQyxNQUNDLE9BQU8sU0FBUyxDQUFBO0dBQ25CLENBQUE7Q0FDRixDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsaUJBQWlCLEVBQUUsWUFBWTtBQUN4QyxTQUFPLFVBQVUsQ0FBQyxFQUFFO0FBQ2xCLFFBQUksQ0FBQyxLQUFLLEdBQUcsRUFDWCxPQUFPLE9BQU8sQ0FBQSxLQUNYLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFDaEIsT0FBTyxXQUFXLENBQUEsS0FDZixJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsRUFDZixPQUFPLEVBQUUsQ0FBQSxLQUVULE9BQU8sU0FBUyxDQUFBO0dBQ25CLENBQUE7Q0FDRixDQUFDLENBQUMsTUFBTSxDQUFDLFlBQVksRUFBRSxZQUFZO0FBQ2xDLFNBQU8sVUFBVSxDQUFDLEVBQUU7QUFDbEIsUUFBSSxDQUFDLEtBQUssU0FBUyxFQUFFO0FBQ25CLFVBQUksQ0FBQyxLQUFLLEdBQUcsRUFDWCxPQUFPLE9BQU8sQ0FBQSxLQUNYLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFDaEIsT0FBTyxPQUFPLENBQUE7S0FDakI7R0FDRixDQUFBO0NBQ0YsQ0FBQyxDQUNELE1BQU0sQ0FBQyxjQUFjLEVBQUUsWUFBWTtBQUNsQyxTQUFPLFVBQVUsQ0FBQyxFQUFFO0FBQ2xCLFFBQUksQ0FBQyxLQUFLLFNBQVMsRUFBRTtBQUNuQixVQUFJLEdBQUcsQ0FBQTtBQUNQLFdBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRyxFQUFFO0FBQ2xDLFlBQUksR0FBRyxFQUNMLEdBQUcsR0FBRyxHQUFHLEdBQUcsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUEsS0FFM0IsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUE7T0FDbEI7QUFDRCxhQUFPLEdBQUcsQ0FBQTtLQUVYO0dBQ0YsQ0FBQTtDQUNGLENBQUMsQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLFlBQVk7QUFDakMsU0FBTyxVQUFVLENBQUMsRUFBRTtBQUNsQixRQUFJLENBQUMsS0FBSyxTQUFTLEVBQUU7QUFDakIsYUFBTyxJQUFJLEdBQUcsQ0FBQyxDQUFBO0tBQ2xCO0dBQ0YsQ0FBQTtDQUNGLENBQUMsQ0FBQTs7O0FDM01OLFlBQVksQ0FBQzs7QUFFYixJQUFJLFNBQVMsR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLGVBQWUsRUFBRSxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsU0FBUyxFQUFFLEtBQUssQ0FBQyxDQUFDOztBQUU1RSxTQUFTLENBQUMsUUFBUSxDQUFDLGFBQWEsRUFBRTs7QUFFaEMsY0FBWSxFQUFFLG9FQUFvRSxHQUNoRixxRkFBcUYsR0FDckYsZ0dBQWdHO0FBQ2xHLFFBQU0sRUFBRSxnQ0FBZ0M7QUFDeEMsZUFBYSxFQUFDLDJEQUEyRCxHQUN2RSx3SUFBd0ksR0FDeEksd0lBQXdJLEdBQ3hJLHNIQUFzSDtBQUN4SCxZQUFVLEVBQUcseUhBQXlILEdBQ3BJLDJJQUEySSxHQUMzSSw2SUFBNkksR0FDN0ksNklBQTZJLEdBQzdJLDJEQUEyRDtBQUM3RCxtQkFBaUIsRUFBRyx1VEFBdVQ7Q0FDNVUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUU7O0FBRXRCLE1BQUksRUFBRTtBQUNFLEtBQUMsRUFBRSxtRUFBbUU7QUFDdEUsS0FBQyxFQUFFLG1FQUFtRTtHQUN2RTtBQUNQLE1BQUksRUFBRTtBQUNKLEtBQUMsRUFBRSxrRUFBa0U7QUFDckUsS0FBQyxFQUFFLG1FQUFtRTtHQUN2RTtBQUNELE1BQUksRUFBRTtBQUNKLEtBQUMsRUFBRSxrRUFBa0U7QUFDckUsS0FBQyxFQUFFLG1FQUFtRTtHQUN2RTtBQUNELE1BQUksRUFBRTtBQUNKLEtBQUMsRUFBRSxrRUFBa0U7QUFDckUsS0FBQyxFQUFFLG1FQUFtRTtHQUN2RTtBQUNELE1BQUksRUFBRTtBQUNKLEtBQUMsRUFBRSxrRUFBa0U7QUFDckUsS0FBQyxFQUFFLG1FQUFtRTtHQUN2RTtBQUNELE1BQUksRUFBRTtBQUNKLEtBQUMsRUFBRSxrRUFBa0U7QUFDckUsS0FBQyxFQUFFLG1FQUFtRTtHQUN2RTtBQUNELE1BQUksRUFBRTtBQUNKLEtBQUMsRUFBRSx1RUFBdUU7QUFDMUUsS0FBQyxFQUFFLG1FQUFtRTtHQUN2RTtBQUNELE1BQUksRUFBRTtBQUNKLEtBQUMsRUFBRSx1RUFBdUU7QUFDMUUsS0FBQyxFQUFFLG1FQUFtRTtHQUN2RTtBQUNELE1BQUksRUFBRTtBQUNKLEtBQUMsRUFBRSxpRUFBaUU7QUFDcEUsS0FBQyxFQUFFLG1FQUFtRTtHQUN2RTtBQUNELE1BQUksRUFBRTtBQUNKLEtBQUMsRUFBRSxpRUFBaUU7QUFDcEUsS0FBQyxFQUFFLG1FQUFtRTtHQUN2RTtBQUNELE1BQUksRUFBRTtBQUNKLEtBQUMsRUFBRSx1RUFBdUU7QUFDMUUsS0FBQyxFQUFFLG1FQUFtRTtHQUN2RTtBQUNELE1BQUksRUFBRTtBQUNKLEtBQUMsRUFBRSxpRUFBaUU7QUFDcEUsS0FBQyxFQUFFLG1FQUFtRTtHQUN2RTtBQUNELE1BQUksRUFBRSxFQUFDLENBQUMsRUFBRSxFQUFFLEVBQUUsQ0FBQyxFQUFFLEVBQUUsRUFBQztBQUNwQixRQUFNLEVBQUM7QUFDTCxRQUFJLEVBQUMsb0pBQW9KO0dBQzFKO0FBQ0QsWUFBVSxFQUFFLDJJQUEySTs7Q0FFeEosQ0FBQyxDQUFBOzs7QUM1RUYsWUFBWSxDQUFDOztBQUViLE9BQU8sQ0FBQyxNQUFNLENBQUMsZUFBZSxFQUFFLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDOzs7O0NBSXhELE9BQU8sQ0FBQyxTQUFTLEVBQUUsQ0FBQyxTQUFTLEVBQUUsY0FBYyxFQUFFLFVBQVUsT0FBTyxFQUFFLFlBQVksRUFBRTtBQUMvRSxNQUFJLENBQUMsTUFBTSxHQUFHLFVBQVUsU0FBUyxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFOztBQUV2RCxnQkFBWSxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsU0FBUyxDQUFDLENBQUE7QUFDeEMsZ0JBQVksQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFBO0FBQzlCLGdCQUFZLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQTtBQUNsQyxnQkFBWSxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUE7R0FFbkMsQ0FBQzs7QUFFRixNQUFJLENBQUMsT0FBTyxHQUFHLFlBQVk7O0FBRXpCLGdCQUFZLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFBO0FBQ2hDLGdCQUFZLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFBO0FBQzNCLGdCQUFZLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFBO0FBQzdCLGdCQUFZLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFBO0dBQzlCLENBQUM7O0FBRUYsTUFBSSxDQUFDLFlBQVksR0FBRyxZQUFZO0FBQzlCLFdBQU8sWUFBWSxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQTtHQUNyQyxDQUFBOztBQUVELE1BQUksQ0FBQyxPQUFPLEdBQUcsWUFBWTtBQUN6QixXQUFPLFlBQVksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUE7R0FDaEMsQ0FBQTs7QUFFRCxNQUFJLENBQUMsU0FBUyxHQUFHLFlBQVk7QUFDM0IsV0FBTyxZQUFZLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFBO0dBQ2xDLENBQUE7O0FBRUQsU0FBTyxJQUFJLENBQUM7Q0FDYixDQUFDLENBQUM7Ozs7Q0FJRixPQUFPLENBQUMsYUFBYSxFQUFFLENBQUMsT0FBTyxFQUFFLElBQUksRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLFVBQVUsS0FBSyxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsT0FBTyxFQUFFOztBQUVyRyxNQUFJLEtBQUssR0FBRyxFQUFFLENBQUM7O0FBRWYsTUFBSSxZQUFZLEdBQUcsU0FBZixZQUFZLENBQWEsSUFBSSxFQUFFO0FBQ2pDLFFBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQzs7QUFFMUIsU0FBSyxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQzFCLE9BQU8sQ0FBQyxVQUFVLElBQUksRUFBRSxNQUFNLEVBQUU7QUFDOUIsYUFBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQTtBQUNqQixhQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFBO0FBQ25CLFVBQUksSUFBSSxDQUFDLFNBQVMsS0FBSyxJQUFJLEVBQ3pCLE9BQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0FBQ3RFLGNBQVEsQ0FBQyxPQUFPLENBQUMsRUFBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLFNBQVMsS0FBSyxJQUFJLEVBQUUsT0FBTyxFQUFFLElBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLEVBQUMsQ0FBQyxDQUFDO0tBQ2hHLENBQUMsQ0FDRixLQUFLLENBQUMsVUFBVSxLQUFLLEVBQUU7QUFDckIsY0FBUSxDQUFDLE9BQU8sQ0FBQyxFQUFDLGFBQWEsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLHNDQUFzQyxFQUFDLENBQUMsQ0FBQztLQUMzRixDQUFDLENBQUM7O0FBRUwsV0FBTyxRQUFRLENBQUMsT0FBTyxDQUFDO0dBQ3pCLENBQUM7O0FBRUYsTUFBSSxlQUFlLEdBQUcsU0FBbEIsZUFBZSxHQUFlO0FBQ2hDLFFBQUksR0FBRyxHQUFHLE9BQU8sQ0FBQyxZQUFZLEVBQUUsQ0FBQTtBQUNoQyxRQUFJLEFBQUMsR0FBRyxLQUFLLFNBQVMsSUFBTSxHQUFHLEtBQUssSUFBSSxBQUFDLEVBQ3ZDLE9BQU8sS0FBSyxDQUFBLEtBRVosT0FBTyxJQUFJLENBQUE7R0FFZCxDQUFDOztBQUVGLE1BQUksUUFBUSxHQUFHLFNBQVgsUUFBUSxDQUFhLElBQUksRUFBRTtBQUM3QixRQUFJLFFBQVEsR0FBRyxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7O0FBRTFCLFNBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUN6QixPQUFPLENBQUMsVUFBVSxJQUFJLEVBQUUsTUFBTSxFQUFFO0FBQzlCLGNBQVEsQ0FBQyxPQUFPLENBQUMsRUFBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFVBQVUsRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU8sRUFBQyxDQUFDLENBQUM7S0FDeEUsQ0FBQyxDQUNGLEtBQUssQ0FBQyxVQUFVLEtBQUssRUFBRTtBQUNyQixjQUFRLENBQUMsT0FBTyxDQUFDLEVBQUMsVUFBVSxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsMkRBQTJELEVBQUMsQ0FBQyxDQUFDO0tBQzdHLENBQUMsQ0FBQzs7QUFFTCxXQUFPLFFBQVEsQ0FBQyxPQUFPLENBQUM7R0FDekIsQ0FBQzs7QUFFRixNQUFJLE1BQU0sR0FBRyxTQUFULE1BQU0sR0FBZTtBQUN2QixXQUFPLENBQUMsT0FBTyxFQUFFLENBQUE7R0FDbEIsQ0FBQzs7QUFFRixNQUFJLHNCQUFzQixHQUFHLFNBQXpCLHNCQUFzQixDQUFhLEtBQUssRUFBRTs7QUFFNUMsUUFBSSxRQUFRLEdBQUcsRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDOztBQUUxQixTQUFLLENBQUMsR0FBRyxDQUFDLDZCQUE2QixHQUFHLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUksRUFBRTtBQUN2RSxhQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFBO0FBQ2pCLGNBQVEsQ0FBQyxPQUFPLENBQUMsRUFBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU0sS0FBSyxJQUFJLEVBQUUsT0FBTyxFQUFFLElBQUksQ0FBQyxPQUFPLEVBQUMsQ0FBQyxDQUFDO0tBQ3pFLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBVSxLQUFLLEVBQUU7QUFDeEIsY0FBUSxDQUFDLE9BQU8sQ0FBQyxFQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLGlFQUFpRSxFQUFDLENBQUMsQ0FBQztLQUMvRyxDQUFDLENBQUM7O0FBRUgsV0FBTyxRQUFRLENBQUMsT0FBTyxDQUFDO0dBQ3pCLENBQUM7O0FBR0YsT0FBSyxDQUFDLE1BQU0sR0FBRyxVQUFVLElBQUksRUFBRTtBQUM3QixXQUFPLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztHQUMzQixDQUFDO0FBQ0YsT0FBSyxDQUFDLE1BQU0sR0FBRyxVQUFVLElBQUksRUFBRTtBQUM3QixXQUFPLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztHQUN2QixDQUFDO0FBQ0YsT0FBSyxDQUFDLE1BQU0sR0FBRyxZQUFZO0FBQ3pCLFdBQU8sTUFBTSxFQUFFLENBQUE7R0FDaEIsQ0FBQztBQUNGLE9BQUssQ0FBQyxVQUFVLEdBQUcsWUFBWTtBQUM3QixXQUFPLGVBQWUsRUFBRSxDQUFBO0dBQ3pCLENBQUM7QUFDRixPQUFLLENBQUMsUUFBUSxHQUFHLFlBQVk7QUFDM0IsV0FBTyxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUE7R0FDekIsQ0FBQzs7QUFFRixPQUFLLENBQUMsc0JBQXNCLEdBQUcsVUFBVSxLQUFLLEVBQUU7QUFDOUMsV0FBTyxzQkFBc0IsQ0FBQyxLQUFLLENBQUMsQ0FBQTtHQUNyQyxDQUFBOztBQUVELFNBQU8sS0FBSyxDQUFDO0NBRWQsQ0FBQyxDQUFDLENBRUYsUUFBUSxDQUFDLGFBQWEsRUFBRTtBQUN2QixjQUFZLEVBQUUsb0JBQW9CO0FBQ2xDLGFBQVcsRUFBRSxtQkFBbUI7QUFDaEMsZUFBYSxFQUFFLHFCQUFxQjtBQUNwQyxnQkFBYyxFQUFFLHNCQUFzQjtBQUN0QyxrQkFBZ0IsRUFBRSx3QkFBd0I7QUFDMUMsZUFBYSxFQUFFLHFCQUFxQjtBQUNwQyxhQUFXLEVBQUUsY0FBYztDQUM1QixDQUFDOzs7OztDQUtELE9BQU8sQ0FBQyxTQUFTLEVBQUMsQ0FBQyxTQUFTLEVBQUUsVUFBVSxPQUFPLEVBQUU7O0FBRWhELE1BQUksQ0FBQyxNQUFNLEdBQUcsWUFBWSxFQUN6QixDQUFDOztBQUVGLE1BQUksQ0FBQyxPQUFPLEdBQUcsWUFBWTtBQUN6QixXQUFPLE9BQU8sQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFBO0dBQ3RDLENBQUM7O0FBRUYsTUFBSSxDQUFDLEdBQUcsR0FBRyxVQUFVLEdBQUcsRUFBRTtBQUN4QixRQUFJLElBQUksR0FBRyxLQUFLLEdBQUcsR0FBRyxDQUFBO0FBQ3RCLFFBQUksR0FBRyxHQUFHLE9BQU8sQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUE7QUFDdEMsV0FBTyxPQUFPLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFBO0FBQ25DLFdBQU8sR0FBRyxDQUFBO0dBQ1gsQ0FBQTs7QUFFRCxXQUFTLGlCQUFpQixHQUFHO0FBQzNCLFdBQU8sSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxHQUNoRCxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7R0FDL0M7O0FBRUQsTUFBSSxDQUFDLEdBQUcsR0FBRyxVQUFVLEdBQUcsRUFBRTtBQUN4QixRQUFJLEdBQUcsR0FBRyxpQkFBaUIsRUFBRSxDQUFBO0FBQzdCLFdBQU8sQ0FBQyxjQUFjLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQyxHQUFHLEdBQUcsQ0FBQTtBQUN6QyxXQUFPLEdBQUcsQ0FBQTtHQUNYLENBQUE7O0FBRUQsU0FBTyxJQUFJLENBQUM7Q0FDYixDQUFDLENBQUMsQ0FBQTs7O0FDMUtMLFlBQVksQ0FBQzs7O0FBR2IsT0FBTyxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsRUFBRSxDQUFDLENBQzVCLE1BQU0sQ0FDUCxPQUFPLEVBQUUsWUFBWTtBQUNuQixNQUFJLE1BQU0sR0FDUixTQURFLE1BQU0sQ0FDRSxHQUFHLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRTtBQUMzQixTQUFLLElBQUksQ0FBQyxHQUFHLEtBQUssRUFBRSxDQUFDLElBQUksS0FBSyxFQUFFLENBQUMsRUFBRSxFQUFFLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUE7QUFDaEQsV0FBTyxHQUFHLENBQUE7R0FDWCxDQUFBO0FBQ0gsU0FBTyxNQUFNLENBQUE7Q0FDZCxDQUNGLENBQ0UsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDLE9BQU8sRUFBRSxJQUFJLEVBQUUsVUFBVSxLQUFLLEVBQUUsRUFBRSxFQUFFOztBQUVyRCxNQUFJLENBQUMscUJBQXFCLEdBQUcsVUFBVSxPQUFPLEVBQUU7QUFDOUMsUUFBSSxRQUFRLEdBQUcsRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDOztBQUUxQixTQUFLLENBQUMsR0FBRyxDQUFDLHFDQUFxQyxHQUFHLE9BQU8sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUksRUFBRTtBQUNqRixhQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQixDQUFDLENBQUE7QUFDakMsYUFBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQTtBQUNqQixjQUFRLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO0tBQ3hCLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBVSxLQUFLLEVBQUU7QUFDeEIsY0FBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztLQUN6QixDQUFDLENBQUM7QUFDSCxXQUFPLFFBQVEsQ0FBQyxPQUFPLENBQUM7R0FDekIsQ0FBQTs7OztBQUlELE1BQUksQ0FBQyxhQUFhLEdBQUcsVUFBVSxPQUFPLEVBQUU7QUFDdEMsUUFBSSxRQUFRLEdBQUcsRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDOztBQUUxQixTQUFLLENBQUMsR0FBRyxDQUFDLDZCQUE2QixHQUFHLE9BQU8sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUksRUFBRTtBQUN6RSxhQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQixDQUFDLENBQUE7QUFDakMsYUFBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQTtBQUNqQixjQUFRLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO0tBQ3hCLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBVSxLQUFLLEVBQUU7QUFDeEIsY0FBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztLQUN6QixDQUFDLENBQUM7Ozs7Ozs7Ozs7Ozs7Ozs7QUFnQkgsV0FBTyxRQUFRLENBQUMsT0FBTyxDQUFDO0dBQ3pCLENBQUE7O0FBR0QsU0FBTyxJQUFJLENBQUM7Q0FFYixDQUFDLENBQUMsQ0FBQTs7O0FDOURMLFlBQVksQ0FBQzs7O0FBR2IsSUFBSSxTQUFTLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFBOztBQUVwQyxPQUFPLENBQUMsTUFBTSxDQUFDLGVBQWUsRUFBRSxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLFlBQVc7QUFDaEUsV0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtBQUNsQixRQUFJLENBQUMsR0FBRyxJQUFJLENBQUE7QUFDWixRQUFJLENBQUMsS0FBSyxJQUFJLEVBQ1osT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFBO0FBQ3BDLFdBQU8sU0FBUyxDQUFDLGFBQWEsRUFBRSxDQUFBO0NBQ25DLENBQUMsQ0FBQTs7O0FDWEYsWUFBWSxDQUFDOzs7QUFHYixPQUFPLENBQUMsTUFBTSxDQUFDLFdBQVcsRUFBRSxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxFQUFFLENBQUMsV0FBVyxFQUFFLFVBQVUsU0FBUyxFQUFFO0FBQ3JGLFNBQU87QUFDTCxZQUFRLEVBQUUsR0FBRztBQUNiLFNBQUssRUFBRTtBQUNMLGFBQU8sRUFBRSxHQUFHO0FBQ1osZUFBUyxFQUFFLEdBQUc7QUFDZCxtQkFBYSxFQUFFLEdBQUc7S0FDbkI7QUFDRCxlQUFXLEVBQUUsc0JBQXNCO0FBQ25DLFFBQUksRUFBRSxjQUFVLEtBQUssRUFBRSxnQkFBZ0IsRUFBRSxLQUFLLEVBQUU7O0FBRTlDLFdBQUssQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFBO0FBQ25CLFdBQUssQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFBOztBQUVwQixXQUFLLENBQUMsV0FBVyxHQUFHLFlBQVk7QUFDOUIsWUFBSSxLQUFLLENBQUMsU0FBUyxLQUFLLFNBQVMsRUFDL0IsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFBLEtBRW5ELE9BQU8sQ0FBQyxDQUFDO09BQ1osQ0FBQTs7QUFFRCxXQUFLLENBQUMsVUFBVSxHQUFHLFlBQVk7QUFDN0IsWUFBSSxHQUFHLEdBQUcsRUFBRTtZQUFFLENBQUM7WUFBRSxDQUFDLENBQUE7QUFDbEIsWUFBSSxLQUFLLENBQUMsU0FBUyxJQUFJLFNBQVMsRUFBRTs7QUFDaEMsV0FBQyxHQUFHLEVBQUUsQ0FBQTtBQUNOLGNBQUksS0FBSyxDQUFDLFNBQVMsR0FBRyxDQUFDLEVBQ3JCLENBQUMsR0FBRyxLQUFLLENBQUMsU0FBUyxDQUFBO0FBQ3JCLGVBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUNwQixHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQTtTQUNsQjtBQUNELGVBQU8sR0FBRyxDQUFBO09BQ1gsQ0FBQTs7QUFFRCxXQUFLLENBQUMsS0FBSyxHQUFHLFlBQVk7QUFDeEIsWUFBSSxHQUFHLEdBQUcsRUFBRTtZQUFFLENBQUMsQ0FBQztBQUNoQixhQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxTQUFTLEVBQUUsQ0FBQyxFQUFFLEVBQ2xDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFBO0FBQ2pCLGVBQU8sR0FBRyxDQUFBO09BQ1gsQ0FBQTs7O0FBR0QsV0FBSyxDQUFDLHNCQUFzQixHQUFHLFlBQVk7O0FBRXpDLFlBQUksS0FBSyxDQUFDLFNBQVMsS0FBSyxTQUFTLEVBQUU7QUFDakMsY0FBSSxLQUFLLENBQUMsU0FBUyxHQUFHLENBQUMsRUFBRTtBQUN2QixnQkFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQTtBQUNuRCxnQkFBSSxDQUFDLEtBQUssS0FBSyxDQUFDLFNBQVMsRUFDdkIsS0FBSyxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUE7V0FDdEI7U0FDRjtPQUVGLENBQUE7O0FBRUQsV0FBSyxDQUFDLGtCQUFrQixHQUFHLFlBQVk7QUFDckMsWUFBSSxLQUFLLENBQUMsU0FBUyxLQUFLLFNBQVMsRUFBRTtBQUNqQyxjQUFJLEdBQUcsR0FBRyxFQUFFO2NBQUUsQ0FBQyxDQUFDO0FBQ2hCLGVBQUssQ0FBQyxzQkFBc0IsRUFBRSxDQUFBO0FBQzlCLGNBQUksTUFBTSxHQUFHLEtBQUssQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDLFNBQVMsQ0FBQTtBQUM5QyxlQUFLLENBQUMsR0FBRyxNQUFNLEVBQUUsQ0FBQyxHQUFHLE1BQU0sR0FBRyxLQUFLLENBQUMsU0FBUyxFQUFFLENBQUMsRUFBRSxFQUFFO0FBQ2xELG1CQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFBO0FBQ3RDLGdCQUFJLENBQUMsR0FBRyxLQUFLLENBQUMsU0FBUyxFQUNyQixHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQTtXQUNsQjtBQUNELGlCQUFPLEdBQUcsQ0FBQTtTQUNYO09BQ0YsQ0FBQTs7QUFFRCxXQUFLLENBQUMsU0FBUyxHQUFHLFlBQVk7QUFDNUIsWUFBSSxHQUFHLEdBQUcsRUFBRTtZQUFFLENBQUMsQ0FBQztBQUNoQixhQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxTQUFTLEVBQUUsQ0FBQyxFQUFFLEVBQ2xDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFBO0FBQ2pCLGVBQU8sR0FBRyxDQUFBO09BQ1gsQ0FBQTs7QUFFRCxXQUFLLENBQUMsZUFBZSxHQUFHLFVBQVUsQ0FBQyxFQUFFO0FBQ25DLGFBQUssQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFBO0FBQ2pCLFlBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxhQUFhLEtBQUssU0FBUyxFQUFFO0FBQzdDLGVBQUssQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQTtBQUMxQyxtQkFBUyxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFBO1NBQ25DO09BQ0YsQ0FBQTs7QUFFRCxXQUFLLENBQUMsUUFBUSxHQUFHLFlBQVk7O0FBRTNCLGVBQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFBO0FBQ3ZDLFlBQUksQ0FBQyxHQUFHLEtBQUssQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFBOztBQUUzQixZQUFJLENBQUMsR0FBRyxLQUFLLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQyxTQUFTLEVBQ3ZDLEtBQUssQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFBOztBQUVyQixlQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFBO0FBQzNDLGVBQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUE7O0FBRXhCLFlBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUE7T0FDeEQsQ0FBQTs7QUFFRCxXQUFLLENBQUMsUUFBUSxHQUFHLFlBQVk7QUFDM0IsWUFBSSxLQUFLLENBQUMsU0FBUyxHQUFHLENBQUMsRUFDckIsS0FBSyxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQTtBQUN2QyxlQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFBO0FBQ3hCLFlBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUE7T0FDeEQsQ0FBQTs7QUFHRCxXQUFLLENBQUMsT0FBTyxHQUFHLFVBQVUsQ0FBQyxFQUFFO0FBQzNCLFlBQUksQ0FBQyxLQUFLLEtBQUssQ0FBQyxPQUFPLEVBQ3JCLE9BQU8sUUFBUSxDQUFBLEtBRWYsT0FBTyxFQUFFLENBQUE7T0FDWixDQUFBOztBQUVELFdBQUssQ0FBQyxJQUFJLEdBQUcsWUFBWTtBQUN2QixlQUFPLEtBQUssQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDO09BQzVCLENBQUE7O0FBRUQsV0FBSyxDQUFDLFFBQVEsR0FBRyxZQUFZO0FBQzNCLGVBQU8sS0FBSyxDQUFDLGFBQWEsQ0FBQztPQUM1QixDQUFBOztBQUVELFdBQUssQ0FBQyxhQUFhLEdBQUcsWUFBWTtBQUNoQyxhQUFLLENBQUMsYUFBYSxHQUFHLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQTtPQUMzQyxDQUFBO0tBQ0Y7R0FDRixDQUFBO0NBQ0YsQ0FBQyxDQUFDLENBQUE7Ozs7O0FDL0hILE1BQU0sQ0FBQyxPQUFPLEdBQUcsQ0FDZixPQUFPLENBQUMsMkJBQTJCLENBQUMsRUFDcEMsT0FBTyxDQUFDLDBCQUEwQixDQUFDLEVBQ25DLE9BQU8sQ0FBQyw2QkFBNkIsQ0FBQyxFQUN0QyxPQUFPLENBQUMsOEJBQThCLENBQUMsRUFDdkMsT0FBTyxDQUFDLGdDQUFnQyxDQUFDLEVBQ3pDLE9BQU8sQ0FBQyxvQkFBb0IsQ0FBQyxFQUM3QixPQUFPLENBQUMsb0JBQW9CLENBQUMsRUFDN0IsT0FBTyxDQUFDLGdCQUFnQixDQUFDLEVBQ3pCLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUMxQixDQUFBOzs7QUNWRCxZQUFZLENBQUM7QUFDYixJQUFJLFNBQVMsR0FBRyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUE7QUFDcEMsSUFBSSxhQUFhLEdBQUcsT0FBTyxDQUFDLDRCQUE0QixDQUFDLENBQUE7QUFDekQsSUFBSSxJQUFJLEdBQUcsT0FBTyxDQUFDLHlCQUF5QixDQUFDLENBQUE7QUFDN0MsSUFBSSxLQUFLLEdBQUcsT0FBTyxDQUFDLDBCQUEwQixDQUFDLENBQUE7O0FBRS9DLFNBQVMsWUFBWSxDQUFDLEtBQUssRUFBQztBQUMxQixNQUFJLEdBQUcsR0FBRywyQkFBMkIsQ0FBQztBQUN0QyxNQUFJLE9BQU8sR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0FBQy9CLE1BQUksT0FBTyxLQUFLLElBQUksRUFBRTtBQUNwQixXQUFPLEVBQUUsS0FBSyxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7R0FDakQ7QUFDRCxTQUFPLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLENBQUM7Q0FDckM7O0FBRUQsU0FBUyxzQkFBc0IsR0FBSTs7QUFFakMsTUFBSSxHQUFHLEdBQUcsRUFBRSxDQUFBO0FBQ1osTUFBSSxHQUFHLEdBQUcsQ0FBQyxDQUFDO0FBQ1osT0FBSyxJQUFJLEdBQUcsSUFBSSxhQUFhLEVBQUU7QUFDN0IsUUFBSSxhQUFhLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxFQUFFOztBQUVyQyxVQUFLLFNBQVMsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEtBQUssU0FBUyxFQUFFO0FBQzdDLFdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFDLElBQUksRUFBRSxTQUFTLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFDLENBQUE7QUFDOUQsV0FBRyxHQUFHLEdBQUcsR0FBRyxFQUFFLENBQUE7T0FDZixNQUFNO0FBQ0wsZUFBTyxDQUFDLEdBQUcsQ0FBQywwQkFBMEIsRUFBRSxHQUFHLENBQUMsQ0FBQTtPQUM3QztLQUNGO0dBQ0Y7O0FBRUQsT0FBTSxHQUFHLElBQUksSUFBSSxFQUFFO0FBQ2pCLFFBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsRUFBRTtBQUM1QixVQUFLLFNBQVMsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEtBQUssU0FBUyxFQUFFO0FBQzdDLFdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFDLElBQUksRUFBRSxTQUFTLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBQyxNQUFNLEVBQUMsQ0FBQTtPQUM1RTtLQUNGO0dBQ0Y7OztBQUlELE9BQU0sR0FBRyxJQUFJLEtBQUssRUFBRTtBQUNsQixRQUFJLEtBQUssQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEVBQUU7QUFDN0IsVUFBSyxTQUFTLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxLQUFLLFNBQVMsRUFBRTtBQUM3QyxXQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUMsT0FBTyxFQUFDLENBQUE7T0FDL0U7S0FDRjtHQUNGOztBQUVELFNBQU8sR0FBRyxDQUFBO0NBRVg7Ozs7QUFJRCxPQUFPLENBQUMsZ0JBQWdCLEdBQUcsVUFBVSxTQUFTLEVBQUUsU0FBUyxFQUFFOztBQUV6RCxNQUFJLFVBQVUsR0FBRyxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUE7Ozs7O0FBS3hDLE1BQUksUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsRUFDM0IsT0FBTyxJQUFJLENBQUMsS0FDVCxJQUFJLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEVBQ2pDLE9BQU8sSUFBSSxDQUFBLEtBQ1IsSUFBSSxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUNoQyxPQUFPLElBQUksQ0FBQSxLQUNSLElBQUksU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsRUFDakMsT0FBTyxJQUFJLENBQUEsS0FDUixJQUFJLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEVBQ2pDLE9BQU8sSUFBSSxDQUFBLEtBQ1IsSUFBSSxDQUFDLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxzQkFBc0IsQ0FBQyxFQUNqRCxPQUFPLElBQUksQ0FBQSxLQUVYLE9BQU8sS0FBSyxDQUFBO0NBRWYsQ0FBQTs7QUFFRCxPQUFPLENBQUMsa0JBQWtCLEdBQUcsWUFBWTs7QUFFdkMsU0FBTyxzQkFBc0IsRUFBRSxDQUFBO0NBRWhDLENBQUE7O0FBRUQsT0FBTyxDQUFDLHlCQUF5QixHQUFHLFVBQVMsSUFBSSxFQUFFOztBQUVqRCxNQUFJLElBQUksR0FBRyxTQUFTLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDLENBQUE7QUFDcEQsTUFBSSxJQUFJLEdBQUcsc0JBQXNCLEVBQUUsQ0FBQTs7QUFFbkMsU0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVMsQ0FBQyxFQUFFO0FBQzdCLFdBQU8sSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxTQUFTLENBQUE7R0FDbEMsQ0FBQyxDQUFBO0NBR0gsQ0FBQTs7O0FDL0ZELFlBQVksQ0FBQzs7OztBQUliLE9BQU8sQ0FBQyxLQUFLLEdBQUcsVUFBVSxhQUFhLEVBQUU7QUFDckMsUUFBSSxPQUFPLEdBQUc7QUFDVixlQUFPLEVBQUUsRUFBRTtBQUNYLFlBQUksRUFBRSxFQUFFO0FBQ1IsZUFBTyxFQUFFLEVBQUU7QUFDWCxlQUFPLEVBQUUsRUFBRTtBQUNYLGFBQUssRUFBRSxFQUFFO0FBQ1QsbUJBQVcsRUFBRSxFQUFFO0FBQ2YsY0FBTSxFQUFFLEdBQUc7QUFDWCxhQUFLLEVBQUUsRUFBRTtBQUNULG9CQUFZLEVBQUUsRUFBRTtBQUNoQixhQUFLLEVBQUUsRUFBRTtBQUNULGNBQU0sRUFBRSxFQUFFO0FBQ1YsV0FBRyxFQUFFLEVBQUU7S0FDVixDQUFBOztBQUVELFFBQUksS0FBSyxHQUFHLGFBQWEsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUE7QUFDckMsUUFBSSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQTs7QUFFcEIsUUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFO0FBQ1AsZUFBTyxDQUFDLE9BQU8sR0FBRyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUE7QUFDaEQsZUFBTyxDQUFDLEdBQUcsR0FBRyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUE7QUFDNUMsZUFBTyxDQUFDLElBQUksR0FBRyxLQUFLLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFBO0FBQzNCLGVBQU8sQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQTtBQUM3QixlQUFPLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUE7S0FDakM7O0FBRUQsV0FBTyxPQUFPLENBQUE7Q0FDakIsQ0FBQTs7QUFFRCxJQUFJLGFBQWEsR0FBRztBQUNoQixvQkFBZ0IsRUFBRSxFQUFDLElBQUksRUFBRSxJQUFJLEVBQUM7Q0FDakMsQ0FBQTtBQUNELFNBQVMsZ0JBQWdCLENBQUMsWUFBWSxFQUFFO0FBQ3BDLFFBQUksSUFBSSxHQUFHLGFBQWEsQ0FBQyxZQUFZLENBQUMsQ0FBQTtBQUN0QyxRQUFJLElBQUksRUFDSixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUEsS0FFaEIsT0FBTyxTQUFTLENBQUE7Q0FDdkI7O0FBRUQsSUFBSSxVQUFVLEdBQUcsMExBQTBMLENBQUE7Ozs7O0FBSzNNLFNBQVMsZ0JBQWdCLENBQUMsSUFBSSxFQUFFO0FBQzVCLFFBQUksR0FBRyxDQUFBO0FBQ1AsUUFBSSxDQUFDLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQTtBQUM3QixRQUFJLENBQUMsRUFBRTtBQUNILFdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7S0FDYjtBQUNELFdBQU8sR0FBRyxDQUFBO0NBQ2I7Ozs7Ozs7Ozs7O0lDeERvQixhQUFhOzs7O0FBSW5CLFNBSk0sYUFBYSxDQUlsQixNQUFNLEVBQUU7MEJBSkgsYUFBYTs7QUFNMUIsUUFBSSxDQUFDLEdBQUcsR0FBRyxNQUFNLENBQUE7O0FBRWpCLFFBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxHQUFHLGFBQWEsQ0FBQTs7QUFFNUIsUUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLEdBQUcsVUFBUyxHQUFHLEVBQUU7QUFDOUIsZUFBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQTtBQUN4QixlQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFBO0FBQ2hCLGVBQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUE7S0FDOUIsQ0FBQTtDQUVKOztxQkFoQmdCLGFBQWE7Ozs7QUNEbEMsWUFBWSxDQUFDOzs7Ozs7OztJQUdRLGNBQWM7Ozs7QUFJcEIsU0FKTSxjQUFjLENBSW5CLE1BQU0sRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLFVBQVUsRUFBRSxrQkFBa0IsRUFBRSxXQUFXLEVBQUUsV0FBVyxFQUFFLElBQUksRUFBRTswQkFKMUYsY0FBYzs7QUFNM0IsVUFBTSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7O0FBRW5CLFVBQU0sQ0FBQyxVQUFVLEdBQUcsVUFBVSxLQUFLLEVBQUU7QUFDakMsY0FBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO0tBQ2xDLENBQUM7O0FBRUYsVUFBTSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUE7O0FBRXBCLFVBQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFBOztBQUV0QixVQUFNLENBQUMsV0FBVyxHQUFHLFlBQVk7QUFDN0IsZUFBTyxXQUFXLENBQUMsUUFBUSxFQUFFLENBQUE7S0FDaEMsQ0FBQTs7QUFFRCxRQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxLQUFLLEVBQUU7QUFDNUMsY0FBTSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUE7S0FDM0IsQ0FBQyxDQUFBOztBQUVGLFVBQU0sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLFlBQVksRUFBRSxVQUFVLENBQUMsRUFBRTtBQUM5QyxjQUFNLENBQUMsUUFBUSxHQUFHLFdBQVcsQ0FBQyxRQUFRLEVBQUUsQ0FBQTtLQUMzQyxDQUFDLENBQUM7O0FBRUgsVUFBTSxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLFVBQVUsQ0FBQyxFQUFFO0FBQy9DLGNBQU0sQ0FBQyxRQUFRLEdBQUcsV0FBVyxDQUFDLFFBQVEsRUFBRSxDQUFBO0tBQzNDLENBQUMsQ0FBQzs7QUFFSCxVQUFNLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsVUFBVSxDQUFDLEVBQUU7QUFDN0MsWUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsS0FBSyxFQUFFO0FBQzVDLGtCQUFNLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQTtTQUMzQixDQUFDLENBQUE7S0FDTCxDQUFDLENBQUM7O0FBR0gsVUFBTSxDQUFDLFVBQVUsR0FBRyxZQUFZO0FBQzVCLGVBQU8sV0FBVyxDQUFDLFVBQVUsRUFBRSxDQUFBO0tBRWxDLENBQUE7O0FBRUQsVUFBTSxDQUFDLE1BQU0sR0FBRyxZQUFZO0FBQ3hCLG1CQUFXLENBQUMsTUFBTSxFQUFFLENBQUE7QUFDcEIsa0JBQVUsQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxDQUFDO0FBQ2pELGtCQUFVLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQztBQUMvQyxpQkFBUyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQTtLQUN0QixDQUFBOztBQUVELFVBQU0sQ0FBQyxPQUFPLEdBQUcsVUFBVSxPQUFPLEVBQUU7QUFDaEMsZUFBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsT0FBTyxDQUFDLENBQUE7S0FDbEMsQ0FBQTs7QUFFRCxVQUFNLENBQUMsR0FBRyxHQUFHLFVBQVUsQ0FBQTs7QUFFdkIsVUFBTSxDQUFDLFlBQVksR0FBRyxVQUFVLElBQUksRUFBRTs7QUFFbEMsbUJBQVcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsTUFBTSxFQUFFO0FBQzVDLGdCQUFJLE1BQU0sQ0FBQyxhQUFhLEtBQUssSUFBSSxFQUFFO0FBQy9CLHVCQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFBOzs7Ozs7Ozs7O0FBVXJCLG9CQUFJLENBQUMsZUFBZSxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsTUFBTSxFQUFFO0FBQzFDLDJCQUFPLENBQUMsR0FBRyxDQUFDLHdCQUF3QixFQUFFLE1BQU0sQ0FBQyxDQUFBO0FBQzdDLHdCQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQTs7QUFFeEIsc0NBQWtCLENBQUMsU0FBUyxFQUFFLENBQUE7QUFDOUIsOEJBQVUsQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxDQUFDOztBQUUvQyw4QkFBVSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLENBQUM7QUFDaEQsd0JBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLEtBQUssRUFBRTtBQUM1Qyw0QkFBSSxLQUFLLEdBQUcsQ0FBQyxFQUNULFNBQVMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQSxLQUVoQyxTQUFTLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFBO3FCQUNuQyxDQUFDLENBQUE7aUJBRUwsQ0FBQyxDQUFBO2FBR0wsTUFDSTtBQUNELHNCQUFNLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQTtBQUNsQixzQkFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBQyxJQUFJLEVBQUUsU0FBUyxFQUFFLEdBQUcsRUFBRSxNQUFNLENBQUMsT0FBTyxFQUFDLENBQUMsQ0FBQzthQUM5RDtTQUNKLENBQUMsQ0FBQztLQUNOLENBQUM7O0FBRUYsVUFBTSxDQUFDLFFBQVEsR0FBRyxVQUFVLE9BQU8sRUFBRTs7QUFFakMsWUFBSSxPQUFPLENBQUMsUUFBUSxLQUFLLE9BQU8sQ0FBQyxTQUFTLEVBQUU7QUFDeEMsa0JBQU0sQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFBO0FBQ2xCLHVCQUFXLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLE1BQU0sRUFBRTtBQUMvQyxvQkFBSSxNQUFNLENBQUMsVUFBVSxFQUFFO0FBQ25CLDBCQUFNLENBQUMsWUFBWSxDQUFDLEVBQUMsS0FBSyxFQUFFLE9BQU8sQ0FBQyxLQUFLLEVBQUUsUUFBUSxFQUFFLE9BQU8sQ0FBQyxRQUFRLEVBQUMsQ0FBQyxDQUFBO2lCQUMxRSxNQUNJO0FBQ0QsMEJBQU0sQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFBO0FBQ2xCLDBCQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFDLElBQUksRUFBRSxTQUFTLEVBQUUsR0FBRyxFQUFFLE1BQU0sQ0FBQyxPQUFPLEVBQUMsQ0FBQyxDQUFDO2lCQUM5RDthQUNKLENBQUMsQ0FBQTtTQUNMLE1BQU07QUFDSCxrQkFBTSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUE7QUFDbEIsa0JBQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUMsSUFBSSxFQUFFLFNBQVMsRUFBRSxHQUFHLEVBQUUseUNBQXlDLEVBQUMsQ0FBQyxDQUFDO1NBQ3pGO0tBQ0osQ0FBQzs7QUFFRixVQUFNLENBQUMsZUFBZSxHQUFHLFVBQVUsSUFBSSxFQUFFO0FBQ3JDLFlBQUksQUFBQyxJQUFJLEtBQUssU0FBUyxJQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQUFBQyxFQUFFO0FBQ3ZDLGtCQUFNLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQTtBQUNsQixrQkFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBQyxJQUFJLEVBQUUsU0FBUyxFQUFFLEdBQUcsRUFBRSxxQ0FBcUMsRUFBQyxDQUFDLENBQUM7U0FDckYsTUFBTTs7QUFFSCx1QkFBVyxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxNQUFNLEVBQUU7QUFDbEUsc0JBQU0sQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFBO0FBQ2xCLG9CQUFJLE1BQU0sQ0FBQyxNQUFNLEVBQ2IsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7QUFDZix3QkFBSSxFQUFFLFNBQVM7QUFDZix1QkFBRyxFQUFFLGdGQUFnRjtpQkFDeEYsQ0FBQyxDQUFBLEtBRUYsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBQyxJQUFJLEVBQUUsU0FBUyxFQUFFLEdBQUcsRUFBRSxNQUFNLENBQUMsT0FBTyxFQUFDLENBQUMsQ0FBQTthQUNqRSxDQUFDLENBQUE7U0FDTDtLQUNKLENBQUM7O0FBRUYsVUFBTSxDQUFDLFlBQVksR0FBRyxVQUFVLFNBQVMsRUFBRTtBQUN2QyxrQkFBVSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQTtLQUM1QixDQUFBO0NBRUo7O3FCQTNJZ0IsY0FBYzs7OztBQ0huQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzlDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FDOUNBLFNBQVMsd0JBQXdCLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRTtBQUMzQyxRQUFJLEtBQUssRUFBRTtBQUNQLFlBQUksS0FBSyxZQUFZLEtBQUssRUFBRTtBQUN4QixnQkFBSSxJQUFJLEtBQUssSUFBSSxJQUFJLElBQUksS0FBSyxLQUFLLEVBQy9CLE9BQU8sS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBRWhCLE9BQU8sS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ3ZCLE1BQ0k7QUFDRCxtQkFBTyxLQUFLLENBQUM7U0FDaEI7S0FDSixNQUVHLE9BQU8sU0FBUyxDQUFDO0NBQ3hCOztBQUVELFNBQVMsZUFBZSxDQUFDLElBQUksRUFBRSxNQUFNLEVBQUU7QUFDbkMsUUFBSSxLQUFLLEdBQUcsd0JBQXdCLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsQ0FBQztBQUN4RCxXQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsQ0FBQztBQUMvQixRQUFJLEdBQUcsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDO0FBQ3JCLFFBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxFQUFFO0FBQ1gsYUFBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO0FBQzFDLGVBQUcsR0FBRyxHQUFHLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQztTQUMxQjtLQUNKLE1BQ0k7QUFDRCxhQUFLLElBQUksQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO0FBQ3JELGVBQUcsR0FBRyxHQUFHLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQztTQUMxQjtLQUNKO0FBQ0QsUUFBSSxJQUFJLENBQUMsY0FBYyxJQUFJLEtBQUssQ0FBQyxlQUFlLEVBQzVDLEdBQUcsR0FBRyxHQUFHLEdBQUcsS0FBSyxDQUFDLGVBQWUsQ0FBQztBQUN0QyxXQUFPLEdBQUcsQ0FBQztDQUNkOztBQUVELE9BQU8sQ0FBQyxVQUFVLEdBQUcsVUFBVSxJQUFJLEVBQUUsTUFBTSxFQUFFO0FBQ3pDLFdBQU8sZUFBZSxDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsQ0FBQztDQUN4QyxDQUFDOzs7O0FDdkNGLFlBQVksQ0FBQzs7QUFFYixPQUFPLENBQUMsWUFBWSxHQUFHLFVBQVUsTUFBTSxFQUFFLEtBQUssRUFBRSxZQUFZLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRTs7QUFFL0UsU0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQTs7QUFFekIsUUFBTSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUE7O0FBRXJCLFFBQU0sQ0FBQyxPQUFPLEdBQUc7QUFDZixnQkFBWSxFQUFHLE9BQU87QUFDdEIsVUFBTSxFQUFHLEdBQUc7QUFDWixlQUFXLEVBQUcsSUFBSTtBQUNsQixXQUFPLEVBQUcsT0FBTztBQUNqQixTQUFLLEVBQUcsa0JBQWtCO0FBQzFCLFFBQUksRUFBQyxNQUFNO0dBQ1osQ0FBQTs7QUFFRCxRQUFNLENBQUMsV0FBVyxHQUFHLFVBQVMsSUFBSSxFQUFFO0FBQ2xDLFVBQU0sQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFBO0FBQ3JCLFdBQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUE7QUFDakIsVUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFBO0dBQ2hCLENBQUE7Q0FFRixDQUFBOzs7QUN4QkQ7QUFDQTtBQUNBOzs7QUNEQSxZQUFZLENBQUM7O0FBR2IsT0FBTyxDQUFDLFVBQVUsR0FBRyxVQUFVLE9BQU8sRUFBRTs7QUFFdEMsTUFBSSxDQUFDLE1BQU0sR0FBRyxZQUFZLEVBQ3pCLENBQUM7O0FBRUYsTUFBSSxDQUFDLE9BQU8sR0FBRyxZQUFZO0FBQ3pCLFdBQU8sT0FBTyxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUE7QUFDeEMsV0FBTyxPQUFPLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQTtBQUN4QyxXQUFPLE9BQU8sQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFBO0dBQzVDLENBQUM7Ozs7QUFJRixNQUFJLENBQUMsR0FBRyxHQUFHLFVBQVUsSUFBSSxFQUFFOztBQUV6QixXQUFPLENBQUMsR0FBRyxDQUFDLHdCQUF3QixDQUFDLENBQUE7QUFDckMsUUFBSSxDQUFDLFVBQVUsR0FBRyxpQkFBaUIsRUFBRSxDQUFBOztBQUVyQyxXQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsQ0FBQTs7QUFFbEMsUUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFOztBQUVuQixhQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUE7S0FFOUIsTUFBTSxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7OztBQUV2QixVQUFJLElBQUksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQTtBQUN2QyxVQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFBO0FBQzlCLFVBQUksVUFBVSxHQUFHLGlCQUFpQixFQUFFLENBQUE7O0FBRXBDLFVBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFBO0FBQzVCLGFBQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQTs7QUFFbkIsVUFBSSxRQUFRLENBQUE7O0FBRVosVUFBSSxPQUFPLENBQUMsY0FBYyxDQUFDLGFBQWEsRUFDdEMsUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsQ0FBQSxLQUUzRCxRQUFRLEdBQUcsRUFBRSxDQUFBOztBQUVmLGNBQVEsQ0FBQyxVQUFVLENBQUMsR0FBRyxPQUFPLENBQUE7O0FBRTlCLGFBQU8sQ0FBQyxjQUFjLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUE7S0FDaEU7O0FBRUQsUUFBSSxLQUFLLENBQUE7O0FBRVQsUUFBSSxPQUFPLENBQUMsY0FBYyxDQUFDLFVBQVUsRUFBRTtBQUNyQyxXQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFBO0tBQ3RELE1BQ0MsS0FBSyxHQUFHLEVBQUUsQ0FBQTs7QUFFWixTQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBOztBQUVoQixXQUFPLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUE7QUFDNUQsV0FBTyxDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQUMsR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFBO0dBRXBELENBQUM7Ozs7QUFJRixNQUFJLENBQUMsV0FBVyxHQUFHLFVBQVUsSUFBSSxFQUFFOztBQUVqQyxRQUFJLEtBQUssQ0FBQTtBQUNULFFBQUksT0FBTyxDQUFDLGNBQWMsQ0FBQyxVQUFVLEVBQUU7QUFDckMsV0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQTtLQUN0RDs7QUFFRCxRQUFJLEtBQUssRUFBRTtBQUNULFVBQUksQ0FBQyxDQUFBO0FBQ0wsV0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO0FBQ2pDLFlBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsS0FBSyxJQUFJLENBQUMsVUFBVSxFQUN6QyxNQUFNO09BQ1Q7QUFDRCxVQUFJLENBQUMsR0FBRyxLQUFLLENBQUMsTUFBTSxFQUFFO0FBQ3BCLGFBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFBO0FBQ2xCLGVBQU8sQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQTtBQUM1RCxlQUFPLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUE7T0FDcEQ7S0FDRjtHQUNGLENBQUE7O0FBRUQsTUFBSSxDQUFDLFdBQVcsR0FBRyxVQUFVLElBQUksRUFBRTs7QUFFakMsUUFBSSxLQUFLLENBQUE7QUFDVCxRQUFJLE9BQU8sQ0FBQyxjQUFjLENBQUMsVUFBVSxFQUFFO0FBQ3JDLFdBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUE7S0FDdEQ7O0FBRUQsUUFBSSxLQUFLLEVBQUU7QUFDVCxVQUFJLENBQUMsQ0FBQTtBQUNMLFdBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtBQUNqQyxZQUFJLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLEtBQUssSUFBSSxDQUFDLFVBQVUsRUFDekMsTUFBTTtPQUNUO0FBQ0QsVUFBSSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sRUFBRTtBQUNwQixhQUFLLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFBO0FBQ2YsZUFBTyxDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFBO0FBQzVELGVBQU8sQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQTtPQUNwRDtLQUNGO0dBQ0YsQ0FBQTs7QUFFRCxNQUFJLENBQUMsUUFBUSxHQUFHLFlBQVk7QUFDMUIsUUFBSSxPQUFPLENBQUMsY0FBYyxDQUFDLFVBQVUsRUFDbkMsT0FBTyxPQUFPLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQSxLQUV4QyxPQUFPLENBQUMsQ0FBQTtHQUNYLENBQUE7O0FBRUQsTUFBSSxDQUFDLE9BQU8sR0FBRyxZQUFZO0FBQ3pCLFFBQUksUUFBUSxHQUFHLGdCQUFnQixFQUFFLENBQUE7QUFDakMsUUFBSSxLQUFLLENBQUE7QUFDVCxRQUFJLE9BQU8sQ0FBQyxjQUFjLENBQUMsVUFBVSxFQUFFO0FBQ3JDLFdBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUE7S0FDdEQsTUFDQyxLQUFLLEdBQUcsRUFBRSxDQUFBO0FBQ1osUUFBSSxNQUFNLEdBQUcsQ0FBQyxDQUFBOztBQUVkLFNBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO0FBQ3JDLFVBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsRUFDbkIsTUFBTSxHQUFHLE1BQU0sR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFBO0FBQ25DLFVBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsRUFDckIsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsR0FBRyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLFlBQVksQ0FBQTtLQUN0RTs7QUFFRCxXQUFPO0FBQ0wsV0FBSyxFQUFFLEtBQUs7QUFDWixZQUFNLEVBQUUsTUFBTTtBQUNkLFNBQUcsRUFBRSxPQUFPO0tBQ2IsQ0FBQTtHQUNGLENBQUE7Ozs7QUFJRCxNQUFJLENBQUMsY0FBYyxHQUFHLFVBQVUsT0FBTyxFQUFFO0FBQ3ZDLFdBQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEVBQUUsT0FBTyxDQUFDLENBQUE7O0FBRXRDLFFBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUE7QUFDbEMsUUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQTtBQUM5QixRQUFJLFVBQVUsR0FBRyxpQkFBaUIsRUFBRSxDQUFBOztBQUVwQyxRQUFJLFFBQVEsQ0FBQTs7QUFFWixRQUFJLE9BQU8sQ0FBQyxjQUFjLENBQUMsYUFBYSxFQUN0QyxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxDQUFBLEtBRTNELFFBQVEsR0FBRyxFQUFFLENBQUE7O0FBRWYsWUFBUSxDQUFDLFVBQVUsQ0FBQyxHQUFHLE9BQU8sQ0FBQTs7QUFFOUIsV0FBTyxDQUFDLGNBQWMsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQTs7QUFFL0QsV0FBTyxVQUFVLENBQUE7R0FDbEIsQ0FBQTs7QUFFRCxNQUFJLENBQUMsV0FBVyxHQUFHLFlBQVk7QUFDN0IsUUFBSSxPQUFPLENBQUMsY0FBYyxDQUFDLGFBQWEsRUFBRTtBQUN4QyxVQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLENBQUE7QUFDL0QsVUFBSSxHQUFHLEdBQUcsRUFBRSxDQUFBO0FBQ1osV0FBSyxJQUFJLEdBQUcsSUFBSSxRQUFRLEVBQUU7QUFDeEIsWUFBSSxRQUFRLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxFQUFFO0FBQ2hDLGFBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsWUFBWSxDQUFBO1NBQ3RDO09BQ0Y7QUFDRCxhQUFPLEdBQUcsQ0FBQTtLQUNYLE1BQ0MsT0FBTyxTQUFTLENBQUE7R0FDbkIsQ0FBQTs7QUFFRCxNQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsVUFBVSxFQUFFO0FBQ3RDLFFBQUksT0FBTyxDQUFDLGNBQWMsQ0FBQyxhQUFhLEVBQUU7QUFDeEMsVUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxDQUFBO0FBQy9ELGFBQU8sUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFBO0tBQzVCLE1BQ0MsT0FBTyxTQUFTLENBQUE7Ozs7OztHQU1uQixDQUFBOztBQUVELE1BQUksQ0FBQyxjQUFjLEdBQUcsWUFBWTtBQUNoQyxXQUFPLGdCQUFnQixFQUFFLENBQUE7R0FDMUIsQ0FBQTs7QUFFRCxNQUFJLENBQUMsZ0JBQWdCLEdBQUcsWUFBWTtBQUNsQyxRQUFJLE1BQU0sR0FBRyxnQkFBZ0IsRUFBRSxDQUFBO0FBQy9CLFdBQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFBO0FBQzdCLFdBQU8sTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQTtHQUMzQixDQUFBOztBQUdELFdBQVMsaUJBQWlCLEdBQUc7QUFDM0IsV0FBTyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLEdBQ2hELElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQztHQUMvQzs7QUFFRCxXQUFTLGdCQUFnQixHQUFHO0FBQzFCLFFBQUksT0FBTyxDQUFDLGNBQWMsQ0FBQyxhQUFhLEVBQUU7QUFDeEMsYUFBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLENBQUE7S0FDeEQsTUFDQyxPQUFPLEVBQUUsQ0FBQTtHQUNaOztBQUVELFNBQU8sSUFBSSxDQUFDO0NBRWIsQ0FBQTs7O0FDcE5ELFlBQVksQ0FBQzs7Ozs7Ozs7MENBU2EsZ0NBQWdDOzs7OzJDQUMvQixpQ0FBaUM7Ozs7QUFONUQsSUFBSSxTQUFTLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFBO0FBQ3BDLElBQUksU0FBUyxHQUFHLE9BQU8sQ0FBQyx5QkFBeUIsQ0FBQyxDQUFBO0FBQ2xELE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQTs7QUFNdkIsU0FBUyw0QkFBNEIsQ0FBQyxHQUFHLEVBQUU7QUFDdkMsUUFBSSxJQUFJLEdBQUcsRUFBRSxDQUFBO0FBQ2IsU0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7QUFDakMsV0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsR0FBRyxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUE7QUFDakUsWUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtLQUNwQjtBQUNELFdBQU8sSUFBSSxDQUFBO0NBQ2Q7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUEwQkQsT0FBTyxDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsRUFBRSxFQUFFLENBQUMsQ0FFaEMsVUFBVSxDQUFDLFNBQVMsMENBQWdCLENBR3BDLFVBQVUsQ0FBQyxVQUFVLEVBQUUsQ0FBQyxRQUFRLEVBQUUsVUFBVSxNQUFNLEVBQUU7QUFDakQsVUFBTSxDQUFDLEdBQUcsR0FBRyxPQUFPLENBQUE7Q0FDdkIsQ0FBQyxDQUFDLENBR0YsVUFBVSxDQUFDLFVBQVUsRUFBRSxDQUFDLFFBQVEsRUFBRSxXQUFXLEVBQUUsVUFBVSxNQUFNLEVBQUUsU0FBUyxFQUFFO0FBQ3pFLFVBQU0sQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFBOztBQUU1QixXQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsRUFBRSxTQUFTLENBQUMsQ0FBQTtDQUN6QyxDQUFDLENBQUMsQ0FFRixVQUFVLENBQUMsYUFBYSxFQUFFLENBQUMsUUFBUSxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBRSxhQUFhLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQzNILE9BQU8sQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBRS9DLFVBQVUsQ0FBQyxjQUFjLEVBQUUsQ0FBQyxJQUFJLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLGFBQWEsRUFBRSxjQUFjLEVBQzVHLGFBQWEsRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQzNELE9BQU8sQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDOzs7O0NBSWhELFVBQVUsQ0FBQyxVQUFVLEVBQUUsQ0FBQyxRQUFRLEVBQUUsV0FBVyxFQUFFLFdBQVcsRUFBRSxhQUFhLEVBQUUsYUFBYSxFQUFFLFlBQVksRUFBRSxVQUFVLE1BQU0sRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBRSxXQUFXLEVBQUUsVUFBVSxFQUFFOztBQUduTCxjQUFVLENBQUMsVUFBVSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsV0FBVyxFQUFFO0FBQy9DLGVBQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLFdBQVcsQ0FBQyxDQUFBO0tBQ3hDLENBQUMsQ0FBQztBQUNILFVBQU0sQ0FBQyxVQUFVLEdBQUcsNEJBQTRCLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFBO0FBQzdGLFVBQU0sQ0FBQyxVQUFVLEdBQUcsNEJBQTRCLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFBO0FBQzdGLFVBQU0sQ0FBQyxVQUFVLEdBQUcsNEJBQTRCLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFBOztBQUU3RixXQUFPLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQTs7QUFHaEMsVUFBTSxDQUFDLE1BQU0sR0FBRztBQUNaLGtCQUFVLEVBQUUsbUJBQW1CO0FBQy9CLGVBQU8sRUFBRSxPQUFPO0FBQ2hCLGFBQUssRUFBRTtBQUNILG1CQUFPLEVBQUUsZ0JBQWdCO0FBQ3pCLHNCQUFVLEVBQUUsT0FBTztBQUNuQix1QkFBVyxFQUFFLE9BQU87U0FDdkI7QUFDRCxjQUFNLEVBQUUsdUJBQXVCO0tBQ2xDLENBQUM7O0FBR0YsVUFBTSxDQUFDLEtBQUssR0FBRyxDQUNYLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUUsU0FBUyxFQUFFLEVBQ2xDLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFFLEVBQ25DLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUUsWUFBWSxFQUFFLENBQ3hDLENBQUM7O0FBRUYsVUFBTSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUE7QUFDeEIsVUFBTSxDQUFDLEtBQUssR0FBRyxTQUFTLENBQUMsa0JBQWtCLEVBQUUsQ0FBQTtBQUM3QyxVQUFNLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQTs7QUFFeEIsVUFBTSxDQUFDLFVBQVUsR0FBRyxDQUNoQixFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLGVBQWUsRUFBRSxHQUFHLEVBQUUsMENBQTBDLEVBQUUsRUFDdEYsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsR0FBRyxFQUFFLDJDQUEyQyxFQUFFLEVBQ2hGLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLEdBQUcsRUFBRSxZQUFZLEVBQUUsRUFDakQsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsR0FBRyxFQUFFLFlBQVksRUFBRSxFQUNsRCxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLGVBQWUsRUFBRSxHQUFHLEVBQUUsWUFBWSxFQUFFLEVBQ3hELEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxZQUFZLEVBQUUsQ0FDbEQsQ0FBQTs7QUFFRCxVQUFNLENBQUMsWUFBWSxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUE7O0FBRTFDLFVBQU0sQ0FBQyxTQUFTLEdBQUcsVUFBVSxTQUFTLEVBQUU7QUFDcEMsZUFBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLEVBQUUsU0FBUyxDQUFDLENBQUE7QUFDdkMsaUJBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLFNBQVMsQ0FBQyxDQUFBO0tBQ3ZDLENBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Q0E2Q0osQ0FBQyxDQUFDLENBRUYsVUFBVSxDQUFDLGVBQWUsRUFBRSxDQUFDLFVBQVUsRUFBRSxVQUFVLFFBQVEsRUFBRTtBQUMxRCxRQUFJLENBQUMsR0FBRyxHQUFHLFFBQVEsQ0FBQTtDQUN0QixDQUFDLENBQUMsQ0FFRixVQUFVLENBQUMsWUFBWSxFQUFFLENBQUMsUUFBUSxFQUFFLE9BQU8sRUFBRSxVQUFVLE1BQU0sRUFBRSxLQUFLLEVBQUU7O0FBRW5FLFVBQU0sQ0FBQyxHQUFHLEdBQUcsU0FBUyxDQUFBOztBQUV0QixTQUFLLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUksRUFBRTtBQUMvQyxjQUFNLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQTtBQUNsQixjQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQTtLQUN0QixDQUFDLENBQUE7Q0FHTCxDQUFDLENBQUMsQ0FHRixVQUFVLENBQUMsWUFBWSxFQUFFLENBQUMsUUFBUSxFQUFFLGNBQWMsRUFBRSxhQUFhLEVBQUUsV0FBVyxFQUFFLFVBQVUsTUFBTSxFQUFFLFlBQVksRUFBRSxXQUFXLEVBQUUsU0FBUyxFQUFFOztBQUVySSxXQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFBO0FBQ3pCLFVBQU0sQ0FBQyxJQUFJLEdBQUcsWUFBWSxDQUFDLElBQUksQ0FBQTs7QUFHL0IsUUFBSSxHQUFHLEdBQUc7QUFDTixZQUFJLEVBQUUsUUFBUTtBQUNkLFlBQUksRUFBRSxRQUFRO0FBQ2QsWUFBSSxFQUFFLFNBQVM7QUFDZixZQUFJLEVBQUUsZUFBZTtBQUNyQixZQUFJLEVBQUUsTUFBTTtBQUNaLFlBQUksRUFBRSxhQUFhO0FBQ25CLFlBQUksRUFBRSxlQUFlO0FBQ3JCLFlBQUksRUFBRSxpQkFBaUI7S0FDMUIsQ0FBQTs7QUFFRCxXQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxHQUFHLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUE7O0FBRTFDLFFBQUksR0FBRyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRTtBQUNsQixZQUFJLFVBQVUsR0FBRyw0QkFBNEIsR0FBRyxHQUFHLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLHNCQUFzQixDQUFBO0FBQ3pGLGlCQUFTLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFBO0FBQzlCLGlCQUFTLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxDQUFBO0FBQ2xDLGlCQUFTLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxDQUFBO0tBRXhDOzs7Ozs7Ozs7Ozs7OztDQW1CSixDQUFDLENBQUMsQ0FFRixVQUFVLENBQUMsU0FBUyxFQUFFLENBQUMsUUFBUSxFQUFFLGNBQWMsRUFBRSxTQUFTLEVBQUUsVUFBVSxNQUFNLEVBQUUsWUFBWSxFQUFFLE9BQU8sRUFBRTtBQUNsRyxXQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFBOztBQUV6QixRQUFJLFlBQVksQ0FBQyxHQUFHLEVBQUU7QUFDbEIsY0FBTSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQTtLQUNsRCxNQUVHLE1BQU0sQ0FBQyxRQUFRLEdBQUcsdUJBQXVCLENBQUE7Q0FDaEQsQ0FBQyxDQUFDLENBRUYsVUFBVSxDQUFDLGtCQUFrQixFQUFFLENBQUMsUUFBUSxFQUFFLFdBQVcsRUFBRSxZQUFZLEVBQUUsYUFBYSxFQUFFLE1BQU0sRUFBRSxhQUFhLEVBQUUsU0FBUyxFQUNqSCxVQUFVLE1BQU0sRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFLE9BQU8sRUFBRTs7QUFFOUUsVUFBTSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUE7QUFDeEIsV0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUE7O0FBRTNCLFdBQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEVBQUUsU0FBUyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUE7O0FBRWxELFVBQU0sQ0FBQyxJQUFJLEdBQUcsWUFBWTtBQUN0QixlQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFBO0FBQ3ZCLFlBQUksV0FBVyxDQUFDLFVBQVUsRUFBRSxFQUFFO0FBQzFCLGdCQUFJLE1BQU0sQ0FBQyxRQUFRLEtBQUssSUFBSSxFQUN4QixNQUFNLENBQUMsUUFBUSxHQUFHLFdBQVcsQ0FBQyxRQUFRLEVBQUUsQ0FBQTtBQUM1QyxnQkFBSSxBQUFDLE1BQU0sQ0FBQyxTQUFTLEtBQUssQ0FBQyxJQUFNLFdBQVcsQ0FBQyxVQUFVLEVBQUUsQUFBQyxFQUN0RCxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxLQUFLLEVBQUU7QUFDNUMsc0JBQU0sQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFBO2FBQzNCLENBQUMsQ0FBQTtTQUNUO0tBQ0osQ0FBQTs7QUFFRCxVQUFNLENBQUMsa0JBQWtCLEdBQUcsWUFBWTtBQUNwQyxlQUFPLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLENBQUMsU0FBUyxFQUFFLFNBQVMsQ0FBQyxDQUFBO0tBQ3BFLENBQUE7O0FBRUQsVUFBTSxDQUFDLFFBQVEsR0FBRyxVQUFVLFlBQVksRUFBRTtBQUN0QyxlQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSxZQUFZLEtBQUssU0FBUyxDQUFDLElBQUksRUFBRSxFQUFFLFNBQVMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFBO0FBQzVFLGVBQU8sWUFBWSxLQUFLLFNBQVMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztLQUM1QyxDQUFDOztBQUVGLFVBQU0sQ0FBQyxVQUFVLEdBQUcsWUFBWTtBQUM1QixlQUFPLFdBQVcsQ0FBQyxVQUFVLEVBQUUsQ0FBQTtLQUNsQyxDQUFBOztBQUVELFVBQU0sQ0FBQyxNQUFNLEdBQUcsWUFBWTtBQUN4QixtQkFBVyxDQUFDLE1BQU0sRUFBRSxDQUFBO0FBQ3BCLGtCQUFVLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsQ0FBQztBQUNqRCxrQkFBVSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLENBQUM7QUFDL0MsaUJBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUE7S0FDdEIsQ0FBQTs7QUFFRCxVQUFNLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQTtBQUN0QixVQUFNLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQTtBQUNwQixRQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxLQUFLLEVBQUU7QUFDNUMsY0FBTSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUE7S0FDM0IsQ0FBQyxDQUFBOztBQUdGLFVBQU0sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLFlBQVksRUFBRSxVQUFVLENBQUMsRUFBRTtBQUM5QyxjQUFNLENBQUMsUUFBUSxHQUFHLFdBQVcsQ0FBQyxRQUFRLEVBQUUsQ0FBQTtLQUMzQyxDQUFDLENBQUM7O0FBRUgsVUFBTSxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLFVBQVUsQ0FBQyxFQUFFO0FBQy9DLGNBQU0sQ0FBQyxRQUFRLEdBQUcsV0FBVyxDQUFDLFFBQVEsRUFBRSxDQUFBO0tBQzNDLENBQUMsQ0FBQzs7QUFFSCxVQUFNLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsVUFBVSxDQUFDLEVBQUU7QUFDN0MsWUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsS0FBSyxFQUFFO0FBQzVDLGtCQUFNLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQTtTQUMzQixDQUFDLENBQUE7S0FDTCxDQUFDLENBQUM7O0FBRUgsVUFBTSxDQUFDLElBQUksRUFBRSxDQUFBOztBQUViLFVBQU0sQ0FBQyxTQUFTLEdBQUcsVUFBVSxTQUFTLEVBQUU7QUFDcEMsZUFBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLEVBQUUsU0FBUyxDQUFDLENBQUE7QUFDdkMsWUFBSSxTQUFTLEtBQUssU0FBUyxFQUN2QixTQUFTLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxTQUFTLENBQUMsQ0FBQTtLQUMzQyxDQUFBO0NBRUosQ0FBQyxDQUFDLENBRU4sVUFBVSxDQUFDLGFBQWEsRUFBRSxDQUFDLFFBQVEsRUFBRSxPQUFPLEVBQUUsVUFBVSxNQUFNLEVBQUUsS0FBSyxFQUFFOztBQUVwRSxVQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQTs7QUFFbkIsU0FBSyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUksRUFBRTtBQUNuRCxlQUFPLENBQUMsR0FBRyxDQUFDLGlCQUFpQixDQUFDLENBQUE7QUFDOUIsY0FBTSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUE7QUFDckIsY0FBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUE7S0FDdEIsQ0FBQyxDQUFBO0NBRUwsQ0FBQyxDQUFDLENBRUYsVUFBVSxDQUFDLGdCQUFnQixFQUFFLENBQUMsUUFBUSxFQUFFLE9BQU8sRUFBRSxZQUFZLEVBQUUsV0FBVyxFQUFFLE1BQU0sRUFBRSxhQUFhLEVBQUUsUUFBUSxFQUN4RyxPQUFPLENBQUMsd0JBQXdCLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUUzQyxVQUFVLENBQUMsc0JBQXNCLEVBQUUsQ0FBQyxRQUFRLEVBQUUsY0FBYyxFQUN6RCxVQUFVLE1BQU0sRUFBRSxZQUFZLEVBQUU7O0FBRTVCLFVBQU0sQ0FBQyxTQUFTLEdBQUcsWUFBWSxDQUFDLFNBQVMsQ0FBQTtDQUU1QyxDQUFDLENBQUMsQ0FFTixVQUFVLENBQUMsb0JBQW9CLEVBQUUsQ0FBQyxRQUFRLEVBQUUsZ0JBQWdCLEVBQUUsU0FBUyxFQUNwRSxVQUFVLE1BQU0sRUFBRSxjQUFjLEVBQUUsT0FBTyxFQUFFO0FBQ3ZDLFVBQU0sQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDOztBQUV6QixVQUFNLENBQUMsRUFBRSxHQUFHLFlBQVk7QUFDcEIsc0JBQWMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztLQUMxQixDQUFDOztBQUdGLFVBQU0sQ0FBQyxNQUFNLEdBQUcsWUFBWTs7S0FFM0IsQ0FBQztDQUVMLENBQUMsQ0FBQyxDQUVOLFVBQVUsQ0FBQyx1QkFBdUIsRUFBRSxDQUFDLFFBQVEsRUFBRSxPQUFPLEVBQUUsWUFBWSxFQUFFLGNBQWMsRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUNwRyxhQUFhLEVBQUUsUUFBUSxFQUN2QixVQUFVLE1BQU0sRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLEVBQUU7O0FBRW5GLFVBQU0sQ0FBQyxHQUFHLEdBQUcsWUFBWSxDQUFDLEVBQUUsQ0FBQTs7QUFFNUIsU0FBSyxDQUFDLEdBQUcsQ0FBQyxhQUFhLEdBQUcsWUFBWSxDQUFDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUksRUFBRTtBQUMvRCxlQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQTtBQUMxQixjQUFNLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUE7S0FDN0IsQ0FBQyxDQUFBOzs7Ozs7Ozs7O0NBV0wsQ0FBQyxDQUFDLENBRU4sVUFBVSxDQUFDLGtCQUFrQixFQUFFLENBQUMsUUFBUSxFQUFFLE9BQU8sRUFBRSxZQUFZLEVBQUUsY0FBYyxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQy9GLGFBQWEsRUFBRSxRQUFRLEVBQ3ZCLFVBQVUsTUFBTSxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sRUFBRTs7QUFFbkYsVUFBTSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7O0FBRXJCLFVBQU0sQ0FBQyxhQUFhLEdBQUcsVUFBVSxPQUFPLEVBQUU7QUFDdEMsZUFBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQTtBQUNwQixjQUFNLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFNBQVMsRUFBRSxFQUFFLEVBQUUsRUFBRSxZQUFZLEVBQzdFLENBQUMsQ0FBQTtLQUNMLENBQUE7O0FBRUQsVUFBTSxDQUFDLGdCQUFnQixHQUFHLFlBQVk7QUFDbEMsZUFBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQTtBQUNuQixhQUFLLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLENBQ3pCLE9BQU8sQ0FBQyxVQUFVLElBQUksRUFBRTtBQUNyQixrQkFBTSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0FBQ2hFLG1CQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQTtTQUNyQyxDQUFDLENBQUE7S0FDVCxDQUFBOztBQUVELFVBQU0sQ0FBQyxXQUFXLEdBQUcsWUFBWSxDQUFDLE9BQU8sR0FBRyxZQUFZLENBQUMsT0FBTyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUE7QUFDeEUsVUFBTSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUE7Q0FFM0MsQ0FBQyxDQUFDLENBR04sVUFBVSxDQUFDLHlCQUF5QixFQUFFLENBQUMsUUFBUSxFQUFFLE9BQU8sRUFBRSxZQUFZLEVBQUUsY0FBYyxFQUNuRixNQUFNLEVBQUUsU0FBUyxFQUFFLGFBQWEsRUFBRSxlQUFlLEVBQ2pELFVBQVUsTUFBTSxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsV0FBVyxFQUFFLGFBQWEsRUFBRTs7QUFFMUYsVUFBTSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7O0FBRXJCLFVBQU0sQ0FBQyxhQUFhLEdBQUcsVUFBVSxPQUFPLEVBQUU7QUFDdEMsZUFBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQTtBQUNwQixjQUFNLENBQUMsYUFBYSxHQUFHLGFBQWEsQ0FBQyxLQUFLLENBQUMsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFNBQVMsRUFBRSxFQUFFLEVBQUUsRUFBRSxZQUFZLEVBQzNGLENBQUMsQ0FBQTtLQUNMLENBQUE7O0FBRUQsVUFBTSxDQUFDLGdCQUFnQixHQUFHLFlBQVk7QUFDbEMsZUFBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQTtBQUNuQixhQUFLLENBQUMsR0FBRyxDQUFDLDBCQUEwQixDQUFDLENBQ2hDLE9BQU8sQ0FBQyxVQUFVLElBQUksRUFBRTtBQUNyQixrQkFBTSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0FBQ2hFLG1CQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQTtTQUNyQyxDQUFDLENBQUE7S0FDVCxDQUFBOztBQUVELFVBQU0sQ0FBQyxXQUFXLEdBQUcsWUFBWSxDQUFDLE9BQU8sR0FBRyxZQUFZLENBQUMsT0FBTyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUE7QUFDeEUsVUFBTSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUE7Q0FFM0MsQ0FBQyxDQUFDLENBRU4sVUFBVSxDQUFDLDhCQUE4QixFQUFFLENBQUMsUUFBUSxFQUFFLE9BQU8sRUFBRSxZQUFZLEVBQUUsY0FBYyxFQUN4RixNQUFNLEVBQUUsU0FBUyxFQUFFLGFBQWEsRUFBRSxlQUFlLEVBQ2pELFVBQVUsTUFBTSxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsV0FBVyxFQUFFLGFBQWEsRUFBRTs7QUFFMUYsV0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsWUFBWSxDQUFDLEVBQUUsQ0FBQyxDQUFBO0FBQ2xDLFVBQU0sQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFBOztBQUVwQixRQUFJLFlBQVksQ0FBQyxFQUFFLEVBQUU7QUFDakIsY0FBTSxDQUFDLEdBQUcsR0FBRyxZQUFZLENBQUMsRUFBRSxDQUFBOztBQUU1QixhQUFLLENBQUMsR0FBRyxDQUFDLG9CQUFvQixHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxJQUFJLEVBQUU7QUFDakUsbUJBQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUE7QUFDakIsa0JBQU0sQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQTtBQUMxQixrQkFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUE7U0FDdEIsQ0FBQyxDQUFBO0tBQ0w7Q0FFSixDQUFDLENBQUMsQ0FFTixVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxFQUFFLGNBQWMsRUFBRSxXQUFXLEVBQUUsUUFBUSxFQUFFLFdBQVcsRUFBRSxhQUFhLEVBQ2pHLFVBQVUsTUFBTSxFQUFFLFlBQVksRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxXQUFXLEVBQUU7O0FBRXZFLFdBQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLFNBQVMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFBO0FBQ3pDLFFBQUksT0FBTyxDQUFBO0FBQ1gsUUFBSSxZQUFZLENBQUMsT0FBTyxFQUNwQixPQUFPLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQSxLQUM3QjtBQUNELFlBQUksSUFBSSxHQUFHLFNBQVMsQ0FBQyxvQkFBb0IsQ0FBQyxxQ0FBcUMsRUFBRSxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQTtBQUNsRyxZQUFJLElBQUksRUFDSixPQUFPLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQTtLQUMxQjs7QUFHRCxRQUFJLE9BQU8sRUFDUCxNQUFNLENBQUMsWUFBWSxHQUFHLE9BQU8sQ0FBQyxXQUFXLEVBQUUsR0FBRyxNQUFNLENBQUE7O0FBRXhELFVBQU0sQ0FBQyxLQUFLLEdBQUcsVUFBVSxJQUFJLEVBQUU7QUFDM0IsZUFBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQTtBQUN6QixZQUFJLEtBQUssR0FBRyxJQUFJLEdBQUcsT0FBTyxDQUFBO0FBQzFCLGNBQU0sQ0FBQyxJQUFJLENBQUM7QUFDUix1QkFBVyxFQUFFLDBCQUEwQjtBQUN2Qyx1QkFBVyxFQUFFLGtCQUFrQjtBQUMvQixzQkFBVSxFQUFFLGVBQWUsRUFBRSxPQUFPLEVBQUU7QUFDbEMsd0JBQVEsRUFBRSxvQkFBWTtBQUNsQiwyQkFBTyxLQUFLLENBQUE7aUJBQ2Y7YUFDSjtTQUNKLENBQUMsQ0FBQztLQUNOLENBQUM7O0FBRUYsVUFBTSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUE7O0FBRWpCLGFBQVMsU0FBUyxHQUFHO0FBQ2pCLG1CQUFXLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksRUFBRTtBQUMxQyxrQkFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7QUFDdkIsZ0JBQUksTUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUN2QixNQUFNLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxDQUFBO1NBQzNCLENBQUMsQ0FBQTtLQUNMOztBQUVELGFBQVMsRUFBRSxDQUFBOzs7Q0FJZCxDQUFDLENBQUMsQ0FFTixVQUFVLENBQUMsZ0JBQWdCLDJDQUFpQixDQUU1QyxVQUFVLENBQUMsZUFBZSxFQUFFLENBQUMsUUFBUSxFQUFFLE9BQU8sRUFBRSxhQUFhLEVBQUUsU0FBUyxFQUNyRSxVQUFVLE1BQU0sRUFBRSxLQUFLLEVBQUUsV0FBVyxFQUFFLE9BQU8sRUFBRTs7QUFFM0MsVUFBTSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7O0FBRW5CLFVBQU0sQ0FBQyxVQUFVLEdBQUcsVUFBVSxLQUFLLEVBQUU7QUFDakMsY0FBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO0tBQ2xDLENBQUM7O0FBRUYsU0FBSyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUksRUFBRTtBQUNqRCxlQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFBO0FBQ2pCLGNBQU0sQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFBO0FBQ2xCLGNBQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFBO0tBQ3RCLENBQUMsQ0FBQTs7QUFFRixVQUFNLENBQUMsY0FBYyxHQUFHLFlBQVk7O0FBRWhDLGVBQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEVBQUUsT0FBTyxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUE7O0FBRWxELG1CQUFXLENBQUMsc0JBQXNCLENBQUMsT0FBTyxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsTUFBTSxFQUFFO0FBQzNFLGtCQUFNLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQTtBQUNsQixnQkFBSSxNQUFNLENBQUMsTUFBTSxFQUNiLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDO0FBQ2Ysb0JBQUksRUFBRSxTQUFTO0FBQ2YsbUJBQUcsRUFBRSxnRkFBZ0Y7YUFDeEYsQ0FBQyxDQUFBLEtBRUYsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLEdBQUcsRUFBRSxNQUFNLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQTtTQUVuRSxDQUFDLENBQUE7S0FDTCxDQUFBO0NBQ0osQ0FBQyxDQUFDLENBRU4sVUFBVSxDQUFDLDBCQUEwQixFQUFFLENBQUMsUUFBUSxFQUFFLGdCQUFnQixFQUMvRCxVQUFVLE1BQU0sRUFBRSxjQUFjLEVBQUU7O0FBRTlCLFVBQU0sQ0FBQyxHQUFHLEdBQUcsa0JBQWtCLENBQUE7O0FBRS9CLFVBQU0sQ0FBQyxVQUFVLEdBQUcsVUFBVSxNQUFNLEVBQUU7QUFDbEMsY0FBTSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUE7QUFDeEIsc0JBQWMsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsTUFBTSxFQUFFO0FBQ2pELG1CQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFBO0FBQ25CLGdCQUFJLE1BQU0sQ0FBQyxNQUFNLEVBQUU7QUFDZixzQkFBTSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFBO0FBQzdCLHNCQUFNLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQTthQUMxQjtTQUNKLENBQUMsQ0FBQTtLQUNMLENBQUE7Q0FDSixDQUFDLENBQUM7Ozs7O0NBS04sVUFBVSxDQUFDLG1CQUFtQixFQUFFLENBQUMsUUFBUSxFQUFFLGdCQUFnQixFQUFFLGdCQUFnQixFQUFFLFVBQVUsTUFBTSxFQUFFLGNBQWMsRUFBRSxjQUFjLEVBQUU7OztBQUc5SCxVQUFNLENBQUMsUUFBUSxHQUFHOztLQUVqQixDQUFDOztBQUVGLFVBQU0sQ0FBQyxFQUFFLEdBQUcsWUFBWTtBQUNwQixlQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQTs7QUFFMUIsWUFBSSxNQUFNLENBQUMsTUFBTSxLQUFLLFNBQVMsRUFDM0IsY0FBYyxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQSxLQUN2QjtBQUNELGdCQUFJLGdCQUFnQixHQUFHLEVBQUUsQ0FBQTtBQUN6QixpQkFBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO0FBQzNDLG9CQUFJLElBQUksR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFBOztBQUUzQixvQkFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsVUFBVSxPQUFPLEVBQUU7QUFDakQsMkJBQU8sT0FBTyxDQUFDLFFBQVEsQ0FBQTtpQkFDMUIsQ0FBQyxDQUFBO0FBQ0YsdUJBQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQTtBQUN0QyxvQkFBSSxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTs7QUFFckIsb0NBQWdCLENBQUMsSUFBSSxDQUFDO0FBQ2xCLDRCQUFJLEVBQUUsSUFBSSxDQUFDLElBQUk7QUFDZixvQ0FBWSxFQUFFLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPO0FBQ2pDLGtDQUFVLEVBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUs7cUJBQ2hDLENBQUMsQ0FBQTtpQkFFTDthQUNKO0FBQ0QsMEJBQWMsQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztTQUMxQztLQUNKLENBQUM7O0FBRUYsVUFBTSxDQUFDLE1BQU0sR0FBRyxZQUFZO0FBQ3hCLHNCQUFjLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0tBQ3BDLENBQUM7O0FBR0YsVUFBTSxDQUFDLFVBQVUsR0FBRyxVQUFVLFdBQVcsRUFBRTtBQUN2QyxlQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFBO0FBQ3hCLHNCQUFjLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLE1BQU0sRUFBRTtBQUN0RCxtQkFBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQTtBQUNuQixnQkFBSSxNQUFNLENBQUMsTUFBTSxFQUFFO0FBQ2Ysc0JBQU0sQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQTtBQUM3QixzQkFBTSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUE7YUFDMUI7U0FDSixDQUFDLENBQUE7S0FDTCxDQUFBO0NBR0osQ0FBQyxDQUFDLENBRUYsVUFBVSxDQUFDLGVBQWUsRUFBRSxDQUFDLFFBQVEsRUFBRSxNQUFNLEVBQUUsZ0JBQWdCLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFBRSxVQUFVLE1BQU0sRUFBRSxJQUFJLEVBQUUsY0FBYyxFQUFFLFFBQVEsRUFBRSxTQUFTLEVBQUU7Ozs7Ozs7QUFPcEosV0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsUUFBUSxDQUFDLENBQUE7QUFDaEMsYUFBUyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxNQUFNLEVBQUU7QUFDM0MsZUFBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtBQUNsQixlQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFBO0FBQ25CLFlBQUksSUFBSSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUE7O0FBRXhCLGNBQU0sQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7QUFDOUMsY0FBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUE7S0FFdkIsQ0FBQyxDQUFBOztBQUVGLFVBQU0sQ0FBQyxFQUFFLEdBQUcsWUFBWTtBQUNwQixlQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQTs7QUFFMUIsWUFBSSxNQUFNLENBQUMsTUFBTSxLQUFLLFNBQVMsRUFDM0IsY0FBYyxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQSxLQUN2QjtBQUNELGdCQUFJLGdCQUFnQixHQUFHLEVBQUUsQ0FBQTtBQUN6QixpQkFBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO0FBQzNDLG9CQUFJLElBQUksR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFBOztBQUUzQixvQkFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsVUFBVSxPQUFPLEVBQUU7QUFDakQsMkJBQU8sT0FBTyxDQUFDLFFBQVEsQ0FBQTtpQkFDMUIsQ0FBQyxDQUFBO0FBQ0YsdUJBQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQTtBQUN0QyxvQkFBSSxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTs7QUFFckIsb0NBQWdCLENBQUMsSUFBSSxDQUFDO0FBQ2xCLDRCQUFJLEVBQUUsSUFBSSxDQUFDLElBQUk7QUFDZixvQ0FBWSxFQUFFLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPO0FBQ2pDLGtDQUFVLEVBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUs7cUJBQ2hDLENBQUMsQ0FBQTtpQkFFTDthQUNKO0FBQ0QsMEJBQWMsQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztTQUMxQztLQUNKLENBQUM7O0FBRUYsVUFBTSxDQUFDLE1BQU0sR0FBRyxZQUFZO0FBQ3hCLHNCQUFjLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0tBQ3BDLENBQUM7Q0FDTCxDQUFDLENBQUMsQ0FFRixVQUFVLENBQUMsYUFBYSxFQUFFLENBQUMsUUFBUSxFQUFFLE9BQU8sRUFDekMsVUFBVSxNQUFNLEVBQUUsS0FBSyxFQUFFOztBQUVyQixVQUFNLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQTs7QUFFekIsVUFBTSxDQUFDLFlBQVksR0FBRyxVQUFVLE9BQU8sRUFBRTtBQUNyQyxjQUFNLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQTtBQUN4QixlQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFBO0FBQ2hELGFBQUssQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLE9BQU8sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUksRUFBRTtBQUN0RCxtQkFBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQTtBQUNqQixnQkFBSSxJQUFJLENBQUMsTUFBTSxLQUFLLElBQUksRUFBRTtBQUN0QixzQkFBTSxDQUFDLFFBQVEsR0FBRywyQkFBMkIsQ0FBQTthQUNoRCxNQUFNO0FBQ0gscUJBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUE7YUFDbEI7U0FDSixDQUFDLENBQUM7S0FDTixDQUFBO0NBRUosQ0FBQyxDQUFDLENBRU4sVUFBVSxDQUFDLG1CQUFtQixFQUFFLENBQUMsUUFBUSxFQUFFLE9BQU8sRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUN4RSxVQUFVLE1BQU0sRUFBRSxLQUFLLEVBQUUsUUFBUSxFQUFFLFNBQVMsRUFBRTs7QUFFMUMsVUFBTSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUE7QUFDNUIsU0FBSyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUksRUFBRTtBQUNqRCxlQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFBO0FBQ2pCLGNBQU0sQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFBO0FBQ2xCLGNBQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFBO0tBQ3RCLENBQUMsQ0FBQTs7QUFFRixVQUFNLENBQUMsV0FBVyxHQUFHLFVBQVUsSUFBSSxFQUFFO0FBQ2pDLGFBQUssQ0FBQyxJQUFJLENBQUMsa0JBQWtCLEVBQUUsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsSUFBSSxFQUFFOztBQUV6RCxrQkFBTSxDQUFDLE1BQU0sR0FBRyxDQUNaLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxHQUFHLEVBQUUsK0JBQStCLEVBQUUsQ0FDNUQsQ0FBQTtBQUNELG9CQUFRLENBQUMsWUFBWTtBQUNqQixzQkFBTSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUE7YUFDckIsRUFBRSxJQUFJLENBQUMsQ0FBQztTQUNaLENBQUMsQ0FBQztLQUNOLENBQUE7Q0FFSixDQUFDLENBQUMsQ0FFTixVQUFVLENBQUMsbUJBQW1CLEVBQUUsQ0FBQyxRQUFRLEVBQUUsT0FBTyxFQUFFLGNBQWMsRUFBRSxXQUFXLEVBQUUsUUFBUSxFQUFFLFVBQVUsRUFBRSxtQkFBbUIsRUFBRSxjQUFjLEVBQ3ZJLFVBQVUsTUFBTSxFQUFFLEtBQUssRUFBRSxZQUFZLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsaUJBQWlCLEVBQUUsWUFBWSxFQUFFOztBQUVqRyxXQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksRUFBRSxpQkFBaUIsQ0FBQyxDQUFBOztBQUU1QyxVQUFNLENBQUMsT0FBTyxHQUFHLGlCQUFpQixDQUFDLE9BQU8sQ0FBQTs7QUFFMUMsVUFBTSxDQUFDLElBQUksR0FBRyxVQUFVLE9BQU8sRUFBRTs7QUFFN0IsWUFBSSxPQUFPLENBQUE7O0FBRVgsWUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLEVBQ3JCLE9BQU8sR0FBRyx3Q0FBd0MsQ0FBQTtBQUN0RCxZQUFJLENBQUMsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFDNUIsT0FBTyxHQUFHLGlDQUFpQyxDQUFBO0FBQy9DLFlBQUksQ0FBQyxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUMxQixPQUFPLEdBQUcsNERBQTRELENBQUE7O0FBRTFFLFlBQUksT0FBTyxFQUNQLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FDWixFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsR0FBRyxFQUFFLE9BQU8sRUFBRSxDQUNwQyxDQUFBLEtBQ0E7O0FBRUQsbUJBQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLEVBQUUsT0FBTyxDQUFDLENBQUE7O0FBRXhDLHdCQUFZLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLE1BQU0sRUFBRTtBQUM5Qyx1QkFBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQTtBQUNuQixvQkFBSSxNQUFNLENBQUMsRUFBRTtBQUNULDZCQUFTLENBQUMsSUFBSSxDQUFDLG1CQUFtQixHQUFHLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQSxLQUM5QztBQUNELDBCQUFNLENBQUMsTUFBTSxHQUFHLENBQ1osRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLEdBQUcsRUFBRSwrQkFBK0IsRUFBRSxDQUM1RCxDQUFBO0FBQ0QsNEJBQVEsQ0FBQyxZQUFZO0FBQ2pCLDhCQUFNLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQTtxQkFDckIsRUFBRSxJQUFJLENBQUMsQ0FBQztpQkFDWjthQUVKLENBQUMsQ0FBQTs7Ozs7Ozs7U0FRTDtLQUVKLENBQUE7Q0FHSixDQUFDLENBQUMsQ0FFTixVQUFVLENBQUMsdUJBQXVCLEVBQUUsQ0FBQyxRQUFRLEVBQUUsT0FBTyxFQUFFLFFBQVEsRUFBRSxXQUFXLEVBQUUsY0FBYyxFQUFFLFdBQVcsRUFDdkcsVUFBVSxNQUFNLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLFNBQVMsRUFBRTs7QUFFakUsU0FBSyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUksRUFBRTtBQUNoRCxlQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFBO0FBQ2pCLGNBQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFBO0tBQ3pCLENBQUMsQ0FBQTs7QUFFRixVQUFNLENBQUMsUUFBUSxHQUFHLFVBQVUsR0FBRyxFQUFFOztBQUU3QixlQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFBO0FBQ2hCLFlBQUksT0FBTyxDQUFDLDhDQUE4QyxHQUFHLEdBQUcsQ0FBQyxZQUFZLENBQUMsRUFBRTs7QUFFNUUsaUJBQUssQ0FBQyxJQUFJLENBQUMscUJBQXFCLEVBQUUsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsSUFBSSxFQUFFO0FBQzNELHVCQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQTtBQUN4QixvQkFBSSxJQUFJLENBQUMsTUFBTSxFQUNYLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQSxLQUVmLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUE7YUFDdEIsQ0FBQyxDQUFDOzs7Ozs7Ozs7OztTQVdOO0tBQ0osQ0FBQTtDQUNKLENBQUMsQ0FBQyxDQUVOLFVBQVUsQ0FBQyxhQUFhLEVBQUUsQ0FBQyxRQUFRLEVBQUUsT0FBTyxFQUFFLGNBQWMsRUFBRSxTQUFTLEVBQ3BFLFVBQVUsTUFBTSxFQUFFLEtBQUssRUFBRSxZQUFZLEVBQUUsT0FBTyxFQUFFOztBQUU1QyxVQUFNLENBQUMsU0FBUyxHQUFHLE9BQU8sQ0FBQyxTQUFTLENBQUE7QUFDcEMsV0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUE7Q0FHakMsQ0FBQyxDQUFDLENBR04sVUFBVSxDQUFDLG1CQUFtQixFQUFFLENBQUMsUUFBUSxFQUFFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxFQUFFLFdBQVcsRUFBRSxTQUFTLEVBQy9GLFVBQVUsTUFBTSxFQUFFLEtBQUssRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxPQUFPLEVBQUU7O0FBRTdELFdBQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUE7QUFDeEIsVUFBTSxDQUFDLE9BQU8sR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFBO0FBQ3JDLFVBQU0sQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDLFNBQVMsQ0FBQTtBQUNwQyxXQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQTs7QUFFOUIsVUFBTSxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFBOztBQUVoQyxRQUFJLE1BQU0sQ0FBQyxPQUFPLEtBQUssSUFBSTtBQUN2QixjQUFNLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQTs7QUFFM0IsUUFBSSxRQUFRLEdBQUcsV0FBVyxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUE7O0FBRWpELGFBQVMsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsTUFBTSxFQUFFO0FBQzNDLGVBQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUE7QUFDbEIsZUFBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQTtBQUNuQixZQUFJLElBQUksR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFBO0FBQ3hCLGNBQU0sQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7QUFDOUMsY0FBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUE7S0FFdkIsQ0FBQyxDQUFBO0NBRUwsQ0FBQyxDQUFDLENBRU4sVUFBVSxDQUFDLFVBQVUsRUFBRSxDQUFDLFFBQVEsRUFBRSxPQUFPLEVBQUUsY0FBYyxFQUN0RCxVQUFVLE1BQU0sRUFBRSxLQUFLLEVBQUUsWUFBWSxFQUFFOztBQUVuQyxXQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFBOztBQUV2QixTQUFLLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsSUFBSSxFQUFFO0FBQ25ELGVBQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUE7QUFDakIsY0FBTSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUE7S0FDcEIsQ0FBQyxDQUFBO0NBR0wsQ0FBQyxDQUFDLENBRU4sVUFBVSxDQUFDLHNCQUFzQixFQUFFLENBQUMsUUFBUSxFQUFFLE9BQU8sRUFBRSxjQUFjLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBRSxPQUFPLEVBQ25HLFVBQVUsTUFBTSxFQUFFLEtBQUssRUFBRSxZQUFZLEVBQUUsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUU7O0FBRTlELFVBQU0sQ0FBQyxXQUFXLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQyxXQUFXLEVBQUUsQ0FBQTtBQUM3QyxVQUFNLENBQUMsWUFBWSxHQUFHLElBQUksSUFBSSxFQUFFLENBQUMsUUFBUSxFQUFFLEdBQUcsQ0FBQyxDQUFBO0FBQy9DLFVBQU0sQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQTtBQUM5QyxVQUFNLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQTs7QUFFNUIsVUFBTSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUE7O0FBR3BCLFFBQUksVUFBVSxHQUFHLFNBQWIsVUFBVSxHQUFlO0FBQ3pCLFlBQUksTUFBTSxDQUFDLFFBQVEsQ0FBQyxRQUFRLElBQUksTUFBTSxDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUU7QUFDdkQsa0JBQU0sQ0FBQyxRQUFRLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsU0FBUyxHQUFHLEdBQUcsR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQTtTQUMxRjtLQUNKLENBQUE7O0FBRUQsU0FBSyxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsSUFBSSxFQUFFO0FBQzNELGNBQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFBO0tBQ3pCLENBQUMsQ0FBQTs7QUFFRixTQUFLLENBQUMscUJBQXFCLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksRUFBRTs7QUFFbkUsZUFBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsRUFBRSxJQUFJLENBQUMsQ0FBQTtBQUN4QyxjQUFNLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQTtLQUVwQixDQUFDLENBQUE7O0FBRUYsVUFBTSxDQUFDLE1BQU0sQ0FBQyxvQkFBb0IsRUFBRSxVQUFVLE1BQU0sRUFBRSxNQUFNLEVBQUU7QUFDMUQsZUFBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFBO0FBQ3RDLGtCQUFVLEVBQUUsQ0FBQTtLQUNmLENBQUMsQ0FBQzs7QUFFSCxVQUFNLENBQUMsTUFBTSxDQUFDLG1CQUFtQixFQUFFLFVBQVUsTUFBTSxFQUFFLE1BQU0sRUFBRTtBQUN6RCxlQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUE7QUFDdEMsa0JBQVUsRUFBRSxDQUFBO0tBQ2YsQ0FBQyxDQUFDOztBQUVILFVBQU0sQ0FBQyxXQUFXLEdBQUcsWUFBWTtBQUM3QixlQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFBO0FBQ3hCLGNBQU0sQ0FBQyxHQUFHLEdBQUcsNENBQTRDLENBQUE7S0FDNUQsQ0FBQTtDQUVKLENBQUMsQ0FBQyxDQUVOLFVBQVUsQ0FBQyx5QkFBeUIsRUFBRSxDQUFDLFFBQVEsRUFBRSxPQUFPLEVBQUUsY0FBYyxFQUNyRSxVQUFVLE1BQU0sRUFBRSxLQUFLLEVBQUUsWUFBWSxFQUFFOztBQUVuQyxXQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSxZQUFZLENBQUMsQ0FBQTs7QUFFckMsU0FBSyxDQUFDLEdBQUcsQ0FBQyx1QkFBdUIsR0FBRyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsSUFBSSxFQUFFO0FBQzFFLGVBQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUE7QUFDakIsY0FBTSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUE7S0FDckIsQ0FBQyxDQUFBO0NBRUwsQ0FBQyxDQUFDOzs7Q0FHTixVQUFVLENBQUMsMEJBQTBCLEVBQUUsQ0FBQyxRQUFRLEVBQUUsT0FBTyxFQUFFLGNBQWMsRUFDdEUsVUFBVSxNQUFNLEVBQUUsS0FBSyxFQUFFLFlBQVksRUFBRTtBQUNuQyxVQUFNLENBQUMsU0FBUyxHQUFHLFlBQVksQ0FBQyxHQUFHLENBQUE7Q0FFdEMsQ0FBQyxDQUFDLENBRU4sVUFBVSxDQUFDLDZCQUE2QixFQUFFLENBQUMsUUFBUSxFQUFFLE9BQU8sRUFBRSxjQUFjLEVBQUUsV0FBVyxFQUFFLFlBQVksRUFDcEcsU0FBUyxFQUFFLFNBQVMsRUFBRSxPQUFPLEVBQUUsYUFBYSxFQUFFLE9BQU8sQ0FBQywwQkFBMEIsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBRzdGLFVBQVUsQ0FBQyx1QkFBdUIsRUFBRSxDQUFDLE9BQU8sRUFBRSxjQUFjLEVBQUUsV0FBVyxFQUFFLFFBQVEsRUFDaEYsU0FBUyxFQUFFLFNBQVMsRUFBRSxPQUFPLEVBQUUsYUFBYSxFQUFFLE9BQU8sQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBR3JHLFVBQVUsQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLFFBQVEsRUFBRSxjQUFjLEVBQUUsV0FBVyxFQUFFLFlBQVksRUFBRSxhQUFhLEVBQUUsTUFBTSxFQUFFLGNBQWMsRUFBRSxhQUFhLEVBQ3RJLE9BQU8sQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBR3ZELFVBQVUsQ0FBQyw2QkFBNkIsRUFBRSxDQUFDLFFBQVEsRUFBRSxjQUFjLEVBQUUsV0FBVyxFQUFFLFlBQVksRUFBRSxhQUFhLEVBQUUsTUFBTSxFQUFFLGFBQWEsRUFDakksT0FBTyxDQUFDLDZCQUE2QixDQUFDLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUVsRSxVQUFVLENBQUMsY0FBYyxFQUFFLENBQUMsUUFBUSxFQUFFLE9BQU8sRUFBRSxjQUFjLEVBQUUsV0FBVyxFQUFFLFFBQVEsRUFDakYsT0FBTyxDQUFDLGdCQUFnQixDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDNzRCaEQsWUFBWSxDQUFDOztBQUViLE9BQU8sQ0FBQyxJQUFJLEdBQUcsVUFBVSxNQUFNLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFFLFdBQVcsRUFBRSxNQUFNLEVBQUU7Ozs7Ozs7Ozs7O0FBV3hGLFFBQU0sQ0FBQyxXQUFXLEdBQUcsVUFBVSxJQUFJLEVBQUU7QUFDbkMsUUFBSSxDQUFDLElBQUksRUFDUCxPQUFPLElBQUksQ0FBQSxLQUNSO0FBQ0gsYUFBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUE7S0FDL0I7R0FDRixDQUFBOztBQUVELFFBQU0sQ0FBQyxlQUFlLEdBQUcsWUFBWTs7QUFFbkMsUUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksRUFBRTtBQUN4QyxZQUFNLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUE7QUFDdkIsZ0JBQVUsQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxDQUFDO0tBQ2hELENBQUMsQ0FBQTtHQUVILENBQUE7O0FBRUQsUUFBTSxDQUFDLGVBQWUsRUFBRSxDQUFBOztBQUV4QixRQUFNLENBQUMsV0FBVyxHQUFHLFVBQVUsT0FBTyxFQUFFLEdBQUcsRUFBRTs7QUFFM0MsUUFBSSxPQUFPLENBQUMseUNBQXlDLENBQUMsRUFBRTs7QUFFdEQsVUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsSUFBSSxFQUFFO0FBQ2xELGNBQU0sQ0FBQyxlQUFlLEVBQUUsQ0FBQTtPQUN6QixDQUFDLENBQUE7S0FDSDtHQUNGLENBQUE7O0FBR0QsUUFBTSxDQUFDLGlCQUFpQixHQUFHLFVBQVUsT0FBTyxFQUFFO0FBQzVDLFdBQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFBOztBQUUvQixRQUFJLE9BQU8sQ0FBQywrQ0FBK0MsQ0FBQyxFQUFFOztBQUU1RCxVQUFJLENBQUMsZUFBZSxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsTUFBTSxFQUFFO0FBQzVDLGVBQU8sQ0FBQyxHQUFHLENBQUMseUJBQXlCLEVBQUUsTUFBTSxDQUFDLENBQUE7Ozs7Ozs7Ozs7Ozs7T0FlL0MsQ0FBQyxDQUFBOzs7OztLQUtIO0dBQ0YsQ0FBQTs7QUFFRCxRQUFNLENBQUMsZ0JBQWdCLEdBQUcsVUFBVSxPQUFPLEVBQUUsR0FBRyxFQUFFO0FBQ2hELFFBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsTUFBTSxFQUFFO0FBQ3pELFlBQU0sQ0FBQyxlQUFlLEVBQUUsQ0FBQTtLQUN6QixDQUFDLENBQUE7R0FDSCxDQUFBOztBQUVELFFBQU0sQ0FBQyxZQUFZLEdBQUcsVUFBVSxHQUFHLEVBQUU7QUFDbkMsV0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLEVBQUUsR0FBRyxFQUFFLE1BQU0sQ0FBQyxDQUFBO0FBQ3hDLFdBQU8sQ0FBQyxHQUFHLENBQUMsYUFBYSxFQUFFLE1BQU0sQ0FBQyxDQUFBO0FBQ2xDLFFBQUksYUFBYSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUM7QUFDOUIsaUJBQVcsRUFBRSxxQ0FBcUM7QUFDbEQsaUJBQVcsRUFBRSxrQkFBa0I7QUFDL0IsZ0JBQVUsRUFBRSxvQkFBb0I7QUFDaEMsYUFBTyxFQUFFO0FBQ1AsZUFBTyxFQUFFLG1CQUFZO0FBQ25CLGlCQUFPLEdBQUcsQ0FBQTtTQUNYO09BQ0Y7S0FDRixDQUFDLENBQUM7R0FDSixDQUFBOzs7QUFHRCxRQUFNLENBQUMscUJBQXFCLEdBQUcsWUFBWTtBQUN6QyxXQUFPLENBQUMsR0FBRyxDQUFDLHVCQUF1QixDQUFDLENBQUE7QUFDcEMsUUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLE1BQU0sRUFBRTtBQUM1QyxhQUFPLENBQUMsR0FBRyxDQUFDLHdCQUF3QixFQUFFLE1BQU0sQ0FBQyxDQUFBO0FBQzdDLFVBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFBO0tBQ3pCLENBQUMsQ0FBQTtHQUNILENBQUE7Q0FFRixDQUFBOzs7QUN0R0QsWUFBWSxDQUFDOztBQUViLElBQUksU0FBUyxHQUFHLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQTs7Ozs7O0FBTXBDLE9BQU8sQ0FBQyxRQUFRLEdBQUcsVUFBVSxNQUFNLEVBQUUsVUFBVSxFQUFFLEVBQUUsRUFBRSxPQUFPLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRTs7QUFFNUcsUUFBSSxtQkFBbUIsR0FBRyxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUE7QUFDaEYsVUFBTSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7QUFDbkIsVUFBTSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUE7O0FBRTFCLFVBQU0sQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDLGFBQWEsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFBOztBQUUvRCxVQUFNLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQTtBQUN2QixVQUFNLENBQUMsVUFBVSxHQUFHLENBQUMsQ0FBQTtBQUNyQixVQUFNLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQTtBQUNwQixVQUFNLENBQUMsZUFBZSxHQUFHLEdBQUcsQ0FBQTtBQUM1QixVQUFNLENBQUMsT0FBTyxHQUFHLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQTs7QUFFbkMsUUFBSSxRQUFRLENBQUMsTUFBTSxLQUFLLE9BQU8sRUFBRTs7OztBQUk3QixnQkFBUSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsbUJBQW1CLENBQUMsQ0FFNUMsSUFBSSxDQUFDLFVBQVUsS0FBSyxFQUFFOztBQUVuQixpQkFBSyxDQUFDLEdBQUcsQ0FBQyxVQUFVLElBQUksRUFBRTs7QUFFdEIscUJBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxNQUFNLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtBQUM5Qyx3QkFBSSxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsT0FBTyxFQUFFO0FBQzNDLDhCQUFNLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFBO0FBQ2xELDhCQUFNLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUE7cUJBQ3ZDO2lCQUNKO2FBQ0osQ0FBQyxDQUFBO1NBQ0wsQ0FBQyxDQUFBOzs7O0FBS04sY0FBTSxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsVUFBVSxJQUFJLEVBQUUsSUFBSSxFQUFFOztBQUU3QyxnQkFBSSxDQUFDO2dCQUFFLGFBQWEsR0FBRyxDQUFDLENBQUE7QUFDeEIsa0JBQU0sQ0FBQyxZQUFZLEdBQUcsQ0FBQyxDQUFBOztBQUV2QixpQkFBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxNQUFNLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtBQUMxQyxvQkFBSSxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsRUFBRTtBQUM5QiwwQkFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUE7QUFDNUQsaUNBQWEsR0FBRyxhQUFhLEdBQUcsQ0FBQyxDQUFBO0FBQ2pDLDBCQUFNLENBQUMsWUFBWSxHQUFHLE1BQU0sQ0FBQyxZQUFZLEdBQUcsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUE7aUJBQ3pFLE1BRUcsT0FBTyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQTthQUN4QztBQUNELGdCQUFJLGFBQWEsR0FBRyxDQUFDLEVBQ2pCLE1BQU0sQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFBLEtBRXBCLE1BQU0sQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFBO0FBQ3hCLGtCQUFNLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFlBQVksR0FBRyxNQUFNLENBQUMsUUFBUSxHQUFHLEdBQUcsQ0FBQyxDQUFBO0FBQ2hGLGtCQUFNLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQyxZQUFZLEdBQUcsTUFBTSxDQUFDLGVBQWUsQ0FBQTtTQUVuRSxFQUFFLElBQUksQ0FBQyxDQUFDO0tBRVo7O0FBRUQsYUFBUyxhQUFhLEdBQUc7QUFDckIsWUFBSSxHQUFHLEdBQUcsRUFBRSxDQUFBO0FBQ1osYUFBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO0FBQzlDLGdCQUFJLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxFQUFFO0FBQzlCLG1CQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUE7YUFDckM7U0FDSjtBQUNELGVBQU8sR0FBRyxDQUFBO0tBQ2I7O0FBRUQsVUFBTSxDQUFDLFVBQVUsR0FBRyxVQUFVLEtBQUssRUFBRTtBQUNqQyxjQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7S0FDbEMsQ0FBQzs7QUFFRixVQUFNLENBQUMsTUFBTSxHQUFHLFlBQVk7O0FBRXhCLFlBQUksYUFBYSxFQUFFLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtBQUM3QixrQkFBTSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUE7QUFDbEIsa0JBQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDO0FBQ2Ysb0JBQUksRUFBRSxTQUFTO0FBQ2YsbUJBQUcsRUFBRSw0REFBNEQ7YUFDcEUsQ0FBQyxDQUFDO1NBQ04sTUFBTTs7QUFFSCxrQ0FBc0IsRUFBRSxDQUFBO1NBQzNCO0tBRUosQ0FBQTs7OztBQUtELGFBQVMsUUFBUSxDQUFDLE1BQU0sRUFBRTtBQUN0QixZQUFJLE1BQU0sQ0FBQyxTQUFTLEVBQ2hCLE9BQU8sSUFBSSxDQUFBLEtBRVgsT0FBTyxHQUFHLENBQUM7S0FDbEI7O0FBR0QsYUFBUyxzQkFBc0IsR0FBRzs7QUFFOUIsZ0JBQVEsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLE9BQU8sRUFBRTs7QUFFekYsbUJBQU8sRUFBQyxPQUFPLEVBQUUsT0FBTyxFQUFDLENBQUE7U0FFNUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksRUFBRTs7QUFFcEIsZ0JBQUksT0FBTyxHQUFHLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsQ0FBQTtBQUNwRCxtQkFBTyxRQUFRLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLFVBQVUsRUFBRTs7QUFFL0Qsb0JBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFBO0FBQzVCLHVCQUFPLElBQUksQ0FBQTthQUVkLENBQUMsQ0FBQTtTQUVMLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxJQUFJLEVBQUU7O0FBRXJCLG1CQUFPLFFBQVEsQ0FBQyxTQUFTLENBQUMsUUFBUSxFQUFFLGFBQWEsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVMsS0FBSyxFQUFFOztBQUVyRSxxQkFBSyxDQUFDLEdBQUcsQ0FBQyxVQUFVLElBQUksRUFBRTs7QUFFdEIsd0JBQUksQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQTtBQUMzQix3QkFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxXQUFXLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUEsQUFBQyxHQUFHLEdBQUcsQ0FBQyxDQUFBOztBQUVwRSwyQkFBTyxJQUFJLENBQUMsT0FBTyxDQUFBO0FBQ25CLHdCQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUE7QUFDakMsd0JBQUksSUFBSSxDQUFDLE9BQU8sS0FBSyxTQUFTLEVBQzFCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQTtpQkFDbkMsQ0FBQyxDQUFBOztBQUVGLHVCQUFPLEtBQUssQ0FBQTthQUVmLENBQUMsQ0FBQTtTQUVMLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBUyxLQUFLLEVBQUU7QUFDcEIsbUJBQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQyxDQUFBOztBQUV2QixnQkFBSSxLQUFLLEdBQUcsRUFBRSxDQUFBOztBQUVkLGlCQUFLLENBQUMsR0FBRyxDQUFDLFVBQVUsSUFBSSxFQUFFO0FBQ3RCLHFCQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQTthQUN4QyxDQUFDLENBQUE7O0FBRUYsY0FBRSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxPQUFPLEVBQUU7QUFDbEMsdUJBQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEVBQUUsT0FBTyxDQUFDLENBQUE7O0FBRXRDLDBCQUFVLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQztBQUMvQyx5QkFBUyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFBO2FBQ25DLENBQUMsQ0FBQTtTQUNMLENBQUMsQ0FBQTtLQUNMOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0NBNkNKLENBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQStIRCxTQUFTLFlBQVksQ0FBQyxFQUFFLEVBQUUsTUFBTSxFQUFFLFFBQVEsRUFBRTtBQUN4QyxRQUFJLFFBQVEsR0FBRyxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7QUFDMUIsUUFBSSxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUU7QUFDMUIsWUFBSSxHQUFHLEdBQUcsUUFBUSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQTtBQUN6RCxnQkFBUSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQTtLQUN4QixNQUNHLFFBQVEsQ0FBQyxNQUFNLENBQUMsRUFBQyxHQUFHLEVBQUUscUJBQXFCLEVBQUMsQ0FBQyxDQUFBO0FBQ2pELFdBQU8sUUFBUSxDQUFDLE9BQU8sQ0FBQTtDQUMxQjs7OztBQUlELE9BQU8sQ0FBQyxVQUFVLEdBQUcsVUFBVSxFQUFFLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxPQUFPLEVBQUUsUUFBUSxFQUFFO0FBQ3JFLFFBQUksU0FBUyxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQTtBQUN4QyxRQUFJLGFBQWEsR0FBRyxhQUFhLENBQUMsU0FBUyxDQUFDLENBQUE7QUFDNUMsUUFBSSxhQUFhLEtBQUssU0FBUztBQUMzQixpQkFBUyxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsYUFBYSxDQUFDLENBQUEsS0FDdkM7QUFDRCxlQUFPLFlBQVksQ0FBQyxFQUFFLEVBQUUsTUFBTSxFQUFFLFFBQVEsQ0FBQyxDQUFBO0tBQzVDO0NBQ0osQ0FBQTs7QUFFRCxTQUFTLGFBQWEsQ0FBQyxRQUFRLEVBQUU7QUFDN0IsUUFBSSxJQUFJLEdBQUcsUUFBUSxDQUFDLFdBQVcsRUFBRSxDQUFBO0FBQ2pDLFFBQUksQUFBQyxJQUFJLENBQUMsTUFBTSxLQUFLLENBQUMsSUFBTSxBQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSyxDQUFDLEFBQUM7QUFDMUQsZUFBTyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFBLEtBRTVFLE9BQU8sSUFBSSxDQUFBO0NBQ2xCOzs7Ozs7Ozs7Ozs7OztBQ3hXRCxZQUFZLENBQUM7Ozs7QUFJYixPQUFPLENBQUMseUJBQXlCLEdBQUcsVUFBVSxJQUFJLEVBQUU7QUFDbEQsU0FBTyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQTtDQUNoQyxDQUFBOzs7Ozs7QUFNRCxPQUFPLENBQUMsV0FBVyxHQUFHLFVBQVUsTUFBTSxFQUFFLFlBQVksRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUUsWUFBWSxFQUFFLFdBQVcsRUFBRTs7QUFFekgsU0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsWUFBWSxDQUFDLENBQUE7O0FBRW5DLE1BQUksV0FBVyxDQUFDLFVBQVUsRUFBRSxFQUFFO0FBQzVCLGFBQVMsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxDQUFBO0dBQzVELE1BQU07QUFDTCxVQUFNLENBQUMsZUFBZSxHQUFHLFlBQVksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFBO0FBQ2hELFFBQUksWUFBWSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQ3pCLE1BQU0sQ0FBQyxJQUFJLEdBQUcsRUFBRSxLQUFLLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUE7U0FDckM7O0FBQ0gsY0FBTSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQTtBQUM5QyxlQUFPLENBQUMsR0FBRyxDQUFDLGlCQUFpQixFQUFFLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQTtBQUM5QyxjQUFNLENBQUMsSUFBSSxHQUFHO0FBQ1osZUFBSyxFQUFFLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSztBQUMzQixjQUFJLEVBQUUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxZQUFZO0FBQ2pDLGlCQUFPLEVBQUUsTUFBTSxDQUFDLE9BQU87U0FDeEIsQ0FBQTtPQUNGO0dBQ0Y7O0FBRUQsUUFBTSxDQUFDLFlBQVksR0FBRyxVQUFVLElBQUksRUFBRTs7QUFFcEMsZUFBVyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxNQUFNLEVBQUU7QUFDOUMsVUFBSSxNQUFNLENBQUMsYUFBYSxLQUFLLElBQUksRUFBRTs7QUFFakMsWUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLE1BQU0sRUFBRTtBQUM1QyxpQkFBTyxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsRUFBRSxNQUFNLENBQUMsQ0FBQTtBQUM3QyxjQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQTs7QUFFeEIsb0JBQVUsQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxDQUFDOztBQUUvQyxvQkFBVSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLENBQUM7QUFDaEQsY0FBSSxDQUFDLGtCQUFrQixFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsS0FBSyxFQUFFO0FBQzlDLGdCQUFJLEtBQUssR0FBRyxDQUFDLEVBQ1gsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksRUFBRTtBQUN4Qyx1QkFBUyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFBO2FBQ3JELENBQUMsQ0FBQSxLQUVGLFNBQVMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUE7V0FDL0IsQ0FBQyxDQUFBO1NBQ0gsQ0FBQyxDQUFBO09BQ0gsTUFDSTtBQUNILGNBQU0sQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFBO0FBQ2xCLGNBQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxHQUFHLEVBQUUsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7T0FDOUQ7S0FDRixDQUFDLENBQUE7R0FDSCxDQUFBOztBQUVELFFBQU0sQ0FBQyxTQUFTLEdBQUcsVUFBVSxJQUFJLEVBQUU7QUFDakMsVUFBTSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUE7QUFDbEIsV0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQTtBQUNuQixXQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFBOztBQUVqQixlQUFXLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLE1BQU0sRUFBRTtBQUM5QyxVQUFJLE1BQU0sQ0FBQyxVQUFVLEVBQUU7QUFDckIsY0FBTSxDQUFDLFlBQVksQ0FBQyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQTtPQUNwRSxNQUNJO0FBQ0gsY0FBTSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUE7QUFDbEIsY0FBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLEdBQUcsRUFBRSxNQUFNLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztPQUM5RDtLQUNGLENBQUMsQ0FBQTtHQUNILENBQUE7Q0FHRixDQUFBOzs7O0FBSUQsT0FBTyxDQUFDLHNCQUFzQixHQUFHLFVBQVUsTUFBTSxFQUFFLFlBQVksRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFO0FBQ3RILFNBQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUE7O0FBRXpCLFFBQU0sQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDOztBQUVuQixRQUFNLENBQUMsVUFBVSxHQUFHLFVBQVUsS0FBSyxFQUFFO0FBQ25DLFVBQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztHQUNoQyxDQUFDOztBQUdGLE1BQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxLQUFLLEVBQUU7QUFDekMsV0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtBQUNsQixVQUFNLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQTtHQUNwQixDQUFDLENBQUE7O0FBRUYsUUFBTSxDQUFDLFNBQVMsR0FBRyxVQUFVLElBQUksRUFBRTtBQUNqQyxVQUFNLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQTtBQUNsQixXQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFBO0FBQ25CLFdBQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUE7O0FBRWpCLGVBQVcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsTUFBTSxFQUFFO0FBQzlDLFVBQUksTUFBTSxDQUFDLFVBQVUsRUFBRTtBQUNyQixjQUFNLENBQUMsWUFBWSxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFBO09BQ3BFLE1BQ0k7QUFDSCxjQUFNLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQTtBQUNsQixjQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsR0FBRyxFQUFFLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO09BQzlEO0tBQ0YsQ0FBQyxDQUFBO0dBQ0gsQ0FBQTs7QUFFRCxRQUFNLENBQUMsWUFBWSxHQUFHLFVBQVUsSUFBSSxFQUFFOztBQUVwQyxlQUFXLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLE1BQU0sRUFBRTtBQUM5QyxVQUFJLE1BQU0sQ0FBQyxhQUFhLEtBQUssSUFBSSxFQUFFOztBQUVqQyxZQUFJLENBQUMsZUFBZSxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsTUFBTSxFQUFFO0FBQzVDLGlCQUFPLENBQUMsR0FBRyxDQUFDLHdCQUF3QixFQUFFLE1BQU0sQ0FBQyxDQUFBO0FBQzdDLGNBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFBOztBQUV4QixvQkFBVSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLENBQUM7O0FBRS9DLG9CQUFVLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsQ0FBQztBQUNoRCxjQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxLQUFLLEVBQUU7QUFDOUMsZ0JBQUksS0FBSyxHQUFHLENBQUMsRUFDWCxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsSUFBSSxFQUFFO0FBQ3hDLHVCQUFTLENBQUMsSUFBSSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUE7YUFDckQsQ0FBQyxDQUFBLEtBRUYsU0FBUyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQTtXQUMvQixDQUFDLENBQUE7U0FDSCxDQUFDLENBQUE7T0FDSCxNQUNJO0FBQ0gsY0FBTSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUE7QUFDbEIsY0FBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLEdBQUcsRUFBRSxNQUFNLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztPQUM5RDtLQUNGLENBQUMsQ0FBQTtHQUNILENBQUE7Q0FHRixDQUFBOzs7QUNoSkQsWUFBWSxDQUFDOztBQUViLElBQUksU0FBUyxHQUFHLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQTs7QUFFcEMsSUFBSSxnQkFBZ0IsR0FBRyxTQUFTLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQUMsSUFBSTtXQUFLLElBQUksQ0FBQyxJQUFJO0NBQUEsQ0FBQyxDQUFBOztBQUV6RixPQUFPLENBQUMsVUFBVSxHQUFHLFVBQVUsS0FBSyxFQUFFLFlBQVksRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLFdBQVcsRUFBRTs7O0FBRXpHLFFBQUksQ0FBQyxHQUFHLEdBQUcsbUJBQW1CLENBQUE7O0FBRTlCLFFBQUksQ0FBQyxxQkFBcUIsR0FBRyxLQUFLLENBQUE7Ozs7QUFJbEMsUUFBSSxHQUFHLEdBQUcsZUFBZSxDQUFBOztBQUd6QixXQUFPLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFDLENBQUE7Ozs7O0FBSy9CLFNBQUssQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQUMsR0FBRyxFQUFLO0FBQzVCLGVBQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFBO0FBQ3ZCLFlBQUksR0FBRyxDQUFDLE9BQU8sRUFBRTs7QUFFYixtQkFBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQTtBQUN2QixtQkFBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUE7QUFDbEQsa0JBQUsscUJBQXFCLEdBQUcsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQTtBQUN4RSxtQkFBTyxDQUFDLEdBQUcsT0FBTSxDQUFBO1NBQ3BCO0tBQ0osQ0FBQyxDQUFBO0NBRUwsQ0FBQTs7O0FDakNELFlBQVksQ0FBQzs7QUFFYixJQUFJLFNBQVMsR0FBRyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUE7O0FBRXBDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsVUFBVSxNQUFNLEVBQUUsS0FBSyxFQUFFLFlBQVksRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLFdBQVcsRUFBRTs7QUFFbkgsUUFBTSxDQUFDLFdBQVcsR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDLFdBQVcsRUFBRSxDQUFBO0FBQzdDLFFBQU0sQ0FBQyxZQUFZLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQyxRQUFRLEVBQUUsR0FBRyxDQUFDLENBQUE7QUFDL0MsUUFBTSxDQUFDLE1BQU0sR0FBRyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFBO0FBQzlDLFFBQU0sQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDLGFBQWEsRUFBRSxDQUFBOztBQUU1QyxRQUFNLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQTs7QUFFcEIsT0FBSyxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsSUFBSSxFQUFFOztBQUU3RCxXQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQTtBQUN6QixRQUFJLElBQUksQ0FBQyxPQUFPLEVBQ2QsSUFBSSxDQUFDLE9BQU8sR0FBRyxTQUFTLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQTtBQUN2RCxVQUFNLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQTtHQUN2QixDQUFDLENBQUE7O0FBRUYsT0FBSyxDQUFDLEdBQUcsQ0FBQyx1QkFBdUIsR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsSUFBSSxFQUFFOztBQUVoRixXQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsQ0FBQTtBQUM5QixVQUFNLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUE7R0FDL0IsQ0FBQyxDQUFBOztBQUVGLE9BQUssQ0FBQyxHQUFHLENBQUMsb0NBQW9DLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxJQUFJLEVBQUU7QUFDdEUsVUFBTSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFBO0dBQ2xDLENBQUMsQ0FBQTs7QUFFRixRQUFNLENBQUMsTUFBTSxDQUFDLG9CQUFvQixFQUFFLFVBQVUsTUFBTSxFQUFFLE1BQU0sRUFBRTtBQUM1RCxVQUFNLENBQUMsUUFBUSxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUE7R0FDOUMsQ0FBQyxDQUFDOztBQUVILFFBQU0sQ0FBQyxNQUFNLENBQUMsbUJBQW1CLEVBQUUsVUFBVSxNQUFNLEVBQUUsTUFBTSxFQUFFO0FBQzNELFVBQU0sQ0FBQyxRQUFRLENBQUMsV0FBVyxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQTtHQUMvQyxDQUFDLENBQUM7O0FBRUgsUUFBTSxDQUFDLFdBQVcsR0FBRyxVQUFVLE1BQU0sRUFBRSxRQUFRLEVBQUU7O0FBRS9DLFFBQUksUUFBUSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsRUFBRTtBQUNwQyxZQUFNLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQTtBQUNsQixZQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsR0FBRyxFQUFFLFFBQVEsQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztLQUN0RSxNQUFNOztBQUVMLFlBQU0sQ0FBQyxHQUFHLEdBQUcsNENBQTRDLENBQUE7O0FBRXpELFdBQUssQ0FBQyxJQUFJLENBQUMscUNBQXFDLEdBQUcsWUFBWSxDQUFDLE9BQU8sRUFBRSxRQUFRLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxJQUFJLEVBQUU7QUFDekcsZUFBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQTtBQUNqQixZQUFJLElBQUksQ0FBQyxFQUFFLEVBQUU7QUFDWCxvQkFBVSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLENBQUM7QUFDL0MsbUJBQVMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFBO1NBQ25ELE1BQU07QUFDTCxnQkFBTSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUE7QUFDakIsZ0JBQU0sQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFBO0FBQ2xCLGdCQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsR0FBRyxFQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztTQUNoRTtPQUNGLENBQUMsQ0FBQztLQUNKO0dBQ0YsQ0FBQztDQUNILENBQUE7Ozs7QUM1REQsWUFBWSxDQUFDOzs7O0FBSWIsSUFBSSxTQUFTLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFBO0FBQ3BDLElBQUksYUFBYSxHQUFHLE9BQU8sQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFBO0FBQy9ELElBQUksSUFBSSxHQUFHLE9BQU8sQ0FBQyx5QkFBeUIsQ0FBQyxDQUFBO0FBQzdDLElBQUksS0FBSyxHQUFHLE9BQU8sQ0FBQywwQkFBMEIsQ0FBQyxDQUFBO0FBQy9DLElBQUksRUFBRSxHQUFHLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxDQUFBOztBQUV6QyxPQUFPLENBQUMsTUFBTSxDQUFDLHFCQUFxQixFQUFFLEVBQUUsQ0FBQzs7OztDQUlwQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxFQUFFLGNBQWMsRUFBRSxXQUFXLEVBQUUsWUFBWSxFQUFFLG9CQUFvQixFQUFFLGNBQWMsRUFBRSxXQUFXLEVBQzVILFVBQVUsTUFBTSxFQUFFLFlBQVksRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLGtCQUFrQixFQUFFLFlBQVksRUFBRSxTQUFTLEVBQUU7O0FBRWhHLFdBQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLFlBQVksQ0FBQyxDQUFBOztBQUdwQyxRQUFJLFVBQVUsR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyw4Q0FBOEMsR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQTs7QUFFdkgsYUFBUyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQTtBQUM5QixhQUFTLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxDQUFBO0FBQ2xDLGFBQVMsQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLENBQUE7O0FBRXJDLFFBQUksWUFBWSxHQUFHLFNBQVMsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQTs7QUFFbkUsVUFBTSxDQUFDLFlBQVksR0FBRyxZQUFZLENBQUE7QUFDbEMsVUFBTSxDQUFDLFlBQVksQ0FBQyxjQUFjLEdBQUcsVUFBVSxDQUFBOztBQUUvQyxXQUFPLENBQUMsR0FBRyxDQUFDLHNCQUFzQixFQUFFLFlBQVksQ0FBQyxDQUFBOztBQUVqRCxRQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLEtBQUssU0FBUyxFQUFFOztBQUUxQyxjQUFNLENBQUMsT0FBTyxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUE7QUFDckMsY0FBTSxDQUFDLEtBQUssR0FBRyxrQkFBa0IsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFBOztBQUUvRCxZQUFJLGFBQWEsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEVBQzdCLE1BQU0sQ0FBQyxNQUFNLEdBQUcsYUFBYSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQSxLQUMvQzs7QUFFRCxnQkFBSSxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxLQUFLLFNBQVMsRUFDekMsTUFBTSxDQUFDLE1BQU0sR0FBRyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxZQUFZLEVBQUUsWUFBWSxDQUFDLENBQUEsS0FDekUsSUFBSSxFQUFFLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxLQUFLLFNBQVMsRUFDM0MsTUFBTSxDQUFDLE1BQU0sR0FBRyxhQUFhLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxZQUFZLEVBQUUsWUFBWSxDQUFDLENBQUE7U0FDOUU7S0FFSixNQUFNO0FBQ0gsaUJBQVMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQTtLQUNwQzs7OztBQUtELFdBQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxFQUFFLE1BQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQTtBQUNuRCxVQUFNLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQTs7QUFFdkIsVUFBTSxDQUFDLE1BQU0sR0FBRyxZQUFZO0FBQ3hCLGVBQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLFVBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFBO0FBQ3pDLGVBQU8sVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFBO0tBQzFCLENBQUE7Q0FHSixDQUNKLENBQUMsQ0FFRCxVQUFVLENBQUMsY0FBYyxFQUFFLFVBQVUsTUFBTSxFQUFFLFlBQVksRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLGtCQUFrQixFQUFFLFlBQVksRUFBRSxTQUFTLEVBQUU7O0FBRTVILFdBQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxFQUFFLFNBQVMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFBO0FBQzdDLFdBQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxFQUFFLFlBQVksQ0FBQyxDQUFBO0FBQ3pDLFdBQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxFQUFFLFlBQVksQ0FBQyxDQUFBOzs7O0FBSXpDLFFBQUksVUFBVSxHQUFHLDRCQUE0QixHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLHNCQUFzQixDQUFBOztBQUVsRyxXQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksRUFBRSxVQUFVLENBQUMsQ0FBQTtBQUNyQyxhQUFTLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFBO0FBQzlCLGFBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFBO0FBQ3hELFdBQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLFlBQVksQ0FBQyxDQUFBOzs7Ozs7O0FBU3RDLFFBQUksWUFBWSxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFBO0FBQzVDLFFBQUksWUFBWSxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFBOztBQUU1QyxVQUFNLENBQUMsWUFBWSxHQUFHLFlBQVksQ0FBQTtBQUNsQyxVQUFNLENBQUMsY0FBYyxHQUFHLFlBQVksQ0FBQyxlQUFlLEdBQUcsRUFBRSxHQUFHLFVBQVUsQ0FBQTtBQUN0RSxVQUFNLENBQUMsa0JBQWtCLEdBQUcsWUFBWSxDQUFDLG1CQUFtQixHQUFHLEVBQUUsR0FBRyxVQUFVLENBQUE7O0FBRTlFLFVBQU0sQ0FBQyxNQUFNLEdBQUcsWUFBWTs7QUFFeEIsZUFBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUE7QUFDekMsZUFBTyxVQUFVLENBQUMsR0FBRyxFQUFFLENBQUE7S0FDMUIsQ0FBQTs7QUFHRCxRQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxTQUFTLEVBQUU7O0FBRWxDLGNBQU0sQ0FBQyxPQUFPLEdBQUcsWUFBWSxDQUFBO0FBQzdCLGNBQU0sQ0FBQyxLQUFLLEdBQUcsa0JBQWtCLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQTs7QUFFL0QsWUFBSSxhQUFhLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxFQUM3QixNQUFNLENBQUMsTUFBTSxHQUFHLGFBQWEsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUEsS0FDL0M7O0FBRUQsZ0JBQUksS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxTQUFTLEVBQ25DLE1BQU0sQ0FBQyxNQUFNLEdBQUcsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsWUFBWSxFQUFFLFlBQVksQ0FBQyxDQUFBLEtBQ3pFLElBQUksRUFBRSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxTQUFTLEVBQ3JDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsYUFBYSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsWUFBWSxFQUFFLFlBQVksQ0FBQyxDQUFBO1NBQzlFO0tBRUosTUFBTTtBQUNILGlCQUFTLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUE7S0FDcEM7Q0FDSixDQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztDQW1EQSxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsY0FBYyxFQUFFLG9CQUFvQixFQUFFLGNBQWMsRUFDMUcsVUFBVSxNQUFNLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsa0JBQWtCLEVBQUUsWUFBWSxFQUFFOztBQUVoRixVQUFNLENBQUMsT0FBTyxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUE7QUFDckMsVUFBTSxDQUFDLFlBQVksR0FBRyxZQUFZLENBQUE7O0FBRWxDLFVBQU0sQ0FBQyxrQkFBa0IsR0FBRyxVQUFVLE9BQU8sRUFBRTtBQUMzQyxlQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFBOztBQUV2QiwwQkFBa0IsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsQ0FBQTtBQUM1QywwQkFBa0IsQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLENBQUE7QUFDNUMsaUJBQVMsQ0FBQyxJQUFJLENBQUMsYUFBYSxHQUFHLE9BQU8sQ0FBQyxDQUFBO0tBQzFDLENBQUE7Q0FDSixDQUNKLENBQUM7Ozs7O0NBS0QsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsRUFBRSxPQUFPLEVBQUUsV0FBVyxFQUFFLFlBQVksRUFBRSxjQUFjLEVBQUUsUUFBUSxFQUFFLFdBQVcsRUFBRSxvQkFBb0IsRUFDL0gsYUFBYSxFQUFFLG1CQUFtQixFQUFFLGNBQWMsRUFDbEQsVUFBVSxNQUFNLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsa0JBQWtCLEVBQUUsV0FBVyxFQUM1RyxpQkFBaUIsRUFBRSxZQUFZLEVBQUU7O0FBRWpDLFVBQU0sQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFBOztBQUU1QixVQUFNLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFBO0FBQzVCLFVBQU0sQ0FBQyxVQUFVLEdBQUcsV0FBVyxDQUFDLFVBQVUsQ0FBQTs7O0FBRzFDLFNBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxFQUFFLEVBQUU7QUFDMUIsWUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFBO0FBQ3BCLFlBQUksQ0FBQyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQ2QsQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDLENBQUE7QUFDZixjQUFNLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDO0FBQ3pCLGdCQUFJLEVBQUUsQ0FBQztBQUNQLG9CQUFRLEVBQUUsS0FBSztTQUNsQixDQUFDLENBQUE7S0FDTDs7O0FBR0QsUUFBSSxhQUFhLEdBQUcsU0FBaEIsYUFBYSxDQUFhLE9BQU8sRUFBRTs7QUFFbkMsYUFBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQ25ELE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFBOztBQUUvQyxhQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtBQUNyQyxnQkFBSSxJQUFJLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQTtBQUNwQyxrQkFBTSxDQUFDLGdCQUFnQixDQUFDLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFBO1NBQ3BEO0tBRUosQ0FBQTs7Ozs7OztBQU9ELFVBQU0sQ0FBQyxPQUFPLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQTtBQUNyQyxVQUFNLENBQUMsWUFBWSxHQUFHLFlBQVksQ0FBQTs7QUFFbEMsVUFBTSxDQUFDLFlBQVksR0FBRyxpQkFBaUIsQ0FBQyxZQUFZLENBQUE7QUFDcEQsVUFBTSxDQUFDLEdBQUcsR0FBRyxpQkFBaUIsQ0FBQyxJQUFJLENBQUE7O0FBRW5DLFFBQUksTUFBTSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEtBQUssRUFBRSxFQUFFO0FBQ3hCLGNBQU0sQ0FBQyxHQUFHLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFBO0FBQ2hELGVBQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUE7S0FDdEI7O0FBRUQsUUFBSSxNQUFNLENBQUMsR0FBRyxFQUNWLGFBQWEsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFBOztBQUVyQyxRQUFJLE9BQU8sR0FBRyxTQUFWLE9BQU8sQ0FBYSxHQUFHLEVBQUUsRUFBRSxFQUFFOztBQUc3QixZQUFJLGFBQWEsR0FBRyxTQUFoQixhQUFhLENBQVksT0FBTyxFQUFFO0FBQ2xDLGdCQUFJLFVBQVUsR0FBRyxDQUFDLEdBQUcsRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUE7QUFDbkMsbUJBQU8sVUFBVSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQTtTQUU1QyxDQUFBOztBQUVELFlBQUksR0FBRyxHQUFHLElBQUksQ0FBQTtBQUNkLFlBQUksR0FBRyxDQUFDLElBQUksS0FBSyxHQUFHLEVBQUU7QUFDbEIsZ0JBQUksR0FBRyxDQUFDLFFBQVEsS0FBSyxFQUFFLEVBQ25CLEdBQUcsR0FBRyw4QkFBOEIsQ0FBQTtTQUMzQztBQUNELFlBQUksQ0FBQyxHQUFHLEVBQUU7OztBQUVOLG1CQUFPLENBQUMsR0FBRyxDQUFDLHdCQUF3QixFQUFFLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQTs7QUFFL0MsZ0JBQUksYUFBYSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFDM0I7QUFDSSxvQkFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQ2IsR0FBRyxHQUFHLDRCQUE0QixDQUFBO2FBQ3pDO1NBRUo7O0FBRUQsWUFBSSxDQUFDLEdBQUcsRUFBRTtBQUNOLGdCQUFJLEdBQUcsQ0FBQyxPQUFPLENBQUMsTUFBTSxLQUFLLENBQUMsRUFDeEIsR0FBRyxHQUFHLHNDQUFzQyxDQUFBLEtBQzNDO0FBQ0QsdUJBQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUE7Ozs7Ozs7O2FBU3BCO1NBQ0o7O0FBR0QsWUFBSSxHQUFHLEVBQ0gsRUFBRSxDQUFDLElBQUksS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUEsS0FFbEIsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFBO0tBQ2YsQ0FBQTs7QUFFRCxVQUFNLENBQUMsTUFBTSxHQUFHLFVBQVUsR0FBRyxFQUFFOztBQUUzQixlQUFPLENBQUMsR0FBRyxFQUFFLFVBQVUsR0FBRyxFQUFFO0FBQ3hCLGdCQUFJLEdBQUcsRUFBRTtBQUNMLHNCQUFNLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQTtBQUNsQixzQkFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7QUFDZix3QkFBSSxFQUFFLFNBQVM7QUFDZix1QkFBRyxFQUFFLEdBQUcsQ0FBQyxPQUFPO2lCQUNuQixDQUFDLENBQUM7YUFDTixNQUFNO0FBQ0gsbUJBQUcsQ0FBQyxPQUFPLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQTtBQUNsQyxtQkFBRyxDQUFDLE1BQU0sR0FBRyxrQkFBa0IsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFBO0FBQ3RELGtDQUFrQixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQTtBQUM1Qix5QkFBUyxDQUFDLElBQUksQ0FBQyxhQUFhLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFBO2FBQzlDO1NBQ0osQ0FBQyxDQUFBO0tBQ0wsQ0FBQTs7QUFFRCxVQUFNLENBQUMsWUFBWSxHQUFHLFVBQVUsR0FBRyxFQUFFOztBQUVqQyxlQUFPLENBQUMsR0FBRyxFQUFFLFVBQVUsR0FBRyxFQUFFO0FBQ3hCLGdCQUFJLEdBQUcsRUFBRTtBQUNMLHNCQUFNLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQTtBQUNsQixzQkFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7QUFDZix3QkFBSSxFQUFFLFNBQVM7QUFDZix1QkFBRyxFQUFFLEdBQUcsQ0FBQyxPQUFPO2lCQUNuQixDQUFDLENBQUM7YUFDTixNQUFNO0FBQ0gsbUJBQUcsQ0FBQyxPQUFPLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQTtBQUNsQyxtQkFBRyxDQUFDLE1BQU0sR0FBRyxrQkFBa0IsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFBO0FBQ3RELGtDQUFrQixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQTs7QUFFNUIseUJBQVMsQ0FBQyxJQUFJLENBQUMscUJBQXFCLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFBO2FBQ3pEO1NBQ0osQ0FBQyxDQUFBO0tBQ0wsQ0FBQTs7QUFFRCxVQUFNLENBQUMsaUJBQWlCLEdBQUcsVUFBVSxLQUFLLEVBQUU7QUFDeEMsZUFBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsS0FBSyxDQUFDLENBQUE7S0FDbkMsQ0FBQTs7QUFFRCxVQUFNLENBQUMsU0FBUyxHQUFHLFlBQVk7QUFDM0IsZUFBTyxrQkFBa0IsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFBO0tBQ3RELENBQUE7O0FBRUQsVUFBTSxDQUFDLGFBQWEsR0FBRyxVQUFVLE9BQU8sRUFBRTtBQUN0QyxlQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxDQUFBO0FBQzVCLGVBQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUE7QUFDcEIsY0FBTSxDQUFDLGFBQWEsR0FBRyxPQUFPLENBQUE7O0tBRWpDLENBQUE7Ozs7QUFJRCxVQUFNLENBQUMsU0FBUyxHQUFHLEdBQUcsQ0FBQTs7O0FBR3RCLFVBQU0sQ0FBQyxVQUFVLEdBQUcsVUFBVSxLQUFLLEVBQUU7O0FBRWpDLGVBQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLE9BQU8sS0FBSyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQTs7QUFFMUgsWUFBSSxPQUFPLEtBQUssS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFOztBQUUzQyxnQkFBSSxFQUFFLEdBQUcsSUFBSSxRQUFRLEVBQUUsQ0FBQzs7O0FBR3hCLGNBQUUsQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDOztBQUVqQyxrQkFBTSxDQUFDLGFBQWEsR0FBRyxjQUFjLENBQUE7QUFDckMsaUJBQUssQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLEVBQUUsRUFBRTtBQUMzQiwrQkFBZSxFQUFFLElBQUk7QUFDckIsdUJBQU8sRUFBRTtBQUNMLGtDQUFjLEVBQUUsU0FBUztpQkFDNUI7QUFDRCxnQ0FBZ0IsRUFBRSxPQUFPLENBQUMsUUFBUTthQUNyQyxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsSUFBSSxFQUFFO0FBQ3ZCLHNCQUFNLENBQUMsR0FBRyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFBO0FBQzlCLHNCQUFNLENBQUMsYUFBYSxHQUFHLFVBQVUsQ0FBQTtBQUNqQyx1QkFBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUE7YUFDL0IsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFVLElBQUksRUFBRTtBQUNyQix1QkFBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQUE7YUFDOUIsQ0FBQyxDQUFDO1NBQ04sTUFBTTtBQUNILGlCQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksR0FBRyx1QkFBdUIsQ0FBQyxDQUFBO1NBQ2pEO0tBQ0osQ0FBQTs7QUFFRCxVQUFNLENBQUMsWUFBWSxHQUFHLFVBQVUsQ0FBQyxFQUFFOztBQUUvQixZQUFJLENBQUMsQ0FBQyxRQUFRLEVBQUU7O0FBRVosZ0JBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxPQUFPLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRTs7QUFDMUQsdUJBQU8sT0FBTyxDQUFDLElBQUksS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFBO2FBQ2pDLENBQUMsRUFBRTtBQUNBLHNCQUFNLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUM7QUFDcEIsd0JBQUksRUFBRSxDQUFDLENBQUMsSUFBSTtBQUNaLDJCQUFPLEVBQUUsRUFBRTtpQkFDZCxDQUFDLENBQUE7YUFDTDtTQUNKLE1BQU07QUFDSCxpQkFBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtBQUNoRCxvQkFBSSxNQUFNLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssQ0FBQyxDQUFDLElBQUksRUFDckMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQTthQUN0QztTQUNKO0tBRUosQ0FBQTs7OztBQUlELFVBQU0sQ0FBQyxJQUFJLEdBQUcsWUFBWTs7QUFHdEIsWUFBSSxhQUFhLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQztBQUM1Qix1QkFBVyxFQUFFLGtDQUFrQztBQUMvQyxzQkFBVSxFQUFFLG1CQUFtQjtBQUMvQixtQkFBTyxFQUFFO0FBQ0wscUJBQUssRUFBRSxpQkFBWTtBQUNmLDJCQUFPLE1BQU0sQ0FBQyxLQUFLLENBQUM7aUJBQ3ZCO2FBQ0o7U0FDSixDQUFDLENBQUM7O0FBRUgscUJBQWEsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsZUFBZSxFQUFFO0FBQ2pELGdCQUFJLGVBQWUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFOztBQUU1Qix1QkFBTyxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsRUFBRyxlQUFlLENBQUMsQ0FBQTtBQUNqRCxzQkFBTSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFBO0FBQ3ZCLHFCQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsZUFBZSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtBQUM3QywyQkFBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtBQUMvQix3QkFBSSxJQUFJLEdBQUcsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsR0FBRyxJQUFJLEdBQUcsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksR0FBRyxHQUFHLENBQUE7QUFDdkYsMEJBQU0sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztBQUNwQiw0QkFBSSxFQUFFLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJO0FBQzdCLCtCQUFPLEVBQUUsSUFBSTtxQkFDaEIsQ0FBQyxDQUFBO2lCQUNMO0FBQ0QsNkJBQWEsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFBO2FBQ3BDOztTQUVKLEVBQUUsWUFBWTs7U0FFZCxDQUFDLENBQUM7S0FDTixDQUFDOzs7Q0FHTCxDQUNKLENBQUM7OztDQUdELFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxRQUFRLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxZQUFZLEVBQUUsY0FBYyxFQUFFLFFBQVEsRUFBRSxXQUFXLEVBQ3pHLG9CQUFvQixFQUFFLFVBQVUsRUFBRSxhQUFhLEVBQUUsYUFBYSxFQUM5RCxVQUFVLE1BQU0sRUFBRSxLQUFLLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxrQkFBa0IsRUFBRSxRQUFRLEVBQUUsV0FBVyxFQUFFLFdBQVcsRUFBRTs7QUFFckksV0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQTs7QUFFckIsV0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQTs7QUFFbkIsVUFBTSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUE7O0FBRzVCLFVBQU0sQ0FBQyxPQUFPLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQTs7QUFFckMsVUFBTSxDQUFDLEdBQUcsR0FBRyxrQkFBa0IsQ0FBQyxHQUFHLEVBQUUsQ0FBQTs7QUFFckMsVUFBTSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFBO0FBQzFDLFVBQU0sQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUE7QUFDL0MsVUFBTSxDQUFDLFVBQVUsR0FBRyxXQUFXLENBQUMsYUFBYSxDQUFBO0FBQzdDLFVBQU0sQ0FBQyxpQkFBaUIsR0FBRyxXQUFXLENBQUMsaUJBQWlCLENBQUE7QUFDeEQsVUFBTSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUMsUUFBUSxDQUFBOztBQUVuQyxXQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQTtBQUN2QixXQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxPQUFPLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUE7O0FBR3ZDLFVBQU0sQ0FBQyxNQUFNLEdBQUcsVUFBVSxHQUFHLEVBQUU7QUFDM0IsV0FBRyxDQUFDLE9BQU8sR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFBO0FBQ2xDLDBCQUFrQixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQTs7QUFFNUIsaUJBQVMsQ0FBQyxJQUFJLENBQUMscUJBQXFCLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFBOzs7Ozs7OztLQVF6RCxDQUFBOztBQUVELFVBQU0sQ0FBQyxJQUFJLEdBQUcsVUFBVSxHQUFHLEVBQUU7QUFDekIsMEJBQWtCLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFBO0FBQzVCLGlCQUFTLENBQUMsSUFBSSxDQUFDLGFBQWEsR0FBRyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUE7S0FDOUMsQ0FBQTs7QUFFRCxVQUFNLENBQUMsUUFBUSxHQUFHLFVBQVUsS0FBSyxFQUFFO0FBQy9CLFlBQUksQ0FBQyxLQUFLLEVBQ04sTUFBTSxDQUFDLEdBQUcsQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFBO0tBQ3hDLENBQUE7O0FBRUQsVUFBTSxDQUFDLFNBQVMsR0FBRyxZQUFZO0FBQzNCLGVBQU8sa0JBQWtCLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQTtLQUN0RCxDQUFBO0NBRUosQ0FDSixDQUFDOzs7O0NBSUQsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsRUFBRSxPQUFPLEVBQUUsV0FBVyxFQUFFLFlBQVksRUFBRSxjQUFjLEVBQUUsUUFBUSxFQUFFLFdBQVcsRUFBRSxvQkFBb0IsRUFBRSxTQUFTLEVBQzFJLFVBQVUsTUFBTSxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLGtCQUFrQixFQUFFLE9BQU8sRUFBRTs7QUFFMUcsVUFBTSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7O0FBRW5CLFNBQUssQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxJQUFJLEVBQUU7QUFDaEQsY0FBTSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUE7S0FDekIsQ0FBQyxDQUFBOztBQUVGLFVBQU0sQ0FBQyxVQUFVLEdBQUcsVUFBVSxLQUFLLEVBQUU7QUFDakMsY0FBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO0tBQ2xDLENBQUM7O0FBRUYsVUFBTSxDQUFDLE9BQU8sR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFBO0FBQ3JDLFVBQU0sQ0FBQyxHQUFHLEdBQUcsa0JBQWtCLENBQUMsR0FBRyxFQUFFLENBQUE7O0FBRXJDLFVBQU0sQ0FBQyxXQUFXLEdBQUcsWUFBWTtBQUM3QixlQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFBO0FBQ3RCLGVBQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUE7QUFDN0IsWUFBSSxJQUFJLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQTtBQUN4QyxpQkFBUyxDQUFDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLENBQUE7S0FDekMsQ0FBQTs7QUFHRCxVQUFNLENBQUMsTUFBTSxHQUFHLFVBQVUsR0FBRyxFQUFFO0FBQzNCLFdBQUcsQ0FBQyxPQUFPLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQTs7QUFFbEMsMEJBQWtCLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFBO0FBQzVCLGVBQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUE7QUFDaEIsY0FBTSxDQUFDLFlBQVksR0FBRyxHQUFHLENBQUMsVUFBVSxDQUFBO0FBQ3BDLGlCQUFTLENBQUMsSUFBSSxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUE7S0FDakQsQ0FBQTs7QUFFRCxVQUFNLENBQUMsSUFBSSxHQUFHLFVBQVUsR0FBRyxFQUFFO0FBQ3pCLDBCQUFrQixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQTtBQUM1QixpQkFBUyxDQUFDLElBQUksQ0FBQyxhQUFhLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFBO0tBQzlDLENBQUE7Q0FFSixDQUNKLENBQUM7Ozs7Q0FJRCxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsWUFBWSxFQUFFLGNBQWMsRUFBRSxvQkFBb0IsRUFBRSxNQUFNLEVBQUUsYUFBYSxFQUFFLG1CQUFtQixFQUFFLGFBQWEsRUFDbkssVUFBVSxNQUFNLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLGtCQUFrQixFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsaUJBQWlCLEVBQUUsV0FBVyxFQUFFOztBQUVqSSxXQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLGlCQUFpQixDQUFDLENBQUE7O0FBRWhELFVBQU0sQ0FBQyxPQUFPLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQTs7QUFFckMsVUFBTSxDQUFDLEdBQUcsR0FBRyxpQkFBaUIsQ0FBQyxJQUFJLENBQUE7QUFDbkMsUUFBSSxpQkFBaUIsQ0FBQyxPQUFPLEVBQ3pCLE1BQU0sQ0FBQyxPQUFPLEdBQUcsaUJBQWlCLENBQUMsT0FBTyxDQUFBLEtBRTFDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUE7O0FBRXZDLFdBQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQTs7QUFHL0IsVUFBTSxDQUFDLElBQUksR0FBRyxVQUFVLEdBQUcsRUFBRTtBQUN6QixXQUFHLENBQUMsT0FBTyxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUE7QUFDbEMsWUFBSSxHQUFHLENBQUMsVUFBVSxFQUNkLFNBQVMsQ0FBQyxJQUFJLENBQUMscUJBQXFCLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxDQUFBLEtBRTVELFNBQVMsQ0FBQyxJQUFJLENBQUMscUJBQXFCLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxDQUFBO0tBQ25FLENBQUE7O0FBRUQsVUFBTSxDQUFDLE1BQU0sR0FBRyxVQUFVLEdBQUcsRUFBRTtBQUMzQixlQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsQ0FBQTtBQUN2QiwwQkFBa0IsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUE7QUFDNUIsWUFBSSxDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLE1BQU0sRUFBRTtBQUNqRSxtQkFBTyxDQUFDLEdBQUcsQ0FBQyw0QkFBNEIsQ0FBQyxDQUFBO0FBQ3pDLHNCQUFVLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQztBQUMvQyxnQkFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsTUFBTSxFQUFFO0FBQzVDLG9CQUFJLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO0FBQ25CLDZCQUFTLENBQUMsSUFBSSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQTtpQkFDbEUsTUFDRyxTQUFTLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUE7YUFDdkMsQ0FBQyxDQUFBO1NBQ0wsQ0FBQyxDQUFBO0tBQ0wsQ0FBQTtDQUVKLENBQ0osQ0FBQyxDQUVELFVBQVUsQ0FBQyxtQkFBbUIsRUFBRSxDQUFDLFFBQVEsRUFBRSxPQUFPLEVBQUUsV0FBVyxFQUFFLFlBQVksRUFBRSxjQUFjLEVBQUUsUUFBUSxFQUFFLFdBQVcsRUFBRSxvQkFBb0IsRUFBRSxtQkFBbUIsRUFDNUosVUFBVSxNQUFNLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsa0JBQWtCLEVBQUUsaUJBQWlCLEVBQUU7O0FBRXBILFVBQU0sQ0FBQyxHQUFHLEdBQUcsaUJBQWlCLENBQUMsSUFBSSxDQUFBOztBQUVuQyxVQUFNLENBQUMsT0FBTyxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUE7QUFDckMsVUFBTSxDQUFDLEdBQUcsR0FBRyxrQkFBa0IsQ0FBQyxHQUFHLEVBQUUsQ0FBQTs7QUFFckMsV0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsaUJBQWlCLENBQUMsQ0FBQTtBQUN0QyxVQUFNLENBQUMsUUFBUSxHQUFHLGlCQUFpQixDQUFDLFFBQVEsQ0FBQTs7QUFHNUMsVUFBTSxDQUFDLElBQUksR0FBRyxVQUFVLEdBQUcsRUFBRTs7QUFFekIsWUFBSSxPQUFPLENBQUE7O0FBRVgsWUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsWUFBWSxFQUN6QixPQUFPLEdBQUcsd0NBQXdDLENBQUE7QUFDdEQsWUFBSSxDQUFDLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUNoQyxPQUFPLEdBQUcsaUNBQWlDLENBQUE7QUFDL0MsWUFBSSxDQUFDLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUM5QixPQUFPLEdBQUcsNERBQTRELENBQUE7O0FBRTFFLFlBQUksT0FBTyxFQUNQLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FDWixFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsR0FBRyxFQUFFLE9BQU8sRUFBRSxDQUNwQyxDQUFBLEtBQ0E7QUFDRCxtQkFBTyxHQUFHLENBQUMsVUFBVSxDQUFBO0FBQ3JCLDhCQUFrQixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQTtBQUM1QixtQkFBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLENBQUE7QUFDeEIscUJBQVMsQ0FBQyxJQUFJLENBQUMscUJBQXFCLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxDQUFBO1NBQy9EO0tBQ0osQ0FBQTs7QUFFRCxVQUFNLENBQUMsSUFBSSxHQUFHLFVBQVUsR0FBRyxFQUFFO0FBQ3pCLDBCQUFrQixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQTtBQUM1QixlQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFBO0FBQ2xCLFlBQUksT0FBTyxHQUFHLEdBQUcsQ0FBQyxPQUFPLEtBQUssT0FBTyxHQUFHLFVBQVUsR0FBRyxhQUFhLENBQUE7QUFDbEUsaUJBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsQ0FBQTtLQUVqRCxDQUFBOztBQUVELFVBQU0sQ0FBQyxvQkFBb0IsR0FBRyxVQUFVLFVBQVUsRUFBRSxHQUFHLEVBQUU7QUFDckQsZUFBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLEVBQUUsR0FBRyxDQUFDLENBQUE7QUFDaEMsZUFBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsVUFBVSxDQUFDLENBQUE7QUFDckMsY0FBTSxDQUFDLEdBQUcsQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFBO0FBQ2xDLDBCQUFrQixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQTtBQUM1QixlQUFPLENBQUMsR0FBRyxDQUFDLGlCQUFpQixFQUFFLGtCQUFrQixDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUE7QUFDeEQsWUFBSSxPQUFPLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEtBQUssT0FBTyxHQUFHLFVBQVUsR0FBRyxhQUFhLENBQUE7QUFDekUsaUJBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsQ0FBQTtLQUNqRCxDQUFBO0NBRUosQ0FDSixDQUFDLENBRUQsVUFBVSxDQUFDLG1CQUFtQixFQUFFLENBQUMsUUFBUSxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsUUFBUSxFQUFFLFNBQVMsRUFBRSxtQkFBbUIsRUFDdEcsVUFBVSxNQUFNLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLGlCQUFpQixFQUFFOztBQUVwRSxVQUFNLENBQUMsT0FBTyxHQUFHLGlCQUFpQixDQUFDLE9BQU8sQ0FBQTtBQUMxQyxVQUFNLENBQUMsa0JBQWtCLEdBQUcsaUJBQWlCLENBQUMsa0JBQWtCLENBQUE7O0FBRWhFLFdBQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLGlCQUFpQixDQUFDLENBQUE7QUFDdEMsV0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUE7O0FBRXZELFVBQU0sQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDLGFBQWEsRUFBRSxDQUFBO0FBQzVDLFFBQUksaUJBQWlCLENBQUMsV0FBVyxFQUM3QixNQUFNLENBQUMsV0FBVyxHQUFHLGlCQUFpQixDQUFDLFdBQVcsQ0FBQSxLQUVsRCxNQUFNLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQTs7QUFHL0IsUUFBSSxPQUFPLENBQUMsY0FBYyxDQUFDLFdBQVcsRUFBRTtBQUNwQyxjQUFNLENBQUMsT0FBTyxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQTtBQUMzRCxlQUFPLE9BQU8sQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFBO0tBQzVDOztBQUVELFFBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRTs7OztBQUl6QixhQUFLLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLEdBQUcsRUFBRTtBQUM5QyxtQkFBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDLENBQUE7QUFDdkIsZ0JBQUksR0FBRyxDQUFDLE9BQU8sRUFBRTtBQUNiLHNCQUFNLENBQUMsT0FBTyxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUMsT0FBTyxDQUFBO0FBQ3BDLHNCQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUMsTUFBTSxDQUFBO0FBQ2pDLHNCQUFNLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFBO2FBQ2pDO1NBQ0osQ0FBQyxDQUFBO0tBQ0w7O0FBRUQsVUFBTSxDQUFDLEtBQUssR0FBRyxDQUNYLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLEVBQzlCLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLEVBQzdCLEVBQUUsSUFBSSxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLEVBQ2pDLEVBQUUsSUFBSSxFQUFFLGFBQWEsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLEVBQ25DLEVBQUUsSUFBSSxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLEVBQ2pDLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLEVBQ2hDLEVBQUUsSUFBSSxFQUFFLGVBQWUsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLENBQ3hDLENBQUM7O0FBRUYsVUFBTSxDQUFDLFVBQVUsR0FBRyxVQUFVLElBQUksRUFBRTtBQUNoQyxlQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQTtBQUN2QixZQUFJLElBQUksS0FBSyxLQUFLLEVBQ2QsTUFBTSxDQUFDLE9BQU8sQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFBLEtBRTdCLE1BQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFBO0tBQzFCLENBQUE7O0FBRUQsVUFBTSxDQUFDLE1BQU0sR0FBRztBQUNaLGNBQU0sRUFBRSxLQUFLO0tBQ2hCLENBQUM7O0FBRUYsVUFBTSxDQUFDLE9BQU8sR0FBRyxVQUFVLElBQUksRUFBRTtBQUM3QixlQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixFQUFFLElBQUksQ0FBQyxDQUFDO0tBQzFDLENBQUM7O0FBR0YsVUFBTSxDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsRUFBRSxVQUFVLE1BQU0sRUFBRSxNQUFNLEVBQUU7QUFDdkQsZUFBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFBOztBQUV0QyxZQUFJLE1BQU0sS0FBSyxNQUFNLEVBQUU7QUFDbkIsbUJBQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUE7QUFDeEIsbUJBQU07U0FDVDs7QUFFRCxlQUFPLENBQUMsY0FBYyxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUE7QUFDM0MsY0FBTSxDQUFDLE1BQU0sRUFBRSxDQUFBO0tBRWxCLENBQUMsQ0FBQzs7QUFFSCxVQUFNLENBQUMsSUFBSSxHQUFHLFVBQVUsT0FBTyxFQUFFOztBQUU3QixZQUFJLE9BQU8sQ0FBQTs7QUFFWCxZQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFDaEIsT0FBTyxHQUFHLGtDQUFrQyxDQUFBO0FBQ2hELFlBQUksQ0FBQyxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUM1QixPQUFPLEdBQUcsMEVBQTBFLENBQUE7QUFDeEYsWUFBSSxDQUFDLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUU7QUFDM0IsZ0JBQUksRUFBRSxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUE7QUFDeEIsbUJBQU8sQ0FBQyxHQUFHLENBQUMsRUFBRSxFQUFFLEVBQUUsS0FBSyxJQUFJLEVBQUUsRUFBRSxLQUFLLElBQUksRUFBRSxFQUFFLEtBQUssSUFBSSxJQUFJLEVBQUUsS0FBSyxJQUFJLENBQUMsQ0FBQTtBQUNyRSxnQkFBSSxFQUFFLE9BQU8sQ0FBQyxPQUFPLEtBQUssSUFBSSxJQUFJLE9BQU8sQ0FBQyxPQUFPLEtBQUssSUFBSSxDQUFBLEFBQUMsRUFBRTtBQUN6RCx1QkFBTyxHQUFHLDBCQUEwQixDQUFBO2FBQ3ZDO1NBRUo7QUFDRCxZQUFJLENBQUMsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssRUFDMUIsT0FBTyxHQUFHLDJCQUEyQixDQUFBOztBQUV6QyxZQUFJLENBQUMsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssRUFDMUIsT0FBTyxHQUFHLDJCQUEyQixDQUFBOztBQUV6QyxZQUFJLE9BQU8sRUFDUCxNQUFNLENBQUMsTUFBTSxHQUFHLENBQ1osRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLEdBQUcsRUFBRSxPQUFPLEVBQUUsQ0FDcEMsQ0FBQSxLQUNBOztBQUVELGdCQUFJLGlCQUFpQixDQUFDLFNBQVMsRUFBRTtBQUM3QixpQ0FBaUIsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUE7YUFDdkMsTUFDRyxTQUFTLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxDQUFBO1NBQ2xEO0tBRUosQ0FBQTs7QUFFRCxVQUFNLENBQUMsSUFBSSxHQUFHLFVBQVUsT0FBTyxFQUFFO0FBQzdCLGlCQUFTLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxDQUFBO0tBQzlDLENBQUE7Q0FFSixDQUNKLENBQUMsQ0FBQTs7OztBQ3R2Qk4sWUFBWSxDQUFDOzs7O0FBSWIsSUFBSSxTQUFTLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFBO0FBQ3BDLElBQUksU0FBUyxHQUFHLE9BQU8sQ0FBQyx5QkFBeUIsQ0FBQyxDQUFBOztBQUVsRCxTQUFVLDBCQUEwQixDQUFFLFNBQVMsRUFBRTtBQUMvQyxNQUFJLElBQUksR0FBRyxTQUFTLENBQUMsa0JBQWtCLEVBQUUsQ0FBQTs7QUFFekMsU0FBTyxTQUFTLENBQUMseUJBQXlCLENBQUMsU0FBUyxDQUFDLENBQUMsTUFBTSxDQUFDLFVBQVUsS0FBSyxFQUFFO0FBQzVFLFdBQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxTQUFTLENBQUE7R0FDdEMsQ0FBQyxDQUFBO0NBQ0g7O0FBR0QsU0FBUyw0QkFBNEIsQ0FBRSxHQUFHLEVBQUU7QUFDMUMsTUFBSSxJQUFJLEdBQUcsRUFBRSxDQUFBO0FBQ2IsT0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFHLEVBQUU7QUFDcEMsT0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsR0FBRyxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUE7QUFDakUsUUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtHQUNsQjtBQUNELFNBQU8sSUFBSSxDQUFBO0NBQ1o7O0FBRUQsT0FBTyxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLFNBQVMsRUFBRSxDQUFDLFlBQVk7O0FBRTdELFNBQU87O0FBRUwsYUFBUyxFQUFFLFNBQVMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFDcEUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUMxRCxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQzs7QUFFOUQsbUJBQWUsRUFBRSw0QkFBNEIsQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7QUFDeEgsUUFBSSxFQUFFLDBCQUEwQixDQUFDLElBQUksQ0FBQztBQUN0QyxVQUFNLEVBQUUsMEJBQTBCLENBQUMsSUFBSSxDQUFDO0FBQ3hDLGFBQVMsRUFBRSwwQkFBMEIsQ0FBQyxJQUFJLENBQUM7QUFDM0MsYUFBUyxFQUFFLDBCQUEwQixDQUFDLElBQUksQ0FBQztBQUMzQyxhQUFTLEVBQUUsMEJBQTBCLENBQUMsSUFBSSxDQUFDO0FBQzNDLFVBQU0sRUFBRSwwQkFBMEIsQ0FBQyxJQUFJLENBQUM7O0FBRXhDLGVBQVcsRUFBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUM7QUFDNUMsV0FBTyxFQUFFLDBCQUEwQixDQUFDLElBQUksQ0FBQzs7QUFFekMsYUFBUyxFQUFFLENBQ1QsRUFBQyxJQUFJLEVBQUUsYUFBYSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUMsRUFDakMsRUFBQyxJQUFJLEVBQUUsZUFBZSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUMsRUFDbkMsRUFBQyxJQUFJLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUMsQ0FDOUI7O0dBRUYsQ0FBQTtDQUdGLENBQUMsQ0FBQyxDQUFBOzs7QUN0REgsWUFBWSxDQUFDOzs7QUFHYixPQUFPLENBQUMsTUFBTSxDQUFDLHlCQUF5QixFQUFFLEVBQUUsQ0FBQzs7OztDQUl4QyxVQUFVLENBQUMsVUFBVSxFQUFFLFVBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFLOztBQUVoRSxVQUFNLENBQUMsT0FBTyxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUE7QUFDckMsVUFBTSxDQUFDLGFBQWEsR0FBRyxVQUFBLE9BQU8sRUFBSTtBQUM5QixpQkFBUyxDQUFDLElBQUksQ0FBQyxZQUFZLEdBQUcsT0FBTyxDQUFDLENBQUE7S0FDekMsQ0FBQTtDQUVKLENBQUMsQ0FFRCxVQUFVLENBQUMsVUFBVSxFQUFFLFVBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFLOztBQUVoRSxVQUFNLENBQUMsR0FBRyxHQUVWO0FBQ0ksWUFBSSxFQUFFLEdBQUc7QUFDVCxlQUFPLEVBQUUsQ0FBQyxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBQyxFQUFFLEVBQUMsTUFBTSxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFDLEVBQUU7QUFDbEYsa0JBQU0sRUFBRSxJQUFJO0FBQ1oscUJBQVMsRUFBRSxTQUFTO1NBQ3ZCLENBQUM7S0FDTCxDQUFBOztBQUVELFVBQU0sQ0FBQyxPQUFPLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQTtBQUNyQyxVQUFNLENBQUMsYUFBYSxHQUFHLFVBQUEsT0FBTyxFQUFJO0FBQzlCLGlCQUFTLENBQUMsSUFBSSxDQUFDLFlBQVksR0FBRyxPQUFPLENBQUMsQ0FBQTtLQUN6QyxDQUFBO0NBRUosQ0FBQyxDQUFBOzs7OztBQy9CTixZQUFZLENBQUM7O0FBRWIsSUFBSSxpQkFBaUIsR0FBRyxPQUFPLENBQUMscUNBQXFDLENBQUMsQ0FBQTtBQUN0RSxJQUFJLFNBQVMsR0FBRyxPQUFPLENBQUMseUJBQXlCLENBQUMsQ0FBQTs7QUFFbEQsSUFBSSxRQUFRLEdBQUc7QUFDWCxpQkFBYSxFQUFFLENBQUMsV0FBVyxFQUFFLElBQUksRUFBRSxhQUFhLEVBQUUsVUFBVSxTQUFTLEVBQUUsRUFBRSxFQUFFLFdBQVcsRUFBRTtBQUNwRixZQUFJLFFBQVEsR0FBRyxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7O0FBRTFCLFlBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxFQUFFLEVBQUU7QUFDM0Isb0JBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQTtBQUNqQixxQkFBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUM3QixNQUNHLFFBQVEsQ0FBQyxPQUFPLEVBQUUsQ0FBQTs7QUFFdEIsZUFBTyxRQUFRLENBQUMsT0FBTyxDQUFDO0tBQzNCLENBQUM7Q0FDTCxDQUFBOztBQUVELElBQUksZ0JBQWdCLEdBQUcsQ0FBQyxRQUFRLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsaUJBQWlCLENBQUMsZ0JBQWdCLENBQUMsQ0FBQTtBQUNsRyxJQUFJLG9CQUFvQixHQUFHLENBQUMsUUFBUSxFQUFFLE9BQU8sRUFBRSxJQUFJLEVBQUUsWUFBWSxFQUFFLGlCQUFpQixDQUFDLG9CQUFvQixDQUFDLENBQUE7O0FBRTFHLElBQUksYUFBYSxHQUFHO0FBQ2hCLFlBQVEsRUFBRSxDQUFDLFFBQVEsRUFBRSxrQkFBa0IsRUFBRSxVQUFVLE1BQU0sRUFBRSxnQkFBZ0IsRUFBRTs7QUFFekUsZUFBTyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUE7S0FDMUQsQ0FBQztDQUNMLENBQUE7O0FBRUQsSUFBSSxhQUFhLEdBQUc7QUFDaEIscUJBQWlCLEVBQUUsQ0FBQyxRQUFRLEVBQUUsSUFBSSxFQUFFLG9CQUFvQixFQUFFLFVBQVUsTUFBTSxFQUFFLEVBQUUsRUFBRSxrQkFBa0IsRUFBRTtBQUNoRyxlQUFPLGlCQUFpQixDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsRUFBRSxFQUFFLGtCQUFrQixDQUFDLENBQUE7S0FDdkUsQ0FBQztBQUNGLGdCQUFZLEVBQUUsZ0JBQWdCO0NBQ2pDLENBQUE7O0FBRUQsSUFBSSxhQUFhLEdBQUc7QUFDaEIscUJBQWlCLEVBQUUsQ0FBQyxRQUFRLEVBQUUsSUFBSSxFQUFFLG9CQUFvQixFQUFFLE1BQU0sRUFBRSxVQUFVLE1BQU0sRUFBRSxFQUFFLEVBQUUsa0JBQWtCLEVBQUUsSUFBSSxFQUFFO0FBQzlHLGVBQU8saUJBQWlCLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxFQUFFLEVBQUUsa0JBQWtCLEVBQUUsSUFBSSxDQUFDLENBQUE7S0FDN0UsQ0FBQztDQUNMLENBQUE7O0FBR0QsSUFBSSxxQkFBcUIsR0FBRztBQUN4QixxQkFBaUIsRUFBRSxDQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxFQUFFLFVBQVUsTUFBTSxFQUFFLEVBQUUsRUFBRSxrQkFBa0IsRUFBRSxJQUFJLEVBQUU7QUFDOUcsZUFBTyxpQkFBaUIsQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLEVBQUUsRUFBRSxFQUFFLGtCQUFrQixFQUFFLElBQUksQ0FBQyxDQUFBO0tBQ3JGLENBQUM7Q0FDTCxDQUFBOztBQUVELElBQUkscUJBQXFCLEdBQUc7QUFDeEIscUJBQWlCLEVBQUUsQ0FBQyxRQUFRLEVBQUUsSUFBSSxFQUFFLG9CQUFvQixFQUFFLE1BQU0sRUFBRSxVQUFVLE1BQU0sRUFBRSxFQUFFLEVBQUUsa0JBQWtCLEVBQUUsSUFBSSxFQUFFO0FBQzlHLGVBQU8saUJBQWlCLENBQUMsbUJBQW1CLENBQUMsTUFBTSxFQUFFLEVBQUUsRUFBRSxrQkFBa0IsRUFBRSxJQUFJLENBQUMsQ0FBQTtLQUNyRixDQUFDO0NBQ0wsQ0FBQTs7QUFHRCxJQUFJLG1CQUFtQixHQUFHO0FBQ3RCLHFCQUFpQixFQUFFLENBQUMsUUFBUSxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsY0FBYyxFQUFFLFVBQVUsTUFBTSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFO0FBQzVHLGVBQU8saUJBQWlCLENBQUMsaUJBQWlCLENBQUMsTUFBTSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsWUFBWSxDQUFDLENBQUE7S0FDbEYsQ0FBQztDQUNMLENBQUE7O0FBR0QsSUFBSSxtQkFBbUIsR0FBRztBQUN0QixxQkFBaUIsRUFBRSxDQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFLGNBQWMsRUFBRSxVQUFVLE1BQU0sRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRTtBQUM1RyxlQUFPLGlCQUFpQixDQUFDLGlCQUFpQixDQUFDLE1BQU0sRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLFlBQVksQ0FBQyxDQUFBO0tBQ2xGLENBQUM7Q0FDTCxDQUFBOztBQUdELElBQUksV0FBVyxHQUFHO0FBQ2QscUJBQWlCLEVBQUUsQ0FBQyxRQUFRLEVBQUUsSUFBSSxFQUFFLFdBQVcsRUFBRSxjQUFjLEVBQUUsVUFBVSxNQUFNLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUU7QUFDNUcsZUFBTyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsWUFBWSxDQUFDLENBQUE7S0FDMUUsQ0FBQztDQUNMLENBQUE7O0FBRUQsSUFBSSxtQkFBbUIsR0FBRztBQUN0QixnQkFBWSxFQUFFLENBQUMsTUFBTSxFQUFFLFVBQVUsSUFBSSxFQUFFO0FBQ25DLGVBQU8sT0FBTyxDQUFDLDZCQUE2QixDQUFDLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDLENBQUE7S0FDaEYsQ0FBQztDQUNMLENBQUE7O0FBRUQsSUFBSSxZQUFZLEdBQUc7QUFDZixZQUFRLEVBQUUsQ0FBQyxJQUFJLEVBQUUsUUFBUSxFQUFHLFdBQVcsRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLFVBQVUsRUFBRSxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsT0FBTyxFQUFFLFFBQVEsRUFBRTtBQUNoSCxlQUFPLE9BQU8sQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxFQUFFLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxPQUFPLEVBQUUsUUFBUSxDQUFDLENBQUE7S0FDaEcsQ0FBQzs7Q0FFTCxDQUFBOztBQUVELElBQUksYUFBYSxHQUFHO0FBQ2hCLFlBQVEsRUFBRSxDQUFDLElBQUksRUFBRSxRQUFRLEVBQUUsV0FBVyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxPQUFPLEVBQUUsUUFBUSxFQUFFO0FBQy9HLGVBQU8sT0FBTyxDQUFDLHdCQUF3QixDQUFDLENBQUMsV0FBVyxDQUFDLEVBQUUsRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLE9BQU8sRUFBRSxRQUFRLENBQUMsQ0FBQTtLQUNqRyxDQUFDO0NBQ0wsQ0FBQTs7QUFHRCxNQUFNLENBQUMsT0FBTyxHQUFHLENBQUMsZ0JBQWdCLEVBQUUsbUJBQW1CLEVBQUUsVUFBVSxjQUFjLEVBQUUsaUJBQWlCLEVBQUU7QUFDbEcsa0JBQWMsQ0FDVixJQUFJLENBQUMsR0FBRyxFQUFFLEVBQUMsV0FBVyxFQUFFLG9CQUFvQixFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUMsQ0FBQyxDQUN0RSxJQUFJLENBQUMsUUFBUSxFQUFFLEVBQUMsV0FBVyxFQUFFLHFCQUFxQixFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUMsQ0FBQyxDQUM1RSxJQUFJLENBQUMsU0FBUyxFQUFFLEVBQUMsV0FBVyxFQUFFLHNCQUFzQixFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUMsQ0FBQyxDQUNoRixJQUFJLENBQUMsVUFBVSxFQUFFLEVBQUMsV0FBVyxFQUFFLHVCQUF1QixFQUFFLFVBQVUsRUFBRSxhQUFhLEVBQUMsQ0FBQyxDQUNuRixJQUFJLENBQUMsWUFBWSxFQUFFLEVBQUMsV0FBVyxFQUFFLG1CQUFtQixFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUMsQ0FBQyxDQUU3RSxJQUFJLENBQUMsZUFBZSxFQUFFLEVBQUMsV0FBVyxFQUFFLDRCQUE0QixFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUMsQ0FBQyxDQUM1RixJQUFJLENBQUMsWUFBWSxFQUFFLEVBQUMsV0FBVyxFQUFFLCtCQUErQixFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUMsQ0FBQyxDQUU1RixJQUFJLENBQUMscUJBQXFCLEVBQUU7QUFDeEIsbUJBQVcsRUFBRSwrQkFBK0IsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLE9BQU8sRUFBRTtBQUM1RSx3QkFBWSxFQUFFLGdCQUFnQjtTQUNqQztLQUNKLENBQUM7Ozs7O0FBS0YsUUFBSSxDQUFDLHFCQUFxQixFQUFFO0FBQ3hCLG1CQUFXLEVBQUUsK0JBQStCLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFBRSxPQUFPLEVBQUU7QUFDNUUsd0JBQVksRUFBRSxnQkFBZ0I7U0FDakM7S0FDSixDQUFDLENBQ0YsSUFBSSxDQUFDLHFCQUFxQixFQUFFO0FBQ3hCLG1CQUFXLEVBQUUsK0JBQStCLEVBQUUsVUFBVSxFQUFFLFdBQVc7QUFDckUsZUFBTyxFQUFFLGFBQWE7S0FDekIsQ0FBQyxDQUNGLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxFQUFDLFdBQVcsRUFBRSwrQkFBK0IsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFDLENBQUMsQ0FDcEcsSUFBSSxDQUFDLHFCQUFxQixFQUFFLEVBQUMsV0FBVyxFQUFFLCtCQUErQixFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQUMsQ0FBQyxDQUVwRyxJQUFJLENBQUMscUJBQXFCLEVBQUU7QUFDeEIsbUJBQVcsRUFBRSwrQkFBK0IsRUFBRSxVQUFVLEVBQUUsV0FBVztBQUNyRSxlQUFPLEVBQUUsYUFBYTtLQUN6QixDQUFDLENBRUYsSUFBSSxDQUFDLDZCQUE2QixFQUFFO0FBQ2hDLG1CQUFXLEVBQUUsdUNBQXVDO0FBQ3BELGtCQUFVLEVBQUUsbUJBQW1CO0FBQy9CLGVBQU8sRUFBRSxxQkFBcUI7S0FDakMsQ0FBQyxDQUNGLElBQUksQ0FBQyw2QkFBNkIsRUFBRTtBQUNoQyxtQkFBVyxFQUFFLHVDQUF1QztBQUNwRCxrQkFBVSxFQUFFLG1CQUFtQjtBQUMvQixlQUFPLEVBQUUscUJBQXFCO0tBQ2pDLENBQUM7OztBQUdGLFFBQUksQ0FBQyxrQkFBa0IsRUFBRSxFQUFDLFdBQVcsRUFBRSw0QkFBNEIsRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFDLENBQUMsQ0FDM0YsSUFBSSxDQUFDLGtCQUFrQixFQUFFO0FBQ3JCLG1CQUFXLEVBQUUsNEJBQTRCO0FBQ3pDLGtCQUFVLEVBQUUsV0FBVztBQUN2QixlQUFPLEVBQUUsYUFBYTtLQUN6QixDQUFDLENBQ0YsSUFBSSxDQUFDLGtCQUFrQixFQUFFO0FBQ3JCLG1CQUFXLEVBQUUsNEJBQTRCO0FBQ3pDLGtCQUFVLEVBQUUsUUFBUTtBQUNwQixlQUFPLEVBQUUsYUFBYTtLQUN6QixDQUFDLENBQ0YsSUFBSSxDQUFDLG9CQUFvQixFQUFFLEVBQUMsV0FBVyxFQUFFLDhCQUE4QixFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUMsQ0FBQyxDQUNqRyxJQUFJLENBQUMsb0JBQW9CLEVBQUU7QUFDdkIsbUJBQVcsRUFBRSw4QkFBOEI7QUFDM0Msa0JBQVUsRUFBRSxVQUFVO0FBQ3RCLGVBQU8sRUFBRSxhQUFhO0tBQ3pCLENBQUMsQ0FFRixJQUFJLENBQUMsbUJBQW1CLEVBQUUsRUFBQyxXQUFXLEVBQUUsc0JBQXNCLEVBQUUsVUFBVSxFQUFFLG1CQUFtQixFQUFDLENBQUMsQ0FFakcsSUFBSSxDQUFDLGdCQUFnQixFQUFFLEVBQUMsV0FBVyxFQUFFLDZCQUE2QixFQUFFLFVBQVUsRUFBRSxnQkFBZ0IsRUFBQyxDQUFDLENBQ2xHLElBQUksQ0FBQyxrQ0FBa0MsRUFBRTtBQUNyQyxtQkFBVyxFQUFFLG9DQUFvQztBQUNqRCxrQkFBVSxFQUFFLHNCQUFzQjtLQUNyQyxDQUFDOzs7QUFHRixRQUFJLENBQUMsZUFBZSxFQUFFLEVBQUMsV0FBVyxFQUFFLDRCQUE0QixFQUFFLFVBQVUsRUFBRSxjQUFjLEVBQUMsQ0FBQzs7O0FBSTlGLFFBQUksQ0FBQyw0QkFBNEIsRUFBRTtBQUMvQixtQkFBVyxFQUFFLGdDQUFnQztBQUM3QyxrQkFBVSxFQUFFLGtCQUFrQjtBQUM5QixlQUFPLEVBQUUsbUJBQW1CO0tBQy9CLENBQUMsQ0FDRixJQUFJLENBQUMsdUJBQXVCLEVBQUU7QUFDMUIsbUJBQVcsRUFBRSwyQ0FBMkM7QUFDeEQsa0JBQVUsRUFBRSw2QkFBNkI7S0FDNUMsQ0FBQyxDQUVGLElBQUksQ0FBQyw0QkFBNEIsRUFBRTtBQUMvQixtQkFBVyxFQUFFLGdDQUFnQztBQUM3QyxrQkFBVSxFQUFFLDZCQUE2QjtBQUN6QyxjQUFNLEVBQUUsSUFBSTtBQUNaLGVBQU8sRUFBRSxRQUFRO0tBQ3BCLENBQUMsQ0FDRixJQUFJLENBQUMsb0JBQW9CLEVBQUUsRUFBQyxXQUFXLEVBQUUsNEJBQTRCLEVBQUUsVUFBVSxFQUFFLHlCQUF5QixFQUFDLENBQUMsQ0FDOUcsSUFBSSxDQUFDLHFCQUFxQixFQUFFO0FBQ3hCLG1CQUFXLEVBQUUsNkJBQTZCO0FBQzFDLGtCQUFVLEVBQUUsMEJBQTBCO0tBQ3pDLENBQUMsQ0FFRixJQUFJLENBQUMsU0FBUyxFQUFFO0FBQ1osbUJBQVcsRUFBRSxzQkFBc0I7QUFDbkMsa0JBQVUsRUFBRSxrQkFBa0I7QUFDOUIsY0FBTSxFQUFFLElBQUk7QUFDWixlQUFPLEVBQUUsUUFBUTtBQUNqQixzQkFBYyxFQUFFLEtBQUs7S0FDeEIsQ0FBQyxDQUNGLElBQUksQ0FBQyxZQUFZLEVBQUU7QUFDZixtQkFBVyxFQUFFLHFCQUFxQjtBQUNsQyxrQkFBVSxFQUFFLHVCQUF1QjtBQUNuQyxjQUFNLEVBQUUsSUFBSTtBQUNaLGVBQU8sRUFBRSxRQUFRO0FBQ2pCLHNCQUFjLEVBQUUsS0FBSztLQUN4QixDQUFDLENBQ0YsSUFBSSxDQUFDLGdCQUFnQixFQUFFO0FBQ25CLG1CQUFXLEVBQUUsNkJBQTZCLEVBQUUsVUFBVSxFQUFFLHlCQUF5QjtBQUNqRixzQkFBYyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxRQUFRO0tBQ3pELENBQUMsQ0FDRixJQUFJLENBQUMsbUJBQW1CLEVBQUU7QUFDdEIsbUJBQVcsRUFBRSw0QkFBNEI7QUFDekMsa0JBQVUsRUFBRSw4QkFBOEI7QUFDMUMsY0FBTSxFQUFFLElBQUk7QUFDWixlQUFPLEVBQUUsUUFBUTtLQUNwQixDQUFDLENBRUYsSUFBSSxDQUFDLFNBQVMsRUFBRSxFQUFDLFdBQVcsRUFBRSxzQkFBc0IsRUFBRSxVQUFVLEVBQUUsZ0JBQWdCLEVBQUMsQ0FBQyxDQUNwRixJQUFJLENBQUMsU0FBUyxFQUFFLEVBQUMsV0FBVyxFQUFFLHNCQUFzQixFQUFFLFVBQVUsRUFBRSxnQkFBZ0IsRUFBQyxDQUFDLENBQ3BGLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBQyxXQUFXLEVBQUUsdUJBQXVCLEVBQUUsVUFBVSxFQUFFLGdCQUFnQixFQUFDLENBQUMsQ0FDdEYsSUFBSSxDQUFDLGtCQUFrQixFQUFFLEVBQUMsV0FBVyxFQUFFLDZCQUE2QixFQUFFLFVBQVUsRUFBRSxnQkFBZ0IsRUFBQyxDQUFDLENBQ3BHLElBQUksQ0FBQyxXQUFXLEVBQUU7QUFDZCxtQkFBVyxFQUFFLHFCQUFxQixFQUFFLFVBQVUsRUFBRSxhQUFhO0FBQzdELGVBQU8sRUFBRSxZQUFZOztLQUV4QixDQUFDOzs7Ozs7QUFPRixRQUFJLENBQUMsWUFBWSxFQUFFO0FBQ2YsbUJBQVcsRUFBRSx5QkFBeUI7QUFDdEMsa0JBQVUsRUFBRSxlQUFlO0FBQzNCLGNBQU0sRUFBRSxJQUFJO0FBQ1osZUFBTyxFQUFFLFFBQVE7S0FDcEIsQ0FBQyxDQUVGLElBQUksQ0FBQyxVQUFVLEVBQUU7QUFDYixtQkFBVyxFQUFFLHVCQUF1QjtBQUNwQyxrQkFBVSxFQUFFLG1CQUFtQjtBQUMvQixjQUFNLEVBQUUsSUFBSTtBQUNaLGVBQU8sRUFBRSxRQUFRO0tBQ3BCLENBQUMsQ0FDRixJQUFJLENBQUMsMkJBQTJCLEVBQUU7QUFDOUIsbUJBQVcsRUFBRSx1Q0FBdUMsRUFBRSxVQUFVLEVBQUUsbUJBQW1CO0FBQ3JGLGVBQU8sRUFBRSxtQkFBbUI7S0FDL0IsQ0FBQyxDQUVGLElBQUksQ0FBQyx5QkFBeUIsRUFBRTtBQUM1QixtQkFBVyxFQUFFLHVCQUF1QjtBQUNwQyxrQkFBVSxFQUFFLG1CQUFtQjtBQUMvQixlQUFPLEVBQUUsV0FBVztLQUN2QixDQUFDLENBQ0YsSUFBSSxDQUFDLHNCQUFzQixFQUFFO0FBQ3pCLG1CQUFXLEVBQUUsdUNBQXVDLEVBQUUsVUFBVSxFQUFFLG1CQUFtQjtBQUNyRixlQUFPLEVBQUUsbUJBQW1CO0tBQy9CLENBQUMsQ0FDRixJQUFJLENBQUMsV0FBVyxFQUFFO0FBQ2QsbUJBQVcsRUFBRSx3QkFBd0I7QUFDckMsa0JBQVUsRUFBRSx1QkFBdUI7QUFDbkMsY0FBTSxFQUFFLElBQUk7QUFDWixlQUFPLEVBQUUsUUFBUTtLQUNwQixDQUFDOzs7QUFHRixRQUFJLENBQUMsUUFBUSxFQUFFLEVBQUMsV0FBVyxFQUFFLHFCQUFxQixFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUMsQ0FBQyxDQUM1RSxJQUFJLENBQUMsWUFBWSxFQUFFLEVBQUMsV0FBVyxFQUFFLDhCQUE4QixFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUMsQ0FBQyxDQUN6RixJQUFJLENBQUMsa0JBQWtCLEVBQUU7QUFDckIsbUJBQVcsRUFBRSx5QkFBeUI7QUFDdEMsa0JBQVUsRUFBRSxzQkFBc0I7QUFDbEMsZUFBTyxFQUFFLGFBQWE7S0FDekIsQ0FBQyxDQUNGLElBQUksQ0FBQyxRQUFRLEVBQUUsRUFBQyxXQUFXLEVBQUUscUJBQXFCLEVBQUUsVUFBVSxFQUFFLFVBQVUsRUFBQyxDQUFDLENBQzVFLElBQUksQ0FBQyxXQUFXLEVBQUUsRUFBQyxXQUFXLEVBQUUsd0JBQXdCLEVBQUUsVUFBVSxFQUFFLFVBQVUsRUFBQyxDQUFDLENBQ2xGLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBQyxXQUFXLEVBQUUsdUJBQXVCLEVBQUUsVUFBVSxFQUFFLFVBQVUsRUFBQyxDQUFDLENBQ2hGLElBQUksQ0FBQyxZQUFZLEVBQUUsRUFBQyxXQUFXLEVBQUUseUJBQXlCLEVBQUUsVUFBVSxFQUFFLFVBQVUsRUFBQyxDQUFDLENBQ3BGLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBQyxXQUFXLEVBQUUsdUJBQXVCLEVBQUUsVUFBVSxFQUFFLFVBQVUsRUFBQyxDQUFDLENBQ2hGLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxFQUFDLFdBQVcsRUFBRSxnQ0FBZ0MsRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFDLENBQUMsQ0FDbEcsSUFBSSxDQUFDLFVBQVUsRUFBRSxFQUFDLFdBQVcsRUFBRSx1QkFBdUIsRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFDLENBQUMsQ0FDaEYsSUFBSSxDQUFDLFVBQVUsRUFBRSxFQUFDLFdBQVcsRUFBRSx1QkFBdUIsRUFBRSxVQUFVLEVBQUUsYUFBYSxFQUFDLENBQUMsQ0FDbkYsSUFBSSxDQUFDLFVBQVUsRUFBRSxFQUFDLFdBQVcsRUFBRSx1QkFBdUIsRUFBRSxVQUFVLEVBQUUsYUFBYSxFQUFDLENBQUMsQ0FDbkYsSUFBSSxDQUFDLGlCQUFpQixFQUFFLEVBQUMsV0FBVyxFQUFFLDhCQUE4QixFQUFFLFVBQVUsRUFBRSwwQkFBMEIsRUFBQyxDQUFDLENBQzlHLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxFQUFDLFdBQVcsRUFBRSxrQ0FBa0MsRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFDLENBQUMsQ0FDeEcsSUFBSSxDQUFDLE1BQU0sRUFBRSxFQUFDLFdBQVcsRUFBRSxtQkFBbUIsRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFDLENBQUMsQ0FBQTs7OztBQUkzRSxRQUFJLFVBQVUsR0FBRyxTQUFTLENBQUMsa0JBQWtCLEVBQUUsQ0FBQTs7QUFFL0MsV0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsVUFBVSxDQUFDLENBQUE7QUFDckMsU0FBSyxJQUFJLElBQUksSUFBSSxVQUFVLEVBQUU7QUFDekIsWUFBSSxJQUFJLEVBQUU7QUFDTixnQkFBSSxLQUFLLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLEdBQUcsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFBO0FBQ2xFLDBCQUFjLENBQUMsSUFBSSxDQUFDLDZCQUE2QixHQUFHLEtBQUssRUFBRTtBQUN2RCwyQkFBVyxFQUFFLCtCQUErQixFQUFFLFVBQVUsRUFBRSxjQUFjLEVBQUUsT0FBTyxFQUFFO0FBQy9FLGdDQUFZLEVBQUUsb0JBQW9CO2lCQUNyQzthQUNKLENBQUMsQ0FBQTtTQUNMO0tBQ0o7Ozs7Ozs7O0FBUUQsa0JBQWMsQ0FBQyxTQUFTLENBQUMsRUFBQyxVQUFVLEVBQUUsR0FBRyxFQUFDLENBQUMsQ0FBQzs7QUFFNUMscUJBQWlCLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFBOzs7Q0FJcEMsQ0FBQyxDQUFBOzs7QUNuVUYsWUFBWSxDQUFDOzs7QUFHYixJQUFJLGNBQWMsR0FBRyxPQUFPLENBQUMsc0JBQXNCLENBQUMsQ0FBQTtBQUNwRCxJQUFJLFVBQVUsR0FBRyxPQUFPLENBQUMscUJBQXFCLENBQUMsQ0FBQTtBQUMvQyxJQUFJLFdBQVcsR0FBRyxPQUFPLENBQUMsNEJBQTRCLENBQUMsQ0FBQTtBQUN2RCxJQUFJLFFBQVEsR0FBRyxPQUFPLENBQUMscUJBQXFCLENBQUMsQ0FBQTtBQUM3QyxJQUFJLFVBQVUsR0FBRyxPQUFPLENBQUMsa0JBQWtCLENBQUMsQ0FBQTs7QUFFNUMsSUFBSSxRQUFRLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxjQUFjLEVBQUUsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUMsQ0FBQzs7QUFFMUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxvQkFBb0IsRUFBRSxDQUFDLE9BQU8sRUFBRSxJQUFJLEVBQUUsV0FBVyxDQUFDLGtCQUFrQixDQUFFLENBQUMsQ0FBQTs7QUFFeEYsUUFBUSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxPQUFPLEVBQUUsSUFBSSxFQUFFLGFBQWEsRUFBRSxZQUFZLEVBQUUsVUFBVSxDQUFDLElBQUksQ0FBRSxDQUFDLENBRXJGLE9BQU8sQ0FBQyxZQUFZLEVBQUUsQ0FBQyxTQUFTLEVBQUcsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBRTFELE9BQU8sQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLE9BQU8sRUFBRSxJQUFJLEVBQUUsY0FBYyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBRXpFLE9BQU8sQ0FBQyxXQUFXLEVBQUUsQ0FBQyxPQUFPLEVBQUUsSUFBSSxFQUFFLGNBQWMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUUvRCxPQUFPLENBQUMsV0FBVyxFQUFFLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQ2hELE9BQU8sQ0FBQyxVQUFVLEVBQUUsQ0FBQyxPQUFPLEVBQUUsSUFBSSxFQUFFLG9CQUFvQixFQUFFLFlBQVksRUFBRSxjQUFjLEVBQUUsYUFBYSxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUUxSCxPQUFPLENBQUMsZUFBZSxFQUFFLENBQUUsV0FBVyxFQUFFLFVBQVUsU0FBUyxFQUFFO0FBQzVELFNBQU8sU0FBUyxDQUFDLG9CQUFvQixDQUFDLENBQUE7Q0FDdkMsQ0FBQyxDQUFDLENBRUYsT0FBTyxDQUFDLFFBQVEsRUFBRSxDQUFDLFdBQVcsRUFBRSxVQUFVLFNBQVMsRUFBRTtBQUNwRCxTQUFPLFNBQVMsQ0FBQyxhQUFhLENBQUMsQ0FBQTtDQUNoQyxDQUFDLENBQUMsQ0FFRixPQUFPLENBQUMsYUFBYSxFQUFFLENBQUMsT0FBTyxFQUFFLElBQUksRUFBRSxjQUFjLENBQUMsV0FBVyxDQUFFLENBQUMsQ0FDcEUsT0FBTyxDQUFDLGNBQWMsRUFBRSxDQUFDLE9BQU8sRUFBRSxJQUFJLEVBQUUsY0FBYyxDQUFDLFlBQVksQ0FBRSxDQUFDLENBQ3RFLE9BQU8sQ0FBQyxjQUFjLEVBQUUsQ0FBQyxPQUFPLEVBQUUsSUFBSSxFQUFFLGNBQWMsQ0FBQyxZQUFZLENBQUUsQ0FBQyxDQUN0RSxPQUFPLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxPQUFPLEVBQUUsSUFBSSxFQUFFLGNBQWMsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLENBQ25GLE9BQU8sQ0FBQyxhQUFhLEVBQUUsQ0FBQyxPQUFPLEVBQUUsSUFBSSxFQUFFLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUE7OztBQ3BDM0UsWUFBWSxDQUFDOzs7QUFHYixPQUFPLENBQUMsSUFBSSxHQUFHLFVBQVUsS0FBSyxFQUFFLEVBQUUsRUFBRSxXQUFXLEVBQUUsVUFBVSxFQUFFOztBQUUzRCxNQUFJLENBQUMsYUFBYSxHQUFHLFlBQVk7QUFDL0IsV0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFBO0FBQzlCLFFBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQzs7QUFFMUIsUUFBSSxXQUFXLENBQUMsVUFBVSxFQUFFLEVBQUU7O0FBRTVCLFdBQUssQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsSUFBSSxFQUFFO0FBQ2pELGVBQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQTtBQUNuQyxnQkFBUSxDQUFDLE9BQU8sQ0FBQyxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksRUFBQyxDQUFDLENBQUM7T0FDekUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFVLEtBQUssRUFBRTtBQUN4QixnQkFBUSxDQUFDLE9BQU8sQ0FBQyxFQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLGlFQUFpRSxFQUFDLENBQUMsQ0FBQztPQUMvRyxDQUFDLENBQUM7S0FDSixNQUNDLFFBQVEsQ0FBQyxPQUFPLENBQUMsRUFBQyxNQUFNLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsSUFBSSxFQUFFLFVBQVUsQ0FBQyxPQUFPLEVBQUUsRUFBQyxDQUFDLENBQUM7O0FBRXJGLFdBQU8sUUFBUSxDQUFDLE9BQU8sQ0FBQztHQUN6QixDQUFBOztBQUdELE1BQUksQ0FBQyxTQUFTLEdBQUcsVUFBVSxPQUFPLEVBQUU7O0FBRWxDLFFBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQzs7QUFFMUIsU0FBSyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxPQUFPLEVBQUUsRUFBQyxPQUFPLEVBQUUsUUFBUSxFQUFDLENBQUMsQ0FDekQsT0FBTyxDQUFDLFVBQVUsSUFBSSxFQUFFO0FBQ3RCLGFBQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUE7QUFDakIsY0FBUSxDQUFDLE9BQU8sQ0FBQyxFQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFFLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFDLENBQUMsQ0FBQztLQUMzRixDQUFDLENBQ0YsS0FBSyxDQUFDLFVBQVUsS0FBSyxFQUFFO0FBQ3JCLGNBQVEsQ0FBQyxPQUFPLENBQUMsRUFBQyxNQUFNLEVBQUUsUUFBUSxFQUFFLE9BQU8sRUFBRSx3REFBd0QsRUFBQyxDQUFDLENBQUM7S0FDekcsQ0FBQyxDQUFDOztBQUVMLFdBQU8sUUFBUSxDQUFDLE9BQU8sQ0FBQztHQUN6QixDQUFBOztBQUVELE1BQUksQ0FBQyxrQkFBa0IsR0FBRyxZQUFZO0FBQ3BDLFFBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQzs7QUFFMUIsV0FBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFBO0FBQ2xDLFFBQUksV0FBVyxDQUFDLFVBQVUsRUFBRSxFQUFFO0FBQzVCLFdBQUssQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxJQUFJLEVBQUU7QUFDdkQsZ0JBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO09BQ25DLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBVSxLQUFLLEVBQUU7QUFDeEIsZ0JBQVEsQ0FBQyxPQUFPLENBQUMsRUFBQyxNQUFNLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxpRUFBaUUsRUFBQyxDQUFDLENBQUM7T0FDL0csQ0FBQyxDQUFDO0tBQ0osTUFBTTs7QUFFTCxjQUFRLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFBO0tBQ3hDOztBQUVELFdBQU8sUUFBUSxDQUFDLE9BQU8sQ0FBQztHQUN6QixDQUFBOztBQUdELE1BQUksQ0FBQyxXQUFXLEdBQUcsWUFBWTtBQUM3QixRQUFJLFFBQVEsR0FBRyxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7QUFDMUIsUUFBSSxXQUFXLENBQUMsVUFBVSxFQUFFLEVBQUU7QUFDNUIsV0FBSyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLFFBQVEsRUFBRTtBQUN6RCxnQkFBUSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztPQUM1QixDQUFDLENBQUE7S0FDSCxNQUFNO0FBQ0wsY0FBUSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQTtLQUMzQztBQUNELFdBQU8sUUFBUSxDQUFDLE9BQU8sQ0FBQztHQUN6QixDQUFBOztBQUVELE1BQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxVQUFVLEVBQUU7QUFDdEMsUUFBSSxRQUFRLEdBQUcsRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDO0FBQzFCLFFBQUksVUFBVSxFQUFFO0FBQ2QsVUFBSSxXQUFXLENBQUMsVUFBVSxFQUFFLEVBQUU7QUFDNUIsYUFBSyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsR0FBRyxVQUFVLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxPQUFPLEVBQUU7QUFDckUsa0JBQVEsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUE7U0FDMUIsQ0FBQyxDQUFBO09BQ0gsTUFBTTtBQUNMLGdCQUFRLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQTtPQUNwRDtLQUNGLE1BQU07QUFDTCxjQUFRLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFBO0tBQzVCO0FBQ0QsV0FBTyxRQUFRLENBQUMsT0FBTyxDQUFDO0dBQ3pCLENBQUE7Ozs7OztBQU1ELE1BQUksQ0FBQyxpQkFBaUIsR0FBRyxZQUFZOztBQUVuQyxRQUFJLFFBQVEsR0FBRyxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7OztBQUcxQixRQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsRUFBRSxFQUFFO0FBQzdCLFVBQUksUUFBUSxHQUFHLFVBQVUsQ0FBQyxjQUFjLEVBQUUsQ0FBQTtBQUMxQyxVQUFJLE1BQU0sR0FBRyxFQUFFLENBQUE7O0FBRWYsV0FBSyxJQUFJLEdBQUcsSUFBSSxRQUFRLEVBQUU7QUFDeEIsWUFBSSxRQUFRLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxFQUFFO0FBQ2hDLGlCQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQTtBQUNoQyxnQkFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUE7U0FDakM7T0FDRjs7QUFFRCxXQUFLLENBQUMsSUFBSSxDQUFDLG9CQUFvQixFQUFFLE1BQU0sQ0FBQyxDQUN0QyxPQUFPLENBQUMsVUFBVSxJQUFJLEVBQUU7QUFDdEIsZ0JBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7T0FDeEIsQ0FBQyxDQUNGLEtBQUssQ0FBQyxVQUFVLEtBQUssRUFBRTtBQUNyQixnQkFBUSxDQUFDLE9BQU8sQ0FBQyxFQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLHlEQUF5RCxFQUFDLENBQUMsQ0FBQztPQUN2RyxDQUFDLENBQUM7S0FDTixNQUNDLFFBQVEsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUE7O0FBRXRCLFdBQU8sUUFBUSxDQUFDLE9BQU8sQ0FBQztHQUV6QixDQUFBOzs7QUFHRCxNQUFJLENBQUMsc0JBQXNCLEdBQUcsWUFBVzs7QUFFdkMsUUFBSSxRQUFRLEdBQUcsVUFBVSxDQUFDLGNBQWMsRUFBRSxDQUFBO0FBQzFDLFNBQUssSUFBSSxHQUFHLElBQUksUUFBUSxFQUFFO0FBQ3hCLFVBQUksUUFBUSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsRUFBRTtBQUNoQyxlQUFPLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQTtPQUNyQjtLQUNGO0dBQ0YsQ0FBQTs7OztBQUlELE1BQUksQ0FBQyxXQUFXLEdBQUcsVUFBVSxPQUFPLEVBQUUsSUFBSSxFQUFFOztBQUUxQyxRQUFJLFFBQVEsR0FBRyxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7QUFDMUIsUUFBSSxXQUFXLENBQUMsVUFBVSxFQUFFLEVBQUU7QUFDNUIsV0FBSyxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsR0FBRyxPQUFPLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxJQUFJLEVBQUU7QUFDckYsZ0JBQVEsQ0FBQyxPQUFPLENBQUMsRUFBQyxNQUFNLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQztPQUNsQyxDQUFDLENBQUE7S0FDSCxNQUFNO0FBQ0wsZ0JBQVUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUE7QUFDNUIsY0FBUSxDQUFDLE9BQU8sQ0FBQyxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUMsQ0FBQyxDQUFDO0tBQ2xDO0FBQ0QsV0FBTyxRQUFRLENBQUMsT0FBTyxDQUFDO0dBQ3pCLENBQUE7Ozs7QUFLRCxNQUFJLENBQUMsZ0JBQWdCLEdBQUcsVUFBVSxPQUFPLEVBQUUsSUFBSSxFQUFFO0FBQy9DLFdBQU8sQ0FBQyxHQUFHLENBQUMsYUFBYSxFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQTtBQUN6QyxRQUFJLFFBQVEsR0FBRyxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7QUFDMUIsUUFBSSxPQUFPLEtBQUssT0FBTyxFQUFFO0FBQ3ZCLGdCQUFVLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFBO0FBQzVCLGNBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUE7S0FDdkIsTUFBTSxJQUFJLFdBQVcsQ0FBQyxVQUFVLEVBQUUsRUFBRTtBQUNuQyxXQUFLLENBQUMsSUFBSSxDQUFDLHdCQUF3QixHQUFHLE9BQU8sR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxJQUFJLEVBQUU7QUFDNUYsZ0JBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUE7T0FDdkIsQ0FBQyxDQUFBO0tBQ0g7QUFDRCxXQUFPLFFBQVEsQ0FBQyxPQUFPLENBQUM7R0FDekIsQ0FBQTs7QUFFRCxNQUFJLENBQUMsY0FBYyxHQUFHLFVBQVUsSUFBSSxFQUFFOztBQUVwQyxRQUFJLFFBQVEsR0FBRyxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7QUFDMUIsUUFBSSxXQUFXLENBQUMsVUFBVSxFQUFFLEVBQUU7QUFDNUIsYUFBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUE7QUFDOUIsV0FBSyxDQUFDLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxJQUFJLENBQUMsQ0FDckMsT0FBTyxDQUFDLFVBQVUsSUFBSSxFQUFFO0FBQ3BCLGVBQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUE7QUFDbkIsZ0JBQVEsQ0FBQyxPQUFPLENBQUMsRUFBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU8sRUFBQyxDQUFDLENBQUM7T0FDcEUsQ0FBQyxDQUNGLEtBQUssQ0FBQyxVQUFVLEtBQUssRUFBRTtBQUNyQixnQkFBUSxDQUFDLE9BQU8sQ0FBQyxFQUFDLFVBQVUsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLHdEQUF3RCxFQUFDLENBQUMsQ0FBQztPQUMxRyxDQUFDLENBQUM7S0FDTixNQUFNO0FBQ0wsZ0JBQVUsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUE7QUFDcEIsY0FBUSxDQUFDLE9BQU8sQ0FBQyxFQUFDLFVBQVUsRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBQyxDQUFDLENBQUM7S0FDdkQ7O0FBRUQsV0FBTyxRQUFRLENBQUMsT0FBTyxDQUFDO0dBRXpCLENBQUE7Ozs7QUFJRCxNQUFJLENBQUMsMEJBQTBCLEdBQUcsWUFBWTs7QUFFNUMsUUFBSSxRQUFRLEdBQUcsRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDOztBQUUxQixRQUFJLFVBQVUsQ0FBQyxRQUFRLEVBQUUsR0FBRyxDQUFDLEVBQUU7QUFDN0IsVUFBSSxJQUFJLEdBQUcsVUFBVSxDQUFDLE9BQU8sRUFBRSxDQUFBO0FBQy9CLGFBQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQTtBQUMvQixVQUFJLENBQUMsR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQTtBQUNsQyxVQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFBOztBQUV6QixXQUFLLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLEtBQUssQ0FBQyxDQUNsQyxPQUFPLENBQUMsVUFBVSxJQUFJLEVBQUU7QUFDdEIsZ0JBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7T0FDeEIsQ0FBQyxDQUNGLEtBQUssQ0FBQyxVQUFVLEtBQUssRUFBRTtBQUNyQixnQkFBUSxDQUFDLE9BQU8sQ0FBQyxFQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLHdEQUF3RCxFQUFDLENBQUMsQ0FBQztPQUN0RyxDQUFDLENBQUM7S0FDTixNQUFNO0FBQ0wsYUFBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFBO0FBQ2pDLGNBQVEsQ0FBQyxPQUFPLENBQUMsRUFBQyxNQUFNLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQztLQUNsQzs7QUFFRCxXQUFPLFFBQVEsQ0FBQyxPQUFPLENBQUM7R0FDekIsQ0FBQTs7QUFFRCxNQUFJLENBQUMsZUFBZSxHQUFHLFlBQVk7O0FBRWpDLFFBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQzs7QUFFMUIsUUFBSSxVQUFVLENBQUMsUUFBUSxFQUFFLEdBQUcsQ0FBQyxFQUFFO0FBQzdCLFVBQUksSUFBSSxHQUFHLFVBQVUsQ0FBQyxPQUFPLEVBQUUsQ0FBQTtBQUMvQixVQUFJLFFBQVEsR0FBRyxVQUFVLENBQUMsY0FBYyxFQUFFLENBQUE7O0FBRTFDLFVBQUksQ0FBQyxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFBO0FBQ2xDLFVBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUE7OztBQUd6QixXQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtBQUNyQyxlQUFPLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUE7T0FDeEI7QUFDRCxhQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFBOztBQUVwQixXQUFLLENBQUMsSUFBSSxDQUFDLGtCQUFrQixFQUFFLEVBQUMsUUFBUSxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFDLENBQUMsQ0FDaEUsT0FBTyxDQUFDLFVBQVUsSUFBSSxFQUFFO0FBQ3RCLGdCQUFRLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO09BQ3hCLENBQUMsQ0FDRixLQUFLLENBQUMsVUFBVSxLQUFLLEVBQUU7QUFDckIsZ0JBQVEsQ0FBQyxPQUFPLENBQUMsRUFBQyxNQUFNLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSx3REFBd0QsRUFBQyxDQUFDLENBQUM7T0FDdEcsQ0FBQyxDQUFDO0tBRU4sTUFBTTtBQUNMLGFBQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLENBQUMsQ0FBQTtBQUNqQyxjQUFRLENBQUMsT0FBTyxDQUFDLEVBQUMsTUFBTSxFQUFFLElBQUksRUFBQyxDQUFDLENBQUM7S0FDbEM7O0FBRUQsV0FBTyxRQUFRLENBQUMsT0FBTyxDQUFDO0dBQ3pCLENBQUE7O0FBRUQsTUFBSSxDQUFDLGlCQUFpQixHQUFHLFlBQVk7QUFDbkMsV0FBTyxDQUFDLEdBQUcsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFBO0FBQ3BDLGNBQVUsQ0FBQyxPQUFPLEVBQUUsQ0FBQTtHQUNyQixDQUFBOztBQUVELFNBQU8sSUFBSSxDQUFDO0NBRWIsQ0FBQTs7O0FDOVBELFlBQVksQ0FBQzs7QUFFYixJQUFJLFVBQVUsR0FBRyxPQUFPLENBQUMsaUJBQWlCLENBQUMsQ0FBQTtBQUMzQyxJQUFJLE9BQU8sR0FBRyxPQUFPLENBQUMsc0JBQXNCLENBQUMsQ0FBQTs7QUFFN0MsT0FBTyxDQUFDLFFBQVEsR0FBRyxVQUFVLEtBQUssRUFBRSxFQUFFLEVBQUUsa0JBQWtCLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBRSxXQUFXLEVBQUU7O0FBRS9GLGFBQVMsUUFBUSxDQUFDLE1BQU0sRUFBRTtBQUN0QixZQUFJLE1BQU0sQ0FBQyxTQUFTLEVBQ2hCLE9BQU8sSUFBSSxDQUFBLEtBRVgsT0FBTyxHQUFHLENBQUM7S0FDbEI7O0FBRUQsYUFBUyxXQUFXLENBQUMsTUFBTSxFQUFFOztBQUV6QixZQUFJLEdBQUcsR0FBRyxFQUFFLENBQUE7QUFDWixZQUFJLE9BQU8sR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQTtBQUN2QyxhQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtBQUNyQyxlQUFHLENBQUMsSUFBSSxDQUFDLEVBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUMsRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFDLENBQUMsQ0FBQTtTQUM1QztBQUNELGVBQU8sR0FBRyxDQUFBOztLQUViOztBQUVELGFBQVMsU0FBUyxDQUFDLE9BQU8sRUFBRTs7QUFFeEIsWUFBSSxRQUFRLEdBQUcsRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDO0FBQzFCLGFBQUssQ0FBQyxHQUFHLENBQUMsaUJBQWlCLEdBQUcsT0FBTyxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsSUFBSSxFQUFFOztBQUMzRCxtQkFBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsRUFBRSxJQUFJLENBQUMsQ0FBQTtBQUN0QyxvQkFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQTtTQUN6QixDQUFDLENBQUMsS0FBSyxDQUFDLFVBQVUsS0FBSyxFQUFFO0FBQ3RCLG9CQUFRLENBQUMsTUFBTSxDQUFDLEVBQUMsR0FBRyxFQUFFLEtBQUssRUFBQyxDQUFDLENBQUE7U0FDaEMsQ0FBQyxDQUFDO0FBQ0gsZUFBTyxRQUFRLENBQUMsT0FBTyxDQUFDO0tBQzNCOztBQUdELGFBQVMsUUFBUSxDQUFDLFFBQVEsRUFBRSxPQUFPLEVBQUU7QUFDakMsWUFBSSxRQUFRLEdBQUcsRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDOztBQUUxQixZQUFJLEdBQUcsR0FBRyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsY0FBYyxFQUFFLE9BQU8sQ0FBQyxDQUFBO0FBQ2xFLFdBQUcsQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFBO0FBQ3JCLFdBQUcsQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQTtBQUM1QixXQUFHLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQyxTQUFTLENBQUE7QUFDbEMsV0FBRyxDQUFDLElBQUksR0FBRyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUE7QUFDN0IsV0FBRyxDQUFDLE9BQU8sR0FBRyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUE7O0FBRW5DLGVBQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLFFBQVEsQ0FBQyxDQUFBOztBQUU1QixpQkFBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLEtBQUssRUFBRTs7QUFFckMsZUFBRyxDQUFDLFdBQVcsR0FBRyxVQUFVLENBQUMsVUFBVSxDQUFDLEdBQUcsRUFBRSxLQUFLLENBQUMsQ0FBQTtBQUNuRCxvQkFBUSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQTtTQUN4QixDQUFDLENBQUE7O0FBRUYsZUFBTyxRQUFRLENBQUMsT0FBTyxDQUFDO0tBQzNCOztBQUVELFdBQU87O0FBRUgsb0JBQVksRUFBRSxzQkFBVSxTQUFTLEVBQUU7O0FBRS9CLGdCQUFJLFFBQVEsR0FBRyxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7O0FBRTFCLGlCQUFLLENBQUMsR0FBRyxDQUFDLGlCQUFpQixHQUFHLFNBQVMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUksRUFBRTtBQUM3RCx3QkFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQTthQUN6QixDQUFDLENBQUE7O0FBRUYsbUJBQU8sUUFBUSxDQUFDLE9BQU8sQ0FBQztTQUMzQjs7QUFFRCxpQkFBUyxFQUFFLG1CQUFVLE9BQU8sRUFBRTs7QUFFMUIsZ0JBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztBQUMxQixpQkFBSyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsR0FBRyxPQUFPLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxJQUFJLEVBQUU7O0FBQzNELHVCQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixFQUFFLElBQUksQ0FBQyxDQUFBO0FBQ3RDLHdCQUFRLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFBO2FBQ3pCLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBVSxLQUFLLEVBQUU7QUFDdEIsd0JBQVEsQ0FBQyxNQUFNLENBQUMsRUFBQyxHQUFHLEVBQUUsS0FBSyxFQUFDLENBQUMsQ0FBQTthQUNoQyxDQUFDLENBQUM7QUFDSCxtQkFBTyxRQUFRLENBQUMsT0FBTyxDQUFDO1NBQzNCOzs7O0FBSUQsaUJBQVMsRUFBRSxtQkFBVSxRQUFRLEVBQUUsaUJBQWlCLEVBQUU7O0FBRTlDLGdCQUFJLEdBQUcsR0FBRyxFQUFFLENBQUE7QUFDWiw2QkFBaUIsQ0FBQyxPQUFPLENBQUMsVUFBVSxPQUFPLEVBQUU7QUFDekMsb0JBQUksQ0FBQyxHQUFHLFFBQVEsQ0FBQyxRQUFRLEVBQUUsT0FBTyxDQUFDLENBQUE7QUFDbkMsbUJBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUE7YUFDZCxDQUFDLENBQUE7O0FBRUYsbUJBQU8sRUFBRSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxPQUFPLEVBQUU7QUFDdkMsdUJBQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxFQUFFLE9BQU8sQ0FBQyxDQUFBO0FBQ2xDLHVCQUFPLE9BQU8sQ0FBQTthQUNqQixDQUFDLENBQUE7U0FDTDs7Ozs7QUFLRCxvQkFBWSxFQUFFLHNCQUFVLFNBQVMsRUFBRTtBQUMvQixnQkFBSSxRQUFRLEdBQUcsRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDOztBQUUxQixtQkFBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsU0FBUyxDQUFDLENBQUE7QUFDbkMsZ0JBQUksU0FBUyxLQUFLLE1BQU0sSUFBSSxTQUFTLEVBQUU7QUFDbkMscUJBQUssQ0FBQyxHQUFHLENBQUMsb0JBQW9CLEdBQUcsU0FBUyxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsVUFBVSxFQUFFO0FBQ3RFLDRCQUFRLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQTtpQkFDbkMsQ0FBQyxDQUFBO2FBQ0wsTUFDRyxRQUFRLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFBOztBQUUvQixtQkFBTyxRQUFRLENBQUMsT0FBTyxDQUFDO1NBQzNCOzs7O0FBSUQsMkJBQW1CLEVBQUUsNkJBQVUsUUFBUSxFQUFFOztBQUVyQyxnQkFBSSxDQUFDLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUE7QUFDdkMsYUFBQyxDQUFDLFlBQVksR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFBO0FBQ25DLG1CQUFPLENBQUMsQ0FBQTtTQUNYOzs7OztBQUtELHNCQUFjLEVBQUUsd0JBQVUsT0FBTyxFQUFFO0FBQy9CLGdCQUFJLFFBQVEsR0FBRyxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7QUFDMUIsZ0JBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxFQUFFLEVBQUU7QUFDM0Isb0JBQUksVUFBVSxDQUFBO0FBQ2Qsb0JBQUksSUFBSSxHQUFHLFVBQVUsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFBO0FBQ3hDLG9CQUFJLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUNmLFVBQVUsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUEsS0FFcEIsVUFBVSxHQUFHLFVBQVUsQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLENBQUE7QUFDbkQsd0JBQVEsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUE7YUFDL0IsTUFBTTs7O0FBR0gsNEJBQVksQ0FBQyxrQkFBa0IsRUFBRSxDQUM1QixJQUFJLENBQUMsVUFBVSxVQUFVLEVBQUU7QUFDeEIsd0JBQUksVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7QUFDdkIsZ0NBQVEsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7cUJBQ2xDLE1BQU07QUFDSCxvQ0FBWSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxNQUFNLEVBQUU7QUFDOUMsb0NBQVEsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUFBO3lCQUM5QixDQUFDLENBQUE7cUJBQ0w7aUJBQ0osQ0FBQyxDQUFBO2FBQ1Q7QUFDRCxtQkFBTyxRQUFRLENBQUMsT0FBTyxDQUFDO1NBQzNCOztLQUVKLENBQUE7Q0FFSixDQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM3SkQsWUFBWSxDQUFDOztBQUViLElBQUksU0FBUyxHQUFHLE9BQU8sQ0FBQywyQ0FBMkMsQ0FBQyxDQUFBOztBQUVwRSxPQUFPLENBQUMsZ0JBQWdCLEdBQUcsVUFBVSxLQUFLLEVBQUUsRUFBRSxFQUFFOztBQUU1QyxRQUFJLEtBQUssR0FBRyxFQUFFLENBQUE7QUFDZCxXQUFPOztBQUVILGlCQUFTLEVBQUUsbUJBQVUsUUFBUSxFQUFFO0FBQzNCLGdCQUFJLFFBQVEsR0FBRyxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7QUFDMUIsbUJBQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxFQUFFLFFBQVEsQ0FBQyxDQUFBOztBQUVuQyxnQkFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtBQUNsQix3QkFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtBQUMxQixxQkFBSyxDQUFDLEtBQUssRUFBRSxDQUFBO2FBQ2hCLE1BQU07O0FBRUgscUJBQUssQ0FBQyxHQUFHLENBQUMsd0JBQXdCLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxJQUFJLEVBQUU7QUFDeEQseUJBQUssR0FBRyxJQUFJLENBQUE7O0FBRVosMkJBQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFBO0FBQzNCLDJCQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTs7QUFFL0IsNEJBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUE7O2lCQUcxQixDQUFDLENBQUMsS0FBSyxDQUFDLFVBQVUsS0FBSyxFQUFFO0FBQ3RCLDJCQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFBO2lCQUNyQixDQUFDLENBQUM7YUFDTjtBQUNELG1CQUFPLFFBQVEsQ0FBQyxPQUFPLENBQUE7U0FDMUI7S0FDSixDQUFBO0NBRUosQ0FBQTs7QUFFRCxPQUFPLENBQUMsc0JBQXNCLEdBQUcsVUFBVSxLQUFLLEVBQUUsRUFBRSxFQUFFOztBQUVsRCxRQUFJLENBQUMsR0FBRyxHQUFHLFVBQVUsSUFBSSxFQUFFOztBQUV2QixZQUFJLFFBQVEsR0FBRyxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7O0FBRTFCLGFBQUssQ0FBQyxHQUFHLENBQUMsMEJBQTBCLEdBQUcsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsSUFBSSxFQUFFO0FBQ2pFLG9CQUFRLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFBO1NBRXpCLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBVSxLQUFLLEVBQUU7QUFDdEIsb0JBQVEsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDeEIsQ0FBQyxDQUFDOztBQUVILGVBQU8sUUFBUSxDQUFDLE9BQU8sQ0FBQztLQUUzQixDQUFBOztBQUVELFdBQU8sSUFBSSxDQUFDO0NBRWYsQ0FBQTs7QUFFRCxPQUFPLENBQUMsY0FBYyxHQUFHLFVBQVUsS0FBSyxFQUFFLEVBQUUsRUFBRTs7QUFFMUMsUUFBSSxDQUFDLE1BQU0sR0FBRyxVQUFVLFdBQVcsRUFBRTtBQUNqQyxlQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUE7QUFDN0IsWUFBSSxRQUFRLEdBQUcsRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDOztBQUUxQixhQUFLLENBQUMsR0FBRyxDQUFDLHlCQUF5QixHQUFHLFdBQVcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUksRUFBRTtBQUN2RSxtQkFBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQTtBQUNqQixvQkFBUSxDQUFDLE9BQU8sQ0FBQyxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTSxLQUFLLElBQUksRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7U0FDbEcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFVLEtBQUssRUFBRTtBQUN0QixvQkFBUSxDQUFDLE9BQU8sQ0FBQztBQUNiLHNCQUFNLEVBQUUsS0FBSztBQUNiLHVCQUFPLEVBQUUsaUVBQWlFO2FBQzdFLENBQUMsQ0FBQztTQUNOLENBQUMsQ0FBQztBQUNILGVBQU8sUUFBUSxDQUFDLE9BQU8sQ0FBQztLQUMzQixDQUFBO0FBQ0QsV0FBTyxJQUFJLENBQUM7Q0FDZixDQUFBOztBQUVELE9BQU8sQ0FBQyxTQUFTLEdBQUcsVUFBVSxLQUFLLEVBQUUsRUFBRSxFQUFFO0FBQ3JDLFFBQUksQ0FBQyxHQUFHLEdBQUcsVUFBVSxRQUFRLEVBQUU7QUFDM0IsWUFBSSxRQUFRLEdBQUcsRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDOztBQUUxQixhQUFLLENBQUMsR0FBRyxDQUFDLHNCQUFzQixHQUFHLFFBQVEsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUksRUFBRTtBQUNqRSxtQkFBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQTtBQUNoQixtQkFBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQTtBQUNqQixvQkFBUSxDQUFDLE9BQU8sQ0FBQyxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTSxLQUFLLElBQUksRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUM7U0FDaEcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFVLEtBQUssRUFBRTtBQUN0QixvQkFBUSxDQUFDLE9BQU8sQ0FBQztBQUNiLHNCQUFNLEVBQUUsS0FBSztBQUNiLHVCQUFPLEVBQUUsaUVBQWlFO2FBQzdFLENBQUMsQ0FBQztTQUNOLENBQUMsQ0FBQzs7QUFFSCxlQUFPLFFBQVEsQ0FBQyxPQUFPLENBQUM7S0FDM0IsQ0FBQTtDQUNKLENBQUE7O0FBRUQsT0FBTyxDQUFDLFdBQVcsR0FBRyxVQUFVLEtBQUssRUFBRSxFQUFFLEVBQUU7QUFDdkMsUUFBSSxDQUFDLEdBQUcsR0FBRyxZQUFZOztBQUVuQixZQUFJLFFBQVEsR0FBRyxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7O0FBRTFCLGFBQUssQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxJQUFJLEVBQUU7QUFDckQsb0JBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDMUIsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFVLEtBQUssRUFBRTtBQUN0QixvQkFBUSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQztTQUN4QixDQUFDLENBQUM7O0FBRUgsZUFBTyxRQUFRLENBQUMsT0FBTyxDQUFDO0tBRTNCLENBQUE7O0FBRUQsV0FBTyxJQUFJLENBQUM7Q0FDZixDQUFBOztBQUVELE9BQU8sQ0FBQyxZQUFZLEdBQUcsVUFBVSxLQUFLLEVBQUUsRUFBRSxFQUFFOztBQUV4QyxRQUFJLENBQUMsR0FBRyxHQUFHLFlBQVk7O0FBRW5CLFlBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQzs7QUFFMUIsYUFBSyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUksRUFBRTtBQUNqRCxvQkFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQTtTQUN6QixDQUFDLENBQUE7O0FBRUYsZUFBTyxRQUFRLENBQUMsT0FBTyxDQUFDO0tBQzNCLENBQUE7O0FBRUQsUUFBSSxDQUFDLElBQUksR0FBRyxVQUFVLE9BQU8sRUFBRTs7QUFFM0IsWUFBSSxRQUFRLEdBQUcsRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDO0FBQzFCLGVBQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLEVBQUUsT0FBTyxDQUFDLENBQUE7QUFDdkMsYUFBSyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxPQUFPLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxJQUFJLEVBQUU7QUFDNUQsb0JBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUE7U0FDekIsQ0FBQyxDQUFDO0FBQ0gsZUFBTyxRQUFRLENBQUMsT0FBTyxDQUFDO0tBQzNCLENBQUE7O0FBRUQsV0FBTyxJQUFJLENBQUM7Q0FFZixDQUFBOztBQUdELE9BQU8sQ0FBQyxZQUFZLEdBQUcsVUFBVSxLQUFLLEVBQUUsRUFBRSxFQUFFOztBQUV4QyxRQUFJLENBQUMsR0FBRyxHQUFHLFVBQVUsVUFBVSxFQUFFOztBQUU3QixZQUFJLFFBQVEsR0FBRyxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7O0FBRTFCLFlBQUksVUFBVSxLQUFLLEtBQUssRUFBRTtBQUN0QixvQkFBUSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQTtTQUN2QixNQUNHLEtBQUssQ0FBQyxHQUFHLENBQUMsZUFBZSxHQUFHLFVBQVUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUksRUFBRTtBQUM1RCxtQkFBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsRUFBRSxJQUFJLENBQUMsQ0FBQTtBQUNwQyxtQkFBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUE7QUFDakMsb0JBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUE7U0FDekIsQ0FBQyxDQUFBOztBQUVOLGVBQU8sUUFBUSxDQUFDLE9BQU8sQ0FBQztLQUMzQixDQUFBOztBQUVELFFBQUksQ0FBQyxJQUFJLEdBQUcsVUFBVSxPQUFPLEVBQUU7O0FBRTNCLFlBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztBQUMxQixhQUFLLENBQUMsSUFBSSxDQUFDLHFCQUFxQixFQUFFLE9BQU8sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUksRUFBRTtBQUMvRCxvQkFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQTtTQUN6QixDQUFDLENBQUM7QUFDSCxlQUFPLFFBQVEsQ0FBQyxPQUFPLENBQUM7S0FDM0IsQ0FBQTs7QUFFRCxRQUFJLENBQUMsa0JBQWtCLEdBQUcsWUFBWTtBQUNsQyxZQUFJLFFBQVEsR0FBRyxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7QUFDMUIsYUFBSyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLFFBQVEsRUFBRTtBQUN2RCxvQkFBUSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUE7U0FDMUMsQ0FBQyxDQUFBO0FBQ0YsZUFBTyxRQUFRLENBQUMsT0FBTyxDQUFDO0tBQzNCLENBQUE7O0FBRUQsV0FBTyxJQUFJLENBQUM7Q0FFZixDQUFBOztBQUdELE9BQU8sQ0FBQyxTQUFTLEdBQUcsWUFBWTs7QUFFNUIsUUFBSSxNQUFNLEdBQUcsa0JBQWtCLENBQUM7QUFDaEMsUUFBSSxTQUFTLEdBQUcscUJBQXFCLENBQUE7QUFDckMsUUFBSSxLQUFLLEdBQUcscUJBQXFCLENBQUE7O0FBRWpDLFFBQUksU0FBUyxHQUFHOztBQUVaLGtCQUFVLEVBQUUsdUJBQXVCO0FBQ25DLGVBQU8sRUFBRSxjQUFjO0FBQ3ZCLGFBQUssRUFBRSw4QkFBOEI7QUFDckMsY0FBTSxFQUFFLHFCQUFxQjtBQUM3QixhQUFLLEVBQUUsOEJBQThCO0FBQ3JDLGNBQU0sRUFBRSwrQ0FBK0M7QUFDdkQsZUFBTyxFQUFFLCtDQUErQztBQUN4RCxxQkFBYSxFQUFFLHdGQUF3Rjs7S0FFMUcsQ0FBQTs7QUFHRCxXQUFPOztBQUVILGFBQUssRUFBRSxpQkFBWTtBQUNmLG1CQUFPLE1BQU0sQ0FBQztTQUNqQjs7QUFFRCxnQkFBUSxFQUFFLGtCQUFVLFFBQVEsRUFBRTtBQUMxQixrQkFBTSxHQUFHLFFBQVEsQ0FBQztTQUNyQjs7QUFFRCxtQkFBVyxFQUFFLHVCQUFZO0FBQ3JCLG1CQUFPLEtBQUssQ0FBQTtTQUNmOztBQUVELGdCQUFRLEVBQUUsb0JBQVk7QUFDbEIsbUJBQU8sU0FBUyxDQUFDO1NBQ3BCOztBQUVELGVBQU8sRUFBRSxtQkFBWTtBQUNqQixtQkFBTyxTQUFTLENBQUE7U0FDbkI7O0FBR0Qsb0JBQVksRUFBRSxzQkFBVSxNQUFNLEVBQUU7QUFDNUIscUJBQVMsR0FBRyxNQUFNLENBQUE7U0FDckI7O0FBRUQsdUJBQWUsRUFBRSx5QkFBVSxJQUFJLEVBQUU7QUFDN0IsaUJBQUssR0FBRyxJQUFJLENBQUE7U0FDZjs7QUFHRCwyQkFBbUIsRUFBRSw2QkFBVSxPQUFPLEVBQUU7O0FBRXBDLGdCQUFJLEdBQUcsR0FBRyw0QkFBNEIsR0FBRyxPQUFPLEdBQUcsb0dBQW9HLENBQUE7QUFDdkosbUJBQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxFQUFFLE9BQU8sQ0FBQyxDQUFBO0FBQ3JDLGdCQUFJLElBQUksR0FBRyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUE7QUFDN0IsZ0JBQUksSUFBSSxFQUFFOztBQUVOLG9CQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQTs7QUFFckUscUJBQUssa0NBQWdDLElBQUksQ0FBQyxJQUFJLDBHQUFxRyxNQUFNLFlBQVMsQ0FBQTtBQUNsSyx5QkFBUyxrQ0FBZ0MsSUFBSSxDQUFDLElBQUksb0NBQStCLE1BQU0sd0dBQXFHLENBQUE7YUFHL0wsTUFBTTs7QUFFSCxzQkFBTSxHQUFHLGtCQUFrQixDQUFDO0FBQzVCLHlCQUFTLEdBQUcscUJBQXFCLENBQUE7QUFDakMscUJBQUssR0FBRyxxQkFBcUIsQ0FBQTthQUVoQztTQUdKOztLQUVKLENBQUM7Q0FFTCxDQUFBOzs7O0FDclFELFlBQVksQ0FBQzs7QUFFYixJQUFJLGFBQWEsR0FBRyxPQUFPLENBQUMsNEJBQTRCLENBQUMsQ0FBQTs7QUFFekQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsQ0FBQTs7QUFFMUIsS0FBSyxJQUFJLEdBQUcsSUFBSSxhQUFhLEVBQUU7QUFDN0IsTUFBSSxhQUFhLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxFQUFFO0FBQ3JDLFdBQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxHQUFHLE1BQU0sR0FBRyxhQUFhLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztBQUMvQyxpQkFBYSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFlBQVksR0FBRyxDQUFDLENBQUMsQ0FBQTtHQUNyQztDQUNGOztBQUdELE9BQU8sQ0FBQyxrQkFBa0IsR0FBRyxVQUFVLEtBQUssRUFBRSxFQUFFLEVBQUU7Ozs7QUFJaEQsTUFBSSxPQUFPLEdBQUcsRUFBQyxJQUFJLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFFLE9BQU8sRUFBRSxFQUFFLEVBQUMsQ0FBQTtBQUM1RSxNQUFJLFlBQVksR0FBRyxFQUFDLFVBQVUsRUFBRSxDQUFDLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLE9BQU8sRUFBRSxJQUFJLEVBQUMsQ0FBQTtBQUMvRixNQUFJLEtBQUssR0FBRyxJQUFJLENBQUE7O0FBRWhCLE1BQUksU0FBUyxHQUFHOztBQUVkLGlCQUFhLEVBQUUsdUJBQVUsT0FBTyxFQUFFO0FBQ2hDLGFBQU8sYUFBYSxDQUFDLE9BQU8sQ0FBQyxLQUFLLFNBQVMsQ0FBQTtLQUM1Qzs7QUFFRCxRQUFJLEVBQUUsY0FBVSxJQUFJLEVBQUU7QUFDcEIsYUFBTyxHQUFHLElBQUksQ0FBQTtLQUNmOztBQUVELGdCQUFZLEVBQUcsc0JBQVMsT0FBTyxFQUFFO0FBQy9CLGFBQU8sQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFBO0tBQzFCOztBQUVELE9BQUcsRUFBRSxlQUFZO0FBQ2YsYUFBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsT0FBTyxDQUFDLENBQUE7QUFDM0IsYUFBTyxPQUFPLENBQUM7S0FDaEI7O0FBRUQsd0JBQW9CLEVBQUUsOEJBQVUsSUFBSSxFQUFFO0FBQ3BDLFVBQUksS0FBSyxFQUFFO0FBQ1QsWUFBSSxFQUFFLEdBQUksSUFBSSxLQUFLLElBQUksSUFBSSxJQUFJLEtBQUssS0FBSyxBQUFDLENBQUE7QUFDMUMsWUFBSSxLQUFLLFlBQVksS0FBSyxFQUFFO0FBQzFCLGNBQUksRUFBRSxFQUNKLE9BQU8sS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFBLEtBRWYsT0FBTyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUE7U0FDbEIsTUFBTTtBQUNMLGlCQUFPLEtBQUssQ0FBQTtTQUNiO09BRUYsTUFDQyxPQUFPLFNBQVMsQ0FBQTtLQUNuQjs7QUFFRCxrQkFBYyxFQUFFLDBCQUFZO0FBQzFCLFVBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUE7QUFDL0MsVUFBSSxDQUFDLEVBQUU7QUFDTCxZQUFJLEdBQUcsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDO0FBQ25CLGFBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtBQUMvQyxhQUFHLEdBQUcsR0FBRyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUE7U0FDckI7QUFDRCxlQUFPLEdBQUcsQ0FBQztPQUNaLE1BRUMsT0FBTyxDQUFDLENBQUMsQ0FBQztLQUNiOztBQUVELGdCQUFZLEVBQUUsd0JBQVk7QUFDeEIsVUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQTs7QUFFL0MsYUFBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLEVBQUUsQ0FBQyxDQUFDLENBQUE7QUFDOUIsYUFBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUE7QUFDNUIsVUFBSSxDQUFDLEVBQUU7QUFDTCxZQUFJLEdBQUcsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDO0FBQ2pCLFlBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFO0FBQ1QsZUFBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO0FBQy9DLGVBQUcsR0FBRyxHQUFHLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQTtXQUNuQjtTQUNGLE1BQU07O0FBQ0wsZUFBSyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUMsR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtBQUN0RCxlQUFHLEdBQUcsR0FBRyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUE7V0FDbkI7U0FDRjs7QUFFRCxZQUFJLE9BQU8sQ0FBQyxjQUFjLElBQUksQ0FBQyxDQUFDLGVBQWUsRUFFN0MsR0FBRyxHQUFHLEdBQUcsR0FBRyxDQUFDLENBQUMsZUFBZSxDQUFBOztBQUUvQixlQUFPLEdBQUcsQ0FBQztPQUNaLE1BRUMsT0FBTyxDQUFDLENBQUMsQ0FBQztLQUNiOztBQUVELGFBQVMsRUFBRSxtQkFBVSxPQUFPLEVBQUU7QUFDNUIsYUFBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsT0FBTyxDQUFDLENBQUE7QUFDbEMsVUFBSSxPQUFPLENBQUMsT0FBTyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQzlCLE9BQU8sQ0FBQyxDQUFBLEtBQ0w7O0FBRUgsWUFBSSxhQUFhLEVBQUUsVUFBVSxFQUFFLEdBQUcsQ0FBQTs7QUFFbEMsWUFBSSxPQUFPLENBQUMsT0FBTyxLQUFLLGNBQWMsRUFBRTtBQUN0QyxpQkFBTyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7U0FnQjNCLE1BQU0sSUFBSSxPQUFPLENBQUMsT0FBTyxLQUFLLE9BQU8sRUFBRTtBQUN0QyxtQkFBTyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUE7V0FDN0IsTUFDQyxPQUFPLENBQUMsQ0FBQztPQUNaO0tBQ0Y7O0FBRUQsYUFBUyxFQUFFLG1CQUFVLE9BQU8sRUFBRTs7QUFFNUIsYUFBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsT0FBTyxDQUFDLENBQUE7O0FBRWpDLFVBQUksT0FBTyxLQUFLLGNBQWMsRUFDNUIsT0FBTyxHQUFHO0FBQ1IsZUFBTyxFQUFFLE9BQU8sRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsWUFBWSxDQUFDLFlBQVk7QUFDekYsZUFBTyxFQUFFLEVBQUUsRUFBRSxZQUFZLEVBQUUsWUFBWSxDQUFDLFlBQVksRUFBRSxRQUFRLEVBQUUsSUFBSTtPQUNyRSxDQUFBLEtBQ0UsSUFBSSxPQUFPLEtBQUssT0FBTyxFQUFFO0FBQzVCLGVBQU8sR0FBRztBQUNSLGlCQUFPLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLEVBQUUsRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFFLGFBQWEsRUFBRSxFQUFFLEVBQUUsVUFBVSxFQUFFLFlBQVksQ0FBQyxVQUFVLEVBQUUsUUFBUSxFQUFFLElBQUk7U0FDOUgsQ0FBQTtPQUNGOztBQUVELGFBQU8sQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFBOztBQUVwQixhQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFBO0tBQ3JCOzs7QUFHRCxnQkFBWSxFQUFHLHNCQUFTLE9BQU8sRUFBRSxPQUFPLEVBQUU7O0FBRXhDLFVBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQTtBQUNqQixVQUFJLGFBQWEsR0FBRyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUE7O0FBSTFDLFVBQUksT0FBTyxLQUFLLGNBQWMsRUFDNUIsUUFBUSxHQUFHO0FBQ1QsZUFBTyxFQUFFLE9BQU8sRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsYUFBYSxDQUFDLFlBQVk7QUFDMUYsZUFBTyxFQUFFLEVBQUUsRUFBRSxZQUFZLEVBQUUsYUFBYSxDQUFDLFlBQVksRUFBRSxRQUFRLEVBQUUsSUFBSTtPQUN0RSxDQUFBLEtBQ0UsSUFBSSxPQUFPLEtBQUssT0FBTyxFQUFFO0FBQzVCLGdCQUFRLEdBQUc7QUFDVCxpQkFBTyxFQUFFLE9BQU8sRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxFQUFFLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxhQUFhLEVBQUUsRUFBRSxFQUFFLFVBQVUsRUFBRSxhQUFhLENBQUMsVUFBVSxFQUFFLFFBQVEsRUFBRSxJQUFJO1NBQy9ILENBQUE7T0FDRjs7QUFFRCxjQUFRLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQTs7QUFFckIsYUFBTyxRQUFRLENBQUE7S0FDaEI7O0FBR0Qsb0JBQWdCLEVBQUUsMEJBQVUsT0FBTyxFQUFFOztBQUVuQyxVQUFJLFFBQVEsR0FBRyxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7O0FBRTFCLFdBQUssQ0FBQyxHQUFHLENBQUMsNkJBQTZCLEdBQUcsT0FBTyxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsWUFBWSxFQUFFO0FBQ2pGLGVBQU8sQ0FBQyxHQUFHLENBQUMsMEJBQTBCLEVBQUUsWUFBWSxDQUFDLENBQUE7QUFDckQsZ0JBQVEsQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUM7T0FDaEMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFVLEtBQUssRUFBRTtBQUN4QixlQUFPLENBQUMsR0FBRyxDQUFDLDJCQUEyQixDQUFDLENBQUE7T0FDekMsQ0FBQyxDQUFDOztBQUVILGFBQU8sUUFBUSxDQUFDLE9BQU8sQ0FBQztLQUV6Qjs7Ozs7QUFNRCxvQkFBZ0IsRUFBRSwwQkFBVSxPQUFPLEVBQUU7QUFDbkMsa0JBQVksR0FBRyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUE7S0FDdEM7O0FBRUQsYUFBUyxFQUFFLG1CQUFVLE9BQU8sRUFBRTs7QUFFNUIsVUFBSSxRQUFRLEdBQUcsRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDO0FBQzFCLFdBQUssQ0FBQyxHQUFHLENBQUMsaUJBQWlCLEdBQUcsT0FBTyxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsSUFBSSxFQUFFOztBQUM3RCxlQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsQ0FBQTtBQUNsQyxhQUFLLEdBQUcsSUFBSSxDQUFBO0FBQ1osZ0JBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUE7T0FDdkIsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFVLEtBQUssRUFBRTtBQUN4QixhQUFLLEdBQUcsSUFBSSxDQUFBO09BQ2IsQ0FBQyxDQUFDO0FBQ0gsYUFBTyxRQUFRLENBQUMsT0FBTyxDQUFDO0tBQ3pCOztBQUdELG9CQUFnQixFQUFFLDRCQUFZO0FBQzVCLGFBQU8sWUFBWSxDQUFBO0tBQ3BCOzs7Ozs7Ozs7Ozs7Ozs7O0dBcUJGLENBQUE7O0FBRUQsU0FBTyxTQUFTLENBQUE7Q0FFakIsQ0FBQTs7OztBQzlPRCxZQUFZLENBQUM7O0FBRWIsSUFBSSxTQUFTLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFBOztBQUdwQyxTQUFTLHFCQUFxQixDQUFDLEtBQUssRUFBRSxFQUFFLEVBQUUsWUFBWSxFQUFFLE1BQU0sRUFBRTs7QUFFNUQsUUFBSSxRQUFRLEdBQUcsRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDO0FBQzFCLFFBQUksT0FBTyxHQUFHLFNBQVMsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLENBQUE7O0FBRWpELFdBQU8sQ0FBQyxHQUFHLENBQUMsNEJBQTRCLEVBQUUsWUFBWSxDQUFDLENBQUE7QUFDdkQsU0FBSyxDQUFDLEdBQUcsQ0FBQyw2QkFBNkIsR0FBRyxZQUFZLEdBQUUsVUFBVSxHQUFHLE1BQU0sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLFlBQVksRUFBRTtBQUN6RyxlQUFPLENBQUMsR0FBRyxDQUFDLDBCQUEwQixFQUFFLFlBQVksQ0FBQyxDQUFBO0FBQ3JELG9CQUFZLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQTtBQUM5QixnQkFBUSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQztLQUNsQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQVUsS0FBSyxFQUFFO0FBQ3RCLGVBQU8sQ0FBQyxHQUFHLENBQUMsMkJBQTJCLENBQUMsQ0FBQTtBQUN4QyxnQkFBUSxDQUFDLE1BQU0sQ0FBQyxFQUFDLEdBQUcsRUFBQywyQkFBMkIsRUFBQyxDQUFDLENBQUE7S0FDckQsQ0FBQyxDQUFDOztBQUVILFdBQU8sUUFBUSxDQUFDLE9BQU8sQ0FBQztDQUMzQjs7QUFHRCxPQUFPLENBQUMsZ0JBQWdCLEdBQUcsVUFBVSxNQUFNLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxVQUFVLEVBQUU7QUFDaEUsV0FBTyxxQkFBcUIsQ0FBQyxLQUFLLEVBQUUsRUFBRSxFQUFFLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxVQUFVLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQTtDQUMzRixDQUFBOzs7O0FBSUQsT0FBTyxDQUFDLG9CQUFvQixHQUFHLFVBQVUsTUFBTSxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsVUFBVSxFQUFFOztBQUVwRSxRQUFJLE9BQU8sR0FBRyxTQUFTLENBQUMsb0JBQW9CLENBQUMscUNBQXFDLEVBQUUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQTtBQUNoSCxRQUFJLE9BQU8sRUFBRTtBQUNULGVBQU8scUJBQXFCLENBQUMsS0FBSyxFQUFFLEVBQUUsRUFBRSxPQUFPLENBQUMsSUFBSSxFQUFFLFVBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBRSxDQUFBO0tBQzNFOzs7Ozs7Ozs7Ozs7Ozs7OztDQXNCSixDQUFBOztBQUVELE9BQU8sQ0FBQyxXQUFXLEdBQUcsVUFBVSxNQUFNLEVBQUUsRUFBRSxFQUFFLGtCQUFrQixFQUFFOztBQUU1RCxRQUFJLE9BQU8sR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUE7O0FBRTNDLHNCQUFrQixDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxDQUFBO0FBQzVDLFFBQUksS0FBSyxHQUFHLGtCQUFrQixDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQTtBQUNqRCxRQUFJLE1BQU0sR0FBRyxrQkFBa0IsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUE7O0FBRWxELFdBQU8sRUFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssRUFBRSxrQkFBa0IsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxPQUFPLEVBQUU7QUFDbEYsZUFBTztBQUNILGlCQUFLLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQztBQUNqQix3QkFBWSxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUM7QUFDeEIsZ0JBQUksRUFBRSxrQkFBa0IsQ0FBQyxHQUFHLEVBQUU7U0FDakMsQ0FBQTtLQUNKLENBQUMsQ0FBQTtDQUVMLENBQUE7O0FBRUQsT0FBTyxDQUFDLFdBQVcsR0FBRyxVQUFVLE1BQU0sRUFBRSxFQUFFLEVBQUUsa0JBQWtCLEVBQUUsSUFBSSxFQUFFOztBQUVsRSxRQUFJLE9BQU8sR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUE7QUFDM0MsUUFBSSxVQUFVLEdBQUcsa0JBQWtCLENBQUMsR0FBRyxFQUFFLENBQUMsVUFBVSxDQUFBOztBQUVwRCxXQUFPLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxPQUFPLEVBQUU7O0FBRWpFLGVBQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEVBQUUsT0FBTyxDQUFDLENBQUE7QUFDekMsZUFBTztBQUNILGdCQUFJLEVBQUUsa0JBQWtCLENBQUMsR0FBRyxFQUFFO0FBQzlCLG1CQUFPLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQztTQUN0QixDQUFBO0tBQ0osQ0FBQyxDQUFBO0NBRUwsQ0FBQTs7QUFHRCxPQUFPLENBQUMsbUJBQW1CLEdBQUcsVUFBVSxNQUFNLEVBQUUsRUFBRSxFQUFFLGtCQUFrQixFQUFFLElBQUksRUFBRTs7QUFFMUUsUUFBSSxPQUFPLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFBOztBQUUzQyxXQUFPLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLEVBQUUsRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLE9BQU8sRUFBRTtBQUNsRixlQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQTtBQUMzQixlQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSxPQUFPLENBQUMsQ0FBQTtBQUNoQyxlQUFPO0FBQ0gsZ0JBQUksRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDO0FBQ2hCLG9CQUFRLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQzs7Ozs7O1NBTXZCLENBQUE7S0FDSixDQUFDLENBQUE7Q0FFTCxDQUFBOztBQUdELE9BQU8sQ0FBQyxtQkFBbUIsR0FBRyxVQUFVLE1BQU0sRUFBRSxFQUFFLEVBQUUsa0JBQWtCLEVBQUUsSUFBSSxFQUFFOztBQUUxRSxRQUFJLE9BQU8sR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUE7QUFDM0MsUUFBSSxHQUFHLEdBQUcsa0JBQWtCLENBQUMsR0FBRyxFQUFFLENBQUE7O0FBR2xDLFdBQU8sQ0FBQyxHQUFHLENBQUMsNEJBQTRCLEVBQUUsR0FBRyxDQUFDLENBQUE7OztBQUc5QyxRQUFJLE9BQU8sR0FBRyxxQkFBcUIsQ0FBQTtBQUNuQyxRQUFJLE9BQU8sR0FBRyxHQUFHLENBQUMsT0FBTyxLQUFLLE9BQU8sR0FBRyxVQUFVLEdBQUcsYUFBYSxDQUFBOztBQUdsRSxXQUFPLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLE9BQU8sRUFBRTtBQUN4RCxlQUFPO0FBQ0gsZ0JBQUksRUFBRSxHQUFHO0FBQ1QsbUJBQU8sRUFBRSxHQUFHLENBQUMsT0FBTztBQUNwQixvQkFBUSxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUM7QUFDcEIscUJBQVMsRUFBRSxPQUFPLEdBQUcsT0FBTztBQUM1QixxQkFBUyxFQUFFLE9BQU8sR0FBRyxPQUFPO1NBTS9CLENBQUE7S0FDSixDQUFDLENBQUE7Q0FFTCxDQUFBOzs7Ozs7O0FBR0QsT0FBTyxDQUFDLGlCQUFpQixHQUFHLFVBQVUsTUFBTSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFOztBQUV2RSxRQUFJLE9BQU8sR0FBRyxXQUFXLENBQUE7QUFDekIsUUFBSSxPQUFPLEdBQUcsTUFBTSxDQUFBOztBQUdwQixXQUFPLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxZQUFZLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLE9BQU8sRUFBRTtBQUN4RCxlQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxPQUFPLENBQUMsQ0FBQTtBQUMvQixlQUFPO0FBQ0gsbUJBQU8sRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDO0FBQ25CLHFCQUFTLEVBQUUsT0FBTztBQUNsQixxQkFBUyxFQUFFLE9BQU87QUFDbEIsdUJBQVcsRUFBRSxNQUFNO0FBQ25CLDhCQUFrQixFQUFFLElBQUk7QUFDeEIscUJBQVMsRUFBRSxtQkFBVSxPQUFPLEVBQUU7QUFDMUIsNEJBQVksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsTUFBTSxFQUFFO0FBQzlDLDJCQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFBO0FBQ25CLDZCQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFBO2lCQUM5QixDQUFDLENBQUE7YUFDTDtTQUNKLENBQUE7S0FDSixDQUFDLENBQUE7Q0FFTCxDQUFBOztBQUdELE9BQU8sQ0FBQyxpQkFBaUIsR0FBRyxVQUFVLE1BQU0sRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRTs7QUFFdkUsUUFBSSxPQUFPLEdBQUcsV0FBVyxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQTs7QUFHcEQsV0FBTyxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsT0FBTyxFQUFFO0FBQ2hGLGVBQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEVBQUUsT0FBTyxDQUFDLENBQUE7QUFDekMsZUFBTztBQUNILG1CQUFPLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQztBQUNuQixxQkFBUyxFQUFFLE9BQU87QUFDbEIscUJBQVMsRUFBRSxFQUFFO0FBQ2IsdUJBQVcsRUFBRSxNQUFNO0FBQ25CLDhCQUFrQixFQUFFLElBQUk7QUFDeEIscUJBQVMsRUFBRSxtQkFBVSxPQUFPLEVBQUU7QUFDMUIsNEJBQVksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsTUFBTSxFQUFFO0FBQzlDLDJCQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFBO0FBQ25CLDZCQUFTLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFBO2lCQUMxQixDQUFDLENBQUE7YUFDTDtTQUNKLENBQUE7S0FDSixDQUFDLENBQUE7Q0FFTCxDQUFBOztBQUdELE9BQU8sQ0FBQyxTQUFTLEdBQUcsVUFBVSxNQUFNLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUU7O0FBRS9ELFdBQU8sRUFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLE9BQU8sRUFBRTtBQUNoRixlQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixFQUFFLE9BQU8sQ0FBQyxDQUFBO0FBQ3pDLGVBQU87QUFDSCxtQkFBTyxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUM7U0FDdEIsQ0FBQTtLQUNKLENBQUMsQ0FBQTtDQUVMLENBQUE7OztBQy9NRCxZQUFZLENBQUM7OztBQUliLE9BQU8sQ0FBQyxNQUFNLENBQUMsdUJBQXVCLEVBQUUsRUFBRSxDQUFDOzs7O0NBSXhDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxRQUFRLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxjQUFjLEVBQUUsb0JBQW9CLEVBQ3pGLFVBQVUsTUFBTSxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLGtCQUFrQixFQUFFOztBQUlwRSxRQUFNLENBQUMsT0FBTyxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUE7O0FBSXJDLFFBQU0sQ0FBQyxXQUFXLEdBQUcsVUFBVSxPQUFPLEVBQUU7O0FBRXRDLFdBQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLE9BQU8sQ0FBQyxDQUFBO0FBQzlCLHNCQUFrQixDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxDQUFBO0FBQzVDLHNCQUFrQixDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQTtBQUNyQyxhQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsR0FBRyxPQUFPLENBQUMsQ0FBQTtHQUNyQyxDQUFBO0NBR0YsQ0FBQyxDQUFDLENBRUosVUFBVSxDQUFDLFFBQVEsRUFBRSxDQUFDLFFBQVEsRUFBRSxPQUFPLEVBQUUsV0FBVyxFQUFFLFlBQVksRUFBRSxjQUFjLEVBQUUsb0JBQW9CLEVBQUUsYUFBYSxFQUFFLE1BQU0sRUFBRSxtQkFBbUIsRUFDbkosVUFBVSxNQUFNLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLGtCQUFrQixFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUUsaUJBQWlCLEVBQUU7O0FBRXRILFFBQU0sQ0FBQyxPQUFPLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQTs7QUFFckMsUUFBTSxDQUFDLEdBQUcsR0FBRyxrQkFBa0IsQ0FBQyxHQUFHLEVBQUUsQ0FBQTs7QUFFckMsU0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsaUJBQWlCLENBQUMsQ0FBQTs7QUFJeEMsTUFBSSxpQkFBaUIsQ0FBQyxPQUFPLEVBQzNCLE1BQU0sQ0FBQyxPQUFPLEdBQUcsaUJBQWlCLENBQUMsT0FBTyxDQUFBLEtBRTFDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUE7O0FBR3JDLFFBQU0sQ0FBQyxJQUFJLEdBQUcsVUFBVSxHQUFHLEVBQUU7QUFDM0IsT0FBRyxDQUFDLE9BQU8sR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFBOztBQUVsQyxRQUFJLEdBQUcsQ0FBQyxVQUFVLEVBQ2hCLFNBQVMsQ0FBQyxJQUFJLENBQUMscUJBQXFCLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxDQUFBLEtBRTVELFNBQVMsQ0FBQyxJQUFJLENBQUMscUJBQXFCLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxDQUFBO0dBQy9ELENBQUE7O0FBRUQsUUFBTSxDQUFDLE1BQU0sR0FBRyxVQUFVLEdBQUcsRUFBRTtBQUM3QixzQkFBa0IsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUE7O0FBRTVCLFFBQUksQ0FBQyxjQUFjLENBQUMsa0JBQWtCLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxNQUFNLEVBQUU7QUFDbkUsYUFBTyxDQUFDLEdBQUcsQ0FBQyw0QkFBNEIsQ0FBQyxDQUFBO0FBQ3pDLGdCQUFVLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQzs7QUFHL0MsVUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsTUFBTSxFQUFFO0FBQzlDLFlBQUksTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7QUFDckIsbUJBQVMsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFBO1NBQ2hFLE1BQ0MsU0FBUyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFBO09BQ25DLENBQUMsQ0FBQTtLQUNILENBQUMsQ0FBQTtHQUVILENBQUE7Q0FFRixDQUFDLENBQUMsQ0FBQTs7O0FDeEVQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDVkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDLzBDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzVCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzlMQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDakJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNSQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUMvREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNoUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNsSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUMzUkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCIvLyBAdHMtY2hlY2syXG4vLy8gPHJlZmVyZW5jZTIgcGF0aD1cIi9ob21lL3dhbmcvcHJvamVjdHMvdGtzX3Byb2plY3QvdGtzL25vZGVfbW9kdWxlcy9AdHlwZXMvYW5ndWxhci9pbmRleC5kLnRzXCIgLz5cbi8vLyA8cmVmZXJlbmNlIHBhdGg9XCIuL292ZXJyaWRlLmQudHNcIiAvPlxuXG4ndXNlIHN0cmljdCc7XG5cbi8qZ2xvYmFsIGFuZ3VsYXIgKi9cblxuLy92YXIgcm91dGVzID0gcmVxdWlyZSgnLi93ZWJfcm91dGVzLmpzJylcblxucmVxdWlyZSgnLi93ZWJfY29udHJvbGxlcnMuanMnKVxucmVxdWlyZSgnLi93ZWJfbWVudS5qcycpXG5yZXF1aXJlKCcuL3dlYl9zZXJ2aWNlcy5qcycpXG5yZXF1aXJlKCcuL3dlYl9jb250cm9sbGVyc190cmFkZW1hcmtzLmpzJylcbnJlcXVpcmUoJy4vd2ViX3N0dWRpZXMuanMnKVxucmVxdWlyZSgnLi93ZWJfbW9uaXRvci5qcycpXG5cbnJlcXVpcmUoJy4uL21vZHVsZXMvbW9kdWxlcy5qcycpXG52YXIgeCA9IHJlcXVpcmUoJy4vdGVtcDIuanNvbicpXG5cblxuXG52YXIgbW9kdWxlcyA9XG4gICAgW1xuICAgICAgICAnbmdSZXNvdXJjZScsICduZ1JvdXRlJywgJ25nQ29va2llcycsICduZ1Nhbml0aXplJywgJ3Bhc2NhbHByZWNodC50cmFuc2xhdGUnLFxuICAgICAgICAnYW5ndWxhcnRpY3MnLCAnYW5ndWxhcnRpY3MuZ29vZ2xlLmFuYWx5dGljcycsICdhbmd1bGFydGljcy5nb29nbGUudGFnbWFuYWdlcicsXG4gICAgICAgICdtb2QucGFnZXInLCAnbW9kLmF1dGhvcml6ZScsICd0a3MuY29udHJvbGxlcnMnLCAndGtzLnNlcnZpY2VzJywgJ3Rrcy5zZXJ2aWNlczInLFxuICAgICAgICAndGtzLmZpbHRlcnMnLCAndGtzLmRpcmVjdGl2ZXMnLCAndGtzLmRpcmVjdGl2ZXMyJywgJ21vZC5jb3VudHJpZXMnLCAnbW9kLmNhcmRzJyxcbiAgICAgICAgJ3Rrcy5yZWcuY29udHJvbGxlcnMnLCAndGtzLnN0dWR5LmNvbnRyb2xsZXJzJywgJ3Rrcy5tZW51JywgJ3Byb2ZpbGUuZGlyZWN0aXZlcycsXG4gICAgICAgICd1aS5ib290c3RyYXAnLCAnYW5ndWxhci1jb29raWUtbGF3JywndmNSZWNhcHRjaGEnXG4gICAgXVxuXG4gICAgXG5cbmFuZ3VsYXIubW9kdWxlKCd0a3MnLCBtb2R1bGVzKVxuICAgIC5jb25maWcocmVxdWlyZSgnLi93ZWJfcm91dGVzLmpzJykpXG4gICAgLmNvbmZpZyhmdW5jdGlvbiAoJHRyYW5zbGF0ZVByb3ZpZGVyKSB7XG5cbiAgICAgICAgbGV0IGxvY2FsZV9lbiA9IHJlcXVpcmUoJy4vbG9jYWxlX2VuLmpzb24nKVxuICAgICAgICBsZXQgbG9jYWxlX3poX0NOID0gcmVxdWlyZSgnLi9sb2NhbGVfemgtQ04uanNvbicpXG4gICAgICAgIGNvbnNvbGUubG9nKCdsb2NhbGVfZW4nLCBsb2NhbGVfZW4pXG5cbiAgICAgICAgJHRyYW5zbGF0ZVByb3ZpZGVyLnVzZVNhbml0aXplVmFsdWVTdHJhdGVneSgnZXNjYXBlJyk7XG5cbiAgICAgICAgJHRyYW5zbGF0ZVByb3ZpZGVyLnRyYW5zbGF0aW9ucygnZW4nLCBsb2NhbGVfZW4pXG4gICAgICAgICAgICAudHJhbnNsYXRpb25zKCd6aC1DTicsIGxvY2FsZV96aF9DTik7XG5cbiAgICAgICAgJHRyYW5zbGF0ZVByb3ZpZGVyLnByZWZlcnJlZExhbmd1YWdlKCdlbicpO1xuXG4gICAgfSlcbiAgICAucnVuKFtcIiRyb290U2NvcGVcIiwgXCIkbG9jYXRpb25cIiwgXCJhdXRoU2VydmljZVwiLCBcInBhZ2VfaW5mb1wiLCBmdW5jdGlvbiAoJHJvb3RTY29wZSwgJGxvY2F0aW9uLCBhdXRoU2VydmljZSwgcGFnZV9pbmZvKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdoYWhhJylcbiAgICAgICAgY29uc29sZS5sb2coJ2hlbGxvJylcbiAgICAgICAgJHJvb3RTY29wZS4kb24oXCIkcm91dGVDaGFuZ2VTdGFydFwiLCBmdW5jdGlvbiAoZXZlbnQsIG5leHQsIGN1cnJlbnQpIHtcblxuICAgICAgICAgICAgY29uc29sZS5sb2coJ2NoYW5naW5nJylcbiAgICAgICAgICAgIGxldCBzdWJmaXggPSBcIiAtIFRyYWRlTWFya2Vyc8KuLmNvbVwiXG4gICAgICAgICAgICBsZXQgcGFnZV90aXRsZSA9IFwiUmVnaXN0ZXIgWW91ciBUcmFkZW1hcmsgV29ybGR3aWRlXCJcbiAgICAgICAgICAgIC8vcGFnZV9pbmZvLnNldFRpdGxlKFwiUmVnaXN0ZXIgWW91ciBUcmFkZW1hcmsgV29ybGR3aWRlXCIgKyBzdWJmaXgpXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhuZXh0LiQkcm91dGUpXG5cblxuICAgICAgICAgICAgaWYgKG5leHQuJCRyb3V0ZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICAgbGV0IG9yaWdpbmFsUGF0aCA9IG5leHQuJCRyb3V0ZS5vcmlnaW5hbFBhdGhcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnb3JpZ2luYWwgcGF0aCcsIG9yaWdpbmFsUGF0aClcbiAgICAgICAgICAgICAgICBpZiAob3JpZ2luYWxQYXRoID09PSAnL2Fib3V0JylcbiAgICAgICAgICAgICAgICAgICAgcGFnZV90aXRsZSA9ICdBYm91dCBVcydcbiAgICAgICAgICAgICAgICBlbHNlIGlmIChvcmlnaW5hbFBhdGggPT09ICcvY291bnRyaWVzJylcbiAgICAgICAgICAgICAgICAgICAgcGFnZV90aXRsZSA9IFwiSW50ZXJuYXRpb25hbCBUcmFkZW1hcmsgUmVnaXN0cmF0aW9uXCJcbiAgICAgICAgICAgICAgICBlbHNlIGlmIChvcmlnaW5hbFBhdGggPT09ICcvY29udGFjdCcpXG4gICAgICAgICAgICAgICAgICAgIHBhZ2VfdGl0bGUgPSBcIkNvbnRhY3QgVXNcIlxuICAgICAgICAgICAgICAgIGVsc2UgaWYgKG9yaWdpbmFsUGF0aCA9PT0gJy9jbGFzc2VzJylcbiAgICAgICAgICAgICAgICAgICAgcGFnZV90aXRsZSA9ICdOaWNlIENsYXNzIERlc2NyaXB0aW9ucydcbiAgICAgICAgICAgICAgICBlbHNlIGlmIChvcmlnaW5hbFBhdGggPT09ICcvc2VhcmNoX2NsYXNzZXMnKVxuICAgICAgICAgICAgICAgICAgICBwYWdlX3RpdGxlID0gJ1NlYXJjaCBOSUNFIENsYXNzZXMnXG4gICAgICAgICAgICAgICAgZWxzZSBpZiAob3JpZ2luYWxQYXRoID09PSAnL3Rlcm1zJylcbiAgICAgICAgICAgICAgICAgICAgcGFnZV90aXRsZSA9IFwiVGVybXMgYW5kIENvbmRpdGlvbnNcIlxuICAgICAgICAgICAgfVxuXG5cbiAgICAgICAgICAgIHBhZ2VfaW5mby5zZXRUaXRsZShwYWdlX3RpdGxlICsgc3ViZml4KVxuXG5cblxuICAgICAgICAgICAgaWYgKG5leHQuJCRyb3V0ZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICAgaWYgKG5leHQuJCRyb3V0ZS5pc1ByaXYgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgICAgICBpZiAobmV4dC4kJHJvdXRlLmlzUHJpdikge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFhdXRoU2VydmljZS5pc1NpZ25lZEluKCkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnY2hhbmdpbmcgdG8gc2lnbmluJylcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkbG9jYXRpb24ucGF0aChcIi9zaWduaW5cIik7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH1dKVxuICAgIC5ydW4oWydEQlN0b3JlJywgZnVuY3Rpb24gKERCU3RvcmUpIHtcbiAgICAgICAgY29uc29sZS5sb2coJ0RCc3RvcmUnKVxuICAgICAgICBEQlN0b3JlLmNyZWF0ZSgpXG4gICAgfV0pXG5cblxuICAgIC5mYWN0b3J5KCdhdXRoSW50ZXJjZXB0b3InLCBbJyRxJywgJyRsb2NhdGlvbicsICdTZXNzaW9uJywgJ0RCU3RvcmUnLCBmdW5jdGlvbiAoJHEsICRsb2NhdGlvbiwgU2Vzc2lvbiwgREJTdG9yZSkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgcmVxdWVzdDogZnVuY3Rpb24gKGNvbmZpZykge1xuICAgICAgICAgICAgICAgIGNvbmZpZy5oZWFkZXJzID0gY29uZmlnLmhlYWRlcnMgfHwge307XG4gICAgICAgICAgICAgICAgaWYgKFNlc3Npb24uZ2V0U2Vzc2lvbklEKCkpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uZmlnLmhlYWRlcnMuQXV0aG9yaXphdGlvbiA9ICdCZWFyZXIgJyArIFNlc3Npb24uZ2V0U2Vzc2lvbklEKClcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgcmV0dXJuIGNvbmZpZztcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICByZXNwb25zZTogZnVuY3Rpb24gKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ3Jlc3AnLCByZXNwb25zZS5zdGF0dXMpXG4gICAgICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlIHx8ICRxLndoZW4ocmVzcG9uc2UpO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHJlc3BvbnNlRXJyb3I6IGZ1bmN0aW9uIChyZXNwb25zZSkge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdyZXNwJywgcmVzcG9uc2Uuc3RhdHVzKVxuICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5zdGF0dXMgPT09IDQwMSkge1xuICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UuZGF0YS5pbmRleE9mKFwiand0IGV4cGlyZWRcIikgIT09IC0xKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBTZXNzaW9uLmRlc3Ryb3koKVxuICAgICAgICAgICAgICAgICAgICAgICAgJGxvY2F0aW9uLnBhdGgoJy9zaWduaW4nKVxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuICRxLnJlamVjdChyZXNwb25zZSk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkbG9jYXRpb24ucGF0aCgnLycpXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gJHEucmVqZWN0KHJlc3BvbnNlKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAocmVzcG9uc2Uuc3RhdHVzID09PSA0MDQpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2cocmVzcG9uc2UpXG4gICAgICAgICAgICAgICAgICAgICRsb2NhdGlvbi5wYXRoKCcvNDA0LycgKyBEQlN0b3JlLmFkZChyZXNwb25zZS5kYXRhKSlcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuICRxLnJlamVjdChyZXNwb25zZSlcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgLy9yZXR1cm4gcmVzcG9uc2UgfHwgJHEud2hlbihyZXNwb25zZSk7XG4gICAgICAgICAgICAgICAgcmV0dXJuICRxLnJlamVjdChyZXNwb25zZSlcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1dKVxuXG4gICAgLmNvbmZpZyhbJyRodHRwUHJvdmlkZXInLCBmdW5jdGlvbiAoJGh0dHBQcm92aWRlcikge1xuICAgICAgICAkaHR0cFByb3ZpZGVyLmludGVyY2VwdG9ycy5wdXNoKCdhdXRoSW50ZXJjZXB0b3InKTtcbiAgICB9XSlcbiIsIlwidXNlIHN0cmljdFwiO1xuLyogZ2xvYmFsIGFuZ3VsYXIgKi9cbi8qIERpcmVjdGl2ZXMgKi9cblxudmFyIHV0aWxpdGllcyA9IHJlcXVpcmUoJy4uL21vZHVsZXMvdXRpbGl0aWVzLmpzJylcbnZhciBjb3VudHJpZXMgPSByZXF1aXJlKCdjb3VudHJpZXMnKVxuXG5hbmd1bGFyLm1vZHVsZSgncHJvZmlsZS5kaXJlY3RpdmVzJywgW10pLlxuICBkaXJlY3RpdmUoJ2FwcFZlcnNpb24nLCBbJ3ZlcnNpb24nLCBmdW5jdGlvbiAodmVyc2lvbikge1xuICAgIHJldHVybiBmdW5jdGlvbiAoc2NvcGUsIGVsbSwgYXR0cnMpIHtcbiAgICAgIGVsbS50ZXh0KHZlcnNpb24pO1xuICAgIH07XG4gIH1dKS5kaXJlY3RpdmUoJ3Byb2ZpbGUxMjMnLCBmdW5jdGlvbiAoKSB7XG4gICAgY29uc29sZS5sb2coJ2luIHByb2ZpbGUgaGVsbG8nKVxuICAgIHJldHVybiB7XG4gICAgICByZXN0cmljdDogJ0UnLFxuICAgICAgc2NvcGU6IHtcbiAgICAgICAgbmFtZTogJ0AnLFxuICAgICAgICBkYXRhc291cmNlIDogJz0nICAgICAgIC8vIHBhc3MgYSBqc29uIG9iamVjdFxuICAgICAgfSxcbiAgICAgIHRlbXBsYXRlOiAnPHNwYW4+SGVsbG8ge3tuYW1lfX08L3NwYW4+J1xuICAgIH1cbiAgfSlcblxuICAvLyB0byBnZXQgbmFtZSwgY291bnRyeSBvZiBpbmMsIGNvbnRhY3QgYW5kIGVtYWlsXG5cbiAgLmRpcmVjdGl2ZSgncHJvZmlsZU5hbWVzJywgWyckaHR0cCcsICdjb3VudHJpZXMnLCBmdW5jdGlvbiAoJGh0dHAsIGNvdW50cmllcykge1xuICAgIHJldHVybiB7XG4gICAgICByZXN0cmljdDogJ0UnLFxuICAgICAgc2NvcGU6IHtcbiAgICAgICAgbmFtZTogJ0AnLFxuICAgICAgICBkYXRhc291cmNlOiAnPSdcbiAgICAgIH0sXG4gICAgICB0ZW1wbGF0ZVVybDogJy9wYXJ0aWFscy9wcm9maWxlTmFtZXMuaHRtbCcsXG4gICAgICBsaW5rOiBmdW5jdGlvbiAoc2NvcGUsIGVsLCBhdHRyKSB7XG5cbiAgICAgICAgaWYgKCFzY29wZS5kYXRhc291cmNlLmluY19jb3VudHJ5KVxuLy8gICAgICAgICAgJGh0dHAuZ2V0KCcvYXBpMi9nZXRfZ2VvP3Rlc3RfaXA9MTc0LjU0LjE2NS4yMDYnKS5zdWNjZXNzKGZ1bmN0aW9uIChnZW8pIHtcbi8vICAgICAgICAgICRodHRwLmdldCgnL2FwaTIvZ2V0X2dlbz90ZXN0X2lwPTExMi4yMTAuMTUuMTA3Jykuc3VjY2VzcyhmdW5jdGlvbiAoZ2VvKSB7XG4gICAgICAgICAgJGh0dHAuZ2V0KCcvYXBpMi9nZXRfZ2VvJykuc3VjY2VzcyhmdW5jdGlvbiAoZ2VvKSB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZygnZ2VvJywgZ2VvKVxuICAgICAgICAgICAgaWYgKGdlby5jb3VudHJ5KVxuICAgICAgICAgICAgICBzY29wZS5kYXRhc291cmNlLmluY19jb3VudHJ5ID0gZ2VvLmNvdW50cnlcbiAgICAgICAgICAgIHNjb3BlLmRhdGFzb3VyY2UubmF0dXJlID0gJ0MnXG4gICAgICAgICAgfSlcblxuICAgICAgICBzY29wZS5uYXR1cmVzID0gW1xuICAgICAgICAgIHtuYW1lOiAnQ29tcGFueScsIGNvZGU6ICdDJ30sXG4gICAgICAgICAge25hbWU6ICdJbmRpdmlkdWFsJywgY29kZTogJ0knfVxuICAgICAgICBdXG4gICAgICAgIHNjb3BlLmNvdW50cmllcyA9IGNvdW50cmllc1xuICAgICAgfVxuICAgIH1cbiAgfV0pXG5cbiAgLmRpcmVjdGl2ZSgncHJvZmlsZUFkZHJlc3MnLCBbJyRodHRwJywgJyR0ZW1wbGF0ZUNhY2hlJywgJyRjb21waWxlJywgZnVuY3Rpb24gKCRodHRwLCAkdGVtcGxhdGVDYWNoZSwgJGNvbXBpbGUpIHtcbiAgICBjb25zb2xlLmxvZygnaW4gcHJvZmlsZTMgaGVsbG8nKVxuICAgIHJldHVybiB7XG4gICAgICByZXN0cmljdDogJ0UnLFxuICAgICAgc2NvcGU6IHtcbiAgICAgICAgbmFtZTogJ0AnLFxuICAgICAgICBkYXRhc291cmNlIDogJz0nLFxuICAgICAgICBjb3VudHJ5IDogJz0nXG4gICAgICB9LFxuLy8gICAgICB0ZW1wbGF0ZVVybDogJy9wYXJ0aWFscy9wcm9maWxlQWRkcmVzcy5odG1sJyxcbi8vICAgICAgdGVtcGxhdGVVcmw6ICcvYXBpMi9zZXJ2ZXJfcGFydGlhbHMvYWRkcmVzcy97e2NvZGV9fScsXG5cbiAgICAgIGxpbms6IGZ1bmN0aW9uIChzY29wZSwgZWwsIGF0dHIpIHtcblxuICAgICAgICAvKlxuICAgICAgICBzY29wZS5zdGF0ZXMyID0ge1xuICAgICAgICAgIFwiUEguQUJcIjogXCJ0ZXN0XCIsXG4gICAgICAgICAgXCJQSC5DQlwiOiBcImNlYnVcIlxuICAgICAgICB9XG4gICAgICAgICovXG5cbiAgICAgICAgc2NvcGUuc3RhdGVzID0gY291bnRyaWVzLmdldF9zdGF0ZXMoc2NvcGUuY291bnRyeSlcblxuICAgICAgICBjb25zb2xlLmxvZygnYXR0cjEnLGF0dHIpXG4gICAgICAgIGNvbnNvbGUubG9nKCdkcycsIHNjb3BlLmRhdGFzb3VyY2UpXG4gICAgICAgIGNvbnNvbGUubG9nKCdjJywgc2NvcGUuY291bnRyeSlcbiAgICAgICAgY29uc29sZS5sb2coJ3Njb3BlJywgc2NvcGUpXG5cblxuICAgICAgICB2YXIgdXJsID0gXCIvYXBpMi9zZXJ2ZXJfcGFydGlhbHMvYWRkcmVzcy9cIiArIHNjb3BlLmNvdW50cnlcbiAgICAgICAgY29uc29sZS5sb2codXJsKVxuICAgICAgICBsb2FkVGVtcGxhdGUoXCIvYXBpMi9zZXJ2ZXJfcGFydGlhbHMvYWRkcmVzcy9cIiArIHNjb3BlLmNvdW50cnkpO1xuICAgICAgICBjb25zb2xlLmxvZygnY291bnRyeSBpbiB3YXRjaDEnLCBzY29wZS5jb3VudHJ5KVxuXG4gICAgICAgIC8qXG5cbiAgICAgICAgc2NvcGUuJHdhdGNoKGF0dHIuY291bnRyeSwgZnVuY3Rpb24gKHZhbHVlKSB7XG4gICAgICAgICAgaWYgKHZhbHVlKSB7XG4gICAgICAgICAgICB2YXIgdXJsID0gXCIvYXBpMi9zZXJ2ZXJfcGFydGlhbHMvYWRkcmVzcy9cIiArIHZhbHVlXG4gICAgICAgICAgICBjb25zb2xlLmxvZyh1cmwpXG4gICAgICAgICAgICBsb2FkVGVtcGxhdGUoXCIvYXBpMi9zZXJ2ZXJfcGFydGlhbHMvYWRkcmVzcy9cIiArIHZhbHVlKTtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdjb3VudHJ5IGluIHdhdGNoJywgdmFsdWUpXG4gICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgICAgKi9cblxuXG4gICAgICAgIGZ1bmN0aW9uIGxvYWRUZW1wbGF0ZSh0ZW1wbGF0ZSkge1xuICAgICAgICAgICRodHRwLmdldCh0ZW1wbGF0ZSAvKiwgeyBjYWNoZTogJHRlbXBsYXRlQ2FjaGUgfSovKVxuICAgICAgICAgICAgLnN1Y2Nlc3MoZnVuY3Rpb24odGVtcGxhdGVDb250ZW50KSB7XG5cbi8vICAgICAgICAgICAgICBjb25zb2xlLmxvZygnZGQnLCB0ZW1wbGF0ZUNvbnRlbnQpXG4gICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdlbCcsZWwpXG4gICAgICAgICAgICAgIGVsLnJlcGxhY2VXaXRoKCRjb21waWxlKHRlbXBsYXRlQ29udGVudCkoc2NvcGUpKTtcbiAgICAgICAgICAgICAgY29uc29sZS5sb2coJ2xvYWRlZCcsIHNjb3BlKVxuICAgICAgICAgICAgICBzY29wZS4kcGFyZW50LnJlYWR5MnNob3cgPSB0cnVlICAvLyBmbGFnIHRvIGluZm9ybSB0aGUgbWFpbiBmb3JtIHRvIHNob3csIGl0J3MgYSBoYWNrXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuXG4gICAgICAgIGNvbnNvbGUubG9nKCdzY29wZScsc2NvcGUpXG5cblxuLy8gICAgICAgIHNjb3BlLmNvZGUgPSAnQVMnXG4gICAgICAgIC8qXG4gICAgICAgIGlmICghc2NvcGUuZGF0YXNvdXJjZS5jb3VudHJ5KVxuICAgICAgICAgICRodHRwLmdldCgnL2FwaTIvZ2V0X2dlbz90ZXN0X2lwPTE3NC41NC4xNjUuMjA2Jykuc3VjY2VzcyhmdW5jdGlvbiAoZ2VvKSB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZygnZ2VvJywgZ2VvKVxuICAgICAgICAgICAgaWYgKGdlby5jb3VudHJ5KSB7XG4gICAgICAgICAgICAgIHNjb3BlLmRhdGFzb3VyY2UuY291bnRyeSA9IGdlby5jb3VudHJ5XG4gICAgICAgICAgICAgIHNjb3BlLmRhdGFzb3VyY2Uuc3RhdGUgPSBnZW8ucmVnaW9uXG4gICAgICAgICAgICAgIHNjb3BlLmRhdGFzb3VyY2UuY2l0eSA9IGdlby5jaXR5XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSlcblxuICAgICAgICBjb25zb2xlLmxvZygnZGF0YXNvdXJjZTInLCBzY29wZS5kYXRhc291cmNlKVxuICAgICAgICAqL1xuICAgICAgICBzY29wZS5jb3VudHJpZXMgPSBjb3VudHJpZXMuZ2V0X2NvdW50cmllcygpXG4gICAgICB9XG4gICAgfVxuICB9XSlcblxuXG5cbiIsIlwidXNlIHN0cmljdFwiO1xuLyogZ2xvYmFsIGFuZ3VsYXIsIGpRdWVyeSAqL1xuLyogRGlyZWN0aXZlcyAqL1xuXG52YXIgdXRpbGl0aWVzID0gcmVxdWlyZSgnLi4vbW9kdWxlcy91dGlsaXRpZXMuanMnKVxuXG5cbmZ1bmN0aW9uIGFwcGVuZF9vbmVfd29yZF9jb3VudHJ5X25hbWUobHN0KSB7XG4gICAgdmFyIHJsc3QgPSBbXVxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgbHN0Lmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIGxzdFtpXS5zaW1wbGVfbmFtZSA9IGxzdFtpXS5uYW1lLnJlcGxhY2UoLyAvZywgJ18nKS50b0xvd2VyQ2FzZSgpXG4gICAgICAgIHJsc3QucHVzaChsc3RbaV0pXG4gICAgfVxuICAgIHJldHVybiBybHN0XG59XG5cblxuYW5ndWxhci5tb2R1bGUoJ3Rrcy5kaXJlY3RpdmVzJywgW10pLlxuICAgIGRpcmVjdGl2ZSgnYXBwVmVyc2lvbicsIFsndmVyc2lvbicsXG4gICAgICAgIGZ1bmN0aW9uICh2ZXJzaW9uKSB7XG4gICAgICAgICAgICByZXR1cm4gZnVuY3Rpb24gKHNjb3BlLCBlbG0sIGF0dHJzKSB7XG4gICAgICAgICAgICAgICAgZWxtLnRleHQodmVyc2lvbik7XG4gICAgICAgICAgICB9O1xuICAgICAgICB9XG4gICAgXSkuZGlyZWN0aXZlKCdoZWxsbzInLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICByZXN0cmljdDogJ0UnLFxuICAgICAgICAgICAgc2NvcGU6IHtcbiAgICAgICAgICAgICAgICBuYW1lOiAnQCdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB0ZW1wbGF0ZTogJzxzcGFuPkhlbGxvIHt7bmFtZX19PC9zcGFuPidcbiAgICAgICAgfVxuICAgIH0pLmRpcmVjdGl2ZSgndHJhZGVtYXJrUmVxdWVzdCcsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIHJlc3RyaWN0OiAnRScsXG4gICAgICAgICAgICBzY29wZToge1xuICAgICAgICAgICAgICAgIG5hbWU6ICdAJyxcbiAgICAgICAgICAgICAgICB0bXJlZzogJz0nLFxuICAgICAgICAgICAgICAgIHByb2ZpbGU6ICc9J1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHRlbXBsYXRlVXJsOiAnL3BhcnRpYWxzL3RtcmVxdWVzdC5odG1sJyxcbiAgICAgICAgICAgIC8qXG4gICAgICAgICAgICAgY29udHJvbGxlcjogZnVuY3Rpb24gKCRzY29wZSkge1xuICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdjdHJsJylcbiAgICAgICAgICAgICBjb25zb2xlLmxvZygkc2NvcGUpXG4gICAgICAgICAgICAgY29uc29sZS5sb2coJ2N0cmwyJylcbiAgICAgICAgICAgICB9Ki9cbiAgICAgICAgICAgIGxpbms6IGZ1bmN0aW9uICgkc2NvcGUsIGVsZW1lbnQpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnYWJjJylcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygkc2NvcGUucHJvZmlsZV9uYW1lKVxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCRzY29wZSlcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnYWJjeCcpXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAvKlxuICAgICAgICAgICAgIGxpbms6IGZ1bmN0aW9uIChzY29wZSwgZWxlbWVudCkge1xuICAgICAgICAgICAgIGNvbnNvbGUubG9nKHNjb3BlLnRtcmVnKVxuICAgICAgICAgICAgIHNjb3BlLm5hbWUyID0gJ0plZmYnO1xuXG4gICAgICAgICAgICAgdmFyIHRlbXBcbiAgICAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8c2NvcGUudG1yZWcuY2xhc3Nlcy5sZW5ndGg7IGkgKyspIHtcbiAgICAgICAgICAgICB2YXIgYyA9IHNjb3BlLnRtcmVnLmNsYXNzZXNbMF1cbiAgICAgICAgICAgICBjb25zb2xlLmxvZyhjKVxuICAgICAgICAgICAgIGlmIChpID09PSAwKVxuICAgICAgICAgICAgIHRlbXAgPSBjLm5hbWUgKyAnICgnICsgYy5kZXRhaWxzICsgJyknXG4gICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgdGVtcCA9IHRlbXAgKyAnLCAnICsgYy5uYW1lICsgJyAoJyArIGMuZGV0YWlscyArICcpJ1xuICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgc2NvcGUuY2xhc3NlcyA9IHRlbXBcbiAgICAgICAgICAgICB9Ki9cblxuICAgICAgICB9XG4gICAgfSkuZGlyZWN0aXZlKCd0bVBob3RvJywgZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgcmVzdHJpY3Q6ICdFJyxcbiAgICAgICAgICAgIHNjb3BlOiB7XG4gICAgICAgICAgICAgICAgcHNyYzogJz0nLFxuICAgICAgICAgICAgICAgIHRtdHlwZTogJz0nXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgdGVtcGxhdGU6ICc8aW1nIG5nLXNob3c9XCJzaG93KHRtdHlwZSlcIiBuZy1zcmM9XCJ7e3BzcmN9fVwiIHdpZHRoPVwiMTAwcHg7XCIgLz4nLFxuICAgICAgICAgICAgbGluazogZnVuY3Rpb24gKHNjb3BlLCBlbCwgYXR0cikge1xuICAgICAgICAgICAgICAgIHNjb3BlLnNob3cgPSBmdW5jdGlvbiAodHlwKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAoWydsJywgJ3dsJywgJ3RtbCddLmluZGV4T2YodHlwKSAhPT0gLTEpXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfSkuZGlyZWN0aXZlKCdjb2RlMmNvdW50cnknLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICByZXN0cmljdDogJ0UnLFxuICAgICAgICAgICAgc2NvcGU6IHtcbiAgICAgICAgICAgICAgICBjb2RlOiAnPSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAvLyAgICAgIHRlbXBsYXRlOiAnPGltZyBzcmM9XCJ7e2NvZGUyc3JjKGNvZGUpfX1cIiAvPicsXG4gICAgICAgICAgICB0ZW1wbGF0ZTogJzxpbWcgbmctc3JjPVwie3tjb2RlMnNyYyhjb2RlKX19XCIgLz4nLFxuICAgICAgICAgICAgbGluazogZnVuY3Rpb24gKHNjb3BlLCBlbCwgYXR0cikge1xuICAgICAgICAgICAgICAgIHNjb3BlLmNvZGUyc3JjID0gZnVuY3Rpb24gKGNvZGUpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coY29kZSlcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuICdpbWFnZXMvZmxhZ3MvJyArIGNvZGUudG9Mb3dlckNhc2UoKSArICcucG5nJ1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH0pLmRpcmVjdGl2ZSgnaW5mb1BvcG92ZXInLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICByZXN0cmljdDogJ0UnLFxuICAgICAgICAgICAgc2NvcGU6IHtcbiAgICAgICAgICAgICAgICBpbmZvOiAnQGluZm8nXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgdGVtcGxhdGU6ICcgPGltZyBuZy1zcmM9XCJpbWFnZXMvaW5mby5naWZcIiBzdHlsZT1cInBhZGRpbmctbGVmdDogM3B4O1wiIHBvcG92ZXI9XCJ7e2luZm99fVwiIHBvcG92ZXItdHJpZ2dlcj1cIm1vdXNlZW50ZXJcIiBwb3BvdmVyLXBsYWNlbWVudD1cImxlZnRcIiAgLz4nXG4gICAgICAgICAgICAvKlxuICAgICAgICAgICAgIGxpbms6IGZ1bmN0aW9uKHNjb3BlLCBlbCwgYXR0cikge1xuICAgICAgICAgICAgIGNvbnNvbGUubG9nKHNjb3BlKVxuICAgICAgICAgICAgIH0qL1xuICAgICAgICB9XG4gICAgfSkuZGlyZWN0aXZlKCdvcmRlcjEyMycsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIHJlc3RyaWN0OiAnRScsXG4gICAgICAgICAgICBzY29wZToge1xuICAgICAgICAgICAgICAgIGRldGFpbHM6ICc9J1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHRlbXBsYXRlOiAnPHA+PGgzPmRldGFpbHMxMjM8L2gzPjwvcD4nLFxuICAgICAgICAgICAgbGluazogZnVuY3Rpb24gKHNjb3BlLCBlbCwgYXR0cikge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdhc2RmYXNkZicpXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coc2NvcGUpXG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9KS5kaXJlY3RpdmUoJ29yZGVySXRlbURldGFpbHMnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICByZXN0cmljdDogJ0UnLFxuICAgICAgICAgICAgc2NvcGU6IHtcbiAgICAgICAgICAgICAgICBkZXRhaWxzOiAnPSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB0ZW1wbGF0ZVVybDogJy9wYXJ0aWFscy9vcmRlcl9pdGVtX2RldGFpbHMuaHRtbCdcbiAgICAgICAgfTtcbiAgICB9KVxuXG4gICAgLmRpcmVjdGl2ZSgnb3JkZXJJdGVtRGV0YWlsc1dpdGhOb3RlcycsIGZ1bmN0aW9uICgpIHsgIC8vIGlmIG5vdGVzIGlzIHByZXNlbnQsIGl0IGlzIGFkZGVkIGZpcnN0IGJ5IGFkbWluXG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICByZXN0cmljdDogJ0UnLFxuICAgICAgICAgICAgc2NvcGU6IHtcbiAgICAgICAgICAgICAgICBkZXRhaWxzOiAnPSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB0ZW1wbGF0ZVVybDogJy9wYXJ0aWFscy9vcmRlcl9pdGVtX2RldGFpbHMyLmh0bWwnXG4gICAgICAgIH07XG4gICAgfSlcblxuICAgIC5kaXJlY3RpdmUoJ2xvYWRpbmcnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICByZXN0cmljdDogJ0UnLFxuICAgICAgICAgICAgc2NvcGU6IHtcbiAgICAgICAgICAgICAgICB3aGVuOiAnPSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB0ZW1wbGF0ZTogJzxzcGFuIG5nLXNob3c9XCJ3aGVuXCI+PGltZyBuZy1zcmM9XCIvaW1hZ2VzL2xvYWRpbmcuZ2lmXCIgLz48L3NwYW4+J1xuICAgICAgICB9XG4gICAgfSlcblxuICAgIC5kaXJlY3RpdmUoJ2dDaGFydCcsIFsnJGxvY2F0aW9uJyxcbiAgICAgICAgZnVuY3Rpb24gKCRsb2NhdGlvbikge1xuICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICByZXN0cmljdDogJ0EnLFxuICAgICAgICAgICAgICAgIHNjb3BlOiB7XG4gICAgICAgICAgICAgICAgICAgIGdpZDogJz0nLFxuICAgICAgICAgICAgICAgICAgICBnZGF0YTogJz0nLFxuICAgICAgICAgICAgICAgICAgICBncmVhZHk6ICc9JyxcbiAgICAgICAgICAgICAgICAgICAgZHR5cGU6ICc9JyAvLyAnVicgdmlkZW9zLCAnUycgc3VibWlzc2lvbnMsICdBJyBhc3NpZ25tZW50c1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgbGluazogZnVuY3Rpb24gKHNjb3BlLCBlbG0sIGF0dHJzKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgZnVuY3Rpb24gZ2V0X2RhdGEoKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHNjb3BlLmdkYXRhKVxuICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUuZ2RhdGEuZm4udGhlbihmdW5jdGlvbiAoZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdkZDEnKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGQpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ2RkMicpXG4gICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgZnVuY3Rpb24gZHJhd1JlZ2lvbnNNYXAoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgZGF0YSA9IG5ldyBnb29nbGUudmlzdWFsaXphdGlvbi5EYXRhVGFibGUoKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgZGF0YS5hZGRDb2x1bW4oJ3N0cmluZycsICdDb3VudHJ5Jyk7XG4gICAgICAgICAgICAgICAgICAgICAgICBkYXRhLmFkZENvbHVtbignbnVtYmVyJywgJ1JlZmVyZW5jZScpO1xuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ2JiYicsIHNjb3BlLmdkYXRhKVxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gICAgVE1Db3VudHJpZXMuZ2V0KCkudGhlbihmdW5jdGlvbihjbnRzKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyAgICAgICAgICAgIGNvbnNvbGUubG9nKCdjY2MnLCBkYXRhKVxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gICAgICAgICAgZGF0YS5hZGRSb3dzKE9iamVjdC5rZXlzKGNudHMpLmxlbmd0aClcbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGEuYWRkUm93cyhPYmplY3Qua2V5cyhzY29wZS5nZGF0YSkubGVuZ3RoKVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyAgICAgICAgICAgIGNvbnNvbGUubG9nKCcnLE9iamVjdC5rZXlzKGNudHMpLmxlbmd0aCk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBrID0gMFxuICAgICAgICAgICAgICAgICAgICAgICAgZm9yICh2YXIga2V5IGluIHNjb3BlLmdkYXRhKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHNjb3BlLmdkYXRhLmhhc093blByb3BlcnR5KGtleSkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gICAgICAgICAgICAgICBjb25zb2xlLmxvZyhjbnRzW2tleV0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGEuc2V0Q2VsbChrLCAwLCBzY29wZS5nZGF0YVtrZXldLm5hbWUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYXRhLnNldENlbGwoaywgMSwgc2NvcGUuZ2RhdGFba2V5XS5wcmljZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGsgPSBrICsgMVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIG9wdGlvbnMgPSB7fTtcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vb3B0aW9uc1snY29sb3JzJ10gPSBbJyNGQURDNDEnLCAnI2M0NDk0OScsICcjZDc0YTEyJywgJyMwZTlhMGUnLCAncmVkJywgJyM3YzRiOTEnLCAnI2ZhZGM0MScsICcjMGQ2Y2NhJywgJyM3YzQ4OTcnXTtcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vb3B0aW9uc1snY29sb3JzJ10gPSBbICcjMGU5YTBlJywgJ3JlZCcsICcjN2M0YjkxJywgJyNmYWRjNDEnLCAnIzBkNmNjYScsICcjN2M0ODk3J107XG4gICAgICAgICAgICAgICAgICAgICAgICBvcHRpb25zLmNvbG9ycyA9IFsnIzAwOGJkNCcsICcjMDA4YmQ0J107XG4gICAgICAgICAgICAgICAgICAgICAgICBvcHRpb25zLmxlZ2VuZCA9ICdub25lJztcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vICAgICAgICAgIG9wdGlvbnMucmVnaW9uID0gJ0VTJ1xuXG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBjaGFydCA9IG5ldyBnb29nbGUudmlzdWFsaXphdGlvbi5HZW9DaGFydChkb2N1bWVudC5nZXRFbGVtZW50QnlJZChzY29wZS5naWQpKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgZ29vZ2xlLnZpc3VhbGl6YXRpb24uZXZlbnRzLmFkZExpc3RlbmVyKGNoYXJ0LCAncmVnaW9uQ2xpY2snLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZ1bmN0aW9uIChyZWdpb24pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUuJGFwcGx5KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRsb2NhdGlvbi5wYXRoKCcvdHJhZGVtYXJrLycgKyByZWdpb24ucmVnaW9uKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8qXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGNudHJ5ID0gc2NvcGUuZ2RhdGFbcmVnaW9uLnJlZ2lvbl1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoY250cnkgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChjbnRyeS5saW5rKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRsb2NhdGlvbi5wYXRoKCcvdHJhZGVtYXJrLycgKyBjbnRyeS5saW5rKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBlbHNlXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJGxvY2F0aW9uLnBhdGgoJy90cmFkZW1hcmsvJyArIHJlZ2lvbi5yZWdpb24pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gKi9cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLmdyZWFkeSA9IHRydWVcblxuICAgICAgICAgICAgICAgICAgICAgICAgLypcbiAgICAgICAgICAgICAgICAgICAgICAgICBnb29nbGUudmlzdWFsaXphdGlvbi5ldmVudHMuYWRkTGlzdGVuZXIoY2hhcnQsICdyZWFkeScsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZS4kYXBwbHkoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdtYXAgaXMgcmVhZHknKVxuICAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLmdyZWFkeSA9IHRydWVcbiAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgKi9cblxuICAgICAgICAgICAgICAgICAgICAgICAgY2hhcnQuZHJhdyhkYXRhLCBvcHRpb25zKTtcblxuXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgICAgIC8qXG4gICAgICAgICAgICAgICAgICAgICAgICAgZGF0YS5hZGRSb3dzKHNjb3BlLmdkYXRhLmxlbmd0aClcblxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgZm9yICh2YXIgayA9IDA7IGsgPCBzY29wZS5nZGF0YS5sZW5ndGg7IGsgKyspIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgYyA9IHNjb3BlLmdkYXRhW2tdXG4gICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coayxjLCBjLnByaWNlLCBjLmNvZGUgKVxuICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGEuc2V0Q2VsbChrLCAwLCBzY29wZS5nZGF0YVtrXS5uYW1lKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICBkYXRhLnNldENlbGwoaywgMSwgc2NvcGUuZ2RhdGFba10ucHJpY2UpO1xuICAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBvcHRpb25zID0geyB9O1xuICAgICAgICAgICAgICAgICAgICAgICAgIC8vICAgICAgICAgIG9wdGlvbnNbJ2NvbG9ycyddID0gWycjRkFEQzQxJywgJyNjNDQ5NDknLCAnI2Q3NGExMicsICcjMGU5YTBlJywgJ3JlZCcsICcjN2M0YjkxJywgJyNmYWRjNDEnLCAnIzBkNmNjYScsICcjN2M0ODk3J107XG4gICAgICAgICAgICAgICAgICAgICAgICAgLy8gICAgICAgICAgb3B0aW9uc1snY29sb3JzJ10gPSBbICcjMGU5YTBlJywgJ3JlZCcsICcjN2M0YjkxJywgJyNmYWRjNDEnLCAnIzBkNmNjYScsICcjN2M0ODk3J107XG4gICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9uc1snbGVnZW5kJ10gPSAnbm9uZSc7XG4gICAgICAgICAgICAgICAgICAgICAgICAgLy8gICAgICAgICAgb3B0aW9ucy5yZWdpb24gPSAnRVMnXG5cblxuICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBjaGFydCA9IG5ldyBnb29nbGUudmlzdWFsaXphdGlvbi5HZW9DaGFydChkb2N1bWVudC5nZXRFbGVtZW50QnlJZChzY29wZS5naWQpKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgIGdvb2dsZS52aXN1YWxpemF0aW9uLmV2ZW50cy5hZGRMaXN0ZW5lcihjaGFydCwgJ3JlZ2lvbkNsaWNrJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICBmdW5jdGlvbihyZWdpb24pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZS4kYXBwbHkoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgJGxvY2F0aW9uLnBhdGgoJy90cmFkZW1hcmsvJyArIHJlZ2lvbi5yZWdpb24pO1xuICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgZ29vZ2xlLnZpc3VhbGl6YXRpb24uZXZlbnRzLmFkZExpc3RlbmVyKGNoYXJ0LCAncmVhZHknLCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZS4kYXBwbHkoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ21hcCBpcyByZWFkeScpXG4gICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUuZ3JlYWR5ID0gdHJ1ZVxuICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICBjaGFydC5kcmF3KGRhdGEsIG9wdGlvbnMpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICovXG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICBkcmF3UmVnaW9uc01hcCgpXG5cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9O1xuICAgICAgICB9XG4gICAgXSlcblxuICAgIC8vICBzaG93IGEgbGlzdCBvZiBjb3VudHJpZXMgaW4gdGhlIHJlZ2lvblxuXG4gICAgLmRpcmVjdGl2ZSgncmVnaW9uTGlzdCcsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIHJlc3RyaWN0OiAnRScsXG4gICAgICAgICAgICBzY29wZToge1xuICAgICAgICAgICAgICAgIGNvZGU6ICdAJyxcbiAgICAgICAgICAgICAgICBjb2xzOiAnPSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB0ZW1wbGF0ZVVybDogJy9wYXJ0aWFscy9yZWdpb25saXN0Lmh0bWwnLFxuICAgICAgICAgICAgbGluazogZnVuY3Rpb24gKHNjb3BlLCBlbCwgYXR0cikge1xuXG4gICAgICAgICAgICAgICAgdmFyIGxzdCA9IGFwcGVuZF9vbmVfd29yZF9jb3VudHJ5X25hbWUodXRpbGl0aWVzLmdldF9jb3VudHJpZXNfYnlfY29udG5lbnQoc2NvcGUuY29kZSkpXG4gICAgICAgICAgICAgICAgdmFyIG51bXMgPSBNYXRoLmNlaWwobHN0Lmxlbmd0aCAvIDMpXG4gICAgICAgICAgICAgICAgc2NvcGUuY291bnRyaWVzMSA9IGxzdC5zbGljZSgwLCBudW1zKSAvLyAwIDEzXG4gICAgICAgICAgICAgICAgc2NvcGUuY291bnRyaWVzMiA9IGxzdC5zbGljZShudW1zLCAyICogbnVtcykgLy8gMTRcbiAgICAgICAgICAgICAgICBzY29wZS5jb3VudHJpZXMzID0gbHN0LnNsaWNlKDIgKiBudW1zLCAzICogbnVtcylcblxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfSlcblxuICAgIC5kaXJlY3RpdmUoJ3N0cmlwZUZvcm0nLCBbJyR3aW5kb3cnLFxuICAgICAgICBmdW5jdGlvbiAoJHdpbmRvdykge1xuXG4gICAgICAgICAgICB2YXIgZGlyZWN0aXZlID0ge1xuICAgICAgICAgICAgICAgIHJlc3RyaWN0OiAnQSdcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICBkaXJlY3RpdmUubGluayA9IGZ1bmN0aW9uIChzY29wZSwgZWxlbWVudCwgYXR0cmlidXRlcykge1xuICAgICAgICAgICAgICAgIHZhciBmb3JtID0gYW5ndWxhci5lbGVtZW50KGVsZW1lbnQpO1xuICAgICAgICAgICAgICAgIGZvcm0uYmluZCgnc3VibWl0JywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICB2YXIgYnV0dG9uID0gZm9ybS5maW5kKCdidXR0b24nKTtcbiAgICAgICAgICAgICAgICAgICAgYnV0dG9uLnByb3AoJ2Rpc2FibGVkJywgdHJ1ZSk7XG4gICAgICAgICAgICAgICAgICAgICR3aW5kb3cuU3RyaXBlLmNyZWF0ZVRva2VuKGZvcm1bMF0sIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBhcmdzID0gYXJndW1lbnRzO1xuICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUuJGFwcGx5KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZVthdHRyaWJ1dGVzLnN0cmlwZUZvcm1dLmFwcGx5KHNjb3BlLCBhcmdzKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgYnV0dG9uLnByb3AoJ2Rpc2FibGVkJywgZmFsc2UpO1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICByZXR1cm4gZGlyZWN0aXZlO1xuXG4gICAgICAgIH1cbiAgICBdKVxuXG4gICAgLmRpcmVjdGl2ZSgncGFzc3dvcmRSZXNldCcsIFsnYXV0aFNlcnZpY2UnLFxuICAgICAgICBmdW5jdGlvbiAoYXV0aFNlcnZpY2UpIHtcbiAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgcmVzdHJpY3Q6ICdBJyxcbiAgICAgICAgICAgICAgICBzY29wZToge1xuICAgICAgICAgICAgICAgICAgICBmbGFnOiAnQCdcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHRlbXBsYXRlVXJsOiAnL3BhcnRpYWxzL2ZvcmdvdF9wYXNzd29yZC5odG1sJyxcbiAgICAgICAgICAgICAgICAvL3RlbXBsYXRlVXJsOiAnL3BhcnRpYWxzL3Rlc3QxMjMuaHRtbCcsXG4gICAgICAgICAgICAgICAgLy90ZW1wbGF0ZTogXCI8aDE+aGVsbG88L2gxPlwiLFxuICAgICAgICAgICAgICAgIGxpbms6IGZ1bmN0aW9uIChzY29wZSwgZWwsIGF0dHIpIHtcblxuXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdpbiByZXNldCBwYXNzd29yZCcpXG5cbiAgICAgICAgICAgICAgICAgICAgc2NvcGUuYWxlcnRzID0gW107XG5cbiAgICAgICAgICAgICAgICAgICAgc2NvcGUuY2xvc2VBbGVydCA9IGZ1bmN0aW9uIChpbmRleCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUuYWxlcnRzLnNwbGljZShpbmRleCwgMSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBzY29wZS4kcGFyZW50LnNob3dfcmVzZXRfZm9ybSA9IGZhbHNlIC8vIHRoZSBwYXJlbnQgc2hvdWxkIHVzZSB0aGlzIGZsYWcgdG8gc2hvdy9oaWRlIHJlbGF0ZWQgZWxlbWVudHNcbiAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLmFsZXJ0cyA9IFtdO1xuXG4gICAgICAgICAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgICAgICAgICAgc2NvcGUuaW5mbyA9IHt9XG4gICAgICAgICAgICAgICAgICAgIHNjb3BlLnJlc2V0X3Bhc3N3b3JkID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ3Jlc2V0dGluZycpXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhzY29wZS5pbmZvKVxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coc2NvcGUpXG4gICAgICAgICAgICAgICAgICAgICAgICBhdXRoU2VydmljZS5yZXF1ZXN0X3Bhc3N3b3JkX3Jlc2V0KHNjb3BlLmluZm8uZW1haWwpLnRoZW4oZnVuY3Rpb24gKHJlc3VsdCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdiYWNrIHJlc2V0ICByZXN1bHQgJywgcmVzdWx0KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLmFsZXJ0cyA9IFtdXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3VsdC5zdGF0dXMpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLmFsZXJ0cy5wdXNoKHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6ICdzdWNjZXNzJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1zZzogJ1dlIGhhdmUgc2VudCBhbiBlbWFpbCB3aXRoIHBhc3N3b3JkIHJlc2V0IGluc3RydWN0aW9uLCBwbGVhc2UgY2hlY2sgeW91ciBpbmJveCdcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlbHNlXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLmFsZXJ0cy5wdXNoKHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6ICd3YXJuaW5nJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1zZzogcmVzdWx0Lm1lc3NhZ2VcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coc2NvcGUpXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgXSlcblxuICAgIC8qXG4gICAgIC8vIHRoaXMgYXNzdW1lcyB0aGUgcGFyZW50IGhhcyBhbGVydHNbXSBkZWZpbmVkXG4gICAgIC8vICRzY29wZS5hbGVydHMucHVzaCh7XG4gICAgIHR5cGU6ICd3YXJuaW5nJyxcbiAgICAgbXNnOiBcInRoaXMgaXMgYSBtZXNzYWdlMTIzXCJcbiAgICAgfSlcbiAgICAgKi9cblxuICAgIC5kaXJlY3RpdmUoJ2FsZXJ0TGluZScsIFsnJHRpbWVvdXQnLCBmdW5jdGlvbiAoJHRpbWVvdXQpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIHJlc3RyaWN0OiAnRScsXG4gICAgICAgICAgICB0ZW1wbGF0ZTogJzxhbGVydCBuZy1yZXBlYXQ9XCJhbGVydCBpbiBhbGVydHNcIiB0eXBlPVwie3thbGVydC50eXBlfX1cIiBjbG9zZT1cImNsb3NlQWxlcnQoJGluZGV4KVwiPnt7YWxlcnQubXNnfX08L2FsZXJ0PicsXG4gICAgICAgICAgICBsaW5rOiBmdW5jdGlvbiAoc2NvcGUsIGVsLCBhdHRyKSB7XG4gICAgICAgICAgICAgICAgLy8gc2NvcGUuYWxlcnRzID0gW107XG4gICAgICAgICAgICAgICAgc2NvcGUuY2xvc2VBbGVydCA9IGZ1bmN0aW9uIChpbmRleCkge1xuICAgICAgICAgICAgICAgICAgICBzY29wZS5hbGVydHMuc3BsaWNlKGluZGV4LCAxKTsgICAgLy8gYWxlcnRzIGlzIGRlZmluZWQgaW4gdGhlIHBhcmVudCBjb250cm9sbGVyXG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICAvKlxuICAgICAgICAgICAgICAgICAkdGltZW91dChmdW5jdGlvbigpe1xuICAgICAgICAgICAgICAgICAvLyAgICAgICAgc2NvcGUuYWxlcnRzLnNwbGljZSgwLCAxKVxuICAgICAgICAgICAgICAgICBzY29wZS5hbGVydHMgPSBbXVxuICAgICAgICAgICAgICAgICB9LCAzMDAwKTsgLy8gbWF5YmUgJ30sIDMwMDAsIGZhbHNlKTsnIHRvIGF2b2lkIGNhbGxpbmcgYXBwbHkqL1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfV0pXG5cbiAgICAuZGlyZWN0aXZlKCd0aWNrZXInLCBbJyR0aW1lb3V0JywgZnVuY3Rpb24gKCR0aW1lb3V0KSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAvLyBSZXN0cmljdCBpdCB0byBiZSBhbiBhdHRyaWJ1dGUgaW4gdGhpcyBjYXNlXG4gICAgICAgICAgICByZXN0cmljdDogJ0EnLFxuICAgICAgICAgICAgLy8gcmVzcG9uc2libGUgZm9yIHJlZ2lzdGVyaW5nIERPTSBsaXN0ZW5lcnMgYXMgd2VsbCBhcyB1cGRhdGluZyB0aGUgRE9NXG4gICAgICAgICAgICBsaW5rOiBmdW5jdGlvbiAoc2NvcGUsIGVsZW1lbnQsIGF0dHJzKSB7XG4gICAgICAgICAgICAgICAgLy8kKGVsZW1lbnQpLnRvb2xiYXIoc2NvcGUuJGV2YWwoYXR0cnMudG9vbGJhclRpcCkpO1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCd0aWNrZXIyJylcbiAgICAgICAgICAgICAgICAvL2pRdWVyeSgnI3dlYnRpY2tlcicpLndlYlRpY2tlcigpXG4gICAgICAgICAgICAgICAgalF1ZXJ5KCcjbmV3cycpLmVhc3lUaWNrZXIoe1xuICAgICAgICAgICAgICAgICAgICBkaXJlY3Rpb246ICd1cCcsXG4gICAgICAgICAgICAgICAgICAgIGVhc2luZzogJ3N3aW5nJyxcbiAgICAgICAgICAgICAgICAgICAgc3BlZWQ6ICdzbG93JyxcbiAgICAgICAgICAgICAgICAgICAgaW50ZXJ2YWw6IDMwMDAsXG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogJ2F1dG8nLFxuICAgICAgICAgICAgICAgICAgICB2aXNpYmxlOiAyLFxuICAgICAgICAgICAgICAgICAgICBtb3VzZVBhdXNlOiAxLFxuICAgICAgICAgICAgICAgICAgICBjb250cm9sczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgdXA6ICcnLFxuICAgICAgICAgICAgICAgICAgICAgICAgZG93bjogJycsXG4gICAgICAgICAgICAgICAgICAgICAgICB0b2dnbGU6ICcnLFxuICAgICAgICAgICAgICAgICAgICAgICAgcGxheVRleHQ6ICdQbGF5JyxcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0b3BUZXh0OiAnU3RvcCdcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgIH1dKVxuXG4gICAgLmRpcmVjdGl2ZSgnY292ZXJmbG93JywgWyckbG9jYXRpb24nLCBmdW5jdGlvbiAoJGxvY2F0aW9uKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAvLyBSZXN0cmljdCBpdCB0byBiZSBhbiBhdHRyaWJ1dGUgaW4gdGhpcyBjYXNlXG4gICAgICAgICAgICByZXN0cmljdDogJ0EnLFxuICAgICAgICAgICAgLy8gcmVzcG9uc2libGUgZm9yIHJlZ2lzdGVyaW5nIERPTSBsaXN0ZW5lcnMgYXMgd2VsbCBhcyB1cGRhdGluZyB0aGUgRE9NXG4gICAgICAgICAgICBsaW5rOiBmdW5jdGlvbiAoc2NvcGUsIGVsZW1lbnQsIGF0dHJzKSB7XG5cbiAgICAgICAgICAgICAgICBzY29wZS5nb3RvX3RyYWRlbWFyayA9IGZ1bmN0aW9uIChwYXRoKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdhc2RmYXNkZicsIHBhdGgpXG4gICAgICAgICAgICAgICAgICAgICRsb2NhdGlvbi5wYXRoKHBhdGgpXG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgc2NvcGUubXlwYXRoID0gJ2JiYmJiJ1xuXG4gICAgICAgICAgICAgICAgalF1ZXJ5KCcjcHJldmlldy1jb3ZlcmZsb3cnKS5jb3ZlcmZsb3coXG4gICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGluZGV4OiAzLFxuICAgICAgICAgICAgICAgICAgICAgICAgaW5uZXJBbmdsZTogLTU1LFxuICAgICAgICAgICAgICAgICAgICAgICAgaW5uZXJPZmZzZXQ6IDksXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25maXJtOiBmdW5jdGlvbiAoZXZlbnQsIGNvdmVyLCBpbmRleCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdjb25maXJtZWQnLCBjb3ZlcilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZS4kYXBwbHkoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyRsb2NhdGlvbi5wYXRoKCcvdHJhZGVtYXJrL1VTJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBpbWcgPSBqUXVlcnkoY292ZXIpLmNoaWxkcmVuKCkuYW5kU2VsZigpLmZpbHRlcignaW1nJykubGFzdCgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnaW1nJywgaW1nLmRhdGEoJ3JlZnNyYycpKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkbG9jYXRpb24ucGF0aChpbWcuZGF0YSgncmVmc3JjJykpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLypcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGltZyA9IGpRdWVyeShjb3ZlcikuY2hpbGRyZW4oKS5hbmRTZWxmKCkuZmlsdGVyKCdpbWcnKS5sYXN0KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdpbWcnLCBpbWcuZGF0YSgncmVmc3JjJykpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICRsb2NhdGlvbi5wYXRoKCcvdHJhZGVtYXJrLycpOyovXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgc2VsZWN0OiBmdW5jdGlvbiAoZXZlbnQsIGNvdmVyKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGltZyA9IGpRdWVyeShjb3ZlcikuY2hpbGRyZW4oKS5hbmRTZWxmKCkuZmlsdGVyKCdpbWcnKS5sYXN0KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgalF1ZXJ5KCcjcGhvdG9zLW5hbWUnKS50ZXh0KCdSZWdpc3RlciAnICsgaW1nLmRhdGEoJ25hbWUnKSB8fCAndW5rbm93bicpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLm15cGF0aCA9IGltZy5kYXRhKCdyZWZzcmMnKVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG5cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcbiAgICB9XSlcblxuICAgIC5kaXJlY3RpdmUoJ25nRW50ZXInLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiBmdW5jdGlvbiAoc2NvcGUsIGVsZW1lbnQsIGF0dHJzKSB7XG4gICAgICAgICAgICBlbGVtZW50LmJpbmQoXCJrZXlkb3duIGtleXByZXNzXCIsIGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgICAgIGlmIChldmVudC53aGljaCA9PT0gMTMpIHtcbiAgICAgICAgICAgICAgICAgICAgc2NvcGUuJGFwcGx5KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLiRldmFsKGF0dHJzLm5nRW50ZXIpO1xuICAgICAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuICAgIH0pXG5cbiAgICAuZGlyZWN0aXZlKCd1c2VyTGluaycsIFsnJGh0dHAnLCBmdW5jdGlvbiAoJGh0dHApIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIHJlc3RyaWN0OiAnRScsXG4gICAgICAgICAgICB0ZW1wbGF0ZTogJzxzcGFuIGNsYXNzPVwiZ2x5cGhpY29uIGdseXBoaWNvbi11c2VyXCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9zcGFuPiZuYnNwOyZuYnNwOzxhIG5nLWhyZWY9XCJhY2NvdW50L3t7YWNjb3VudC5faWR9fVwiPnt7YWNjb3VudC5uYW1lfX08L2E+JyxcbiAgICAgICAgICAgIGxpbms6IGZ1bmN0aW9uIChzY29wZSwgZWwsIGF0dHJzKSB7XG4gICAgICAgICAgICAgICAgYXR0cnMuJG9ic2VydmUoJ3VzcmlkJywgZnVuY3Rpb24gKHZhbHVlKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmICh2YWx1ZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgJGh0dHAuZ2V0KCcvYWRtaW5fYXBpL2FjY291bnQvJyArIHZhbHVlKS50aGVuKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUuYWNjb3VudCA9IGRhdGEuZGF0YVxuXG4gICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XSlcblxuICAgIC5kaXJlY3RpdmUoJ3Byb2ZpbGVMaW5rJywgWyckaHR0cCcsIGZ1bmN0aW9uICgkaHR0cCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgcmVzdHJpY3Q6ICdFJyxcbiAgICAgICAgICAgIHNjb3BlOiB7XG4gICAgICAgICAgICAgICAgcHJvZmlsZV9pZDogJz0nXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgdGVtcGxhdGU6ICc8c3BhbiBjbGFzcz1cImdseXBoaWNvbiBnbHlwaGljb24tZmlsZVwiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvc3Bhbj4mbmJzcDs8YSBuZy1ocmVmPVwicHJvZmlsZS97e2FjY291bnQyLl9pZH19XCI+e3thY2NvdW50Mi5wcm9maWxlX25hbWV9fTwvYT4nLFxuICAgICAgICAgICAgbGluazogZnVuY3Rpb24gKHNjb3BlLCBlbCwgYXR0cnMpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnc2NwMicsIHNjb3BlKVxuICAgICAgICAgICAgICAgIGF0dHJzLiRvYnNlcnZlKCdwcm9maWxlSWQnLCBmdW5jdGlvbiAodmFsdWUpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ3Byb2ZpbGUgdmFsdWUnLCB2YWx1ZSlcbiAgICAgICAgICAgICAgICAgICAgaWYgKHZhbHVlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkaHR0cC5nZXQoJy9hZG1pbl9hcGkvcHJvZmlsZS8nICsgdmFsdWUpLnRoZW4oZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZS5hY2NvdW50MiA9IGRhdGEuZGF0YVxuXG4gICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1dKVxuXG4gICAgLmRpcmVjdGl2ZSgncHJvZmlsZU5hbWUnLCBbJyRodHRwJywgJ2F1dGhTZXJ2aWNlJywgZnVuY3Rpb24gKCRodHRwLCBhdXRoU2VydmljZSkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgcmVzdHJpY3Q6ICdFJyxcbiAgICAgICAgICAgIHNjb3BlOiB7XG4gICAgICAgICAgICAgICAgcHJvZmlsZV9pZDogJz0nXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgdGVtcGxhdGU6ICc8YT57e2FjY291bnQyLnByb2ZpbGVfbmFtZX19PC9hPicsXG4gICAgICAgICAgICBsaW5rOiBmdW5jdGlvbiAoc2NvcGUsIGVsLCBhdHRycykge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdzY3AyJywgc2NvcGUpXG4gICAgICAgICAgICAgICAgYXR0cnMuJG9ic2VydmUoJ3Byb2ZpbGVJZCcsIGZ1bmN0aW9uICh2YWx1ZSkge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygncHJvZmlsZSB2YWx1ZSBpbiBwcm9maWxlTmFtZSAnLCB2YWx1ZSlcbiAgICAgICAgICAgICAgICAgICAgaWYgKGF1dGhTZXJ2aWNlLmlzU2lnbmVkSW4oKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHZhbHVlKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkaHR0cC5nZXQoJy9hcGkvcHJvZmlsZS8nICsgdmFsdWUpLnRoZW4oZnVuY3Rpb24gc3VjY2VzcyhkYXRhKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLmFjY291bnQyID0gZGF0YS5kYXRhXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgZnVuY3Rpb24gZXJyb3IoZXJyb3IpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ3dlIGdvdCBhbiBlcnJvcicpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycm9yKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG5cbiAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICB9XG5cblxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfV0pLmRpcmVjdGl2ZSgnanNvbmxkJywgWyckZmlsdGVyJywgJyRzY2UnLCBmdW5jdGlvbiAoJGZpbHRlciwgJHNjZSkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgcmVzdHJpY3Q6ICdFJyxcbiAgICAgICAgICAgIHRlbXBsYXRlOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuICc8c2NyaXB0IHR5cGU9XCJhcHBsaWNhdGlvbi9sZCtqc29uXCIgbmctYmluZC1odG1sPVwib25HZXRKc29uKClcIj48L3NjcmlwdD4nO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHNjb3BlOiB7XG4gICAgICAgICAgICAgICAganNvbjogJz1qc29uJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGxpbms6IGZ1bmN0aW9uIChzY29wZSwgZWxlbWVudCwgYXR0cnMpIHtcbiAgICAgICAgICAgICAgICBzY29wZS5vbkdldEpzb24gPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAkc2NlLnRydXN0QXNIdG1sKCRmaWx0ZXIoJ2pzb24nKShzY29wZS5qc29uKSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHJlcGxhY2U6IHRydWVcbiAgICAgICAgfTtcbiAgICB9XSk7XG5cblxuIiwiXCJ1c2Ugc3RyaWN0XCI7XG4vKiBnbG9iYWwgYW5ndWxhciwgalF1ZXJ5ICovXG4vKiBEaXJlY3RpdmVzICovXG5cbnZhciB1dGlsaXRpZXMgPSByZXF1aXJlKCcuLi9tb2R1bGVzL3V0aWxpdGllcy5qcycpXG5cbmFuZ3VsYXIubW9kdWxlKCd0a3MuZGlyZWN0aXZlczInLCBbXSlcblxuICAgIC5kaXJlY3RpdmUoJ3RyYWRlbWFya1R5cGUnLCBbJyRodHRwJywgZnVuY3Rpb24gKCRodHRwKSB7XG5cbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIHJlc3RyaWN0OiAnRScsXG4gICAgICAgICAgICBzY29wZToge1xuICAgICAgICAgICAgICAgIHJlZzogJz0nLFxuICAgICAgICAgICAgICAgIG9wdG5zIDogJz0nXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgdGVtcGxhdGVVcmw6ICcvcGFydGlhbHMvdHJhZGVtYXJrX3R5cGUuaHRtbCcsXG4gICAgICAgICAgICBsaW5rOiBmdW5jdGlvbiAoc2NvcGUsIGVsLCBhdHRycykge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdzY3AyJywgc2NvcGUpXG5cbiAgICAgICAgICAgICAgICBzY29wZS51cGxvYWRGaWxlID0gZnVuY3Rpb24gKGZpbGVzKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ3VwbG9hZCBmaWxlJylcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZmlsZXMpXG4gICAgICAgICAgICAgICAgICAgIGxldCBmZCA9IG5ldyBGb3JtRGF0YSgpO1xuXG4gICAgICAgICAgICAgICAgICAgIC8vVGFrZSB0aGUgZmlyc3Qgc2VsZWN0ZWQgZmlsZVxuICAgICAgICAgICAgICAgICAgICBmZC5hcHBlbmQoXCJsb2dvX2ZpbGVcIiwgZmlsZXNbMF0pO1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnZmQnLCBmZClcblxuICAgICAgICAgICAgICAgICAgICAkaHR0cC5wb3N0KCcvYXBpMi91cGxvYWRfZmlsZScsIGZkLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICB3aXRoQ3JlZGVudGlhbHM6IHRydWUsXG4gICAgICAgICAgICAgICAgICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJ0NvbnRlbnQtVHlwZSc6IHVuZGVmaW5lZFxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgIHRyYW5zZm9ybVJlcXVlc3Q6IGFuZ3VsYXIuaWRlbnRpdHlcbiAgICAgICAgICAgICAgICAgICAgfSkuc3VjY2VzcyhmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUucmVnLmxvZ29fdXJsID0gZGF0YS51cmxcbiAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLnVwbG9hZF9zdGF0dXMgPSAnVXBsb2FkZWQnXG4gICAgICAgICAgICAgICAgICAgIH0pLmVycm9yKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnZmFpbGVkJywgZGF0YSlcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XSlcblxuICAgIC5kaXJlY3RpdmUoJ3RyYWRlbWFya0NsYXNzZXMnLCBbJyRodHRwJywgZnVuY3Rpb24gKCRodHRwKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICByZXN0cmljdDogJ0UnLFxuICAgICAgICAgICAgc2NvcGU6IHtcbiAgICAgICAgICAgICAgICByZWc6ICc9JyxcbiAgICAgICAgICAgICAgICBvcHRucyA6ICc9J1xuXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgdGVtcGxhdGVVcmw6ICcvcGFydGlhbHMvdHJhZGVtYXJrX2NsYXNzZXMuaHRtbCcsXG4gICAgICAgICAgICBsaW5rOiBmdW5jdGlvbiAoc2NvcGUsIGVsLCBhdHRycykge1xuXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ3NjcDInLCBzY29wZS5vcHRucylcblxuXG4gICAgICAgICAgICAgICAgc2NvcGUubGFiZWxzID0ge1xuICAgICAgICAgICAgICAgICAgICAvL2NsYXNzX3NlbGVjdGlvbiA6IFwiU2VsZWN0IHRoZSBjbGFzc2VzIGluIHdoaWNoIHlvdSBuZWVkIHRvIHJlZ2lzdGVyIHlvdXIgVHJhZGVtYXJrOlwiXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzX3NlbGVjdGlvbiA6IFwiU2VsZWN0IHRoZSBjbGFzc2VzIHdoaWNoIHlvdSB3b3VsZCBsaWtlIHlvdXIgdHJhZGVtYXJrIG1vbml0b3JlZCBmb3I6XCJcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAvKlxuICAgICAgICAgICAgICAgIHNjb3BlLm9wdGlvbnMgPSB7XG4gICAgICAgICAgICAgICAgICAgIGVuYWJsZV9jbGFzc19kZXRhaWwgOiBmYWxzZVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAqL1xuXG5cbiAgICAgICAgICAgICAgICBzY29wZS5jbGFzc2VzX3NlbGVjdGVkID0gW11cblxuICAgICAgICAgICAgICAgIC8vIGdlbmVyYXRlIGFuIGFycmF5IGZvciBVSVxuICAgICAgICAgICAgICAgIGZvciAodmFyIHggPSAxOyB4IDw9IDQ1OyB4KyspIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGMgPSB4LnRvU3RyaW5nKClcbiAgICAgICAgICAgICAgICAgICAgaWYgKGMubGVuZ3RoID09PSAxKVxuICAgICAgICAgICAgICAgICAgICAgICAgYyA9ICcwJyArIGNcbiAgICAgICAgICAgICAgICAgICAgc2NvcGUuY2xhc3Nlc19zZWxlY3RlZC5wdXNoKHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvZGU6IGMsXG4gICAgICAgICAgICAgICAgICAgICAgICBzZWxlY3RlZDogZmFsc2VcbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBjbGFzc2VzMmFycmF5KHNjb3BlLnJlZy5jbGFzc2VzKVxuXG4gICAgICAgICAgICAgICAgc2NvcGUuY2xhc3NfY2hhbmdlID0gZnVuY3Rpb24gKHMpIHtcblxuICAgICAgICAgICAgICAgICAgICBpZiAocy5zZWxlY3RlZCkge1xuXG4gICAgICAgICAgICAgICAgICAgICAgICBzY29wZS5yZWcuY2xhc3Nlcy5wdXNoKHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBzLmNvZGUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGV0YWlsczogJydcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXG5cbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBzY29wZS5yZWcuY2xhc3Nlcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzY29wZS5yZWcuY2xhc3Nlc1tpXS5uYW1lID09PSBzLmNvZGUpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLnJlZy5jbGFzc2VzLnNwbGljZShpLCAxKVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBmdW5jdGlvbiBjbGFzc2VzMmFycmF5KGNsYXNzZXMpIHtcblxuICAgICAgICAgICAgICAgICAgICBmb3IgKHZhciBrID0gMDsgayA8IHNjb3BlLmNsYXNzZXNfc2VsZWN0ZWQubGVuZ3RoOyBrKyspXG4gICAgICAgICAgICAgICAgICAgICAgICBzY29wZS5jbGFzc2VzX3NlbGVjdGVkW2tdLnNlbGVjdGVkID0gZmFsc2VcblxuICAgICAgICAgICAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGNsYXNzZXMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBpbmR4ID0gcGFyc2VJbnQoY2xhc3Nlc1tpXS5uYW1lKVxuICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUuY2xhc3Nlc19zZWxlY3RlZFtpbmR4IC0gMV0uc2VsZWN0ZWQgPSB0cnVlXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1dKVxuXG5cblxuIiwiJ3VzZSBzdHJpY3QnO1xuLypnbG9iYWwgYW5ndWxhciwgZHVtcCwgbW9tZW50LCBpbywgY29uc29sZSAqL1xuXG4vKiBGaWx0ZXJzICovXG5cbmFuZ3VsYXIubW9kdWxlKCd0a3MuZmlsdGVycycsIFtdKVxuIC5maWx0ZXIoJ3JldmVyc2UnLCBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uIChpbnB1dCwgdXBwZXJjYXNlKSB7XG4gICAgICB2YXIgb3V0ID0gXCJcIjtcbiAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgaW5wdXQubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgb3V0ID0gaW5wdXQuY2hhckF0KGkpICsgb3V0O1xuICAgICAgfVxuICAgICAgLy8gY29uZGl0aW9uYWwgYmFzZWQgb24gb3B0aW9uYWwgYXJndW1lbnRcbiAgICAgIGR1bXAoaW5wdXQpXG4gICAgICBpZiAodXBwZXJjYXNlKVxuICAgICAgICBvdXQgPSBvdXQudG9VcHBlckNhc2UoKTtcblxuICAgICAgZHVtcChvdXQpXG4gICAgICByZXR1cm4gb3V0O1xuICAgIH07XG4gIH0pLmZpbHRlcigndG1fY2xhc3NlcycsIGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gZnVuY3Rpb24gKGlucHV0KSB7XG4gICAgICBpZiAodHlwZW9mKGlucHV0KSA9PT0gJ29iamVjdCcpIHtcbiAgICAgICAgdmFyIHJldCA9ICcnXG4gICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgaW5wdXQubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICBpZiAocmV0ID09PSAnJykge1xuICAgICAgICAgICAgaWYgKGlucHV0W2ldLmRldGFpbHMpXG4gICAgICAgICAgICAgIHJldCA9IGlucHV0W2ldLm5hbWUgKyAnICgnICsgaW5wdXRbaV0uZGV0YWlscyArICcpJ1xuICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAgICByZXQgPSBpbnB1dFtpXS5uYW1lXG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgaWYgKGlucHV0W2ldLmRldGFpbHMpXG4gICAgICAgICAgICAgIHJldCA9IHJldCArICctJyArIGlucHV0W2ldLm5hbWUgKyAnICgnICsgaW5wdXRbaV0uZGV0YWlscyArICcpJ1xuICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAgICByZXQgPSByZXQgKyAnLScgKyBpbnB1dFtpXS5uYW1lXG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiByZXRcbiAgICAgIH0gZWxzZVxuICAgICAgICByZXR1cm4gJ24vYSdcbiAgICB9XG4gIH0pXG5cbiAgLy8gc2hvdyBhIGNsYXNzZXMgaW4gYSBsaW5lIHdpdGhvdXQgdGV4dFxuICAuZmlsdGVyKCd0bV9jbGFzc2VzU2ltcGxlJywgZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiBmdW5jdGlvbiAoaW5wdXQpIHtcbiAgICAgIGlmICh0eXBlb2YoaW5wdXQpID09PSAnb2JqZWN0Jykge1xuICAgICAgICB2YXIgcmV0ID0gJydcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBpbnB1dC5sZW5ndGg7IGkrKykge1xuICAgICAgICAgIGlmIChyZXQgPT09ICcnKSB7XG4gICAgICAgICAgICAgIHJldCA9IGlucHV0W2ldLm5hbWVcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIHJldCA9IHJldCArICctJyArIGlucHV0W2ldLm5hbWVcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHJldFxuICAgICAgfSBlbHNlXG4gICAgICAgIHJldHVybiAnbi9hJ1xuICAgIH1cbiAgfSlcblxuXG4gIC5maWx0ZXIoJ2Jvb2wydGV4dCcsIGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gZnVuY3Rpb24gKGJvb2wpIHtcbiAgICAgIGlmIChib29sKVxuICAgICAgICByZXR1cm4gJ1llcydcbiAgICAgIGVsc2VcbiAgICAgICAgcmV0dXJuICdObydcbiAgICB9XG4gIH0pLmZpbHRlcigncmVndHlwZTJ0ZXh0JywgZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiBmdW5jdGlvbiAodCkge1xuICAgICAgaWYgKHQgPT09ICd3JylcbiAgICAgICAgcmV0dXJuICdXb3JkbWFyaydcbiAgICAgIGVsc2UgaWYgKHQgPT09ICdsJylcbiAgICAgICAgcmV0dXJuICdMb2dvJ1xuICAgICAgZWxzZSBpZiAodCA9PT0gJ3dsJylcbiAgICAgICAgcmV0dXJuICdXb3JkbWFyayBhbmQgbG9nbydcbiAgICAgIGVsc2UgaWYgKHQgPT09ICd0bScpXG4gICAgICAgIHJldHVybiAnVHJhZGVtYXJrJ1xuICAgICAgZWxzZSBpZiAodCA9PT0gJ3RtbCcpXG4gICAgICAgIHJldHVybiAnVHJhZGVtYXJrIHdpdGggbG9nbydcbiAgICAgIGVsc2VcbiAgICAgICAgcmV0dXJuICdVbmtub3duJ1xuICAgIH1cbiAgfSkuZmlsdGVyKCdwb2EydGV4dCcsIFsnUE9BX0lORk8nLGZ1bmN0aW9uIChQT0FfSU5GTykge1xuICAgIHJldHVybiBmdW5jdGlvbiAocCwgY291bnRyeSkge1xuICAgICAgaWYgKHAgPT09IC0xKVxuICAgICAgICByZXR1cm4gJ1RoaXMgYXBwbGljYXRpb24gZG9lcyBub3QgcmVxdWlyZSBhIFBvd2VyIG9mIEF0dG9ybmV5LidcbiAgICAgIGVsc2UgaWYgKHAgPT09IDIpXG4gICAgICAgIHJldHVybiAnQSBwb3dlciBvZiBhdHRvcm5leSBpcyByZXF1aXJlZCBiZWZvcmUgcmVnaXN0cmF0aW9uIGNhbiBzdGFydC4nXG4gICAgICBlbHNlIHtcbiAgICAgICAgdmFyIGxhYmVsID0gUE9BX0lORk9bY291bnRyeV1cbiAgICAgICAgaWYgKGxhYmVsICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICByZXR1cm4gbGFiZWxbcF0gLy9UT0RPIGhhbmRsZSBpbnZhbGlkIFBcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICByZXR1cm4gJ05vIFBPQSBsYWJlbCBmb3VuZCBmb3IgJyArIHA7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1dKS5maWx0ZXIoJ2NvdW50cnkydGV4dCcsIFsnY291bnRyaWVzJyxmdW5jdGlvbiAoY291bnRyaWVzKSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uIChwKSB7XG4gICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGNvdW50cmllcy5sZW5ndGg7IGkgKyspIHtcbiAgICAgICAgaWYgKGNvdW50cmllc1tpXS5jb2RlID09PSBwKVxuICAgICAgICAgIHJldHVybiBjb3VudHJpZXNbaV0ubmFtZVxuICAgICAgfVxuXG4gICAgfVxuICB9XSkuZmlsdGVyKCdvcmRlclN0YXR1cycsIGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gZnVuY3Rpb24gKHApIHtcblxuICAgICAgaWYgKHAgPT09IDEpXG4gICAgICAgIHJldHVybiAnUEVORElORydcbiAgICAgIGVsc2UgaWYgKHAgPT09IDIpXG4gICAgICAgIHJldHVybiAnQ29tcGxldGVkJ1xuICAgICAgZWxzZSBpZiAocCA9PT0gMylcbiAgICAgICAgcmV0dXJuICdDYW5jZWxsZWQnXG4gICAgICBlbHNlXG4gICAgICAgIHJldHVybiAnVW5rbm93bidcbiAgICB9XG4gIH0pXG5cbiAgLmZpbHRlcigndGlja2V0U3RhdHVzJywgZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiBmdW5jdGlvbiAocCkge1xuXG4gICAgICBpZiAocCA9PT0gMClcbiAgICAgICAgcmV0dXJuICdBQ1RJVkUnXG4gICAgICBlbHNlIGlmIChwID09PSAxKVxuICAgICAgICByZXR1cm4gJ0NMT1NFRCdcbiAgICAgIGVsc2UgaWYgKHAgPT09IDIpXG4gICAgICAgIHJldHVybiAnQ2FuY2VsbGVkJ1xuICAgICAgZWxzZVxuICAgICAgICByZXR1cm4gJ1Vua25vd24nXG4gICAgfVxuICB9KS5cblxuICBmaWx0ZXIoJ2NvZGUyZmxhZ3NyYycsIGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gZnVuY3Rpb24gKGNvZGUpIHtcbiAgICAgIGlmIChjb2RlICE9PSB1bmRlZmluZWQpIHtcblxuICAgICAgICBjb25zb2xlLmxvZyhjb2RlLCB0eXBlb2YgY29kZSlcbiAgICAgICAgcmV0dXJuICdpbWFnZXMvZmxhZ3MvJyArIGNvZGUudG9Mb3dlckNhc2UoKSArICcucG5nJ1xuICAgICAgfSAvKmVsc2VcbiAgICAgICAgcmV0dXJuICdpbWFnZXMvJyovXG4gICAgfVxuICB9KS5maWx0ZXIoJ2NvbW1lcmNlX3VzZScsIFsnJGZpbHRlcicsZnVuY3Rpb24gKCRmaWx0ZXIpIHtcbiAgICByZXR1cm4gZnVuY3Rpb24gKGNvZGUsIHVzZV9kYXRlKSB7XG4gICAgICBpZiAoY29kZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgaWYgKGNvZGUgPT09IC0xKVxuICAgICAgICAgICAgcmV0dXJuICdub3QgYXBwbGljYWJsZSdcbiAgICAgICAgICBlbHNlIGlmIChjb2RlID09PSAwKVxuICAgICAgICAgICAgcmV0dXJuICdObydcbiAgICAgICAgICBlbHNlIGlmIChjb2RlID09PSAxKVxuICAgICAgICAgICAgcmV0dXJuICdZZXMgKCBzdGFydGluZyBkYXRlIDogJyArICRmaWx0ZXIoJ2RhdGUnKSh1c2VfZGF0ZSwgJ21lZGl1bURhdGUnKSArJyknXG4gICAgICAgICAgZWxzZSBpZiAoY29kZSA9PT0gMilcbiAgICAgICAgICAgIHJldHVybiAnTm8sIGJ1dCByZWdpc3RlcmVkIGFicm9hZCdcbiAgICAgICAgICBlbHNlXG4gICAgICAgICAgICByZXR1cm4gJ3Vua25vdyBjb2RlIDogJyArIGNvZGVcbiAgICAgIH0gZWxzZVxuICAgICAgICByZXR1cm4gJ3Vua25vd24nXG4gICAgfVxuICB9XSkuZmlsdGVyKCdzdHVkeV90eXBlMnRleHQnLCBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uICh0KSB7XG4gICAgICBpZiAodCA9PT0gXCIwXCIpXG4gICAgICAgIHJldHVybiAnQmFzaWMnXG4gICAgICBlbHNlIGlmICh0ID09PSBcIjFcIilcbiAgICAgICAgcmV0dXJuICdFeHRlbnNpdmUnXG4gICAgICBlbHNlIGlmICh0ID09PSAtMSlcbiAgICAgICAgcmV0dXJuICcnXG4gICAgICBlbHNlXG4gICAgICAgIHJldHVybiAnVW5rbm93bidcbiAgICB9XG4gIH0pLmZpbHRlcignYWRtaW5fdHlwZScsIGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gZnVuY3Rpb24gKHQpIHtcbiAgICAgIGlmICh0ICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgaWYgKHQgPT09ICdBJylcbiAgICAgICAgICByZXR1cm4gJ0FkbWluJ1xuICAgICAgICBlbHNlIGlmICh0ID09PSAnUycpXG4gICAgICAgICAgcmV0dXJuICdTdGFmZidcbiAgICAgIH1cbiAgICB9XG4gIH0pXG4gIC5maWx0ZXIoJ2F1dGhvcnMydGV4dCcsIGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gZnVuY3Rpb24gKHQpIHtcbiAgICAgIGlmICh0ICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgdmFyIHJldFxuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHQubGVuZ3RoOyBpICsrKSB7XG4gICAgICAgICAgaWYgKHJldClcbiAgICAgICAgICAgIHJldCA9IHJldCArICcsJyArIHRbaV0ubmFtZVxuICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgIHJldCA9IHRbaV0ubmFtZVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiByZXRcblxuICAgICAgfVxuICAgIH1cbiAgfSkuZmlsdGVyKCdhbXRfaW5fY2FzZScsIGZ1bmN0aW9uICgpIHtcbiAgICAgIHJldHVybiBmdW5jdGlvbiAodCkge1xuICAgICAgICBpZiAodCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICByZXR1cm4gJyQgJyArIHRcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0pXG5cblxuXG5cblxuIiwiXCJ1c2Ugc3RyaWN0XCI7XG4vKiBnbG9iYWwgYW5ndWxhciAqL1xudmFyIHNlcnZpY2VzMiA9IGFuZ3VsYXIubW9kdWxlKCd0a3Muc2VydmljZXMyJywgW10pLnZhbHVlKCd2ZXJzaW9uJywgJzAuMScpO1xuXG5zZXJ2aWNlczIuY29uc3RhbnQoJ1NUQVRJQ19JTkZPJywge1xuXG4gIHJlcXVpcmVtZW50czogJ0luIG9yZGVyIHRvIGFwcGx5IGZvciBhIFRyYWRlbWFyayBpbiBVU0EgeW91IG5lZWQgdG8gcHJvdmlkZSA8YnIvPicgK1xuICAgICcxLiBJbmZvcm1hdGlvbiB0aHJvdWdoIG91ciBUcmFkZW1hcmsgUmVnaXN0cmF0aW9uIE9yZGVyIEZvcm0sIGFuZCB5b3UgbmVlZCB0bzxici8+ICcgK1xuICAgICcyLiBEZW1vbnN0cmF0ZSB1c2Ugb2YgdGhlIFRyYWRlbWFyay4gTm8gUG93ZXIgb2YgQXR0b3JuZXkgaXMgbmVlZGVkIGZvciBVU0EuPGJyLz4gTW9yZSB0byBjb21lJyxcbiAgcHJpY2VzOiAnUHJpY2VzIHRvIGJlIHByb3ZpZGVkIGxhdGVyLi4uJyxcbiAgcHJpb3JpdHlfaGVscDonQ2xhaW1pbmcgcHJpb3JpdHkgaXMgcGFydCBvZiB0aGUgUGFyaXMgQ29udmVudGlvbiB0cmVhdHkuJyArXG4gICAgJ0l0IG1lYW5zIHRoYXQgaWYgeW91IGhhdmUgZmlsZWQgYSB0cmFkZW1hcmsgd2l0aGluIHRoZSBsYXN0IDYgbW9udGhzIGluIGFueSBvZiB0aGUgc2lnbmF0b3J5IGNvdW50cmllcyAobW9zdCBjb3VudHJpZXMgb2YgdGhlIHdvcmxkKSwgJyArXG4gICAgJ3lvdSBjYW4gdXNlIHRoZSBmaWxpbmcgZGF0ZSBvZiB0aGlzIGZpcnN0IHJlZ2lzdHJhdGlvbiBhcyB0aGUgZmlsaW5nIGRhdGUgaW4gdGhlIG90aGVyIHNpZ25hdG9yeSBjb3VudHJpZXMuICBUaGlzIHJpZ2h0IG1heSBiZSB1c2VmdWwgJyArXG4gICAgJ3RvIHRoZSBvd25lciBvZiB0aGUgdHJhZGVtYXJrLCB0byBwcm90ZWN0IGhpcyBpbnRlbGxlY3R1YWwgcHJvcGVydHksIGluIGNhc2VzIG9mIGxlZ2FsIGNvbmZsaWN0cyB3aXRoIG90aGVyIHBhcnRpZXMuJyxcbiAgY2xhc3NfaGVscCA6ICdXaGVuIHJlZ2lzdGVyaW5nIGEgdHJhZGVtYXJrIHlvdSBuZWVkIHRvIHNwZWNpZnkgdGhlIHByb2R1Y3RzIGFuZCBzZXJ2aWNlcyB0aGF0IHdpbGwgYmUgYXNzb2NpYXRlZCB3aXRoIHlvdXIgdHJhZGVtYXJrLicgK1xuICAgICcgVGhlIGxhcmdlIG1ham9yaXR5IG9mIGNvdW50cmllcyBvZiB0aGUgd29ybGQgaGF2ZSBhZG9wdGVkIHRoZSBJbnRlcm5hdGlvbmFsIENsYXNzaWZpY2F0aW9uIG9mIE5pY2UuIFRoaXMgc3lzdGVtIGdyb3VwcyBhbGwgcHJvZHVjdHMgYW5kICcgK1xuICAgICdzZXJ2aWNlcyBpbnRvIDQ1IGNsYXNzZXMgLSAzNCBmb3IgcHJvZHVjdHMsIDExIGZvciBzZXJ2aWNlcyAtIHBlcm1pdHRpbmcgeW91IHRvIHNwZWNpZnkgcHJlY2lzZSBhbmQgY2xlYXIgY2xhc3NlcyBjb3ZlcmluZyB5b3VyIHRyYWRlbWFyay4gJyArXG4gICAgJ1RoZSBwcm90ZWN0aW9uIHRoYXQgaXMgb2ZmZXJlZCB0byBhIHJlZ2lzdGVyZWQgdHJhZGVtYXJrIGNvdmVycyBvbmx5IHRoZSBjbGFzc2VzIHNwZWNpZmllZCBhdCB0aGUgdGltZSBvZiByZWdpc3RyYXRpb24sIHRoZXJlZm9yZSwgYWxsb3dpbmcnICtcbiAgICAnIHR3byBpZGVudGljYWwgdHJhZGVtYXJrcyB0byBjb2V4aXN0IGluIGRpc3RpbmN0IGNsYXNzZXMuJyxcbiAgY29tbWVyY2VfdXNlX2hlbHAgOiAn4oCcVXNlZCBpbiBjb21tZXJjZeKAnSBtZWFucyB0aGF0IHRoZSBtYXJrIGlzIHVzZWQgaW4gdGhlIG9yZGluYXJ5IGNvdXJzZSBvZiB0cmFkZS4gRm9yIGV4YW1wbGUsIGZvciBnb29kcyBpdCB3b3VsZCBiZSB1c2VkIHdoZW4gaXQgaXMgcGxhY2VkIGluIGFueSBtYW5uZXIgb24gdGhlIGdvb2RzLCB0aGVpciBjb250YWluZXJzIG9yIHRoZWlyIGRpc3BsYXlzLiBGb3Igc2VydmljZXMgaXQgaXMgdXNlZCB3aGVuIHRoZSB0cmFkZW1hcmsgaXMgdXNlZCBvciBkaXNwbGF5ZWQgaW4gYWR2ZXJ0aXNpbmcgb3IgZHVyaW5nIHRoZSBzYWxlcyBwcm9jZXNzLidcbn0pLmNvbnN0YW50KCdQT0FfSU5GTycsIHtcblxuICAnSU4nOiB7XG4gICAgICAgICAgMDogJ0kgd2lsbCBiZSBzZW5kaW5nIGl0IHdpdGhpbiAxMjAgZGF5czsgZmlsZSB0cmFkZW1hcmsgaW1tZWRpYXRlbHkuJyxcbiAgICAgICAgICAxOiAnTm8sIGRvIG5vdCBmaWxlIGFwcGxpY2F0aW9uIHVudGlsIHJlY2VpdmVkIHRoZSBQb3dlciBvZiBBdHRvcm5leS4nXG4gICAgICAgIH0sXG4gICdBUic6IHtcbiAgICAwOiAnSSB3aWxsIGJlIHNlbmRpbmcgaXQgd2l0aGluIDQwIGRheXM7IGZpbGUgdHJhZGVtYXJrIGltbWVkaWF0ZWx5LicsXG4gICAgMTogJ05vLCBkbyBub3QgZmlsZSBhcHBsaWNhdGlvbiB1bnRpbCByZWNlaXZlZCB0aGUgUG93ZXIgb2YgQXR0b3JuZXkuJ1xuICB9LFxuICAnQVonOiB7XG4gICAgMDogJ0kgd2lsbCBiZSBzZW5kaW5nIGl0IHdpdGhpbiAzMCBkYXlzOyBmaWxlIHRyYWRlbWFyayBpbW1lZGlhdGVseS4nLFxuICAgIDE6ICdObywgZG8gbm90IGZpbGUgYXBwbGljYXRpb24gdW50aWwgcmVjZWl2ZWQgdGhlIFBvd2VyIG9mIEF0dG9ybmV5LidcbiAgfSxcbiAgJ0JPJzoge1xuICAgIDA6ICdJIHdpbGwgYmUgc2VuZGluZyBpdCB3aXRoaW4gMzAgZGF5czsgZmlsZSB0cmFkZW1hcmsgaW1tZWRpYXRlbHkuJyxcbiAgICAxOiAnTm8sIGRvIG5vdCBmaWxlIGFwcGxpY2F0aW9uIHVudGlsIHJlY2VpdmVkIHRoZSBQb3dlciBvZiBBdHRvcm5leS4nXG4gIH0sXG4gICdDQSc6IHtcbiAgICAwOiAnSSB3aWxsIGJlIHNlbmRpbmcgaXQgd2l0aGluIDQ1IGRheXM7IGZpbGUgdHJhZGVtYXJrIGltbWVkaWF0ZWx5LicsXG4gICAgMTogJ05vLCBkbyBub3QgZmlsZSBhcHBsaWNhdGlvbiB1bnRpbCByZWNlaXZlZCB0aGUgUG93ZXIgb2YgQXR0b3JuZXkuJ1xuICB9LFxuICAnQ08nOiB7XG4gICAgMDogJ0kgd2lsbCBiZSBzZW5kaW5nIGl0IHdpdGhpbiA2MCBkYXlzOyBmaWxlIHRyYWRlbWFyayBpbW1lZGlhdGVseS4nLFxuICAgIDE6ICdObywgZG8gbm90IGZpbGUgYXBwbGljYXRpb24gdW50aWwgcmVjZWl2ZWQgdGhlIFBvd2VyIG9mIEF0dG9ybmV5LidcbiAgfSxcbiAgJ01YJzoge1xuICAgIDA6ICdJIHdpbGwgYmUgc2VuZGluZyBpdCB3aXRoaW4gMzAgZGF5czsgZmlsZSB0cmFkZW1hcmsgaW1tZWRpYXRlbHkoJDE1MSknLFxuICAgIDE6ICdObywgZG8gbm90IGZpbGUgYXBwbGljYXRpb24gdW50aWwgcmVjZWl2ZWQgdGhlIFBvd2VyIG9mIEF0dG9ybmV5LidcbiAgfSxcbiAgJ0JSJzoge1xuICAgIDA6ICdJIHdpbGwgYmUgc2VuZGluZyBpdCB3aXRoaW4gNTUgZGF5czsgZmlsZSB0cmFkZW1hcmsgaW1tZWRpYXRlbHkoJDE2MyknLFxuICAgIDE6ICdObywgZG8gbm90IGZpbGUgYXBwbGljYXRpb24gdW50aWwgcmVjZWl2ZWQgdGhlIFBvd2VyIG9mIEF0dG9ybmV5LidcbiAgfSxcbiAgJ05PJzoge1xuICAgIDA6ICdJIHdpbGwgYmUgc2VuZGluZyBpdCB3aXRoaW4gOTAgZGF5czsgZmlsZSB0cmFkZW1hcmsgaW1tZWRpYXRlbHknLFxuICAgIDE6ICdObywgZG8gbm90IGZpbGUgYXBwbGljYXRpb24gdW50aWwgcmVjZWl2ZWQgdGhlIFBvd2VyIG9mIEF0dG9ybmV5LidcbiAgfSxcbiAgJ1JVJzoge1xuICAgIDA6ICdJIHdpbGwgYmUgc2VuZGluZyBpdCB3aXRoaW4gNjAgZGF5czsgZmlsZSB0cmFkZW1hcmsgaW1tZWRpYXRlbHknLFxuICAgIDE6ICdObywgZG8gbm90IGZpbGUgYXBwbGljYXRpb24gdW50aWwgcmVjZWl2ZWQgdGhlIFBvd2VyIG9mIEF0dG9ybmV5LidcbiAgfSxcbiAgJ1ZOJzoge1xuICAgIDA6ICdJIHdpbGwgYmUgc2VuZGluZyBpdCB3aXRoaW4gMzAgZGF5czsgZmlsZSB0cmFkZW1hcmsgaW1tZWRpYXRlbHkgKCQ1MCknLFxuICAgIDE6ICdObywgZG8gbm90IGZpbGUgYXBwbGljYXRpb24gdW50aWwgcmVjZWl2ZWQgdGhlIFBvd2VyIG9mIEF0dG9ybmV5LidcbiAgfSxcbiAgJ1RXJzoge1xuICAgIDA6ICdJIHdpbGwgYmUgc2VuZGluZyBpdCB3aXRoaW4gOTAgZGF5czsgZmlsZSB0cmFkZW1hcmsgaW1tZWRpYXRlbHknLFxuICAgIDE6ICdObywgZG8gbm90IGZpbGUgYXBwbGljYXRpb24gdW50aWwgcmVjZWl2ZWQgdGhlIFBvd2VyIG9mIEF0dG9ybmV5LidcbiAgfSxcbiAgJ0NOJzogezA6ICcnLCAxOiAnJ30sXG4gICdIRUxQJzp7XG4gICAgJ0lOJzonSW4gSU5ESUEgaXQgaXMgcG9zc2libGUgdG8gZmlsZSBpbW1lZGlhdGVseSB5b3VyIHRyYWRlbWFyayBhcHBsaWNhdGlvbiwgd2l0aG91dCBQb3dlciBvZiBBdHRvcm5leSwgcHJvdmlkZWQgdGhhdCB5b3Ugd2lsbCBzZW5kIGl0IHdpdGhpbiAxMjAgRGF5cy4nXG4gIH0sXG4gICdQT0FfTVVTVCc6ICdQbGVhc2Ugbm90ZSB0aGF0IGluIG9yZGVyIHRvIGZpbGUgeW91ciB0cmFkZW1hcmsgYXBwbGljYXRpb24sIHdlIG5lZWQgdGhhdCB5b3Ugc2VuZCB1cyBhIFBvd2VyIG9mIEF0dG9ybmV5LCBiZWxvdyB5b3UgY2FuIGZpbmQgYSBUZW1wbGF0ZSdcblxufSlcblxuIiwiXCJ1c2Ugc3RyaWN0XCI7XG4vKiBnbG9iYWwgYW5ndWxhciovXG5hbmd1bGFyLm1vZHVsZSgnbW9kLmF1dGhvcml6ZScsIFtdKS52YWx1ZSgndmVyc2lvbicsICcwLjEnKVxuXG4gIC8vIGNvbnRhaW5zIGxvZ2dlZCB1c2VyIGluZm8sIHVzZXMgc2Vzc2lvblN0b3JhZ2UsIGNhbiBiZSBjaGFuZ2VkIHRvIHVzZSBsb2NhbFN0b3JhZ2UgaWYgbmVlZGVkXG5cbiAgLnNlcnZpY2UoJ1Nlc3Npb24nLCBbXCIkd2luZG93XCIsIFwiJGNvb2tpZVN0b3JlXCIsIGZ1bmN0aW9uICgkd2luZG93LCAkY29va2llU3RvcmUpIHtcbiAgICB0aGlzLmNyZWF0ZSA9IGZ1bmN0aW9uIChzZXNzaW9uSWQsIHVzZXJJRCwgbmFtZSwgbG9jYWxlKSB7XG5cbiAgICAgICRjb29raWVTdG9yZS5wdXQoJ3Nlc3Npb25JRCcsIHNlc3Npb25JZClcbiAgICAgICRjb29raWVTdG9yZS5wdXQoJ25hbWUnLCBuYW1lKVxuICAgICAgJGNvb2tpZVN0b3JlLnB1dCgndXNlcklEJywgdXNlcklEKVxuICAgICAgJGNvb2tpZVN0b3JlLnB1dCgnbG9jYWxlJywgbG9jYWxlKVxuXG4gICAgfTtcblxuICAgIHRoaXMuZGVzdHJveSA9IGZ1bmN0aW9uICgpIHtcblxuICAgICAgJGNvb2tpZVN0b3JlLnJlbW92ZSgnc2Vzc2lvbklEJylcbiAgICAgICRjb29raWVTdG9yZS5yZW1vdmUoJ25hbWUnKVxuICAgICAgJGNvb2tpZVN0b3JlLnJlbW92ZSgndXNlcklEJylcbiAgICAgICRjb29raWVTdG9yZS5yZW1vdmUoJ2xvY2FsZScpXG4gICAgfTtcblxuICAgIHRoaXMuZ2V0U2Vzc2lvbklEID0gZnVuY3Rpb24gKCkge1xuICAgICAgcmV0dXJuICRjb29raWVTdG9yZS5nZXQoJ3Nlc3Npb25JRCcpXG4gICAgfVxuXG4gICAgdGhpcy5nZXROYW1lID0gZnVuY3Rpb24gKCkge1xuICAgICAgcmV0dXJuICRjb29raWVTdG9yZS5nZXQoJ25hbWUnKVxuICAgIH1cblxuICAgIHRoaXMuZ2V0VXNlcklEID0gZnVuY3Rpb24gKCkge1xuICAgICAgcmV0dXJuICRjb29raWVTdG9yZS5nZXQoJ3VzZXJJRCcpXG4gICAgfVxuXG4gICAgcmV0dXJuIHRoaXM7XG4gIH1dKVxuXG4gIC8vIGF1dGhvcml6ZSBzZXJ2aWNlc1xuXG4gIC5mYWN0b3J5KCdhdXRoU2VydmljZScsIFsnJGh0dHAnLCAnJHEnLCAnJHRpbWVvdXQnLCAnU2Vzc2lvbicsIGZ1bmN0aW9uICgkaHR0cCwgJHEsICR0aW1lb3V0LCBTZXNzaW9uKSB7XG5cbiAgICB2YXIgZmFzYWQgPSB7fTtcblxuICAgIHZhciBhdXRoZW50aWNhdGUgPSBmdW5jdGlvbiAodXNlcikge1xuICAgICAgdmFyIGRlZmVycmVkID0gJHEuZGVmZXIoKTtcblxuICAgICAgJGh0dHAucG9zdCgnL3NpZ25pbi8nLCB1c2VyKS5cbiAgICAgICAgc3VjY2VzcyhmdW5jdGlvbiAoZGF0YSwgc3RhdHVzKSB7XG4gICAgICAgICAgY29uc29sZS5sb2coZGF0YSlcbiAgICAgICAgICBjb25zb2xlLmxvZyhzdGF0dXMpXG4gICAgICAgICAgaWYgKGRhdGEuc2Vzc2lvbklEICE9PSBudWxsKVxuICAgICAgICAgICAgU2Vzc2lvbi5jcmVhdGUoZGF0YS5zZXNzaW9uSUQsIGRhdGEudXNlcklELCBkYXRhLm5hbWUsIGRhdGEubG9jYWxlKTtcbiAgICAgICAgICBkZWZlcnJlZC5yZXNvbHZlKHthdXRoZW50aWNhdGVkOiBkYXRhLnNlc3Npb25JRCAhPT0gbnVsbCwgbWVzc2FnZTogZGF0YS5tc2csIG5hbWU6IGRhdGEubmFtZX0pO1xuICAgICAgICB9KS5cbiAgICAgICAgZXJyb3IoZnVuY3Rpb24gKGVycm9yKSB7XG4gICAgICAgICAgZGVmZXJyZWQucmVzb2x2ZSh7YXV0aGVudGljYXRlZDogZmFsc2UsIG1lc3NhZ2U6ICdFcnJvciBvY2N1cnJlZCB3aGlsZSBhdXRoZW50aWNhdGluZy4nfSk7XG4gICAgICAgIH0pO1xuXG4gICAgICByZXR1cm4gZGVmZXJyZWQucHJvbWlzZTtcbiAgICB9O1xuXG4gICAgdmFyIGlzQXV0aGVudGljYXRlZCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciBzaWQgPSBTZXNzaW9uLmdldFNlc3Npb25JRCgpXG4gICAgICBpZiAoKHNpZCA9PT0gdW5kZWZpbmVkKSB8fCAoc2lkID09PSBudWxsKSlcbiAgICAgICAgcmV0dXJuIGZhbHNlXG4gICAgICBlbHNlXG4gICAgICAgIHJldHVybiB0cnVlXG5cbiAgICB9O1xuXG4gICAgdmFyIHJlZ2lzdGVyID0gZnVuY3Rpb24gKHVzZXIpIHtcbiAgICAgIHZhciBkZWZlcnJlZCA9ICRxLmRlZmVyKCk7XG5cbiAgICAgICRodHRwLnBvc3QoJy9zaWdudXAnLCB1c2VyKS5cbiAgICAgICAgc3VjY2VzcyhmdW5jdGlvbiAoZGF0YSwgc3RhdHVzKSB7XG4gICAgICAgICAgZGVmZXJyZWQucmVzb2x2ZSh7cmVnaXN0ZXJlZDogZGF0YS5yZWdpc3RlcmVkLCBtZXNzYWdlOiBkYXRhLm1lc3NhZ2V9KTtcbiAgICAgICAgfSkuXG4gICAgICAgIGVycm9yKGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgICAgIGRlZmVycmVkLnJlc29sdmUoe3JlZ2lzdGVyZWQ6IGZhbHNlLCBtZXNzYWdlOiAnU29ycnksIGVycm9yIG9jY3VycmVkIHdoaWxlIHNpZ25pbmcgdXAsIHBsZWFzZSB0cnkgYWdhaW4uJ30pO1xuICAgICAgICB9KTtcblxuICAgICAgcmV0dXJuIGRlZmVycmVkLnByb21pc2U7XG4gICAgfTtcblxuICAgIHZhciBsb2dPdXQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICBTZXNzaW9uLmRlc3Ryb3koKVxuICAgIH07XG5cbiAgICB2YXIgcmVxdWVzdF9wYXNzd29yZF9yZXNldCA9IGZ1bmN0aW9uIChlbWFpbCkge1xuXG4gICAgICB2YXIgZGVmZXJyZWQgPSAkcS5kZWZlcigpO1xuXG4gICAgICAkaHR0cC5nZXQoJy9hcGkyL3Bhc3N3b3JkX3Jlc2V0P2VtYWlsPScgKyBlbWFpbCkuc3VjY2VzcyhmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICBjb25zb2xlLmxvZyhkYXRhKVxuICAgICAgICBkZWZlcnJlZC5yZXNvbHZlKHtzdGF0dXM6IGRhdGEuc3RhdHVzID09PSAnb2snLCBtZXNzYWdlOiBkYXRhLm1lc3NhZ2V9KTtcbiAgICAgIH0pLmVycm9yKGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgICBkZWZlcnJlZC5yZXNvbHZlKHtzdGF0dXM6ICdlcnInLCBtZXNzYWdlOiAnU29ycnksIGVycm9yIG9jY3VycmVkIHdoaWxlIGFjY2Vzc2luZyBzZXJ2ZXIsIHBsZWFzZSB0cnkgYWdhaW4uJ30pO1xuICAgICAgfSk7XG5cbiAgICAgIHJldHVybiBkZWZlcnJlZC5wcm9taXNlO1xuICAgIH07XG5cblxuICAgIGZhc2FkLnNpZ25JbiA9IGZ1bmN0aW9uICh1c2VyKSB7XG4gICAgICByZXR1cm4gYXV0aGVudGljYXRlKHVzZXIpO1xuICAgIH07XG4gICAgZmFzYWQuc2lnblVwID0gZnVuY3Rpb24gKHVzZXIpIHtcbiAgICAgIHJldHVybiByZWdpc3Rlcih1c2VyKTtcbiAgICB9O1xuICAgIGZhc2FkLmxvZ091dCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIHJldHVybiBsb2dPdXQoKVxuICAgIH07XG4gICAgZmFzYWQuaXNTaWduZWRJbiA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIHJldHVybiBpc0F1dGhlbnRpY2F0ZWQoKVxuICAgIH07XG4gICAgZmFzYWQudXNlck5hbWUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICByZXR1cm4gU2Vzc2lvbi5nZXROYW1lKClcbiAgICB9O1xuXG4gICAgZmFzYWQucmVxdWVzdF9wYXNzd29yZF9yZXNldCA9IGZ1bmN0aW9uIChlbWFpbCkge1xuICAgICAgcmV0dXJuIHJlcXVlc3RfcGFzc3dvcmRfcmVzZXQoZW1haWwpXG4gICAgfVxuXG4gICAgcmV0dXJuIGZhc2FkO1xuXG4gIH1dKVxuXG4gIC5jb25zdGFudCgnQVVUSF9FVkVOVFMnLCB7XG4gICAgbG9naW5TdWNjZXNzOiAnYXV0aC1sb2dpbi1zdWNjZXNzJyxcbiAgICBsb2dpbkZhaWxlZDogJ2F1dGgtbG9naW4tZmFpbGVkJyxcbiAgICBsb2dvdXRTdWNjZXNzOiAnYXV0aC1sb2dvdXQtc3VjY2VzcycsXG4gICAgc2Vzc2lvblRpbWVvdXQ6ICdhdXRoLXNlc3Npb24tdGltZW91dCcsXG4gICAgbm90QXV0aGVudGljYXRlZDogJ2F1dGgtbm90LWF1dGhlbnRpY2F0ZWQnLFxuICAgIG5vdEF1dGhvcml6ZWQ6ICdhdXRoLW5vdC1hdXRob3JpemVkJyxcbiAgICBjYXJ0VXBkYXRlZDogJ2NhcnQtdXBkYXRlZCdcbiAgfSlcblxuICAvLyBhIHNpbXBsZSBsb2NhbCBkYiBzdG9yZSwgc3RvcmVzIGtleS92YWx1ZSBpbiBzZXNzaW9uU3RvcmFnZSwgaWRlYWwgZm9yIGtlZXBwaW5nIHNvbWUgZGF0YSBsaWtlIFVSTCBpbiB0aGUgc3RvcmUsXG4gIC8vIGFuZCB1c2UgdGhlIGtleSB0byBwYXNzIHRvIHVybFxuXG4gIC5zZXJ2aWNlKCdEQlN0b3JlJyxbJyR3aW5kb3cnLCBmdW5jdGlvbiAoJHdpbmRvdykge1xuXG4gICAgdGhpcy5jcmVhdGUgPSBmdW5jdGlvbiAoKSB7XG4gICAgfTtcblxuICAgIHRoaXMuZGVzdHJveSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIGRlbGV0ZSAkd2luZG93LnNlc3Npb25TdG9yYWdlLmRiU3RvcmVcbiAgICB9O1xuXG4gICAgdGhpcy5nZXQgPSBmdW5jdGlvbiAoa2V5KSB7XG4gICAgICB2YXIgcWtleSA9ICdEQl8nICsga2V5XG4gICAgICB2YXIgcmV0ID0gJHdpbmRvdy5zZXNzaW9uU3RvcmFnZVtxa2V5XVxuICAgICAgZGVsZXRlICR3aW5kb3cuc2Vzc2lvblN0b3JhZ2VbcWtleV1cbiAgICAgIHJldHVybiByZXRcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBnZW5lcmF0ZVF1aWNrR3VpZCgpIHtcbiAgICAgIHJldHVybiBNYXRoLnJhbmRvbSgpLnRvU3RyaW5nKDM2KS5zdWJzdHJpbmcoMiwgMTUpICtcbiAgICAgICAgTWF0aC5yYW5kb20oKS50b1N0cmluZygzNikuc3Vic3RyaW5nKDIsIDE1KTtcbiAgICB9XG5cbiAgICB0aGlzLmFkZCA9IGZ1bmN0aW9uIChvYmopIHtcbiAgICAgIHZhciBrZXkgPSBnZW5lcmF0ZVF1aWNrR3VpZCgpXG4gICAgICAkd2luZG93LnNlc3Npb25TdG9yYWdlWydEQl8nICsga2V5XSA9IG9ialxuICAgICAgcmV0dXJuIGtleVxuICAgIH1cblxuICAgIHJldHVybiB0aGlzO1xuICB9XSlcblxuIiwiXCJ1c2Ugc3RyaWN0XCI7XG4vKiBnbG9iYWwgYW5ndWxhciovXG5cbmFuZ3VsYXIubW9kdWxlKCdtb2QuY2FyZHMnLCBbXSlcbiAgLmZpbHRlcihcbiAgJ3JhbmdlJywgZnVuY3Rpb24gKCkge1xuICAgIHZhciBmaWx0ZXIgPVxuICAgICAgZnVuY3Rpb24gKGFyciwgbG93ZXIsIHVwcGVyKSB7XG4gICAgICAgIGZvciAodmFyIGkgPSBsb3dlcjsgaSA8PSB1cHBlcjsgaSsrKSBhcnIucHVzaChpKVxuICAgICAgICByZXR1cm4gYXJyXG4gICAgICB9XG4gICAgcmV0dXJuIGZpbHRlclxuICB9XG4pXG4gIC5zZXJ2aWNlKCdDYXJkcycsIFsnJGh0dHAnLCAnJHEnLCBmdW5jdGlvbiAoJGh0dHAsICRxKSB7XG5cbiAgICB0aGlzLmdldF9zaG9wcGluZ19jYXJ0X2RwbSA9IGZ1bmN0aW9uIChjYXJ0X2lkKSB7XG4gICAgICB2YXIgZGVmZXJyZWQgPSAkcS5kZWZlcigpO1xuXG4gICAgICAkaHR0cC5nZXQoJy9hcGlfcGF5bWVudC9nZXRfc2hvcHBpbmdfY2FydF9kcG0vJyArIGNhcnRfaWQpLnN1Y2Nlc3MoZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgY29uc29sZS5sb2coJ2JhY2sgZnJvbSBjYXJ0X2RwbScpXG4gICAgICAgIGNvbnNvbGUubG9nKGRhdGEpXG4gICAgICAgIGRlZmVycmVkLnJlc29sdmUoZGF0YSk7XG4gICAgICB9KS5lcnJvcihmdW5jdGlvbiAoZXJyb3IpIHtcbiAgICAgICAgZGVmZXJyZWQucmVzb2x2ZShlcnJvcik7XG4gICAgICB9KTtcbiAgICAgIHJldHVybiBkZWZlcnJlZC5wcm9taXNlO1xuICAgIH1cblxuICAgIC8vIHRoaXMgcmV0dXJucyBhbiBibGFuayBjYXJkIGluZm8sIHNvbWUgc3VnZ2VzdGVkIGFkZHJlc3NcblxuICAgIHRoaXMuZ2V0X3VzZXJfaW5mbyA9IGZ1bmN0aW9uIChjYXJ0X2lkKSB7XG4gICAgICB2YXIgZGVmZXJyZWQgPSAkcS5kZWZlcigpO1xuXG4gICAgICAkaHR0cC5nZXQoJy9hcGlfcGF5bWVudC9nZXRfdXNlcl9pbmZvLycgKyBjYXJ0X2lkKS5zdWNjZXNzKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdiYWNrIGZyb20gY2FydF9kcG0nKVxuICAgICAgICBjb25zb2xlLmxvZyhkYXRhKVxuICAgICAgICBkZWZlcnJlZC5yZXNvbHZlKGRhdGEpO1xuICAgICAgfSkuZXJyb3IoZnVuY3Rpb24gKGVycm9yKSB7XG4gICAgICAgIGRlZmVycmVkLnJlc29sdmUoZXJyb3IpO1xuICAgICAgfSk7XG5cbi8qXG4gICAgICB2YXIgaW5mbyAgPSB7XG4gICAgICAgIFwieF9jYXJkX251bVwiOiBcIjYwMTEwMDAwMDAwMDAwMTJcIixcbiAgICAgICAgeF9leHBfZGF0ZTogXCIwNC8xN1wiLFxuICAgICAgICB4X2NhcmRfY29kZTogXCI3ODJcIixcbiAgICAgICAgeF9maXJzdF9uYW1lOiBcIkpvaG5cIixcbiAgICAgICAgeF9sYXN0X25hbWU6IFwiRG9lXCIsXG4gICAgICAgIHhfYWRkcmVzczogXCIxMjMgTWFpbiBTdHJlZXRcIixcbiAgICAgICAgeF9jaXR5OiBcIkJvc3RvblwiLFxuICAgICAgICB4X3N0YXRlOiBcIk1BXCIsXG4gICAgICAgIHhfemlwOiBcIjAyMTQyXCIsXG4gICAgICAgIHhfY291bnRyeTogXCJVU1wiXG4gICAgICB9O1xuICAgICAgKi9cbiAgICAgIHJldHVybiBkZWZlcnJlZC5wcm9taXNlO1xuICAgIH1cblxuXG4gICAgcmV0dXJuIHRoaXM7XG5cbiAgfV0pIiwiXCJ1c2Ugc3RyaWN0XCI7XG4vKiBnbG9iYWxzIGFuZ3VsYXIgKi9cblxudmFyIGNvdW50cmllcyA9IHJlcXVpcmUoJ2NvdW50cmllcycpXG5cbmFuZ3VsYXIubW9kdWxlKCdtb2QuY291bnRyaWVzJywgW10pLmZhY3RvcnkoXCJjb3VudHJpZXNcIiwgZnVuY3Rpb24oKSB7XG4gICAgY29uc29sZS5sb2coJzEyMycpXG4gICAgdmFyIGkgPSAxMDAwXG4gICAgaWYgKGkgPT09IDEwMDApXG4gICAgICBjb25zb2xlLmxvZygnaSBhbSBoZXJlIGFuZCB0aGVyZScpXG4gICAgcmV0dXJuIGNvdW50cmllcy5nZXRfY291bnRyaWVzKClcbn0pXG5cbiIsIlwidXNlIHN0cmljdFwiO1xuLyogZ2xvYmFsIGFuZ3VsYXIqL1xuXG5hbmd1bGFyLm1vZHVsZSgnbW9kLnBhZ2VyJywgW10pLmRpcmVjdGl2ZSgncGFnZXIyJywgWyckbG9jYXRpb24nLCBmdW5jdGlvbiAoJGxvY2F0aW9uKSB7XG4gIHJldHVybiB7XG4gICAgcmVzdHJpY3Q6ICdFJyxcbiAgICBzY29wZToge1xuICAgICAgY3VycmVudDogJz0nLFxuICAgICAgcGFnZUNvdW50OiAnPScsXG4gICAgICBzaG93TW9yZVBhZ2VzOiAnQCdcbiAgICB9LFxuICAgIHRlbXBsYXRlVXJsOiAnL3BhcnRpYWxzL3BhZ2VyLmh0bWwnLFxuICAgIGxpbms6IGZ1bmN0aW9uIChzY29wZSwgaXRlclN0YXJ0RWxlbWVudCwgYXR0cnMpIHtcblxuICAgICAgc2NvcGUucGFnZXJQYWdlID0gMFxuICAgICAgc2NvcGUucGFnZXJTaXplID0gMTBcblxuICAgICAgc2NvcGUucGFnZXJfY291bnQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmIChzY29wZS5wYWdlQ291bnQgIT09IHVuZGVmaW5lZClcbiAgICAgICAgICByZXR1cm4gTWF0aC5jZWlsKHNjb3BlLnBhZ2VDb3VudCAvIHNjb3BlLnBhZ2VyU2l6ZSlcbiAgICAgICAgZWxzZVxuICAgICAgICAgIHJldHVybiAwO1xuICAgICAgfVxuXG4gICAgICBzY29wZS5maXJzdF9wYWdlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgcmV0ID0gW10sIGksIG5cbiAgICAgICAgaWYgKHNjb3BlLnBhZ2VDb3VudCAhPSB1bmRlZmluZWQpIHsgLy8gcGFnZUNvdW50IHdpbGwgYmUgYXNzaWduZWRcbiAgICAgICAgICBuID0gMTBcbiAgICAgICAgICBpZiAoc2NvcGUucGFnZUNvdW50IDwgbilcbiAgICAgICAgICAgIG4gPSBzY29wZS5wYWdlQ291bnRcbiAgICAgICAgICBmb3IgKGkgPSAwOyBpIDwgbjsgaSsrKVxuICAgICAgICAgICAgcmV0LnB1c2goaSArIDEpXG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHJldFxuICAgICAgfVxuXG4gICAgICBzY29wZS5wYWdlcyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIHJldCA9IFtdLCBpO1xuICAgICAgICBmb3IgKGkgPSAwOyBpIDwgc2NvcGUucGFnZUNvdW50OyBpKyspXG4gICAgICAgICAgcmV0LnB1c2goaSArIDEpXG4gICAgICAgIHJldHVybiByZXRcbiAgICAgIH1cblxuICAgICAgLy8gdHJ5IHRvIHNlZSBpZiBwYWdlIG51bWJlciBoYXMgYmVlbiBzZXQgYnkgY2FsbGVyLCBhbmQgYWRqdXN0IHRoZSBjdXJyZW50IHBhZ2VyIG5vXG4gICAgICBzY29wZS5yZV9hZGp1c3RfY3VycmVudF9wYWdlID0gZnVuY3Rpb24gKCkge1xuXG4gICAgICAgIGlmIChzY29wZS5wYWdlQ291bnQgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgIGlmIChzY29wZS5wYWdlclNpemUgPiAwKSB7XG4gICAgICAgICAgICB2YXIgbiA9IE1hdGguZmxvb3Ioc2NvcGUuY3VycmVudCAvIHNjb3BlLnBhZ2VyU2l6ZSlcbiAgICAgICAgICAgIGlmIChuICE9PSBzY29wZS5wYWdlclBhZ2UpXG4gICAgICAgICAgICAgIHNjb3BlLnBhZ2VyUGFnZSA9IG5cbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgfVxuXG4gICAgICBzY29wZS5jdXJyZW50X3BhZ2VyX3BhZ2UgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmIChzY29wZS5wYWdlQ291bnQgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgIHZhciByZXQgPSBbXSwgaTtcbiAgICAgICAgICBzY29wZS5yZV9hZGp1c3RfY3VycmVudF9wYWdlKClcbiAgICAgICAgICB2YXIgb2Zmc2V0ID0gc2NvcGUucGFnZXJQYWdlICogc2NvcGUucGFnZXJTaXplXG4gICAgICAgICAgZm9yIChpID0gb2Zmc2V0OyBpIDwgb2Zmc2V0ICsgc2NvcGUucGFnZXJTaXplOyBpKyspIHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdiYicsIGkgPCBzY29wZS5wYWdlQ291bnQpXG4gICAgICAgICAgICBpZiAoaSA8IHNjb3BlLnBhZ2VDb3VudClcbiAgICAgICAgICAgICAgcmV0LnB1c2goaSArIDEpXG4gICAgICAgICAgfVxuICAgICAgICAgIHJldHVybiByZXRcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBzY29wZS5hbGxfcGFnZXMgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciByZXQgPSBbXSwgaTtcbiAgICAgICAgZm9yIChpID0gMDsgaSA8IHNjb3BlLnBhZ2VDb3VudDsgaSsrKVxuICAgICAgICAgIHJldC5wdXNoKGkgKyAxKVxuICAgICAgICByZXR1cm4gcmV0XG4gICAgICB9XG5cbiAgICAgIHNjb3BlLnNldF9wYWdlX251bWJlciA9IGZ1bmN0aW9uIChuKSB7XG4gICAgICAgIHNjb3BlLmN1cnJlbnQgPSBuXG4gICAgICAgIGlmIChzY29wZS4kcGFyZW50LnNldERhdGFTb3VyY2UgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgIHNjb3BlLiRwYXJlbnQuc2V0RGF0YVNvdXJjZShzY29wZS5jdXJyZW50KVxuICAgICAgICAgICRsb2NhdGlvbi5zZWFyY2goJ3BhZ2Vfbm8nLCBuICsgMSlcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBzY29wZS5uZXh0UGFnZSA9IGZ1bmN0aW9uICgpIHtcblxuICAgICAgICBjb25zb2xlLmxvZygnc3NzJywgc2NvcGUucGFnZXJfY291bnQoKSlcbiAgICAgICAgdmFyIG4gPSBzY29wZS5wYWdlclBhZ2UgKyAxXG5cbiAgICAgICAgaWYgKG4gPCBzY29wZS5wYWdlQ291bnQgLyBzY29wZS5wYWdlclNpemUpXG4gICAgICAgICAgc2NvcGUucGFnZXJQYWdlID0gblxuXG4gICAgICAgIGNvbnNvbGUubG9nKHNjb3BlLmN1cnJlbnQsIHNjb3BlLnBhZ2VyUGFnZSlcbiAgICAgICAgY29uc29sZS5sb2coJ25leHQgcGFnZScpXG5cbiAgICAgICAgdGhpcy5zZXRfcGFnZV9udW1iZXIoc2NvcGUucGFnZXJQYWdlICogc2NvcGUucGFnZXJTaXplKVxuICAgICAgfVxuXG4gICAgICBzY29wZS5wcmV2UGFnZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKHNjb3BlLnBhZ2VyUGFnZSA+IDApXG4gICAgICAgICAgc2NvcGUucGFnZXJQYWdlID0gc2NvcGUucGFnZXJQYWdlIC0gMVxuICAgICAgICBjb25zb2xlLmxvZygncHJldiBwYWdlJylcbiAgICAgICAgdGhpcy5zZXRfcGFnZV9udW1iZXIoc2NvcGUucGFnZXJQYWdlICogc2NvcGUucGFnZXJTaXplKVxuICAgICAgfVxuXG5cbiAgICAgIHNjb3BlLm15Y2xhc3MgPSBmdW5jdGlvbiAobikge1xuICAgICAgICBpZiAobiA9PT0gc2NvcGUuY3VycmVudClcbiAgICAgICAgICByZXR1cm4gJ2FjdGl2ZSdcbiAgICAgICAgZWxzZVxuICAgICAgICAgIHJldHVybiAnJ1xuICAgICAgfVxuXG4gICAgICBzY29wZS5zaG93ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4gc2NvcGUucGFnZUNvdW50ID4gMTtcbiAgICAgIH1cblxuICAgICAgc2NvcGUuc2hvd01vcmUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiBzY29wZS5zaG93TW9yZVBhZ2VzO1xuICAgICAgfVxuXG4gICAgICBzY29wZS5zZXRfc2hvd19tb3JlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICBzY29wZS5zaG93TW9yZVBhZ2VzID0gIXNjb3BlLnNob3dNb3JlUGFnZXNcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1dKVxuIiwibW9kdWxlLmV4cG9ydHMgPSBbXG4gIHJlcXVpcmUoJy4uL2NvbW1vbi90a3Nfc2VydmljZXMuanMnKSxcbiAgcmVxdWlyZSgnLi4vY29tbW9uL3Rrc19maWx0ZXJzLmpzJyksXG4gIHJlcXVpcmUoJy4uL2NvbW1vbi90a3NfZGlyZWN0aXZlcy5qcycpLFxuICByZXF1aXJlKCcuLi9jb21tb24vdGtzX2RpcmVjdGl2ZXMyLmpzJyksXG4gIHJlcXVpcmUoJy4uL2NvbW1vbi9wcm9maWxlX2RpcmVjdGl2ZS5qcycpLFxuICByZXF1aXJlKCcuL21vZF9jb3VudHJpZXMuanMnKSxcbiAgcmVxdWlyZSgnLi9tb2RfYXV0aG9yaXplLmpzJyksXG4gIHJlcXVpcmUoJy4vbW9kX3BhZ2VyLmpzJyksXG4gIHJlcXVpcmUoJy4vbW9kX2NhcmRzLmpzJylcbl1cbiIsIlwidXNlIHN0cmljdFwiO1xudmFyIGNvdW50cmllcyA9IHJlcXVpcmUoJ2NvdW50cmllcycpXG52YXIgaW5wdXRfZm9ybWF0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi9kYXRhL2Zvcm1hdHMuanNvblwiKVxudmFyIG9hcGkgPSByZXF1aXJlKFwiLi4vLi4vLi4vZGF0YS9vYXBpLmpzb25cIilcbnZhciBhcmlwbyA9IHJlcXVpcmUoXCIuLi8uLi8uLi9kYXRhL2FyaXBvLmpzb25cIilcblxuZnVuY3Rpb24gZ2V0SUVWZXJzaW9uKGFnZW50KXtcbiAgdmFyIHJlZyA9IC9NU0lFXFxzPyhcXGQrKSg/OlxcLihcXGQrKSk/L2k7XG4gIHZhciBtYXRjaGVzID0gYWdlbnQubWF0Y2gocmVnKTtcbiAgaWYgKG1hdGNoZXMgIT09IG51bGwpIHtcbiAgICByZXR1cm4geyBtYWpvcjogbWF0Y2hlc1sxXSwgbWlub3I6IG1hdGNoZXNbMl0gfTtcbiAgfVxuICByZXR1cm4geyBtYWpvcjogXCItMVwiLCBtaW5vcjogXCItMVwiIH07XG59XG5cbmZ1bmN0aW9uIHN1cHBvcnRlZENvdW50cmllc1Byb2MgKCkge1xuXG4gIHZhciByZXQgPSB7fVxuICB2YXIgY250ID0gMDtcbiAgZm9yICh2YXIga2V5IGluIGlucHV0X2Zvcm1hdHMpIHtcbiAgICBpZiAoaW5wdXRfZm9ybWF0cy5oYXNPd25Qcm9wZXJ0eShrZXkpKSB7XG4gIFxuICAgICAgaWYgIChjb3VudHJpZXMuZ2V0X2NvdW50cnkoa2V5KSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIHJldFtrZXldID0ge25hbWU6IGNvdW50cmllcy5nZXRfY291bnRyeShrZXkpLm5hbWUsIHByaWNlOiBjbnR9XG4gICAgICAgIGNudCA9IGNudCArIDEwXG4gICAgICB9IGVsc2Uge1xuICAgICAgICBjb25zb2xlLmxvZygnY2FuIG5vdCBmaW5kIGluIGNvdW50cnkgJywga2V5KVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIGZvciAoIGtleSBpbiBvYXBpKSB7XG4gICAgaWYgKG9hcGkuaGFzT3duUHJvcGVydHkoa2V5KSkge1xuICAgICAgaWYgIChjb3VudHJpZXMuZ2V0X2NvdW50cnkoa2V5KSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIHJldFtrZXldID0ge25hbWU6IGNvdW50cmllcy5nZXRfY291bnRyeShrZXkpLm5hbWUsIHByaWNlOiAxMDAsIGxpbms6J09BUEknfVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG5cbiAgLy8gY291bnRyaWVzIGluIGFyaXBvIGhhcyBvcHRpb24gdG8gZWl0aGVyIHJlZ2lzdGVyIGFzIHNwZWNpZmljIGNvdW50cnkgb3IgdW5kZXIgQVJJUCB0cmVhdHlcbiAgZm9yICgga2V5IGluIGFyaXBvKSB7XG4gICAgaWYgKGFyaXBvLmhhc093blByb3BlcnR5KGtleSkpIHtcbiAgICAgIGlmICAoY291bnRyaWVzLmdldF9jb3VudHJ5KGtleSkgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICByZXRba2V5XSA9IHtuYW1lOiBjb3VudHJpZXMuZ2V0X2NvdW50cnkoa2V5KS5uYW1lLCBwcmljZTogMTAwLCBvdGhlcnM6J0FSSVBPJ31cbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICByZXR1cm4gcmV0XG5cbn1cblxuLy8gZnJhbWV3b3JrIGNhbiBiZSA6ICdhbmd1bGFyJywgJ2Jvb3RzdHJhcDMnXG5cbmV4cG9ydHMuc3VwcG9ydGVkQnJvd3NlciA9IGZ1bmN0aW9uICh1c2VyQWdlbnQsIGZyYW1ld29yaykge1xuXG4gIHZhciB1c2VyX2FnZW50ID0gdXNlckFnZW50LnRvTG93ZXJDYXNlKClcbiAgLy8gQ2hyb21lIHN1cHBvcnRlZCBpbiBhbGwgcGxhdGZvcm1zXG5cbi8vICBjb25zb2xlLmxvZygndXNlciBhZ2VudCcsIHVzZXJfYWdlbnQsICEhdXNlcl9hZ2VudC5tYXRjaCgvdHJpZGVudC4qcnZbIDpdKjExXFwuLykpXG5cbiAgaWYgKC9jaHJvbWUvLnRlc3QodXNlcl9hZ2VudCkpXG4gICAgcmV0dXJuIHRydWU7XG4gIGVsc2UgaWYgKC9maXJlZm94Ly50ZXN0KHVzZXJfYWdlbnQpKVxuICAgIHJldHVybiB0cnVlXG4gIGVsc2UgaWYgKC9zYWZhcmkvLnRlc3QodXNlcl9hZ2VudCkpXG4gICAgcmV0dXJuIHRydWVcbiAgZWxzZSBpZiAoL21vemlsbGEvLnRlc3QodXNlcl9hZ2VudCkpXG4gICAgcmV0dXJuIHRydWVcbiAgZWxzZSBpZiAoL21zaWUgMTAvLnRlc3QodXNlcl9hZ2VudCkpXG4gICAgcmV0dXJuIHRydWVcbiAgZWxzZSBpZiAoISF1c2VyX2FnZW50Lm1hdGNoKC90cmlkZW50LipydlsgOl0qMTFcXC4vKSlcbiAgICByZXR1cm4gdHJ1ZVxuICBlbHNlXG4gICAgcmV0dXJuIGZhbHNlXG5cbn1cblxuZXhwb3J0cy5zdXBwb3J0ZWRDb3VudHJpZXMgPSBmdW5jdGlvbiAoKSB7XG5cbiAgcmV0dXJuIHN1cHBvcnRlZENvdW50cmllc1Byb2MoKVxuXG59XG5cbmV4cG9ydHMuZ2V0X2NvdW50cmllc19ieV9jb250bmVudCA9IGZ1bmN0aW9uKGNvZGUpIHtcblxuICB2YXIgY2xzdCA9IGNvdW50cmllcy5nZXRfY291bnRyaWVzX2J5X2NvbnRuZW50KGNvZGUpXG4gIHZhciBzbHN0ID0gc3VwcG9ydGVkQ291bnRyaWVzUHJvYygpXG5cbiAgcmV0dXJuIGNsc3QuZmlsdGVyKGZ1bmN0aW9uKGUpIHtcbiAgICByZXR1cm4gc2xzdFtlLmNvZGVdICE9PSB1bmRlZmluZWRcbiAgfSlcblxuXG59XG5cblxuIiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbi8vIGNvbnZlcnQgYWRkcmVzcyBsaW5lIHRvIGEgcHJvZmlsZSByZWNvcmRcblxuZXhwb3J0cy5wYXJzZSA9IGZ1bmN0aW9uIChhZGRyZXNzX2xpbmVzKSB7XG4gICAgdmFyIHByb2ZpbGUgPSB7XG4gICAgICAgIGFkZHJlc3M6IFwiXCIsXG4gICAgICAgIGNpdHk6IFwiXCIsXG4gICAgICAgIGNvbnRhY3Q6IFwiXCIsXG4gICAgICAgIGNvdW50cnk6IFwiXCIsXG4gICAgICAgIGVtYWlsOiBcIlwiLFxuICAgICAgICBpbmNfY291bnRyeTogXCJcIixcbiAgICAgICAgbmF0dXJlOiBcIklcIixcbiAgICAgICAgcGhvbmU6IFwiXCIsXG4gICAgICAgIHByb2ZpbGVfbmFtZTogJycsXG4gICAgICAgIHN0YXRlOiBcIlwiLFxuICAgICAgICBzdHJlZXQ6IFwiXCIsXG4gICAgICAgIHppcDogXCJcIlxuICAgIH1cblxuICAgIHZhciBsaW5lcyA9IGFkZHJlc3NfbGluZXMuc3BsaXQoXCJcXG5cIilcbiAgICB2YXIgbiA9IGxpbmVzLmxlbmd0aFxuXG4gICAgaWYgKG4gPiAzKSB7XG4gICAgICAgIHByb2ZpbGUuY291bnRyeSA9IGdldF9jb3VudHJ5X2NvZGUobGluZXNbbiAtIDFdKVxuICAgICAgICBwcm9maWxlLnppcCA9IGdldF96aXBjb2RlX3Byb2MobGluZXNbbiAtIDJdKVxuICAgICAgICBwcm9maWxlLmNpdHkgPSBsaW5lc1tuIC0gMl1cbiAgICAgICAgcHJvZmlsZS5zdHJlZXQgPSBsaW5lc1tuIC0gM11cbiAgICAgICAgcHJvZmlsZS5hZGRyZXNzID0gbGluZXNbbiAtIDRdXG4gICAgfVxuXG4gICAgcmV0dXJuIHByb2ZpbGVcbn1cblxudmFyIGNvdW50cnlfY29kZXMgPSB7XG4gICAgXCJVbml0ZWQgS2luZ2RvbVwiOiB7Y29kZTogJ0dCJ31cbn1cbmZ1bmN0aW9uIGdldF9jb3VudHJ5X2NvZGUoY291bnRyeV9uYW1lKSB7XG4gICAgdmFyIHRlbXAgPSBjb3VudHJ5X2NvZGVzW2NvdW50cnlfbmFtZV1cbiAgICBpZiAodGVtcClcbiAgICAgICAgcmV0dXJuIHRlbXAuY29kZVxuICAgIGVsc2VcbiAgICAgICAgcmV0dXJuIHVuZGVmaW5lZFxufVxuXG52YXIgdWtfemlwX3JlZyA9IC8oPzpbQS1aYS16XVxcZCA/XFxkW0EtWmEtel17Mn0pfCg/OltBLVphLXpdW0EtWmEtelxcZF1cXGQgP1xcZFtBLVphLXpdezJ9KXwoPzpbQS1aYS16XXsyfVxcZHsyfSA/XFxkW0EtWmEtel17Mn0pfCg/OltBLVphLXpdXFxkW0EtWmEtel0gP1xcZFtBLVphLXpdezJ9KXwoPzpbQS1aYS16XXsyfVxcZFtBLVphLXpdID9cXGRbQS1aYS16XXsyfSkvXG5cbi8vIHJldHVybnMgYSB2YWxpZCB6aXAgY29kZSwgb3IgdW5kZWZpbmVkXG4vLyBVSyBvbmx5IGZvciBub3dcblxuZnVuY3Rpb24gZ2V0X3ppcGNvZGVfcHJvYyhsaW5lKSB7XG4gICAgdmFyIHJldFxuICAgIHZhciByID0gdWtfemlwX3JlZy5leGVjKGxpbmUpXG4gICAgaWYgKHIpIHtcbiAgICAgICAgcmV0ID0gclswXVxuICAgIH1cbiAgICByZXR1cm4gcmV0XG59XG5cbiIsIlxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgRXM2Y29udHJvbGxlciB7XG5cbiAgICAvKkBuZ0luamVjdDsqL1xuXG4gICAgY29uc3RydWN0b3IoJHNjb3BlKSB7XG5cbiAgICAgICAgdGhpcy5jdHggPSAkc2NvcGVcblxuICAgICAgICB0aGlzLmN0eC5tc2cgPSAnaGVsbG8gRVM2MTInXG5cbiAgICAgICAgdGhpcy5jdHguY2xpY2tfbWUgPSBmdW5jdGlvbihtc2cpIHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdjbGljayBtZSAnKVxuICAgICAgICAgICAgY29uc29sZS5sb2cobXNnKVxuICAgICAgICAgICAgY29uc29sZS5sb2coJ2NsaWNrIG1lIGVuZCcpXG4gICAgICAgIH1cblxuICAgIH1cblxufVxuIiwiXCJ1c2Ugc3RyaWN0XCI7XG5cblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVXNlckNvbnRyb2xsZXIge1xuXG4gICAgLypAbmdJbmplY3Q7Ki9cblxuICAgIGNvbnN0cnVjdG9yKCRzY29wZSwgJGxvY2F0aW9uLCAkcm9vdFNjb3BlLCAkdHJhbnNsYXRlLCByZWdpc3Rlcl90cmFkZW1hcmssIGF1dGhTZXJ2aWNlLCBBVVRIX0VWRU5UUywgQ2FydCkge1xuXG4gICAgICAgICRzY29wZS5hbGVydHMgPSBbXTtcblxuICAgICAgICAkc2NvcGUuY2xvc2VBbGVydCA9IGZ1bmN0aW9uIChpbmRleCkge1xuICAgICAgICAgICAgJHNjb3BlLmFsZXJ0cy5zcGxpY2UoaW5kZXgsIDEpO1xuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5jYXJ0Q291bnQgPSAwXG5cbiAgICAgICAgJHNjb3BlLnVzZXJOYW1lID0gbnVsbFxuXG4gICAgICAgICRzY29wZS5nZXRVc2VyTmFtZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHJldHVybiBhdXRoU2VydmljZS51c2VyTmFtZSgpXG4gICAgICAgIH1cblxuICAgICAgICBDYXJ0LmdldFJlbW90ZUNhcnRDb3VudCgpLnRoZW4oZnVuY3Rpb24gKGNvdW50KSB7XG4gICAgICAgICAgICAkc2NvcGUuY2FydENvdW50ID0gY291bnRcbiAgICAgICAgfSlcblxuICAgICAgICAkc2NvcGUuJG9uKEFVVEhfRVZFTlRTLmxvZ2luU3VjY2VzcywgZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgICRzY29wZS51c2VyTmFtZSA9IGF1dGhTZXJ2aWNlLnVzZXJOYW1lKClcbiAgICAgICAgfSk7XG5cbiAgICAgICAgJHNjb3BlLiRvbihBVVRIX0VWRU5UUy5sb2dvdXRTdWNjZXNzLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgJHNjb3BlLnVzZXJOYW1lID0gYXV0aFNlcnZpY2UudXNlck5hbWUoKVxuICAgICAgICB9KTtcblxuICAgICAgICAkc2NvcGUuJG9uKEFVVEhfRVZFTlRTLmNhcnRVcGRhdGVkLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgQ2FydC5nZXRSZW1vdGVDYXJ0Q291bnQoKS50aGVuKGZ1bmN0aW9uIChjb3VudCkge1xuICAgICAgICAgICAgICAgICRzY29wZS5jYXJ0Q291bnQgPSBjb3VudFxuICAgICAgICAgICAgfSlcbiAgICAgICAgfSk7XG5cblxuICAgICAgICAkc2NvcGUuaXNMb2dnZWRJbiA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHJldHVybiBhdXRoU2VydmljZS5pc1NpZ25lZEluKClcblxuICAgICAgICB9XG5cbiAgICAgICAgJHNjb3BlLmxvZ091dCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGF1dGhTZXJ2aWNlLmxvZ091dCgpXG4gICAgICAgICAgICAkcm9vdFNjb3BlLiRicm9hZGNhc3QoQVVUSF9FVkVOVFMubG9nb3V0U3VjY2Vzcyk7XG4gICAgICAgICAgICAkcm9vdFNjb3BlLiRicm9hZGNhc3QoQVVUSF9FVkVOVFMuY2FydFVwZGF0ZWQpO1xuICAgICAgICAgICAgJGxvY2F0aW9uLnBhdGgoJy8nKVxuICAgICAgICB9XG5cbiAgICAgICAgJHNjb3BlLmdvX2Nhc2UgPSBmdW5jdGlvbiAoY2FzZV9ubykge1xuICAgICAgICAgICAgY29uc29sZS5sb2coJ2Nhc2Vfbm8nLCBjYXNlX25vKVxuICAgICAgICB9XG5cbiAgICAgICAgJHNjb3BlLm1zZyA9ICdoZWxsbyBtZSdcblxuICAgICAgICAkc2NvcGUuYXV0aGVudGljYXRlID0gZnVuY3Rpb24gKHVzZXIpIHtcblxuICAgICAgICAgICAgYXV0aFNlcnZpY2Uuc2lnbkluKHVzZXIpLnRoZW4oZnVuY3Rpb24gKHN0YXR1cykge1xuICAgICAgICAgICAgICAgIGlmIChzdGF0dXMuYXV0aGVudGljYXRlZCA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygndSByIGluJylcbiAgICAgICAgICAgICAgICAgICAgLypcbiAgICAgICAgICAgICAgICAgICAgIENhcnQuZ2V0UmVtb3RlQ2FydENvdW50KCkudGhlbihmdW5jdGlvbiAoY291bnQpIHtcbiAgICAgICAgICAgICAgICAgICAgIGlmIChjb3VudCA+IDApXG4gICAgICAgICAgICAgICAgICAgICAkbG9jYXRpb24ucGF0aCgnL3Nob3BwaW5nX2NhcnQnKVxuICAgICAgICAgICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAgICAgICAgICAgJGxvY2F0aW9uLnBhdGgoJy9kYXNoYm9hcmQnKVxuICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgICovXG5cbiAgICAgICAgICAgICAgICAgICAgQ2FydC5zdG9yZV90b19zZXJ2ZXIoKS50aGVuKGZ1bmN0aW9uIChzdGF0dXMpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdzdG9yZSB0byBzZXJ2ZXIgc3RhdHVzJywgc3RhdHVzKVxuICAgICAgICAgICAgICAgICAgICAgICAgQ2FydC5jbGVhbl9sb2NhbF9zdG9yZSgpXG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHJlZ2lzdGVyX3RyYWRlbWFyay5pbml0X2l0ZW0oKVxuICAgICAgICAgICAgICAgICAgICAgICAgJHJvb3RTY29wZS4kYnJvYWRjYXN0KEFVVEhfRVZFTlRTLmNhcnRVcGRhdGVkKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgJHJvb3RTY29wZS4kYnJvYWRjYXN0KEFVVEhfRVZFTlRTLmxvZ2luU3VjY2Vzcyk7XG4gICAgICAgICAgICAgICAgICAgICAgICBDYXJ0LmdldFJlbW90ZUNhcnRDb3VudCgpLnRoZW4oZnVuY3Rpb24gKGNvdW50KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGNvdW50ID4gMClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJGxvY2F0aW9uLnBhdGgoJy9zaG9wcGluZ19jYXJ0JylcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlbHNlXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRsb2NhdGlvbi5wYXRoKCcvZGFzaGJvYXJkJylcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXG5cbiAgICAgICAgICAgICAgICAgICAgfSlcblxuXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAkc2NvcGUuYWxlcnRzID0gW11cbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmFsZXJ0cy5wdXNoKHt0eXBlOiAnd2FybmluZycsIG1zZzogc3RhdHVzLm1lc3NhZ2V9KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUucmVnaXN0ZXIgPSBmdW5jdGlvbiAobmV3VXNlcikge1xuXG4gICAgICAgICAgICBpZiAobmV3VXNlci5wYXNzd29yZCA9PT0gbmV3VXNlci5wYXNzd29yZDIpIHtcbiAgICAgICAgICAgICAgICAkc2NvcGUuYWxlcnRzID0gW11cbiAgICAgICAgICAgICAgICBhdXRoU2VydmljZS5zaWduVXAobmV3VXNlcikudGhlbihmdW5jdGlvbiAoc3RhdHVzKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChzdGF0dXMucmVnaXN0ZXJlZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmF1dGhlbnRpY2F0ZSh7ZW1haWw6IG5ld1VzZXIuZW1haWwsIHBhc3N3b3JkOiBuZXdVc2VyLnBhc3N3b3JkfSlcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5hbGVydHMgPSBbXVxuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmFsZXJ0cy5wdXNoKHt0eXBlOiAnd2FybmluZycsIG1zZzogc3RhdHVzLm1lc3NhZ2V9KTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICRzY29wZS5hbGVydHMgPSBbXVxuICAgICAgICAgICAgICAgICRzY29wZS5hbGVydHMucHVzaCh7dHlwZTogJ3dhcm5pbmcnLCBtc2c6IFwiUGFzc3dvcmQgYW5kIHJldHlwZWQgcGFzc3dvcmQgbm90IGVxdWFsXCJ9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUuZm9yZ290X3Bhc3N3b3JkID0gZnVuY3Rpb24gKHVzZXIpIHtcbiAgICAgICAgICAgIGlmICgodXNlciA9PT0gdW5kZWZpbmVkKSB8fCAoIXVzZXIuZW1haWwpKSB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLmFsZXJ0cyA9IFtdXG4gICAgICAgICAgICAgICAgJHNjb3BlLmFsZXJ0cy5wdXNoKHt0eXBlOiAnd2FybmluZycsIG1zZzogJ1BsZWFzZSBlbnRlciBhbiBlbWFpbCBhZGRyZXNzIGZpcnN0J30pO1xuICAgICAgICAgICAgfSBlbHNlIHtcblxuICAgICAgICAgICAgICAgIGF1dGhTZXJ2aWNlLnJlcXVlc3RfcGFzc3dvcmRfcmVzZXQodXNlci5lbWFpbCkudGhlbihmdW5jdGlvbiAocmVzdWx0KSB7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5hbGVydHMgPSBbXVxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzdWx0LnN0YXR1cylcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5hbGVydHMucHVzaCh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ3N1Y2Nlc3MnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1zZzogJ1dlIGhhdmUgc2VudCBhbiBlbWFpbCB3aXRoIHBhc3N3b3JkIHJlc2V0IGluc3RydWN0aW9uLCBwbGVhc2UgY2hlY2sgeW91ciBpbmJveCdcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5hbGVydHMucHVzaCh7dHlwZTogJ3dhcm5pbmcnLCBtc2c6IHJlc3VsdC5tZXNzYWdlfSlcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5zZXRfbGFuZ3VhZ2UgPSBmdW5jdGlvbiAobGFuZ19jb2RlKSB7XG4gICAgICAgICAgICAkdHJhbnNsYXRlLnVzZShsYW5nX2NvZGUpXG4gICAgICAgIH1cblxuICAgIH1cblxufVxuXG4iLCJtb2R1bGUuZXhwb3J0cz17XG4gIFwiR19HT1wiOlwiR29cIixcbiAgXCJHX1NFTkRcIjpcIlNlbmRcIixcbiAgXCJHX0NPUFlSSUdIVFwiOlwiQWxsIHJpZ2h0cyByZXNlcnZlZC5cIixcbiAgXCJNX0xPR0lOX0hFUkVcIjpcIkxvZ2luIEhlcmVcIixcbiAgXCJNX0xPR0lOXCI6XCJMb2dpblwiLFxuICBcIk1fU0lHTlVQXCI6XCJOZXcgVXNlclwiLFxuICBcIk1fUkVDT1ZFUl9QV0RcIjpcIlJlY292ZXIgUGFzc3dvcmRcIixcbiAgXCJNX1NIT1BQSU5HX0NBUlRcIjpcIlNob3BwaW5nIENhcnRcIixcbiAgXCJNX0NBU0VfTlVNQkVSXCI6XCJQbGVhc2UgZW50ZXIgY2FzZSBudW1iZXIsIGlmIGFwcGxpY2FibGU6XCIsXG4gIFwiTV9BUFBMWVwiOlwiQXBwbHlcIixcbiAgXCJNX0hPTUVcIjpcIkhvbWVcIixcbiAgXCJNX0hPTUVfQk9YMVwiOlwiVFJBREVNQVJLRVJTIGFyZSBzcGVjaWFsaXN0cyBpbiBoYW5kbGluZyB0aGUgcmVnaXN0cmF0aW9uIG9mIHlvdXIgdHJhZGVtYXJrcyB3b3JsZHdpZGUuXCIsXG4gIFwiTV9IT01FX0JPWDJcIjpcIiBZb3VyIG9uZSBzdG9wIHNob3AgdG8gaGF2ZSB5b3VyIGJyYW5kIHJlZ2lzdGVyZWQgYnkgZXhwZXJpZW5jZWQgYXR0b3JuZXlzIGluIDE5NSBjb3VudHJpZXMgYW5kIGp1cmlzZGljdGlvbnMuXCIsXG4gIFwiTV9IT01FX1JFR19JTlwiOlwiSSB3b3VsZCBsaWtlIHRvIHJlZ2lzdGVyIGEgdHJhZGVtYXJrIGluIDpcIixcbiAgXCJNX1RSQURFTUFSS19SRUdJU1RSQVRJT05cIiA6XCJUcmFkZW1hcmsgUmVnaXN0cmF0aW9uXCIsXG4gIFwiTV9DT05UQUNUXCI6XCJDb250YWN0XCIsXG4gIFwiTV9BQk9VVFwiOlwiQWJvdXRcIixcbiAgXCJNX1RNX0NMQVNTRVNcIjpcIlRyYWRlbWFyayBDbGFzc2VzXCIsXG4gIFwiTV9URVJNU1wiOlwiVGVybXMgYW5kIENvbmRpdGlvbnNcIixcbiAgXCJNX1NFQVJDSF9OSUNFX0NMQVNTRVNcIjpcIlNlYXJjaCBOSUNFIENsYXNzZXNcIixcbiAgXCJNX0NPTlRBQ1RfVVNcIjpcIkNvbnRhY3QgdXNcIixcbiAgXCJNX0NPTlRBQ1RfTkFNRVwiOlwiTmFtZVwiLFxuICBcIk1fQ09OVEFDVF9FTUFJTFwiOlwiRW1haWwgQWRkcmVzc1wiLFxuICBcIk1fQ09OVEFDVF9URUxcIjpcIlRlbGVwaG9uZVwiLFxuICBcIk1fQ09OVEFDVF9NU0dcIjpcIk1lc3NhZ2VcIixcbiAgXCJNX0NPVEFDVF9PVVJfRU1BSUxcIjpcIkVtYWlsIFVzXCIsXG4gIFwiTV9BQk9VVF8xXCI6XCJUcmFkZW1hcmtlcnMuY29tIGlzIGEgbGVhZGluZyBpbnRlcm5hdGlvbmFsIGJyYW5kIHByb3RlY3Rpb24gY29tcGFueSB0aGF0IHNwZWNpYWxpemVzIGluIGdsb2JhbCB0cmFkZW1hcmsgYW5kIGRvbWFpbiBuYW1lIHJlZ2lzdHJhdGlvbi4gT3VyIGFjY3JlZGl0ZWQgYW5kIGludGVybmF0aW9uYWxseSByZXNwZWN0ZWQgbGF3eWVycyBoYXZlIGhlbHBlZCBudW1lcm91cyBjdXN0b21lcnMgcHJvdGVjdCB0aGVpciBicmFuZHMgYW5kIGludGVsbGVjdHVhbCBwcm9wZXJ0eSB3b3JsZHdpZGUuXCIsXG4gIFwiTV9BQk9VVF8yXCI6XCJPdXIgY2xpZW50cyByYW5nZSBmcm9tIGluZGl2aWR1YWxzIHdobyB3aXNoIHRvIHByb3RlY3Qgb25lIGlkZWEgaW4gb25lIGNvdW50cnksIHRvIGludGVsbGVjdHVhbCBwcm9wZXJ0eSBhdHRvcm5leXMgd2l0aCBtdWx0aXBsZSBjbGllbnRzLCB0byBsYXJnZSBidXNpbmVzc2VzIHRyYWRlbWFya2luZyB0aGVpciBicmFuZHMgdGhyb3VnaG91dCBlbnRpcmUgaW5kdXN0cmllcyBnbG9iYWxseS5cIixcbiAgXCJNX0FCT1VUXzNcIjpcIldlIGhhdmUgYSB0ZWFtIG9mIGxhd3llcnMsIHdpdGggeWVhcnMgb2YgZXhwZXJpZW5jZSBpbiBUcmFkZW1hcmsgUmVnaXN0cmF0aW9ucywgd2hpY2ggYWxsb3dzIHVzIHRvIGFzc3VyZSB5b3U6XCIsXG4gIFwiTV9BQk9VVF8zXzFcIjpcIkNvbXBldGVudCBwcm9mZXNzaW9uYWxzIHdpbGwgcGVyZm9ybSBhbGwgbmVjZXNzYXJ5IHByb2NlZHVyZXMgZm9yIHRoZSByZWdpc3RyYXRpb24gb2YgeW91ciB0cmFkZW1hcmtcIixcbiAgXCJNX0FCT1VUXzNfMlwiOlwiWW91IHdpbGwgcmVjZWl2ZSB0aW1lbHkgaW5mb3JtYXRpb24gcmVnYXJkaW5nIHVwZGF0ZXMgdG8gdGhlIHByb2Nlc3MgZnJvbSBkZWRpY2F0ZWQgYWNjb3VudCByZXByZXNlbnRhdGl2ZXNcIixcbiAgXCJNX0FCT1VUXzNfM1wiOlwiSWYgb2JqZWN0aW9ucyBhcmlzZSBpbiB0aGUgcmVnaXN0cmF0aW9uIHByb2Nlc3MgKG9wcG9zaXRpb24sIHJlZnVzYWwsIGV0Yy4pLCBrbm93bGVkZ2VhYmxlIGxhd3llcnMgd2lsbCBhZHZpc2UgeW91IG9uIHRoZSBhcHByb3ByaWF0ZSBjb3Vyc2Ugb2YgYWN0aW9uXCIsXG4gIFwiTV9BQk9VVF8zXzRcIjpcIllvdSBjYW4gZm9sbG93IHRoZSBpbnRlcmltIHN0YXR1cyBvZiB5b3VyIHRyYWRlbWFyayBpbiByZWFswq10aW1lLCBmcm9tIGFwcGxpY2F0aW9uIHRvIHJlZ2lzdHJhdGlvbiwgdGhyb3VnaCBhIHNlY3VyZSBtZW1iZXLigJlzIHNlY3Rpb25cIixcbiAgXCJNX0FCT1VUXzNfNVwiOlwiQWxsIGluZm9ybWF0aW9uIHByb3ZpZGVkIHRvIHVzIHdpbGwgYmUga2VwdCBpbiBhYnNvbHV0ZSBjb25maWRlbnRpYWxpdHkgwq0gcGxlYXNlIHZpZXcgb3VyIGNvbmZpZGVudGlhbGl0eSBwb2xpY3lcIixcbiAgXCJNX0FCT1VUXzZcIjpcIlwiLFxuICBcIk1fQUJPVVRfTVwiOlwiQWJvdXQgVHJhZGVtYXJrZXJzLmNvbVwiLFxuICBcIk1fQUJPVVRfV0hZXCI6XCJXaHkgVHJhZGVtYXJrZXJzLmNvbSA/XCIsXG4gIFwiTV9UUkFERU1BUktfU1RFUDFcIjpcIlN0ZXAgMTogVHJhZGVtYXJrIENvbXByZWhlbnNpdmUgU3R1ZHk6XCIsXG4gIFwiTV9UUkFERU1BUktfU1RFUDJcIjpcIlN0ZXAgMjogVHJhZGVtYXJrIFJlZ2lzdHJhdGlvbiBSZXF1ZXN0OlwiLFxuICBcIk1fVFJBREVNQVJLX1NUQVJUMVwiOlwiU3RhcnQgU3R1ZHlcIixcbiAgXCJNX1RSQURFTUFSS19TVEFSVDJcIjpcIlN0YXJ0IFJlZ2lzdHJhdGlvblwiLFxuICBcIk1fVFJBREVNQVJLXzFcIjpcIlRyYWRlbWFyayBzZWFyY2ggcmVwb3J0IHdpdGggQXR0b3JuZXkncyBhbmFseXNpcyBhYm91dCByZWdpc3RyYXRpb24gcHJvYmFiaWxpdGllcy4gVGhpcyByZXBvcnQgaXMgb3B0aW9uYWwgYnV0IGhpZ2hseSByZWNvbW1lbmRlZC5cIixcbiAgXCJNX1NJREVQQU5FTF9SRUdfUFJJQ0VcIjpcIlJlZ2lzdHJhdGlvbiBQcmljZXNcIlxuXG5cbn0iLCJtb2R1bGUuZXhwb3J0cz17XG4gICAgXCJHX0NPUFlSSUdIVFwiOlwi54mI5p2D5omA5pyJXCIsXG4gICAgXCJHX0dPXCI6XCLlvIDlp4tcIixcbiAgICBcIkdfU0VORFwiOlwi5Y+R6YCBXCIsXG4gICAgXCJNX0xPR0lOX0hFUkVcIjpcIuivt+WcqOatpOeZu+WFpVwiLFxuICAgIFwiTV9MT0dJTlwiOlwi55m75YWlXCIsXG4gICAgXCJNX1NJR05VUFwiOlwi5rOo5YaMXCIsXG4gICAgXCJNX1JFQ09WRVJfUFdEXCI6XCLlv5jorrDlr4bnoIFcIixcbiAgICBcIk1fU0hPUFBJTkdfQ0FSVFwiOlwi6LSt54mp6L2mXCIsXG4gICAgXCJNX0NBU0VfTlVNQkVSXCI6XCLlpoLmnpzmgqjmnIlDQVNFIE5VTUJFUu+8jOivt+WcqOatpOi+k+WFpSA6XCIsXG4gICAgXCJNX0FQUExZXCI6XCLotY7lm55cIixcbiAgICBcIk1fSE9NRVwiOlwi6aaW6aG1XCIsXG4gICAgXCJNX0hPTUVfQk9YMVwiOlwiVHJhZGVtYXJrZXJz5Y+v5Zyo5LiW55WM5ZCE5Zyw5Li65oKo5Yqe55CG5ZWG5qCH5rOo5YaM44CCXCIsXG4gICAgXCJNX0hPTUVfQk9YMlwiOlwi5oiR5Lus57uP6aqM5Liw5a+M55qE5ZWG5qCH5b6L5biI5YiG5biD5Zyo5LiA55m+5aSa5Liq5Zu95a626Lef5Zyw5Yy644CB5Li65oKo5o+Q5L6b5LiA56uZ5byP55qE5pyN5Yqh44CCXCIsXG4gICAgXCJNX0hPTUVfUkVHX0lOXCI6XCLor7fluK7miJHlnKjku6XkuIvnmoTlm73lrrbnmbvorrDllYbmoIcgOlwiLFxuICAgIFwiTV9UUkFERU1BUktfUkVHSVNUUkFUSU9OXCIgOlwi5ZWG5qCH55m76K6wXCIsXG4gICAgXCJNX0NPTlRBQ1RcIjpcIuiBlOezu1wiLFxuICAgIFwiTV9BQk9VVFwiOlwi5YWz5LqO5oiR5LusXCIsXG4gICAgXCJNX1RNX0NMQVNTRVNcIjpcIuWVhuagh+exu1wiLFxuICAgIFwiTV9URVJNU1wiOlwi5L2/55So5p2h5qy+XCIsXG4gICAgXCJNX1NFQVJDSF9OSUNFX0NMQVNTRVNcIjpcIuaQnOe0ok5JQ0XllYbmoIfnsbtcIixcbiAgICBcIk1fQ09OVEFDVF9VU1wiOlwi6IGU57O75oiR5LusXCIsXG4gICAgXCJNX0NPTlRBQ1RfTkFNRVwiOlwi5aeT5ZCNXCIsXG4gICAgXCJNX0NPTlRBQ1RfRU1BSUxcIjpcIueUtemCrlwiLFxuICAgIFwiTV9DT05UQUNUX1RFTFwiOlwi55S16K+dXCIsXG4gICAgXCJNX0NPTlRBQ1RfTVNHXCI6XCLlhoXlrrlcIixcbiAgICBcIk1fQ09UQUNUX09VUl9FTUFJTFwiOlwi55S16YKuXCIsXG4gICAgXCJNX0FCT1VUXzFcIjpcIlRyYWRlbWFya2Vycy5jb23mmK/kuIDlrrblhajnkIPpooblhYjnmoTlm73pmYXlk4HniYzkv53miqTlhazlj7jvvIzkuJPpl6jku47kuovlhajnkIPmgKfnmoTllYbmoIflkozln5/lkI3ms6jlhozjgILmiJHku6zorqTlj6/nmoTlkozlnKjlm73pmYXkuIrlj5fliLDlsIrph43nmoTlvovluIjlt7Lnu4/luK7liqnkvJflpJrlrqLmiLflnKjlhajnkIPojIPlm7TlhoXkv53miqTlhbblk4HniYzlkoznn6Xor4bkuqfmnYPjgIJcIixcbiAgICBcIk1fQUJPVVRfMlwiOlwi5oiR5Lus55qE5a6i5oi35LuO5Zyo5LiA5Liq5Zu95a625L+d5oqk6Ieq5bex55qE5p6E5oOz5Liq5Lq644CB5Yiw5pyJ5LyX5aSa5a6i5oi355qE55+l6K+G5Lqn5p2D5b6L5biI44CB5Yiw5Zyo5YWo55CD6IyD5Zu05YaF5YWo5pa55L2N5L+d5oqk5LuW5Lus55qE5ZOB54mM5aSn5Z6L5ZWG5Lia5py65p6E44CB5bqU5pyJ5bC95pyJLlwiLFxuICAgIFwiTV9BQk9VVF8zXCI6XCLmiJHku6zmnInkuIDkuKrlvovluIjlm6LpmJ/vvIzmi6XmnInlpJrlubTnmoTllYbmoIfms6jlhoznmoTnu4/pqozvvIzov5nkvb/lvpfmiJHku6zlj6/ku6XlkJHmgqjkv53or4HvvJpcIixcbiAgICBcIk1fQUJPVVRfM18xXCI6XCLlkIjmoLznmoTkuJPkuJrkurrlkZjlsIbkuLrmgqjllYbmoIfms6jlhozov5vooYzmiYDmnInlv4XopoHnmoTmiYvnu61cIixcbiAgICBcIk1fQUJPVVRfM18yXCI6XCLmgqjlsIbmnInliIbphY3nu5nmgqjnmoTlrqLmiLfku6PooajvvIzku47ku5bpgqPmgqjkvJrmlLbliLDljbPml7bnmoTlhbPkuo7mgqjnmoTllYbmoIfnmoTku7vkvZXkv6Hmga9cIixcbiAgICBcIk1fQUJPVVRfM18zXCI6XCLlpoLmnpzlnKjnmbvorrDov4fnqIvkuK3mnInlj43lr7nmhI/op4Hlh7rnjrDvvIjlj43lr7nvvIzmi5Lnu53nrYnvvInvvIznu4/pqozkuLDlr4znmoTlvovluIjkvJrlu7rorq7mgqjpgILlvZPnmoTooaXmlZHmlrnms5VcIixcbiAgICBcIk1fQUJPVVRfM180XCI6XCLmgqjlj6/ku6Xlrp7ml7bnmoTmjozmjqfmgqjnmoTnlLPor7fjgIHku47nlLPor7fliLDnmbvorrBcIixcbiAgICBcIk1fQUJPVVRfM181XCI6XCLmgqjmiYDmj5DkvpvnmoTmiYDmnInkv6Hmga/pg73lsIbooqvnu53lr7nkv53lr4bvvIzor7fmn6XnnIvmiJHku6znmoTkv53lr4bmlL/nrZZcIixcbiAgICBcIk1fQUJPVVRfTVwiOlwiVHJhZGVtYXJrZXJzLmNvbeeugOS7i1wiLFxuICAgIFwiTV9BQk9VVF9XSFlcIjpcIuS4uuS7gOS5iOimgemAiVRyYWRlbWFya2Vycy5jb20gP1wiLFxuICAgIFwiTV9UUkFERU1BUktfU1RFUDFcIjpcIuesrDHmraXvvJrllYbmoIfnu7zlkIjnoJTnqbZcIixcbiAgICBcIk1fVFJBREVNQVJLX1NURVAyXCI6XCLnrKwy5q2l77ya5ZWG5qCH5rOo5YaM55Sz6K+377yaXCIsXG4gICAgXCJNX1RSQURFTUFSS19TVEFSVDFcIjpcIuW8gOWni+eglOeptlwiLFxuICAgIFwiTV9UUkFERU1BUktfU1RBUlQyXCI6XCLlvIDlp4vnlLPor7dcIixcbiAgICBcIk1fVFJBREVNQVJLXzFcIjpcIuWVhuagh+ajgOe0ouaKpeWRiuS4juW+i+W4iOeahOacieWFs+azqOWGjOamgueOh+WIhuaekOOAgui/meS7veaKpeWRiuaYr+WPr+mAieeahO+8jOS9huW8uueDiOW7uuiuruOAglwiLFxuICAgIFwiTV9TSURFUEFORUxfUkVHX1BSSUNFXCI6XCLllYbmoIfnmbvorrDku7fmoLxcIlxufVxuXG5cblxuXG5cbiIsIlxuLy8gcHJpY2UgY2FsY3VsYXRvclxuXG5mdW5jdGlvbiBnZXRfcHJpY2Vfb2JqZWN0X2J5X3R5cGUodHlwZSwgcHJpY2UpIHtcbiAgICBpZiAocHJpY2UpIHtcbiAgICAgICAgaWYgKHByaWNlIGluc3RhbmNlb2YgQXJyYXkpIHtcbiAgICAgICAgICAgIGlmICh0eXBlID09PSAnd2wnIHx8IHR5cGUgPT09ICd0bWwnKVxuICAgICAgICAgICAgICAgIHJldHVybiBwcmljZVsxXTtcbiAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgICAgICByZXR1cm4gcHJpY2VbMF07XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gcHJpY2U7XG4gICAgICAgIH1cbiAgICB9XG4gICAgZWxzZVxuICAgICAgICByZXR1cm4gdW5kZWZpbmVkO1xufVxuXG5mdW5jdGlvbiBnZXRfYW1vdW50X3Byb2MoaXRlbSwgcHJpY2VzKSB7XG4gICAgdmFyIHByaWNlID0gZ2V0X3ByaWNlX29iamVjdF9ieV90eXBlKGl0ZW0udHlwZSwgcHJpY2VzKTtcbiAgICBjb25zb2xlLmxvZygncHJpY2UyMjInLCBwcmljZSk7XG4gICAgdmFyIGFtdCA9IHByaWNlLnJlZzE7XG4gICAgaWYgKCFwcmljZS5tYykge1xuICAgICAgICBmb3IgKHZhciBqID0gMTsgaiA8IGl0ZW0uY2xhc3Nlcy5sZW5ndGg7IGorKykge1xuICAgICAgICAgICAgYW10ID0gYW10ICsgcHJpY2UucmVnMjtcbiAgICAgICAgfVxuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgICAgZm9yICh2YXIgayA9IHByaWNlLm1jX251bTsgayA8IGl0ZW0uY2xhc3Nlcy5sZW5ndGg7IGsrKykge1xuICAgICAgICAgICAgYW10ID0gYW10ICsgcHJpY2UucmVnMjtcbiAgICAgICAgfVxuICAgIH1cbiAgICBpZiAoaXRlbS5jbGFpbV9wcmlvcml0eSAmJiBwcmljZS5wcmlvcml0eV9maWxpbmcpXG4gICAgICAgIGFtdCA9IGFtdCArIHByaWNlLnByaW9yaXR5X2ZpbGluZztcbiAgICByZXR1cm4gYW10O1xufVxuXG5leHBvcnRzLmdldF9hbW91bnQgPSBmdW5jdGlvbiAoaXRlbSwgcHJpY2VzKSB7XG4gICAgcmV0dXJuIGdldF9hbW91bnRfcHJvYyhpdGVtLCBwcmljZXMpO1xufTtcbiIsIlxuXCJ1c2Ugc3RyaWN0XCI7XG5cbmV4cG9ydHMuUHJvZmlsZV9FZGl0ID0gZnVuY3Rpb24gKCRzY29wZSwgJGh0dHAsICRyb3V0ZVBhcmFtcywgJGxvY2F0aW9uLCAkcm91dGUpIHtcblxuICBjb25zb2xlLmxvZygncHJvZmlsZWRpdCcpXG5cbiAgJHNjb3BlLmNvdW50cnkgPSAnQ0EnXG5cbiAgJHNjb3BlLnByb2ZpbGUgPSB7XG4gICAgcHJvZmlsZV9uYW1lIDogJ2ZyYW5rJyxcbiAgICBuYXR1cmUgOiAnQycsXG4gICAgaW5jX2NvdW50cnkgOiAnQ0EnLFxuICAgIGNvbnRhY3QgOiAnYW5nZWwnLFxuICAgIGVtYWlsIDogJ2FiY2RlQHNhbXBsZS5jb20nLFxuICAgIGNpdHk6J2NlYnUnXG4gIH1cblxuICAkc2NvcGUuc2V0X2NvdW50cnkgPSBmdW5jdGlvbihjb2RlKSB7XG4gICAgJHNjb3BlLmNvdW50cnkgPSBjb2RlXG4gICAgY29uc29sZS5sb2coY29kZSlcbiAgICAkcm91dGUucmVsb2FkKClcbiAgfVxuXG59XG4iLCJtb2R1bGUuZXhwb3J0cz17XG4gIFwiSEVBRExJTkVcIjogXCLkuK3mlofnmoTkv6Hmga9cIlxufSIsIi8vIEB0cy1jaGVja1xuXCJ1c2Ugc3RyaWN0XCI7XG5cblxuZXhwb3J0cy5DYXJ0X1N0b3JlID0gZnVuY3Rpb24gKCR3aW5kb3cpIHtcblxuICB0aGlzLmNyZWF0ZSA9IGZ1bmN0aW9uICgpIHtcbiAgfTtcblxuICB0aGlzLmRlc3Ryb3kgPSBmdW5jdGlvbiAoKSB7XG4gICAgZGVsZXRlICR3aW5kb3cuc2Vzc2lvblN0b3JhZ2UuY2FydF9pdGVtc1xuICAgIGRlbGV0ZSAkd2luZG93LnNlc3Npb25TdG9yYWdlLmNhcnRfY291bnRcbiAgICBkZWxldGUgJHdpbmRvdy5zZXNzaW9uU3RvcmFnZS5jYXJ0X3Byb2ZpbGVzXG4gIH07XG5cbiAgLy8gdGhpcyBmdW5jdGlvbiBub3JtaWFsaXplIHRoZSBwcm9maWxlcyBpbnRvIGFub3RoZXIgdGFibGVcblxuICB0aGlzLmFkZCA9IGZ1bmN0aW9uIChpdGVtKSB7XG5cbiAgICBjb25zb2xlLmxvZygnYWRkIGl0ZW0gaW4gQ2FydF9zdG9yZScpXG4gICAgaXRlbS4kJHRlbXBfa2V5ID0gZ2VuZXJhdGVRdWlja0d1aWQoKSAvLyB0aGlzIGtleSBzaG91bGQgYmUgcmVtb3ZlZCBiZWZvcmUgcG9zdGluZyB0byBzZXJ2ZXJcblxuICAgIGNvbnNvbGUubG9nKCdpdGVtIGluIHN0b3JlJywgaXRlbSlcblxuICAgIGlmIChpdGVtLnByb2ZpbGVfaWQpIHtcblxuICAgICAgY29uc29sZS5sb2coJ3VzZSBwcm9maWxlX2lkJylcblxuICAgIH0gZWxzZSBpZiAoaXRlbS5wcm9maWxlKSB7IC8vIGZ3IGJlIHN1cmUgdG8gY2hlY2sgdGhlIHByb2ZpbGUgaXMgb25seSBhIHJlZmVyZW5jZSB0byBhbiBleGlzdGluZyBwcm9maWxlXG5cbiAgICAgIHZhciB0ZW1wID0gSlNPTi5zdHJpbmdpZnkoaXRlbS5wcm9maWxlKVxuICAgICAgdmFyIHByb2ZpbGUgPSBKU09OLnBhcnNlKHRlbXApXG4gICAgICB2YXIgcHJvZmlsZV9pZCA9IGdlbmVyYXRlUXVpY2tHdWlkKClcblxuICAgICAgaXRlbS5wcm9maWxlX2lkID0gcHJvZmlsZV9pZFxuICAgICAgZGVsZXRlIGl0ZW0ucHJvZmlsZVxuXG4gICAgICB2YXIgcHJvZmlsZXNcblxuICAgICAgaWYgKCR3aW5kb3cuc2Vzc2lvblN0b3JhZ2UuY2FydF9wcm9maWxlcylcbiAgICAgICAgcHJvZmlsZXMgPSBKU09OLnBhcnNlKCR3aW5kb3cuc2Vzc2lvblN0b3JhZ2UuY2FydF9wcm9maWxlcylcbiAgICAgIGVsc2VcbiAgICAgICAgcHJvZmlsZXMgPSB7fVxuXG4gICAgICBwcm9maWxlc1twcm9maWxlX2lkXSA9IHByb2ZpbGVcblxuICAgICAgJHdpbmRvdy5zZXNzaW9uU3RvcmFnZS5jYXJ0X3Byb2ZpbGVzID0gSlNPTi5zdHJpbmdpZnkocHJvZmlsZXMpXG4gICAgfVxuXG4gICAgdmFyIGl0ZW1zXG5cbiAgICBpZiAoJHdpbmRvdy5zZXNzaW9uU3RvcmFnZS5jYXJ0X2l0ZW1zKSB7XG4gICAgICBpdGVtcyA9IEpTT04ucGFyc2UoJHdpbmRvdy5zZXNzaW9uU3RvcmFnZS5jYXJ0X2l0ZW1zKVxuICAgIH0gZWxzZVxuICAgICAgaXRlbXMgPSBbXVxuXG4gICAgaXRlbXMucHVzaChpdGVtKVxuXG4gICAgJHdpbmRvdy5zZXNzaW9uU3RvcmFnZVsnY2FydF9pdGVtcyddID0gSlNPTi5zdHJpbmdpZnkoaXRlbXMpXG4gICAgJHdpbmRvdy5zZXNzaW9uU3RvcmFnZVsnY2FydF9jb3VudCddID0gaXRlbXMubGVuZ3RoXG5cbiAgfTtcblxuICAvLyBkZWxldGUgaXRlbSBkb2VzIG5vdCBkZWxldGUgdGhlIHJlbGF0ZWQgcHJvZmlsZVxuXG4gIHRoaXMuZGVsZXRlX2l0ZW0gPSBmdW5jdGlvbiAoaXRlbSkge1xuXG4gICAgdmFyIGl0ZW1zXG4gICAgaWYgKCR3aW5kb3cuc2Vzc2lvblN0b3JhZ2UuY2FydF9pdGVtcykge1xuICAgICAgaXRlbXMgPSBKU09OLnBhcnNlKCR3aW5kb3cuc2Vzc2lvblN0b3JhZ2UuY2FydF9pdGVtcylcbiAgICB9XG5cbiAgICBpZiAoaXRlbXMpIHtcbiAgICAgIHZhciBpXG4gICAgICBmb3IgKGkgPSAwOyBpIDwgaXRlbXMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgaWYgKGl0ZW1zW2ldLiQkdGVtcF9rZXkgPT09IGl0ZW0uJCR0ZW1wX2tleSlcbiAgICAgICAgICBicmVhaztcbiAgICAgIH1cbiAgICAgIGlmIChpIDwgaXRlbXMubGVuZ3RoKSB7XG4gICAgICAgIGl0ZW1zLnNwbGljZShpLCAxKVxuICAgICAgICAkd2luZG93LnNlc3Npb25TdG9yYWdlWydjYXJ0X2l0ZW1zJ10gPSBKU09OLnN0cmluZ2lmeShpdGVtcylcbiAgICAgICAgJHdpbmRvdy5zZXNzaW9uU3RvcmFnZVsnY2FydF9jb3VudCddID0gaXRlbXMubGVuZ3RoXG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgdGhpcy51cGRhdGVfaXRlbSA9IGZ1bmN0aW9uIChpdGVtKSB7XG5cbiAgICB2YXIgaXRlbXNcbiAgICBpZiAoJHdpbmRvdy5zZXNzaW9uU3RvcmFnZS5jYXJ0X2l0ZW1zKSB7XG4gICAgICBpdGVtcyA9IEpTT04ucGFyc2UoJHdpbmRvdy5zZXNzaW9uU3RvcmFnZS5jYXJ0X2l0ZW1zKVxuICAgIH1cblxuICAgIGlmIChpdGVtcykge1xuICAgICAgdmFyIGlcbiAgICAgIGZvciAoaSA9IDA7IGkgPCBpdGVtcy5sZW5ndGg7IGkrKykge1xuICAgICAgICBpZiAoaXRlbXNbaV0uJCR0ZW1wX2tleSA9PT0gaXRlbS4kJHRlbXBfa2V5KVxuICAgICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgICAgaWYgKGkgPCBpdGVtcy5sZW5ndGgpIHtcbiAgICAgICAgaXRlbXNbaV0gPSBpdGVtXG4gICAgICAgICR3aW5kb3cuc2Vzc2lvblN0b3JhZ2VbJ2NhcnRfaXRlbXMnXSA9IEpTT04uc3RyaW5naWZ5KGl0ZW1zKVxuICAgICAgICAkd2luZG93LnNlc3Npb25TdG9yYWdlWydjYXJ0X2NvdW50J10gPSBpdGVtcy5sZW5ndGhcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICB0aGlzLmdldENvdW50ID0gZnVuY3Rpb24gKCkge1xuICAgIGlmICgkd2luZG93LnNlc3Npb25TdG9yYWdlLmNhcnRfY291bnQpXG4gICAgICByZXR1cm4gJHdpbmRvdy5zZXNzaW9uU3RvcmFnZS5jYXJ0X2NvdW50XG4gICAgZWxzZVxuICAgICAgcmV0dXJuIDBcbiAgfVxuXG4gIHRoaXMuZ2V0Q2FydCA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgcHJvZmlsZXMgPSBnZXRQcm9maWxlc0xvY2FsKClcbiAgICB2YXIgaXRlbXNcbiAgICBpZiAoJHdpbmRvdy5zZXNzaW9uU3RvcmFnZS5jYXJ0X2l0ZW1zKSB7XG4gICAgICBpdGVtcyA9IEpTT04ucGFyc2UoJHdpbmRvdy5zZXNzaW9uU3RvcmFnZS5jYXJ0X2l0ZW1zKVxuICAgIH0gZWxzZVxuICAgICAgaXRlbXMgPSBbXVxuICAgIHZhciBhbW91bnQgPSAwXG5cbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGl0ZW1zLmxlbmd0aDsgaSsrKSB7XG4gICAgICBpZiAoaXRlbXNbaV0uc2VsZWN0ZWQpXG4gICAgICAgIGFtb3VudCA9IGFtb3VudCArIGl0ZW1zW2ldLmFtb3VudFxuICAgICAgaWYgKGl0ZW1zW2ldLnByb2ZpbGVfaWQpXG4gICAgICAgIGl0ZW1zW2ldLiRwcm9maWxlX25hbWUgPSBwcm9maWxlc1tpdGVtc1tpXS5wcm9maWxlX2lkXS5wcm9maWxlX25hbWVcbiAgICB9XG5cbiAgICByZXR1cm4ge1xuICAgICAgaXRlbXM6IGl0ZW1zLFxuICAgICAgYW1vdW50OiBhbW91bnQsXG4gICAgICBfaWQ6ICdsb2NhbCdcbiAgICB9XG4gIH1cblxuICAvLyBjcmVhdGUgYSBwcm9maWxlIGluIHRoZSBsb2NhbCBzdG9yYWdlLCB1c2VkIGJ5IGNhc2VfZ2VuIGZvciBub3dcblxuICB0aGlzLmNyZWF0ZV9wcm9maWxlID0gZnVuY3Rpb24gKHByb2ZpbGUpIHtcbiAgICBjb25zb2xlLmxvZygnY3JlYXRlX3Byb2ZpbGUnLCBwcm9maWxlKVxuXG4gICAgdmFyIHRlbXAgPSBKU09OLnN0cmluZ2lmeShwcm9maWxlKVxuICAgIHZhciBwcm9maWxlID0gSlNPTi5wYXJzZSh0ZW1wKVxuICAgIHZhciBwcm9maWxlX2lkID0gZ2VuZXJhdGVRdWlja0d1aWQoKVxuXG4gICAgdmFyIHByb2ZpbGVzXG5cbiAgICBpZiAoJHdpbmRvdy5zZXNzaW9uU3RvcmFnZS5jYXJ0X3Byb2ZpbGVzKVxuICAgICAgcHJvZmlsZXMgPSBKU09OLnBhcnNlKCR3aW5kb3cuc2Vzc2lvblN0b3JhZ2UuY2FydF9wcm9maWxlcylcbiAgICBlbHNlXG4gICAgICBwcm9maWxlcyA9IHt9XG5cbiAgICBwcm9maWxlc1twcm9maWxlX2lkXSA9IHByb2ZpbGVcblxuICAgICR3aW5kb3cuc2Vzc2lvblN0b3JhZ2UuY2FydF9wcm9maWxlcyA9IEpTT04uc3RyaW5naWZ5KHByb2ZpbGVzKVxuXG4gICAgcmV0dXJuIHByb2ZpbGVfaWRcbiAgfVxuXG4gIHRoaXMuZ2V0UHJvZmlsZXMgPSBmdW5jdGlvbiAoKSB7XG4gICAgaWYgKCR3aW5kb3cuc2Vzc2lvblN0b3JhZ2UuY2FydF9wcm9maWxlcykge1xuICAgICAgdmFyIHByb2ZpbGVzID0gSlNPTi5wYXJzZSgkd2luZG93LnNlc3Npb25TdG9yYWdlLmNhcnRfcHJvZmlsZXMpXG4gICAgICB2YXIgcmV0ID0ge31cbiAgICAgIGZvciAodmFyIGtleSBpbiBwcm9maWxlcykge1xuICAgICAgICBpZiAocHJvZmlsZXMuaGFzT3duUHJvcGVydHkoa2V5KSkge1xuICAgICAgICAgIHJldFtrZXldID0gcHJvZmlsZXNba2V5XS5wcm9maWxlX25hbWVcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgcmV0dXJuIHJldFxuICAgIH0gZWxzZVxuICAgICAgcmV0dXJuIHVuZGVmaW5lZFxuICB9XG5cbiAgdGhpcy5nZXRQcm9maWxlID0gZnVuY3Rpb24gKHByb2ZpbGVfaWQpIHtcbiAgICBpZiAoJHdpbmRvdy5zZXNzaW9uU3RvcmFnZS5jYXJ0X3Byb2ZpbGVzKSB7XG4gICAgICB2YXIgcHJvZmlsZXMgPSBKU09OLnBhcnNlKCR3aW5kb3cuc2Vzc2lvblN0b3JhZ2UuY2FydF9wcm9maWxlcylcbiAgICAgIHJldHVybiBwcm9maWxlc1twcm9maWxlX2lkXVxuICAgIH0gZWxzZVxuICAgICAgcmV0dXJuIHVuZGVmaW5lZFxuXG4gICAgLypcbiAgICByZXR1cm4ge1xuICAgICAgcHJvZmlsZV9uYW1lIDogJ0phY2snXG4gICAgfSovXG4gIH1cblxuICB0aGlzLmdldEFsbFByb2ZpbGVzID0gZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiBnZXRQcm9maWxlc0xvY2FsKClcbiAgfVxuXG4gIHRoaXMuZ2V0QWxsUHJvZmlsZXNJRCA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgcGZpbGVzID0gZ2V0UHJvZmlsZXNMb2NhbCgpXG4gICAgY29uc29sZS5sb2coJ3BmaWxlcycsIHBmaWxlcylcbiAgICByZXR1cm4gT2JqZWN0LmtleXMocGZpbGVzKVxuICB9XG5cblxuICBmdW5jdGlvbiBnZW5lcmF0ZVF1aWNrR3VpZCgpIHtcbiAgICByZXR1cm4gTWF0aC5yYW5kb20oKS50b1N0cmluZygzNikuc3Vic3RyaW5nKDIsIDE1KSArXG4gICAgICBNYXRoLnJhbmRvbSgpLnRvU3RyaW5nKDM2KS5zdWJzdHJpbmcoMiwgMTUpO1xuICB9XG5cbiAgZnVuY3Rpb24gZ2V0UHJvZmlsZXNMb2NhbCgpIHtcbiAgICBpZiAoJHdpbmRvdy5zZXNzaW9uU3RvcmFnZS5jYXJ0X3Byb2ZpbGVzKSB7XG4gICAgICByZXR1cm4gSlNPTi5wYXJzZSgkd2luZG93LnNlc3Npb25TdG9yYWdlLmNhcnRfcHJvZmlsZXMpXG4gICAgfSBlbHNlXG4gICAgICByZXR1cm4ge31cbiAgfVxuXG4gIHJldHVybiB0aGlzO1xuXG59IiwiXCJ1c2Ugc3RyaWN0XCI7XG4vKmdsb2JhbCBhbmd1bGFyLCBjb25zb2xlICovXG4vKiBqc2hpbnQgZXNuZXh0OiB0cnVlICovXG5cbnZhciBjb3VudHJpZXMgPSByZXF1aXJlKCdjb3VudHJpZXMnKVxudmFyIHV0aWxpdGllcyA9IHJlcXVpcmUoJy4uL21vZHVsZXMvdXRpbGl0aWVzLmpzJylcbnJlcXVpcmUoJ3NldGltbWVkaWF0ZScpXG4vL3ZhciBjbyA9IHJlcXVpcmUoJ2NvJylcblxuaW1wb3J0IEVzNmNvbnRyb2xsZXIgZnJvbSAnLi9jb250cm9sbGVycy9FczZjb250cm9sbGVyLmpzJ1xuaW1wb3J0IFVzZXJDb250cm9sbGVyIGZyb20gJy4vY29udHJvbGxlcnMvVXNlckNvbnRyb2xsZXIuanMnXG5cbmZ1bmN0aW9uIGFwcGVuZF9vbmVfd29yZF9jb3VudHJ5X25hbWUobHN0KSB7XG4gICAgdmFyIHJsc3QgPSBbXVxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgbHN0Lmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIGxzdFtpXS5zaW1wbGVfbmFtZSA9IGxzdFtpXS5uYW1lLnJlcGxhY2UoLyAvZywgJ18nKS50b0xvd2VyQ2FzZSgpXG4gICAgICAgIHJsc3QucHVzaChsc3RbaV0pXG4gICAgfVxuICAgIHJldHVybiBybHN0XG59XG5cbi8qXG4gY2xhc3MgRXM2Y29udHJvbGxlciB7XG5cbiBjb25zdHJ1Y3Rvcigkc2NvcGUpIHtcblxuIHRoaXMuY3R4ID0gJHNjb3BlXG5cbiB0aGlzLmN0eC5tc2cgPSAnaGVsbG8gRVM2MTInXG5cbiB0aGlzLmN0eC5jbGlja19tZSA9IGZ1bmN0aW9uKG1zZykge1xuIGNvbnNvbGUubG9nKCdjbGljayBtZSAnKVxuIGNvbnNvbGUubG9nKG1zZylcbiBjb25zb2xlLmxvZygnY2xpY2sgbWUgZW5kJylcbiB9XG5cbiB9XG5cbiB9XG4gKi9cblxuXG4vL2NvbnNvbGUubG9nKCdlczY2JywgRXM2Y29udHJvbGxlcilcbi8vRXM2Y29udHJvbGxlci4kaW5qZWN0ID0gW1wiJHNjb3BlXCJdXG5cbmFuZ3VsYXIubW9kdWxlKCd0a3MuY29udHJvbGxlcnMnLCBbXSlcblxuICAgIC5jb250cm9sbGVyKCdlczZDdHJsJywgRXM2Y29udHJvbGxlcilcblxuXG4gICAgLmNvbnRyb2xsZXIoJ2VzMjZDdHJsJywgW1wiJHNjb3BlXCIsIGZ1bmN0aW9uICgkc2NvcGUpIHtcbiAgICAgICAgJHNjb3BlLm1zZyA9ICdoZWxsbydcbiAgICB9XSlcblxuXG4gICAgLmNvbnRyb2xsZXIoJ3BhZ2VDdHJsJywgW1wiJHNjb3BlXCIsIFwicGFnZV9pbmZvXCIsIGZ1bmN0aW9uICgkc2NvcGUsIHBhZ2VfaW5mbykge1xuICAgICAgICAkc2NvcGUucGFnZV9pbmZvID0gcGFnZV9pbmZvXG5cbiAgICAgICAgY29uc29sZS5sb2coJ2FzZmRhc2RmYXNkZicsIHBhZ2VfaW5mbylcbiAgICB9XSlcblxuICAgIC5jb250cm9sbGVyKCdjYXNlR2VuQ3RybCcsIFtcIiRzY29wZVwiLCBcIiRyb290U2NvcGVcIiwgXCIkcVwiLCBcIiR3aW5kb3dcIiwgXCIkbG9jYXRpb25cIiwgXCJBVVRIX0VWRU5UU1wiLCBcIkNhcnRcIiwgXCJDYXNlX2dlblwiLCBcImNhc2Vfb2JqXCIsXG4gICAgICAgIHJlcXVpcmUoJy4vd2ViX2NvbnRyb2xsZXJzX2Nhc2UnKS5jYXNlX2dlbl0pXG5cbiAgICAuY29udHJvbGxlcignY2FzZUdlbkN0cmwyJywgW1wiJHFcIiwgXCIkc2NvcGVcIiwgXCIkcm9vdFNjb3BlXCIsIFwiJHdpbmRvd1wiLCBcIiRsb2NhdGlvblwiLCBcIkFVVEhfRVZFTlRTXCIsIFwidXNlcl9wcm9maWxlXCIsXG4gICAgICAgIFwiYXV0aFNlcnZpY2VcIiwgXCJDYXJ0XCIsIFwiQ2FydF9TdG9yZVwiLCBcIkNhc2VfZ2VuXCIsIFwiY2FzZV9vYmpcIixcbiAgICAgICAgcmVxdWlyZSgnLi93ZWJfY29udHJvbGxlcnNfY2FzZScpLmNhc2VfZ2VuMl0pXG5cbiAgICAvLy5jb250cm9sbGVyKCdob21lQ3RybCcsIGZ1bmN0aW9uICgkc2NvcGUsICRpbnRlcnZhbCwgJGxvY2F0aW9uLCBUTUNvdW50cmllcywgbmV3c19mZWVkZXIsICR0cmFuc2xhdGUpIHtcblxuICAgIC5jb250cm9sbGVyKCdob21lQ3RybCcsIFtcIiRzY29wZVwiLCBcIiRpbnRlcnZhbFwiLCBcIiRsb2NhdGlvblwiLCBcIlRNQ291bnRyaWVzXCIsIFwibmV3c19mZWVkZXJcIiwgXCIkdHJhbnNsYXRlXCIsIGZ1bmN0aW9uICgkc2NvcGUsICRpbnRlcnZhbCwgJGxvY2F0aW9uLCBUTUNvdW50cmllcywgbmV3c19mZWVkZXIsICR0cmFuc2xhdGUpIHtcblxuXG4gICAgICAgICR0cmFuc2xhdGUoJ0hFQURMSU5FJykudGhlbihmdW5jdGlvbiAodHJhbnNsYXRpb24pIHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCd0cmFuc2xhdGUnLCB0cmFuc2xhdGlvbilcbiAgICAgICAgfSk7XG4gICAgICAgICRzY29wZS5jb3VudHJpZXMxID0gYXBwZW5kX29uZV93b3JkX2NvdW50cnlfbmFtZShjb3VudHJpZXMuZ2V0X2NvdW50cmllcyhbJ1VTJywgJ01YJywgJ0JSJ10pKVxuICAgICAgICAkc2NvcGUuY291bnRyaWVzMiA9IGFwcGVuZF9vbmVfd29yZF9jb3VudHJ5X25hbWUoY291bnRyaWVzLmdldF9jb3VudHJpZXMoWydFVScsICdDSCcsICdOTyddKSlcbiAgICAgICAgJHNjb3BlLmNvdW50cmllczMgPSBhcHBlbmRfb25lX3dvcmRfY291bnRyeV9uYW1lKGNvdW50cmllcy5nZXRfY291bnRyaWVzKFsnQ04nLCAnUlUnLCAnVk4nXSkpXG5cbiAgICAgICAgY29uc29sZS50YWJsZSgkc2NvcGUuY291bnRyaWVzMSlcblxuXG4gICAgICAgICRzY29wZS5qc29uSWQgPSB7XG4gICAgICAgICAgICBcIkBjb250ZXh0XCI6IFwiaHR0cDovL3NjaGVtYS5vcmdcIixcbiAgICAgICAgICAgIFwiQHR5cGVcIjogXCJQbGFjZVwiLFxuICAgICAgICAgICAgXCJnZW9cIjoge1xuICAgICAgICAgICAgICAgIFwiQHR5cGVcIjogXCJHZW9Db29yZGluYXRlc1wiLFxuICAgICAgICAgICAgICAgIFwibGF0aXR1ZGVcIjogXCI0MC43NVwiLFxuICAgICAgICAgICAgICAgIFwibG9uZ2l0dWRlXCI6IFwiNzMuOThcIlxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIFwibmFtZVwiOiBcIkVtcGlyZSBTdGF0ZSBCdWlsZGluZ1wiXG4gICAgICAgIH07XG5cblxuICAgICAgICAkc2NvcGUuaXRlbXMgPSBbXG4gICAgICAgICAgICB7IElEOiAnMDAwMDAxJywgVGl0bGU6ICdDaGljYWdvJyB9LFxuICAgICAgICAgICAgeyBJRDogJzAwMDAwMicsIFRpdGxlOiAnTmV3IFlvcmsnIH0sXG4gICAgICAgICAgICB7IElEOiAnMDAwMDAzJywgVGl0bGU6ICdXYXNoaW5ndG9uJyB9LFxuICAgICAgICBdO1xuXG4gICAgICAgICRzY29wZS50aGFua3MgPSAndGhhbmtzJ1xuICAgICAgICAkc2NvcGUuc3RhdHMgPSB1dGlsaXRpZXMuc3VwcG9ydGVkQ291bnRyaWVzKClcbiAgICAgICAgJHNjb3BlLm1hcF9yZWFkeSA9IGZhbHNlXG5cbiAgICAgICAgJHNjb3BlLnF1aWNrbGlua3MgPSBbXG4gICAgICAgICAgICB7IGNvZGU6ICdVUycsIG5hbWU6ICdVbml0ZWQgU3RhdGVzJywgdXJsOiAnL3RyYWRlbWFyay1yZWdpc3RyYXRpb24taW4tdW5pdGVkX3N0YXRlcycgfSxcbiAgICAgICAgICAgIHsgY29kZTogJ0VVJywgbmFtZTogJ0V1cm9wZScsIHVybDogJy90cmFkZW1hcmstcmVnaXN0cmF0aW9uLWluLWV1cm9wZWFuX3VuaW9uJyB9LFxuICAgICAgICAgICAgeyBjb2RlOiAnQUYnLCBuYW1lOiAnQWZyaWNhJywgdXJsOiAnL3JlZ2lvbi9BRicgfSxcbiAgICAgICAgICAgIHsgY29kZTogJ09DJywgbmFtZTogJ09jZWFuaWEnLCB1cmw6ICcvcmVnaW9uL09DJyB9LFxuICAgICAgICAgICAgeyBjb2RlOiAnTkEnLCBuYW1lOiAnTm9ydGggQW1lcmljYScsIHVybDogJy9yZWdpb24vTkEnIH0sXG4gICAgICAgICAgICB7IGNvZGU6ICdBUycsIG5hbWU6ICdBc2lhJywgdXJsOiAnL3JlZ2lvbi9BUycgfVxuICAgICAgICBdXG5cbiAgICAgICAgJHNjb3BlLnNlbGVjdGVkTGluayA9ICRzY29wZS5xdWlja2xpbmtzWzBdXG5cbiAgICAgICAgJHNjb3BlLmdvdG9fY2FzZSA9IGZ1bmN0aW9uIChjYXNlX2NvZGUpIHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdjYXNlIGNvZGUgaXMgJywgY2FzZV9jb2RlKVxuICAgICAgICAgICAgJGxvY2F0aW9uLnBhdGgoJy9jYXNlLycgKyBjYXNlX2NvZGUpXG4gICAgICAgIH1cblxuICAgICAgICAvKlxuICAgICAgICAgJHNjb3BlLiR3YXRjaCgnc2VsZWN0ZWRMaW5rMicsIGZ1bmN0aW9uIChuZXdWYWwsIG9sZFZhbCkge1xuICAgICAgICAgY29uc29sZS5sb2coJ2NoYW5nZWQnLCBuZXdWYWwsIG9sZFZhbClcbiAgICAgICAgIGlmIChuZXdWYWwpIHtcbiAgICAgICAgICRsb2NhdGlvbi5wYXRoKG5ld1ZhbC51cmwpXG4gICAgICAgICB9XG4gICAgICAgICB9KTtcbiAgICAgICAgICovXG5cbiAgICAgICAgLypcbiAgICAgICAgICRzY29wZS5mZWVkcyA9IG5ld3NfZmVlZHMyXG5cbiAgICAgICAgIGZ1bmN0aW9uIGdldF9mZWVkcygpIHtcbiAgICAgICAgIG5ld3NfZmVlZGVyLmdldF9mZWVkcyg1KS50aGVuKGZ1bmN0aW9uIChmZWVkcykge1xuICAgICAgICAgY29uc29sZS5sb2coJ2ZlZWQgcmV0dXJuJywgZmVlZHMpXG4gICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGZlZWRzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICBjb25zb2xlLmxvZyhpLCBmZWVkc1tpXSlcbiAgICAgICAgICRzY29wZS5mZWVkcy5wdXNoKGZlZWRzW2ldKVxuICAgICAgICAgfVxuICAgICAgICAgfSlcbiAgICAgICAgIH1cblxuICAgICAgICAgZnVuY3Rpb24gdXBkYXRlX2N1cnJlbnRfZmVlZCgpIHtcblxuICAgICAgICAgY29uc29sZS5sb2coJ3VwZGF0ZWYgZmVlZCcpXG4gICAgICAgICBpZiAoJHNjb3BlLmZlZWRzLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICRzY29wZS5jdXJyZW50X2ZlZWQgPSAkc2NvcGUuZmVlZHNbMF1cbiAgICAgICAgICRzY29wZS5mZWVkcy5zaGlmdCgpXG4gICAgICAgICB9IGVsc2VcbiAgICAgICAgIGdldF9mZWVkcygpXG5cblxuICAgICAgICAgfVxuICAgICAgICAgKi9cblxuICAgICAgICAvL2dldF9mZWVkcygpXG5cblxuICAgICAgICAvLyRpbnRlcnZhbCh1cGRhdGVfY3VycmVudF9mZWVkLCA1MDAwKVxuXG4gICAgICAgIC8vJGludGVydmFsKGdldF9mZWVkcywgNTAwMCk7XG5cblxuICAgIH1dKVxuXG4gICAgLmNvbnRyb2xsZXIoJ2F0dG9ybmV5c0N0cmwnLCBbXCJiaW9fZGF0YVwiLCBmdW5jdGlvbiAoYmlvX2RhdGEpIHtcbiAgICAgICAgdGhpcy5vYmogPSBiaW9fZGF0YVxuICAgIH1dKVxuXG4gICAgLmNvbnRyb2xsZXIoJ215bG9nc0N0cmwnLCBbXCIkc2NvcGVcIiwgXCIkaHR0cFwiLCBmdW5jdGlvbiAoJHNjb3BlLCAkaHR0cCkge1xuXG4gICAgICAgICRzY29wZS5tc2cgPSAnbXkgbG9ncydcblxuICAgICAgICAkaHR0cC5nZXQoJy9hcGkyL215bG9ncy8nKS5zdWNjZXNzKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICAkc2NvcGUubG9ncyA9IGRhdGFcbiAgICAgICAgICAgICRzY29wZS5yZWFkeSA9IHRydWVcbiAgICAgICAgfSlcblxuXG4gICAgfV0pXG5cblxuICAgIC5jb250cm9sbGVyKCdyZWdpb25DdHJsJywgW1wiJHNjb3BlXCIsICckcm91dGVQYXJhbXMnLCBcIlRNQ291bnRyaWVzXCIsIFwicGFnZV9pbmZvXCIsIGZ1bmN0aW9uICgkc2NvcGUsICRyb3V0ZVBhcmFtcywgVE1Db3VudHJpZXMsIHBhZ2VfaW5mbykge1xuXG4gICAgICAgIGNvbnNvbGUubG9nKCRyb3V0ZVBhcmFtcylcbiAgICAgICAgJHNjb3BlLmNvZGUgPSAkcm91dGVQYXJhbXMuY29kZVxuXG5cbiAgICAgICAgbGV0IHRhYiA9IHtcbiAgICAgICAgICAgICdFVSc6ICdFdXJvcGUnLFxuICAgICAgICAgICAgJ0FGJzogJ0FmcmljYScsXG4gICAgICAgICAgICAnT0MnOiAnT2NlYW5pYScsXG4gICAgICAgICAgICAnTkEnOiAnTm9ydGggQW1lcmljYScsXG4gICAgICAgICAgICAnQVMnOiAnQXNpYScsXG4gICAgICAgICAgICAnTUUnOiAnTWlkZGxlIEVhc3QnLFxuICAgICAgICAgICAgJ1NBJzogJ1NvdXRoIEFtZXJpY2EnLFxuICAgICAgICAgICAgJ0NBJzogJ0NlbnRyYWwgQW1lcmljYSdcbiAgICAgICAgfVxuXG4gICAgICAgIGNvbnNvbGUubG9nKCd0YWIgY29kZSAnLCB0YWJbJHNjb3BlLmNvZGVdKVxuXG4gICAgICAgIGlmICh0YWJbJHNjb3BlLmNvZGVdKSB7XG4gICAgICAgICAgICB2YXIgc2VvX3N0cmluZyA9IFwiVHJhZGVtYXJrIFJlZ2lzdHJhdGlvbiBpbiBcIiArIHRhYlskc2NvcGUuY29kZV0gKyBcIiAtIFRyYWRlTWFya2Vyc8KuLmNvbVwiXG4gICAgICAgICAgICBwYWdlX2luZm8uc2V0VGl0bGUoc2VvX3N0cmluZylcbiAgICAgICAgICAgIHBhZ2VfaW5mby5zZXRfa2V5d29yZHMoc2VvX3N0cmluZylcbiAgICAgICAgICAgIHBhZ2VfaW5mby5zZXRfZGVzY3JpcHRpb24oc2VvX3N0cmluZylcblxuICAgICAgICB9XG5cblxuICAgICAgICAvKlxuICAgICAgICAgLy8gICAgdmFyIGxzdCA9IGNvdW50cmllcy5nZXRfY291bnRyaWVzX2J5X2NvbnRuZW50KCRyb3V0ZVBhcmFtcy5jb2RlKVxuICAgICAgICAgdmFyIGxzdCA9IHV0aWxpdGllcy5nZXRfY291bnRyaWVzX2J5X2NvbnRuZW50KCRyb3V0ZVBhcmFtcy5jb2RlKVxuXG4gICAgICAgICB2YXIgbnVtcyA9IE1hdGguY2VpbChsc3QubGVuZ3RoIC8gMylcblxuICAgICAgICAgY29uc29sZS5sb2coJ2xzdCcsIGxzdClcbiAgICAgICAgIGNvbnNvbGUubG9nKG51bXMsIGxzdC5sZW5ndGgpXG4gICAgICAgICAvLyAgICAkc2NvcGUuY291bnRyaWVzMSA9IGNvdW50cmllcy5nZXRfY291bnRyaWVzKFsnVVMnLCAnTVgnLCAnQlInXSlcbiAgICAgICAgICRzY29wZS5jb3VudHJpZXMxID0gbHN0LnNsaWNlKDAsIG51bXMpICAgLy8gMCAxM1xuICAgICAgICAgJHNjb3BlLmNvdW50cmllczIgPSBsc3Quc2xpY2UobnVtcyAsIDIqIG51bXMgICkgLy8gMTRcbiAgICAgICAgICRzY29wZS5jb3VudHJpZXMzID0gbHN0LnNsaWNlKDIqbnVtcyAsIDMqIG51bXMgIClcblxuICAgICAgICAgJHNjb3BlLnRoYW5rcyA9ICd0aGFua3MnXG4gICAgICAgICAqL1xuXG4gICAgfV0pXG5cbiAgICAuY29udHJvbGxlcignNDA0Q3RybCcsIFsnJHNjb3BlJywgJyRyb3V0ZVBhcmFtcycsICdEQlN0b3JlJywgZnVuY3Rpb24gKCRzY29wZSwgJHJvdXRlUGFyYW1zLCBEQlN0b3JlKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKCRyb3V0ZVBhcmFtcylcblxuICAgICAgICBpZiAoJHJvdXRlUGFyYW1zLmtleSkge1xuICAgICAgICAgICAgJHNjb3BlLnJlc3BvbnNlID0gREJTdG9yZS5nZXQoJHJvdXRlUGFyYW1zLmtleSlcbiAgICAgICAgfVxuICAgICAgICBlbHNlXG4gICAgICAgICAgICAkc2NvcGUucmVzcG9uc2UgPSAnU29ycnksIHBhZ2Ugbm90IGZvdW5kJ1xuICAgIH1dKVxuXG4gICAgLmNvbnRyb2xsZXIoJ0hlYWRlckNvbnRyb2xsZXInLCBbXCIkc2NvcGVcIiwgXCIkbG9jYXRpb25cIiwgXCIkcm9vdFNjb3BlXCIsIFwiYXV0aFNlcnZpY2VcIiwgXCJDYXJ0XCIsIFwiQVVUSF9FVkVOVFNcIiwgJ1RNX01FTlUnLFxuICAgICAgICBmdW5jdGlvbiAoJHNjb3BlLCAkbG9jYXRpb24sICRyb290U2NvcGUsIGF1dGhTZXJ2aWNlLCBDYXJ0LCBBVVRIX0VWRU5UUywgVE1fTUVOVSkge1xuXG4gICAgICAgICAgICAkc2NvcGUudG1fbWVudSA9IFRNX01FTlVcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCRzY29wZS50bV9tZW51KVxuXG4gICAgICAgICAgICBjb25zb2xlLmxvZygnQUFBQUFBQUFBQUFBQUFBQUEnLCAkbG9jYXRpb24ucGF0aCgpKVxuXG4gICAgICAgICAgICAkc2NvcGUuaW5pdCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnaW5pdHRpbmcnKVxuICAgICAgICAgICAgICAgIGlmIChhdXRoU2VydmljZS5pc1NpZ25lZEluKCkpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKCRzY29wZS51c2VyTmFtZSA9PT0gbnVsbClcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS51c2VyTmFtZSA9IGF1dGhTZXJ2aWNlLnVzZXJOYW1lKClcbiAgICAgICAgICAgICAgICAgICAgaWYgKCgkc2NvcGUuY2FydENvdW50ID09PSAwKSAmJiAoYXV0aFNlcnZpY2UuaXNTaWduZWRJbigpKSlcbiAgICAgICAgICAgICAgICAgICAgICAgIENhcnQuZ2V0UmVtb3RlQ2FydENvdW50KCkudGhlbihmdW5jdGlvbiAoY291bnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUuY2FydENvdW50ID0gY291bnRcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAkc2NvcGUuaXNCcm93c2VyU3VwcG9ydGVkID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIHJldHVybiB1dGlsaXRpZXMuc3VwcG9ydGVkQnJvd3NlcihuYXZpZ2F0b3IudXNlckFnZW50LCAnYW5ndWxhcicpXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICRzY29wZS5pc0FjdGl2ZSA9IGZ1bmN0aW9uICh2aWV3TG9jYXRpb24pIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnaXNhY3RpdmUnLCB2aWV3TG9jYXRpb24gPT09ICRsb2NhdGlvbi5wYXRoKCksICRsb2NhdGlvbi5wYXRoKCkpXG4gICAgICAgICAgICAgICAgcmV0dXJuIHZpZXdMb2NhdGlvbiA9PT0gJGxvY2F0aW9uLnBhdGgoKTtcbiAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgICRzY29wZS5pc0xvZ2dlZEluID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIHJldHVybiBhdXRoU2VydmljZS5pc1NpZ25lZEluKClcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgJHNjb3BlLmxvZ091dCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICBhdXRoU2VydmljZS5sb2dPdXQoKVxuICAgICAgICAgICAgICAgICRyb290U2NvcGUuJGJyb2FkY2FzdChBVVRIX0VWRU5UUy5sb2dvdXRTdWNjZXNzKTtcbiAgICAgICAgICAgICAgICAkcm9vdFNjb3BlLiRicm9hZGNhc3QoQVVUSF9FVkVOVFMuY2FydFVwZGF0ZWQpO1xuICAgICAgICAgICAgICAgICRsb2NhdGlvbi5wYXRoKCcvJylcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgJHNjb3BlLnVzZXJOYW1lID0gbnVsbFxuICAgICAgICAgICAgJHNjb3BlLmNhcnRDb3VudCA9IDBcbiAgICAgICAgICAgIENhcnQuZ2V0UmVtb3RlQ2FydENvdW50KCkudGhlbihmdW5jdGlvbiAoY291bnQpIHtcbiAgICAgICAgICAgICAgICAkc2NvcGUuY2FydENvdW50ID0gY291bnRcbiAgICAgICAgICAgIH0pXG5cblxuICAgICAgICAgICAgJHNjb3BlLiRvbihBVVRIX0VWRU5UUy5sb2dpblN1Y2Nlc3MsIGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLnVzZXJOYW1lID0gYXV0aFNlcnZpY2UudXNlck5hbWUoKVxuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICRzY29wZS4kb24oQVVUSF9FVkVOVFMubG9nb3V0U3VjY2VzcywgZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgICAgICAkc2NvcGUudXNlck5hbWUgPSBhdXRoU2VydmljZS51c2VyTmFtZSgpXG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgJHNjb3BlLiRvbihBVVRIX0VWRU5UUy5jYXJ0VXBkYXRlZCwgZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgICAgICBDYXJ0LmdldFJlbW90ZUNhcnRDb3VudCgpLnRoZW4oZnVuY3Rpb24gKGNvdW50KSB7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5jYXJ0Q291bnQgPSBjb3VudFxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgJHNjb3BlLmluaXQoKVxuXG4gICAgICAgICAgICAkc2NvcGUuZ290b19jYXNlID0gZnVuY3Rpb24gKGNhc2VfY29kZSkge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdjYXNlIGNvZGUgaXMgJywgY2FzZV9jb2RlKVxuICAgICAgICAgICAgICAgIGlmIChjYXNlX2NvZGUgIT09IHVuZGVmaW5lZClcbiAgICAgICAgICAgICAgICAgICAgJGxvY2F0aW9uLnBhdGgoJy9jYXNlLycgKyBjYXNlX2NvZGUpXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgfV0pXG5cbiAgICAuY29udHJvbGxlcignY2xhc3Nlc0N0cmwnLCBbJyRzY29wZScsICckaHR0cCcsIGZ1bmN0aW9uICgkc2NvcGUsICRodHRwKSB7XG5cbiAgICAgICAgJHNjb3BlLnJlYWR5ID0gdHJ1ZVxuXG4gICAgICAgICRodHRwLmdldCgnL2FwaTIvY3JfY2xhc3Nlcy8nKS5zdWNjZXNzKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZygnbGlzdGluZyBjbGFzc2VzJylcbiAgICAgICAgICAgICRzY29wZS5jbGFzc2VzID0gZGF0YVxuICAgICAgICAgICAgJHNjb3BlLnJlYWR5ID0gdHJ1ZVxuICAgICAgICB9KVxuXG4gICAgfV0pXG5cbiAgICAuY29udHJvbGxlcignY2FydENvbnRyb2xsZXInLCBbJyRzY29wZScsICckaHR0cCcsICckcm9vdFNjb3BlJywgJyRsb2NhdGlvbicsICdDYXJ0JywgJ0FVVEhfRVZFTlRTJywgJyRtb2RhbCcsXG4gICAgICAgIHJlcXVpcmUoJy4vd2ViX2NvbnRyb2xsZXJzX2NhcnQnKS5jYXJ0XSlcblxuICAgIC5jb250cm9sbGVyKCdjYXJ0VGhhbmtzQ29udHJvbGxlcicsIFsnJHNjb3BlJywgJyRyb3V0ZVBhcmFtcycsXG4gICAgICAgIGZ1bmN0aW9uICgkc2NvcGUsICRyb3V0ZVBhcmFtcykge1xuXG4gICAgICAgICAgICAkc2NvcGUub3JkZXJfa2V5ID0gJHJvdXRlUGFyYW1zLm9yZGVyX2tleVxuXG4gICAgICAgIH1dKVxuXG4gICAgLmNvbnRyb2xsZXIoJ01vZGFsSW5zdGFuY2VDdHJsMicsIFsnJHNjb3BlJywgJyRtb2RhbEluc3RhbmNlJywgJ2RldGFpbHMnLFxuICAgICAgICBmdW5jdGlvbiAoJHNjb3BlLCAkbW9kYWxJbnN0YW5jZSwgZGV0YWlscykge1xuICAgICAgICAgICAgJHNjb3BlLmRldGFpbHMgPSBkZXRhaWxzO1xuXG4gICAgICAgICAgICAkc2NvcGUub2sgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgJG1vZGFsSW5zdGFuY2UuY2xvc2UoKTtcbiAgICAgICAgICAgIH07XG5cblxuICAgICAgICAgICAgJHNjb3BlLmNhbmNlbCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAvLyAgICAgICAgJG1vZGFsSW5zdGFuY2UuZGlzbWlzcygnY2FuY2VsJyk7XG4gICAgICAgICAgICB9O1xuXG4gICAgICAgIH1dKVxuXG4gICAgLmNvbnRyb2xsZXIoJ29yZGVyRGV0YWlsQ29udHJvbGxlcicsIFsnJHNjb3BlJywgJyRodHRwJywgJyRyb290U2NvcGUnLCAnJHJvdXRlUGFyYW1zJywgJ0NhcnQnLCAnU2Vzc2lvbicsXG4gICAgICAgICdBVVRIX0VWRU5UUycsICdPcmRlcnMnLFxuICAgICAgICBmdW5jdGlvbiAoJHNjb3BlLCAkaHR0cCwgJHJvb3RTY29wZSwgJHJvdXRlUGFyYW1zLCBDYXJ0LCBTZXNzaW9uLCBBVVRIX0VWRU5UUywgT3JkZXJzKSB7XG5cbiAgICAgICAgICAgICRzY29wZS5yZWYgPSAkcm91dGVQYXJhbXMuaWRcblxuICAgICAgICAgICAgJGh0dHAuZ2V0KCcvYXBpL29yZGVyLycgKyAkcm91dGVQYXJhbXMuaWQpLnN1Y2Nlc3MoZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnb3JkZXInLCBkYXRhKVxuICAgICAgICAgICAgICAgICRzY29wZS5kZXRhaWxzID0gZGF0YS5kYXRhXG4gICAgICAgICAgICB9KVxuXG4gICAgICAgICAgICAvKlxuICAgICAgICAgICAgJHNjb3BlLmdldF9yZWNlaXB0ID0gZnVuY3Rpb24gKG9yZGVyX2lkKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2cob3JkZXJfaWQpXG4gICAgICAgICAgICAgICAgJGh0dHAuZ2V0KCcvYXBpL3JlY2VpcHQvJyArIG9yZGVyX2lkKS5zdWNjZXNzKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGEpXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgICovXG5cbiAgICAgICAgfV0pXG5cbiAgICAuY29udHJvbGxlcignb3JkZXJzQ29udHJvbGxlcicsIFsnJHNjb3BlJywgJyRodHRwJywgJyRyb290U2NvcGUnLCAnJHJvdXRlUGFyYW1zJywgJ0NhcnQnLCAnU2Vzc2lvbicsXG4gICAgICAgICdBVVRIX0VWRU5UUycsICdPcmRlcnMnLFxuICAgICAgICBmdW5jdGlvbiAoJHNjb3BlLCAkaHR0cCwgJHJvb3RTY29wZSwgJHJvdXRlUGFyYW1zLCBDYXJ0LCBTZXNzaW9uLCBBVVRIX0VWRU5UUywgT3JkZXJzKSB7XG5cbiAgICAgICAgICAgICRzY29wZS5wYWdlU2l6ZSA9IDEwO1xuXG4gICAgICAgICAgICAkc2NvcGUuc2V0RGF0YVNvdXJjZSA9IGZ1bmN0aW9uIChwYWdlX25vKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2cocGFnZV9ubylcbiAgICAgICAgICAgICAgICAkc2NvcGUub3JkZXJzID0gT3JkZXJzLnF1ZXJ5KHsgcGFnZV9ubzogcGFnZV9ubywgcGFnZV9zaXplOiAxMCB9LCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgJHNjb3BlLnNldE51bWJlck9mUGFnZXMgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ2luaXQnKVxuICAgICAgICAgICAgICAgICRodHRwLmdldCgnL2FwaS9vcmRlcnNfY291bnQnKVxuICAgICAgICAgICAgICAgICAgICAuc3VjY2VzcyhmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm51bWJlck9mUGFnZXMyID0gTWF0aC5jZWlsKGRhdGEuY291bnQgLyAkc2NvcGUucGFnZVNpemUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJHNjb3BlLm51bWJlck9mUGFnZXMyKVxuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAkc2NvcGUuY3VycmVudFBhZ2UgPSAkcm91dGVQYXJhbXMucGFnZV9ubyA/ICRyb3V0ZVBhcmFtcy5wYWdlX25vIC0gMSA6IDBcbiAgICAgICAgICAgICRzY29wZS5zZXREYXRhU291cmNlKCRzY29wZS5jdXJyZW50UGFnZSkgIC8vIGdvb2QgZm9yIGZpcnN0IHBhZ2VcblxuICAgICAgICB9XSlcblxuXG4gICAgLmNvbnRyb2xsZXIoJ3JlZ2lzdHJhdGlvbnNDb250cm9sbGVyJywgWyckc2NvcGUnLCAnJGh0dHAnLCAnJHJvb3RTY29wZScsICckcm91dGVQYXJhbXMnLFxuICAgICAgICAnQ2FydCcsICdTZXNzaW9uJywgJ0FVVEhfRVZFTlRTJywgJ1JlZ2lzdHJhdGlvbnMnLFxuICAgICAgICBmdW5jdGlvbiAoJHNjb3BlLCAkaHR0cCwgJHJvb3RTY29wZSwgJHJvdXRlUGFyYW1zLCBDYXJ0LCBTZXNzaW9uLCBBVVRIX0VWRU5UUywgUmVnaXN0cmF0aW9ucykge1xuXG4gICAgICAgICAgICAkc2NvcGUucGFnZVNpemUgPSAxMDtcblxuICAgICAgICAgICAgJHNjb3BlLnNldERhdGFTb3VyY2UgPSBmdW5jdGlvbiAocGFnZV9ubykge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHBhZ2Vfbm8pXG4gICAgICAgICAgICAgICAgJHNjb3BlLnJlZ2lzdHJhdGlvbnMgPSBSZWdpc3RyYXRpb25zLnF1ZXJ5KHsgcGFnZV9ubzogcGFnZV9ubywgcGFnZV9zaXplOiAxMCB9LCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgJHNjb3BlLnNldE51bWJlck9mUGFnZXMgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ2luaXQnKVxuICAgICAgICAgICAgICAgICRodHRwLmdldCgnL2FwaS9yZWdpc3RyYXRpb25zX2NvdW50JylcbiAgICAgICAgICAgICAgICAgICAgLnN1Y2Nlc3MoZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5udW1iZXJPZlBhZ2VzMiA9IE1hdGguY2VpbChkYXRhLmNvdW50IC8gJHNjb3BlLnBhZ2VTaXplKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCRzY29wZS5udW1iZXJPZlBhZ2VzMilcbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgJHNjb3BlLmN1cnJlbnRQYWdlID0gJHJvdXRlUGFyYW1zLnBhZ2Vfbm8gPyAkcm91dGVQYXJhbXMucGFnZV9ubyAtIDEgOiAwXG4gICAgICAgICAgICAkc2NvcGUuc2V0RGF0YVNvdXJjZSgkc2NvcGUuY3VycmVudFBhZ2UpICAvLyBnb29kIGZvciBmaXJzdCBwYWdlXG5cbiAgICAgICAgfV0pXG5cbiAgICAuY29udHJvbGxlcigncmVnaXN0cmF0aW9uRGV0YWlsQ29udHJvbGxlcicsIFsnJHNjb3BlJywgJyRodHRwJywgJyRyb290U2NvcGUnLCAnJHJvdXRlUGFyYW1zJyxcbiAgICAgICAgJ0NhcnQnLCAnU2Vzc2lvbicsICdBVVRIX0VWRU5UUycsICdSZWdpc3RyYXRpb25zJyxcbiAgICAgICAgZnVuY3Rpb24gKCRzY29wZSwgJGh0dHAsICRyb290U2NvcGUsICRyb3V0ZVBhcmFtcywgQ2FydCwgU2Vzc2lvbiwgQVVUSF9FVkVOVFMsIFJlZ2lzdHJhdGlvbnMpIHtcblxuICAgICAgICAgICAgY29uc29sZS5sb2coJ3h4JywgJHJvdXRlUGFyYW1zLmlkKVxuICAgICAgICAgICAgJHNjb3BlLnJlYWR5ID0gZmFsc2VcblxuICAgICAgICAgICAgaWYgKCRyb3V0ZVBhcmFtcy5pZCkge1xuICAgICAgICAgICAgICAgICRzY29wZS5yZWYgPSAkcm91dGVQYXJhbXMuaWRcblxuICAgICAgICAgICAgICAgICRodHRwLmdldCgnL2FwaS9yZWdpc3RyYXRpb24vJyArICRzY29wZS5yZWYpLnN1Y2Nlc3MoZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZGF0YSlcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmRldGFpbHMgPSBkYXRhLmRhdGFcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnJlYWR5ID0gdHJ1ZVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgfV0pXG5cbiAgICAuY29udHJvbGxlcignaGVscF9pbmZvJywgWyckc2NvcGUnLCAnJHJvdXRlUGFyYW1zJywgJyRsb2NhdGlvbicsICckbW9kYWwnLCAnJGludGVydmFsJywgJ25ld3NfZmVlZGVyJyxcbiAgICAgICAgZnVuY3Rpb24gKCRzY29wZSwgJHJvdXRlUGFyYW1zLCAkbG9jYXRpb24sICRtb2RhbCwgJGludGVydmFsLCBuZXdzX2ZlZWRlcikge1xuXG4gICAgICAgICAgICBjb25zb2xlLmxvZygnaGVscGluZm8nLCAkbG9jYXRpb24ucGF0aCgpKVxuICAgICAgICAgICAgdmFyIGNvdW50cnlcbiAgICAgICAgICAgIGlmICgkcm91dGVQYXJhbXMuY291bnRyeSlcbiAgICAgICAgICAgICAgICBjb3VudHJ5ID0gJHJvdXRlUGFyYW1zLmNvdW50cnlcbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIHZhciB0ZW1wID0gY291bnRyaWVzLmdldF9jb3VudHJ5X2J5X3JlZ2V4KC9eXFwvdHJhZGVtYXJrLXJlZ2lzdHJhdGlvbi1pbi0oXFx3KykvaSwgJGxvY2F0aW9uLnBhdGgoKSlcbiAgICAgICAgICAgICAgICBpZiAodGVtcClcbiAgICAgICAgICAgICAgICAgICAgY291bnRyeSA9IHRlbXAuY29kZVxuICAgICAgICAgICAgfVxuXG5cbiAgICAgICAgICAgIGlmIChjb3VudHJ5KVxuICAgICAgICAgICAgICAgICRzY29wZS5jb3VudHJ5X2ZsYWcgPSBjb3VudHJ5LnRvTG93ZXJDYXNlKCkgKyAnLnBuZydcblxuICAgICAgICAgICAgJHNjb3BlLnBvcHVwID0gZnVuY3Rpb24gKGluZm8pIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygkcm91dGVQYXJhbXMpXG4gICAgICAgICAgICAgICAgdmFyIGluZm8yID0gaW5mbyArIGNvdW50cnlcbiAgICAgICAgICAgICAgICAkbW9kYWwub3Blbih7XG4gICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlVXJsOiAncGFydGlhbHMvbW9kYWxfaW5mby5odG1sJyxcbiAgICAgICAgICAgICAgICAgICAgd2luZG93Q2xhc3M6ICdhcHAtbW9kYWwtd2luZG93JyxcbiAgICAgICAgICAgICAgICAgICAgY29udHJvbGxlcjogJ01vZGFsSW5mb0N0cmwnLCByZXNvbHZlOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpbmZvX2tleTogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBpbmZvMlxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICAkc2NvcGUuZmVlZHMgPSBbXVxuXG4gICAgICAgICAgICBmdW5jdGlvbiBnZXRfZmVlZHMoKSB7XG4gICAgICAgICAgICAgICAgbmV3c19mZWVkZXIuZ2V0X2ZlZWRzKDEpLnRoZW4oZnVuY3Rpb24gKGZlZWQpIHtcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmZlZWRzLnB1c2goZmVlZClcbiAgICAgICAgICAgICAgICAgICAgaWYgKCRzY29wZS5mZWVkcy5sZW5ndGggPiAzKVxuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmZlZWRzLnNoaWZ0KClcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBnZXRfZmVlZHMoKVxuXG4gICAgICAgICAgICAvLyAgICAkaW50ZXJ2YWwoZ2V0X2ZlZWRzLCAxMDAwMCk7ICAvL2Z3ISB1bmNvbW1lbnQgd2hlbiBkZXBsb3llZFxuXG4gICAgICAgIH1dKVxuXG4gICAgLmNvbnRyb2xsZXIoJ1VzZXJDb250cm9sbGVyJywgVXNlckNvbnRyb2xsZXIpXG5cbiAgICAuY29udHJvbGxlcignZGFzaGJvYXJkQ3RybCcsIFsnJHNjb3BlJywgJyRodHRwJywgJ2F1dGhTZXJ2aWNlJywgJ1Nlc3Npb24nLFxuICAgICAgICBmdW5jdGlvbiAoJHNjb3BlLCAkaHR0cCwgYXV0aFNlcnZpY2UsIFNlc3Npb24pIHtcblxuICAgICAgICAgICAgJHNjb3BlLmFsZXJ0cyA9IFtdO1xuXG4gICAgICAgICAgICAkc2NvcGUuY2xvc2VBbGVydCA9IGZ1bmN0aW9uIChpbmRleCkge1xuICAgICAgICAgICAgICAgICRzY29wZS5hbGVydHMuc3BsaWNlKGluZGV4LCAxKTtcbiAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgICRodHRwLmdldCgnL2FwaS9teWFjY291bnQvJykuc3VjY2VzcyhmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGEpXG4gICAgICAgICAgICAgICAgJHNjb3BlLnVzZXIgPSBkYXRhXG4gICAgICAgICAgICAgICAgJHNjb3BlLnJlYWR5ID0gdHJ1ZVxuICAgICAgICAgICAgfSlcblxuICAgICAgICAgICAgJHNjb3BlLnJlc2V0X3Bhc3N3b3JkID0gZnVuY3Rpb24gKCkge1xuXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ3Jlc2V0IHBhc3N3b3JkJywgU2Vzc2lvbi5nZXRVc2VySUQoKSlcblxuICAgICAgICAgICAgICAgIGF1dGhTZXJ2aWNlLnJlcXVlc3RfcGFzc3dvcmRfcmVzZXQoU2Vzc2lvbi5nZXRVc2VySUQoKSkudGhlbihmdW5jdGlvbiAocmVzdWx0KSB7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5hbGVydHMgPSBbXVxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzdWx0LnN0YXR1cylcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5hbGVydHMucHVzaCh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ3N1Y2Nlc3MnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1zZzogJ1dlIGhhdmUgc2VudCBhbiBlbWFpbCB3aXRoIHBhc3N3b3JkIHJlc2V0IGluc3RydWN0aW9uLCBwbGVhc2UgY2hlY2sgeW91ciBpbmJveCdcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5hbGVydHMucHVzaCh7IHR5cGU6ICd3YXJuaW5nJywgbXNnOiByZXN1bHQubWVzc2FnZSB9KVxuXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH1cbiAgICAgICAgfV0pXG5cbiAgICAuY29udHJvbGxlcignc2VhcmNoX2NsYXNzZXNDb250cm9sbGVyJywgWyckc2NvcGUnLCAnU2VhcmNoX2NsYXNzZXMnLFxuICAgICAgICBmdW5jdGlvbiAoJHNjb3BlLCBTZWFyY2hfY2xhc3Nlcykge1xuXG4gICAgICAgICAgICAkc2NvcGUubXNnID0gJ3Jlc3VsdCBlbXB0eSB5ZXQnXG5cbiAgICAgICAgICAgICRzY29wZS5zZWFyY2hfbm93ID0gZnVuY3Rpb24gKHNlYXJjaCkge1xuICAgICAgICAgICAgICAgICRzY29wZS5oYXNSZXN1bHQgPSBmYWxzZVxuICAgICAgICAgICAgICAgIFNlYXJjaF9jbGFzc2VzLnNlYXJjaChzZWFyY2gpLnRoZW4oZnVuY3Rpb24gKHJlc3VsdCkge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhyZXN1bHQpXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXN1bHQuc3RhdHVzKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUucmVzdWx0ID0gcmVzdWx0LnJlc3VsdFxuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmhhc1Jlc3VsdCA9IHRydWVcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICB9XG4gICAgICAgIH1dKVxuXG4gICAgLy8gUGxlYXNlIG5vdGUgdGhhdCBJbnN0YW5jZSByZXByZXNlbnRzIGEgbW9kYWwgd2luZG93IChpbnN0YW5jZSkgZGVwZW5kZW5jeS5cbiAgICAvLyBJdCBpcyBub3QgdGhlIHNhbWUgYXMgdGhlICRtb2RhbCBzZXJ2aWNlIHVzZWQgYWJvdmUuXG5cbiAgICAuY29udHJvbGxlcignTW9kYWxJbnN0YW5jZUN0cmwnLCBbXCIkc2NvcGVcIiwgXCIkbW9kYWxJbnN0YW5jZVwiLCBcIlNlYXJjaF9jbGFzc2VzXCIsIGZ1bmN0aW9uICgkc2NvcGUsICRtb2RhbEluc3RhbmNlLCBTZWFyY2hfY2xhc3Nlcykge1xuXG4gICAgICAgIC8vICAkc2NvcGUuaXRlbXMgPSBpdGVtcztcbiAgICAgICAgJHNjb3BlLnNlbGVjdGVkID0ge1xuICAgICAgICAgICAgLy8gICAgaXRlbTogJHNjb3BlLml0ZW1zWzBdXG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLm9rID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgY29uc29sZS5sb2coJHNjb3BlLnJlc3VsdClcblxuICAgICAgICAgICAgaWYgKCRzY29wZS5yZXN1bHQgPT09IHVuZGVmaW5lZClcbiAgICAgICAgICAgICAgICAkbW9kYWxJbnN0YW5jZS5jbG9zZShbXSlcbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIHZhciBjbGFzc2VzX3NlbGVjdGVkID0gW11cbiAgICAgICAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8ICRzY29wZS5yZXN1bHQubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGl0ZW0gPSAkc2NvcGUucmVzdWx0W2ldXG5cbiAgICAgICAgICAgICAgICAgICAgdmFyIHNlbGVjdGVkID0gaXRlbS5iYXNpY3MuZmlsdGVyKGZ1bmN0aW9uIChlbGVtZW50KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZWxlbWVudC5zZWxlY3RlZFxuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhzZWxlY3RlZCwgc2VsZWN0ZWQubGVuZ3RoKVxuICAgICAgICAgICAgICAgICAgICBpZiAoc2VsZWN0ZWQubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gdXNlIG9ubHkgdGhlIGZpcnN0IGl0ZW0gc2VsZWN0ZWQsIGFzIGFsbCBiZWxvbmcgdG8gdGhlIHNhbWUgY2xhc3NcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzZXNfc2VsZWN0ZWQucHVzaCh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29kZTogaXRlbS5jb2RlLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJhc2ljX251bWJlcjogc2VsZWN0ZWRbMF0uYm51bWJlcixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBiYXNpY19kZXNjOiBzZWxlY3RlZFswXS5iZGVzY1xuICAgICAgICAgICAgICAgICAgICAgICAgfSlcblxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICRtb2RhbEluc3RhbmNlLmNsb3NlKGNsYXNzZXNfc2VsZWN0ZWQpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5jYW5jZWwgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAkbW9kYWxJbnN0YW5jZS5kaXNtaXNzKCdjYW5jZWwnKTtcbiAgICAgICAgfTtcblxuXG4gICAgICAgICRzY29wZS5zZWFyY2hfbm93ID0gZnVuY3Rpb24gKHNlYXJjaF90ZXh0KSB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZygnc2VhcmMgbm93JylcbiAgICAgICAgICAgIFNlYXJjaF9jbGFzc2VzLnNlYXJjaChzZWFyY2hfdGV4dCkudGhlbihmdW5jdGlvbiAocmVzdWx0KSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2cocmVzdWx0KVxuICAgICAgICAgICAgICAgIGlmIChyZXN1bHQuc3RhdHVzKSB7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5yZXN1bHQgPSByZXN1bHQucmVzdWx0XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5oYXNSZXN1bHQgPSB0cnVlXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSlcbiAgICAgICAgfVxuXG5cbiAgICB9XSlcblxuICAgIC5jb250cm9sbGVyKCdNb2RhbEluZm9DdHJsJywgW1wiJHNjb3BlXCIsIFwiJHNjZVwiLCBcIiRtb2RhbEluc3RhbmNlXCIsICdpbmZvX2tleScsIFwiaGVscF9pbmZvXCIsIGZ1bmN0aW9uICgkc2NvcGUsICRzY2UsICRtb2RhbEluc3RhbmNlLCBpbmZvX2tleSwgaGVscF9pbmZvKSB7XG5cbiAgICAgICAgLy8gICRzY29wZS5pdGVtcyA9IGl0ZW1zO1xuICAgICAgICAvLyAgJHNjb3BlLnNlbGVjdGVkID0ge1xuICAgICAgICAvLyAgICBpdGVtOiAkc2NvcGUuaXRlbXNbMF1cbiAgICAgICAgLy8gIH07XG5cbiAgICAgICAgY29uc29sZS5sb2coJ29wZW5pbmcnLCBpbmZvX2tleSlcbiAgICAgICAgaGVscF9pbmZvLmdldChpbmZvX2tleSkudGhlbihmdW5jdGlvbiAoc3RhdHVzKSB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZygncmV0JylcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKHN0YXR1cylcbiAgICAgICAgICAgIHZhciBpbmZvID0gc3RhdHVzLnJlc3VsdFxuICAgICAgICAgICAgLy8gICAgJHNjb3BlLnJlc3VsdCA9IHN0YXR1cy5yZXN1bHRcbiAgICAgICAgICAgICRzY29wZS5pbmZvX2JvZHkgPSAkc2NlLnRydXN0QXNIdG1sKGluZm8uYm9keSlcbiAgICAgICAgICAgICRzY29wZS5yZXN1bHQgPSBpbmZvXG5cbiAgICAgICAgfSlcblxuICAgICAgICAkc2NvcGUub2sgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZygkc2NvcGUucmVzdWx0KVxuXG4gICAgICAgICAgICBpZiAoJHNjb3BlLnJlc3VsdCA9PT0gdW5kZWZpbmVkKVxuICAgICAgICAgICAgICAgICRtb2RhbEluc3RhbmNlLmNsb3NlKFtdKVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgdmFyIGNsYXNzZXNfc2VsZWN0ZWQgPSBbXVxuICAgICAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgJHNjb3BlLnJlc3VsdC5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgICAgICB2YXIgaXRlbSA9ICRzY29wZS5yZXN1bHRbaV1cblxuICAgICAgICAgICAgICAgICAgICB2YXIgc2VsZWN0ZWQgPSBpdGVtLmJhc2ljcy5maWx0ZXIoZnVuY3Rpb24gKGVsZW1lbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBlbGVtZW50LnNlbGVjdGVkXG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHNlbGVjdGVkLCBzZWxlY3RlZC5sZW5ndGgpXG4gICAgICAgICAgICAgICAgICAgIGlmIChzZWxlY3RlZC5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyB1c2Ugb25seSB0aGUgZmlyc3QgaXRlbSBzZWxlY3RlZCwgYXMgYWxsIGJlbG9uZyB0byB0aGUgc2FtZSBjbGFzc1xuICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3Nlc19zZWxlY3RlZC5wdXNoKHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2RlOiBpdGVtLmNvZGUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYmFzaWNfbnVtYmVyOiBzZWxlY3RlZFswXS5ibnVtYmVyLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJhc2ljX2Rlc2M6IHNlbGVjdGVkWzBdLmJkZXNjXG4gICAgICAgICAgICAgICAgICAgICAgICB9KVxuXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgJG1vZGFsSW5zdGFuY2UuY2xvc2UoY2xhc3Nlc19zZWxlY3RlZCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLmNhbmNlbCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICRtb2RhbEluc3RhbmNlLmRpc21pc3MoJ2NhbmNlbCcpO1xuICAgICAgICB9O1xuICAgIH1dKVxuXG4gICAgLmNvbnRyb2xsZXIoJ2NvbnRhY3RDdHJsJywgWyckc2NvcGUnLCAnJGh0dHAnLFxuICAgICAgICBmdW5jdGlvbiAoJHNjb3BlLCAkaHR0cCkge1xuXG4gICAgICAgICAgICAkc2NvcGUuZGlkQ2xpY2tlZCA9IGZhbHNlXG5cbiAgICAgICAgICAgICRzY29wZS5jb250YWN0X3Byb2MgPSBmdW5jdGlvbiAoY29udGFjdCkge1xuICAgICAgICAgICAgICAgICRzY29wZS5kaWRDbGlja2VkID0gdHJ1ZVxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdvbmNlICcsIGNvbnRhY3QsICRzY29wZS5kaWRDbGlja2VkKVxuICAgICAgICAgICAgICAgICRodHRwLnBvc3QoXCIvY29udGFjdHVzXCIsIGNvbnRhY3QpLnN1Y2Nlc3MoZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZGF0YSlcbiAgICAgICAgICAgICAgICAgICAgaWYgKGRhdGEuc3RhdHVzID09PSAnb2snKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUuc2hvd19tc2cgPSBcIlRoYW5rcyBmb3IgY29udGFjdGluZyB1cy5cIlxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgYWxlcnQoZGF0YS5tc2cpXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICB9XSlcblxuICAgIC5jb250cm9sbGVyKCdhY2NvdW50Q29udHJvbGxlcicsIFsnJHNjb3BlJywgJyRodHRwJywgJyR0aW1lb3V0JywgJ2NvdW50cmllcycsXG4gICAgICAgIGZ1bmN0aW9uICgkc2NvcGUsICRodHRwLCAkdGltZW91dCwgY291bnRyaWVzKSB7XG5cbiAgICAgICAgICAgICRzY29wZS5jb3VudHJpZXMgPSBjb3VudHJpZXNcbiAgICAgICAgICAgICRodHRwLmdldCgnL2FwaS9teWFjY291bnQvJykuc3VjY2VzcyhmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGEpXG4gICAgICAgICAgICAgICAgJHNjb3BlLnVzZXIgPSBkYXRhXG4gICAgICAgICAgICAgICAgJHNjb3BlLnJlYWR5ID0gdHJ1ZVxuICAgICAgICAgICAgfSlcblxuICAgICAgICAgICAgJHNjb3BlLnVwZGF0ZV91c2VyID0gZnVuY3Rpb24gKHVzZXIpIHtcbiAgICAgICAgICAgICAgICAkaHR0cC5wb3N0KFwiL2FwaS91cGRhdGVfdXNlclwiLCB1c2VyKS5zdWNjZXNzKGZ1bmN0aW9uIChkYXRhKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmFsZXJ0cyA9IFtcbiAgICAgICAgICAgICAgICAgICAgICAgIHsgdHlwZTogJ3N1Y2Nlc3MnLCBtc2c6ICdZb3VyIGFjY291bnQgaGFzIGJlZW4gdXBkYXRlZCcgfVxuICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgICAgICR0aW1lb3V0KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5hbGVydHMgPSBbXVxuICAgICAgICAgICAgICAgICAgICB9LCAxNTAwKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICB9XSlcblxuICAgIC5jb250cm9sbGVyKCdwcm9maWxlQ29udHJvbGxlcicsIFsnJHNjb3BlJywgJyRodHRwJywgJyRyb3V0ZVBhcmFtcycsICckbG9jYXRpb24nLCAnJHJvdXRlJywgJyR0aW1lb3V0JywgJ3JlZ2lzdHJ5X2luaXREYXRhJywgJ3VzZXJfcHJvZmlsZScsXG4gICAgICAgIGZ1bmN0aW9uICgkc2NvcGUsICRodHRwLCAkcm91dGVQYXJhbXMsICRsb2NhdGlvbiwgJHJvdXRlLCAkdGltZW91dCwgcmVnaXN0cnlfaW5pdERhdGEsIHVzZXJfcHJvZmlsZSkge1xuXG4gICAgICAgICAgICBjb25zb2xlLmxvZygnaW5pdCBkYXRhICcsIHJlZ2lzdHJ5X2luaXREYXRhKVxuXG4gICAgICAgICAgICAkc2NvcGUucHJvZmlsZSA9IHJlZ2lzdHJ5X2luaXREYXRhLnByb2ZpbGVcblxuICAgICAgICAgICAgJHNjb3BlLm5leHQgPSBmdW5jdGlvbiAocHJvZmlsZSkge1xuXG4gICAgICAgICAgICAgICAgdmFyIGVycl9tc2dcblxuICAgICAgICAgICAgICAgIGlmICghcHJvZmlsZS5wcm9maWxlX25hbWUpXG4gICAgICAgICAgICAgICAgICAgIGVycl9tc2cgPSAnUGxlYXNlIHByb3ZpZGUgYSBuYW1lIGZvciB0aGlzIHByb2ZpbGUnXG4gICAgICAgICAgICAgICAgaWYgKCFlcnJfbXNnICYmICFwcm9maWxlLmNvbnRhY3QpXG4gICAgICAgICAgICAgICAgICAgIGVycl9tc2cgPSAnUGxlYXNlIHByb3ZpZGUgYSBjb250YWN0IHBlcnNvbidcbiAgICAgICAgICAgICAgICBpZiAoIWVycl9tc2cgJiYgIXByb2ZpbGUuZW1haWwpXG4gICAgICAgICAgICAgICAgICAgIGVycl9tc2cgPSAnUGxlYXNlIHByb3ZpZGUgYSB2YWxpZCBlbWFpbCBhZGRyZXNzIHNvIHdlIGNhbiBjb250YWN0IHlvdSdcblxuICAgICAgICAgICAgICAgIGlmIChlcnJfbXNnKVxuICAgICAgICAgICAgICAgICAgICAkc2NvcGUuYWxlcnRzID0gW1xuICAgICAgICAgICAgICAgICAgICAgICAgeyB0eXBlOiAnd2FybmluZycsIG1zZzogZXJyX21zZyB9XG4gICAgICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgICBlbHNlIHtcblxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygncG9zdGluZyBwcm9maWxlICcsIHByb2ZpbGUpXG5cbiAgICAgICAgICAgICAgICAgICAgdXNlcl9wcm9maWxlLnBvc3QocHJvZmlsZSkudGhlbihmdW5jdGlvbiAoc3RhdHVzKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhzdGF0dXMpXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoc3RhdHVzLmlkKSAvLyB0aGlzIGhhcHBlbnMgb25seSBmb3IgbmV3IHByb2ZpbGVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkbG9jYXRpb24ucGF0aCgnL3Byb2ZpbGVfYWRkcmVzcy8nICsgc3RhdHVzLmlkKVxuICAgICAgICAgICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmFsZXJ0cyA9IFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyB0eXBlOiAnc3VjY2VzcycsIG1zZzogJ1lvdXIgcHJvZmlsZSBoYXMgYmVlbiB1cGRhdGVkJyB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICR0aW1lb3V0KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmFsZXJ0cyA9IFtdXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgMTUwMCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgfSlcblxuICAgICAgICAgICAgICAgICAgICAvKlxuICAgICAgICAgICAgICAgICAgICAgZGVsZXRlIHJlZy5wcm9maWxlX2lkXG4gICAgICAgICAgICAgICAgICAgICByZWdpc3Rlcl90cmFkZW1hcmsuc2F2ZShyZWcpXG4gICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygncmVnMCcscmVnKVxuICAgICAgICAgICAgICAgICAgICAgJGxvY2F0aW9uLnBhdGgoJy90cmFkZW1hcmtfYWRkcmVzcy8nICsgJHJvdXRlUGFyYW1zLmNvdW50cnkpXG4gICAgICAgICAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgfVxuXG5cbiAgICAgICAgfV0pXG5cbiAgICAuY29udHJvbGxlcigncHJvZmlsZUxpc3RDb250cm9sbGVyJywgWyckc2NvcGUnLCAnJGh0dHAnLCAnJHJvdXRlJywgJyRsb2NhdGlvbicsICckcm91dGVQYXJhbXMnLCAnY291bnRyaWVzJyxcbiAgICAgICAgZnVuY3Rpb24gKCRzY29wZSwgJGh0dHAsICRyb3V0ZSwgJGxvY2F0aW9uLCAkcm91dGVQYXJhbXMsIGNvdW50cmllcykge1xuXG4gICAgICAgICAgICAkaHR0cC5nZXQoJy9hcGkvcHJvZmlsZXMvJykuc3VjY2VzcyhmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGEpXG4gICAgICAgICAgICAgICAgJHNjb3BlLnByb2ZpbGVzID0gZGF0YVxuICAgICAgICAgICAgfSlcblxuICAgICAgICAgICAgJHNjb3BlLmRlbF9wcm9jID0gZnVuY3Rpb24gKHJlYykge1xuXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2cocmVjKVxuICAgICAgICAgICAgICAgIGlmIChjb25maXJtKCdEbyB5b3UgcmVhbGx5IHdhbnQgdG8gZGVsZXRlIHRoaXMgcHJvZmlsZSA6ICcgKyByZWMucHJvZmlsZV9uYW1lKSkge1xuXG4gICAgICAgICAgICAgICAgICAgICRodHRwLnBvc3QoXCIvYXBpL2RlbGV0ZV9wcm9maWxlXCIsIHJlYykuc3VjY2VzcyhmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZGF0YS5zdGF0dXMpXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YS5zdGF0dXMpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJHJvdXRlLnJlbG9hZCgpXG4gICAgICAgICAgICAgICAgICAgICAgICBlbHNlXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYWxlcnQoZGF0YS5tc2cpXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgICAgIC8qXG4gICAgICAgICAgICAgICAgICAgICAkaHR0cC5wb3N0KCcvYXBpL2RlbGV0ZV9wcm9maWxlJywgcmVjLl9pZCkuc3VjY2VzcyhmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZGF0YSlcbiAgICAgICAgICAgICAgICAgICAgIGlmIChkYXRhLnN0YXR1cyA9PT0gJ29rJykge1xuICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmdldF91c2VycygpXG4gICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgYWxlcnQoZGF0YS5tc2cpXG4gICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICB9KTsqL1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfV0pXG5cbiAgICAuY29udHJvbGxlcigncmVnX2FsbEN0cmwnLCBbJyRzY29wZScsICckaHR0cCcsICckcm91dGVQYXJhbXMnLCAnVE1fTUVOVScsXG4gICAgICAgIGZ1bmN0aW9uICgkc2NvcGUsICRodHRwLCAkcm91dGVQYXJhbXMsIFRNX01FTlUpIHtcblxuICAgICAgICAgICAgJHNjb3BlLmNvdW50cmllcyA9IFRNX01FTlUubWFpbl9tZW51XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhUTV9NRU5VLm1haW5fbWVudSlcblxuXG4gICAgICAgIH1dKVxuXG5cbiAgICAuY29udHJvbGxlcigncHJpY2VzX2NvbnRyb2xsZXInLCBbJyRzY29wZScsICckaHR0cCcsICckcm91dGVQYXJhbXMnLCBcIiRzY2VcIiwgJ2hlbHBfaW5mbycsICdUTV9NRU5VJyxcbiAgICAgICAgZnVuY3Rpb24gKCRzY29wZSwgJGh0dHAsICRyb3V0ZVBhcmFtcywgJHNjZSwgaGVscF9pbmZvLCBUTV9NRU5VKSB7XG5cbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdjb3VudHJpZXMnKVxuICAgICAgICAgICAgJHNjb3BlLmNvdW50cnkgPSAkcm91dGVQYXJhbXMuY291bnRyeVxuICAgICAgICAgICAgJHNjb3BlLmNvdW50cmllcyA9IFRNX01FTlUubWFpbl9tZW51XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhUTV9NRU5VLm1haW5fbWVudSlcblxuICAgICAgICAgICAgJHNjb3BlLmNvdW50cnkyID0gJHNjb3BlLmNvdW50cnlcblxuICAgICAgICAgICAgaWYgKCRzY29wZS5jb3VudHJ5ID09PSAnTk8nKSAgIC8vIHdoZW4gY291bnRyeSBpcyBOb3J3YXkoTk8pLCBpdCB3aWxsIGJlICdmYWxzZXknIGluIHRoZSBodG1sLCB1c2UgY291bnRyeTIgdG8gdGVzdCBpdCBpbiBodG1sIHRvIGJ5IHBhc3MgdGhpc1xuICAgICAgICAgICAgICAgICRzY29wZS5jb3VudHJ5MiA9ICdOT18nXG5cbiAgICAgICAgICAgIHZhciBpbmZvX2tleSA9IFwiVE1fUFJJQ0VfXCIgKyAkcm91dGVQYXJhbXMuY291bnRyeVxuXG4gICAgICAgICAgICBoZWxwX2luZm8uZ2V0KGluZm9fa2V5KS50aGVuKGZ1bmN0aW9uIChzdGF0dXMpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygncmV0JylcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhzdGF0dXMpXG4gICAgICAgICAgICAgICAgdmFyIGluZm8gPSBzdGF0dXMucmVzdWx0XG4gICAgICAgICAgICAgICAgJHNjb3BlLmluZm9fYm9keSA9ICRzY2UudHJ1c3RBc0h0bWwoaW5mby5ib2R5KVxuICAgICAgICAgICAgICAgICRzY29wZS5yZXN1bHQgPSBpbmZvXG5cbiAgICAgICAgICAgIH0pXG5cbiAgICAgICAgfV0pXG5cbiAgICAuY29udHJvbGxlcignZHBtX3Rlc3QnLCBbJyRzY29wZScsICckaHR0cCcsICckcm91dGVQYXJhbXMnLFxuICAgICAgICBmdW5jdGlvbiAoJHNjb3BlLCAkaHR0cCwgJHJvdXRlUGFyYW1zKSB7XG5cbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdkcG0gdGVzdCcpXG5cbiAgICAgICAgICAgICRodHRwLmdldCgnL2FwaS9nZXRfZHBtX3Rlc3QnKS5zdWNjZXNzKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coZGF0YSlcbiAgICAgICAgICAgICAgICAkc2NvcGUuZHBtID0gZGF0YVxuICAgICAgICAgICAgfSlcblxuXG4gICAgICAgIH1dKVxuXG4gICAgLmNvbnRyb2xsZXIoJ2NoZWNrX291dF9jb250cm9sbGVyJywgWyckc2NvcGUnLCAnJGh0dHAnLCAnJHJvdXRlUGFyYW1zJywgJyRsb2NhbGUnLCAnY291bnRyaWVzJywgJ0NhcmRzJyxcbiAgICAgICAgZnVuY3Rpb24gKCRzY29wZSwgJGh0dHAsICRyb3V0ZVBhcmFtcywgJGxvY2FsZSwgY291bnRyaWVzLCBDYXJkcykge1xuXG4gICAgICAgICAgICAkc2NvcGUuY3VycmVudFllYXIgPSBuZXcgRGF0ZSgpLmdldEZ1bGxZZWFyKClcbiAgICAgICAgICAgICRzY29wZS5jdXJyZW50TW9udGggPSBuZXcgRGF0ZSgpLmdldE1vbnRoKCkgKyAxXG4gICAgICAgICAgICAkc2NvcGUubW9udGhzID0gJGxvY2FsZS5EQVRFVElNRV9GT1JNQVRTLk1PTlRIXG4gICAgICAgICAgICAkc2NvcGUuY291bnRyaWVzID0gY291bnRyaWVzXG5cbiAgICAgICAgICAgICRzY29wZS5mb3JtRGF0YSA9IHt9XG5cblxuICAgICAgICAgICAgdmFyIHNldF9leHBpcnkgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgaWYgKCRzY29wZS5mb3JtRGF0YS5leHBfeWVhciAmJiAkc2NvcGUuZm9ybURhdGEuZXhwX21vbnRoKSB7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5mb3JtRGF0YS54X2V4cF9kYXRlID0gJHNjb3BlLmZvcm1EYXRhLmV4cF9tb250aCArICcvJyArICRzY29wZS5mb3JtRGF0YS5leHBfeWVhclxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgQ2FyZHMuZ2V0X3VzZXJfaW5mbygkcm91dGVQYXJhbXMuY2FydF9pZCkudGhlbihmdW5jdGlvbiAoaW5mbykge1xuICAgICAgICAgICAgICAgICRzY29wZS5mb3JtRGF0YSA9IGluZm8gIC8vIHNlZSBhYm92ZSBmb3IgZGF0YSByZXR1cm5lZFxuICAgICAgICAgICAgfSlcblxuICAgICAgICAgICAgQ2FyZHMuZ2V0X3Nob3BwaW5nX2NhcnRfZHBtKCRyb3V0ZVBhcmFtcy5jYXJ0X2lkKS50aGVuKGZ1bmN0aW9uIChkYXRhKSB7XG5cbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnaGFoYSBiYWNrIGZyb20gZHBtICcsIGRhdGEpXG4gICAgICAgICAgICAgICAgJHNjb3BlLmRwbSA9IGRhdGFcblxuICAgICAgICAgICAgfSlcblxuICAgICAgICAgICAgJHNjb3BlLiR3YXRjaCgnZm9ybURhdGEuZXhwX21vbnRoJywgZnVuY3Rpb24gKG5ld1ZhbCwgb2xkVmFsKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ2NoYW5nZWQnLCBuZXdWYWwsIG9sZFZhbClcbiAgICAgICAgICAgICAgICBzZXRfZXhwaXJ5KClcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAkc2NvcGUuJHdhdGNoKCdmb3JtRGF0YS5leHBfeWVhcicsIGZ1bmN0aW9uIChuZXdWYWwsIG9sZFZhbCkge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdjaGFuZ2VkJywgbmV3VmFsLCBvbGRWYWwpXG4gICAgICAgICAgICAgICAgc2V0X2V4cGlyeSgpXG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgJHNjb3BlLnByb2Nlc3NGb3JtID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdwcm9jZXNzZWQnKVxuICAgICAgICAgICAgICAgICRzY29wZS5tc2cgPSAnQ29udGFjdGluZyBQYXltZW50IEdhdGV3YXksIFBsZWFzZSB3YWl0Li4uJ1xuICAgICAgICAgICAgfVxuXG4gICAgICAgIH1dKVxuXG4gICAgLmNvbnRyb2xsZXIoJ3Bvc3RfcGF5bWVudF9jb250cm9sbGVyJywgWyckc2NvcGUnLCAnJGh0dHAnLCAnJHJvdXRlUGFyYW1zJyxcbiAgICAgICAgZnVuY3Rpb24gKCRzY29wZSwgJGh0dHAsICRyb3V0ZVBhcmFtcykge1xuXG4gICAgICAgICAgICBjb25zb2xlLmxvZygnZHBtIHRlc3QnLCAkcm91dGVQYXJhbXMpXG5cbiAgICAgICAgICAgICRodHRwLmdldCgnL2FwaS9nZXRfZ3dfcmVzcG9uc2UvJyArICRyb3V0ZVBhcmFtcy5rZXkpLnN1Y2Nlc3MoZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhKVxuICAgICAgICAgICAgICAgICRzY29wZS5yZXNwID0gZGF0YVxuICAgICAgICAgICAgfSlcblxuICAgICAgICB9XSlcblxuICAgIC8vIGtleSBpbiB0aGlzIHJvdXRlIGlzIHRoZSBpZCBmb3IgdGhlIG9yZGVyXG4gICAgLmNvbnRyb2xsZXIoJ3Bvc3RfcGF5bWVudDJfY29udHJvbGxlcicsIFsnJHNjb3BlJywgJyRodHRwJywgJyRyb3V0ZVBhcmFtcycsXG4gICAgICAgIGZ1bmN0aW9uICgkc2NvcGUsICRodHRwLCAkcm91dGVQYXJhbXMpIHtcbiAgICAgICAgICAgICRzY29wZS5vcmRlcl9rZXkgPSAkcm91dGVQYXJhbXMua2V5XG5cbiAgICAgICAgfV0pXG5cbiAgICAuY29udHJvbGxlcignY2hlY2tfb3V0X3N0cmlwZV9jb250cm9sbGVyJywgWyckc2NvcGUnLCAnJGh0dHAnLCAnJHJvdXRlUGFyYW1zJywgJyRsb2NhdGlvbicsICckcm9vdFNjb3BlJyxcbiAgICAgICAgJyRsb2NhbGUnLCAnJHdpbmRvdycsICdDYXJkcycsICdBVVRIX0VWRU5UUycsIHJlcXVpcmUoJy4vd2ViX2NvbnRyb2xsZXJzX3N0cmlwZScpLnN0cmlwZV0pXG5cblxuICAgIC5jb250cm9sbGVyKCdldV9jb29raWVzX2NvbnRyb2xsZXInLCBbJyRodHRwJywgJyRyb3V0ZVBhcmFtcycsICckbG9jYXRpb24nLCAnJHJvdXRlJyxcbiAgICAgICAgJyRsb2NhbGUnLCAnJHdpbmRvdycsICdDYXJkcycsICdBVVRIX0VWRU5UUycsIHJlcXVpcmUoJy4vd2ViX2NvbnRyb2xsZXJzX2V1X2Nvb2tpZXMnKS5ldV9jb29raWVzXSlcblxuXG4gICAgLmNvbnRyb2xsZXIoJ2JlZm9yZV9jaGVja19vdXQnLCBbJyRzY29wZScsICckcm91dGVQYXJhbXMnLCAnJGxvY2F0aW9uJywgJyRyb290U2NvcGUnLCAnYXV0aFNlcnZpY2UnLCAnQ2FydCcsICd2YWxpZF9lbWFpbHMnLCAnQVVUSF9FVkVOVFMnLFxuICAgICAgICByZXF1aXJlKCcuL3dlYl9jb250cm9sbGVyc19jaGVja19vdXQnKS5iNGNoZWNrX291dF0pXG5cblxuICAgIC5jb250cm9sbGVyKCdiZWZvcmVfY2hlY2tfb3V0X25vbl9tZW1iZXInLCBbJyRzY29wZScsICckcm91dGVQYXJhbXMnLCAnJGxvY2F0aW9uJywgJyRyb290U2NvcGUnLCAnYXV0aFNlcnZpY2UnLCAnQ2FydCcsICdBVVRIX0VWRU5UUycsXG4gICAgICAgIHJlcXVpcmUoJy4vd2ViX2NvbnRyb2xsZXJzX2NoZWNrX291dCcpLmI0Y2hlY2tfb3V0X25vbl9tZW1iZXJdKVxuXG4gICAgLmNvbnRyb2xsZXIoJ3Byb2ZpbGVfZWRpdCcsIFsnJHNjb3BlJywgJyRodHRwJywgJyRyb3V0ZVBhcmFtcycsICckbG9jYXRpb24nLCAnJHJvdXRlJyxcbiAgICAgICAgcmVxdWlyZSgnLi9wcm9maWxlX2VkaXQnKS5Qcm9maWxlX0VkaXRdKVxuXG4vKlxuIC5jb250cm9sbGVyKCdDYXJvdXNlbERlbW9DdHJsJywgZnVuY3Rpb24gKCRzY29wZSkge1xuICRzY29wZS5jb3ZlcmZsb3cgPSB7fTtcbiAkc2NvcGUuaW1hZ2VzID0gW1xuICcvYW5ndWxhci1jb3ZlcmZsb3cvY292ZXJzLzEuanBnJyxcbiAnL2FuZ3VsYXItY292ZXJmbG93L2NvdmVycy8yLmpwZycsXG4gJy9hbmd1bGFyLWNvdmVyZmxvdy9jb3ZlcnMvMy5qcGcnLFxuICcvYW5ndWxhci1jb3ZlcmZsb3cvY292ZXJzLzQuanBnJyxcbiAnL2FuZ3VsYXItY292ZXJmbG93L2NvdmVycy81LmpwZycsXG4gJy9hbmd1bGFyLWNvdmVyZmxvdy9jb3ZlcnMvNi5qcGcnLFxuICcvYW5ndWxhci1jb3ZlcmZsb3cvY292ZXJzLzcuanBnJyxcbiAnL2FuZ3VsYXItY292ZXJmbG93L2NvdmVycy84LmpwZycsXG4gJy9hbmd1bGFyLWNvdmVyZmxvdy9jb3ZlcnMvOS5qcGcnLFxuICcvYW5ndWxhci1jb3ZlcmZsb3cvY292ZXJzLzEwLmpwZycsXG4gJy9hbmd1bGFyLWNvdmVyZmxvdy9jb3ZlcnMvMTEuanBnJyxcbiAnL2FuZ3VsYXItY292ZXJmbG93L2NvdmVycy8xMi5qcGcnLFxuICcvYW5ndWxhci1jb3ZlcmZsb3cvY292ZXJzLzEzLmpwZycsXG4gJy9hbmd1bGFyLWNvdmVyZmxvdy9jb3ZlcnMvMTQuanBnJyxcbiAnL2FuZ3VsYXItY292ZXJmbG93L2NvdmVycy8xNS5qcGcnLFxuICcvYW5ndWxhci1jb3ZlcmZsb3cvY292ZXJzLzE2LmpwZycsXG4gJy9hbmd1bGFyLWNvdmVyZmxvdy9jb3ZlcnMvMTcuanBnJyxcbiAnL2FuZ3VsYXItY292ZXJmbG93L2NvdmVycy8xOC5qcGcnLFxuICcvYW5ndWxhci1jb3ZlcmZsb3cvY292ZXJzLzE5LmpwZycsXG4gJy9hbmd1bGFyLWNvdmVyZmxvdy9jb3ZlcnMvMjAuanBnJ1xuIF07XG4gJHNjb3BlLmltYWdlU2VsZWN0ZWQgPSBmdW5jdGlvbiAoKSB7XG4gY29uc29sZS5sb2coJ2ltZyBjbGsnKVxuIH1cbiB9KSovIiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbmV4cG9ydHMuY2FydCA9IGZ1bmN0aW9uICgkc2NvcGUsICRodHRwLCAkcm9vdFNjb3BlLCAkbG9jYXRpb24sIENhcnQsIEFVVEhfRVZFTlRTLCAkbW9kYWwpIHtcblxuICAvLyAgJHNjb3BlLmNhcnQgPSBDYXJ0LmdldENhcnQoKVxuLy8gICAgJHNjb3BlLm5hbWUgPSBTZXNzaW9uLmdldE5hbWUoKVxuXG4gIC8qXG4gICRzY29wZS5hbGVydCA9IGZ1bmN0aW9uIChtKSB7XG4gICAgYWxlcnQobSlcbiAgfVxuKi9cblxuICAkc2NvcGUuaXNDYXJ0RW1wdHkgPSBmdW5jdGlvbiAoY2FydCkge1xuICAgIGlmICghY2FydClcbiAgICAgIHJldHVybiB0cnVlXG4gICAgZWxzZSB7XG4gICAgICByZXR1cm4gY2FydC5pdGVtcy5sZW5ndGggPT09IDBcbiAgICB9XG4gIH1cblxuICAkc2NvcGUuZ2V0X3JlbW90ZV9jYXJ0ID0gZnVuY3Rpb24gKCkge1xuXG4gICAgQ2FydC5nZXRSZW1vdGVDYXJ0KCkudGhlbihmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgJHNjb3BlLmNhcnQgPSBkYXRhLmNhcnRcbiAgICAgICRyb290U2NvcGUuJGJyb2FkY2FzdChBVVRIX0VWRU5UUy5jYXJ0VXBkYXRlZCk7XG4gICAgfSlcblxuICB9XG5cbiAgJHNjb3BlLmdldF9yZW1vdGVfY2FydCgpXG5cbiAgJHNjb3BlLmRlbGV0ZV9pdGVtID0gZnVuY3Rpb24gKGNhcnRfaWQsIHJlYykge1xuXG4gICAgaWYgKGNvbmZpcm0oJ0RvIHlvdSByZWFsbHkgd2FudCB0byBkZWxldGUgdGhpcyBpdGVtPycpKSB7XG5cbiAgICAgIENhcnQuZGVsZXRlX2l0ZW0oY2FydF9pZCwgcmVjKS50aGVuKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICRzY29wZS5nZXRfcmVtb3RlX2NhcnQoKVxuICAgICAgfSlcbiAgICB9XG4gIH1cblxuXG4gICRzY29wZS51cGxvYWRfbG9jYWxfY2FydCA9IGZ1bmN0aW9uIChjYXJ0X2lkKSB7XG4gICAgY29uc29sZS5sb2coJ2NhcnRfaWQnLCBjYXJ0X2lkKVxuXG4gICAgaWYgKGNvbmZpcm0oJ0RvIHlvdSByZWFsbHkgd2FudCB0byB1cGxvYWQgdGhpcyBsb2NhbCBjYXJ0PycpKSB7XG5cbiAgICAgIENhcnQuc3RvcmVfdG9fc2VydmVyKCkudGhlbihmdW5jdGlvbiAoc3RhdHVzKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdzdG9yZSB0byBzZXJ2ZXIgc3RhdHVzMicsIHN0YXR1cylcbiAgICAgICAgLypcbiAgICAgICAgQ2FydC5jbGVhbl9sb2NhbF9zdG9yZSgpXG5cbiAgICAgICAgcmVnaXN0ZXJfdHJhZGVtYXJrLmluaXRfaXRlbSgpXG4gICAgICAgICRyb290U2NvcGUuJGJyb2FkY2FzdChBVVRIX0VWRU5UUy5jYXJ0VXBkYXRlZCk7XG5cbiAgICAgICAgJHJvb3RTY29wZS4kYnJvYWRjYXN0KEFVVEhfRVZFTlRTLmxvZ2luU3VjY2Vzcyk7XG4gICAgICAgIENhcnQuZ2V0UmVtb3RlQ2FydENvdW50KCkudGhlbihmdW5jdGlvbiAoY291bnQpIHtcbiAgICAgICAgICBpZiAoY291bnQgPiAwKVxuICAgICAgICAgICAgJGxvY2F0aW9uLnBhdGgoJy9zaG9wcGluZ19jYXJ0JylcbiAgICAgICAgICBlbHNlXG4gICAgICAgICAgICAkbG9jYXRpb24ucGF0aCgnL2Rhc2hib2FyZCcpXG4gICAgICAgIH0pXG4qL1xuICAgICAgfSlcbiAgICAgIC8qXG4gICAgICBDYXJ0LmRlbGV0ZV9pdGVtKGNhcnRfaWQsIHJlYykudGhlbihmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAkc2NvcGUuZ2V0X3JlbW90ZV9jYXJ0KClcbiAgICAgIH0pKi9cbiAgICB9XG4gIH1cblxuICAkc2NvcGUuc2VsZWN0aW9uX2NoYW5nZSA9IGZ1bmN0aW9uIChjYXJ0X2lkLCByZWMpIHtcbiAgICBDYXJ0LnNlbGVjdGlvbl9jaGFuZ2UoY2FydF9pZCwgcmVjKS50aGVuKGZ1bmN0aW9uIChzdGF0dXMpIHtcbiAgICAgICRzY29wZS5nZXRfcmVtb3RlX2NhcnQoKVxuICAgIH0pXG4gIH1cblxuICAkc2NvcGUuc2hvd19kZXRhaWxzID0gZnVuY3Rpb24gKHJlYykge1xuICAgIGNvbnNvbGUubG9nKCdzaG93X2RldGFpbHMnLCByZWMsICRtb2RhbClcbiAgICBjb25zb2xlLmxvZygnc2hvd18kbW9kYWwnLCAkbW9kYWwpXG4gICAgdmFyIG1vZGFsSW5zdGFuY2UgPSAkbW9kYWwub3Blbih7XG4gICAgICB0ZW1wbGF0ZVVybDogJ3BhcnRpYWxzL3Nob3BwaW5nX2NhcnRfZGV0YWlscy5odG1sJyxcbiAgICAgIHdpbmRvd0NsYXNzOiAnYXBwLW1vZGFsLXdpbmRvdycsXG4gICAgICBjb250cm9sbGVyOiAnTW9kYWxJbnN0YW5jZUN0cmwyJyxcbiAgICAgIHJlc29sdmU6IHtcbiAgICAgICAgZGV0YWlsczogZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHJldHVybiByZWNcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgLy8gaGFwcGVuc1xuICAkc2NvcGUubG9jYWxfc3RvcmVfdG9fc2VydmVyID0gZnVuY3Rpb24gKCkge1xuICAgIGNvbnNvbGUubG9nKCdsb2NhbCBzdG9yZSB0byBzZXJ2ZXInKVxuICAgIENhcnQuc3RvcmVfdG9fc2VydmVyKCkudGhlbihmdW5jdGlvbiAoc3RhdHVzKSB7XG4gICAgICBjb25zb2xlLmxvZygnc3RvcmUgdG8gc2VydmVyIHN0YXR1cycsIHN0YXR1cylcbiAgICAgIENhcnQuY2xlYW5fbG9jYWxfc3RvcmUoKVxuICAgIH0pXG4gIH1cblxufSIsIlwidXNlIHN0cmljdFwiO1xuXG52YXIgY291bnRyaWVzID0gcmVxdWlyZSgnY291bnRyaWVzJylcblxuLy8gQ09OVFJPTExFUlNcblxuLy8gY29udHJvbGxlciBmb3IgY2FzZTFcblxuZXhwb3J0cy5jYXNlX2dlbiA9IGZ1bmN0aW9uICgkc2NvcGUsICRyb290U2NvcGUsICRxLCAkd2luZG93LCAkbG9jYXRpb24sIEFVVEhfRVZFTlRTLCBDYXJ0LCBDYXNlX2dlbiwgY2FzZV9vYmopIHtcblxuICAgIHZhciBhdmFpbGFibGVfY291bnRyaWVzID0gWydVUycsICdDQScsICdCUicsICdSVScsICdJTicsICdDTicsICdaQScsICdOTycsICdDSCddXG4gICAgJHNjb3BlLmFsZXJ0cyA9IFtdO1xuICAgICRzY29wZS5jYXNlX29iaiA9IGNhc2Vfb2JqXG5cbiAgICAkc2NvcGUuY291bnRyaWVzID0gY291bnRyaWVzLmdldF9jb3VudHJpZXMoYXZhaWxhYmxlX2NvdW50cmllcylcblxuICAgICRzY29wZS50b3RhbF9hbW91bnQgPSAwXG4gICAgJHNjb3BlLm5ldF9hbW91bnQgPSAwXG4gICAgJHNjb3BlLmRpc2NvdW50ID0gMTBcbiAgICAkc2NvcGUuZGlzY291bnRfYW1vdW50ID0gMTAwXG4gICAgJHNjb3BlLnRtX3R5cGUgPSBnZXRfdHlwZShjYXNlX29iailcblxuICAgIGlmIChjYXNlX29iai5zdGF0dXMgPT09ICdmb3VuZCcpIHtcblxuICAgICAgICAvLyBnZXQgcHJpY2VzIGZvciBjb3VudHJpZXMgYXZhaWxhYmxlXG5cbiAgICAgICAgQ2FzZV9nZW4uZ2V0X2l0ZW1zKGNhc2Vfb2JqLCBhdmFpbGFibGVfY291bnRyaWVzKVxuXG4gICAgICAgICAgICAudGhlbihmdW5jdGlvbiAoaXRlbXMpIHtcblxuICAgICAgICAgICAgICAgIGl0ZW1zLm1hcChmdW5jdGlvbiAoaXRlbSkge1xuXG4gICAgICAgICAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgJHNjb3BlLmNvdW50cmllcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCRzY29wZS5jb3VudHJpZXNbaV0uY29kZSA9PT0gaXRlbS5jb3VudHJ5KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmNvdW50cmllc1tpXS5hbW91bnRfb3JpZyA9IGl0ZW0uYW1vdW50X29yaWdcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUuY291bnRyaWVzW2ldLnNlbGVjdGVkID0gZmFsc2VcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICB9KVxuXG5cbiAgICAgICAgLy8gd2F0Y2ggdXNlcidzIHNlbGVjdGlvbiBhbmQgYWRqdXN0IHRoZSBhbW91bnRzXG5cbiAgICAgICAgJHNjb3BlLiR3YXRjaCgnY291bnRyaWVzJywgZnVuY3Rpb24gKG5ld1YsIG9sZFYpIHtcblxuICAgICAgICAgICAgdmFyIGksIGNvdW50cmllc19jbnQgPSAwXG4gICAgICAgICAgICAkc2NvcGUudG90YWxfYW1vdW50ID0gMFxuXG4gICAgICAgICAgICBmb3IgKGkgPSAwOyBpIDwgJHNjb3BlLmNvdW50cmllcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgIGlmICgkc2NvcGUuY291bnRyaWVzW2ldLnNlbGVjdGVkKSB7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5jb3VudHJpZXNbaV0uYW1vdW50ID0gJHNjb3BlLmNvdW50cmllc1tpXS5hbW91bnRfb3JpZ1xuICAgICAgICAgICAgICAgICAgICBjb3VudHJpZXNfY250ID0gY291bnRyaWVzX2NudCArIDFcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnRvdGFsX2Ftb3VudCA9ICRzY29wZS50b3RhbF9hbW91bnQgKyAkc2NvcGUuY291bnRyaWVzW2ldLmFtb3VudFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlXG4gICAgICAgICAgICAgICAgICAgIGRlbGV0ZSAkc2NvcGUuY291bnRyaWVzW2ldLmFtb3VudFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGNvdW50cmllc19jbnQgPiAxKVxuICAgICAgICAgICAgICAgICRzY29wZS5kaXNjb3VudCA9IDIwXG4gICAgICAgICAgICBlbHNlXG4gICAgICAgICAgICAgICAgJHNjb3BlLmRpc2NvdW50ID0gMTBcbiAgICAgICAgICAgICRzY29wZS5kaXNjb3VudF9hbW91bnQgPSBNYXRoLnJvdW5kKCRzY29wZS50b3RhbF9hbW91bnQgKiAkc2NvcGUuZGlzY291bnQgLyAxMDApXG4gICAgICAgICAgICAkc2NvcGUubmV0X2Ftb3VudCA9ICRzY29wZS50b3RhbF9hbW91bnQgLSAkc2NvcGUuZGlzY291bnRfYW1vdW50XG5cbiAgICAgICAgfSwgdHJ1ZSk7XG5cbiAgICB9XG5cbiAgICBmdW5jdGlvbiBnZXRfc2VsZWN0aW9uKCkge1xuICAgICAgICB2YXIgcmV0ID0gW11cbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCAkc2NvcGUuY291bnRyaWVzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBpZiAoJHNjb3BlLmNvdW50cmllc1tpXS5zZWxlY3RlZCkge1xuICAgICAgICAgICAgICAgIHJldC5wdXNoKCRzY29wZS5jb3VudHJpZXNbaV0uY29kZSlcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gcmV0XG4gICAgfVxuXG4gICAgJHNjb3BlLmNsb3NlQWxlcnQgPSBmdW5jdGlvbiAoaW5kZXgpIHtcbiAgICAgICAgJHNjb3BlLmFsZXJ0cy5zcGxpY2UoaW5kZXgsIDEpO1xuICAgIH07XG5cbiAgICAkc2NvcGUuc3VibWl0ID0gZnVuY3Rpb24gKCkge1xuXG4gICAgICAgIGlmIChnZXRfc2VsZWN0aW9uKCkubGVuZ3RoID09IDApIHtcbiAgICAgICAgICAgICRzY29wZS5hbGVydHMgPSBbXVxuICAgICAgICAgICAgJHNjb3BlLmFsZXJ0cy5wdXNoKHtcbiAgICAgICAgICAgICAgICB0eXBlOiAnd2FybmluZycsXG4gICAgICAgICAgICAgICAgbXNnOiAnUGxlYXNlIHNlbGVjdCBhdCBsZWFzdCBvbmUgZnJvbSB0aGUgY291bnRyaWVzIGxpc3RlZCBhYm92ZSdcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9IGVsc2Uge1xuXG4gICAgICAgICAgICBzYXZlX3NlbGVjdGlvbl90b19jYXJ0KClcbiAgICAgICAgfVxuXG4gICAgfVxuXG5cbiAgICAvLyBmdyBhc3N1bWVzIG1hcmsgYWx3YXlzIGV4aXN0XG5cbiAgICBmdW5jdGlvbiBnZXRfdHlwZShjYXNlX28pIHtcbiAgICAgICAgaWYgKGNhc2Vfby5pbWFnZV91cmwpXG4gICAgICAgICAgICByZXR1cm4gJ3dsJ1xuICAgICAgICBlbHNlXG4gICAgICAgICAgICByZXR1cm4gJ3cnOyAvLyAndycsICdsJywgJ3dsJyAgIHdvcmQgb25seS9sb2dvL3dvcmQgYW5kIGxvZ29cbiAgICB9XG5cblxuICAgIGZ1bmN0aW9uIHNhdmVfc2VsZWN0aW9uX3RvX2NhcnQoKSB7XG5cbiAgICAgICAgQ2FzZV9nZW4uZmV0Y2hfaW1hZ2VzKHdpbmRvdy5lbmNvZGVVUklDb21wb25lbnQoY2FzZV9vYmouaW1hZ2VfdXJsKSkudGhlbihmdW5jdGlvbiAobmV3X3VybCkge1xuXG4gICAgICAgICAgICByZXR1cm4ge25ld191cmw6IG5ld191cmx9XG5cbiAgICAgICAgfSkudGhlbihmdW5jdGlvbiAoanJldCkge1xuXG4gICAgICAgICAgICB2YXIgcHJvZmlsZSA9IENhc2VfZ2VuLmdldF9kZWZhdWx0X3Byb2ZpbGUoY2FzZV9vYmopIC8vIHBhcnNlIHRoZSBpbmZvIGluIGNhc2Vfb2JqIGludG8gcHJvZmlsZVxuICAgICAgICAgICAgcmV0dXJuIENhc2VfZ2VuLmNyZWF0ZV9wcm9maWxlKHByb2ZpbGUpLnRoZW4oZnVuY3Rpb24gKHByb2ZpbGVfaWQpIHtcblxuICAgICAgICAgICAgICAgIGpyZXQucHJvZmlsZV9pZCA9IHByb2ZpbGVfaWRcbiAgICAgICAgICAgICAgICByZXR1cm4ganJldFxuXG4gICAgICAgICAgICB9KVxuXG4gICAgICAgIH0pLnRoZW4oZnVuY3Rpb24gKGpyZXQpIHtcblxuICAgICAgICAgICByZXR1cm4gQ2FzZV9nZW4uZ2V0X2l0ZW1zKGNhc2Vfb2JqLCBnZXRfc2VsZWN0aW9uKCkpLnRoZW4oZnVuY3Rpb24oaXRlbXMpIHtcblxuICAgICAgICAgICAgICAgIGl0ZW1zLm1hcChmdW5jdGlvbiAoaXRlbSkge1xuXG4gICAgICAgICAgICAgICAgICAgIGl0ZW0uZGlzYyA9ICRzY29wZS5kaXNjb3VudFxuICAgICAgICAgICAgICAgICAgICBpdGVtLmFtb3VudCA9IE1hdGgucm91bmQoaXRlbS5hbW91bnRfb3JpZyAqICgxMDAgLSBpdGVtLmRpc2MpIC8gMTAwKVxuXG4gICAgICAgICAgICAgICAgICAgIGRlbGV0ZSBpdGVtLnByb2ZpbGVcbiAgICAgICAgICAgICAgICAgICAgaXRlbS5wcm9maWxlX2lkID0ganJldC5wcm9maWxlX2lkXG4gICAgICAgICAgICAgICAgICAgIGlmIChqcmV0Lm5ld191cmwgIT09IHVuZGVmaW5lZClcbiAgICAgICAgICAgICAgICAgICAgICAgIGl0ZW0ubG9nb191cmwgPSBqcmV0Lm5ld191cmxcbiAgICAgICAgICAgICAgICB9KVxuXG4gICAgICAgICAgICAgICAgcmV0dXJuIGl0ZW1zXG5cbiAgICAgICAgICAgIH0pXG5cbiAgICAgICAgfSkudGhlbihmdW5jdGlvbihpdGVtcykge1xuICAgICAgICAgICAgY29uc29sZS5sb2coJz8nLCBpdGVtcylcblxuICAgICAgICAgICAgdmFyIHBvc3RzID0gW11cblxuICAgICAgICAgICAgaXRlbXMubWFwKGZ1bmN0aW9uIChpdGVtKSB7XG4gICAgICAgICAgICAgICAgcG9zdHMucHVzaChDYXJ0LnBvc3RfdG9fc2VydmVyKGl0ZW0pKVxuICAgICAgICAgICAgfSlcblxuICAgICAgICAgICAgJHEuYWxsKHBvc3RzKS50aGVuKGZ1bmN0aW9uIChyZXN1bHRzKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ3Bvc3RlZCByZXN1bHRzJywgcmVzdWx0cylcbiAgICAgICAgICAgICAgICAvLyBldmVyeXRoaW5nIGhhcyBiZWVuIHBvc3RlZCB0byBjYXJ0IGF0IHRoaXMgcG9pbnRcbiAgICAgICAgICAgICAgICAkcm9vdFNjb3BlLiRicm9hZGNhc3QoQVVUSF9FVkVOVFMuY2FydFVwZGF0ZWQpO1xuICAgICAgICAgICAgICAgICRsb2NhdGlvbi5wYXRoKCcvc2hvcHBpbmdfY2FydCcpXG4gICAgICAgICAgICB9KVxuICAgICAgICB9KVxuICAgIH1cblxuXG4gICAgLypcbiAgICBDYXNlX2dlbi5nZXRfaXRlbXMoY2FzZV9vYmosIHNlbGVjdGVkKSAgLy8gZ2V0IHByaWNlIGZpcnN0XG5cbiAgICAgICAgLnRoZW4oZnVuY3Rpb24gKGl0ZW1zKSB7XG5cbiAgICAgICAgICAgIGl0ZW1zLm1hcChmdW5jdGlvbiAoaXRlbSkge1xuXG4gICAgICAgICAgICAgICAgaXRlbS5kaXNjID0gJHNjb3BlLmRpc2NvdW50XG4gICAgICAgICAgICAgICAgaXRlbS5hbW91bnQgPSBNYXRoLnJvdW5kKGl0ZW0uYW1vdW50X29yaWcgKiAoMTAwIC0gaXRlbS5kaXNjKSAvIDEwMClcblxuICAgICAgICAgICAgfSlcblxuICAgICAgICAgICAgQ2FzZV9nZW4uZmV0Y2hfaW1hZ2VzKHdpbmRvdy5lbmNvZGVVUklDb21wb25lbnQoY2FzZV9vYmouaW1hZ2VfdXJsKSkudGhlbihmdW5jdGlvbiAobmV3X3VybCkge1xuXG4gICAgICAgICAgICAgICAgdmFyIHByb2ZpbGUgPSBDYXNlX2dlbi5nZXRfZGVmYXVsdF9wcm9maWxlKGNhc2Vfb2JqKVxuXG4gICAgICAgICAgICAgICAgQ2FzZV9nZW4uY3JlYXRlX3Byb2ZpbGUocHJvZmlsZSkudGhlbihmdW5jdGlvbiAocHJvZmlsZV9pZCkge1xuXG4gICAgICAgICAgICAgICAgICAgIHZhciBwb3N0cyA9IFtdXG5cbiAgICAgICAgICAgICAgICAgICAgaXRlbXMubWFwKGZ1bmN0aW9uIChpdGVtKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBkZWxldGUgaXRlbS5wcm9maWxlXG4gICAgICAgICAgICAgICAgICAgICAgICBpdGVtLnByb2ZpbGVfaWQgPSBwcm9maWxlX2lkXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAobmV3X3VybClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpdGVtLmxvZ29fdXJsID0gbmV3X3VybFxuICAgICAgICAgICAgICAgICAgICAgICAgcG9zdHMucHVzaChDYXJ0LnBvc3RfdG9fc2VydmVyKGl0ZW0pKVxuICAgICAgICAgICAgICAgICAgICB9KVxuXG4gICAgICAgICAgICAgICAgICAgICRxLmFsbChwb3N0cykudGhlbihmdW5jdGlvbiAocmVzdWx0cykge1xuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ3Bvc3RlZCByZXN1bHRzJywgcmVzdWx0cylcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGV2ZXJ5dGhpbmcgaGFzIGJlZW4gcG9zdGVkIHRvIGNhcnQgYXQgdGhpcyBwb2ludFxuICAgICAgICAgICAgICAgICAgICAgICAgJHJvb3RTY29wZS4kYnJvYWRjYXN0KEFVVEhfRVZFTlRTLmNhcnRVcGRhdGVkKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICRsb2NhdGlvbi5wYXRoKCcvc2hvcHBpbmdfY2FydCcpXG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH0pXG4gICAgICAgIH0pXG5cbiAgICB9XG4gICAgKi9cblxuXG59XG5cbi8vIGNvbnRyb2xsZXIgZm9yIGNhc2UyXG4vKlxuIGV4cG9ydHMuY2FzZV9nZW4yID0gZnVuY3Rpb24gKCRxLCAkc2NvcGUsICRyb290U2NvcGUsICR3aW5kb3csICRsb2NhdGlvbiwgQVVUSF9FVkVOVFMsIHVzZXJfcHJvZmlsZSwgYXV0aFNlcnZpY2UsIENhcnQsXG4gQ2FydF9TdG9yZSwgQ2FzZV9nZW4sIGNhc2Vfb2JqKSB7XG5cbiAkc2NvcGUuYWxlcnRzID0gW107XG4gJHNjb3BlLmNhc2Vfb2JqID0gY2FzZV9vYmpcblxuICRzY29wZS50b3RhbF9hbW91bnQgPSAwXG4gJHNjb3BlLnRtX3R5cGUgPSBnZXRfdHlwZShjYXNlX29iailcblxuICRzY29wZS5wcm9maWxlX25hbWUgPSAnJ1xuICRzY29wZS5wcm9maWxlX2VtYWlsID0gJydcblxuIHZhciBzZWxlY3Rpb25fb2JqID0gSlNPTi5wYXJzZSgkd2luZG93LnNlc3Npb25TdG9yYWdlLmNhc2Vfb2JqKVxuXG4gQ2FzZV9nZW4uZ2V0X2l0ZW1zKHNlbGVjdGlvbl9vYmouY2FzZV9vYmosIHNlbGVjdGlvbl9vYmouc2VsZWN0aW9uKVxuXG4gLnRoZW4oZnVuY3Rpb24gKGl0ZW1zKSB7XG4gY29uc29sZS5sb2coJ2l0ZW1zJywgaXRlbXMpXG4gdmFyIGRpc2MgPSBpdGVtcy5sZW5ndGggPiAxID8gMjAgOiAxMFxuIGl0ZW1zLm1hcChmdW5jdGlvbiAoaXRlbSkge1xuIGNvbnNvbGUubG9nKGl0ZW0pXG4gaXRlbS5kaXNjID0gZGlzY1xuIGl0ZW0uYW1vdW50ID0gTWF0aC5yb3VuZChpdGVtLmFtb3VudF9vcmlnICogKDEwMCAtIGRpc2MpIC8gMTAwKVxuICRzY29wZS50b3RhbF9hbW91bnQgPSAkc2NvcGUudG90YWxfYW1vdW50ICsgaXRlbS5hbW91bnRcblxuIH0pXG4gY29uc29sZS5sb2coJ2l0ZW0yJywgaXRlbXMsICRzY29wZS50b3RhbF9hbW91bnQpXG4gJHNjb3BlLml0ZW1zID0gaXRlbXNcblxuIH0pXG5cbiAkc2NvcGUuY2xvc2VBbGVydCA9IGZ1bmN0aW9uIChpbmRleCkge1xuICRzY29wZS5hbGVydHMuc3BsaWNlKGluZGV4LCAxKTtcbiB9O1xuXG4gJHNjb3BlLmJhY2sgPSBmdW5jdGlvbiAoKSB7XG4gJGxvY2F0aW9uLnBhdGgoJy9jYXNlLycgKyAkc2NvcGUuY2FzZV9vYmouY2FzZV9jb2RlKVxuIH1cblxuICRzY29wZS5zdWJtaXQgPSBmdW5jdGlvbiAoKSB7XG5cbiB2YXIgbXNnID0gJydcblxuIGlmICghJHNjb3BlLnByb2ZpbGVfbmFtZSlcbiBtc2cgPSAnUGxlYXNlIHByb3ZpZGUgYSBuYW1lJ1xuIGVsc2UgaWYgKCEkc2NvcGUucHJvZmlsZV9lbWFpbClcbiBtc2cgPSAnUGxlYWVzIHByb3ZpZGUgYW4gZW1haWwgYWRkcmVzcydcblxuIG1zZyA9ICcnXG5cbiBpZiAobXNnKSB7XG4gJHNjb3BlLmFsZXJ0cyA9IFtdXG4gJHNjb3BlLmFsZXJ0cy5wdXNoKHtcbiB0eXBlOiAnd2FybmluZycsXG4gbXNnOiBtc2dcbiB9KTtcbiB9IGVsc2Uge1xuXG4gaWYgKCRzY29wZS5pdGVtcy5sZW5ndGggPiAwKSB7XG4gY29uc29sZS5sb2coJ2l0ZW1zMjIyJywgJHNjb3BlLml0ZW1zKVxuXG4gZGVsZXRlX2NhY2hlZF9jYXNlX29iaigkd2luZG93KVxuXG4gQ2FzZV9nZW4uZmV0Y2hfaW1hZ2VzKHdpbmRvdy5lbmNvZGVVUklDb21wb25lbnQoY2FzZV9vYmouaW1hZ2VfdXJsKSkudGhlbihmdW5jdGlvbiAobmV3X3VybCkge1xuXG4gdmFyIHByb2ZpbGUgPSBDYXNlX2dlbi5nZXRfZGVmYXVsdF9wcm9maWxlKGNhc2Vfb2JqKVxuXG4gQ2FzZV9nZW4uY3JlYXRlX3Byb2ZpbGUocHJvZmlsZSkudGhlbihmdW5jdGlvbiAocHJvZmlsZV9pZCkge1xuIHZhciBwb3N0cyA9IFtdXG5cbiAkc2NvcGUuaXRlbXMubWFwKGZ1bmN0aW9uIChpdGVtKSB7XG4gZGVsZXRlIGl0ZW0ucHJvZmlsZVxuIGl0ZW0ucHJvZmlsZV9pZCA9IHByb2ZpbGVfaWRcbiBpZiAobmV3X3VybClcbiBpdGVtLmxvZ29fdXJsID0gbmV3X3VybFxuIHBvc3RzLnB1c2goQ2FydC5wb3N0X3RvX3NlcnZlcihpdGVtKSlcbiB9KVxuXG4gJHEuYWxsKHBvc3RzKS50aGVuKGZ1bmN0aW9uIChyZXN1bHRzKSB7XG4gY29uc29sZS5sb2coJ3Bvc3RlZCByZXN1bHRzJywgcmVzdWx0cylcbiAvLyBldmVyeXRoaW5nIGhhcyBiZWVuIHBvc3RlZCB0byBjYXJ0IGF0IHRoaXMgcG9pbnRcbiAkcm9vdFNjb3BlLiRicm9hZGNhc3QoQVVUSF9FVkVOVFMuY2FydFVwZGF0ZWQpO1xuICRsb2NhdGlvbi5wYXRoKCcvc2hvcHBpbmdfY2FydCcpXG4gfSlcblxuIH0pXG5cbiB9KVxuIH1cbiB9XG4gfVxuXG4gLy8gZncgYXNzdW1lcyBtYXJrIGFsd2F5cyBleGlzdFxuXG4gZnVuY3Rpb24gZ2V0X3R5cGUoY2FzZV9vKSB7XG4gaWYgKGNhc2Vfby5pbWFnZV91cmwpXG4gcmV0dXJuICd3bCdcbiBlbHNlXG4gcmV0dXJuICd3JzsgLy8gJ3cnLCAnbCcsICd3bCcgICB3b3JkIG9ubHkvbG9nby93b3JkIGFuZCBsb2dvXG4gfVxuXG4gfVxuICovXG4vKlxuIC8vIFJPVVRFIFJFU09MVkVcblxuIC8vIGZyb20gYnJvd3NlclxuIGZ1bmN0aW9uIGdldF9jYWNoZWRfY2FzZV9vYmooJHdpbmRvdykge1xuIHZhciByZXRcbiBpZiAoJHdpbmRvdy5zZXNzaW9uU3RvcmFnZSAmJiAkd2luZG93LnNlc3Npb25TdG9yYWdlLmNhc2Vfb2JqKVxuIHJldCA9IEpTT04ucGFyc2UoJHdpbmRvdy5zZXNzaW9uU3RvcmFnZS5jYXNlX29iailcbiByZXR1cm4gcmV0XG4gfVxuXG4gZnVuY3Rpb24gZGVsZXRlX2NhY2hlZF9jYXNlX29iaigkd2luZG93KSB7XG4gaWYgKCR3aW5kb3cuc2Vzc2lvblN0b3JhZ2UuY2FzZV9vYmopXG4gZGVsZXRlICR3aW5kb3cuc2Vzc2lvblN0b3JhZ2UuY2FzZV9vYmpcbiB9XG5cbiAqL1xuXG5cbi8vIGNhc2Vfb2JqIGZyb20gc2VydmVyXG5mdW5jdGlvbiBnZXRfY2FzZV9vYmooJHEsICRyb3V0ZSwgQ2FzZV9nZW4pIHtcbiAgICB2YXIgZGVmZXJyZWQgPSAkcS5kZWZlcigpO1xuICAgIGlmICgkcm91dGUuY3VycmVudC5wYXJhbXMuaWQpIHtcbiAgICAgICAgdmFyIG9iaiA9IENhc2VfZ2VuLmdldF9jYXNlX29iaigkcm91dGUuY3VycmVudC5wYXJhbXMuaWQpXG4gICAgICAgIGRlZmVycmVkLnJlc29sdmUob2JqKVxuICAgIH0gZWxzZVxuICAgICAgICBkZWZlcnJlZC5yZWplY3Qoe21zZzogJ25vIGNhc2Ugbm8gcHJvdmlkZWQnfSlcbiAgICByZXR1cm4gZGVmZXJyZWQucHJvbWlzZVxufVxuXG4vLyByZXNvbHZlIGZvciBjYXNlMVxuXG5leHBvcnRzLmI0Y2FzZV9nZW4gPSBmdW5jdGlvbiAoJHEsICRyb3V0ZSwgJGxvY2F0aW9uLCAkd2luZG93LCBDYXNlX2dlbikge1xuICAgIHZhciBjYXNlX2NvZGUgPSAkcm91dGUuY3VycmVudC5wYXJhbXMuaWRcbiAgICB2YXIgbmV3X2Nhc2VfY29kZSA9IGdldF9jYXNlX2NvZGUoY2FzZV9jb2RlKSAgICAvLyBsZXQncyBub3JtYWl6ZSB0aGUgY2FzZSBjb2RlIGlmIHBvc3NpYmxlXG4gICAgaWYgKG5ld19jYXNlX2NvZGUgIT09IGNhc2VfY29kZSkgICAgICAgICAgICAgICAgLy8gbm9ybWxpemVkIG9uZSBpcyBkaWZmZXJlbnQsIGRvIGEgcmVkaXJlY3RcbiAgICAgICAgJGxvY2F0aW9uLnBhdGgoJy9jYXNlLycgKyBuZXdfY2FzZV9jb2RlKVxuICAgIGVsc2Uge1xuICAgICAgICByZXR1cm4gZ2V0X2Nhc2Vfb2JqKCRxLCAkcm91dGUsIENhc2VfZ2VuKSAgIC8vIG1ha2UgdGhlIGNhc2Vfb2JqIGF2YWlsYWJsZSB0byBjb250cm9sbGVyXG4gICAgfVxufVxuXG5mdW5jdGlvbiBnZXRfY2FzZV9jb2RlKG9sZF9jb2RlKSB7XG4gICAgdmFyIHRlbXAgPSBvbGRfY29kZS50b1VwcGVyQ2FzZSgpXG4gICAgaWYgKCh0ZW1wLmxlbmd0aCA9PT0gNykgJiYgKCh0ZW1wLnNwbGl0KCctJykubGVuZ3RoIC0gMSkgPT0gMCkpIC8vIE0xMjM0QUE/IGxldCdzIG5vcm1hbGl6ZSBpdFxuICAgICAgICByZXR1cm4gdGVtcC5zdWJzdHIoMCwgMSkgKyAnLScgKyB0ZW1wLnN1YnN0cigxLCA0KSArICctJyArIHRlbXAuc3Vic3RyKDUsIDIpXG4gICAgZWxzZVxuICAgICAgICByZXR1cm4gdGVtcFxufVxuXG4vKlxuXG4gLy8gcmVzb2x2ZSBmb3IgY2FzZTJcbiBleHBvcnRzLmI0Y2FzZV9nZW4yID0gZnVuY3Rpb24gKCRxLCAkcm91dGUsICRsb2NhdGlvbiwgJHdpbmRvdywgQ2FzZV9nZW4pIHtcbiBpZiAoJHdpbmRvdy5zZXNzaW9uU3RvcmFnZS5jYXNlX29iailcbiByZXR1cm4gZ2V0X2Nhc2Vfb2JqKCRxLCAkcm91dGUsIENhc2VfZ2VuKVxuIGVsc2VcbiAkbG9jYXRpb24ucGF0aCgnL2Nhc2UvJyArICRyb3V0ZS5jdXJyZW50LnBhcmFtcy5pZClcbiB9XG4gKi8iLCJcInVzZSBzdHJpY3RcIjtcblxuLy8gdGhpcyByZXR1cm5zIGluZm8gYWJvdXQgdGhlIG5vbiBsb2cgaW4gdXNlciwgZW1haWxzIHJldHVybnMgZXhpc3RpbmcgZW1haWxcblxuZXhwb3J0cy5iNGNoZWNrX291dF9pbml0aWFsX2NoZWNrID0gZnVuY3Rpb24gKENhcnQpIHtcbiAgcmV0dXJuIENhcnQuZ2V0RXhpc3RpbmdFbWFpbHMoKSAgLy8gcmV0dXJuIGEgcHJvbWlzZSBmb3IgJ3Jlc29sdmUnIGluIG5nLXJvdXRlXG59XG5cbi8vIGlmIHNpZ25pbiwgaXQgcmVkaXJlY3RzIHRvICdjaGVja19vdXRfc3RyaXBlJ1xuLy8gZWxzZSBjaGVjayBpZiBlbWFpbHMgb2YgcHJvZmlsZSBleGlzdCwgaWYgeWVzLCBwcm9tcHQgZm9yIGxvZ2luXG4vLyBlbHNlIHByb21wdCBmb3IgYSBwYXNzd29yZCB0byBkbyBhIHF1aWNrIHNpZ24gdXBcblxuZXhwb3J0cy5iNGNoZWNrX291dCA9IGZ1bmN0aW9uICgkc2NvcGUsICRyb3V0ZVBhcmFtcywgJGxvY2F0aW9uLCAkcm9vdFNjb3BlLCBhdXRoU2VydmljZSwgQ2FydCwgdmFsaWRfZW1haWxzLCBBVVRIX0VWRU5UUykge1xuXG4gIGNvbnNvbGUubG9nKCdiNGRhdGEnLCB2YWxpZF9lbWFpbHMpXG5cbiAgaWYgKGF1dGhTZXJ2aWNlLmlzU2lnbmVkSW4oKSkge1xuICAgICRsb2NhdGlvbi5wYXRoKCcvY2hlY2tfb3V0X3N0cmlwZS8nICsgJHJvdXRlUGFyYW1zLmNhcnRfaWQpXG4gIH0gZWxzZSB7XG4gICAgJHNjb3BlLmV4aXN0aW5nX2VtYWlscyA9IHZhbGlkX2VtYWlscy5sZW5ndGggPiAwXG4gICAgaWYgKHZhbGlkX2VtYWlscy5sZW5ndGggPiAwKVxuICAgICAgJHNjb3BlLnVzZXIgPSB7IGVtYWlsOiB2YWxpZF9lbWFpbHNbMF0gfSAvLyBzZXQgdXNlciBlbWFpbCBmb3IgbG9naW5cbiAgICBlbHNlIHsgLy8gbGV0J3Mgb2J0YWluIHRoZSBmaXJzdCBwcm9maWxlIGFzIGRlZmF1bHQgdG8gc2lnbiB1cFxuICAgICAgJHNjb3BlLnByb2ZpbGUgPSBDYXJ0LmdldERlZmF1bHRMb2NhbFByb2ZpbGUoKVxuICAgICAgY29uc29sZS5sb2coJ2RlZmF1bHQgcHJvZmlsZScsICRzY29wZS5wcm9maWxlKVxuICAgICAgJHNjb3BlLnVzZXIgPSB7XG4gICAgICAgIGVtYWlsOiAkc2NvcGUucHJvZmlsZS5lbWFpbCxcbiAgICAgICAgbmFtZTogJHNjb3BlLnByb2ZpbGUucHJvZmlsZV9uYW1lLFxuICAgICAgICBwcm9maWxlOiAkc2NvcGUucHJvZmlsZVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gICRzY29wZS5hdXRoZW50aWNhdGUgPSBmdW5jdGlvbiAodXNlcikge1xuXG4gICAgYXV0aFNlcnZpY2Uuc2lnbkluKHVzZXIpLnRoZW4oZnVuY3Rpb24gKHN0YXR1cykge1xuICAgICAgaWYgKHN0YXR1cy5hdXRoZW50aWNhdGVkID09PSB0cnVlKSB7XG5cbiAgICAgICAgQ2FydC5zdG9yZV90b19zZXJ2ZXIoKS50aGVuKGZ1bmN0aW9uIChzdGF0dXMpIHtcbiAgICAgICAgICBjb25zb2xlLmxvZygnc3RvcmUgdG8gc2VydmVyIHN0YXR1cycsIHN0YXR1cylcbiAgICAgICAgICBDYXJ0LmNsZWFuX2xvY2FsX3N0b3JlKClcblxuICAgICAgICAgICRyb290U2NvcGUuJGJyb2FkY2FzdChBVVRIX0VWRU5UUy5jYXJ0VXBkYXRlZCk7XG5cbiAgICAgICAgICAkcm9vdFNjb3BlLiRicm9hZGNhc3QoQVVUSF9FVkVOVFMubG9naW5TdWNjZXNzKTtcbiAgICAgICAgICBDYXJ0LmdldFJlbW90ZUNhcnRDb3VudCgpLnRoZW4oZnVuY3Rpb24gKGNvdW50KSB7XG4gICAgICAgICAgICBpZiAoY291bnQgPiAwKVxuICAgICAgICAgICAgICBDYXJ0LmdldFJlbW90ZUNhcnQoKS50aGVuKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICAgICAgJGxvY2F0aW9uLnBhdGgoJy9jaGVja19vdXRfc3RyaXBlLycgKyBkYXRhLmNhcnQuX2lkKVxuICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAgICAkbG9jYXRpb24ucGF0aCgnL2Rhc2hib2FyZCcpXG4gICAgICAgICAgfSlcbiAgICAgICAgfSlcbiAgICAgIH1cbiAgICAgIGVsc2Uge1xuICAgICAgICAkc2NvcGUuYWxlcnRzID0gW11cbiAgICAgICAgJHNjb3BlLmFsZXJ0cy5wdXNoKHsgdHlwZTogJ3dhcm5pbmcnLCBtc2c6IHN0YXR1cy5tZXNzYWdlIH0pO1xuICAgICAgfVxuICAgIH0pXG4gIH1cblxuICAkc2NvcGUucmVnaXN0ZXIyID0gZnVuY3Rpb24gKHVzZXIpIHtcbiAgICAkc2NvcGUuYWxlcnRzID0gW11cbiAgICBjb25zb2xlLmxvZygncmVnMicpXG4gICAgY29uc29sZS5sb2codXNlcilcblxuICAgIGF1dGhTZXJ2aWNlLnNpZ25VcCh1c2VyKS50aGVuKGZ1bmN0aW9uIChzdGF0dXMpIHtcbiAgICAgIGlmIChzdGF0dXMucmVnaXN0ZXJlZCkge1xuICAgICAgICAkc2NvcGUuYXV0aGVudGljYXRlKHsgZW1haWw6IHVzZXIuZW1haWwsIHBhc3N3b3JkOiB1c2VyLnBhc3N3b3JkIH0pXG4gICAgICB9XG4gICAgICBlbHNlIHtcbiAgICAgICAgJHNjb3BlLmFsZXJ0cyA9IFtdXG4gICAgICAgICRzY29wZS5hbGVydHMucHVzaCh7IHR5cGU6ICd3YXJuaW5nJywgbXNnOiBzdGF0dXMubWVzc2FnZSB9KTtcbiAgICAgIH1cbiAgICB9KVxuICB9XG5cblxufVxuXG4vLyBub3QgaW4gdXNlXG5cbmV4cG9ydHMuYjRjaGVja19vdXRfbm9uX21lbWJlciA9IGZ1bmN0aW9uICgkc2NvcGUsICRyb3V0ZVBhcmFtcywgJGxvY2F0aW9uLCAkcm9vdFNjb3BlLCBhdXRoU2VydmljZSwgQ2FydCwgQVVUSF9FVkVOVFMpIHtcbiAgY29uc29sZS5sb2coJ25vbiBtZW1iZXInKVxuXG4gICRzY29wZS5hbGVydHMgPSBbXTtcblxuICAkc2NvcGUuY2xvc2VBbGVydCA9IGZ1bmN0aW9uIChpbmRleCkge1xuICAgICRzY29wZS5hbGVydHMuc3BsaWNlKGluZGV4LCAxKTtcbiAgfTtcblxuXG4gIENhcnQuZ2V0UmVtb3RlQ2FydCgpLnRoZW4oZnVuY3Rpb24gKGl0ZW1zKSB7XG4gICAgY29uc29sZS5sb2coaXRlbXMpXG4gICAgJHNjb3BlLmRhdGEgPSBpdGVtc1xuICB9KVxuXG4gICRzY29wZS5yZWdpc3RlcjIgPSBmdW5jdGlvbiAodXNlcikge1xuICAgICRzY29wZS5hbGVydHMgPSBbXVxuICAgIGNvbnNvbGUubG9nKCdyZWcyJylcbiAgICBjb25zb2xlLmxvZyh1c2VyKVxuXG4gICAgYXV0aFNlcnZpY2Uuc2lnblVwKHVzZXIpLnRoZW4oZnVuY3Rpb24gKHN0YXR1cykge1xuICAgICAgaWYgKHN0YXR1cy5yZWdpc3RlcmVkKSB7XG4gICAgICAgICRzY29wZS5hdXRoZW50aWNhdGUoeyBlbWFpbDogdXNlci5lbWFpbCwgcGFzc3dvcmQ6IHVzZXIucGFzc3dvcmQgfSlcbiAgICAgIH1cbiAgICAgIGVsc2Uge1xuICAgICAgICAkc2NvcGUuYWxlcnRzID0gW11cbiAgICAgICAgJHNjb3BlLmFsZXJ0cy5wdXNoKHsgdHlwZTogJ3dhcm5pbmcnLCBtc2c6IHN0YXR1cy5tZXNzYWdlIH0pO1xuICAgICAgfVxuICAgIH0pXG4gIH1cblxuICAkc2NvcGUuYXV0aGVudGljYXRlID0gZnVuY3Rpb24gKHVzZXIpIHtcblxuICAgIGF1dGhTZXJ2aWNlLnNpZ25Jbih1c2VyKS50aGVuKGZ1bmN0aW9uIChzdGF0dXMpIHtcbiAgICAgIGlmIChzdGF0dXMuYXV0aGVudGljYXRlZCA9PT0gdHJ1ZSkge1xuXG4gICAgICAgIENhcnQuc3RvcmVfdG9fc2VydmVyKCkudGhlbihmdW5jdGlvbiAoc3RhdHVzKSB7XG4gICAgICAgICAgY29uc29sZS5sb2coJ3N0b3JlIHRvIHNlcnZlciBzdGF0dXMnLCBzdGF0dXMpXG4gICAgICAgICAgQ2FydC5jbGVhbl9sb2NhbF9zdG9yZSgpXG5cbiAgICAgICAgICAkcm9vdFNjb3BlLiRicm9hZGNhc3QoQVVUSF9FVkVOVFMuY2FydFVwZGF0ZWQpO1xuXG4gICAgICAgICAgJHJvb3RTY29wZS4kYnJvYWRjYXN0KEFVVEhfRVZFTlRTLmxvZ2luU3VjY2Vzcyk7XG4gICAgICAgICAgQ2FydC5nZXRSZW1vdGVDYXJ0Q291bnQoKS50aGVuKGZ1bmN0aW9uIChjb3VudCkge1xuICAgICAgICAgICAgaWYgKGNvdW50ID4gMClcbiAgICAgICAgICAgICAgQ2FydC5nZXRSZW1vdGVDYXJ0KCkudGhlbihmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgICAgICRsb2NhdGlvbi5wYXRoKCcvY2hlY2tfb3V0X3N0cmlwZS8nICsgZGF0YS5jYXJ0Ll9pZClcbiAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgICAgJGxvY2F0aW9uLnBhdGgoJy9kYXNoYm9hcmQnKVxuICAgICAgICAgIH0pXG4gICAgICAgIH0pXG4gICAgICB9XG4gICAgICBlbHNlIHtcbiAgICAgICAgJHNjb3BlLmFsZXJ0cyA9IFtdXG4gICAgICAgICRzY29wZS5hbGVydHMucHVzaCh7IHR5cGU6ICd3YXJuaW5nJywgbXNnOiBzdGF0dXMubWVzc2FnZSB9KTtcbiAgICAgIH1cbiAgICB9KVxuICB9XG5cblxufVxuXG5cbiIsIlwidXNlIHN0cmljdFwiO1xuXG52YXIgY291bnRyaWVzID0gcmVxdWlyZSgnY291bnRyaWVzJylcblxudmFyIGV1cm9wZV9jb3VudHJpZXMgPSBjb3VudHJpZXMuZ2V0X2NvdW50cmllc19ieV9jb250bmVudChcIkVVXCIpLm1hcCgoaXRlbSkgPT4gaXRlbS5jb2RlKVxuXG5leHBvcnRzLmV1X2Nvb2tpZXMgPSBmdW5jdGlvbiAoJGh0dHAsICRyb3V0ZVBhcmFtcywgJGxvY2F0aW9uLCAkcm91dGUsICRsb2NhbGUsICR3aW5kb3csIENhcmRzLCBBVVRIX0VWRU5UUykge1xuXG4gICAgdGhpcy5tc2cgPSAndGVzdGluZyBFVSBDT09LSUUnXG5cbiAgICB0aGlzLnNob3dfY29va2llc19yZW1pbmRlciA9IGZhbHNlICAvLyBpbml0aWFsIGZhbHNlXG5cbiAgICAvLyBsZXQgdXJsID0gJy9hcGkyL2dldF9nZW8/dGVzdF9pcD02Mi4yMTAuNzEuMjI1J1xuICAvLyAgbGV0IHVybCA9ICcvYXBpMi9nZXRfZ2VvP3Rlc3RfaXA9NDUuNzQuNC4xMDInXG4gICAgbGV0IHVybCA9ICcvYXBpMi9nZXRfZ2VvJ1xuXG5cbiAgICBjb25zb2xlLmxvZygnY2hlY2tpbmcgY29va2llcycpXG4gICAgXG4gICAgLy8gMTIzLjEyMy4xMjMuMTIzIGNuXG4gICAgLy8gNjIuMjEwLjcxLjIyNSBmclxuXG4gICAgJGh0dHAuZ2V0KHVybCkuc3VjY2VzcygoZ2VvKSA9PiB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdnZW8nLCBnZW8pXG4gICAgICAgIGlmIChnZW8uY291bnRyeSkge1xuXG4gICAgICAgICAgICBjb25zb2xlLmxvZygnY2hlY2tpbmcnKVxuICAgICAgICAgICAgY29uc29sZS5sb2coZXVyb3BlX2NvdW50cmllcy5pbmRleE9mKGdlby5jb3VudHJ5KSlcbiAgICAgICAgICAgIHRoaXMuc2hvd19jb29raWVzX3JlbWluZGVyID0gZXVyb3BlX2NvdW50cmllcy5pbmRleE9mKGdlby5jb3VudHJ5KSAhPSAtMVxuICAgICAgICAgICAgY29uc29sZS5sb2codGhpcylcbiAgICAgICAgfVxuICAgIH0pXG5cbn1cblxuXG5cbiIsIlwidXNlIHN0cmljdFwiO1xuXG52YXIgY291bnRyaWVzID0gcmVxdWlyZSgnY291bnRyaWVzJylcblxuZXhwb3J0cy5zdHJpcGUgPSBmdW5jdGlvbiAoJHNjb3BlLCAkaHR0cCwgJHJvdXRlUGFyYW1zLCAkbG9jYXRpb24sICRyb290U2NvcGUsICRsb2NhbGUsICR3aW5kb3csIENhcmRzLCBBVVRIX0VWRU5UUykge1xuXG4gICRzY29wZS5jdXJyZW50WWVhciA9IG5ldyBEYXRlKCkuZ2V0RnVsbFllYXIoKVxuICAkc2NvcGUuY3VycmVudE1vbnRoID0gbmV3IERhdGUoKS5nZXRNb250aCgpICsgMVxuICAkc2NvcGUubW9udGhzID0gJGxvY2FsZS5EQVRFVElNRV9GT1JNQVRTLk1PTlRIXG4gICRzY29wZS5jb3VudHJpZXMgPSBjb3VudHJpZXMuZ2V0X2NvdW50cmllcygpXG5cbiAgJHNjb3BlLmZvcm1EYXRhID0ge31cblxuICBDYXJkcy5nZXRfdXNlcl9pbmZvKCRyb3V0ZVBhcmFtcy5jYXJ0X2lkKS50aGVuKGZ1bmN0aW9uIChpbmZvKSB7XG5cbiAgICBjb25zb2xlLmxvZygnaW5mbycsIGluZm8pXG4gICAgaWYgKGluZm8ueF9zdGF0ZSlcbiAgICAgIGluZm8ueF9zdGF0ZSA9IGNvdW50cmllcy5nZXRfc3RhdGVfbmFtZShpbmZvLnhfc3RhdGUpXG4gICAgJHNjb3BlLmZvcm1EYXRhID0gaW5mbyAgLy8gc2VlIGFib3ZlIGZvciBkYXRhIHJldHVybmVkXG4gIH0pXG5cbiAgJGh0dHAuZ2V0KFwiL2FwaS9nZXRfY2FydF9hbW91bnQvXCIgKyAkcm91dGVQYXJhbXMuY2FydF9pZCkuc3VjY2VzcyhmdW5jdGlvbiAoZGF0YSkge1xuXG4gICAgY29uc29sZS5sb2coJ3RvdGFsIGFtdCcsIGRhdGEpXG4gICAgJHNjb3BlLmNhcnRfdG90YWwgPSBkYXRhLnRvdGFsXG4gIH0pXG5cbiAgJGh0dHAuZ2V0KFwiL2FwaV9wYXltZW50L2dldF9zdHJpcGVfcHVibGljX2tleVwiKS5zdWNjZXNzKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgJHNjb3BlLnN0cmlwZV9wayA9IGRhdGEuc3RyaXBlX3BrXG4gIH0pXG5cbiAgJHNjb3BlLiR3YXRjaCgnZm9ybURhdGEuZXhwX21vbnRoJywgZnVuY3Rpb24gKG5ld1ZhbCwgb2xkVmFsKSB7XG4gICAgJHNjb3BlLmZvcm1EYXRhLnhfZXhwX21vbjIgPSBwYXJzZUludChuZXdWYWwpXG4gIH0pO1xuXG4gICRzY29wZS4kd2F0Y2goJ2Zvcm1EYXRhLmV4cF95ZWFyJywgZnVuY3Rpb24gKG5ld1ZhbCwgb2xkVmFsKSB7XG4gICAgJHNjb3BlLmZvcm1EYXRhLnhfZXhwX3llYXIyID0gcGFyc2VJbnQobmV3VmFsKVxuICB9KTtcblxuICAkc2NvcGUucHJvY2Vzc0Zvcm0gPSBmdW5jdGlvbiAoc3RhdHVzLCByZXNwb25zZSkge1xuXG4gICAgaWYgKHJlc3BvbnNlLmhhc093blByb3BlcnR5KFwiZXJyb3JcIikpIHtcbiAgICAgICRzY29wZS5hbGVydHMgPSBbXVxuICAgICAgJHNjb3BlLmFsZXJ0cy5wdXNoKHsgdHlwZTogJ3dhcm5pbmcnLCBtc2c6IHJlc3BvbnNlLmVycm9yLm1lc3NhZ2UgfSk7XG4gICAgfSBlbHNlIHtcblxuICAgICAgJHNjb3BlLm1zZyA9ICdDb250YWN0aW5nIFBheW1lbnQgR2F0ZXdheSwgUGxlYXNlIHdhaXQuLi4nXG5cbiAgICAgICRodHRwLnBvc3QoXCIvYXBpX3BheW1lbnQvY2hlY2tfb3V0X3dpdGhfc3RyaXBlL1wiICsgJHJvdXRlUGFyYW1zLmNhcnRfaWQsIHJlc3BvbnNlKS5zdWNjZXNzKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKGRhdGEpXG4gICAgICAgIGlmIChkYXRhLm9rKSB7XG4gICAgICAgICAgJHJvb3RTY29wZS4kYnJvYWRjYXN0KEFVVEhfRVZFTlRTLmNhcnRVcGRhdGVkKTtcbiAgICAgICAgICAkbG9jYXRpb24ucGF0aCgnL3Bvc3RfcGF5bWVudDIvJyArIGRhdGEub3JkZXJfa2V5KVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICRzY29wZS5tc2cgPSBudWxsXG4gICAgICAgICAgJHNjb3BlLmFsZXJ0cyA9IFtdXG4gICAgICAgICAgJHNjb3BlLmFsZXJ0cy5wdXNoKHsgdHlwZTogJ3dhcm5pbmcnLCBtc2c6IGRhdGEuZXJyLm1lc3NhZ2UgfSk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH1cbiAgfTtcbn1cblxuXG5cbiIsIi8vIEB0cy1jaGVja1xuXCJ1c2Ugc3RyaWN0XCI7XG4vKmdsb2JhbCBhbmd1bGFyLCBjb25zb2xlICovXG4vLyB0aGlzIGlzIGRlZmluZWQgYXMgYW5ndWxhciBtb2R1bGUsIHNvIHNob3VsZCBiZSBpbmNsdWRlZCBpbiB3ZWJfYXBwLmpzXG5cbnZhciBjb3VudHJpZXMgPSByZXF1aXJlKCdjb3VudHJpZXMnKVxudmFyIG9yZ2FuaXphdGlvbnMgPSByZXF1aXJlKFwiLi4vLi4vLi4vZGF0YS9vcmdhbml6YXRpb25zLmpzb25cIilcbnZhciBvYXBpID0gcmVxdWlyZShcIi4uLy4uLy4uL2RhdGEvb2FwaS5qc29uXCIpXG52YXIgYXJpcG8gPSByZXF1aXJlKFwiLi4vLi4vLi4vZGF0YS9hcmlwby5qc29uXCIpXG52YXIgZXUgPSByZXF1aXJlKFwiLi4vLi4vLi4vZGF0YS9ldS5qc29uXCIpXG5cbmFuZ3VsYXIubW9kdWxlKCd0a3MucmVnLmNvbnRyb2xsZXJzJywgW10pXG5cbiAgICAvLyB0cmFkZW1hcmsgc3RhcnQgaGVyZSwgZWl0aGVyIGdvZXMgdG8gYSAnc3R1ZHknIG9yICdyZWdpc3RlcidcblxuICAgIC5jb250cm9sbGVyKCd0cmFkZW1hcmsnLCBbJyRzY29wZScsICckcm91dGVQYXJhbXMnLCAnJGxvY2F0aW9uJywgJyR0cmFuc2xhdGUnLCAncmVnaXN0ZXJfdHJhZGVtYXJrJywgJ2NvdW50cnlfaW5mbycsICdwYWdlX2luZm8nLFxuICAgICAgICBmdW5jdGlvbiAoJHNjb3BlLCAkcm91dGVQYXJhbXMsICRsb2NhdGlvbiwgJHRyYW5zbGF0ZSwgcmVnaXN0ZXJfdHJhZGVtYXJrLCBjb3VudHJ5X2luZm8sIHBhZ2VfaW5mbykge1xuXG4gICAgICAgICAgICBjb25zb2xlLmxvZygnaW5mbzEyMycsIGNvdW50cnlfaW5mbylcblxuXG4gICAgICAgICAgICB2YXIgc2VvX3N0cmluZyA9IGNvdW50cnlfaW5mby5jb3VudHJ5Lm5hbWUgKyBcIiBUcmFkZW1hcmsgUmVnaXN0cmF0aW9uLCBSZWdpc3RlciBUcmFkZW1hcmsgXCIgKyBjb3VudHJ5X2luZm8uY291bnRyeS5uYW1lXG5cbiAgICAgICAgICAgIHBhZ2VfaW5mby5zZXRUaXRsZShzZW9fc3RyaW5nKVxuICAgICAgICAgICAgcGFnZV9pbmZvLnNldF9rZXl3b3JkcyhzZW9fc3RyaW5nKVxuICAgICAgICAgICAgcGFnZV9pbmZvLnNldF9kZXNjcmlwdGlvbihzZW9fc3RyaW5nKVxuXG4gICAgICAgICAgICB2YXIgY291bnRyeV9uYW1lID0gY291bnRyaWVzLmdldF9jb3VudHJ5KCRyb3V0ZVBhcmFtcy5jb3VudHJ5KS5uYW1lXG5cbiAgICAgICAgICAgICRzY29wZS5jb3VudHJ5X2luZm8gPSBjb3VudHJ5X2luZm9cbiAgICAgICAgICAgICRzY29wZS5jb3VudHJ5X2luZm8uc3R1ZHlfZGlzYWJsZWQgPSAnZGlzYWJsZWQnXG5cbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdzaG93IG1lIGNvdW50cnlfaW5mbycsIGNvdW50cnlfaW5mbylcblxuICAgICAgICAgICAgaWYgKG9hcGlbJHJvdXRlUGFyYW1zLmNvdW50cnldID09PSB1bmRlZmluZWQpIHtcblxuICAgICAgICAgICAgICAgICRzY29wZS5jb3VudHJ5ID0gJHJvdXRlUGFyYW1zLmNvdW50cnlcbiAgICAgICAgICAgICAgICAkc2NvcGUucmVhZHkgPSByZWdpc3Rlcl90cmFkZW1hcmsuaXNJbXBsZW1lbnRlZCgkc2NvcGUuY291bnRyeSlcblxuICAgICAgICAgICAgICAgIGlmIChvcmdhbml6YXRpb25zWyRzY29wZS5jb3VudHJ5XSlcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm90aGVycyA9IG9yZ2FuaXphdGlvbnNbJHNjb3BlLmNvdW50cnldWzBdXG4gICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIC8vIG5vdCBhbiBvcmdhbml6YXRpb24sIGNoZWNrIGlmIHRoZSBjb3VudHJ5IGhhcyByZWxhdGVkIG9yZ2FuaXphdGlvblxuICAgICAgICAgICAgICAgICAgICBpZiAoYXJpcG9bJHJvdXRlUGFyYW1zLmNvdW50cnldICE9PSB1bmRlZmluZWQpXG4gICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUub3RoZXJzID0gb3JnYW5pemF0aW9ucy5BUklQT1sxXS5yZXBsYWNlKC9fY291bnRyeV8vZywgY291bnRyeV9uYW1lKVxuICAgICAgICAgICAgICAgICAgICBlbHNlIGlmIChldVskcm91dGVQYXJhbXMuY291bnRyeV0gIT09IHVuZGVmaW5lZClcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5vdGhlcnMgPSBvcmdhbml6YXRpb25zLkVVWzFdLnJlcGxhY2UoL19jb3VudHJ5Xy9nLCBjb3VudHJ5X25hbWUpXG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICRsb2NhdGlvbi5wYXRoKCcvdHJhZGVtYXJrL09BUEknKVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAvLyAgJHNjb3BlLnN0dWR5X2Rpc2FibGVkID0gY291bnRyeV9pbmZvLnN0dWR5X2F2YWlsYWJsZSA/IFwiXCI6IFwiZGlzYWJsZWRcIlxuXG5cbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdzdHVkeV9kaXNhYmxlJywgJHNjb3BlLnN0dWR5X2Rpc2FibGVkKVxuICAgICAgICAgICAgJHNjb3BlLnN0YXR1cyA9IFwicmVhZHlcIlxuXG4gICAgICAgICAgICAkc2NvcGUubG9jYWxlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdhc2RmYXNkZicsICR0cmFuc2xhdGUudXNlKCkpXG4gICAgICAgICAgICAgICAgcmV0dXJuICR0cmFuc2xhdGUudXNlKClcbiAgICAgICAgICAgIH1cblxuXG4gICAgICAgIH1cbiAgICBdKVxuXG4gICAgLmNvbnRyb2xsZXIoJ3RyYWRlbWFya1NFTycsIGZ1bmN0aW9uICgkc2NvcGUsICRyb3V0ZVBhcmFtcywgJGxvY2F0aW9uLCAkdHJhbnNsYXRlLCByZWdpc3Rlcl90cmFkZW1hcmssIGNvdW50cnlfaW5mbywgcGFnZV9pbmZvKSB7XG5cbiAgICAgICAgY29uc29sZS5sb2coJ3RyYWRlbWFya1NFTycsICRsb2NhdGlvbi5wYXRoKCkpXG4gICAgICAgIGNvbnNvbGUuZGlyKCd0cmFkZW1hcmtTRU8nLCAkcm91dGVQYXJhbXMpXG4gICAgICAgIGNvbnNvbGUubG9nKCdjb3VudHJ5IGluZm8nLCBjb3VudHJ5X2luZm8pXG5cbiAgICAgICAgLy8gdmFyIHNlb19zdHJpbmcgPSBjb3VudHJ5X2luZm8uY291bnRyeS5uYW1lICsgXCIgVHJhZGVtYXJrIFJlZ2lzdHJhdGlvbiwgUmVnaXN0ZXIgVHJhZGVtYXJrIFwiICsgY291bnRyeV9pbmZvLmNvdW50cnkubmFtZVxuXG4gICAgICAgIHZhciBzZW9fc3RyaW5nID0gXCJUcmFkZW1hcmsgUmVnaXN0cmF0aW9uIGluIFwiICsgY291bnRyeV9pbmZvLmNvdW50cnkubmFtZSArIFwiIC0gVHJhZGVNYXJrZXJzwq4uY29tXCJcblxuICAgICAgICBjb25zb2xlLmxvZygnc2VvX3N0cmluZycsIHNlb19zdHJpbmcpXG4gICAgICAgIHBhZ2VfaW5mby5zZXRUaXRsZShzZW9fc3RyaW5nKVxuICAgICAgICBwYWdlX2luZm8uc2V0X2J5X2NvdW50cnlfY29kZShjb3VudHJ5X2luZm8uY291bnRyeS5jb2RlKVxuICAgICAgICBjb25zb2xlLmxvZygnY29uIGluZm8gJywgY291bnRyeV9pbmZvKVxuXG5cbiAgICAgICAgLypcbiAgICAgICAgcGFnZV9pbmZvLnNldF9rZXl3b3JkcyhzZW9fc3RyaW5nKVxuICAgICAgICBwYWdlX2luZm8uc2V0X2Rlc2NyaXB0aW9uKHNlb19zdHJpbmcpXG4gICAgICAgICovXG5cblxuICAgICAgICB2YXIgY291bnRyeV9uYW1lID0gY291bnRyeV9pbmZvLmNvdW50cnkubmFtZVxuICAgICAgICB2YXIgY291bnRyeV9jb2RlID0gY291bnRyeV9pbmZvLmNvdW50cnkuY29kZVxuXG4gICAgICAgICRzY29wZS5jb3VudHJ5X2luZm8gPSBjb3VudHJ5X2luZm9cbiAgICAgICAgJHNjb3BlLnN0dWR5X2Rpc2FibGVkID0gY291bnRyeV9pbmZvLnN0dWR5X2F2YWlsYWJsZSA/IFwiXCIgOiBcImRpc2FibGVkXCJcbiAgICAgICAgJHNjb3BlLnRyYWRlbWFya19kaXNhYmxlZCA9IGNvdW50cnlfaW5mby50cmFkZW1hcmtfYXZhaWxhYmxlID8gXCJcIiA6IFwiZGlzYWJsZWRcIlxuXG4gICAgICAgICRzY29wZS5sb2NhbGUgPSBmdW5jdGlvbiAoKSB7XG5cbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdhc2RmYXNkZicsICR0cmFuc2xhdGUudXNlKCkpXG4gICAgICAgICAgICByZXR1cm4gJHRyYW5zbGF0ZS51c2UoKVxuICAgICAgICB9XG5cblxuICAgICAgICBpZiAob2FwaVtjb3VudHJ5X2NvZGVdID09PSB1bmRlZmluZWQpIHtcblxuICAgICAgICAgICAgJHNjb3BlLmNvdW50cnkgPSBjb3VudHJ5X2NvZGVcbiAgICAgICAgICAgICRzY29wZS5yZWFkeSA9IHJlZ2lzdGVyX3RyYWRlbWFyay5pc0ltcGxlbWVudGVkKCRzY29wZS5jb3VudHJ5KVxuXG4gICAgICAgICAgICBpZiAob3JnYW5pemF0aW9uc1skc2NvcGUuY291bnRyeV0pXG4gICAgICAgICAgICAgICAgJHNjb3BlLm90aGVycyA9IG9yZ2FuaXphdGlvbnNbJHNjb3BlLmNvdW50cnldWzBdXG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAvLyBub3QgYW4gb3JnYW5pemF0aW9uLCBjaGVjayBpZiB0aGUgY291bnRyeSBoYXMgcmVsYXRlZCBvcmdhbml6YXRpb25cbiAgICAgICAgICAgICAgICBpZiAoYXJpcG9bJHNjb3BlLmNvdW50cnldICE9PSB1bmRlZmluZWQpXG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5vdGhlcnMgPSBvcmdhbml6YXRpb25zLkFSSVBPWzFdLnJlcGxhY2UoL19jb3VudHJ5Xy9nLCBjb3VudHJ5X25hbWUpXG4gICAgICAgICAgICAgICAgZWxzZSBpZiAoZXVbJHNjb3BlLmNvdW50cnldICE9PSB1bmRlZmluZWQpXG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5vdGhlcnMgPSBvcmdhbml6YXRpb25zLkVVWzFdLnJlcGxhY2UoL19jb3VudHJ5Xy9nLCBjb3VudHJ5X25hbWUpXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICRsb2NhdGlvbi5wYXRoKCcvdHJhZGVtYXJrL09BUEknKVxuICAgICAgICB9XG4gICAgfVxuICAgIClcblxuXG4gICAgLypcbiAgICAgLmNvbnRyb2xsZXIoJ3RyYWRlbWFya1NFTycsIFsnJHNjb3BlJywgJyRyb3V0ZVBhcmFtcycsICckbG9jYXRpb24nLCAncmVnaXN0ZXJfdHJhZGVtYXJrJywgJ2NvdW50cnlfaW5mbycsICdwYWdlX2luZm8nLFxuICAgICBmdW5jdGlvbiAoJHNjb3BlLCAkcm91dGVQYXJhbXMsICRsb2NhdGlvbiwgcmVnaXN0ZXJfdHJhZGVtYXJrLCBjb3VudHJ5X2luZm8sIHBhZ2VfaW5mbykge1xuXG4gICAgIGNvbnNvbGUubG9nKCd0cmFkZW1hcmtTRU8nLCAkbG9jYXRpb24ucGF0aCgpKVxuICAgICBjb25zb2xlLmRpcigndHJhZGVtYXJrU0VPJywgJHJvdXRlUGFyYW1zKVxuICAgICBjb25zb2xlLmxvZygnY291bnRyeSBpbmZvJywgY291bnRyeV9pbmZvKVxuXG4gICAgIHZhciBzZW9fc3RyaW5nID0gY291bnRyeV9pbmZvLmNvdW50cnkubmFtZSArIFwiIFRyYWRlbWFyayBSZWdpc3RyYXRpb24sIFJlZ2lzdGVyIFRyYWRlbWFyayBcIiArIGNvdW50cnlfaW5mby5jb3VudHJ5Lm5hbWVcblxuICAgICBjb25zb2xlLmxvZygnc2VvX3N0cmluZycsIHNlb19zdHJpbmcpXG4gICAgIHBhZ2VfaW5mby5zZXRUaXRsZShzZW9fc3RyaW5nKVxuICAgICBwYWdlX2luZm8uc2V0X2tleXdvcmRzKHNlb19zdHJpbmcpXG4gICAgIHBhZ2VfaW5mby5zZXRfZGVzY3JpcHRpb24oc2VvX3N0cmluZylcblxuXG4gICAgIHZhciBjb3VudHJ5X25hbWUgPSBjb3VudHJ5X2luZm8uY291bnRyeS5uYW1lXG4gICAgIHZhciBjb3VudHJ5X2NvZGUgPSBjb3VudHJ5X2luZm8uY291bnRyeS5jb2RlXG5cbiAgICAgJHNjb3BlLmNvdW50cnlfaW5mbyA9IGNvdW50cnlfaW5mb1xuICAgICAkc2NvcGUuc3R1ZHlfZGlzYWJsZWQgPSBjb3VudHJ5X2luZm8uc3R1ZHlfYXZhaWxhYmxlID8gXCJcIjogXCJkaXNhYmxlZFwiXG4gICAgICRzY29wZS50cmFkZW1hcmtfZGlzYWJsZWQgPSBjb3VudHJ5X2luZm8udHJhZGVtYXJrX2F2YWlsYWJsZSA/IFwiXCI6IFwiZGlzYWJsZWRcIlxuXG5cbiAgICAgaWYgKG9hcGlbY291bnRyeV9jb2RlXSA9PT0gdW5kZWZpbmVkKSB7XG5cbiAgICAgJHNjb3BlLmNvdW50cnkgPSBjb3VudHJ5X2NvZGVcbiAgICAgJHNjb3BlLnJlYWR5ID0gcmVnaXN0ZXJfdHJhZGVtYXJrLmlzSW1wbGVtZW50ZWQoJHNjb3BlLmNvdW50cnkpXG5cbiAgICAgaWYgKG9yZ2FuaXphdGlvbnNbJHNjb3BlLmNvdW50cnldKVxuICAgICAkc2NvcGUub3RoZXJzID0gb3JnYW5pemF0aW9uc1skc2NvcGUuY291bnRyeV1bMF1cbiAgICAgZWxzZSB7XG4gICAgIC8vIG5vdCBhbiBvcmdhbml6YXRpb24sIGNoZWNrIGlmIHRoZSBjb3VudHJ5IGhhcyByZWxhdGVkIG9yZ2FuaXphdGlvblxuICAgICBpZiAoYXJpcG9bJHNjb3BlLmNvdW50cnldICE9PSB1bmRlZmluZWQpXG4gICAgICRzY29wZS5vdGhlcnMgPSBvcmdhbml6YXRpb25zLkFSSVBPWzFdLnJlcGxhY2UoL19jb3VudHJ5Xy9nLCBjb3VudHJ5X25hbWUpXG4gICAgIGVsc2UgaWYgKGV1WyRzY29wZS5jb3VudHJ5XSAhPT0gdW5kZWZpbmVkKVxuICAgICAkc2NvcGUub3RoZXJzID0gb3JnYW5pemF0aW9ucy5FVVsxXS5yZXBsYWNlKC9fY291bnRyeV8vZywgY291bnRyeV9uYW1lKVxuICAgICB9XG5cbiAgICAgfSBlbHNlIHtcbiAgICAgJGxvY2F0aW9uLnBhdGgoJy90cmFkZW1hcmsvT0FQSScpXG4gICAgIH1cbiAgICAgfVxuICAgICBdKVxuXG4gICAgICovXG5cbiAgICAvLyBzdGFydGluZyBvZiByZWdpc3RyYXRpb24gcHJvY2Vzc1xuICAgIC5jb250cm9sbGVyKCdyZWdpc3RlcjEnLCBbJyRzY29wZScsICckaHR0cCcsICckbG9jYXRpb24nLCAnJHJvdXRlUGFyYW1zJywgJ3JlZ2lzdGVyX3RyYWRlbWFyaycsICdjb3VudHJ5X2luZm8nLFxuICAgICAgICBmdW5jdGlvbiAoJHNjb3BlLCAkaHR0cCwgJGxvY2F0aW9uLCAkcm91dGVQYXJhbXMsIHJlZ2lzdGVyX3RyYWRlbWFyaywgY291bnRyeV9pbmZvKSB7XG5cbiAgICAgICAgICAgICRzY29wZS5jb3VudHJ5ID0gJHJvdXRlUGFyYW1zLmNvdW50cnlcbiAgICAgICAgICAgICRzY29wZS5jb3VudHJ5X2luZm8gPSBjb3VudHJ5X2luZm9cblxuICAgICAgICAgICAgJHNjb3BlLnN0YXJ0X3JlZ2lzdHJhdGlvbiA9IGZ1bmN0aW9uIChjb3VudHJ5KSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ3N0YXJ0aW5nJylcbiAgICAgICAgICAgICAgICAvLyAgICAgICAgaWYgKGF1dGhTZXJ2aWNlLmlzU2lnbmVkSW4oKSkge1xuICAgICAgICAgICAgICAgIHJlZ2lzdGVyX3RyYWRlbWFyay5zZXRfaW5wdXRfZm9ybWF0KGNvdW50cnkpXG4gICAgICAgICAgICAgICAgcmVnaXN0ZXJfdHJhZGVtYXJrLmluaXRfaXRlbSgncmVnaXN0cmF0aW9uJykgLy8gbGluZSBidWZmZXIgZ290IGluaXRpYWxpemVkIG9ubHkgaGVyZSB0byBhbGxvdyAnYmFjaycgdG8gd29ya1xuICAgICAgICAgICAgICAgICRsb2NhdGlvbi5wYXRoKCcvcmVnaXN0ZXIyLycgKyBjb3VudHJ5KVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgXSlcblxuICAgIC8vIGdldCB0eXBlIG9mIHJlZ2lzdHJhdGlvbiwgd29yZCBtYXJrLCBwaG90b3MsIGNsYXNzZXNcbiAgICAvLyB0aGlzIGNvbnRyb2xsZXIgc2hhcmVkIGJ5IHJlZ2lzdGVyMi5odG1sIGFuZCBzdHVkeTIuaHRtbFxuXG4gICAgLmNvbnRyb2xsZXIoJ3JlZ2lzdGVyMicsIFsnJHNjb3BlJywgJyRodHRwJywgJyRsb2NhdGlvbicsICckcm9vdFNjb3BlJywgJyRyb3V0ZVBhcmFtcycsICckbW9kYWwnLCAnY291bnRyaWVzJywgJ3JlZ2lzdGVyX3RyYWRlbWFyaycsXG4gICAgICAgICdTVEFUSUNfSU5GTycsICdyZWdpc3RyeV9pbml0RGF0YScsICdjb3VudHJ5X2luZm8nLFxuICAgICAgICBmdW5jdGlvbiAoJHNjb3BlLCAkaHR0cCwgJGxvY2F0aW9uLCAkcm9vdFNjb3BlLCAkcm91dGVQYXJhbXMsICRtb2RhbCwgY291bnRyaWVzLCByZWdpc3Rlcl90cmFkZW1hcmssIFNUQVRJQ19JTkZPLFxuICAgICAgICAgICAgcmVnaXN0cnlfaW5pdERhdGEsIGNvdW50cnlfaW5mbykge1xuXG4gICAgICAgICAgICAkc2NvcGUuY291bnRyaWVzID0gY291bnRyaWVzXG5cbiAgICAgICAgICAgICRzY29wZS5jbGFzc2VzX3NlbGVjdGVkID0gW11cbiAgICAgICAgICAgICRzY29wZS5DTEFTU19IRUxQID0gU1RBVElDX0lORk8uY2xhc3NfaGVscFxuXG4gICAgICAgICAgICAvLyBnZW5lcmF0ZSBOSUNFIGNsYXNzZXMgYmxvY2sgZm9yIHNlbGVjdGlvblxuICAgICAgICAgICAgZm9yICh2YXIgeCA9IDE7IHggPD0gNDU7IHgrKykge1xuICAgICAgICAgICAgICAgIHZhciBjID0geC50b1N0cmluZygpXG4gICAgICAgICAgICAgICAgaWYgKGMubGVuZ3RoID09PSAxKVxuICAgICAgICAgICAgICAgICAgICBjID0gJzAnICsgY1xuICAgICAgICAgICAgICAgICRzY29wZS5jbGFzc2VzX3NlbGVjdGVkLnB1c2goe1xuICAgICAgICAgICAgICAgICAgICBjb2RlOiBjLFxuICAgICAgICAgICAgICAgICAgICBzZWxlY3RlZDogZmFsc2VcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAvLyBhc3NpZ24gdG8gY2xhc3Nlc19zZWxlY3RlZCBmb3IgZGF0YSBlbnRyeVxuICAgICAgICAgICAgdmFyIGNsYXNzZXMyYXJyYXkgPSBmdW5jdGlvbiAoY2xhc3Nlcykge1xuXG4gICAgICAgICAgICAgICAgZm9yICh2YXIgayA9IDA7IGsgPCAkc2NvcGUuY2xhc3Nlc19zZWxlY3RlZC5sZW5ndGg7IGsrKylcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmNsYXNzZXNfc2VsZWN0ZWRba10uc2VsZWN0ZWQgPSBmYWxzZVxuXG4gICAgICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBjbGFzc2VzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciBpbmR4ID0gcGFyc2VJbnQoY2xhc3Nlc1tpXS5uYW1lKVxuICAgICAgICAgICAgICAgICAgICAkc2NvcGUuY2xhc3Nlc19zZWxlY3RlZFtpbmR4IC0gMV0uc2VsZWN0ZWQgPSB0cnVlXG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAvKlxuICAgICAgICAgICAgIHZhciB0ZW1wID0gcmVnaXN0ZXJfdHJhZGVtYXJrLmdldCgpXG4gICAgICAgICAgICAgaWYgKHRlbXApXG4gICAgICAgICAgICAgY2xhc3NlczJhcnJheSh0ZW1wLmNsYXNzZXMpXG4gICAgICAgICAgICAgKi9cblxuICAgICAgICAgICAgJHNjb3BlLmNvdW50cnkgPSAkcm91dGVQYXJhbXMuY291bnRyeVxuICAgICAgICAgICAgJHNjb3BlLmNvdW50cnlfaW5mbyA9IGNvdW50cnlfaW5mb1xuXG4gICAgICAgICAgICAkc2NvcGUuaW5wdXRfZm9ybWF0ID0gcmVnaXN0cnlfaW5pdERhdGEuaW5wdXRfZm9ybWF0XG4gICAgICAgICAgICAkc2NvcGUucmVnID0gcmVnaXN0cnlfaW5pdERhdGEubGluZVxuXG4gICAgICAgICAgICBpZiAoJHNjb3BlLnJlZy50eXBlID09PSAnJykge1xuICAgICAgICAgICAgICAgICRzY29wZS5yZWcudHlwZSA9ICRzY29wZS5pbnB1dF9mb3JtYXQudHJhZGVfdHlwZVxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdyZWRvJylcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKCRzY29wZS5yZWcpXG4gICAgICAgICAgICAgICAgY2xhc3NlczJhcnJheSgkc2NvcGUucmVnLmNsYXNzZXMpXG5cbiAgICAgICAgICAgIHZhciBjaGtfcmVnID0gZnVuY3Rpb24gKHJlZywgY2IpIHtcblxuXG4gICAgICAgICAgICAgICAgdmFyIHJlcXVpcmVkX2xvZ28gPSBmdW5jdGlvbih0bV90eXBlKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciBsb2dvX3R5cGVzID0gWydsJywgJ3dsJywgJ3RtbCddXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBsb2dvX3R5cGVzLmluZGV4T2YodG1fdHlwZSkgIT09IC0xXG5cbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICB2YXIgZXJyID0gbnVsbFxuICAgICAgICAgICAgICAgIGlmIChyZWcudHlwZSAhPT0gJ2wnKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChyZWcud29yZG1hcmsgPT09ICcnKVxuICAgICAgICAgICAgICAgICAgICAgICAgZXJyID0gJ1dvcmRtYXJrIHNob3VsZCBub3QgYmUgYmxhbmsnXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGlmICghZXJyKSB7IC8vIGNoZWNrIGZvciBsb2dvXG5cbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ2NoZWNraW5nIHR5cGUgcmVnLnR5cGUnLCByZWcudHlwZSlcbiAgICAgICAgICAgICAgICAgICAgLy9pZiAocmVnLnR5cGUgIT09ICd3JykgfHwgcmVnLnR5cGUgLy8gaWYgbm90IHdvcmQgb25seSwgdGhlbiBjaGVjayBsb2dvXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXF1aXJlZF9sb2dvKHJlZy50eXBlKSlcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFyZWcubG9nb191cmwpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZXJyID0gJ1BsZWFzZSBwcm92aWRlIGEgbG9nbyBmaWxlJ1xuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBpZiAoIWVycikge1xuICAgICAgICAgICAgICAgICAgICBpZiAocmVnLmNsYXNzZXMubGVuZ3RoID09PSAwKVxuICAgICAgICAgICAgICAgICAgICAgICAgZXJyID0gJ1lvdSBzaG91bGQgY2hvb3NlIGF0IGxlYXN0IG9uZSBjbGFzcydcbiAgICAgICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnaGknKVxuICAgICAgICAgICAgICAgICAgICAgICAgLypcbiAgICAgICAgICAgICAgICAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHJlZy5jbGFzc2VzLmxlbmd0aDsgaSsrKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoIXJlZy5jbGFzc2VzW2ldLmRldGFpbHMpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICBlcnIgPSAnY2xhc3MgJyArIHJlZy5jbGFzc2VzW2ldLm5hbWUgKyAnIHNob3VsZCBoYXZlIGEgZGVzY3JpcHRpb24nXG4gICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWtcbiAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgfSovXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG5cblxuICAgICAgICAgICAgICAgIGlmIChlcnIpXG4gICAgICAgICAgICAgICAgICAgIGNiKG5ldyBFcnJvcihlcnIpKVxuICAgICAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgICAgICAgICAgY2IobnVsbClcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgJHNjb3BlLnN1Ym1pdCA9IGZ1bmN0aW9uIChyZWcpIHtcblxuICAgICAgICAgICAgICAgIGNoa19yZWcocmVnLCBmdW5jdGlvbiAoZXJyKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChlcnIpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5hbGVydHMgPSBbXVxuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmFsZXJ0cy5wdXNoKHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAnd2FybmluZycsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbXNnOiBlcnIubWVzc2FnZVxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZWcuY291bnRyeSA9ICRyb3V0ZVBhcmFtcy5jb3VudHJ5XG4gICAgICAgICAgICAgICAgICAgICAgICByZWcuYW1vdW50ID0gcmVnaXN0ZXJfdHJhZGVtYXJrLmdldEFtb3VudChyZWcuY291bnRyeSlcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlZ2lzdGVyX3RyYWRlbWFyay5zYXZlKHJlZylcbiAgICAgICAgICAgICAgICAgICAgICAgICRsb2NhdGlvbi5wYXRoKCcvcmVnaXN0ZXIzLycgKyByZWcuY291bnRyeSlcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICRzY29wZS5zdWJtaXRfc3R1ZHkgPSBmdW5jdGlvbiAocmVnKSB7XG5cbiAgICAgICAgICAgICAgICBjaGtfcmVnKHJlZywgZnVuY3Rpb24gKGVycikge1xuICAgICAgICAgICAgICAgICAgICBpZiAoZXJyKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUuYWxlcnRzID0gW11cbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5hbGVydHMucHVzaCh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ3dhcm5pbmcnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1zZzogZXJyLm1lc3NhZ2VcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVnLmNvdW50cnkgPSAkcm91dGVQYXJhbXMuY291bnRyeVxuICAgICAgICAgICAgICAgICAgICAgICAgcmVnLmFtb3VudCA9IHJlZ2lzdGVyX3RyYWRlbWFyay5nZXRBbW91bnQocmVnLmNvdW50cnkpXG4gICAgICAgICAgICAgICAgICAgICAgICByZWdpc3Rlcl90cmFkZW1hcmsuc2F2ZShyZWcpXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyAgICAgICAgICAgICRsb2NhdGlvbi5wYXRoKCcvc3R1ZHkzLycgKyByZWcuY291bnRyeSlcbiAgICAgICAgICAgICAgICAgICAgICAgICRsb2NhdGlvbi5wYXRoKCcvdHJhZGVtYXJrX2NvbnRhY3QvJyArICRzY29wZS5jb3VudHJ5KVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgJHNjb3BlLnN0dWR5X3R5cGVfY2hhbmdlID0gZnVuY3Rpb24gKHZhbHVlKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ3N0dWR5IHR5cGUnLCB2YWx1ZSlcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgJHNjb3BlLmdldEFtb3VudCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gcmVnaXN0ZXJfdHJhZGVtYXJrLmdldEFtb3VudCgkc2NvcGUuY291bnRyeSlcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgJHNjb3BlLnVwbG9hZF9yZXN1bHQgPSBmdW5jdGlvbiAoY29udGVudCkge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCd1cGxvYWRfcmVzdWx0JylcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhjb250ZW50KVxuICAgICAgICAgICAgICAgICRzY29wZS51cGxvYWRfc3RhdHVzID0gY29udGVudFxuICAgICAgICAgICAgICAgIC8vICAgIHJlZnJlc2hfdmlkZW8oJGh0dHAsICRzY29wZSwgJHJvdXRlUGFyYW1zLmNvZGUpXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC8vaHR0cDovL3N0YWNrb3ZlcmZsb3cuY29tL3F1ZXN0aW9ucy8xNzIxNjgwNi9hbmd1bGFyanMtdXBsb2FkaW5nLWFuLWltYWdlLXdpdGgtbmctdXBsb2FkXG5cbiAgICAgICAgICAgICRzY29wZS5maWxlX3NpemUgPSAxMDBcbiAgICAgICAgICAgIC8vICAgICAgJHNjb3BlLnVwbG9hZF9zdGF0dXNcblxuICAgICAgICAgICAgJHNjb3BlLnVwbG9hZEZpbGUgPSBmdW5jdGlvbiAoZmlsZXMpIHtcblxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHR5cGVvZiBmaWxlc1swXS50eXBlLCBmaWxlc1swXS50eXBlLCBmaWxlc1swXS50eXBlLnN1YnN0cmluZygwLCA1KSwgJ2ltYWdlJyA9PT0gZmlsZXNbMF0udHlwZS5zdWJzdHJpbmcoMCwgNSkpXG5cbiAgICAgICAgICAgICAgICBpZiAoJ2ltYWdlJyA9PT0gZmlsZXNbMF0udHlwZS5zdWJzdHJpbmcoMCwgNSkpIHtcblxuICAgICAgICAgICAgICAgICAgICB2YXIgZmQgPSBuZXcgRm9ybURhdGEoKTtcblxuICAgICAgICAgICAgICAgICAgICAvL1Rha2UgdGhlIGZpcnN0IHNlbGVjdGVkIGZpbGVcbiAgICAgICAgICAgICAgICAgICAgZmQuYXBwZW5kKFwibG9nb19maWxlXCIsIGZpbGVzWzBdKTtcblxuICAgICAgICAgICAgICAgICAgICAkc2NvcGUudXBsb2FkX3N0YXR1cyA9ICd1cGxvYWRpbmcuLi4nXG4gICAgICAgICAgICAgICAgICAgICRodHRwLnBvc3QoJy91cGxvYWRfZmlsZScsIGZkLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICB3aXRoQ3JlZGVudGlhbHM6IHRydWUsXG4gICAgICAgICAgICAgICAgICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJ0NvbnRlbnQtVHlwZSc6IHVuZGVmaW5lZFxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgIHRyYW5zZm9ybVJlcXVlc3Q6IGFuZ3VsYXIuaWRlbnRpdHlcbiAgICAgICAgICAgICAgICAgICAgfSkuc3VjY2VzcyhmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnJlZy5sb2dvX3VybCA9IGRhdGEudXJsXG4gICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUudXBsb2FkX3N0YXR1cyA9ICdVcGxvYWRlZCdcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdzdWNjZXNzJywgZGF0YSlcbiAgICAgICAgICAgICAgICAgICAgfSkuZXJyb3IoZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdmYWlsZWQnLCBkYXRhKVxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBhbGVydChmaWxlc1swXS5uYW1lICsgJyBpcyBub3QgYW4gaW1hZ2UgZmlsZScpXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAkc2NvcGUuY2xhc3NfY2hhbmdlID0gZnVuY3Rpb24gKHMpIHtcblxuICAgICAgICAgICAgICAgIGlmIChzLnNlbGVjdGVkKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgaWYgKCEkc2NvcGUucmVnLmNsYXNzZXMuc29tZShmdW5jdGlvbiAoZWxlbWVudCwgaW5kZXgsIGFycmF5KSB7IC8vIGlmIG5vdCB5ZXQgc2VsZWN0ZWRcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBlbGVtZW50Lm5hbWUgPT09IHMuY29kZVxuICAgICAgICAgICAgICAgICAgICB9KSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnJlZy5jbGFzc2VzLnB1c2goe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IHMuY29kZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZXRhaWxzOiAnJ1xuICAgICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgJHNjb3BlLnJlZy5jbGFzc2VzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoJHNjb3BlLnJlZy5jbGFzc2VzW2ldLm5hbWUgPT09IHMuY29kZSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUucmVnLmNsYXNzZXMuc3BsaWNlKGksIDEpXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLy8gb3BlbiBhIG1vZGFsIGRpYWxvZyB0byBzZWFyY2ggY2xhc3NlcyBmcm9tIE5JQ0Ugd2Vic2l0ZVxuXG4gICAgICAgICAgICAkc2NvcGUub3BlbiA9IGZ1bmN0aW9uICgpIHtcblxuXG4gICAgICAgICAgICAgICAgdmFyIG1vZGFsSW5zdGFuY2UgPSAkbW9kYWwub3Blbih7XG4gICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlVXJsOiAncGFydGlhbHMvc2VhcmNoX2NsYXNzX21vZGFsLmh0bWwnLFxuICAgICAgICAgICAgICAgICAgICBjb250cm9sbGVyOiAnTW9kYWxJbnN0YW5jZUN0cmwnLFxuICAgICAgICAgICAgICAgICAgICByZXNvbHZlOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpdGVtczogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAkc2NvcGUuaXRlbXM7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgIG1vZGFsSW5zdGFuY2UucmVzdWx0LnRoZW4oZnVuY3Rpb24gKHNlbGVjdGVkQ2xhc3Nlcykge1xuICAgICAgICAgICAgICAgICAgICBpZiAoc2VsZWN0ZWRDbGFzc2VzLmxlbmd0aCA+IDApIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ3NlbGVjdGVkIGNsYXNzZXMnICwgc2VsZWN0ZWRDbGFzc2VzKVxuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnJlZy5jbGFzc2VzID0gW11cbiAgICAgICAgICAgICAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgc2VsZWN0ZWRDbGFzc2VzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coc2VsZWN0ZWRDbGFzc2VzW2ldKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciB0ZW1wID0gc2VsZWN0ZWRDbGFzc2VzW2ldLmJhc2ljX2Rlc2MgKyAnICgnICsgc2VsZWN0ZWRDbGFzc2VzW2ldLmJhc2ljX251bWJlciArICcpJ1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5yZWcuY2xhc3Nlcy5wdXNoKHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogc2VsZWN0ZWRDbGFzc2VzW2ldLmNvZGUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRldGFpbHM6IHRlbXBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NlczJhcnJheSgkc2NvcGUucmVnLmNsYXNzZXMpXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgLy8gJHNjb3BlLnNlbGVjdGVkID0gc2VsZWN0ZWRJdGVtO1xuICAgICAgICAgICAgICAgIH0sIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgLy8gICAgICAgICAgJGxvZy5pbmZvKCdNb2RhbCBkaXNtaXNzZWQgYXQ6ICcgKyBuZXcgRGF0ZSgpKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgIC8vICAgIH1cbiAgICAgICAgfVxuICAgIF0pXG5cbiAgICAvLyBnZXQgUE9BIGFuZCBjb21tZXJjZSB1c2VcbiAgICAuY29udHJvbGxlcigncmVnaXN0ZXIzJywgWyckc2NvcGUnLCAnJGh0dHAnLCAnJGxvY2F0aW9uJywgJyRyb290U2NvcGUnLCAnJHJvdXRlUGFyYW1zJywgJyRtb2RhbCcsICdjb3VudHJpZXMnLFxuICAgICAgICAncmVnaXN0ZXJfdHJhZGVtYXJrJywgJ1BPQV9JTkZPJywgJ1NUQVRJQ19JTkZPJywgJ2F1dGhTZXJ2aWNlJyxcbiAgICAgICAgZnVuY3Rpb24gKCRzY29wZSwgJGh0dHAsICRsb2NhdGlvbiwgJHJvb3RTY29wZSwgJHJvdXRlUGFyYW1zLCAkbW9kYWwsIGNvdW50cmllcywgcmVnaXN0ZXJfdHJhZGVtYXJrLCBQT0FfSU5GTywgU1RBVElDX0lORk8sIGF1dGhTZXJ2aWNlKSB7XG5cbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFBPQV9JTkZPKVxuXG4gICAgICAgICAgICBjb25zb2xlLmxvZygncmVnMycpXG5cbiAgICAgICAgICAgICRzY29wZS5jb3VudHJpZXMgPSBjb3VudHJpZXNcblxuXG4gICAgICAgICAgICAkc2NvcGUuY291bnRyeSA9ICRyb3V0ZVBhcmFtcy5jb3VudHJ5XG5cbiAgICAgICAgICAgICRzY29wZS5yZWcgPSByZWdpc3Rlcl90cmFkZW1hcmsuZ2V0KClcblxuICAgICAgICAgICAgJHNjb3BlLlBPQV9JTkZPID0gUE9BX0lORk9bJHNjb3BlLmNvdW50cnldIC8vIGNvdWxkIGJlICd1bmRlZmluZWQnIGlmIHRoZSBjb3VudHJ5IGRvZXMgbm90IG5lZWQgUE9BXG4gICAgICAgICAgICAkc2NvcGUuUE9BX0hFTFAgPSBQT0FfSU5GTy5IRUxQWyRzY29wZS5jb3VudHJ5XVxuICAgICAgICAgICAgJHNjb3BlLkNMQUlNX0hFTFAgPSBTVEFUSUNfSU5GTy5wcmlvcml0eV9oZWxwXG4gICAgICAgICAgICAkc2NvcGUuQ09NTUVSQ0VfVVNFX0hFTFAgPSBTVEFUSUNfSU5GTy5jb21tZXJjZV91c2VfaGVscFxuICAgICAgICAgICAgJHNjb3BlLlBPQV9NVVNUID0gUE9BX0lORk8uUE9BX01VU1RcblxuICAgICAgICAgICAgY29uc29sZS5sb2coJHNjb3BlLnJlZylcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCd0JywgdHlwZW9mICRzY29wZS5yZWcuUE9BKVxuXG5cbiAgICAgICAgICAgICRzY29wZS5zdWJtaXQgPSBmdW5jdGlvbiAocmVnKSB7XG4gICAgICAgICAgICAgICAgcmVnLmNvdW50cnkgPSAkcm91dGVQYXJhbXMuY291bnRyeVxuICAgICAgICAgICAgICAgIHJlZ2lzdGVyX3RyYWRlbWFyay5zYXZlKHJlZylcblxuICAgICAgICAgICAgICAgICRsb2NhdGlvbi5wYXRoKCcvdHJhZGVtYXJrX2NvbnRhY3QvJyArICRzY29wZS5jb3VudHJ5KVxuXG4gICAgICAgICAgICAgICAgLypcbiAgICAgICAgICAgICAgICAgaWYgKGF1dGhTZXJ2aWNlLmlzU2lnbmVkSW4oKSlcbiAgICAgICAgICAgICAgICAgJGxvY2F0aW9uLnBhdGgoJy9yZWdpc3RlcjQvJyArICRzY29wZS5jb3VudHJ5KVxuICAgICAgICAgICAgICAgICBlbHNlXG4gICAgICAgICAgICAgICAgICRsb2NhdGlvbi5wYXRoKCcvcmVnaXN0ZXI1LycgKyAkc2NvcGUuY291bnRyeSlcbiAgICAgICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgJHNjb3BlLmJhY2sgPSBmdW5jdGlvbiAocmVnKSB7XG4gICAgICAgICAgICAgICAgcmVnaXN0ZXJfdHJhZGVtYXJrLnNhdmUocmVnKVxuICAgICAgICAgICAgICAgICRsb2NhdGlvbi5wYXRoKCcvcmVnaXN0ZXIyLycgKyByZWcuY291bnRyeSlcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgJHNjb3BlLm5ld1ZhbHVlID0gZnVuY3Rpb24gKHZhbHVlKSB7XG4gICAgICAgICAgICAgICAgaWYgKCF2YWx1ZSlcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnJlZy5jbGFpbV9wcmlvcml0eSA9IGZhbHNlXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICRzY29wZS5nZXRBbW91bnQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHJlZ2lzdGVyX3RyYWRlbWFyay5nZXRBbW91bnQoJHNjb3BlLmNvdW50cnkpXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgfVxuICAgIF0pXG5cbiAgICAvLyBnZXQgcHJvZmlsZVxuXG4gICAgLmNvbnRyb2xsZXIoJ3JlZ2lzdGVyNCcsIFsnJHNjb3BlJywgJyRodHRwJywgJyRsb2NhdGlvbicsICckcm9vdFNjb3BlJywgJyRyb3V0ZVBhcmFtcycsICckbW9kYWwnLCAnY291bnRyaWVzJywgJ3JlZ2lzdGVyX3RyYWRlbWFyaycsICdEQlN0b3JlJyxcbiAgICAgICAgZnVuY3Rpb24gKCRzY29wZSwgJGh0dHAsICRsb2NhdGlvbiwgJHJvb3RTY29wZSwgJHJvdXRlUGFyYW1zLCAkbW9kYWwsIGNvdW50cmllcywgcmVnaXN0ZXJfdHJhZGVtYXJrLCBEQlN0b3JlKSB7XG5cbiAgICAgICAgICAgICRzY29wZS5hbGVydHMgPSBbXTtcblxuICAgICAgICAgICAgJGh0dHAuZ2V0KCcvYXBpL3Byb2ZpbGVzLycpLnN1Y2Nlc3MoZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgICAgICAgICAkc2NvcGUucHJvZmlsZXMgPSBkYXRhXG4gICAgICAgICAgICB9KVxuXG4gICAgICAgICAgICAkc2NvcGUuY2xvc2VBbGVydCA9IGZ1bmN0aW9uIChpbmRleCkge1xuICAgICAgICAgICAgICAgICRzY29wZS5hbGVydHMuc3BsaWNlKGluZGV4LCAxKTtcbiAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgICRzY29wZS5jb3VudHJ5ID0gJHJvdXRlUGFyYW1zLmNvdW50cnlcbiAgICAgICAgICAgICRzY29wZS5yZWcgPSByZWdpc3Rlcl90cmFkZW1hcmsuZ2V0KClcblxuICAgICAgICAgICAgJHNjb3BlLm5ld19wcm9maWxlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCRsb2NhdGlvbilcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygkbG9jYXRpb24ucGF0aCgpKVxuICAgICAgICAgICAgICAgIHZhciB1dWlkID0gREJTdG9yZS5hZGQoJGxvY2F0aW9uLnBhdGgoKSlcbiAgICAgICAgICAgICAgICAkbG9jYXRpb24ucGF0aCgnL3Byb2ZpbGUvbmV3LycgKyB1dWlkKVxuICAgICAgICAgICAgfVxuXG5cbiAgICAgICAgICAgICRzY29wZS5zdWJtaXQgPSBmdW5jdGlvbiAocmVnKSB7XG4gICAgICAgICAgICAgICAgcmVnLmNvdW50cnkgPSAkcm91dGVQYXJhbXMuY291bnRyeVxuXG4gICAgICAgICAgICAgICAgcmVnaXN0ZXJfdHJhZGVtYXJrLnNhdmUocmVnKVxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlZylcbiAgICAgICAgICAgICAgICAkc2NvcGUucHJvZmlsZV9uYW1lID0gcmVnLnByb2ZpbGVfaWRcbiAgICAgICAgICAgICAgICAkbG9jYXRpb24ucGF0aCgnL3JlZ2lzdGVyNS8nICsgJHNjb3BlLmNvdW50cnkpXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICRzY29wZS5iYWNrID0gZnVuY3Rpb24gKHJlZykge1xuICAgICAgICAgICAgICAgIHJlZ2lzdGVyX3RyYWRlbWFyay5zYXZlKHJlZylcbiAgICAgICAgICAgICAgICAkbG9jYXRpb24ucGF0aCgnL3JlZ2lzdGVyMy8nICsgcmVnLmNvdW50cnkpXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgfVxuICAgIF0pXG5cbiAgICAvLyBmaW5hbCBjb25maXJtYXRpb25cblxuICAgIC5jb250cm9sbGVyKCdyZWdpc3RlcjUnLCBbJyRzY29wZScsICckaHR0cCcsICckbG9jYXRpb24nLCAnJHJvb3RTY29wZScsICckcm91dGVQYXJhbXMnLCAncmVnaXN0ZXJfdHJhZGVtYXJrJywgJ0NhcnQnLCAnQVVUSF9FVkVOVFMnLCAncmVnaXN0cnlfaW5pdERhdGEnLCAnYXV0aFNlcnZpY2UnLFxuICAgICAgICBmdW5jdGlvbiAoJHNjb3BlLCAkaHR0cCwgJGxvY2F0aW9uLCAkcm9vdFNjb3BlLCAkcm91dGVQYXJhbXMsIHJlZ2lzdGVyX3RyYWRlbWFyaywgQ2FydCwgQVVUSF9FVkVOVFMsIHJlZ2lzdHJ5X2luaXREYXRhLCBhdXRoU2VydmljZSkge1xuXG4gICAgICAgICAgICBjb25zb2xlLmxvZygncmVnNSBpbml0IGRhdGEnLCByZWdpc3RyeV9pbml0RGF0YSlcblxuICAgICAgICAgICAgJHNjb3BlLmNvdW50cnkgPSAkcm91dGVQYXJhbXMuY291bnRyeVxuXG4gICAgICAgICAgICAkc2NvcGUucmVnID0gcmVnaXN0cnlfaW5pdERhdGEubGluZVxuICAgICAgICAgICAgaWYgKHJlZ2lzdHJ5X2luaXREYXRhLnByb2ZpbGUpXG4gICAgICAgICAgICAgICAgJHNjb3BlLnByb2ZpbGUgPSByZWdpc3RyeV9pbml0RGF0YS5wcm9maWxlXG4gICAgICAgICAgICBlbHNlXG4gICAgICAgICAgICAgICAgJHNjb3BlLnByb2ZpbGUgPSAkc2NvcGUucmVnLnByb2ZpbGUgICAgIC8vIG5vIHByb2ZpbGVfaWQsIHVzZSBlbWJlZGRlZCBwcm9maWxlXG5cbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdyZWc1JywgJHNjb3BlLnJlZylcblxuXG4gICAgICAgICAgICAkc2NvcGUuYmFjayA9IGZ1bmN0aW9uIChyZWcpIHtcbiAgICAgICAgICAgICAgICByZWcuY291bnRyeSA9ICRyb3V0ZVBhcmFtcy5jb3VudHJ5XG4gICAgICAgICAgICAgICAgaWYgKHJlZy5wcm9maWxlX2lkKVxuICAgICAgICAgICAgICAgICAgICAkbG9jYXRpb24ucGF0aCgnL3RyYWRlbWFya19jb250YWN0LycgKyAkcm91dGVQYXJhbXMuY291bnRyeSlcbiAgICAgICAgICAgICAgICBlbHNlXG4gICAgICAgICAgICAgICAgICAgICRsb2NhdGlvbi5wYXRoKCcvdHJhZGVtYXJrX2FkZHJlc3MvJyArICRyb3V0ZVBhcmFtcy5jb3VudHJ5KVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAkc2NvcGUuc3VibWl0ID0gZnVuY3Rpb24gKHJlZykge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdyZWcnLCByZWcpXG4gICAgICAgICAgICAgICAgcmVnaXN0ZXJfdHJhZGVtYXJrLnNhdmUocmVnKVxuICAgICAgICAgICAgICAgIENhcnQucG9zdF90b19zZXJ2ZXIocmVnaXN0ZXJfdHJhZGVtYXJrLmdldCgpKS50aGVuKGZ1bmN0aW9uIChzdGF0dXMpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ2NhcnQgcG9zdCB0byBzZXJ2ZXIgc3RhdHVzJylcbiAgICAgICAgICAgICAgICAgICAgJHJvb3RTY29wZS4kYnJvYWRjYXN0KEFVVEhfRVZFTlRTLmNhcnRVcGRhdGVkKTtcbiAgICAgICAgICAgICAgICAgICAgQ2FydC5nZXRFeGlzdGluZ0VtYWlscygpLnRoZW4oZnVuY3Rpb24gKGVtYWlscykge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGVtYWlscy5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJGxvY2F0aW9uLnBhdGgoJy9iZWZvcmVfY2hlY2tfb3V0LycgKyBDYXJ0LmdldFJlbW90ZUNhcnQoKS5faWQpXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2VcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkbG9jYXRpb24ucGF0aCgnL3Nob3BwaW5nX2NhcnQnKVxuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgfVxuICAgIF0pXG5cbiAgICAuY29udHJvbGxlcigndHJhZGVtYXJrX2NvbnRhY3QnLCBbJyRzY29wZScsICckaHR0cCcsICckbG9jYXRpb24nLCAnJHJvb3RTY29wZScsICckcm91dGVQYXJhbXMnLCAnJG1vZGFsJywgJ2NvdW50cmllcycsICdyZWdpc3Rlcl90cmFkZW1hcmsnLCAncmVnaXN0cnlfaW5pdERhdGEnLFxuICAgICAgICBmdW5jdGlvbiAoJHNjb3BlLCAkaHR0cCwgJGxvY2F0aW9uLCAkcm9vdFNjb3BlLCAkcm91dGVQYXJhbXMsICRtb2RhbCwgY291bnRyaWVzLCByZWdpc3Rlcl90cmFkZW1hcmssIHJlZ2lzdHJ5X2luaXREYXRhKSB7XG5cbiAgICAgICAgICAgICRzY29wZS5yZWcgPSByZWdpc3RyeV9pbml0RGF0YS5saW5lXG5cbiAgICAgICAgICAgICRzY29wZS5jb3VudHJ5ID0gJHJvdXRlUGFyYW1zLmNvdW50cnlcbiAgICAgICAgICAgICRzY29wZS5yZWcgPSByZWdpc3Rlcl90cmFkZW1hcmsuZ2V0KClcblxuICAgICAgICAgICAgY29uc29sZS5sb2coJ2luaXQnLCByZWdpc3RyeV9pbml0RGF0YSlcbiAgICAgICAgICAgICRzY29wZS5wcm9maWxlcyA9IHJlZ2lzdHJ5X2luaXREYXRhLnByb2ZpbGVzXG5cblxuICAgICAgICAgICAgJHNjb3BlLm5leHQgPSBmdW5jdGlvbiAocmVnKSB7XG5cbiAgICAgICAgICAgICAgICB2YXIgZXJyX21zZ1xuXG4gICAgICAgICAgICAgICAgaWYgKCFyZWcucHJvZmlsZS5wcm9maWxlX25hbWUpXG4gICAgICAgICAgICAgICAgICAgIGVycl9tc2cgPSAnUGxlYXNlIHByb3ZpZGUgYSBuYW1lIGZvciB0aGlzIHByb2ZpbGUnXG4gICAgICAgICAgICAgICAgaWYgKCFlcnJfbXNnICYmICFyZWcucHJvZmlsZS5jb250YWN0KVxuICAgICAgICAgICAgICAgICAgICBlcnJfbXNnID0gJ1BsZWFzZSBwcm92aWRlIGEgY29udGFjdCBwZXJzb24nXG4gICAgICAgICAgICAgICAgaWYgKCFlcnJfbXNnICYmICFyZWcucHJvZmlsZS5lbWFpbClcbiAgICAgICAgICAgICAgICAgICAgZXJyX21zZyA9ICdQbGVhc2UgcHJvdmlkZSBhIHZhbGlkIGVtYWlsIGFkZHJlc3Mgc28gd2UgY2FuIGNvbnRhY3QgeW91J1xuXG4gICAgICAgICAgICAgICAgaWYgKGVycl9tc2cpXG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5hbGVydHMgPSBbXG4gICAgICAgICAgICAgICAgICAgICAgICB7IHR5cGU6ICd3YXJuaW5nJywgbXNnOiBlcnJfbXNnIH1cbiAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBkZWxldGUgcmVnLnByb2ZpbGVfaWRcbiAgICAgICAgICAgICAgICAgICAgcmVnaXN0ZXJfdHJhZGVtYXJrLnNhdmUocmVnKVxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygncmVnMCcsIHJlZylcbiAgICAgICAgICAgICAgICAgICAgJGxvY2F0aW9uLnBhdGgoJy90cmFkZW1hcmtfYWRkcmVzcy8nICsgJHJvdXRlUGFyYW1zLmNvdW50cnkpXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAkc2NvcGUuYmFjayA9IGZ1bmN0aW9uIChyZWcpIHtcbiAgICAgICAgICAgICAgICByZWdpc3Rlcl90cmFkZW1hcmsuc2F2ZShyZWcpXG4gICAgICAgICAgICAgICAgY29uc29sZS50YWJsZShyZWcpXG4gICAgICAgICAgICAgICAgdmFyIGJhY2tfdG8gPSByZWcuc2VydmljZSA9PT0gJ3N0dWR5JyA/ICcvc3R1ZHkyLycgOiAnL3JlZ2lzdGVyMy8nXG4gICAgICAgICAgICAgICAgJGxvY2F0aW9uLnBhdGgoYmFja190byArICRyb3V0ZVBhcmFtcy5jb3VudHJ5KVxuXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICRzY29wZS51c2VfZXhpc3RpbmdfcHJvZmlsZSA9IGZ1bmN0aW9uIChwcm9maWxlX2lkLCByZWcpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygndXNlX2V4aXN0aW5nJywgcmVnKVxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdwcm9maWxlX2lkJywgcHJvZmlsZV9pZClcbiAgICAgICAgICAgICAgICAkc2NvcGUucmVnLnByb2ZpbGVfaWQgPSBwcm9maWxlX2lkXG4gICAgICAgICAgICAgICAgcmVnaXN0ZXJfdHJhZGVtYXJrLnNhdmUocmVnKVxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdwcm9maWxlIC1pZCBuZXcnLCByZWdpc3Rlcl90cmFkZW1hcmsuZ2V0KCkpXG4gICAgICAgICAgICAgICAgdmFyIG5leHRfdG8gPSAkc2NvcGUucmVnLnNlcnZpY2UgPT09ICdzdHVkeScgPyAnL3N0dWR5My8nIDogJy9yZWdpc3RlcjUvJ1xuICAgICAgICAgICAgICAgICRsb2NhdGlvbi5wYXRoKG5leHRfdG8gKyAkcm91dGVQYXJhbXMuY291bnRyeSlcbiAgICAgICAgICAgIH1cblxuICAgICAgICB9XG4gICAgXSlcblxuICAgIC5jb250cm9sbGVyKCd0cmFkZW1hcmtfYWRkcmVzcycsIFsnJHNjb3BlJywgJyRodHRwJywgJyRsb2NhdGlvbicsICckcm91dGUnLCAnJHdpbmRvdycsICdyZWdpc3RyeV9pbml0RGF0YScsXG4gICAgICAgIGZ1bmN0aW9uICgkc2NvcGUsICRodHRwLCAkbG9jYXRpb24sICRyb3V0ZSwgJHdpbmRvdywgcmVnaXN0cnlfaW5pdERhdGEpIHtcblxuICAgICAgICAgICAgJHNjb3BlLnByb2ZpbGUgPSByZWdpc3RyeV9pbml0RGF0YS5wcm9maWxlXG4gICAgICAgICAgICAkc2NvcGUuc2lkZV9wYW5lbF9hY2NvdW50ID0gcmVnaXN0cnlfaW5pdERhdGEuc2lkZV9wYW5lbF9hY2NvdW50XG5cbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdpbml0JywgcmVnaXN0cnlfaW5pdERhdGEpXG4gICAgICAgICAgICBjb25zb2xlLmxvZygnaW5pdDInLCBKU09OLnN0cmluZ2lmeShyZWdpc3RyeV9pbml0RGF0YSkpXG5cbiAgICAgICAgICAgICRzY29wZS5jb3VudHJpZXMgPSBjb3VudHJpZXMuZ2V0X2NvdW50cmllcygpXG4gICAgICAgICAgICBpZiAocmVnaXN0cnlfaW5pdERhdGEubmV4dF9wcm9tcHQpXG4gICAgICAgICAgICAgICAgJHNjb3BlLm5leHRfcHJvbXB0ID0gcmVnaXN0cnlfaW5pdERhdGEubmV4dF9wcm9tcHRcbiAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgICAgICAkc2NvcGUubmV4dF9wcm9tcHQgPSAnTmV4dCdcblxuXG4gICAgICAgICAgICBpZiAoJHdpbmRvdy5zZXNzaW9uU3RvcmFnZS5uZXdfY291bnRyeSkge1xuICAgICAgICAgICAgICAgICRzY29wZS5wcm9maWxlLmNvdW50cnkgPSAkd2luZG93LnNlc3Npb25TdG9yYWdlLm5ld19jb3VudHJ5XG4gICAgICAgICAgICAgICAgZGVsZXRlICR3aW5kb3cuc2Vzc2lvblN0b3JhZ2UubmV3X2NvdW50cnlcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKCEkc2NvcGUucHJvZmlsZS5jb3VudHJ5KSB7XG4gICAgICAgICAgICAgICAgLy8gbGV0J3MgZGV0ZXJtaW5lXG4gICAgICAgICAgICAgICAgLy8gICAgICAgICRodHRwLmdldCgnL2FwaTIvZ2V0X2dlbz90ZXN0X2lwPTE3NC41NC4xNjUuMjA2Jykuc3VjY2VzcyhmdW5jdGlvbiAoZ2VvKSB7XG4gICAgICAgICAgICAgICAgLy8gICAgICAgICRodHRwLmdldCgnL2FwaTIvZ2V0X2dlbz90ZXN0X2lwPTExMi4yMTAuMTUuMTA3Jykuc3VjY2VzcyhmdW5jdGlvbiAoZ2VvKSB7XG4gICAgICAgICAgICAgICAgJGh0dHAuZ2V0KCcvYXBpMi9nZXRfZ2VvJykuc3VjY2VzcyhmdW5jdGlvbiAoZ2VvKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdnZW8nLCBnZW8pXG4gICAgICAgICAgICAgICAgICAgIGlmIChnZW8uY291bnRyeSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnByb2ZpbGUuY291bnRyeSA9IGdlby5jb3VudHJ5XG4gICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUucHJvZmlsZS5zdGF0ZSA9IGdlby5yZWdpb25cbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5wcm9maWxlLmNpdHkgPSBnZW8uY2l0eVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgJHNjb3BlLml0ZW1zID0gW1xuICAgICAgICAgICAgICAgIHsgbmFtZTogJ0NhbmFkYScsIGNvZGU6ICdDQScgfSxcbiAgICAgICAgICAgICAgICB7IG5hbWU6ICdDaGluYScsIGNvZGU6ICdDTicgfSxcbiAgICAgICAgICAgICAgICB7IG5hbWU6ICdIb25nIEtvbmcnLCBjb2RlOiAnSEsnIH0sXG4gICAgICAgICAgICAgICAgeyBuYW1lOiAnUGhpbGlwcGluZXMnLCBjb2RlOiAnUEgnIH0sXG4gICAgICAgICAgICAgICAgeyBuYW1lOiAnU2luZ2Fwb3JlJywgY29kZTogJ1NHJyB9LFxuICAgICAgICAgICAgICAgIHsgbmFtZTogJ1RoYWlsYW5kJywgY29kZTogJ1RIJyB9LFxuICAgICAgICAgICAgICAgIHsgbmFtZTogJ1VuaXRlZCBTdGF0ZXMnLCBjb2RlOiAnVVMnIH1cbiAgICAgICAgICAgIF07XG5cbiAgICAgICAgICAgICRzY29wZS5zZXRDb3VudHJ5ID0gZnVuY3Rpb24gKGNvZGUpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnZGQnLCBjb2RlKVxuICAgICAgICAgICAgICAgIGlmIChjb2RlICE9PSAnQUxMJylcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnByb2ZpbGUuY291bnRyeSA9IGNvZGVcbiAgICAgICAgICAgICAgICBlbHNlXG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5zaG93QyA9IHRydWVcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgJHNjb3BlLnN0YXR1cyA9IHtcbiAgICAgICAgICAgICAgICBpc29wZW46IGZhbHNlXG4gICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICAkc2NvcGUudG9nZ2xlZCA9IGZ1bmN0aW9uIChvcGVuKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ0Ryb3Bkb3duIGlzIG5vdzogJywgb3Blbik7XG4gICAgICAgICAgICB9O1xuXG5cbiAgICAgICAgICAgICRzY29wZS4kd2F0Y2goJ3Byb2ZpbGUuY291bnRyeScsIGZ1bmN0aW9uIChuZXdWYWwsIG9sZFZhbCkge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdjaGFuZ2VkJywgbmV3VmFsLCBvbGRWYWwpXG5cbiAgICAgICAgICAgICAgICBpZiAobmV3VmFsID09PSBvbGRWYWwpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ25vIGNoYW5nZScpXG4gICAgICAgICAgICAgICAgICAgIHJldHVyblxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAvLyAgcmVnaXN0ZXJfdHJhZGVtYXJrLnNhdmVfcHJvZmlsZSgkc2NvcGUucHJvZmlsZSlcbiAgICAgICAgICAgICAgICAkd2luZG93LnNlc3Npb25TdG9yYWdlLm5ld19jb3VudHJ5ID0gbmV3VmFsXG4gICAgICAgICAgICAgICAgJHJvdXRlLnJlbG9hZCgpXG5cbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAkc2NvcGUubmV4dCA9IGZ1bmN0aW9uIChwcm9maWxlKSB7XG5cbiAgICAgICAgICAgICAgICB2YXIgZXJyX21zZ1xuXG4gICAgICAgICAgICAgICAgaWYgKCFwcm9maWxlLmFkZHJlc3MpXG4gICAgICAgICAgICAgICAgICAgIGVycl9tc2cgPSAnQWRkcmVzcyBsaW5lIHNob3VsZCBub3QgYmUgYmxhbmsnXG4gICAgICAgICAgICAgICAgaWYgKCFlcnJfbXNnICYmICFwcm9maWxlLmNvdW50cnkpXG4gICAgICAgICAgICAgICAgICAgIGVycl9tc2cgPSAnUGxlYXNlIHNlbGVjdCBhIGNvdW50cnkgYnkgY2xpY2tpbmcgdGhlIGxpbmsgYXQgbG93ZXIgcmlnaHQgb2YgdGhpcyBmb3JtJ1xuICAgICAgICAgICAgICAgIGlmICghZXJyX21zZyAmJiAhcHJvZmlsZS5jaXR5KSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciBjbiA9IHByb2ZpbGUuY291bnRyeVxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhjbiwgY24gIT09ICdISycsIGNuICE9PSAnU0cnLCBjbiAhPT0gJ0hLJyB8fCBjbiAhPT0gJ1NHJylcbiAgICAgICAgICAgICAgICAgICAgaWYgKCEocHJvZmlsZS5jb3VudHJ5ID09PSAnSEsnIHx8IHByb2ZpbGUuY291bnRyeSA9PT0gJ1NHJykpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGVycl9tc2cgPSAnQ2l0eSBzaG91bGQgbm90IGJlIGJsYW5rJ1xuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgaWYgKCFlcnJfbXNnICYmICFwcm9maWxlLnN0YXRlKVxuICAgICAgICAgICAgICAgICAgICBlcnJfbXNnID0gJ1N0YXRlIHNob3VsZCBub3QgYmUgYmxhbmsnXG5cbiAgICAgICAgICAgICAgICBpZiAoIWVycl9tc2cgJiYgIXByb2ZpbGUucGhvbmUpXG4gICAgICAgICAgICAgICAgICAgIGVycl9tc2cgPSAnUGhvbmUgc2hvdWxkIG5vdCBiZSBibGFuaydcblxuICAgICAgICAgICAgICAgIGlmIChlcnJfbXNnKVxuICAgICAgICAgICAgICAgICAgICAkc2NvcGUuYWxlcnRzID0gW1xuICAgICAgICAgICAgICAgICAgICAgICAgeyB0eXBlOiAnd2FybmluZycsIG1zZzogZXJyX21zZyB9XG4gICAgICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgICBlbHNlIHtcblxuICAgICAgICAgICAgICAgICAgICBpZiAocmVnaXN0cnlfaW5pdERhdGEubmV4dF9wcm9jKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZWdpc3RyeV9pbml0RGF0YS5uZXh0X3Byb2MocHJvZmlsZSlcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlXG4gICAgICAgICAgICAgICAgICAgICAgICAkbG9jYXRpb24ucGF0aChyZWdpc3RyeV9pbml0RGF0YS5uZXh0X3BhdGgpXG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICRzY29wZS5iYWNrID0gZnVuY3Rpb24gKHByb2ZpbGUpIHtcbiAgICAgICAgICAgICAgICAkbG9jYXRpb24ucGF0aChyZWdpc3RyeV9pbml0RGF0YS5iYWNrX3BhdGgpXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgfVxuICAgIF0pXG5cbiIsIi8vIEB0cy1jaGVja1xuXCJ1c2Ugc3RyaWN0XCI7XG5cbi8qZ2xvYmFsIGFuZ3VsYXIgKi9cblxudmFyIGNvdW50cmllcyA9IHJlcXVpcmUoJ2NvdW50cmllcycpXG52YXIgdXRpbGl0aWVzID0gcmVxdWlyZSgnLi4vbW9kdWxlcy91dGlsaXRpZXMuanMnKVxuXG5mdW5jdGlvbiAgZ2V0X2NvdW50cmllc19ieV9jb250aW5lbnQgKGNvbnRfY29kZSkge1xuICB2YXIgdGVtcCA9IHV0aWxpdGllcy5zdXBwb3J0ZWRDb3VudHJpZXMoKVxuXG4gIHJldHVybiBjb3VudHJpZXMuZ2V0X2NvdW50cmllc19ieV9jb250bmVudChjb250X2NvZGUpLmZpbHRlcihmdW5jdGlvbiAodmFsdWUpIHtcbiAgICByZXR1cm4gdGVtcFt2YWx1ZS5jb2RlXSAhPT0gdW5kZWZpbmVkXG4gIH0pXG59XG5cblxuZnVuY3Rpb24gYXBwZW5kX29uZV93b3JkX2NvdW50cnlfbmFtZSAobHN0KSB7XG4gIHZhciBybHN0ID0gW11cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBsc3QubGVuZ3RoOyBpICsrKSB7XG4gICAgbHN0W2ldLnNpbXBsZV9uYW1lID0gbHN0W2ldLm5hbWUucmVwbGFjZSgvIC9nLCAnXycpLnRvTG93ZXJDYXNlKClcbiAgICBybHN0LnB1c2gobHN0W2ldKVxuICB9XG4gIHJldHVybiBybHN0XG59XG5cbmFuZ3VsYXIubW9kdWxlKCd0a3MubWVudScsIFtdKS5mYWN0b3J5KFwiVE1fTUVOVVwiLCBbZnVuY3Rpb24gKCkge1xuXG4gIHJldHVybiB7XG5cbiAgICBtYWluX21lbnU6IGNvdW50cmllcy5nZXRfY291bnRyaWVzKFsnRVUnLCAnVVMnLCAnQ0EnLCAnUlUnLCAnQ0gnLCAnTk8nLFxuICAgICAgJ1NHJywgJ0hLJywgJ0NOJywgJ1RXJywgJ1RIJywgJ1BIJywgJ0tSJywgJ0pQJywgJ1ZOJywgJ0lOJyxcbiAgICAgICdBUicsICdNWCcsICdCUicsICdBVScsICdBWicsICdCSCcsICdCWScsICdCWicsICdCTycsICdDTyddKSxcblxuICAgIG1ham9yX2NvdW50cmllczogYXBwZW5kX29uZV93b3JkX2NvdW50cnlfbmFtZShjb3VudHJpZXMuZ2V0X2NvdW50cmllcyhbJ1VTJywgJ0VVJywgJ0NBJywgJ0NOJywgJ1NHJywgJ0hLJywgJ1RXJywgJ1BIJ10pKSxcbiAgICBhc2lhOiBnZXRfY291bnRyaWVzX2J5X2NvbnRpbmVudCgnQVMnKSxcbiAgICBldXJvcGU6IGdldF9jb3VudHJpZXNfYnlfY29udGluZW50KCdFVScpLFxuICAgIG5fYW1lcmljYTogZ2V0X2NvdW50cmllc19ieV9jb250aW5lbnQoJ05BJyksXG4gICAgc19hbWVyaWNhOiBnZXRfY291bnRyaWVzX2J5X2NvbnRpbmVudCgnU0EnKSxcbiAgICBjX2FtZXJpY2E6IGdldF9jb3VudHJpZXNfYnlfY29udGluZW50KCdDQScpLFxuICAgIGFmcmljYTogZ2V0X2NvdW50cmllc19ieV9jb250aW5lbnQoJ0FGJyksXG4vLyAgICBtaWRkbGVfZWFzdDogY291bnRyaWVzLmdldF9jb3VudHJpZXMoWydCSCcsICdDQSddKSxcbiAgICBtaWRkbGVfZWFzdDpnZXRfY291bnRyaWVzX2J5X2NvbnRpbmVudCgnTUUnKSxcbiAgICBvY2VhbmlhOiBnZXRfY291bnRyaWVzX2J5X2NvbnRpbmVudCgnT0MnKSxcblxuICAgIHJlc3RfbWVudTogW1xuICAgICAge25hbWU6ICdBZmdoYW5pc3RhbicsIGNvZGU6ICdBRid9LFxuICAgICAge25hbWU6ICfDhWxhbmQgSXNsYW5kcycsIGNvZGU6ICdBWCd9LFxuICAgICAge25hbWU6ICdBbGJhbmlhJywgY29kZTogJ0FMJ31cbiAgICBdXG5cbiAgfVxuXG5cbn1dKVxuIiwiXCJ1c2Ugc3RyaWN0XCI7XG4vKmdsb2JhbCBhbmd1bGFyLCBjb25zb2xlICovXG5cbmFuZ3VsYXIubW9kdWxlKCd0a3MubW9uaXRvci5jb250cm9sbGVycycsIFtdKVxuXG4gICAgLy8gc3RhcnRpbmcgb2YgbW9uaXRvciBwcm9jZXNzXG5cbiAgICAuY29udHJvbGxlcignbW9uaXRvcjEnLCAoJHNjb3BlLCAkaHR0cCwgJGxvY2F0aW9uLCAkcm91dGVQYXJhbXMpID0+IHtcblxuICAgICAgICAkc2NvcGUuY291bnRyeSA9ICRyb3V0ZVBhcmFtcy5jb3VudHJ5XG4gICAgICAgICRzY29wZS5zdGFydF9tb25pdG9yID0gY291bnRyeSA9PiB7XG4gICAgICAgICAgICAkbG9jYXRpb24ucGF0aCgnL21vbml0b3IyLycgKyBjb3VudHJ5KVxuICAgICAgICB9XG5cbiAgICB9KVxuXG4gICAgLmNvbnRyb2xsZXIoJ21vbml0b3IyJywgKCRzY29wZSwgJGh0dHAsICRsb2NhdGlvbiwgJHJvdXRlUGFyYW1zKSA9PiB7XG5cbiAgICAgICAgJHNjb3BlLnJlZyA9XG5cbiAgICAgICAge1xuICAgICAgICAgICAgdHlwZTogJ3cnLFxuICAgICAgICAgICAgY2xhc3NlczogW3tcIm5hbWVcIjogXCI0MlwiLCBcImRldGFpbHNcIjogXCJjbGFzczQyXCJ9LCB7XCJuYW1lXCI6IFwiMDFcIiwgXCJkZXRhaWxzXCI6IFwiY2xhc3MwMVwifSwge1xuICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcIjE1XCIsXG4gICAgICAgICAgICAgICAgXCJkZXRhaWxzXCI6IFwiY2xhc3MxNVwiXG4gICAgICAgICAgICB9XVxuICAgICAgICB9XG5cbiAgICAgICAgJHNjb3BlLmNvdW50cnkgPSAkcm91dGVQYXJhbXMuY291bnRyeVxuICAgICAgICAkc2NvcGUuc3RhcnRfbW9uaXRvciA9IGNvdW50cnkgPT4ge1xuICAgICAgICAgICAgJGxvY2F0aW9uLnBhdGgoJy9tb25pdG9yMi8nICsgY291bnRyeSlcbiAgICAgICAgfVxuXG4gICAgfSlcbiIsIi8vIEB0cy1jaGVja1xuXG5cInVzZSBzdHJpY3RcIjtcblxudmFyIHRyYW5zYWN0aW9uX3Byb2NzID0gcmVxdWlyZSgnLi93ZWJfc2VydmljZXNfdHJhbnNhY3Rpb25fcHJvY3MuanMnKVxudmFyIHV0aWxpdGllcyA9IHJlcXVpcmUoJy4uL21vZHVsZXMvdXRpbGl0aWVzLmpzJylcblxudmFyIGxvZ2luUmVxID0ge1xuICAgIGxvZ2luUmVxdWlyZWQ6IFsnJGxvY2F0aW9uJywgJyRxJywgJ2F1dGhTZXJ2aWNlJywgZnVuY3Rpb24gKCRsb2NhdGlvbiwgJHEsIGF1dGhTZXJ2aWNlKSB7XG4gICAgICAgIHZhciBkZWZlcnJlZCA9ICRxLmRlZmVyKCk7XG5cbiAgICAgICAgaWYgKCFhdXRoU2VydmljZS5pc1NpZ25lZEluKCkpIHtcbiAgICAgICAgICAgIGRlZmVycmVkLnJlamVjdCgpXG4gICAgICAgICAgICAkbG9jYXRpb24ucGF0aCgnL3NpZ25pbicpO1xuICAgICAgICB9IGVsc2VcbiAgICAgICAgICAgIGRlZmVycmVkLnJlc29sdmUoKVxuXG4gICAgICAgIHJldHVybiBkZWZlcnJlZC5wcm9taXNlO1xuICAgIH1dXG59XG5cbnZhciBnZXRfY291bnRyeV9pbmZvID0gWyckcm91dGUnLCAnJGh0dHAnLCAnJHEnLCAnJHRyYW5zbGF0ZScsIHRyYW5zYWN0aW9uX3Byb2NzLmdldF9jb3VudHJ5X2luZm9dXG52YXIgZ2V0X2NvdW50cnlfaW5mb19TRU8gPSBbJyRyb3V0ZScsICckaHR0cCcsICckcScsICckdHJhbnNsYXRlJywgdHJhbnNhY3Rpb25fcHJvY3MuZ2V0X2NvdW50cnlfaW5mb19TRU9dXG5cbnZhciBwcmVfYXR0b3JuZXlzID0ge1xuICAgIGJpb19kYXRhOiBbJyRyb3V0ZScsICdhdHRvcm5leXNTZXJ2aWNlJywgZnVuY3Rpb24gKCRyb3V0ZSwgYXR0b3JuZXlzU2VydmljZSkge1xuLy8gICAgICAgIGNvbnNvbGUubG9nKCdhdHR5ICcsICRyb3V0ZS5jdXJyZW50LnBhcmFtcy5uYW1lKVxuICAgICAgICByZXR1cm4gYXR0b3JuZXlzU2VydmljZS5nZXQoJHJvdXRlLmN1cnJlbnQucGFyYW1zLm5hbWUpXG4gICAgfV1cbn1cblxudmFyIHByZV9yZWdpc3RyeTIgPSB7XG4gICAgcmVnaXN0cnlfaW5pdERhdGE6IFsnJHJvdXRlJywgJyRxJywgJ3JlZ2lzdGVyX3RyYWRlbWFyaycsIGZ1bmN0aW9uICgkcm91dGUsICRxLCByZWdpc3Rlcl90cmFkZW1hcmspIHtcbiAgICAgICAgcmV0dXJuIHRyYW5zYWN0aW9uX3Byb2NzLmI0cmVnaXN0cnkyKCRyb3V0ZSwgJHEsIHJlZ2lzdGVyX3RyYWRlbWFyaylcbiAgICB9XSxcbiAgICBjb3VudHJ5X2luZm86IGdldF9jb3VudHJ5X2luZm9cbn1cblxudmFyIHByZV9yZWdpc3RyeTUgPSB7XG4gICAgcmVnaXN0cnlfaW5pdERhdGE6IFsnJHJvdXRlJywgJyRxJywgJ3JlZ2lzdGVyX3RyYWRlbWFyaycsICdDYXJ0JywgZnVuY3Rpb24gKCRyb3V0ZSwgJHEsIHJlZ2lzdGVyX3RyYWRlbWFyaywgQ2FydCkge1xuICAgICAgICByZXR1cm4gdHJhbnNhY3Rpb25fcHJvY3MuYjRyZWdpc3RyeTUoJHJvdXRlLCAkcSwgcmVnaXN0ZXJfdHJhZGVtYXJrLCBDYXJ0KVxuICAgIH1dXG59XG5cblxudmFyIHByZV90cmFkZW1hcmtfY29udGFjdCA9IHtcbiAgICByZWdpc3RyeV9pbml0RGF0YTogWyckcm91dGUnLCAnJHEnLCAncmVnaXN0ZXJfdHJhZGVtYXJrJywgJ0NhcnQnLCBmdW5jdGlvbiAoJHJvdXRlLCAkcSwgcmVnaXN0ZXJfdHJhZGVtYXJrLCBDYXJ0KSB7XG4gICAgICAgIHJldHVybiB0cmFuc2FjdGlvbl9wcm9jcy5iNHRyYWRlbWFya19jb250YWN0KCRyb3V0ZSwgJHEsIHJlZ2lzdGVyX3RyYWRlbWFyaywgQ2FydClcbiAgICB9XVxufVxuXG52YXIgcHJlX3RyYWRlbWFya19hZGRyZXNzID0ge1xuICAgIHJlZ2lzdHJ5X2luaXREYXRhOiBbJyRyb3V0ZScsICckcScsICdyZWdpc3Rlcl90cmFkZW1hcmsnLCAnQ2FydCcsIGZ1bmN0aW9uICgkcm91dGUsICRxLCByZWdpc3Rlcl90cmFkZW1hcmssIENhcnQpIHtcbiAgICAgICAgcmV0dXJuIHRyYW5zYWN0aW9uX3Byb2NzLmI0dHJhZGVtYXJrX2FkZHJlc3MoJHJvdXRlLCAkcSwgcmVnaXN0ZXJfdHJhZGVtYXJrLCBDYXJ0KVxuICAgIH1dXG59XG5cblxudmFyIHByZV9hY2NvdW50X2FkZHJlc3MgPSB7XG4gICAgcmVnaXN0cnlfaW5pdERhdGE6IFsnJHJvdXRlJywgJyRxJywgJyRsb2NhdGlvbicsICd1c2VyX2FjY291bnQnLCBmdW5jdGlvbiAoJHJvdXRlLCAkcSwgJGxvY2F0aW9uLCB1c2VyX2FjY291bnQpIHtcbiAgICAgICAgcmV0dXJuIHRyYW5zYWN0aW9uX3Byb2NzLmI0YWNjb3VudF9hZGRyZXNzKCRyb3V0ZSwgJHEsICRsb2NhdGlvbiwgdXNlcl9hY2NvdW50KVxuICAgIH1dXG59XG5cblxudmFyIHByZV9wcm9maWxlX2FkZHJlc3MgPSB7XG4gICAgcmVnaXN0cnlfaW5pdERhdGE6IFsnJHJvdXRlJywgJyRxJywgJyRsb2NhdGlvbicsICd1c2VyX3Byb2ZpbGUnLCBmdW5jdGlvbiAoJHJvdXRlLCAkcSwgJGxvY2F0aW9uLCB1c2VyX3Byb2ZpbGUpIHtcbiAgICAgICAgcmV0dXJuIHRyYW5zYWN0aW9uX3Byb2NzLmI0cHJvZmlsZV9hZGRyZXNzKCRyb3V0ZSwgJHEsICRsb2NhdGlvbiwgdXNlcl9wcm9maWxlKVxuICAgIH1dXG59XG5cblxudmFyIHByZV9wcm9maWxlID0ge1xuICAgIHJlZ2lzdHJ5X2luaXREYXRhOiBbJyRyb3V0ZScsICckcScsICckbG9jYXRpb24nLCAndXNlcl9wcm9maWxlJywgZnVuY3Rpb24gKCRyb3V0ZSwgJHEsICRsb2NhdGlvbiwgdXNlcl9wcm9maWxlKSB7XG4gICAgICAgIHJldHVybiB0cmFuc2FjdGlvbl9wcm9jcy5iNHByb2ZpbGUoJHJvdXRlLCAkcSwgJGxvY2F0aW9uLCB1c2VyX3Byb2ZpbGUpXG4gICAgfV1cbn1cblxudmFyIHByZV9iZWZvcmVfY2hlY2tvdXQgPSB7XG4gICAgdmFsaWRfZW1haWxzOiBbJ0NhcnQnLCBmdW5jdGlvbiAoQ2FydCkge1xuICAgICAgICByZXR1cm4gcmVxdWlyZSgnLi93ZWJfY29udHJvbGxlcnNfY2hlY2tfb3V0JykuYjRjaGVja19vdXRfaW5pdGlhbF9jaGVjayhDYXJ0KVxuICAgIH1dXG59XG5cbnZhciBwcmVfY2FzZV9nZW4gPSB7XG4gICAgY2FzZV9vYmo6IFsnJHEnLCAnJHJvdXRlJywgICckbG9jYXRpb24nLCAnJHdpbmRvdycsICdDYXNlX2dlbicsIGZ1bmN0aW9uICgkcSwgJHJvdXRlLCAkbG9jYXRpb24sICR3aW5kb3csIENhc2VfZ2VuKSB7XG4gICAgICAgIHJldHVybiByZXF1aXJlKCcuL3dlYl9jb250cm9sbGVyc19jYXNlJykuYjRjYXNlX2dlbigkcSwgJHJvdXRlLCAkbG9jYXRpb24sICR3aW5kb3csIENhc2VfZ2VuKVxuICAgIH1dXG5cbn1cblxudmFyIHByZV9jYXNlX2dlbjIgPSB7XG4gICAgY2FzZV9vYmo6IFsnJHEnLCAnJHJvdXRlJywgJyRsb2NhdGlvbicsICckd2luZG93JywgJ0Nhc2VfZ2VuJywgZnVuY3Rpb24gKCRxLCAkcm91dGUsICRsb2NhdGlvbiwgJHdpbmRvdywgQ2FzZV9nZW4pIHtcbiAgICAgICAgcmV0dXJuIHJlcXVpcmUoJy4vd2ViX2NvbnRyb2xsZXJzX2Nhc2UnKS5iNGNhc2VfZ2VuMigkcSwgJHJvdXRlLCAkbG9jYXRpb24sICR3aW5kb3csIENhc2VfZ2VuKVxuICAgIH1dXG59XG5cblxubW9kdWxlLmV4cG9ydHMgPSBbJyRyb3V0ZVByb3ZpZGVyJywgJyRsb2NhdGlvblByb3ZpZGVyJywgZnVuY3Rpb24gKCRyb3V0ZVByb3ZpZGVyLCAkbG9jYXRpb25Qcm92aWRlcikge1xuICAgICRyb3V0ZVByb3ZpZGVyLlxuICAgICAgICB3aGVuKCcvJywge3RlbXBsYXRlVXJsOiAncGFydGlhbHMvaG9tZS5odG1sJywgY29udHJvbGxlcjogJ2hvbWVDdHJsJ30pLlxuICAgICAgICB3aGVuKCcvd2h5dXMnLCB7dGVtcGxhdGVVcmw6ICdwYXJ0aWFscy93aHl1cy5odG1sJywgY29udHJvbGxlcjogJ2hvbWVDdHJsJ30pLlxuICAgICAgICB3aGVuKCcvbXlsb2dzJywge3RlbXBsYXRlVXJsOiAncGFydGlhbHMvbXlsb2dzLmh0bWwnLCBjb250cm9sbGVyOiAnbXlsb2dzQ3RybCd9KS5cbiAgICAgICAgd2hlbignL3JlZ19hbGwnLCB7dGVtcGxhdGVVcmw6ICdwYXJ0aWFscy9yZWdfYWxsLmh0bWwnLCBjb250cm9sbGVyOiAncmVnX2FsbEN0cmwnfSkuXG4gICAgICAgIHdoZW4oJy80MDQvOmtleT8nLCB7dGVtcGxhdGVVcmw6ICdwYXJ0aWFscy80MDQuaHRtbCcsIGNvbnRyb2xsZXI6ICc0MDRDdHJsJ30pLlxuXG4gICAgICAgIHdoZW4oJy9yZWdpb24vOmNvZGUnLCB7dGVtcGxhdGVVcmw6ICcuLi8uLi9wYXJ0aWFscy9yZWdpb24uaHRtbCcsIGNvbnRyb2xsZXI6ICdyZWdpb25DdHJsJ30pLlxuICAgICAgICB3aGVuKCcvY291bnRyaWVzJywge3RlbXBsYXRlVXJsOiAnLi4vLi4vcGFydGlhbHMvY291bnRyaWVzLmh0bWwnLCBjb250cm9sbGVyOiAncmVnaW9uQ3RybCd9KS5cblxuICAgICAgICB3aGVuKCcvdHJhZGVtYXJrLzpjb3VudHJ5Jywge1xuICAgICAgICAgICAgdGVtcGxhdGVVcmw6ICcuLi8uLi9wYXJ0aWFscy90cmFkZW1hcmsuaHRtbCcsIGNvbnRyb2xsZXI6ICd0cmFkZW1hcmsnLCByZXNvbHZlOiB7XG4gICAgICAgICAgICAgICAgY291bnRyeV9pbmZvOiBnZXRfY291bnRyeV9pbmZvXG4gICAgICAgICAgICB9XG4gICAgICAgIH0pLlxuXG4vLyAgICB3aGVuKCcvdGVzdF9zdHJpcGUnLCB7dGVtcGxhdGVVcmw6ICcuLi8uLi9wYXJ0aWFscy90ZXN0X3N0cmlwZS5odG1sJywgY29udHJvbGxlcjogJ0luZGV4Q29udHJvbGxlcid9KS5cblxuICAgICAgICAvLyByZWdpc3RyYXRpb24gb2YgdHJhZGVtYXJrIDUgc3RlcHNcbiAgICAgICAgd2hlbignL3JlZ2lzdGVyMS86Y291bnRyeScsIHtcbiAgICAgICAgICAgIHRlbXBsYXRlVXJsOiAnLi4vLi4vcGFydGlhbHMvcmVnaXN0ZXIxLmh0bWwnLCBjb250cm9sbGVyOiAncmVnaXN0ZXIxJywgcmVzb2x2ZToge1xuICAgICAgICAgICAgICAgIGNvdW50cnlfaW5mbzogZ2V0X2NvdW50cnlfaW5mb1xuICAgICAgICAgICAgfVxuICAgICAgICB9KS5cbiAgICAgICAgd2hlbignL3JlZ2lzdGVyMi86Y291bnRyeScsIHtcbiAgICAgICAgICAgIHRlbXBsYXRlVXJsOiAnLi4vLi4vcGFydGlhbHMvcmVnaXN0ZXIyLmh0bWwnLCBjb250cm9sbGVyOiAncmVnaXN0ZXIyJyxcbiAgICAgICAgICAgIHJlc29sdmU6IHByZV9yZWdpc3RyeTJcbiAgICAgICAgfSkuXG4gICAgICAgIHdoZW4oJy9yZWdpc3RlcjMvOmNvdW50cnknLCB7dGVtcGxhdGVVcmw6ICcuLi8uLi9wYXJ0aWFscy9yZWdpc3RlcjMuaHRtbCcsIGNvbnRyb2xsZXI6ICdyZWdpc3RlcjMnfSkuXG4gICAgICAgIHdoZW4oJy9yZWdpc3RlcjQvOmNvdW50cnknLCB7dGVtcGxhdGVVcmw6ICcuLi8uLi9wYXJ0aWFscy9yZWdpc3RlcjQuaHRtbCcsIGNvbnRyb2xsZXI6ICdyZWdpc3RlcjQnfSkuXG5cbiAgICAgICAgd2hlbignL3JlZ2lzdGVyNS86Y291bnRyeScsIHtcbiAgICAgICAgICAgIHRlbXBsYXRlVXJsOiAnLi4vLi4vcGFydGlhbHMvcmVnaXN0ZXI1Lmh0bWwnLCBjb250cm9sbGVyOiAncmVnaXN0ZXI1JyxcbiAgICAgICAgICAgIHJlc29sdmU6IHByZV9yZWdpc3RyeTVcbiAgICAgICAgfSkuXG5cbiAgICAgICAgd2hlbignL3RyYWRlbWFya19jb250YWN0Lzpjb3VudHJ5Jywge1xuICAgICAgICAgICAgdGVtcGxhdGVVcmw6ICcuLi8uLi9wYXJ0aWFscy90cmFkZW1hcmtfY29udGFjdC5odG1sJyxcbiAgICAgICAgICAgIGNvbnRyb2xsZXI6ICd0cmFkZW1hcmtfY29udGFjdCcsXG4gICAgICAgICAgICByZXNvbHZlOiBwcmVfdHJhZGVtYXJrX2NvbnRhY3RcbiAgICAgICAgfSkuXG4gICAgICAgIHdoZW4oJy90cmFkZW1hcmtfYWRkcmVzcy86Y291bnRyeScsIHtcbiAgICAgICAgICAgIHRlbXBsYXRlVXJsOiAnLi4vLi4vcGFydGlhbHMvdHJhZGVtYXJrX2FkZHJlc3MuaHRtbCcsXG4gICAgICAgICAgICBjb250cm9sbGVyOiAndHJhZGVtYXJrX2FkZHJlc3MnLFxuICAgICAgICAgICAgcmVzb2x2ZTogcHJlX3RyYWRlbWFya19hZGRyZXNzXG4gICAgICAgIH0pLlxuXG4gICAgICAgIC8vIHN0dWR5IG9mIHRyYWRlbWFyayA/IHN0ZXBzXG4gICAgICAgIHdoZW4oJy9zdHVkeTEvOmNvdW50cnknLCB7dGVtcGxhdGVVcmw6ICcuLi8uLi9wYXJ0aWFscy9zdHVkeTEuaHRtbCcsIGNvbnRyb2xsZXI6ICdzdHVkeTEnfSkuXG4gICAgICAgIHdoZW4oJy9zdHVkeTIvOmNvdW50cnknLCB7XG4gICAgICAgICAgICB0ZW1wbGF0ZVVybDogJy4uLy4uL3BhcnRpYWxzL3N0dWR5Mi5odG1sJyxcbiAgICAgICAgICAgIGNvbnRyb2xsZXI6ICdyZWdpc3RlcjInLFxuICAgICAgICAgICAgcmVzb2x2ZTogcHJlX3JlZ2lzdHJ5MlxuICAgICAgICB9KS5cbiAgICAgICAgd2hlbignL3N0dWR5My86Y291bnRyeScsIHtcbiAgICAgICAgICAgIHRlbXBsYXRlVXJsOiAnLi4vLi4vcGFydGlhbHMvc3R1ZHkzLmh0bWwnLFxuICAgICAgICAgICAgY29udHJvbGxlcjogJ3N0dWR5MycsXG4gICAgICAgICAgICByZXNvbHZlOiBwcmVfcmVnaXN0cnk1XG4gICAgICAgIH0pLlxuICAgICAgICB3aGVuKCcvbW9uaXRvcjEvOmNvdW50cnknLCB7dGVtcGxhdGVVcmw6ICcuLi8uLi9wYXJ0aWFscy9tb25pdG9yMS5odG1sJywgY29udHJvbGxlcjogJ21vbml0b3IxJ30pLlxuICAgICAgICB3aGVuKCcvbW9uaXRvcjIvOmNvdW50cnknLCB7XG4gICAgICAgICAgICB0ZW1wbGF0ZVVybDogJy4uLy4uL3BhcnRpYWxzL21vbml0b3IyLmh0bWwnLFxuICAgICAgICAgICAgY29udHJvbGxlcjogJ21vbml0b3IyJyxcbiAgICAgICAgICAgIHJlc29sdmU6IHByZV9yZWdpc3RyeTJcbiAgICAgICAgfSkuXG5cbiAgICAgICAgd2hlbignL3ByaWNlcy86Y291bnRyeT8nLCB7dGVtcGxhdGVVcmw6ICdwYXJ0aWFscy9wcmljZXMuaHRtbCcsIGNvbnRyb2xsZXI6ICdwcmljZXNfY29udHJvbGxlcid9KS5cblxuICAgICAgICB3aGVuKCcvc2hvcHBpbmdfY2FydCcsIHt0ZW1wbGF0ZVVybDogJ3BhcnRpYWxzL3Nob3BwaW5nX2NhcnQuaHRtbCcsIGNvbnRyb2xsZXI6ICdjYXJ0Q29udHJvbGxlcid9KS5cbiAgICAgICAgd2hlbignL3Nob3BwaW5nX2NhcnRfdGhhbmtzLzpvcmRlcl9rZXknLCB7XG4gICAgICAgICAgICB0ZW1wbGF0ZVVybDogJ3BhcnRpYWxzL3Nob3BwaW5nX2NhcnRfdGhhbmtzLmh0bWwnLFxuICAgICAgICAgICAgY29udHJvbGxlcjogJ2NhcnRUaGFua3NDb250cm9sbGVyJ1xuICAgICAgICB9KS5cblxuICAgICAgICAvLyB0aGlzIGlzIG9ubHkgZm9yIHRlc3RcbiAgICAgICAgd2hlbignL3Byb2ZpbGVfZWRpdCcsIHt0ZW1wbGF0ZVVybDogJ3BhcnRpYWxzL3Byb2ZpbGVfZWRpdC5odG1sJywgY29udHJvbGxlcjogJ3Byb2ZpbGVfZWRpdCd9KS5cblxuXG4vLyAgICB3aGVuKCcvY2hlY2tfb3V0LzpjYXJ0X2lkJywge3RlbXBsYXRlVXJsOiAncGFydGlhbHMvY2hlY2tfb3V0Lmh0bWwnLCBjb250cm9sbGVyOiAnY2hlY2tfb3V0X2NvbnRyb2xsZXInfSkuXG4gICAgICAgIHdoZW4oJy9iZWZvcmVfY2hlY2tfb3V0LzpjYXJ0X2lkJywge1xuICAgICAgICAgICAgdGVtcGxhdGVVcmw6ICdwYXJ0aWFscy9iZWZvcmVfY2hlY2tfb3V0Lmh0bWwnLFxuICAgICAgICAgICAgY29udHJvbGxlcjogJ2JlZm9yZV9jaGVja19vdXQnLFxuICAgICAgICAgICAgcmVzb2x2ZTogcHJlX2JlZm9yZV9jaGVja291dFxuICAgICAgICB9KS5cbiAgICAgICAgd2hlbignL2NoZWNrX291dF9ub25fbWVtYmVyJywge1xuICAgICAgICAgICAgdGVtcGxhdGVVcmw6ICdwYXJ0aWFscy9iZWZvcmVfY2hlY2tfb3V0X25vbl9tZW1iZXIuaHRtbCcsXG4gICAgICAgICAgICBjb250cm9sbGVyOiAnYmVmb3JlX2NoZWNrX291dF9ub25fbWVtYmVyJ1xuICAgICAgICB9KS5cblxuICAgICAgICB3aGVuKCcvY2hlY2tfb3V0X3N0cmlwZS86Y2FydF9pZCcsIHtcbiAgICAgICAgICAgIHRlbXBsYXRlVXJsOiAncGFydGlhbHMvY2hlY2tfb3V0X3N0cmlwZS5odG1sJyxcbiAgICAgICAgICAgIGNvbnRyb2xsZXI6ICdjaGVja19vdXRfc3RyaXBlX2NvbnRyb2xsZXInLFxuICAgICAgICAgICAgaXNQcml2OiB0cnVlLFxuICAgICAgICAgICAgcmVzb2x2ZTogbG9naW5SZXFcbiAgICAgICAgfSkuXG4gICAgICAgIHdoZW4oJy9wb3N0X3BheW1lbnQvOmtleScsIHt0ZW1wbGF0ZVVybDogJ3BhcnRpYWxzL3Bvc3RfcGF5bWVudC5odG1sJywgY29udHJvbGxlcjogJ3Bvc3RfcGF5bWVudF9jb250cm9sbGVyJ30pLlxuICAgICAgICB3aGVuKCcvcG9zdF9wYXltZW50Mi86a2V5Jywge1xuICAgICAgICAgICAgdGVtcGxhdGVVcmw6ICdwYXJ0aWFscy9wb3N0X3BheW1lbnQyLmh0bWwnLFxuICAgICAgICAgICAgY29udHJvbGxlcjogJ3Bvc3RfcGF5bWVudDJfY29udHJvbGxlcidcbiAgICAgICAgfSkuXG5cbiAgICAgICAgd2hlbignL29yZGVycycsIHtcbiAgICAgICAgICAgIHRlbXBsYXRlVXJsOiAncGFydGlhbHMvb3JkZXJzLmh0bWwnLFxuICAgICAgICAgICAgY29udHJvbGxlcjogJ29yZGVyc0NvbnRyb2xsZXInLFxuICAgICAgICAgICAgaXNQcml2OiB0cnVlLFxuICAgICAgICAgICAgcmVzb2x2ZTogbG9naW5SZXEsXG4gICAgICAgICAgICByZWxvYWRPblNlYXJjaDogZmFsc2VcbiAgICAgICAgfSkuXG4gICAgICAgIHdoZW4oJy9vcmRlci86aWQnLCB7XG4gICAgICAgICAgICB0ZW1wbGF0ZVVybDogJ3BhcnRpYWxzL29yZGVyLmh0bWwnLFxuICAgICAgICAgICAgY29udHJvbGxlcjogJ29yZGVyRGV0YWlsQ29udHJvbGxlcicsXG4gICAgICAgICAgICBpc1ByaXY6IHRydWUsXG4gICAgICAgICAgICByZXNvbHZlOiBsb2dpblJlcSxcbiAgICAgICAgICAgIHJlbG9hZE9uU2VhcmNoOiBmYWxzZVxuICAgICAgICB9KS5cbiAgICAgICAgd2hlbignL3JlZ2lzdHJhdGlvbnMnLCB7XG4gICAgICAgICAgICB0ZW1wbGF0ZVVybDogJ3BhcnRpYWxzL3JlZ2lzdHJhdGlvbnMuaHRtbCcsIGNvbnRyb2xsZXI6ICdyZWdpc3RyYXRpb25zQ29udHJvbGxlcicsXG4gICAgICAgICAgICByZWxvYWRPblNlYXJjaDogZmFsc2UsIGlzUHJpdjogdHJ1ZSwgcmVzb2x2ZTogbG9naW5SZXFcbiAgICAgICAgfSkuXG4gICAgICAgIHdoZW4oJy9yZWdpc3RyYXRpb24vOmlkJywge1xuICAgICAgICAgICAgdGVtcGxhdGVVcmw6ICdwYXJ0aWFscy9yZWdpc3RyYXRpb24uaHRtbCcsXG4gICAgICAgICAgICBjb250cm9sbGVyOiAncmVnaXN0cmF0aW9uRGV0YWlsQ29udHJvbGxlcicsXG4gICAgICAgICAgICBpc1ByaXY6IHRydWUsXG4gICAgICAgICAgICByZXNvbHZlOiBsb2dpblJlcVxuICAgICAgICB9KS5cblxuICAgICAgICB3aGVuKCcvc2lnbnVwJywge3RlbXBsYXRlVXJsOiAncGFydGlhbHMvc2lnbnVwLmh0bWwnLCBjb250cm9sbGVyOiAnVXNlckNvbnRyb2xsZXInfSkuXG4gICAgICAgIHdoZW4oJy9zaWduaW4nLCB7dGVtcGxhdGVVcmw6ICdwYXJ0aWFscy9zaWduaW4uaHRtbCcsIGNvbnRyb2xsZXI6ICdVc2VyQ29udHJvbGxlcid9KS5cbiAgICAgICAgd2hlbignL3NpZ25vdXQnLCB7dGVtcGxhdGVVcmw6ICdwYXJ0aWFscy9jb21taW5nLmh0bWwnLCBjb250cm9sbGVyOiAnVXNlckNvbnRyb2xsZXInfSkuXG4gICAgICAgIHdoZW4oJy9yZWNvdmVycGFzc3dvcmQnLCB7dGVtcGxhdGVVcmw6ICdwYXJ0aWFscy9yZXNldHBhc3N3b3JkLmh0bWwnLCBjb250cm9sbGVyOiAnVXNlckNvbnRyb2xsZXInfSkuXG4gICAgICAgIHdoZW4oJy9jYXNlLzppZCcsIHtcbiAgICAgICAgICAgIHRlbXBsYXRlVXJsOiAnL3BhcnRpYWxzL2Nhc2UuaHRtbCcsIGNvbnRyb2xsZXI6ICdjYXNlR2VuQ3RybCcsXG4gICAgICAgICAgICByZXNvbHZlOiBwcmVfY2FzZV9nZW5cblxuICAgICAgICB9KS5cbiAgICAgICAgLypcbiAgICAgICAgd2hlbignL2Nhc2UyLzppZCcsIHtcbiAgICAgICAgICAgIHRlbXBsYXRlVXJsOiAnL3BhcnRpYWxzL2Nhc2UyLmh0bWwnLCBjb250cm9sbGVyOiAnY2FzZUdlbkN0cmwyJyxcbiAgICAgICAgICAgIHJlc29sdmU6IHByZV9jYXNlX2dlbjJcblxuICAgICAgICB9KS4gKi9cbiAgICAgICAgd2hlbignL2Rhc2hib2FyZCcsIHtcbiAgICAgICAgICAgIHRlbXBsYXRlVXJsOiAncGFydGlhbHMvZGFzaGJvYXJkLmh0bWwnLFxuICAgICAgICAgICAgY29udHJvbGxlcjogJ2Rhc2hib2FyZEN0cmwnLFxuICAgICAgICAgICAgaXNQcml2OiB0cnVlLFxuICAgICAgICAgICAgcmVzb2x2ZTogbG9naW5SZXFcbiAgICAgICAgfSkuXG5cbiAgICAgICAgd2hlbignL2FjY291bnQnLCB7XG4gICAgICAgICAgICB0ZW1wbGF0ZVVybDogJ3BhcnRpYWxzL2FjY291bnQuaHRtbCcsXG4gICAgICAgICAgICBjb250cm9sbGVyOiAnYWNjb3VudENvbnRyb2xsZXInLFxuICAgICAgICAgICAgaXNQcml2OiB0cnVlLFxuICAgICAgICAgICAgcmVzb2x2ZTogbG9naW5SZXFcbiAgICAgICAgfSkuXG4gICAgICAgIHdoZW4oJy9hY2NvdW50X2FkZHJlc3MvOmNvdW50cnknLCB7XG4gICAgICAgICAgICB0ZW1wbGF0ZVVybDogJy4uLy4uL3BhcnRpYWxzL3RyYWRlbWFya19hZGRyZXNzLmh0bWwnLCBjb250cm9sbGVyOiAndHJhZGVtYXJrX2FkZHJlc3MnLFxuICAgICAgICAgICAgcmVzb2x2ZTogcHJlX2FjY291bnRfYWRkcmVzc1xuICAgICAgICB9KS5cblxuICAgICAgICB3aGVuKCcvcHJvZmlsZS86aWQvOmJhY2tsaW5rPycsIHtcbiAgICAgICAgICAgIHRlbXBsYXRlVXJsOiAncGFydGlhbHMvcHJvZmlsZS5odG1sJyxcbiAgICAgICAgICAgIGNvbnRyb2xsZXI6ICdwcm9maWxlQ29udHJvbGxlcicsXG4gICAgICAgICAgICByZXNvbHZlOiBwcmVfcHJvZmlsZVxuICAgICAgICB9KS5cbiAgICAgICAgd2hlbignL3Byb2ZpbGVfYWRkcmVzcy86aWQnLCB7XG4gICAgICAgICAgICB0ZW1wbGF0ZVVybDogJy4uLy4uL3BhcnRpYWxzL3RyYWRlbWFya19hZGRyZXNzLmh0bWwnLCBjb250cm9sbGVyOiAndHJhZGVtYXJrX2FkZHJlc3MnLFxuICAgICAgICAgICAgcmVzb2x2ZTogcHJlX3Byb2ZpbGVfYWRkcmVzc1xuICAgICAgICB9KS5cbiAgICAgICAgd2hlbignL3Byb2ZpbGVzJywge1xuICAgICAgICAgICAgdGVtcGxhdGVVcmw6ICdwYXJ0aWFscy9wcm9maWxlcy5odG1sJyxcbiAgICAgICAgICAgIGNvbnRyb2xsZXI6ICdwcm9maWxlTGlzdENvbnRyb2xsZXInLFxuICAgICAgICAgICAgaXNQcml2OiB0cnVlLFxuICAgICAgICAgICAgcmVzb2x2ZTogbG9naW5SZXFcbiAgICAgICAgfSkuXG5cbiAgICAgICAgLy8gc3RhdGljIGluZm9ybWF0aW9uIG9mIHRoZSBzaXRlXG4gICAgICAgIHdoZW4oJy9hYm91dCcsIHt0ZW1wbGF0ZVVybDogJ3BhcnRpYWxzL2Fib3V0Lmh0bWwnLCBjb250cm9sbGVyOiAnaG9tZUN0cmwnfSkuXG4gICAgICAgIHdoZW4oJy9hdHRvcm5leXMnLCB7dGVtcGxhdGVVcmw6ICdwYXJ0aWFscy9hdHRvcm5leXNfbGlzdC5odG1sJywgY29udHJvbGxlcjogJ2hvbWVDdHJsJ30pLlxuICAgICAgICB3aGVuKCcvYXR0b3JuZXlzLzpuYW1lJywge1xuICAgICAgICAgICAgdGVtcGxhdGVVcmw6ICdwYXJ0aWFscy9hdHRvcm5leXMuaHRtbCcsXG4gICAgICAgICAgICBjb250cm9sbGVyOiAnYXR0b3JuZXlzQ3RybCBhcyBiaW8nLFxuICAgICAgICAgICAgcmVzb2x2ZTogcHJlX2F0dG9ybmV5c1xuICAgICAgICB9KS5cbiAgICAgICAgd2hlbignL3Rlcm1zJywge3RlbXBsYXRlVXJsOiAncGFydGlhbHMvdGVybXMuaHRtbCcsIGNvbnRyb2xsZXI6ICdob21lQ3RybCd9KS5cbiAgICAgICAgd2hlbignL3NlcnZpY2VzJywge3RlbXBsYXRlVXJsOiAncGFydGlhbHMvc2VydmljZXMuaHRtbCcsIGNvbnRyb2xsZXI6ICdob21lQ3RybCd9KS5cbiAgICAgICAgd2hlbignL2Nvb2tpZXMnLCB7dGVtcGxhdGVVcmw6ICdwYXJ0aWFscy9wcml2YWN5Lmh0bWwnLCBjb250cm9sbGVyOiAnaG9tZUN0cmwnfSkuXG4gICAgICAgIHdoZW4oJy9yZXNvdXJjZXMnLCB7dGVtcGxhdGVVcmw6ICdwYXJ0aWFscy9yZXNvdXJjZXMuaHRtbCcsIGNvbnRyb2xsZXI6ICdob21lQ3RybCd9KS5cbiAgICAgICAgd2hlbignL2NhcmVlcnMnLCB7dGVtcGxhdGVVcmw6ICdwYXJ0aWFscy9jYXJlZXJzLmh0bWwnLCBjb250cm9sbGVyOiAnaG9tZUN0cmwnfSkuXG4gICAgICAgIHdoZW4oJy9zZXJ2aWNlX2NvbnRyYWN0Jywge3RlbXBsYXRlVXJsOiAncGFydGlhbHMvc2VydmljZV9jb250cmFjdC5odG1sJywgY29udHJvbGxlcjogJ2hvbWVDdHJsJ30pLlxuICAgICAgICB3aGVuKCcvcHJpdmFjeScsIHt0ZW1wbGF0ZVVybDogJ3BhcnRpYWxzL3ByaXZhY3kuaHRtbCcsIGNvbnRyb2xsZXI6ICdob21lQ3RybCd9KS5cbiAgICAgICAgd2hlbignL2NvbnRhY3QnLCB7dGVtcGxhdGVVcmw6ICdwYXJ0aWFscy9jb250YWN0Lmh0bWwnLCBjb250cm9sbGVyOiAnY29udGFjdEN0cmwnfSkuXG4gICAgICAgIHdoZW4oJy9jbGFzc2VzJywge3RlbXBsYXRlVXJsOiAncGFydGlhbHMvY2xhc3Nlcy5odG1sJywgY29udHJvbGxlcjogJ2NsYXNzZXNDdHJsJ30pLlxuICAgICAgICB3aGVuKCcvc2VhcmNoX2NsYXNzZXMnLCB7dGVtcGxhdGVVcmw6ICdwYXJ0aWFscy9zZWFyY2hfY2xhc3Nlcy5odG1sJywgY29udHJvbGxlcjogJ3NlYXJjaF9jbGFzc2VzQ29udHJvbGxlcid9KS5cbiAgICAgICAgd2hlbignL3N1cHBvcnRlZF9icm93c2VycycsIHt0ZW1wbGF0ZVVybDogJ3BhcnRpYWxzL3N1cHBvcnRlZF9icm93c2Vycy5odG1sJywgY29udHJvbGxlcjogJ3NpZ251cEN0cmwnfSkuXG4gICAgICAgIHdoZW4oJy9lczYnLCB7dGVtcGxhdGVVcmw6ICdwYXJ0aWFscy9lczYuaHRtbCcsIGNvbnRyb2xsZXI6ICdlczZDdHJsJ30pXG5cbiAgICAvL2NvbnNvbGUudGFibGUodXRpbGl0aWVzLnN1cHBvcnRlZENvdW50cmllcygpKVxuXG4gICAgdmFyIHNjb3VudHJpZXMgPSB1dGlsaXRpZXMuc3VwcG9ydGVkQ291bnRyaWVzKClcblxuICAgIGNvbnNvbGUubG9nKCdzY291bnRyaWVzJywgc2NvdW50cmllcylcbiAgICBmb3IgKHZhciBjb2RlIGluIHNjb3VudHJpZXMpIHtcbiAgICAgICAgaWYgKHRydWUpIHtcbiAgICAgICAgICAgIHZhciBzbmFtZSA9IHNjb3VudHJpZXNbY29kZV0ubmFtZS5yZXBsYWNlKC8gL2csICdfJykudG9Mb3dlckNhc2UoKVxuICAgICAgICAgICAgJHJvdXRlUHJvdmlkZXIud2hlbignL3RyYWRlbWFyay1yZWdpc3RyYXRpb24taW4tJyArIHNuYW1lLCB7XG4gICAgICAgICAgICAgICAgdGVtcGxhdGVVcmw6ICcuLi8uLi9wYXJ0aWFscy90cmFkZW1hcmsuaHRtbCcsIGNvbnRyb2xsZXI6ICd0cmFkZW1hcmtTRU8nLCByZXNvbHZlOiB7XG4gICAgICAgICAgICAgICAgICAgIGNvdW50cnlfaW5mbzogZ2V0X2NvdW50cnlfaW5mb19TRU9cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgLypcbiAgICAgJHJvdXRlUHJvdmlkZXIud2hlbignL3RyYWRlbWFyay1yZWdpc3RyYXRpb24taW4tdHVya2V5Jywge3RlbXBsYXRlVXJsOiAnLi4vLi4vcGFydGlhbHMvdHJhZGVtYXJrLmh0bWwnLCBjb250cm9sbGVyOiAndHJhZGVtYXJrU0VPJywgcmVzb2x2ZToge1xuICAgICBjb3VudHJ5X2luZm86IGdldF9jb3VudHJ5X2luZm9fU0VPXG4gICAgIH19KS5cbiAgICAgKi9cblxuICAgICRyb3V0ZVByb3ZpZGVyLm90aGVyd2lzZSh7cmVkaXJlY3RUbzogJy8nfSk7XG5cbiAgICAkbG9jYXRpb25Qcm92aWRlci5odG1sNU1vZGUodHJ1ZSlcblxuICAgIC8vICRsb2NhdGlvblByb3ZpZGVyLmh0bWw1bW9kZSh7IGVuYWJsZWQ6IHRydWUsIHJlcXVpcmVCYXNlOiBmYWxzZSB9KTtcblxufV0iLCJcInVzZSBzdHJpY3RcIjtcbi8qIGdsb2JhbCBhbmd1bGFyKi9cblxudmFyIHNlcnZpY2VzX3Byb2NzID0gcmVxdWlyZSgnLi93ZWJfc2VydmljZXNfcHJvY3MnKVxudmFyIGNhcnRfcHJvY3MgPSByZXF1aXJlKCcuL3dlYl9zZXJ2aWNlc19jYXJ0JylcbnZhciB0cmFuc2FjdGlvbiA9IHJlcXVpcmUoJy4vd2ViX3NlcnZpY2VzX3RyYW5zYWN0aW9uJylcbnZhciBjYXNlX2dlbiA9IHJlcXVpcmUoJy4vd2ViX3NlcnZpY2VzX2Nhc2UnKVxudmFyIGNhcnRfc3RvcmUgPSByZXF1aXJlKCcuL3dlYl9jYXJ0X3N0b3JlJylcblxudmFyIHNlcnZpY2VzID0gYW5ndWxhci5tb2R1bGUoJ3Rrcy5zZXJ2aWNlcycsIFtdKS52YWx1ZSgndmVyc2lvbicsICcwLjEnKTtcblxuc2VydmljZXMuZmFjdG9yeSgncmVnaXN0ZXJfdHJhZGVtYXJrJywgWyckaHR0cCcsICckcScsIHRyYW5zYWN0aW9uLnJlZ2lzdGVyX3RyYWRlbWFyayBdKVxuXG5zZXJ2aWNlcy5zZXJ2aWNlKCdDYXJ0JywgW1wiJGh0dHBcIiwgXCIkcVwiLCBcImF1dGhTZXJ2aWNlXCIsIFwiQ2FydF9TdG9yZVwiLCBjYXJ0X3Byb2NzLkNhcnQgXSlcblxuICAuc2VydmljZSgnQ2FydF9TdG9yZScsIFtcIiR3aW5kb3dcIiwgIGNhcnRfc3RvcmUuQ2FydF9TdG9yZV0pXG5cbiAgLnNlcnZpY2UoJ1NlYXJjaF9jbGFzc2VzJywgW1wiJGh0dHBcIiwgXCIkcVwiLCBzZXJ2aWNlc19wcm9jcy5TZWFyY2hfY2xhc3Nlc10pXG5cbiAgLnNlcnZpY2UoJ2hlbHBfaW5mbycsIFsnJGh0dHAnLCAnJHEnLCBzZXJ2aWNlc19wcm9jcy5oZWxwX2luZm9dKVxuXG4gIC5zZXJ2aWNlKCdwYWdlX2luZm8nLCBbc2VydmljZXNfcHJvY3MucGFnZV9pbmZvXSlcbiAgLnNlcnZpY2UoJ0Nhc2VfZ2VuJywgW1wiJGh0dHBcIiwgXCIkcVwiLCBcInJlZ2lzdGVyX3RyYWRlbWFya1wiLCBcIkNhcnRfU3RvcmVcIiwgXCJ1c2VyX3Byb2ZpbGVcIiwgXCJhdXRoU2VydmljZVwiLCBjYXNlX2dlbi5DYXNlX2dlbl0pXG5cbiAgLmZhY3RvcnkoJ1JlZ2lzdHJhdGlvbnMnLCBbICckcmVzb3VyY2UnLCBmdW5jdGlvbiAoJHJlc291cmNlKSB7XG4gICAgcmV0dXJuICRyZXNvdXJjZSgnL2FwaS9yZWdpc3RyYXRpb25zJylcbiAgfV0pXG5cbiAgLmZhY3RvcnkoJ09yZGVycycsIFsnJHJlc291cmNlJywgZnVuY3Rpb24gKCRyZXNvdXJjZSkge1xuICAgIHJldHVybiAkcmVzb3VyY2UoJy9hcGkvb3JkZXJzJylcbiAgfV0pXG5cbiAgLnNlcnZpY2UoJ1RNQ291bnRyaWVzJywgWyckaHR0cCcsICckcScsIHNlcnZpY2VzX3Byb2NzLlRNQ291bnRyaWVzIF0pXG4gIC5zZXJ2aWNlKCd1c2VyX2FjY291bnQnLCBbJyRodHRwJywgJyRxJywgc2VydmljZXNfcHJvY3MudXNlcl9hY2NvdW50IF0pXG4gIC5zZXJ2aWNlKCd1c2VyX3Byb2ZpbGUnLCBbJyRodHRwJywgJyRxJywgc2VydmljZXNfcHJvY3MudXNlcl9wcm9maWxlIF0pXG4gIC5zZXJ2aWNlKCdhdHRvcm5leXNTZXJ2aWNlJywgWyckaHR0cCcsICckcScsIHNlcnZpY2VzX3Byb2NzLmF0dG9ybmV5c19zZXJ2aWNlX3Byb2NdKVxuICAuZmFjdG9yeSgnbmV3c19mZWVkZXInLCBbJyRodHRwJywgJyRxJywgc2VydmljZXNfcHJvY3MubmV3c19mZWVkZXJfcHJvY10pXG4iLCJcInVzZSBzdHJpY3RcIjtcbi8qZ2xvYmFsIGFuZ3VsYXIgKi9cblxuZXhwb3J0cy5DYXJ0ID0gZnVuY3Rpb24gKCRodHRwLCAkcSwgYXV0aFNlcnZpY2UsIENhcnRfU3RvcmUpIHtcblxuICB0aGlzLmdldFJlbW90ZUNhcnQgPSBmdW5jdGlvbiAoKSB7XG4gICAgY29uc29sZS5sb2coJ2dldCByZW1vdGUgY2FydCcpXG4gICAgdmFyIGRlZmVycmVkID0gJHEuZGVmZXIoKTtcblxuICAgIGlmIChhdXRoU2VydmljZS5pc1NpZ25lZEluKCkpIHtcblxuICAgICAgJGh0dHAuZ2V0KCcvYXBpL2dldF9jYXJ0Jykuc3VjY2VzcyhmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICBjb25zb2xlLmxvZygnZGF0YS5jYXJ0JywgZGF0YS5jYXJ0KVxuICAgICAgICBkZWZlcnJlZC5yZXNvbHZlKHtzdGF0dXM6ICdvaycsIG1lc3NhZ2U6ICdzdWNjZWVkZWQnLCBjYXJ0OiBkYXRhLmNhcnR9KTtcbiAgICAgIH0pLmVycm9yKGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgICBkZWZlcnJlZC5yZXNvbHZlKHtzdGF0dXM6ICdlcnInLCBtZXNzYWdlOiAnU29ycnksIGVycm9yIG9jY3VycmVkIHdoaWxlIGFjY2Vzc2luZyBzZXJ2ZXIsIHBsZWFzZSB0cnkgYWdhaW4uJ30pO1xuICAgICAgfSk7XG4gICAgfSBlbHNlXG4gICAgICBkZWZlcnJlZC5yZXNvbHZlKHtzdGF0dXM6ICdvaycsIG1lc3NhZ2U6ICdzdWNjZWVkZWQnLCBjYXJ0OiBDYXJ0X1N0b3JlLmdldENhcnQoKX0pO1xuXG4gICAgcmV0dXJuIGRlZmVycmVkLnByb21pc2U7XG4gIH1cblxuXG4gIHRoaXMuY2hlY2tfb3V0ID0gZnVuY3Rpb24gKGNhcnRfaWQpIHtcblxuICAgIHZhciBkZWZlcnJlZCA9ICRxLmRlZmVyKCk7XG5cbiAgICAkaHR0cC5wb3N0KCcvYXBpL2NoZWNrb3V0LycgKyBjYXJ0X2lkLCB7cGF5bWVudDogJ3BheXBhbCd9KS5cbiAgICAgIHN1Y2Nlc3MoZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgY29uc29sZS5sb2coZGF0YSlcbiAgICAgICAgZGVmZXJyZWQucmVzb2x2ZSh7c3RhdHVzOiBkYXRhLnN0YXR1cywgbWVzc2FnZTogZGF0YS5tZXNzYWdlLCBvcmRlcl9rZXk6IGRhdGEub3JkZXJfa2V5fSk7XG4gICAgICB9KS5cbiAgICAgIGVycm9yKGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgICBkZWZlcnJlZC5yZXNvbHZlKHtzdGF0dXM6ICdmYWlsZWQnLCBtZXNzYWdlOiAnU29ycnksIGVycm9yIG9jY3VycmVkIHdoaWxlIHBvc3RpbmcsIHBsZWFzZSB0cnkgYWdhaW4uJ30pO1xuICAgICAgfSk7XG5cbiAgICByZXR1cm4gZGVmZXJyZWQucHJvbWlzZTtcbiAgfVxuXG4gIHRoaXMuZ2V0UmVtb3RlQ2FydENvdW50ID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciBkZWZlcnJlZCA9ICRxLmRlZmVyKCk7XG5cbiAgICBjb25zb2xlLmxvZygnZ2V0UmVtb3RlQ2hhcnRDb3VudCcpXG4gICAgaWYgKGF1dGhTZXJ2aWNlLmlzU2lnbmVkSW4oKSkge1xuICAgICAgJGh0dHAuZ2V0KCcvYXBpL2dldF9jYXJ0X2NvdW50Jykuc3VjY2VzcyhmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICBkZWZlcnJlZC5yZXNvbHZlKGRhdGEuY2FydF9jb3VudCk7XG4gICAgICB9KS5lcnJvcihmdW5jdGlvbiAoZXJyb3IpIHtcbiAgICAgICAgZGVmZXJyZWQucmVzb2x2ZSh7c3RhdHVzOiAnZXJyJywgbWVzc2FnZTogJ1NvcnJ5LCBlcnJvciBvY2N1cnJlZCB3aGlsZSBhY2Nlc3Npbmcgc2VydmVyLCBwbGVhc2UgdHJ5IGFnYWluLid9KTtcbiAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG5cbiAgICAgIGRlZmVycmVkLnJlc29sdmUoQ2FydF9TdG9yZS5nZXRDb3VudCgpKVxuICAgIH1cblxuICAgIHJldHVybiBkZWZlcnJlZC5wcm9taXNlO1xuICB9XG5cblxuICB0aGlzLmdldFByb2ZpbGVzID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciBkZWZlcnJlZCA9ICRxLmRlZmVyKCk7XG4gICAgaWYgKGF1dGhTZXJ2aWNlLmlzU2lnbmVkSW4oKSkge1xuICAgICAgJGh0dHAuZ2V0KCcvYXBpL2dldF9wcm9maWxlcycpLnN1Y2Nlc3MoZnVuY3Rpb24gKHByb2ZpbGVzKSB7XG4gICAgICAgIGRlZmVycmVkLnJlc29sdmUocHJvZmlsZXMpO1xuICAgICAgfSlcbiAgICB9IGVsc2Uge1xuICAgICAgZGVmZXJyZWQucmVzb2x2ZShDYXJ0X1N0b3JlLmdldFByb2ZpbGVzKCkpXG4gICAgfVxuICAgIHJldHVybiBkZWZlcnJlZC5wcm9taXNlO1xuICB9XG5cbiAgdGhpcy5nZXRQcm9maWxlID0gZnVuY3Rpb24gKHByb2ZpbGVfaWQpIHtcbiAgICB2YXIgZGVmZXJyZWQgPSAkcS5kZWZlcigpO1xuICAgIGlmIChwcm9maWxlX2lkKSB7XG4gICAgICBpZiAoYXV0aFNlcnZpY2UuaXNTaWduZWRJbigpKSB7XG4gICAgICAgICRodHRwLmdldCgnL2FwaS9nZXRfcHJvZmlsZS8nICsgcHJvZmlsZV9pZCkuc3VjY2VzcyhmdW5jdGlvbiAocHJvZmlsZSkge1xuICAgICAgICAgIGRlZmVycmVkLnJlc29sdmUocHJvZmlsZSlcbiAgICAgICAgfSlcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGRlZmVycmVkLnJlc29sdmUoQ2FydF9TdG9yZS5nZXRQcm9maWxlKHByb2ZpbGVfaWQpKVxuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICBkZWZlcnJlZC5yZXNvbHZlKHVuZGVmaW5lZClcbiAgICB9XG4gICAgcmV0dXJuIGRlZmVycmVkLnByb21pc2U7XG4gIH1cblxuICAvLyB0aGlzIGZ1bmN0aW9uIHVzZSBlbWFpbCBhZGRyZXNzIG9mIHByb2ZpbGVzIGluIHRoZSBsb2NhbCBzdG9yZSB0byB2ZXJpZnkgd2l0aCBzZXJ2ZXIsIHNlZSBpZlxuICAvLyBleGlzdHMgaW4gdGhlIHNlcnZlclxuICAvLyB0aGlzIHdvcmtzIG9ubHkgZm9yIG5vdCBzaWduIGluLCBpZiBzaWduIGluLCBlbWFpbCByZXR1cm4gZW1wdHlcblxuICB0aGlzLmdldEV4aXN0aW5nRW1haWxzID0gZnVuY3Rpb24gKCkge1xuXG4gICAgdmFyIGRlZmVycmVkID0gJHEuZGVmZXIoKTtcbi8vICAgIGNvbnNvbGUubG9nKCdwcm9maWxlcycsIENhcnRfU3RvcmUuZ2V0QWxsUHJvZmlsZXMoKSlcblxuICAgIGlmICghYXV0aFNlcnZpY2UuaXNTaWduZWRJbigpKSB7XG4gICAgICB2YXIgcHJvZmlsZXMgPSBDYXJ0X1N0b3JlLmdldEFsbFByb2ZpbGVzKClcbiAgICAgIHZhciBlbWFpbHMgPSBbXVxuXG4gICAgICBmb3IgKHZhciBrZXkgaW4gcHJvZmlsZXMpIHtcbiAgICAgICAgaWYgKHByb2ZpbGVzLmhhc093blByb3BlcnR5KGtleSkpIHtcbiAgICAgICAgICBjb25zb2xlLmxvZygna2snLCBwcm9maWxlc1trZXldKVxuICAgICAgICAgIGVtYWlscy5wdXNoKHByb2ZpbGVzW2tleV0uZW1haWwpXG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgJGh0dHAucG9zdCgnL2FwaTIvdmFsaWRfZW1haWxzJywgZW1haWxzKS5cbiAgICAgICAgc3VjY2VzcyhmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgIGRlZmVycmVkLnJlc29sdmUoZGF0YSk7XG4gICAgICAgIH0pLlxuICAgICAgICBlcnJvcihmdW5jdGlvbiAoZXJyb3IpIHtcbiAgICAgICAgICBkZWZlcnJlZC5yZXNvbHZlKHtzdGF0dXM6IGZhbHNlLCBtZXNzYWdlOiAnU29ycnksIGVycm9yIG9jY3VycmVkIHdoaWxlIGNoZWNraW5nLCBwbGVhc2UgdHJ5IGFnYWluLid9KTtcbiAgICAgICAgfSk7XG4gICAgfSBlbHNlXG4gICAgICBkZWZlcnJlZC5yZXNvbHZlKFtdKVxuXG4gICAgcmV0dXJuIGRlZmVycmVkLnByb21pc2U7XG5cbiAgfVxuXG4gIC8vIGdldCB0aGUgZmlyc3QgbG9jYWwgcHJvZmlsZSBmb3Igc2lnbiB1cFxuICB0aGlzLmdldERlZmF1bHRMb2NhbFByb2ZpbGUgPSBmdW5jdGlvbigpIHtcblxuICAgIHZhciBwcm9maWxlcyA9IENhcnRfU3RvcmUuZ2V0QWxsUHJvZmlsZXMoKVxuICAgIGZvciAodmFyIGtleSBpbiBwcm9maWxlcykge1xuICAgICAgaWYgKHByb2ZpbGVzLmhhc093blByb3BlcnR5KGtleSkpIHtcbiAgICAgICAgcmV0dXJuIHByb2ZpbGVzW2tleV1cbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICAvLyBjYXJ0X2lkIG5vdCBuZWVkZWQgaWYgbm90IGxvZ2dlZFxuXG4gIHRoaXMuZGVsZXRlX2l0ZW0gPSBmdW5jdGlvbiAoY2FydF9pZCwgaXRlbSkge1xuXG4gICAgdmFyIGRlZmVycmVkID0gJHEuZGVmZXIoKTtcbiAgICBpZiAoYXV0aFNlcnZpY2UuaXNTaWduZWRJbigpKSB7XG4gICAgICAkaHR0cC5nZXQoJy9hcGkvZGVsZXRlX2NhcnRfaXRlbS8nICsgY2FydF9pZCArICcvJyArIGl0ZW0uX2lkKS5zdWNjZXNzKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgIGRlZmVycmVkLnJlc29sdmUoe3N0YXR1czogdHJ1ZX0pO1xuICAgICAgfSlcbiAgICB9IGVsc2Uge1xuICAgICAgQ2FydF9TdG9yZS5kZWxldGVfaXRlbShpdGVtKVxuICAgICAgZGVmZXJyZWQucmVzb2x2ZSh7c3RhdHVzOiB0cnVlfSk7XG4gICAgfVxuICAgIHJldHVybiBkZWZlcnJlZC5wcm9taXNlO1xuICB9XG5cblxuICAvLyBjYXJ0X2lkIG1pZ2h0IGJlICdsb2NhbCcgaWYgbm90IGxvZ2dlZCBpblxuXG4gIHRoaXMuc2VsZWN0aW9uX2NoYW5nZSA9IGZ1bmN0aW9uIChjYXJ0X2lkLCBpdGVtKSB7XG4gICAgY29uc29sZS5sb2coJ2NoYW5nZV9pdGVtJywgY2FydF9pZCwgaXRlbSlcbiAgICB2YXIgZGVmZXJyZWQgPSAkcS5kZWZlcigpO1xuICAgIGlmIChjYXJ0X2lkID09PSAnbG9jYWwnKSB7XG4gICAgICBDYXJ0X1N0b3JlLnVwZGF0ZV9pdGVtKGl0ZW0pXG4gICAgICBkZWZlcnJlZC5yZXNvbHZlKHRydWUpXG4gICAgfSBlbHNlIGlmIChhdXRoU2VydmljZS5pc1NpZ25lZEluKCkpIHtcbiAgICAgICRodHRwLnBvc3QoJy9hcGkvdXBkYXRlX2NhcnRfaXRlbS8nICsgY2FydF9pZCArICcvJyArIGl0ZW0uX2lkLCBpdGVtKS5zdWNjZXNzKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgIGRlZmVycmVkLnJlc29sdmUodHJ1ZSlcbiAgICAgIH0pXG4gICAgfVxuICAgIHJldHVybiBkZWZlcnJlZC5wcm9taXNlO1xuICB9XG5cbiAgdGhpcy5wb3N0X3RvX3NlcnZlciA9IGZ1bmN0aW9uIChpdGVtKSB7XG5cbiAgICB2YXIgZGVmZXJyZWQgPSAkcS5kZWZlcigpO1xuICAgIGlmIChhdXRoU2VydmljZS5pc1NpZ25lZEluKCkpIHtcbiAgICAgIGNvbnNvbGUubG9nKCdzaG93IGl0ZW0nLCBpdGVtKVxuICAgICAgJGh0dHAucG9zdCgnL2FwaS9wb3N0X3RyYWRlbWFyaycsIGl0ZW0pLlxuICAgICAgICBzdWNjZXNzKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhKVxuICAgICAgICAgIGRlZmVycmVkLnJlc29sdmUoe3JlZ2lzdGVyZWQ6IGRhdGEuc3RhdHVzLCBtZXNzYWdlOiBkYXRhLm1lc3NhZ2V9KTtcbiAgICAgICAgfSkuXG4gICAgICAgIGVycm9yKGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgICAgIGRlZmVycmVkLnJlc29sdmUoe3JlZ2lzdGVyZWQ6IGZhbHNlLCBtZXNzYWdlOiAnU29ycnksIGVycm9yIG9jY3VycmVkIHdoaWxlIHBvc3RpbmcsIHBsZWFzZSB0cnkgYWdhaW4uJ30pO1xuICAgICAgICB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgQ2FydF9TdG9yZS5hZGQoaXRlbSlcbiAgICAgIGRlZmVycmVkLnJlc29sdmUoe3JlZ2lzdGVyZWQ6IHRydWUsIG1lc3NhZ2U6ICd0cnVlJ30pO1xuICAgIH1cblxuICAgIHJldHVybiBkZWZlcnJlZC5wcm9taXNlO1xuXG4gIH1cblxuICAvLyBzYXZlIHRoZSBsb2NhbCBzdG9yZSB0byBzZXJ2ZXIsIHVzZWQgdGhpcyBmb3Igbm90IGxvZ2dlZCBpbiBzZWxlY3Rpb25zIHJpZ2h0IGFmdGVyIGxvZ2dpblxuXG4gIHRoaXMuc3RvcmVfdG9fc2VydmVyX25vdF90b191c2UgPSBmdW5jdGlvbiAoKSB7XG5cbiAgICB2YXIgZGVmZXJyZWQgPSAkcS5kZWZlcigpO1xuXG4gICAgaWYgKENhcnRfU3RvcmUuZ2V0Q291bnQoKSA+IDApIHtcbiAgICAgIHZhciB0ZW1wID0gQ2FydF9TdG9yZS5nZXRDYXJ0KClcbiAgICAgIGNvbnNvbGUubG9nKCd0ZW1wJywgdGVtcC5pdGVtcylcbiAgICAgIHZhciBzID0gYW5ndWxhci50b0pzb24odGVtcC5pdGVtcykgLy8gdGhpcyByZW1vdmVzIGFsbCBwcm9wZXJ0aWVzIHdpdGggJCRcbiAgICAgIHZhciBpdGVtcyA9IEpTT04ucGFyc2UocykgICAvLyBjb252ZXJ0IHRvIGpzb25cblxuICAgICAgJGh0dHAucG9zdCgnL2FwaS9iYXRjaF9wb3N0JywgaXRlbXMpLlxuICAgICAgICBzdWNjZXNzKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgZGVmZXJyZWQucmVzb2x2ZShkYXRhKTtcbiAgICAgICAgfSkuXG4gICAgICAgIGVycm9yKGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgICAgIGRlZmVycmVkLnJlc29sdmUoe3N0YXR1czogZmFsc2UsIG1lc3NhZ2U6ICdTb3JyeSwgZXJyb3Igb2NjdXJyZWQgd2hpbGUgcG9zdGluZywgcGxlYXNlIHRyeSBhZ2Fpbi4nfSk7XG4gICAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICBjb25zb2xlLmxvZygnbm90aGluZyBmcm9tIHN0b3JlJylcbiAgICAgIGRlZmVycmVkLnJlc29sdmUoe3N0YXR1czogdHJ1ZX0pO1xuICAgIH1cblxuICAgIHJldHVybiBkZWZlcnJlZC5wcm9taXNlO1xuICB9XG5cbiAgdGhpcy5zdG9yZV90b19zZXJ2ZXIgPSBmdW5jdGlvbiAoKSB7XG5cbiAgICB2YXIgZGVmZXJyZWQgPSAkcS5kZWZlcigpO1xuXG4gICAgaWYgKENhcnRfU3RvcmUuZ2V0Q291bnQoKSA+IDApIHtcbiAgICAgIHZhciB0ZW1wID0gQ2FydF9TdG9yZS5nZXRDYXJ0KClcbiAgICAgIHZhciBwcm9maWxlcyA9IENhcnRfU3RvcmUuZ2V0QWxsUHJvZmlsZXMoKVxuXG4gICAgICB2YXIgcyA9IGFuZ3VsYXIudG9Kc29uKHRlbXAuaXRlbXMpIC8vIHRoaXMgcmVtb3ZlcyBhbGwgcHJvcGVydGllcyB3aXRoICQkXG4gICAgICB2YXIgaXRlbXMgPSBKU09OLnBhcnNlKHMpICAgLy8gY29udmVydCB0byBqc29uXG5cbiAgICAgIC8vIGxldCdzIHJlbW92ZSBwcm9maWxlIG9iamVjdCBhcyB3ZSBhbHJlYWR5IGhhdmUgdGhlIHByb2ZpbGVfaWRcbiAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgaXRlbXMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgZGVsZXRlIGl0ZW1zW2ldLnByb2ZpbGVcbiAgICAgIH1cbiAgICAgIGNvbnNvbGUudGFibGUoaXRlbXMpXG5cbiAgICAgICRodHRwLnBvc3QoJy9hcGkvYmF0Y2hfcG9zdDInLCB7cHJvZmlsZXM6IHByb2ZpbGVzLCBpdGVtczogaXRlbXN9KS5cbiAgICAgICAgc3VjY2VzcyhmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgIGRlZmVycmVkLnJlc29sdmUoZGF0YSk7XG4gICAgICAgIH0pLlxuICAgICAgICBlcnJvcihmdW5jdGlvbiAoZXJyb3IpIHtcbiAgICAgICAgICBkZWZlcnJlZC5yZXNvbHZlKHtzdGF0dXM6IGZhbHNlLCBtZXNzYWdlOiAnU29ycnksIGVycm9yIG9jY3VycmVkIHdoaWxlIHBvc3RpbmcsIHBsZWFzZSB0cnkgYWdhaW4uJ30pO1xuICAgICAgICB9KTtcblxuICAgIH0gZWxzZSB7XG4gICAgICBjb25zb2xlLmxvZygnbm90aGluZyBmcm9tIHN0b3JlJylcbiAgICAgIGRlZmVycmVkLnJlc29sdmUoe3N0YXR1czogdHJ1ZX0pO1xuICAgIH1cblxuICAgIHJldHVybiBkZWZlcnJlZC5wcm9taXNlO1xuICB9XG5cbiAgdGhpcy5jbGVhbl9sb2NhbF9zdG9yZSA9IGZ1bmN0aW9uICgpIHtcbiAgICBjb25zb2xlLmxvZygnY2xlYW4gdGhlIGxvY2FsIHN0b3JlJylcbiAgICBDYXJ0X1N0b3JlLmRlc3Ryb3koKVxuICB9XG5cbiAgcmV0dXJuIHRoaXM7XG5cbn1cbiIsIlwidXNlIHN0cmljdFwiO1xuXG52YXIgcHJpY2VfY2FsYyA9IHJlcXVpcmUoXCIuL3ByaWNlX2NhbGMuanNcIilcbnZhciBhZGRyZXNzID0gcmVxdWlyZShcIi4vYWRkcmVzc190b19wcm9maWxlXCIpXG5cbmV4cG9ydHMuQ2FzZV9nZW4gPSBmdW5jdGlvbiAoJGh0dHAsICRxLCByZWdpc3Rlcl90cmFkZW1hcmssIENhcnRfU3RvcmUsIHVzZXJfcHJvZmlsZSwgYXV0aFNlcnZpY2UpIHtcblxuICAgIGZ1bmN0aW9uIGdldF90eXBlKGNhc2Vfbykge1xuICAgICAgICBpZiAoY2FzZV9vLmltYWdlX3VybClcbiAgICAgICAgICAgIHJldHVybiAnd2wnXG4gICAgICAgIGVsc2VcbiAgICAgICAgICAgIHJldHVybiAndyc7IC8vICd3JywgJ2wnLCAnd2wnICAgd29yZCBvbmx5L2xvZ28vd29yZCBhbmQgbG9nb1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGdldF9jbGFzc2VzKGNhc2Vfbykge1xuXG4gICAgICAgIHZhciByZXQgPSBbXVxuICAgICAgICB2YXIgY2xhc3NlcyA9IGNhc2Vfby5jbGFzc2VzLnNwbGl0KFwiLFwiKVxuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGNsYXNzZXMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIHJldC5wdXNoKHtuYW1lOiBjbGFzc2VzW2ldLCBkZXRhaWxzOiBcIlwifSlcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gcmV0XG4gICAgICAgIC8vICByZXR1cm4gW3tcIm5hbWVcIjpcIjAxXCIsXCJkZXRhaWxzXCI6XCJ3MVwifV1cbiAgICB9XG5cbiAgICBmdW5jdGlvbiBnZXRfcHJpY2UoY291bnRyeSkge1xuXG4gICAgICAgIHZhciBkZWZlcnJlZCA9ICRxLmRlZmVyKCk7XG4gICAgICAgICRodHRwLmdldCgnL2FwaS9nZXRfcHJpY2UvJyArIGNvdW50cnkpLnN1Y2Nlc3MoZnVuY3Rpb24gKGRhdGEpIHsgIC8vIGN1c3RvbWl6ZWQgcHJpY2Ugd2lsbCBiZSB1c2VkIGlmIHVzZXIgbG9nZ2VkIGluXG4gICAgICAgICAgICBjb25zb2xlLmxvZygnZ2V0IHByaWNlIDEyMyBpbiAnLCBkYXRhKVxuICAgICAgICAgICAgZGVmZXJyZWQucmVzb2x2ZShkYXRhKVxuICAgICAgICB9KS5lcnJvcihmdW5jdGlvbiAoZXJyb3IpIHtcbiAgICAgICAgICAgIGRlZmVycmVkLnJlamVjdCh7bXNnOiBlcnJvcn0pXG4gICAgICAgIH0pO1xuICAgICAgICByZXR1cm4gZGVmZXJyZWQucHJvbWlzZTtcbiAgICB9XG5cblxuICAgIGZ1bmN0aW9uIHNldF9pdGVtKGNhc2Vfb2JqLCBjb3VudHJ5KSB7XG4gICAgICAgIHZhciBkZWZlcnJlZCA9ICRxLmRlZmVyKCk7XG5cbiAgICAgICAgdmFyIHJlYyA9IHJlZ2lzdGVyX3RyYWRlbWFyay5pbml0X2l0ZW1fZXgoXCJyZWdpc3RyYXRpb25cIiwgY291bnRyeSlcbiAgICAgICAgcmVjLmNvdW50cnkgPSBjb3VudHJ5XG4gICAgICAgIHJlYy53b3JkbWFyayA9IGNhc2Vfb2JqLm1hcmtcbiAgICAgICAgcmVjLmltYWdlX3VybCA9IGNhc2Vfb2JqLmltYWdlX3VybFxuICAgICAgICByZWMudHlwZSA9IGdldF90eXBlKGNhc2Vfb2JqKVxuICAgICAgICByZWMuY2xhc3NlcyA9IGdldF9jbGFzc2VzKGNhc2Vfb2JqKVxuXG4gICAgICAgIGNvbnNvbGUubG9nKCdyZWMnLCBjYXNlX29iailcblxuICAgICAgICBnZXRfcHJpY2UoY291bnRyeSkudGhlbihmdW5jdGlvbiAocHJpY2UpIHtcbiAgICAgICAgICAgIC8vcmVjLnByaWNlID0gcHJpY2VcbiAgICAgICAgICAgIHJlYy5hbW91bnRfb3JpZyA9IHByaWNlX2NhbGMuZ2V0X2Ftb3VudChyZWMsIHByaWNlKVxuICAgICAgICAgICAgZGVmZXJyZWQucmVzb2x2ZShyZWMpXG4gICAgICAgIH0pXG5cbiAgICAgICAgcmV0dXJuIGRlZmVycmVkLnByb21pc2U7XG4gICAgfVxuXG4gICAgcmV0dXJuIHtcblxuICAgICAgICBnZXRfY2FzZV9vYmo6IGZ1bmN0aW9uIChjYXNlX2NvZGUpIHtcblxuICAgICAgICAgICAgdmFyIGRlZmVycmVkID0gJHEuZGVmZXIoKTtcblxuICAgICAgICAgICAgJGh0dHAuZ2V0KCcvYXBpMi9nZXRfY2FzZS8nICsgY2FzZV9jb2RlKS5zdWNjZXNzKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICAgICAgZGVmZXJyZWQucmVzb2x2ZShkYXRhKVxuICAgICAgICAgICAgfSlcblxuICAgICAgICAgICAgcmV0dXJuIGRlZmVycmVkLnByb21pc2U7XG4gICAgICAgIH0sXG5cbiAgICAgICAgZ2V0X3ByaWNlOiBmdW5jdGlvbiAoY291bnRyeSkge1xuXG4gICAgICAgICAgICB2YXIgZGVmZXJyZWQgPSAkcS5kZWZlcigpO1xuICAgICAgICAgICAgJGh0dHAuZ2V0KCcvYXBpL2dldF9wcmljZS8nICsgY291bnRyeSkuc3VjY2VzcyhmdW5jdGlvbiAoZGF0YSkgeyAgLy8gY3VzdG9taXplZCBwcmljZSB3aWxsIGJlIHVzZWQgaWYgdXNlciBsb2dnZWQgaW5cbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnZ2V0IHByaWNlIDEyMyBpbiAnLCBkYXRhKVxuICAgICAgICAgICAgICAgIGRlZmVycmVkLnJlc29sdmUoZGF0YSlcbiAgICAgICAgICAgIH0pLmVycm9yKGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgICAgICAgICAgIGRlZmVycmVkLnJlamVjdCh7bXNnOiBlcnJvcn0pXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIHJldHVybiBkZWZlcnJlZC5wcm9taXNlO1xuICAgICAgICB9LFxuXG4gICAgICAgIC8vIGJhc2VkIG9uIGlucHV0LCByZXR1cm4gdGhlIGl0ZW1zIG9yZGVyZWRcblxuICAgICAgICBnZXRfaXRlbXM6IGZ1bmN0aW9uIChjYXNlX29iaiwgc2VsZWN0ZWRDb3VudGlyZXMpIHtcblxuICAgICAgICAgICAgdmFyIHJldCA9IFtdXG4gICAgICAgICAgICBzZWxlY3RlZENvdW50aXJlcy5mb3JFYWNoKGZ1bmN0aW9uIChjb3VudHJ5KSB7XG4gICAgICAgICAgICAgICAgdmFyIHAgPSBzZXRfaXRlbShjYXNlX29iaiwgY291bnRyeSlcbiAgICAgICAgICAgICAgICByZXQucHVzaChwKVxuICAgICAgICAgICAgfSlcblxuICAgICAgICAgICAgcmV0dXJuICRxLmFsbChyZXQpLnRoZW4oZnVuY3Rpb24gKHJlc3VsdHMpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygncmVzdWx0czEyMycsIHJlc3VsdHMpXG4gICAgICAgICAgICAgICAgcmV0dXJuIHJlc3VsdHNcbiAgICAgICAgICAgIH0pXG4gICAgICAgIH0sXG5cbiAgICAgICAgLy8gcmVxdWVzdCBzZXJ2ZXIgdG8gZmV0Y2ggZnJvbSByZW1vdGUgdGhlIGltYWdlXG4gICAgICAgIC8vIGFuZCByZXR1cm4gdGhlIG5ldyB1cmwgaW4gdGhlIGxvY2FsIHNlcnZlclxuXG4gICAgICAgIGZldGNoX2ltYWdlczogZnVuY3Rpb24gKGltYWdlX3VybCkge1xuICAgICAgICAgICAgdmFyIGRlZmVycmVkID0gJHEuZGVmZXIoKTtcblxuICAgICAgICAgICAgY29uc29sZS5sb2coJ2ltYWdlX3VybCcsIGltYWdlX3VybClcbiAgICAgICAgICAgIGlmIChpbWFnZV91cmwgIT09ICdudWxsJyAmJiBpbWFnZV91cmwpIHtcbiAgICAgICAgICAgICAgICAkaHR0cC5nZXQoJy9hcGkyL2ZldGNoX2ltYWdlLycgKyBpbWFnZV91cmwpLnN1Y2Nlc3MoZnVuY3Rpb24gKHNlcnZlcl91cmwpIHtcbiAgICAgICAgICAgICAgICAgICAgZGVmZXJyZWQucmVzb2x2ZShzZXJ2ZXJfdXJsLnVybClcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgfSBlbHNlXG4gICAgICAgICAgICAgICAgZGVmZXJyZWQucmVzb2x2ZSh1bmRlZmluZWQpXG5cbiAgICAgICAgICAgIHJldHVybiBkZWZlcnJlZC5wcm9taXNlO1xuICAgICAgICB9LFxuXG4gICAgICAgIC8vIHJldHVybiBhIGRlZmF1bHQgcHJvZmlsZSBiYXNlZCBvbiBhZGRyZXNzXG5cbiAgICAgICAgZ2V0X2RlZmF1bHRfcHJvZmlsZTogZnVuY3Rpb24gKGNhc2Vfb2JqKSB7XG4gICAgICAgICAgICAvLyAgcmV0dXJuIGFkZHJlc3NfdG9fcHJvZmlsZShjYXNlX29iailcbiAgICAgICAgICAgIHZhciBwID0gYWRkcmVzcy5wYXJzZShjYXNlX29iai5hZGRyZXNzKVxuICAgICAgICAgICAgcC5wcm9maWxlX25hbWUgPSBjYXNlX29iai5jYXNlX2NvZGVcbiAgICAgICAgICAgIHJldHVybiBwXG4gICAgICAgIH0sXG5cbiAgICAgICAgLy8gY3JlYXRlIGEgcHJvZmlsZSBpbiBsb2NhbCBzdG9yZSBvciBpbiB0aGUgc2VydmVyKGlmIHNpZ25lZClcbiAgICAgICAgLy8gcmV0dXJuIHByb2ZpbGVfaWRcblxuICAgICAgICBjcmVhdGVfcHJvZmlsZTogZnVuY3Rpb24gKHByb2ZpbGUpIHtcbiAgICAgICAgICAgIHZhciBkZWZlcnJlZCA9ICRxLmRlZmVyKCk7XG4gICAgICAgICAgICBpZiAoIWF1dGhTZXJ2aWNlLmlzU2lnbmVkSW4oKSkge1xuICAgICAgICAgICAgICAgIHZhciBwcm9maWxlX2lkXG4gICAgICAgICAgICAgICAgdmFyIHBJRHMgPSBDYXJ0X1N0b3JlLmdldEFsbFByb2ZpbGVzSUQoKSAgICAvLyBsZXQncyB0cnkgdG8gZ2V0IGV4aXN0aW5nIHByb2ZpbGUgZmlyc3RcbiAgICAgICAgICAgICAgICBpZiAocElEcy5sZW5ndGggPiAwKVxuICAgICAgICAgICAgICAgICAgICBwcm9maWxlX2lkID0gcElEc1swXVxuICAgICAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgICAgICAgICAgcHJvZmlsZV9pZCA9IENhcnRfU3RvcmUuY3JlYXRlX3Byb2ZpbGUocHJvZmlsZSlcbiAgICAgICAgICAgICAgICBkZWZlcnJlZC5yZXNvbHZlKHByb2ZpbGVfaWQpXG4gICAgICAgICAgICB9IGVsc2Uge1xuXG4gICAgICAgICAgICAgICAgLy8gbGV0J3Mgc2VlIGlmIHRoZXJlIGlzIGFuIGV4aXN0aW5nIHByb2ZpbGUgaW4gdGhlIHNlcnZlclxuICAgICAgICAgICAgICAgIHVzZXJfcHJvZmlsZS5nZXRfYWxsX3Byb2ZpbGVzSUQoKVxuICAgICAgICAgICAgICAgICAgICAudGhlbihmdW5jdGlvbiAocHJvZmlsZXNJRCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHByb2ZpbGVzSUQubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlZmVycmVkLnJlc29sdmUocHJvZmlsZXNJRFswXSlcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdXNlcl9wcm9maWxlLnBvc3QocHJvZmlsZSkudGhlbihmdW5jdGlvbiAoc3RhdHVzKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlZmVycmVkLnJlc29sdmUoc3RhdHVzLmlkKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gZGVmZXJyZWQucHJvbWlzZTtcbiAgICAgICAgfVxuXG4gICAgfVxuXG59XG5cbi8vIHNvbWUgbW9yZSBzcGVjaWZpYyBsb2dpYyBpcyBuZWVkZWQgaGVyZVxuLypcbiBmdW5jdGlvbiBhZGRyZXNzX3RvX3Byb2ZpbGUoY2FzZV9vYmopIHtcblxuIGNvbnNvbGUubG9nKCdjYXNlX29iaicsIGNhc2Vfb2JqKVxuIHZhciBwcm9maWxlID0ge1xuIGFkZHJlc3M6IFwiY2ViXCIsXG4gY2l0eTogXCJjZWJcIixcbiBjb250YWN0OiBcIlwiLFxuIGNvdW50cnk6IFwiVUtcIixcbiBlbWFpbDogXCJcIixcbiBpbmNfY291bnRyeTogXCJVS1wiLFxuIG5hdHVyZTogXCJJXCIsXG4gcGhvbmU6IFwiXCIsXG4gcHJvZmlsZV9uYW1lOiBjYXNlX29iai5jYXNlX2NvZGUsXG4gc3RhdGU6IFwiXCIsXG4gc3RyZWV0OiBcIlwiLFxuIHppcDogXCJcIlxuIH1cbiByZXR1cm4gcHJvZmlsZVxuIH1cbiAqL1xuXG4iLCIvLyBAdHMtY2hlY2tcblwidXNlIHN0cmljdFwiO1xuXG52YXIgY291bnRyaWVzID0gcmVxdWlyZShcIi4uLy4uLy4uL2RhdGEvY291bnRyaWVzL3RtX2NvdW50cmllcy5qc29uXCIpXG5cbmV4cG9ydHMubmV3c19mZWVkZXJfcHJvYyA9IGZ1bmN0aW9uICgkaHR0cCwgJHEpIHtcblxuICAgIHZhciBmZWVkcyA9IFtdXG4gICAgcmV0dXJuIHtcblxuICAgICAgICBnZXRfZmVlZHM6IGZ1bmN0aW9uIChuZXdzX251bSkge1xuICAgICAgICAgICAgdmFyIGRlZmVycmVkID0gJHEuZGVmZXIoKTtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdnZXQgZmVlZHMgJywgbmV3c19udW0pXG5cbiAgICAgICAgICAgIGlmIChmZWVkcy5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgICAgZGVmZXJyZWQucmVzb2x2ZShmZWVkc1swXSlcbiAgICAgICAgICAgICAgICBmZWVkcy5zaGlmdCgpXG4gICAgICAgICAgICB9IGVsc2Uge1xuXG4gICAgICAgICAgICAgICAgJGh0dHAuZ2V0KCcvYXBpX2luZm8vZ2V0X2ZlZWRzLzEwJykuc3VjY2VzcyhmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgICAgICAgICBmZWVkcyA9IGRhdGFcblxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnZmVlZHMnLCBmZWVkcylcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ2ZlZWRzMCcsIGZlZWRzWzBdKVxuICAgICAgICAgICAgICAgICAgICAvLyAgICBkZWZlcnJlZC5yZXNvbHZlKGZlZWRzWzBdKVxuICAgICAgICAgICAgICAgICAgICBkZWZlcnJlZC5yZXNvbHZlKGZlZWRzKVxuICAgICAgICAgICAgICAgICAgICAvLyAgZmVlZHMuc2hpZnQoKVxuXG4gICAgICAgICAgICAgICAgfSkuZXJyb3IoZnVuY3Rpb24gKGVycm9yKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycm9yKVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIGRlZmVycmVkLnByb21pc2VcbiAgICAgICAgfVxuICAgIH1cblxufVxuXG5leHBvcnRzLmF0dG9ybmV5c19zZXJ2aWNlX3Byb2MgPSBmdW5jdGlvbiAoJGh0dHAsICRxKSB7XG5cbiAgICB0aGlzLmdldCA9IGZ1bmN0aW9uIChuYW1lKSB7XG5cbiAgICAgICAgdmFyIGRlZmVycmVkID0gJHEuZGVmZXIoKTtcblxuICAgICAgICAkaHR0cC5nZXQoJy9hcGlfaW5mby9nZXRfYXR0b3JuZXlzLycgKyBuYW1lKS5zdWNjZXNzKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICBkZWZlcnJlZC5yZXNvbHZlKGRhdGEpXG5cbiAgICAgICAgfSkuZXJyb3IoZnVuY3Rpb24gKGVycm9yKSB7XG4gICAgICAgICAgICBkZWZlcnJlZC5yZXNvbHZlKHt9KTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgcmV0dXJuIGRlZmVycmVkLnByb21pc2U7XG5cbiAgICB9XG5cbiAgICByZXR1cm4gdGhpcztcblxufVxuXG5leHBvcnRzLlNlYXJjaF9jbGFzc2VzID0gZnVuY3Rpb24gKCRodHRwLCAkcSkge1xuXG4gICAgdGhpcy5zZWFyY2ggPSBmdW5jdGlvbiAoc2VhcmNoX3RleHQpIHtcbiAgICAgICAgY29uc29sZS5sb2coJ3NlYXJjaCBjbGFzc2VzJylcbiAgICAgICAgdmFyIGRlZmVycmVkID0gJHEuZGVmZXIoKTtcblxuICAgICAgICAkaHR0cC5nZXQoJy9hcGkyL3NlYXJjaF9jbGFzc2VzP3E9JyArIHNlYXJjaF90ZXh0KS5zdWNjZXNzKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhKVxuICAgICAgICAgICAgZGVmZXJyZWQucmVzb2x2ZSh7IHN0YXR1czogZGF0YS5zdGF0dXMgPT09ICdvaycsIG1lc3NhZ2U6IGRhdGEubWVzc2FnZSwgcmVzdWx0OiBkYXRhLnJlc3VsdCB9KTtcbiAgICAgICAgfSkuZXJyb3IoZnVuY3Rpb24gKGVycm9yKSB7XG4gICAgICAgICAgICBkZWZlcnJlZC5yZXNvbHZlKHtcbiAgICAgICAgICAgICAgICBzdGF0dXM6IGZhbHNlLFxuICAgICAgICAgICAgICAgIG1lc3NhZ2U6ICdTb3JyeSwgZXJyb3Igb2NjdXJyZWQgd2hpbGUgYWNjZXNzaW5nIHNlcnZlciwgcGxlYXNlIHRyeSBhZ2Fpbi4nXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgICAgIHJldHVybiBkZWZlcnJlZC5wcm9taXNlO1xuICAgIH1cbiAgICByZXR1cm4gdGhpcztcbn1cblxuZXhwb3J0cy5oZWxwX2luZm8gPSBmdW5jdGlvbiAoJGh0dHAsICRxKSB7XG4gICAgdGhpcy5nZXQgPSBmdW5jdGlvbiAoaW5mb19rZXkpIHtcbiAgICAgICAgdmFyIGRlZmVycmVkID0gJHEuZGVmZXIoKTtcblxuICAgICAgICAkaHR0cC5nZXQoJy9hcGkyL2dldF9oZWxwX2luZm8vJyArIGluZm9fa2V5KS5zdWNjZXNzKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZygnYicpXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhKVxuICAgICAgICAgICAgZGVmZXJyZWQucmVzb2x2ZSh7IHN0YXR1czogZGF0YS5zdGF0dXMgPT09ICdvaycsIG1lc3NhZ2U6IGRhdGEubWVzc2FnZSwgcmVzdWx0OiBkYXRhLmluZm8gfSk7XG4gICAgICAgIH0pLmVycm9yKGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgICAgICAgZGVmZXJyZWQucmVzb2x2ZSh7XG4gICAgICAgICAgICAgICAgc3RhdHVzOiBmYWxzZSxcbiAgICAgICAgICAgICAgICBtZXNzYWdlOiAnU29ycnksIGVycm9yIG9jY3VycmVkIHdoaWxlIGFjY2Vzc2luZyBzZXJ2ZXIsIHBsZWFzZSB0cnkgYWdhaW4uJ1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIHJldHVybiBkZWZlcnJlZC5wcm9taXNlO1xuICAgIH1cbn1cblxuZXhwb3J0cy5UTUNvdW50cmllcyA9IGZ1bmN0aW9uICgkaHR0cCwgJHEpIHtcbiAgICB0aGlzLmdldCA9IGZ1bmN0aW9uICgpIHtcblxuICAgICAgICB2YXIgZGVmZXJyZWQgPSAkcS5kZWZlcigpO1xuXG4gICAgICAgICRodHRwLmdldCgnL2FwaTIvZ2V0X2NvdW50cmllcycpLnN1Y2Nlc3MoZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgICAgIGRlZmVycmVkLnJlc29sdmUoZGF0YSk7XG4gICAgICAgIH0pLmVycm9yKGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgICAgICAgZGVmZXJyZWQucmVzb2x2ZSh7fSk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIHJldHVybiBkZWZlcnJlZC5wcm9taXNlO1xuXG4gICAgfVxuXG4gICAgcmV0dXJuIHRoaXM7XG59XG5cbmV4cG9ydHMudXNlcl9hY2NvdW50ID0gZnVuY3Rpb24gKCRodHRwLCAkcSkge1xuXG4gICAgdGhpcy5nZXQgPSBmdW5jdGlvbiAoKSB7XG5cbiAgICAgICAgdmFyIGRlZmVycmVkID0gJHEuZGVmZXIoKTtcblxuICAgICAgICAkaHR0cC5nZXQoJy9hcGkvbXlhY2NvdW50LycpLnN1Y2Nlc3MoZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgICAgIGRlZmVycmVkLnJlc29sdmUoZGF0YSlcbiAgICAgICAgfSlcblxuICAgICAgICByZXR1cm4gZGVmZXJyZWQucHJvbWlzZTtcbiAgICB9XG5cbiAgICB0aGlzLnBvc3QgPSBmdW5jdGlvbiAoYWNjb3VudCkge1xuXG4gICAgICAgIHZhciBkZWZlcnJlZCA9ICRxLmRlZmVyKCk7XG4gICAgICAgIGNvbnNvbGUubG9nKCdzYXZpbmcgYWNjb3VudCAnLCBhY2NvdW50KVxuICAgICAgICAkaHR0cC5wb3N0KFwiL2FwaS91cGRhdGVfdXNlclwiLCBhY2NvdW50KS5zdWNjZXNzKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICBkZWZlcnJlZC5yZXNvbHZlKGRhdGEpXG4gICAgICAgIH0pO1xuICAgICAgICByZXR1cm4gZGVmZXJyZWQucHJvbWlzZTtcbiAgICB9XG5cbiAgICByZXR1cm4gdGhpcztcblxufVxuXG5cbmV4cG9ydHMudXNlcl9wcm9maWxlID0gZnVuY3Rpb24gKCRodHRwLCAkcSkge1xuXG4gICAgdGhpcy5nZXQgPSBmdW5jdGlvbiAocHJvZmlsZV9pZCkge1xuXG4gICAgICAgIHZhciBkZWZlcnJlZCA9ICRxLmRlZmVyKCk7XG5cbiAgICAgICAgaWYgKHByb2ZpbGVfaWQgPT09ICduZXcnKSB7XG4gICAgICAgICAgICBkZWZlcnJlZC5yZXNvbHZlKHt9KVxuICAgICAgICB9IGVsc2VcbiAgICAgICAgICAgICRodHRwLmdldCgnL2FwaS9wcm9maWxlLycgKyBwcm9maWxlX2lkKS5zdWNjZXNzKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ3VzZXJfcHJvZmlsZTExMScsIGRhdGEpXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coSlNPTi5zdHJpbmdpZnkoZGF0YSkpXG4gICAgICAgICAgICAgICAgZGVmZXJyZWQucmVzb2x2ZShkYXRhKVxuICAgICAgICAgICAgfSlcblxuICAgICAgICByZXR1cm4gZGVmZXJyZWQucHJvbWlzZTtcbiAgICB9XG5cbiAgICB0aGlzLnBvc3QgPSBmdW5jdGlvbiAocHJvZmlsZSkge1xuXG4gICAgICAgIHZhciBkZWZlcnJlZCA9ICRxLmRlZmVyKCk7XG4gICAgICAgICRodHRwLnBvc3QoXCIvYXBpL3VwZGF0ZV9wcm9maWxlXCIsIHByb2ZpbGUpLnN1Y2Nlc3MoZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgICAgIGRlZmVycmVkLnJlc29sdmUoZGF0YSlcbiAgICAgICAgfSk7XG4gICAgICAgIHJldHVybiBkZWZlcnJlZC5wcm9taXNlO1xuICAgIH1cblxuICAgIHRoaXMuZ2V0X2FsbF9wcm9maWxlc0lEID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgZGVmZXJyZWQgPSAkcS5kZWZlcigpO1xuICAgICAgICAkaHR0cC5nZXQoJy9hcGkvZ2V0X3Byb2ZpbGVzJykuc3VjY2VzcyhmdW5jdGlvbiAocHJvZmlsZXMpIHtcbiAgICAgICAgICAgIGRlZmVycmVkLnJlc29sdmUoT2JqZWN0LmtleXMocHJvZmlsZXMpKVxuICAgICAgICB9KVxuICAgICAgICByZXR1cm4gZGVmZXJyZWQucHJvbWlzZTtcbiAgICB9XG5cbiAgICByZXR1cm4gdGhpcztcblxufVxuXG5cbmV4cG9ydHMucGFnZV9pbmZvID0gZnVuY3Rpb24gKCkge1xuXG4gICAgdmFyIF90aXRsZSA9ICdUcmFkZW1hcmtlcnMuY29tJztcbiAgICB2YXIgX2tleXdvcmRzID0gXCJSZWdpc3RlciBUcmFkZW1hcmtzXCJcbiAgICB2YXIgX2Rlc2MgPSBcIlJlZ2lzdGVyIFRyYWRlbWFya3NcIlxuXG4gICAgdmFyIF9qc29uZF9pZCA9IHtcblxuICAgICAgICBcIkBjb250ZXh0XCI6IFwiaHR0cDovL3d3dy5zY2hlbWEub3JnXCIsXG4gICAgICAgIFwiQHR5cGVcIjogXCJPcmdhbml6YXRpb25cIixcbiAgICAgICAgXCJAaWRcIjogXCJodHRwczovL3d3dy50cmFkZW1hcmtlcnMuY29tXCIsXG4gICAgICAgIFwibmFtZVwiOiBcIlRyYWRlbWFya2VycyBMTEMxMjNcIixcbiAgICAgICAgXCJ1cmxcIjogXCJodHRwczovL3d3dy50cmFkZW1hcmtlcnMuY29tXCIsXG4gICAgICAgIFwibG9nb1wiOiBcImh0dHBzOi8vd3d3LnRyYWRlbWFya2Vycy5jb20vaW1hZ2VzL2xvZ28yLnBuZ1wiLFxuICAgICAgICBcImltYWdlXCI6IFwiaHR0cHM6Ly93d3cudHJhZGVtYXJrZXJzLmNvbS9pbWFnZXMvbG9nbzIucG5nXCIsXG4gICAgICAgIFwiZGVzY3JpcHRpb25cIjogXCJUUkFERU1BUktFUlMgYXJlIHNwZWNpYWxpc3RzIGluIGhhbmRsaW5nIHRoZSByZWdpc3RyYXRpb24gb2YgeW91ciB0cmFkZW1hcmtzIHdvcmxkd2lkZVwiLFxuXG4gICAgfVxuXG5cbiAgICByZXR1cm4ge1xuXG4gICAgICAgIHRpdGxlOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICByZXR1cm4gX3RpdGxlO1xuICAgICAgICB9LFxuXG4gICAgICAgIHNldFRpdGxlOiBmdW5jdGlvbiAobmV3VGl0bGUpIHtcbiAgICAgICAgICAgIF90aXRsZSA9IG5ld1RpdGxlO1xuICAgICAgICB9LFxuXG4gICAgICAgIGRlc2NyaXB0aW9uOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICByZXR1cm4gX2Rlc2NcbiAgICAgICAgfSxcblxuICAgICAgICBrZXl3b3JkczogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgcmV0dXJuIF9rZXl3b3JkcztcbiAgICAgICAgfSxcblxuICAgICAgICBqc29uX2lkOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICByZXR1cm4gX2pzb25kX2lkXG4gICAgICAgIH0sXG5cblxuICAgICAgICBzZXRfa2V5d29yZHM6IGZ1bmN0aW9uIChrd29yZHMpIHtcbiAgICAgICAgICAgIF9rZXl3b3JkcyA9IGt3b3Jkc1xuICAgICAgICB9LFxuXG4gICAgICAgIHNldF9kZXNjcmlwdGlvbjogZnVuY3Rpb24gKGRlc2MpIHtcbiAgICAgICAgICAgIF9kZXNjID0gZGVzY1xuICAgICAgICB9LFxuXG5cbiAgICAgICAgc2V0X2J5X2NvdW50cnlfY29kZTogZnVuY3Rpb24gKGNvdW50cnkpIHtcbiAgICAgICAgICAgIC8vX3RpdGxlID0gbmV3VGl0bGU7XG4gICAgICAgICAgICB2YXIgc2VvID0gJ1RyYWRlbWFyayBSZWdpc3RyYXRpb24gaW4gJyArIGNvdW50cnkgKyAnLlRyYWRlTWFya2VycyBQcm92aWRlcyBRdWljayBhbmQgRWFzeSBUcmFkZW1hcmsgRmlsaW5nLCBNb25pdG9yaW5nIGFuZCBTdHVkeSBTZXJ2aWNlcy4gRmlsZSBUb2RheSEnXG4gICAgICAgICAgICBjb25zb2xlLmxvZygnc2VvIGdvZXMgaGVyZScsIGNvdW50cnkpXG4gICAgICAgICAgICB2YXIgaW5mbyA9IGNvdW50cmllc1tjb3VudHJ5XVxuICAgICAgICAgICAgaWYgKGluZm8pIHtcblxuICAgICAgICAgICAgICAgIHZhciBhZ2VuY3kgPSBpbmZvLmFnZW5jeV9zaG9ydCA/IGluZm8uYWdlbmN5X3Nob3J0IDogaW5mby5hZ2VuY3lfbmFtZVxuXG4gICAgICAgICAgICAgICAgX2Rlc2MgPSBgVHJhZGVtYXJrIFJlZ2lzdHJhdGlvbiBpbiAke2luZm8ubmFtZX0uIFRyYWRlTWFya2VycyBQcm92aWRlcyBRdWljayBhbmQgRWFzeSBUcmFkZW1hcmsgRmlsaW5nLCBNb25pdG9yaW5nIGFuZCBTdHVkeSBTZXJ2aWNlcy4gRmlsZSB3aXRoICR7YWdlbmN5fSBUb2RheSFgXG4gICAgICAgICAgICAgICAgX2tleXdvcmRzID0gYFRyYWRlbWFyayBSZWdpc3RyYXRpb24gaW4gJHtpbmZvLm5hbWV9LCBUcmFkZW1hcmsgUmVnaXN0cmF0aW9uIGluICR7YWdlbmN5fSwgSW50ZXJuYXRpb25hbCBUcmFkZW1hcmsgUmVnaXN0cmF0aW9uLCBUcmFkZW1hcmsgRmlsaW5nIFdvcmxkd2lkZSwgVHJhZGVtYXJrIFJlZ2lzdHJhdGlvbiBHbG9iYWxseWBcblxuXG4gICAgICAgICAgICB9IGVsc2Uge1xuXG4gICAgICAgICAgICAgICAgX3RpdGxlID0gJ1RyYWRlbWFya2Vycy5jb20nO1xuICAgICAgICAgICAgICAgIF9rZXl3b3JkcyA9IFwiUmVnaXN0ZXIgVHJhZGVtYXJrc1wiXG4gICAgICAgICAgICAgICAgX2Rlc2MgPSBcIlJlZ2lzdGVyIFRyYWRlbWFya3NcIlxuXG4gICAgICAgICAgICB9XG5cblxuICAgICAgICB9XG5cbiAgICB9O1xuXG59XG4iLCIvLyBAdHMtY2hlY2tcblwidXNlIHN0cmljdFwiO1xuXG52YXIgaW5wdXRfZm9ybWF0cyA9IHJlcXVpcmUoXCIuLi8uLi8uLi9kYXRhL2Zvcm1hdHMuanNvblwiKVxuXG5jb25zb2xlLmxvZyhpbnB1dF9mb3JtYXRzKVxuXG5mb3IgKHZhciBrZXkgaW4gaW5wdXRfZm9ybWF0cykge1xuICBpZiAoaW5wdXRfZm9ybWF0cy5oYXNPd25Qcm9wZXJ0eShrZXkpKSB7XG4gICAgY29uc29sZS5sb2coa2V5ICsgXCIgLT4gXCIgKyBpbnB1dF9mb3JtYXRzW2tleV0pO1xuICAgIGlucHV0X2Zvcm1hdHNba2V5XS5QT0FfcmVxdWlyZWQgPSAtMVxuICB9XG59XG5cblxuZXhwb3J0cy5yZWdpc3Rlcl90cmFkZW1hcmsgPSBmdW5jdGlvbiAoJGh0dHAsICRxKSB7XG5cbiAgLy8gUE9BX3JlcXVpcmVkIDogLTEsIG5vdCBuZWVkZWQsICgwLDEsMi4uLiBuZWVkZWQsIGFuZCBkaWZmZXJlbnQgY29uZGl0aW9uIGZvciBkaWZmZXJlbnQgY291bnRyaWVzKVxuXG4gIHZhciB0bV9pdGVtID0ge3R5cGU6IFwiXCIsIHdvcmRtYXJrOiBcIlwiLCBsYXN0Nm1vbjogZmFsc2UsIFBPQTogMCwgY2xhc3NlczogW119IC8vIHcsIGwsIHdsLCB0bSAodHJhZGVtYXJrICksIHRtbCAodHJhZGVtYXJrIHdpdGggbG9nbylcbiAgdmFyIGlucHV0X2Zvcm1hdCA9IHtpbnB1dF90eXBlOiAxLCBQT0FfcmVxdWlyZWQ6IGZhbHNlLCB3b3JkX2xhYmVsOiAnVHJhZGVtYXJrJywgY291bnRyeTogJ1VTJ31cbiAgdmFyIHByaWNlID0gbnVsbFxuXG4gIHZhciBteVNlcnZpY2UgPSB7XG5cbiAgICBpc0ltcGxlbWVudGVkOiBmdW5jdGlvbiAoY291bnRyeSkge1xuICAgICAgcmV0dXJuIGlucHV0X2Zvcm1hdHNbY291bnRyeV0gIT09IHVuZGVmaW5lZFxuICAgIH0sXG5cbiAgICBzYXZlOiBmdW5jdGlvbiAoaXRlbSkge1xuICAgICAgdG1faXRlbSA9IGl0ZW1cbiAgICB9LFxuXG4gICAgc2F2ZV9wcm9maWxlIDogZnVuY3Rpb24ocHJvZmlsZSkge1xuICAgICAgdG1faXRlbS5wcm9maWxlID0gcHJvZmlsZVxuICAgIH0sXG5cbiAgICBnZXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgIGNvbnNvbGUubG9nKCdnZXQnLCB0bV9pdGVtKVxuICAgICAgcmV0dXJuIHRtX2l0ZW07XG4gICAgfSxcblxuICAgIGdldFByaWNlT2JqZWN0QnlUeXBlOiBmdW5jdGlvbiAodHlwZSkge1xuICAgICAgaWYgKHByaWNlKSB7XG4gICAgICAgIHZhciBwMiA9ICh0eXBlID09PSAnd2wnIHx8IHR5cGUgPT09ICd0bWwnKVxuICAgICAgICBpZiAocHJpY2UgaW5zdGFuY2VvZiBBcnJheSkge1xuICAgICAgICAgIGlmIChwMilcbiAgICAgICAgICAgIHJldHVybiBwcmljZVsxXVxuICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgIHJldHVybiBwcmljZVswXVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHJldHVybiBwcmljZVxuICAgICAgICB9XG5cbiAgICAgIH0gZWxzZVxuICAgICAgICByZXR1cm4gdW5kZWZpbmVkXG4gICAgfSxcblxuICAgIGdldFN0dWR5QW1vdW50OiBmdW5jdGlvbiAoKSB7XG4gICAgICB2YXIgcCA9IHRoaXMuZ2V0UHJpY2VPYmplY3RCeVR5cGUodG1faXRlbS50eXBlKVxuICAgICAgaWYgKHApIHtcbiAgICAgICAgdmFyIGFtdCA9IHAuc3R1ZHkxO1xuICAgICAgICBmb3IgKHZhciBqID0gMTsgaiA8IHRtX2l0ZW0uY2xhc3Nlcy5sZW5ndGg7IGorKykge1xuICAgICAgICAgIGFtdCA9IGFtdCArIHAuc3R1ZHkyXG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGFtdDtcbiAgICAgIH1cbiAgICAgIGVsc2VcbiAgICAgICAgcmV0dXJuIC0xO1xuICAgIH0sXG5cbiAgICBnZXRSZWdBbW91bnQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciBwID0gdGhpcy5nZXRQcmljZU9iamVjdEJ5VHlwZSh0bV9pdGVtLnR5cGUpXG5cbiAgICAgIGNvbnNvbGUubG9nKCdnZXRSZWdBbW91bnQnLCBwKVxuICAgICAgY29uc29sZS5sb2coJ2l0ZW0nLCB0bV9pdGVtKVxuICAgICAgaWYgKHApIHtcbiAgICAgICAgdmFyIGFtdCA9IHAucmVnMTtcbiAgICAgICAgaWYgKCFwLm1jKSB7XG4gICAgICAgICAgZm9yICh2YXIgaiA9IDE7IGogPCB0bV9pdGVtLmNsYXNzZXMubGVuZ3RoOyBqKyspIHtcbiAgICAgICAgICAgIGFtdCA9IGFtdCArIHAucmVnMlxuICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHsgIC8vIG11bHRpcGxlIGNsYXNzZXMgaW4gdGhlIHByaWNlIDFcbiAgICAgICAgICBmb3IgKHZhciBrID0gcC5tY19udW07IGsgPCB0bV9pdGVtLmNsYXNzZXMubGVuZ3RoOyBrKyspIHtcbiAgICAgICAgICAgIGFtdCA9IGFtdCArIHAucmVnMlxuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGlmICh0bV9pdGVtLmNsYWltX3ByaW9yaXR5ICYmIHAucHJpb3JpdHlfZmlsaW5nKVxuXG4gICAgICAgICAgYW10ID0gYW10ICsgcC5wcmlvcml0eV9maWxpbmdcblxuICAgICAgICByZXR1cm4gYW10O1xuICAgICAgfVxuICAgICAgZWxzZVxuICAgICAgICByZXR1cm4gLTE7XG4gICAgfSxcblxuICAgIGdldEFtb3VudDogZnVuY3Rpb24gKGNvdW50cnkpIHtcbiAgICAgIGNvbnNvbGUubG9nKCdnZXRfYW1vdW50JywgdG1faXRlbSlcbiAgICAgIGlmICh0bV9pdGVtLmNsYXNzZXMubGVuZ3RoID09PSAwKVxuICAgICAgICByZXR1cm4gMFxuICAgICAgZWxzZSB7XG5cbiAgICAgICAgdmFyIGNvdW50cnlfcHJpY2UsIHVuaXRfcHJpY2UsIGFtdFxuXG4gICAgICAgIGlmICh0bV9pdGVtLnNlcnZpY2UgPT09ICdyZWdpc3RyYXRpb24nKSB7XG4gICAgICAgICAgcmV0dXJuIHRoaXMuZ2V0UmVnQW1vdW50KClcblxuICAgICAgICAgIC8qXG4gICAgICAgICAgIGlmIChbXCJFVVwiLCBcIkNIXCIsIFwiTk9cIl0uaW5kZXhPZihjb3VudHJ5KSAhPT0gLTEpIHtcbiAgICAgICAgICAgY291bnRyeV9wcmljZSA9IHByaWNlc1tjb3VudHJ5XVxuICAgICAgICAgICB1bml0X3ByaWNlID0gY291bnRyeV9wcmljZVt0bV9pdGVtLnR5cGVdXG4gICAgICAgICAgIGFtdCA9IHVuaXRfcHJpY2UucHJpY2UxXG4gICAgICAgICAgIGZvciAodmFyIGkgPSAxOyBpIDwgdG1faXRlbS5jbGFzc2VzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgIGFtdCA9IGFtdCArIHVuaXRfcHJpY2UucHJpY2UyXG4gICAgICAgICAgIH1cbiAgICAgICAgICAgaWYgKGFtdCA9PT0gdW5kZWZpbmVkKVxuICAgICAgICAgICBhbXQgPSAwXG4gICAgICAgICAgIHJldHVybiBhbXRcbiAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgcmV0dXJuIHRoaXMuZ2V0UmVnQW1vdW50KClcbiAgICAgICAgICAgfSovXG4gICAgICAgIH0gZWxzZSBpZiAodG1faXRlbS5zZXJ2aWNlID09PSAnc3R1ZHknKSB7XG4gICAgICAgICAgcmV0dXJuIHRoaXMuZ2V0U3R1ZHlBbW91bnQoKVxuICAgICAgICB9IGVsc2VcbiAgICAgICAgICByZXR1cm4gMDtcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgaW5pdF9pdGVtOiBmdW5jdGlvbiAoc2VydmljZSkge1xuXG4gICAgICBjb25zb2xlLmxvZygnaW5pdGluZ2dnJywgc2VydmljZSlcblxuICAgICAgaWYgKHNlcnZpY2UgPT09ICdyZWdpc3RyYXRpb24nKVxuICAgICAgICB0bV9pdGVtID0ge1xuICAgICAgICAgIHNlcnZpY2U6IHNlcnZpY2UsIHR5cGU6IFwiXCIsIHdvcmRtYXJrOiBcIlwiLCBsYXN0Nm1vbjogZmFsc2UsIFBPQTogaW5wdXRfZm9ybWF0LlBPQV9yZXF1aXJlZCxcbiAgICAgICAgICBjbGFzc2VzOiBbXSwgY29tbWVyY2VfdXNlOiBpbnB1dF9mb3JtYXQuY29tbWVyY2VfdXNlLCBzZWxlY3RlZDogdHJ1ZVxuICAgICAgICB9XG4gICAgICBlbHNlIGlmIChzZXJ2aWNlID09PSAnc3R1ZHknKSB7XG4gICAgICAgIHRtX2l0ZW0gPSB7XG4gICAgICAgICAgc2VydmljZTogc2VydmljZSwgdHlwZTogXCJcIiwgd29yZG1hcms6IFwiXCIsIGNsYXNzZXM6IFtdLCBzdHVkeV9zdW1tYXJ5OiBcIlwiLCBzdHVkeV90eXBlOiBpbnB1dF9mb3JtYXQuc3R1ZHlfdHlwZSwgc2VsZWN0ZWQ6IHRydWVcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICB0bV9pdGVtLnByb2ZpbGUgPSB7fSAgLy8gdGhpcyB3aWxsIGJlIHBvcHVsYXRlZCBsYXRlclxuXG4gICAgICBjb25zb2xlLmxvZyh0bV9pdGVtKVxuICAgIH0sXG5cbiAgICAvLyBub3QgdXNpbmcgYW55IGludGVybmFsIHZhcmlhYmxlIHRvIGluaXRcbiAgICBpbml0X2l0ZW1fZXggOiBmdW5jdGlvbihzZXJ2aWNlLCBjb3VudHJ5KSB7XG5cbiAgICAgIHZhciByZXRfaXRlbSA9IHt9XG4gICAgICB2YXIgaW5wdXRfZm9ybWF0MiA9IGlucHV0X2Zvcm1hdHNbY291bnRyeV1cblxuXG5cbiAgICAgIGlmIChzZXJ2aWNlID09PSAncmVnaXN0cmF0aW9uJylcbiAgICAgICAgcmV0X2l0ZW0gPSB7XG4gICAgICAgICAgc2VydmljZTogc2VydmljZSwgdHlwZTogXCJcIiwgd29yZG1hcms6IFwiXCIsIGxhc3Q2bW9uOiBmYWxzZSwgUE9BOiBpbnB1dF9mb3JtYXQyLlBPQV9yZXF1aXJlZCxcbiAgICAgICAgICBjbGFzc2VzOiBbXSwgY29tbWVyY2VfdXNlOiBpbnB1dF9mb3JtYXQyLmNvbW1lcmNlX3VzZSwgc2VsZWN0ZWQ6IHRydWVcbiAgICAgICAgfVxuICAgICAgZWxzZSBpZiAoc2VydmljZSA9PT0gJ3N0dWR5Jykge1xuICAgICAgICByZXRfaXRlbSA9IHtcbiAgICAgICAgICBzZXJ2aWNlOiBzZXJ2aWNlLCB0eXBlOiBcIlwiLCB3b3JkbWFyazogXCJcIiwgY2xhc3NlczogW10sIHN0dWR5X3N1bW1hcnk6IFwiXCIsIHN0dWR5X3R5cGU6IGlucHV0X2Zvcm1hdDIuc3R1ZHlfdHlwZSwgc2VsZWN0ZWQ6IHRydWVcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICByZXRfaXRlbS5wcm9maWxlID0ge30gIC8vIHRoaXMgd2lsbCBiZSBwb3B1bGF0ZWQgbGF0ZXJcblxuICAgICAgcmV0dXJuIHJldF9pdGVtXG4gICAgfSxcblxuXG4gICAgZ2V0X2NvdW50cnlfaW5mbzogZnVuY3Rpb24gKGNvdW50cnkpIHtcblxuICAgICAgdmFyIGRlZmVycmVkID0gJHEuZGVmZXIoKTtcblxuICAgICAgJGh0dHAuZ2V0KCcvYXBpX2luZm8vZ2V0X2NvdW50cnlfaW5mby8nICsgY291bnRyeSkuc3VjY2VzcyhmdW5jdGlvbiAoY291bnRyeV9pbmZvKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdnZXQgY291bnRyeSBpbmZvIHNlcnZpY2UnLCBjb3VudHJ5X2luZm8pXG4gICAgICAgIGRlZmVycmVkLnJlc29sdmUoY291bnRyeV9pbmZvKTtcbiAgICAgIH0pLmVycm9yKGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgICBjb25zb2xlLmxvZygnZXJyb3IgaW4gZ2V0X2NvdW50cnlfaW5mbycpXG4gICAgICB9KTtcblxuICAgICAgcmV0dXJuIGRlZmVycmVkLnByb21pc2U7XG5cbiAgICB9LFxuXG5cbiAgICAvLyBzZXQncyBmb3JtYXQgYW5kIHByaWNlcyBmb3IgdGhlIHBhcnRpY3VsYXIgY291bnRyeVxuICAgIC8vIHByaWNlIGlzIHJldHVybmVkIGFzIHdlbGwgYXMgcHJvbWlzZSwgdGhpcyBjYW4gYmUgdXNlZCBpbiBuZ1JvdXRlJ3MgcmVzb2x2ZVxuXG4gICAgc2V0X2lucHV0X2Zvcm1hdDogZnVuY3Rpb24gKGNvdW50cnkpIHtcbiAgICAgIGlucHV0X2Zvcm1hdCA9IGlucHV0X2Zvcm1hdHNbY291bnRyeV1cbiAgICB9LFxuXG4gICAgc2V0X3ByaWNlOiBmdW5jdGlvbiAoY291bnRyeSkge1xuXG4gICAgICB2YXIgZGVmZXJyZWQgPSAkcS5kZWZlcigpO1xuICAgICAgJGh0dHAuZ2V0KCcvYXBpL2dldF9wcmljZS8nICsgY291bnRyeSkuc3VjY2VzcyhmdW5jdGlvbiAoZGF0YSkgeyAgLy8gY3VzdG9taXplZCBwcmljZSB3aWxsIGJlIHVzZWQgaWYgdXNlciBsb2dnZWQgaW5cbiAgICAgICAgY29uc29sZS5sb2coJ2dldCBwcmljZSBpbiAnLCBkYXRhKVxuICAgICAgICBwcmljZSA9IGRhdGFcbiAgICAgICAgZGVmZXJyZWQucmVzb2x2ZShkYXRhKVxuICAgICAgfSkuZXJyb3IoZnVuY3Rpb24gKGVycm9yKSB7XG4gICAgICAgIHByaWNlID0gbnVsbFxuICAgICAgfSk7XG4gICAgICByZXR1cm4gZGVmZXJyZWQucHJvbWlzZTtcbiAgICB9LFxuXG5cbiAgICBnZXRfaW5wdXRfZm9ybWF0OiBmdW5jdGlvbiAoKSB7XG4gICAgICByZXR1cm4gaW5wdXRfZm9ybWF0XG4gICAgfVxuXG4gICAgLyogbW92ZWQgdG8gd2ViX3NlcnZpY2VzX2NhcnRcblxuICAgIHBvc3RfdG9fc2VydmVyOiBmdW5jdGlvbiAoKSB7XG5cbiAgICAgIHZhciBkZWZlcnJlZCA9ICRxLmRlZmVyKCk7XG5cbiAgICAgICRodHRwLnBvc3QoJy9hcGkvcG9zdF90cmFkZW1hcmsnLCB0bV9pdGVtKS5cbiAgICAgICAgc3VjY2VzcyhmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgIG15U2VydmljZS5pbml0X2l0ZW0oKVxuICAgICAgICAgIGRlZmVycmVkLnJlc29sdmUoe3JlZ2lzdGVyZWQ6IGRhdGEuc3RhdHVzLCBtZXNzYWdlOiBkYXRhLm1lc3NhZ2V9KTtcbiAgICAgICAgfSkuXG4gICAgICAgIGVycm9yKGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgICAgIGRlZmVycmVkLnJlc29sdmUoe3JlZ2lzdGVyZWQ6IGZhbHNlLCBtZXNzYWdlOiAnU29ycnksIGVycm9yIG9jY3VycmVkIHdoaWxlIHBvc3RpbmcsIHBsZWFzZSB0cnkgYWdhaW4uJ30pO1xuICAgICAgICB9KTtcblxuICAgICAgcmV0dXJuIGRlZmVycmVkLnByb21pc2U7XG5cbiAgICB9Ki9cblxuICB9XG5cbiAgcmV0dXJuIG15U2VydmljZVxuXG59XG5cbiIsIi8vIEB0cy1jaGVja1xuXCJ1c2Ugc3RyaWN0XCI7XG5cbnZhciBjb3VudHJpZXMgPSByZXF1aXJlKCdjb3VudHJpZXMnKVxuXG5cbmZ1bmN0aW9uIGdldF9jb3VudHJ5X2luZm9fcHJvYygkaHR0cCwgJHEsIGNvdW50cnlfY29kZSwgbG9jYWxlKSB7XG5cbiAgICB2YXIgZGVmZXJyZWQgPSAkcS5kZWZlcigpO1xuICAgIHZhciBjb3VudHJ5ID0gY291bnRyaWVzLmdldF9jb3VudHJ5KGNvdW50cnlfY29kZSlcblxuICAgIGNvbnNvbGUubG9nKCdnZXRfY291bnRyeV9pbmZvMTIzNDUgZm9yICcsIGNvdW50cnlfY29kZSlcbiAgICAkaHR0cC5nZXQoJy9hcGlfaW5mby9nZXRfY291bnRyeV9pbmZvLycgKyBjb3VudHJ5X2NvZGUgKyc/bG9jYWxlPScgKyBsb2NhbGUpLnN1Y2Nlc3MoZnVuY3Rpb24gKGNvdW50cnlfaW5mbykge1xuICAgICAgICBjb25zb2xlLmxvZygnZ2V0IGNvdW50cnkgaW5mbyBzZXJ2aWNlJywgY291bnRyeV9pbmZvKVxuICAgICAgICBjb3VudHJ5X2luZm8uY291bnRyeSA9IGNvdW50cnlcbiAgICAgICAgZGVmZXJyZWQucmVzb2x2ZShjb3VudHJ5X2luZm8pO1xuICAgIH0pLmVycm9yKGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgICBjb25zb2xlLmxvZygnZXJyb3IgaW4gZ2V0X2NvdW50cnlfaW5mbycpXG4gICAgICAgIGRlZmVycmVkLnJlamVjdCh7bXNnOidlcnJvciBpbiBnZXRfY291bnRyeV9pbmZvJ30pXG4gICAgfSk7XG5cbiAgICByZXR1cm4gZGVmZXJyZWQucHJvbWlzZTtcbn1cblxuXG5leHBvcnRzLmdldF9jb3VudHJ5X2luZm8gPSBmdW5jdGlvbiAoJHJvdXRlLCAkaHR0cCwgJHEsICR0cmFuc2xhdGUpIHtcbiAgICByZXR1cm4gZ2V0X2NvdW50cnlfaW5mb19wcm9jKCRodHRwLCAkcSwgJHJvdXRlLmN1cnJlbnQucGFyYW1zLmNvdW50cnksICR0cmFuc2xhdGUudXNlKCkpXG59XG5cbi8vIGdldCBjb3VudHJ5IGluZm8gZGlyZWN0bHkgZnJvbSBwYXRoLCBleCB0cmFkZW1hcmstcmVnaXN0cmF0aW9uLWluLXR1cm5rZXlcblxuZXhwb3J0cy5nZXRfY291bnRyeV9pbmZvX1NFTyA9IGZ1bmN0aW9uICgkcm91dGUsICRodHRwLCAkcSwgJHRyYW5zbGF0ZSkge1xuXG4gICAgdmFyIGNvdW50cnkgPSBjb3VudHJpZXMuZ2V0X2NvdW50cnlfYnlfcmVnZXgoL15cXC90cmFkZW1hcmstcmVnaXN0cmF0aW9uLWluLShcXHcrKS9pLCAkcm91dGUuY3VycmVudC5vcmlnaW5hbFBhdGgpXG4gICAgaWYgKGNvdW50cnkpIHtcbiAgICAgICAgcmV0dXJuIGdldF9jb3VudHJ5X2luZm9fcHJvYygkaHR0cCwgJHEsIGNvdW50cnkuY29kZSwgJHRyYW5zbGF0ZS51c2UoKSApXG4gICAgfVxuICAgIC8qXG4gICAgdmFyIGRlZmVycmVkID0gJHEuZGVmZXIoKTtcblxuICAgIHZhciBjb3VudHJ5XG5cbiAgICBjb3VudHJ5ID0gY291bnRyaWVzLmdldF9jb3VudHJ5X2J5X3JlZ2V4KC9eXFwvdHJhZGVtYXJrLXJlZ2lzdHJhdGlvbi1pbi0oXFx3KykvaSwgJHJvdXRlLmN1cnJlbnQub3JpZ2luYWxQYXRoKVxuXG4gICAgaWYgKGNvdW50cnkpIHtcblxuICAgICAgICBjb25zb2xlLmxvZygnY291bnRyeSBjb2RlJywgY291bnRyeS5jb2RlKVxuICAgICAgICAkaHR0cC5nZXQoJy9hcGlfaW5mby9nZXRfY291bnRyeV9pbmZvLycgKyBjb3VudHJ5LmNvZGUpLnN1Y2Nlc3MoZnVuY3Rpb24gKGNvdW50cnlfaW5mbykge1xuICAgICAgICAgICAgY29uc29sZS5sb2coJ2dldCBjb3VudHJ5IGluZm8gc2VydmljZSBzZW8nLCBjb3VudHJ5X2luZm8pXG4gICAgICAgICAgICBjb3VudHJ5X2luZm8uY291bnRyeSA9IGNvdW50cnlcbiAgICAgICAgICAgIGRlZmVycmVkLnJlc29sdmUoY291bnRyeV9pbmZvKTtcbiAgICAgICAgfSkuZXJyb3IoZnVuY3Rpb24gKGVycm9yKSB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZygnZXJyb3IgaW4gZ2V0X2NvdW50cnlfaW5mbycpXG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHJldHVybiBkZWZlcnJlZC5wcm9taXNlO1xuKi9cbn1cblxuZXhwb3J0cy5iNHJlZ2lzdHJ5MiA9IGZ1bmN0aW9uICgkcm91dGUsICRxLCByZWdpc3Rlcl90cmFkZW1hcmspIHtcblxuICAgIHZhciBjb3VudHJ5ID0gJHJvdXRlLmN1cnJlbnQucGFyYW1zLmNvdW50cnlcblxuICAgIHJlZ2lzdGVyX3RyYWRlbWFyay5zZXRfaW5wdXRfZm9ybWF0KGNvdW50cnkpXG4gICAgdmFyIHByaWNlID0gcmVnaXN0ZXJfdHJhZGVtYXJrLnNldF9wcmljZShjb3VudHJ5KVxuICAgIHZhciBmb3JtYXQgPSByZWdpc3Rlcl90cmFkZW1hcmsuc2V0X3ByaWNlKGNvdW50cnkpXG5cbiAgICByZXR1cm4gJHEuYWxsKFtwcmljZSwgcmVnaXN0ZXJfdHJhZGVtYXJrLmdldF9pbnB1dF9mb3JtYXQoKV0pLnRoZW4oZnVuY3Rpb24gKHJlc3VsdHMpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIHByaWNlOiByZXN1bHRzWzBdLFxuICAgICAgICAgICAgaW5wdXRfZm9ybWF0OiByZXN1bHRzWzFdLFxuICAgICAgICAgICAgbGluZTogcmVnaXN0ZXJfdHJhZGVtYXJrLmdldCgpIC8vIGxpbmUgYnVmZmVyIGZvciB0aGUgdHJhbnNhY3Rpb25cbiAgICAgICAgfVxuICAgIH0pXG5cbn1cblxuZXhwb3J0cy5iNHJlZ2lzdHJ5NSA9IGZ1bmN0aW9uICgkcm91dGUsICRxLCByZWdpc3Rlcl90cmFkZW1hcmssIENhcnQpIHtcblxuICAgIHZhciBjb3VudHJ5ID0gJHJvdXRlLmN1cnJlbnQucGFyYW1zLmNvdW50cnlcbiAgICB2YXIgcHJvZmlsZV9pZCA9IHJlZ2lzdGVyX3RyYWRlbWFyay5nZXQoKS5wcm9maWxlX2lkIC8vIG1pZ2h0IGJlIHVuZGVmaW5lZFxuXG4gICAgcmV0dXJuICRxLmFsbChbQ2FydC5nZXRQcm9maWxlKHByb2ZpbGVfaWQpXSkudGhlbihmdW5jdGlvbiAocmVzdWx0cykge1xuXG4gICAgICAgIGNvbnNvbGUubG9nKCdiNCByZWc1IG9yIHN0dWR5MycsIHJlc3VsdHMpXG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBsaW5lOiByZWdpc3Rlcl90cmFkZW1hcmsuZ2V0KCksIC8vIGxpbmUgYnVmZmVyIGZvciB0aGUgdHJhbnNhY3Rpb25cbiAgICAgICAgICAgIHByb2ZpbGU6IHJlc3VsdHNbMF1cbiAgICAgICAgfVxuICAgIH0pXG5cbn1cblxuXG5leHBvcnRzLmI0dHJhZGVtYXJrX2NvbnRhY3QgPSBmdW5jdGlvbiAoJHJvdXRlLCAkcSwgcmVnaXN0ZXJfdHJhZGVtYXJrLCBDYXJ0KSB7XG5cbiAgICB2YXIgY291bnRyeSA9ICRyb3V0ZS5jdXJyZW50LnBhcmFtcy5jb3VudHJ5XG5cbiAgICByZXR1cm4gJHEuYWxsKFtyZWdpc3Rlcl90cmFkZW1hcmsuZ2V0KCksIENhcnQuZ2V0UHJvZmlsZXMoKV0pLnRoZW4oZnVuY3Rpb24gKHJlc3VsdHMpIHtcbiAgICAgICAgY29uc29sZS5sb2cocmVzdWx0cy5sZW5ndGgpXG4gICAgICAgIGNvbnNvbGUubG9nKCdiNHRycmFkZScsIHJlc3VsdHMpXG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBsaW5lOiByZXN1bHRzWzBdLCAvLyBsaW5lIGJ1ZmZlciBmb3IgdGhlIHRyYW5zYWN0aW9uXG4gICAgICAgICAgICBwcm9maWxlczogcmVzdWx0c1sxXVxuICAgICAgICAgICAgLypcbiAgICAgICAgICAgICBwcm9maWxlcyA6IHtcbiAgICAgICAgICAgICAnMTIzJzogJ0phY2snLFxuICAgICAgICAgICAgICdhYmMnOiAnRGljaydcbiAgICAgICAgICAgICB9Ki9cbiAgICAgICAgfVxuICAgIH0pXG5cbn1cblxuXG5leHBvcnRzLmI0dHJhZGVtYXJrX2FkZHJlc3MgPSBmdW5jdGlvbiAoJHJvdXRlLCAkcSwgcmVnaXN0ZXJfdHJhZGVtYXJrLCBDYXJ0KSB7XG5cbiAgICB2YXIgY291bnRyeSA9ICRyb3V0ZS5jdXJyZW50LnBhcmFtcy5jb3VudHJ5XG4gICAgdmFyIHJlZyA9IHJlZ2lzdGVyX3RyYWRlbWFyay5nZXQoKVxuXG5cbiAgICBjb25zb2xlLmxvZygncmVnIGluIGI0dHJhZGVtYXJrX2FkZHJlc3MnLCByZWcpXG5cbiAgICAvLyB2YXIgYmFja190byA9IHJlZy5zZXJ2aWNlID09PSAnc3R1ZHknID8gJy9zdHVkeTIvJyA6ICcvcmVnaXN0ZXIzLydcbiAgICB2YXIgYmFja190byA9ICcvdHJhZGVtYXJrX2NvbnRhY3QvJ1xuICAgIHZhciBuZXh0X3RvID0gcmVnLnNlcnZpY2UgPT09ICdzdHVkeScgPyAnL3N0dWR5My8nIDogJy9yZWdpc3RlcjUvJ1xuXG5cbiAgICByZXR1cm4gJHEuYWxsKFtDYXJ0LmdldFByb2ZpbGVzKCldKS50aGVuKGZ1bmN0aW9uIChyZXN1bHRzKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBsaW5lOiByZWcsIC8vIGxpbmUgYnVmZmVyIGZvciB0aGUgdHJhbnNhY3Rpb25cbiAgICAgICAgICAgIHByb2ZpbGU6IHJlZy5wcm9maWxlLFxuICAgICAgICAgICAgcHJvZmlsZXM6IHJlc3VsdHNbMF0sXG4gICAgICAgICAgICBiYWNrX3BhdGg6IGJhY2tfdG8gKyBjb3VudHJ5LFxuICAgICAgICAgICAgbmV4dF9wYXRoOiBuZXh0X3RvICsgY291bnRyeSxcbiAgICAgICAgICAgIC8qXG4gICAgICAgICAgICAgcHJvZmlsZXMgOiB7XG4gICAgICAgICAgICAgJzEyMyc6ICdKYWNrJyxcbiAgICAgICAgICAgICAnYWJjJzogJ0RpY2snXG4gICAgICAgICAgICAgfSovXG4gICAgICAgIH1cbiAgICB9KVxuXG59XG5cblxuZXhwb3J0cy5iNGFjY291bnRfYWRkcmVzcyA9IGZ1bmN0aW9uICgkcm91dGUsICRxLCAkbG9jYXRpb24sIHVzZXJfYWNjb3VudCkge1xuXG4gICAgdmFyIGJhY2tfdG8gPSAnL2FjY291bnQvJ1xuICAgIHZhciBuZXh0X3RvID0gJ25leHQnIC8vIHRoaXMgd2lsbCBub3QgYmUgdXNlZCBhcyBuZXh0X3Byb2MgaXMgcHJvdmlkZWQgYmVsb3dcblxuXG4gICAgcmV0dXJuICRxLmFsbChbdXNlcl9hY2NvdW50LmdldCgpXSkudGhlbihmdW5jdGlvbiAocmVzdWx0cykge1xuICAgICAgICBjb25zb2xlLmxvZygncmVzdWx0cycsIHJlc3VsdHMpXG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBwcm9maWxlOiByZXN1bHRzWzBdLFxuICAgICAgICAgICAgYmFja19wYXRoOiBiYWNrX3RvLFxuICAgICAgICAgICAgbmV4dF9wYXRoOiBuZXh0X3RvLFxuICAgICAgICAgICAgbmV4dF9wcm9tcHQ6ICdTYXZlJyxcbiAgICAgICAgICAgIHNpZGVfcGFuZWxfYWNjb3VudDogdHJ1ZSxcbiAgICAgICAgICAgIG5leHRfcHJvYzogZnVuY3Rpb24gKGFjY291bnQpIHtcbiAgICAgICAgICAgICAgICB1c2VyX2FjY291bnQucG9zdChhY2NvdW50KS50aGVuKGZ1bmN0aW9uIChzdGF0dXMpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coc3RhdHVzKVxuICAgICAgICAgICAgICAgICAgICAkbG9jYXRpb24ucGF0aCgnL2FjY291bnQvJylcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfSlcblxufVxuXG5cbmV4cG9ydHMuYjRwcm9maWxlX2FkZHJlc3MgPSBmdW5jdGlvbiAoJHJvdXRlLCAkcSwgJGxvY2F0aW9uLCB1c2VyX3Byb2ZpbGUpIHtcblxuICAgIHZhciBiYWNrX3RvID0gJy9wcm9maWxlLycgKyAkcm91dGUuY3VycmVudC5wYXJhbXMuaWRcblxuXG4gICAgcmV0dXJuICRxLmFsbChbdXNlcl9wcm9maWxlLmdldCgkcm91dGUuY3VycmVudC5wYXJhbXMuaWQpXSkudGhlbihmdW5jdGlvbiAocmVzdWx0cykge1xuICAgICAgICBjb25zb2xlLmxvZygncmVzdWx0cyA0IHByb2ZpbGUnLCByZXN1bHRzKVxuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgcHJvZmlsZTogcmVzdWx0c1swXSxcbiAgICAgICAgICAgIGJhY2tfcGF0aDogYmFja190byxcbiAgICAgICAgICAgIG5leHRfcGF0aDogJycsXG4gICAgICAgICAgICBuZXh0X3Byb21wdDogJ1NhdmUnLFxuICAgICAgICAgICAgc2lkZV9wYW5lbF9hY2NvdW50OiB0cnVlLFxuICAgICAgICAgICAgbmV4dF9wcm9jOiBmdW5jdGlvbiAoYWNjb3VudCkge1xuICAgICAgICAgICAgICAgIHVzZXJfcHJvZmlsZS5wb3N0KGFjY291bnQpLnRoZW4oZnVuY3Rpb24gKHN0YXR1cykge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhzdGF0dXMpXG4gICAgICAgICAgICAgICAgICAgICRsb2NhdGlvbi5wYXRoKGJhY2tfdG8pXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH0pXG5cbn1cblxuXG5leHBvcnRzLmI0cHJvZmlsZSA9IGZ1bmN0aW9uICgkcm91dGUsICRxLCAkbG9jYXRpb24sIHVzZXJfcHJvZmlsZSkge1xuXG4gICAgcmV0dXJuICRxLmFsbChbdXNlcl9wcm9maWxlLmdldCgkcm91dGUuY3VycmVudC5wYXJhbXMuaWQpXSkudGhlbihmdW5jdGlvbiAocmVzdWx0cykge1xuICAgICAgICBjb25zb2xlLmxvZygncmVzdWx0cyA0IHByb2ZpbGUnLCByZXN1bHRzKVxuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgcHJvZmlsZTogcmVzdWx0c1swXVxuICAgICAgICB9XG4gICAgfSlcblxufVxuIiwiXCJ1c2Ugc3RyaWN0XCI7XG4vKmdsb2JhbCBhbmd1bGFyLCBjb25zb2xlICovXG5cblxuYW5ndWxhci5tb2R1bGUoJ3Rrcy5zdHVkeS5jb250cm9sbGVycycsIFtdKVxuXG4gIC8vIHN0YXJ0aW5nIG9mIHN0dWR5IHByb2Nlc3NcblxuICAuY29udHJvbGxlcignc3R1ZHkxJywgWyckc2NvcGUnLCAnJGh0dHAnLCAnJGxvY2F0aW9uJywgJyRyb3V0ZVBhcmFtcycsICdyZWdpc3Rlcl90cmFkZW1hcmsnLFxuICAgIGZ1bmN0aW9uICgkc2NvcGUsICRodHRwLCAkbG9jYXRpb24sICRyb3V0ZVBhcmFtcywgcmVnaXN0ZXJfdHJhZGVtYXJrKSB7XG5cblxuXG4gICAgICAkc2NvcGUuY291bnRyeSA9ICRyb3V0ZVBhcmFtcy5jb3VudHJ5XG5cblxuXG4gICAgICAkc2NvcGUuc3RhcnRfc3R1ZHkgPSBmdW5jdGlvbiAoY291bnRyeSkge1xuXG4gICAgICAgIGNvbnNvbGUubG9nKCdzdHVkeSAnLCBjb3VudHJ5KVxuICAgICAgICByZWdpc3Rlcl90cmFkZW1hcmsuc2V0X2lucHV0X2Zvcm1hdChjb3VudHJ5KVxuICAgICAgICByZWdpc3Rlcl90cmFkZW1hcmsuaW5pdF9pdGVtKCdzdHVkeScpXG4gICAgICAgICRsb2NhdGlvbi5wYXRoKCcvc3R1ZHkyLycgKyBjb3VudHJ5KVxuICAgICAgfVxuXG5cbiAgICB9XSlcblxuICAuY29udHJvbGxlcignc3R1ZHkzJywgWyckc2NvcGUnLCAnJGh0dHAnLCAnJGxvY2F0aW9uJywgJyRyb290U2NvcGUnLCAnJHJvdXRlUGFyYW1zJywgJ3JlZ2lzdGVyX3RyYWRlbWFyaycsICdBVVRIX0VWRU5UUycsICdDYXJ0JywgJ3JlZ2lzdHJ5X2luaXREYXRhJyxcbiAgICBmdW5jdGlvbiAoJHNjb3BlLCAkaHR0cCwgJGxvY2F0aW9uLCAkcm9vdFNjb3BlLCAkcm91dGVQYXJhbXMsIHJlZ2lzdGVyX3RyYWRlbWFyaywgQVVUSF9FVkVOVFMsIENhcnQsIHJlZ2lzdHJ5X2luaXREYXRhKSB7XG5cbiAgICAgICRzY29wZS5jb3VudHJ5ID0gJHJvdXRlUGFyYW1zLmNvdW50cnlcblxuICAgICAgJHNjb3BlLnJlZyA9IHJlZ2lzdGVyX3RyYWRlbWFyay5nZXQoKVxuXG4gICAgICBjb25zb2xlLmxvZygnc3R1ZHkzJywgcmVnaXN0cnlfaW5pdERhdGEpXG5cblxuXG4gICAgICBpZiAocmVnaXN0cnlfaW5pdERhdGEucHJvZmlsZSlcbiAgICAgICAgJHNjb3BlLnByb2ZpbGUgPSByZWdpc3RyeV9pbml0RGF0YS5wcm9maWxlXG4gICAgICBlbHNlXG4gICAgICAgICRzY29wZS5wcm9maWxlID0gJHNjb3BlLnJlZy5wcm9maWxlICAgICAvLyBubyBwcm9maWxlX2lkLCB1c2UgZW1iZWRkZWQgcHJvZmlsZVxuXG5cbiAgICAgICRzY29wZS5iYWNrID0gZnVuY3Rpb24gKHJlZykge1xuICAgICAgICByZWcuY291bnRyeSA9ICRyb3V0ZVBhcmFtcy5jb3VudHJ5XG4gICAgICAgIC8vICAgICAgICAkbG9jYXRpb24ucGF0aCgnL3N0dWR5Mi8nICsgJHNjb3BlLmNvdW50cnkpXG4gICAgICAgIGlmIChyZWcucHJvZmlsZV9pZClcbiAgICAgICAgICAkbG9jYXRpb24ucGF0aCgnL3RyYWRlbWFya19jb250YWN0LycgKyAkcm91dGVQYXJhbXMuY291bnRyeSlcbiAgICAgICAgZWxzZVxuICAgICAgICAgICRsb2NhdGlvbi5wYXRoKCcvdHJhZGVtYXJrX2FkZHJlc3MvJyArICRyb3V0ZVBhcmFtcy5jb3VudHJ5KVxuICAgICAgfVxuXG4gICAgICAkc2NvcGUuc3VibWl0ID0gZnVuY3Rpb24gKHJlZykge1xuICAgICAgICByZWdpc3Rlcl90cmFkZW1hcmsuc2F2ZShyZWcpXG5cbiAgICAgICAgQ2FydC5wb3N0X3RvX3NlcnZlcihyZWdpc3Rlcl90cmFkZW1hcmsuZ2V0KCkpLnRoZW4oZnVuY3Rpb24gKHN0YXR1cykge1xuICAgICAgICAgIGNvbnNvbGUubG9nKCdjYXJ0IHBvc3QgdG8gc2VydmVyIHN0YXR1cycpXG4gICAgICAgICAgJHJvb3RTY29wZS4kYnJvYWRjYXN0KEFVVEhfRVZFTlRTLmNhcnRVcGRhdGVkKTtcblxuXG4gICAgICAgICAgQ2FydC5nZXRFeGlzdGluZ0VtYWlscygpLnRoZW4oZnVuY3Rpb24gKGVtYWlscykge1xuICAgICAgICAgICAgaWYgKGVtYWlscy5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgICRsb2NhdGlvbi5wYXRoKCcvYmVmb3JlX2NoZWNrX291dC8nICsgQ2FydC5nZXRSZW1vdGVDYXJ0KCkuX2lkKVxuICAgICAgICAgICAgfSBlbHNlXG4gICAgICAgICAgICAgICRsb2NhdGlvbi5wYXRoKCcvc2hvcHBpbmdfY2FydCcpXG4gICAgICAgICAgfSlcbiAgICAgICAgfSlcblxuICAgICAgfVxuXG4gICAgfV0pXG5cbiIsIm1vZHVsZS5leHBvcnRzPXtcbiAgXCJCV1wiOlwiQm90c3dhbmFcIixcbiAgXCJMU1wiOlwiTGVzb3Rob1wiLFxuICBcIkxSXCI6XCJMaWJlcmlhXCIsXG4gIFwiTVdcIjpcIk1hbGF3aVwiLFxuICBcIk5BXCI6XCJOYW1pYmlhXCIsXG4gIFwiU1pcIjpcIlN3YXppbGFuZFwiLFxuICBcIlRaXCI6XCJUYW56YW5pYVwiLFxuICBcIlVHXCI6XCJVZ2FuZGFcIixcbiAgXCJaV1wiOlwiWmltYmFid2VcIlxufSIsIm1vZHVsZS5leHBvcnRzPXtcbiAgXCJDSFwiOiB7XG4gICAgXCJuYW1lXCI6IFwiU3dpdHplcmxhbmRcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiU3dpc3MgRmVkZXJhbCBJbnN0aXR1dGUgb2YgSW50ZWxsZWN0dWFsIFByb3BlcnR5XCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJJUElcIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwidGhyZWUgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwid2l0aGluIDIgd2Vla3NcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiB0cnVlXG4gIH0sXG4gIFwiTk9cIjoge1xuICAgIFwibmFtZVwiOiBcIk5vcndheVwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJOb3J3ZWdpYW4gSW5kdXN0cmlhbCBQcm9wZXJ0eSBPZmZpY2VcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIk5JUE9cIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwidGhyZWUgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiMy02IG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IHRydWVcbiAgfSxcbiAgXCJDTlwiOiB7XG4gICAgXCJuYW1lXCI6IFwiQ2hpbmFcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiU3RhdGUgQWRtaW5pc3RyYXRpb24gZm9yIEluZHVzdHJ5IGFuZCBDb21tZXJjZSBUcmFkZW1hcmsgT2ZmaWNlXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJDVE1PXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcInRocmVlIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjE1IG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IHRydWVcbiAgfSxcbiAgXCJSVVwiOiB7XG4gICAgXCJuYW1lXCI6IFwiVGhlIFJ1c3NpYW4gRmVkZXJhdGlvblwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJGZWRlcmFsIFNlcnZpY2VzIGZvciBJbnRlbGxlY3R1YWwgcHJvcGVydHlcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIlJPU1BBVEVOVFwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCI1IHllYXJzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiMTIgdG8gMTQgbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogdHJ1ZVxuICB9LFxuICBcIlVTXCI6IHtcbiAgICBcIm5hbWVcIjogXCJ0aGUgVW5pdGVkIFN0YXRlcyBvZiBBbWVyaWNhXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIlUuUy4gUGF0ZW50IGFuZCBUcmFkZW1hcmsgT2ZmaWNlXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJVU1BUT1wiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIzMCBkYXlzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiOSB0byAxNiBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiB0cnVlXG4gIH0sXG4gIFwiTVhcIjoge1xuICAgIFwibmFtZVwiOiBcIk1leGljb1wiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJNZXhpY2FuIEluc3RpdHV0ZSBvZiBJbmR1c3RyaWFsIFByb3BlcnR5XCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJNSUlQXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjQgdG8gNiBtb250aHNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCI0IHRvIDYgbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogdHJ1ZVxuICB9LFxuICBcIkJSXCI6IHtcbiAgICBcIm5hbWVcIjogXCJCcmF6aWxcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiQnJhemlsaWFuIFBhdGVudCBhbmQgVHJhZGVtYXJrIE9mZmljZVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiQlBUT1wiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCI2MCBkYXlzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwidGhyZWUgeWVhcnNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiB0cnVlXG4gIH0sXG4gIFwiSVRcIjoge1xuICAgIFwibmFtZVwiOiBcIkl0YWx5XCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIlBhdGVudCBhbmQgVHJhZGVtYXJrIE9mZmljZVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiVUlCTVwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIzIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjYgbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogdHJ1ZVxuICB9LFxuICBcIkxWXCI6IHtcbiAgICBcIm5hbWVcIjogXCJMYXR2aWFcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiUGF0ZW50IE9mZmljZSBvZiB0aGUgcmVwdWJsaWMgb2YgTGF0dmlhXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJQT1wiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIzIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjgtMTAgbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogdHJ1ZVxuICB9LFxuICBcIkRFXCI6IHtcbiAgICBcIm5hbWVcIjogXCJHZXJtYW55XCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIlRyYWRlbWFyayBEZXBhcnRtZW50IG9mIHRoZSBHZXJtYW4gUGF0ZW50IGFuZCBUcmFkZW1hcmsgT2ZmaWNlXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJEUE1BXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjMgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiNi05IG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IHRydWVcbiAgfSxcbiAgXCJFU1wiOiB7XG4gICAgXCJuYW1lXCI6IFwiU3BhaW5cIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiU3BhbmlzaCBQYXRlbnQgYW5kIFRyYWRlIE1hcmsgT2ZmaWNlXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJTUFRPXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjIgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiNi0xMiBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiB0cnVlXG4gIH0sXG4gIFwiRlJcIjoge1xuICAgIFwibmFtZVwiOiBcIkZyYW5jZVwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJOYXRpb25hbCBJbnN0aXR1dGUgb2YgSW5kdXN0cmlhbCBQcm9wZXJ0eVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiSS5OLlAuSVwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIyIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjQtNiBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiB0cnVlXG4gIH0sXG4gIFwiUFRcIjoge1xuICAgIFwibmFtZVwiOiBcIlBvcnR1Z2FsXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIlBvcnR1Z3Vlc2UgSW5zdGl0dXRlIG9mIGluZHVzdHJpYWwgcHJvcGVydHlcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIklOUElcIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMiBtb250aHNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCI0LTYgbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogdHJ1ZVxuICB9LFxuICBcIkdCXCI6IHtcbiAgICBcIm5hbWVcIjogXCJVbml0ZWQgS2luZ2RvbVwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJJbnRlbGxlY3R1YWwgUHJvcGVydHkgT2ZmaWNlXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJJUFwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIyIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjQtNiBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiB0cnVlXG4gIH0sXG4gIFwiU0VcIjoge1xuICAgIFwibmFtZVwiOiBcIlN3ZWRlblwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJTd2VkaXNoIFBhdGVudCBhbmQgUmVnaXN0cmF0aW9uIE9mZmljZVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiUFJWXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjMgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiMy02IG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IHRydWVcbiAgfSxcbiAgXCJQTFwiOiB7XG4gICAgXCJuYW1lXCI6IFwiUG9sYW5kXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIlBvbGlzaCBQYXRlbnQgT2ZmaWNlXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJVUFJQXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjYgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiMiB5ZWFyc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IHRydWVcbiAgfSxcbiAgXCJDWlwiOiB7XG4gICAgXCJuYW1lXCI6IFwiQ3plY2ggUmVwdWJsaWNcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiSW5kdXN0cmlhbCBQcm9wZXJ0eSBPZmZpY2VcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIlVQVlwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIzIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjYtMTAgbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogdHJ1ZVxuICB9LFxuICBcIkVFXCI6IHtcbiAgICBcIm5hbWVcIjogXCJFc3RvbmlhXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIkVzdG9uaWFuIFBhdGVudCBPZmZpY2VcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIkVQQVwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIyIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjE2LTE4IG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IHRydWVcbiAgfSxcbiAgXCJGSVwiOiB7XG4gICAgXCJuYW1lXCI6IFwiRmlubGFuZFwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJGaW5uaXNoIFBhdGVudCBhbmQgUmVnaXN0cmF0aW9uIE9mZmljZVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiUFJIXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjIgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiNC01IG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IHRydWVcbiAgfSxcbiAgXCJBVFwiOiB7XG4gICAgXCJuYW1lXCI6IFwiQXVzdHJpYVwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJBdXN0cmlhbiBQYXRlbnQgT2ZmaWNlXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJBUE9cIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMiBtb250aHNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCIzLTQgbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogdHJ1ZVxuICB9LFxuICBcIkRLXCI6IHtcbiAgICBcIm5hbWVcIjogXCJEZW5tYXJrXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIkRhbmlzaCBQYXRlbnQgYW5kIFRyYWRlbWFyayBPZmZpY2VcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIkRQVE9cIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMiBtb250aHNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCIzIG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IHRydWVcbiAgfSxcbiAgXCJNQ1wiOiB7XG4gICAgXCJuYW1lXCI6IFwiTW9uYWNvXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIk1vbmFjbyBJbnRlbGxlY3R1YWwgUHJvcGVydHlcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIk1JUFJPXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjMgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiMyBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiB0cnVlXG4gIH0sXG4gIFwiTVRcIjoge1xuICAgIFwibmFtZVwiOiBcIk1hbHRhXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIkluZHVzdHJpYWwgUHJvcGVydHkgUmVnaXN0cmF0aW9ucyBEaXJlY3RvcmF0ZVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiSVBSXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjkgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiOSBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiBmYWxzZVxuICB9LFxuICBcIklFXCI6IHtcbiAgICBcIm5hbWVcIjogXCJJcmVsYW5kXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIklyaXNoIFBhdGVudHMgT2ZmaWNlXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJJUE9cIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMyBtb250aHNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCIyIG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IHRydWVcbiAgfSxcbiAgXCJDQVwiOiB7XG4gICAgXCJuYW1lXCI6IFwiQ2FuYWRhXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIlRyYWRlbWFya3MgT2ZmaWNlIG9mIHRoZSBDYW5hZGlhbiBJbnRlbGxlY3R1YWwgUHJvcGVydHkgT2ZmaWNlXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJDSVBPXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjIgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiMTQtMTggbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogZmFsc2VcbiAgfSxcbiAgXCJDVVwiOiB7XG4gICAgXCJuYW1lXCI6IFwiQ3ViYVwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJDdWJhbiBJbnRlbGxlY3R1YWwgUHJvcGVydHkgT2ZmaWNlXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJPQ1BJXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjIgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiMTggbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogdHJ1ZVxuICB9LFxuICBcIlVBXCI6IHtcbiAgICBcIm5hbWVcIjogXCJVa3JhaW5lXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIlVrcmFpbmlhbiBJbnN0aXR1dGUgb2YgSW5kdXN0cmlhbCBQcm9wZXJ0eVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiVUlQVlwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIxMi0xNSBtb250aHNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCIxMi0xNSBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiB0cnVlXG4gIH0sXG4gIFwiVVlcIjoge1xuICAgIFwibmFtZVwiOiBcIlVydWd1YXlcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiTmF0aW9uYWwgRGlyZWN0aW9uIG9mIEluZHVzdHJpYWwgUHJvcGVydHlcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIkROUElcIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMzAgZGF5c1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjEyLTE0IG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IGZhbHNlXG4gIH0sXG4gIFwiVVpcIjoge1xuICAgIFwibmFtZVwiOiBcIlV6YmVraXN0YW5cIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiQWdlbmN5IG9uIEludGVsbGVjdHVhbCBQcm9wZXJ0eSBvZiB0aGUgUmVwdWJsaWMgb2YgVXpiZWtpc3RhblwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjEyIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjEyIG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IHRydWVcbiAgfSxcbiAgXCJWRVwiOiB7XG4gICAgXCJuYW1lXCI6IFwiVmVuZXp1ZWxhXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIkluZHVzdHJpYWwgUHJvcGVydHkgUmVnaXN0cnkgLSBBdXRvbm9tb3VzIFNlcnZpY2Ugb2YgSW50ZWxsZWN0dWFsIFByb3BlcnR5XCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJTQVBJXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjMwIHdvcmtpbmcgZGF5c1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjEyIG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IGZhbHNlXG4gIH0sXG4gIFwiV0JcIjoge1xuICAgIFwibmFtZVwiOiBcIldlc3RiYW5rXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIk9mZmljZSBvZiB0aGUgUmVnaXN0cmFyIGluIHRoZSBXZXN0IEJhbmtcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIk9mZmljZSBvZiB0aGUgUmVnaXN0cmFyIGluIHRoZSBXZXN0IEJhbmtcIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMyBtb250aHNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCIxNi0xOCBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiBmYWxzZVxuICB9LFxuICBcIllFXCI6IHtcbiAgICBcIm5hbWVcIjogXCJZZW1lblwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJTZWN0b3IsIEdlbmVyYWwgQWRtaW5pc3RyYXRpb24gb2YgSW50ZWxsZWN0dWFsIFByb3BlcnR5IFByb3RlY3Rpb25cIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIllJUE9cIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMyBtb250aHNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCIxMiBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiBmYWxzZVxuICB9LFxuICBcIlpXXCI6IHtcbiAgICBcIm5hbWVcIjogXCJaaW1iYWJ3ZVwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJ0aGUgQWZyaWNhbiBSZWdpb25hbCBJbmR1c3RyaWFsIFByb3BlcnR5IE9yZ2FuaXphdGlvblwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiQVJJUE9cIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMyBtb250aHNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCI4LTE2IG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IGZhbHNlXG4gIH0sXG4gIFwiUEtcIjoge1xuICAgIFwibmFtZVwiOiBcIlBha2lzdGFuXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIkludGVsbGVjdHVhbCBQcm9wZXJ0eSBPcmdhbml6YXRpb24gb2YgUGFraXN0YW4sIFRyYWRlIE1hcmtzIFJlZ2lzdHJ5XCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJJUE9cIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMiBtb250aHNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCI5LTEyIG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IGZhbHNlXG4gIH0sXG4gIFwiUEFcIjoge1xuICAgIFwibmFtZVwiOiBcIlBhbmFtYVwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJQYW5hbWEgSW5kdXN0cmlhbCBQcm9wZXJ0eSBPZmZpY2VcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIkRJR0VSUElcIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMiBtb250aHNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCIxMi0xOCBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiBmYWxzZVxuICB9LFxuICBcIlBZXCI6IHtcbiAgICBcIm5hbWVcIjogXCJQYXJhZ3VheVwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJOYXRpb25hbCBEaXJlY3RvcmF0ZSBvZiBJbnRlbGxlY3R1YWwgUHJvcGVydHlcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIlwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCI2MCB3b3JraW5nIGRheXNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCI4IG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IGZhbHNlXG4gIH0sXG4gIFwiUEhcIjoge1xuICAgIFwibmFtZVwiOiBcIlBoaWxpcHBpbmVzXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIkludGVsbGVjdHVhbCBQcm9wZXJ0eSBPZmZpY2Ugb2YgUGhpbGlwcGluZXNcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIklQT1BISUxcIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMzAgd29ya2luZyBkYXlzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiNC0xMiBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiB0cnVlXG4gIH0sXG4gIFwiUFJcIjoge1xuICAgIFwibmFtZVwiOiBcIlB1ZXJ0byBSaWNvXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcInRoZSBEZXBhcnRtZW50IG9mIEVjb25vbWljIGRldmVsb3BtZW50IGFuZCBDb21tZXJjZVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjMwIHdvcmtpbmcgZGF5c1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjE1LTIwIG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IGZhbHNlXG4gIH0sXG4gIFwiUUFcIjoge1xuICAgIFwibmFtZVwiOiBcIlFhdGFyXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIkluZHVzdHJpYWwgUHJvcGVydHkgT2ZmaWNlIFRyYWRlbWFya3MsIEluZHVzdHJpYWwgRGVzaWducyAmIEcuSS4gQ29tcGV0ZW50IGFkbWluaXN0cmF0aW9uIE1pbmlzdHJ5IG9mIEJ1c2luZXNzIGFuZCBUcmFkZSwgRGVwYXJ0bWVudCBvZiBDb21tZXJjaWFsIFJlZ2lzdHJhdGlvbiBhbmQgTGljZW5zaW5nXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJcIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiNCBtb250aHNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCI4LTEyIG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IGZhbHNlXG4gIH0sXG4gIFwiUk9cIjoge1xuICAgIFwibmFtZVwiOiBcIlJvbWFuaWFcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiU3RhdGUgT2ZmaWNlIGZvciBJbnZlbnRpb25zIGFuZCBUcmFkZW1hcmtzXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJPU0lNXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjIgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiOC0xMiBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiB0cnVlXG4gIH0sXG4gIFwiUldcIjoge1xuICAgIFwibmFtZVwiOiBcIlJ3YW5kYVwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJSd2FuZGEgRGV2ZWxvcG1lbnQgQm9hcmQgKFJEQinCoE9mZmljZSBvZiB0aGUgUmVnaXN0cmFyIEdlbmVyYWxcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIlJEQlwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIzMCB3b3JraW5nIGRheXNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCIxMiBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiB0cnVlXG4gIH0sXG4gIFwiU0FcIjoge1xuICAgIFwibmFtZVwiOiBcIlNhdWRpIEFyYWJpYVwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJHZW5lcmFsIERpcmVjdG9yYXRlIG9mIEluZHVzdHJpYWwgUHJvcGVydHnCoEtpbmcgQWJkdWxsYXppeiBDaXR5IGZvciBTY2llbmNlIGFuZCBUZWNobm9sb2d5XCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJLQUNTVFwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCI5MCB3b3JraW5nIGRheXNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCIxMi0xNCBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiBmYWxzZVxuICB9LFxuICBcIlJTXCI6IHtcbiAgICBcIm5hbWVcIjogXCJTZXJiaWFcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiSW50ZWxsZWN0dWFsIFByb3BlcnR5IE9mZmljZSBSZXB1YmxpYyBvZiBTZXJiaWFcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIlpJU1wiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIzMCB3b3JraW5nIGRheXNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCIzLTYgbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogdHJ1ZVxuICB9LFxuICBcIlNHXCI6IHtcbiAgICBcIm5hbWVcIjogXCJTaW5nYXBvcmVcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiSW50ZWxsZWN0dWFsIFByb3BlcnR5IE9mZmljZSBvZiBTaW5nYXBvcmVcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIklQT1NcIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwidHdvIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjYgdG8gMTIgbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogdHJ1ZVxuICB9LFxuICBcIlNLXCI6IHtcbiAgICBcIm5hbWVcIjogXCJTbG92YWtpYVwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJJbmR1c3RyaWFsIFByb3BlcnR5IE9mZmljZSBvZiB0aGUgU2xvdmFrIFJlcHVibGljXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJJTkRQUk9QXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjMgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiNC01IG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IHRydWVcbiAgfSxcbiAgXCJTSVwiOiB7XG4gICAgXCJuYW1lXCI6IFwiU2xvdmVuaWFcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiU2xvdmVuaWFuIEludGVsbGVjdHVhbCBQcm9wZXJ0eSBPZmZpY2UsIE1pbmlzdHJ5IG9mIEVjb25vbXlcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIlNJUE9cIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMyBtb250aHNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCI0LTUgbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogdHJ1ZVxuICB9LFxuICBcIlpBXCI6IHtcbiAgICBcIm5hbWVcIjogXCJTb3V0aCBBZnJpY2FcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiQ29tcGFuaWVzIGFuZCBJbnRlbGxlY3R1YWwgUHJvcGVydHkgQ29tbWlzc2lvblwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiQ0lQQ1wiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIzIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjItMyB5ZWFyc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IGZhbHNlXG4gIH0sXG4gIFwiQUxcIjoge1xuICAgIFwibmFtZVwiOiBcIkFsYmFuaWFcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiQWxiYW5pYW4gRGlyZWN0b3JhdGUgb2YgUGF0ZW50cyBhbmQgTWFya3NcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIkFMUFRPXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjMgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiNi05IG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IHRydWVcbiAgfSxcbiAgXCJBR1wiOiB7XG4gICAgXCJuYW1lXCI6IFwiQW50aWd1YSBhbmQgQmFyYnVkYVwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJBbnRpZ3VhICYgQmFyYnVkYSBJbnRlbGxlY3R1YWwgUHJvcGVydHkgJiBDb21tZXJjZSBPZmZpY2VcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIlwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIzIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjIgeWVhcnNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiB0cnVlXG4gIH0sXG4gIFwiQU1cIjoge1xuICAgIFwibmFtZVwiOiBcIkFybWVuaWFcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiSW50ZWxsZWN0dWFsIFByb3BlcnR5IEFnZW5jeVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIj9cIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCI2LTEyIG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IHRydWVcbiAgfSxcbiAgXCJWTlwiOiB7XG4gICAgXCJuYW1lXCI6IFwiVmlldG5hbVwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJOYXRpb25hbCBPZmZpY2Ugb2YgSW5kdXN0cmlhbCBQcm9wZXJ0eSBvZiBWaWV0bmFtXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJOT0lQXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjEyIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjEyIG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IHRydWVcbiAgfSxcbiAgXCJHUlwiOiB7XG4gICAgXCJuYW1lXCI6IFwiR3JlZWNlXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIkluZHVzdHJpYWwgUHJvcGVydHkgT3JnYW5pemF0aW9uXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJPQklcIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMyBtb250aHNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCIzIG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IHRydWVcbiAgfSxcbiAgXCJMVFwiOiB7XG4gICAgXCJuYW1lXCI6IFwiTGl0aHVhbmlhXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIlN0YXRlIFBhdGVudCBCdXJlYXUgb2YgdGhlIFJlcHVibGljIG9mIExpdGh1YW5pYVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiU1BCXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjEwLTE4IG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjMgbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogdHJ1ZVxuICB9LFxuICBcIlBFXCI6IHtcbiAgICBcIm5hbWVcIjogXCJQZXJ1XCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIk5hdGlvbmFsIEluc3RpdHV0ZSBmb3IgdGhlIERlZmVuc2Ugb2YgQ29tcGV0aXRpb24gYW5kIFByb3RlY3Rpb24gb2YgSW5kdXN0cmlhbCBQcm9wZXJ0eVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiSU5ERUNPUElcIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMzAgd29ya2luZyBkYXlzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiNS0zIG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IGZhbHNlXG4gIH0sXG4gIFwiQVVcIjoge1xuICAgIFwibmFtZVwiOiBcIkF1c3RyYWxpYVwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJJUCBBdXN0cmFsaWEgRGVwYXJ0bWVudCBvZiBJbmR1c3RyeVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjIgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiOC0xMiBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiB0cnVlXG4gIH0sXG4gIFwiQVpcIjoge1xuICAgIFwibmFtZVwiOiBcIkF6ZXJiYWlqYW5cIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiU3RhdGUgQ29tbWl0dGVlIG9uIFN0YW5kYXJkaXphdGlvbiwgTWV0cm9sb2d5IGFuZCBQYXRlbnRzIG9mIHRoZSBSZXB1YmxpYyBvZiBBemVyYmFpamFuXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJcIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMiBtb250aHNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCI4LTEyIG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IHRydWVcbiAgfSxcbiAgXCJCWVwiOiB7XG4gICAgXCJuYW1lXCI6IFwiQmVsYXJ1c1wiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJOYXRpb25hbCBDZW50ZXIgb2YgSW50ZWxsZWN0dWFsIFByb3BlcnR5wqBTdGF0ZSBDb21taXR0ZWUgb24gU2NpZW5jZSBhbmQgVGVjaG5vbG9naWVzXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJOQ0lQXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIj9cIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCIzIHllYXJzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogdHJ1ZVxuICB9LFxuICBcIkJUXCI6IHtcbiAgICBcIm5hbWVcIjogXCJCaHV0YW5cIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiSW5kdXN0cmlhbCBQcm9wZXJ0eSBEaXZpc2lvbiDCoE1pbmlzdHJ5IG9mIEVjb25vbWljIEFmZmFpcnNcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIklQRFwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIzIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjYgLTcgbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogdHJ1ZVxuICB9LFxuICBcIkJBXCI6IHtcbiAgICBcIm5hbWVcIjogXCJCb3NuaWEgYW5kIEhlcnplZ292aW5hXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIkluc3RpdHV0ZSBmb3IgSW50ZWxsZWN0dWFsIFByb3BlcnR5IG9mIEJvc25pYSBhbmQgSGVyemVnb3ZpbmFcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIklQUlwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIzIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjEgLSAyIHllYXJzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogdHJ1ZVxuICB9LFxuICBcIkJHXCI6IHtcbiAgICBcIm5hbWVcIjogXCJCdWxnYXJpYVwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJQYXRlbnQgT2ZmaWNlIG9mIHRoZSBSZXB1YmxpYyBvZiBCdWxnYXJpYVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiQlBPXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjMgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiNi05IG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IHRydWVcbiAgfSxcbiAgXCJDT1wiOiB7XG4gICAgXCJuYW1lXCI6IFwiQ29sb21iaWFcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiTWluaXN0cnkgb2YgRWNvbm9taWMgRGV2ZWxvcG1lbnQgU3VwZXJpbnRlbmRlbmNlIG9mIEluZHVzdHJ5IGFuZCBDb21tZXJjZSBJbmR1c3RyaWFsIFByb3BlcnR5IERlbGVnYXRpb25cIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIlNJQ1wiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIxIG1vbnRoXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiNC02IG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IHRydWVcbiAgfSxcbiAgXCJIUlwiOiB7XG4gICAgXCJuYW1lXCI6IFwiQ3JvYXRpYVwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJDcm9hdGlhbiBTdGF0ZSBJbnRlbGxlY3R1YWwgUHJvcGVydHkgT2ZmaWNlXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJDSVNQT1wiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIzIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjktMTIgbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogdHJ1ZVxuICB9LFxuICBcIkNZXCI6IHtcbiAgICBcIm5hbWVcIjogXCJDeXBydXNcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiRGVwYXJ0bWVudCBvZiBSZWdpc3RyYXIgb2YgQ29tcGFuaWVzIGFuZCBPZmZpY2lhbCBSZWNlaXZlciBNaW5pc3RyeSBvZiBDb21tZXJjZSBJbmR1c3RyeSBhbmQgVG91cmlzbVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjIgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiMi0zIHllYXJzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogdHJ1ZVxuICB9LFxuICBcIktQXCI6IHtcbiAgICBcIm5hbWVcIjogXCJOb3J0aCBLb3JlYVwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJUcmFkZW1hcmtzIEluZHVzdHJpYWwgRGVzaWducyBhbmQgR2VvZ3JhcGhpY2FsIEluZGljYXRpb25zIE9mZmljZSBvZiB0aGUgRGVtb2NyYXRpYyBQZW9wbGXigJlzIFJlcHVibGljIG9mIEtvcmVhXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJcIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiNiBtb250aHNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCIxMiBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiB0cnVlXG4gIH0sXG4gIFwiRUdcIjoge1xuICAgIFwibmFtZVwiOiBcIkVneXB0XCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIkNvbW1lcmNpYWwgUmVnaXN0cnkgQWRtaW5pc3RyYXRpb24gTWluaXN0cnkgb2YgVHJhZGUgYW5kIEluZHVzdHJ5IFRyYWRlbWFya3MgT2ZmaWNlXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJcIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMiBtb250aHNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCIyLTMgeWVhcnNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiB0cnVlXG4gIH0sXG4gIFwiR0VcIjoge1xuICAgIFwibmFtZVwiOiBcIkdlb3JnaWFcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiTmF0aW9uYWwgSW50ZWxsZWN0dWFsIFByb3BlcnR5IENlbnRlclwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiU2FrcGF0ZW50aVwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIzIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjEyIG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IHRydWVcbiAgfSxcbiAgXCJIVVwiOiB7XG4gICAgXCJuYW1lXCI6IFwiSHVuZ2FyeVwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJIdW5nYXJpYW4gSW50ZWxsZWN0dWFsIFByb3BlcnR5IE9mZmljZVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiSElQT1wiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIzIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjctOSBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiB0cnVlXG4gIH0sXG4gIFwiSVNcIjoge1xuICAgIFwibmFtZVwiOiBcIkljZWxhbmRcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiSWNlbGFuZGljIFBhdGVudCBPZmZpY2VcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIlwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIyIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjItNCBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiB0cnVlXG4gIH0sXG4gIFwiSU5cIjoge1xuICAgIFwibmFtZVwiOiBcIkluZGlhXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIkNvbnRyb2xsZXIgR2VuZXJhbCBvZiBQYXRlbnRzIERlc2lnbnMgJiBUcmFkZSBNYXJrc1wiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjQgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiMTItMTggbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogdHJ1ZVxuICB9LFxuICBcIklSXCI6IHtcbiAgICBcIm5hbWVcIjogXCJJcmFuXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIm5kdXN0cmlhbCBQcm9wZXJ0eSBPZmZpY2VcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIlwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIxIG1vbnRoXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiIDggbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogdHJ1ZVxuICB9LFxuICBcIklMXCI6IHtcbiAgICBcIm5hbWVcIjogXCJJc3JhZWxcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiTWluaXN0cnkgb2YgSnVzdGljZSBQYXRlbnQgT2ZmaWNlIFRyYWRlbWFya3MgRGVwYXJ0bWVudFwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjMgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiMTggbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogdHJ1ZVxuICB9LFxuICBcIkpQXCI6IHtcbiAgICBcIm5hbWVcIjogXCJKYXBhblwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJJbnRlcm5hdGlvbmFsIEFmZmFpcnMgRGl2aXNpb24gR2VuZXJhbCBBZmZhaXJzIERlcGFydG1lbnQgSmFwYW4gUGF0ZW50IE9mZmljZVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjIgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiNy04IG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IHRydWVcbiAgfSxcbiAgXCJLWlwiOiB7XG4gICAgXCJuYW1lXCI6IFwiS2F6YWtoc3RhblwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJDb21taXR0ZWUgZm9yIEludGVsbGVjdHVhbCBQcm9wZXJ0eSBSaWdodHNcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIlwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCI1IHllYXJzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiMTAgbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogdHJ1ZVxuICB9LFxuICBcIktFXCI6IHtcbiAgICBcIm5hbWVcIjogXCJLZW55YVwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJLZW55YSBJbmR1c3RyaWFsIFByb3BlcnR5IEluc3RpdHV0ZVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiS0lQSVwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIyIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjE1IG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IHRydWVcbiAgfSxcbiAgXCJLR1wiOiB7XG4gICAgXCJuYW1lXCI6IFwiS3lyZ3l6c3RhblwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJLeXJneXpwYXRlbnRcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIlwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCI/XCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiNi0xNCBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiB0cnVlXG4gIH0sXG4gIFwiTFNcIjoge1xuICAgIFwibmFtZVwiOiBcIkxlc290aG9cIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiTWluaXN0cnkgb2YgTGF3IGFuZCBDb25zdGl0dXRpb25hbCBBZmZhaXJzXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJcIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMyBtb250aHNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCIyNCBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiB0cnVlXG4gIH0sXG4gIFwiTFlcIjoge1xuICAgIFwibmFtZVwiOiBcIkxpYnlhXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIkdlbmVyYWwgQ29ycG9yYXRpb25zIGFuZCBDb21tZXJjaWFsIFJlZ2lzdHJpZXMgRGlyZWN0b3JhdGUgTWluaXN0cnkgb2YgSW5kdXN0cmlhbCBFY29ub21pYyBhbmQgVHJhZGVcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIlwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIzIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjMgeWVhcnNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiBmYWxzZVxuICB9LFxuICBcIkxJXCI6IHtcbiAgICBcIm5hbWVcIjogXCJMaWVjaHRlbnN0ZWluXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIkFtdCBmw7xyIFZvbGtzd2lydHNjaGFmdFwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiQVZXXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIj9cIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCI0LTYgbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogdHJ1ZVxuICB9LFxuICBcIk1HXCI6IHtcbiAgICBcIm5hbWVcIjogXCJNYWRhZ2FzY2FyXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIk1hbGFnYXN5IE9mZmljZSBvZiBJbmR1c3RyaWFsIFByb3BlcnR5XCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJPTUFQSVwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCI/XCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiMTAgbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogdHJ1ZVxuICB9LFxuICBcIk1OXCI6IHtcbiAgICBcIm5hbWVcIjogXCJNb25nb2xpYVwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJJbnRlbGxlY3R1YWwgUHJvcGVydHkgT2ZmaWNlXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJJUE9NXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjEgeWVhclwiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjYtOSBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiB0cnVlXG4gIH0sXG4gIFwiTUVcIjoge1xuICAgIFwibmFtZVwiOiBcIk1vbnRlbmVncm9cIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiSW50ZWxsZWN0dWFsIFByb3BlcnR5IE9mZmljZSBNb250ZW5lZ3JvXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJcIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMyBtb250aHNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCIxMiBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiB0cnVlXG4gIH0sXG4gIFwiTUFcIjoge1xuICAgIFwibmFtZVwiOiBcIk1vcm9jY29cIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiTW9yb2NjYW4gSW5kdXN0cmlhbCBhbmQgQ29tbWVyY2lhbCBQcm9wZXJ0eSBPZmZpY2VcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIk9NUElDXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjIgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiNiBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiB0cnVlXG4gIH0sXG4gIFwiTVpcIjoge1xuICAgIFwibmFtZVwiOiBcIk1vemFtYmlxdWVcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiSW5kdXN0cmlhbCBQcm9wZXJ0eSBJbnN0aXR1dGVcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIklQSVwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIyIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjQgbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogdHJ1ZVxuICB9LFxuICBcIk5BXCI6IHtcbiAgICBcIm5hbWVcIjogXCJOYW1pYmlhXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIk1pbmlzdHJ5IG9mIFRyYWRlIGFuZCBJbmR1c3RyeVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiRGlyZWN0b3JhdGUgQ29tbWVyY2VcIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMiBtb250aHNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCI1IHllYXJzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogdHJ1ZVxuICB9LFxuICBcIk5aXCI6IHtcbiAgICBcIm5hbWVcIjogXCJOZXcgWmVhbGFuZFwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJOZXcgWmVhbGFuZCBJbnRlbGxlY3R1YWwgUHJvcGVydHkgT2ZmaWNlXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJJUE9OWlwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIzIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjYtMTIgbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogdHJ1ZVxuICB9LFxuICBcIk9NXCI6IHtcbiAgICBcIm5hbWVcIjogXCJPbWFuXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIkludGVsbGVjdHVhbCBQcm9wZXJ0eSBPZmZpY2Ugb2YgT21hblwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjMgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiNi0xMiBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiB0cnVlXG4gIH0sXG4gIFwiS1JcIjoge1xuICAgIFwibmFtZVwiOiBcIlNvdXRoIEtvcmVhXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIktvcmVhbiBJbnRlbGxlY3R1YWwgUHJvcGVydHkgT2ZmaWNlXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJLSVBPXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjIgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiOS0xMiBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiB0cnVlXG4gIH0sXG4gIFwiTURcIjoge1xuICAgIFwibmFtZVwiOiBcIlJlcHVibGljIG9mIE1vbGRvdmFcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiU3RhdGUgQWdlbmN5IG9mIEludGVsbGVjdHVhbCBQcm9wZXJ0eVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiQUdFUElcIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMyBtb250aHNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCIxMi0xNCBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiB0cnVlXG4gIH0sXG4gIFwiU01cIjoge1xuICAgIFwibmFtZVwiOiBcIlNhbiBNYXJpbm9cIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiVWZmaWNpbyBkaSBTdGF0byBCcmV2ZXR0aSBlIE1hcmNoaVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiVVNCTVwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIzIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjEyLTE4IG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IHRydWVcbiAgfSxcbiAgXCJMS1wiOiB7XG4gICAgXCJuYW1lXCI6IFwiU3JpIExhbmthXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIk5hdGlvbmFsIEludGVsbGVjdHVhbCBQcm9wZXJ0eSBPZmZpY2Ugb2YgU3JpIExhbmthXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJOSVBPXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjMgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiNiBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiBmYWxzZVxuICB9LFxuICBcIlNaXCI6IHtcbiAgICBcIm5hbWVcIjogXCJTd2F6aWxhbmRcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiTWluaXN0cnkgb2YgQ29tbWVyY2UgSW5kdXN0cnkgYW5kIFRyYWRlXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJcIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiOCBtb250aHNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCI5LTEyIG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IHRydWVcbiAgfSxcbiAgXCJTWVwiOiB7XG4gICAgXCJuYW1lXCI6IFwiU3lyaWFcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiVHJhZGVtYXJrIE9mZmljZSAoTWluaXN0cnkgb2YgSW50ZXJpb3IgVHJhZGUgYW5kIENvbnN1bWVyIFByb3RlY3Rpb24pXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJUcmFkZW1hcmsgT2ZmaWNlXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjMgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiOC0xMiBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiB0cnVlXG4gIH0sXG4gIFwiVFdcIjoge1xuICAgIFwibmFtZVwiOiBcIlRhaXdhblwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJJbnRlbGxlY3R1YWwgT2ZmaWNlXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJNT0VBXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjMgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiMTAtMTIgbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogZmFsc2VcbiAgfSxcbiAgXCJUSlwiOiB7XG4gICAgXCJuYW1lXCI6IFwiVGFqaWtpc3RhblwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJOYXRpb25hbCBQYXRlbnQgYW5kIEluZm9ybWF0aW9uIENlbnRyZVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiTkNQSVwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCI/XCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiMTggbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogdHJ1ZVxuICB9LFxuICBcIk1LXCI6IHtcbiAgICBcIm5hbWVcIjogXCJNYWNlZG9uaWFcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiU3RhdGUgT2ZmaWNlIG9mIEluZHVzdHJpYWwgUHJvcGVydHlcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIlNPSVBcIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMyBtb250aHNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCI2LTEyIG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IHRydWVcbiAgfSxcbiAgXCJUTlwiOiB7XG4gICAgXCJuYW1lXCI6IFwiVHVuaXNpYVwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJOYXRpb25hbCBJbnN0aXR1dGUgZm9yIFN0YW5kYXJkaXphdGlvbiBhbmQgSW5kdXN0cmlhbCBQcm9wZXJ0eVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiSU5OT1JQSVwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIyIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjIgeWVhcnNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiB0cnVlXG4gIH0sXG4gIFwiVFJcIjoge1xuICAgIFwibmFtZVwiOiBcIlR1cmtleVwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJOYXRpb25hbCBQYXRlbnQgT2ZmaWNlXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJUUEVcIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMyBtb250aHNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCIxMC0xMiBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiB0cnVlXG4gIH0sXG4gIFwiVE1cIjoge1xuICAgIFwibmFtZVwiOiBcIlR1cmttZW5pc3RhblwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJQYXRlbnQgRGVwYXJ0bWVudCBvZiBNaW5pc3RyeSBvZiBFY29ub215IGFuZCBGaW5hbmNlIG9mIFR1cmttZW5pc3RhblwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIj9cIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCIxOCBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiB0cnVlXG4gIH0sXG4gIFwiVUdcIjoge1xuICAgIFwibmFtZVwiOiBcIlVnYW5kYVwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJUcmFkZW1hcmtzIFJlZ2lzdHJ5IEludGVsbGVjdHVhbCBQcm9wZXJ0eSBEZXBhcnRtZW50IGluIHRoZSBVZ2FuZGEgUmVnaXN0cmF0aW9uIFNlcnZpY2VzIEJ1cmVhdVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjIgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiMyBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiB0cnVlXG4gIH0sXG4gIFwiQUZcIjoge1xuICAgIFwibmFtZVwiOiBcIkFmZ2hhbmlzdGFuXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIkFmZ2hhbmlzdGFuIENlbnRyYWwgQnVzaW5lc3MgUmVnaXN0cnkgYW5kIEludGVsbGVjdHVhbCBQcm9wZXJ0ecKgTWluaXN0cnkgb2YgQ29tbWVyY2UgYW5kIEluZHVzdHJ5XCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJBQ0JSXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjEgbW9udGhcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCIzLTYgbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogdHJ1ZVxuICB9LFxuICBcIkRaXCI6IHtcbiAgICBcIm5hbWVcIjogXCJBbGdlcmlhXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIk5hdGlvbmFsIE9mZmljZSBvZiBDb3B5cmlnaHRzIGFuZCBSZWxhdGVkIFJpZ2h0cyBNaW5pc3RyeSBvZiBDb21tdW5pY2F0aW9uIGFuZCBDdWx0dXJlXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJPTkRBXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIj9cIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCIyNCBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiB0cnVlXG4gIH0sXG4gIFwiQURcIjoge1xuICAgIFwibmFtZVwiOiBcIkFuZG9ycmFcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiVHJhZGVtYXJrcyBPZmZpY2Ugb2YgdGhlIFByaW5jaXBhbGl0eSBvZiBBbmRvcnJhwqBEZXBhcnRtZW50IG9mIEVjb25vbXnCoE1pbmlzdHJ5IG9mIEVjb25vbXkgYW5kIExhbmQgTWFuYWdlbWVudFwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiT01QQVwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCI/XCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiMTggbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogdHJ1ZVxuICB9LFxuICBcIkFPXCI6IHtcbiAgICBcIm5hbWVcIjogXCJBbmdvbGFcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiQW5nb2xhbiBJbnN0aXR1dGUgb2YgSW5kdXN0cmlhbCBQcm9wZXJ0ecKgTWluaXN0cnkgb2YgR2VvbG9neSBNaW5lcyBhbmQgSW5kdXN0cnlcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIk1JTkdNSVwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIyIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjI0LTI4IG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IHRydWVcbiAgfSxcbiAgXCJBUlwiOiB7XG4gICAgXCJuYW1lXCI6IFwiQXJnZW50aW5hXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIk5hdGlvbmFsIEluc3RpdHV0ZSBvZiBJbmR1c3RyaWFsIFByb3BlcnR5XCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJJTlBJXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjEgbW9udGhcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCIxOCBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiBmYWxzZVxuICB9LFxuICBcIkFXXCI6IHtcbiAgICBcIm5hbWVcIjogXCJBcnViYVwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJCdXJlYXUgb2YgSW50ZWxsZWN0dWFsIFByb3BlcnR5IGluIEFydWJhXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJcIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiNiBtb250aHNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCIyIG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IHRydWVcbiAgfSxcbiAgXCJCU1wiOiB7XG4gICAgXCJuYW1lXCI6IFwiQmFoYW1hc1wiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJSZWdpc3RyYXIgR2VuZXJhbCdzIERlcGFydG1lbnRcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIlwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIxIG1vbnRoXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiNCB5ZWFyc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IGZhbHNlXG4gIH0sXG4gIFwiQkRcIjoge1xuICAgIFwibmFtZVwiOiBcIkJhbmdsYWRlc2hcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiRGVwYXJ0bWVudCBvZiBQYXRlbnRzIERlc2lnbnMgYW5kIFRyYWRlbWFya3PCoE1pbmlzdHJ5IG9mIEluZHVzdHJpZXNcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIkRQRFRcIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMSBtb250aFwiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjYgeWVhcnNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiBmYWxzZVxuICB9LFxuICBcIkJaXCI6IHtcbiAgICBcIm5hbWVcIjogXCJCZWxpemVcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiQmVsaXplIEludGVsbGVjdHVhbCBQcm9wZXJ0eSBPZmZpY2VcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIkJFTElQT1wiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIzIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjYgbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogZmFsc2VcbiAgfSxcbiAgXCJCT1wiOiB7XG4gICAgXCJuYW1lXCI6IFwiQm9saXZpYVwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJTZXJ2aWNpbyBOYWNpb25hbCBkZSBQcm9wcmllZGFkIEludGVsZWN0dWFsXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJTRU5BUElcIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMSBtb250aFwiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjQgbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogdHJ1ZVxuICB9LFxuICBcIkJOXCI6IHtcbiAgICBcIm5hbWVcIjogXCJCcnVuZWlcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiQnJ1bmVpIEludGVsbGVjdHVhbCBQcm9wZXJ0eSBPZmZpY2VcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIkJydUlQT1wiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIzIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjEtMiB5ZWFyc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IGZhbHNlXG4gIH0sXG4gIFwiQklcIjoge1xuICAgIFwibmFtZVwiOiBcIkJ1cnVuZGlcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiTWluaXN0cnkgb2YgVHJhZGUgSW5kdXN0cnkgYW5kIFRvdXJpc21cIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIlwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIxIG1vbnRoXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiNiBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiBmYWxzZVxuICB9LFxuICBcIktIXCI6IHtcbiAgICBcIm5hbWVcIjogXCJDYW1ib2RpYVwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJEZXBhcnRtZW50IG9mIEluZHVzdHJpYWwgUHJvcGVydHkgTWluaXN0cnkgb2YgSW5kdXN0cnkgYW5kIEhhbmRpY3JhZnRcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIkRJUFwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIzIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjEgeWVhclwiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IGZhbHNlXG4gIH0sXG4gIFwiQ1ZcIjoge1xuICAgIFwibmFtZVwiOiBcIkNhcGUgVmVyZGVcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiSW5kdXN0cmlhbCBQcm9wZXJ0eSBJbnN0aXR1dGVcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIklQSUNWXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjMgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiNiBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiBmYWxzZVxuICB9LFxuICBcIkNMXCI6IHtcbiAgICBcIm5hbWVcIjogXCJDaGlsZVwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJOYXRpb25hbCBJbnN0aXR1dGUgb2YgSW5kdXN0cmlhbCBQcm9wZXJ0eVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiSU5BUElcIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMSBtb250aFwiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjggbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogZmFsc2VcbiAgfSxcbiAgXCJDUlwiOiB7XG4gICAgXCJuYW1lXCI6IFwiQ29zdGEgUmljYVwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJSZWdpc3RybyBkZSBsYSBQcm9waWVkYWQgSW5kdXN0cmlhbMKgUmVnaXN0cm8gTmFjaW9uYWzCoCBNaW5pc3RlcmlvIGRlIEp1c3RpY2lhIHkgUGF6XCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJSTlBESUdJVEFMXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjIgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiNS02IG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IGZhbHNlXG4gIH0sXG4gIFwiRE1cIjoge1xuICAgIFwibmFtZVwiOiBcIkRvbWluaWNhXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIkNvbXBhbmllcyBhbmQgSW50ZWxsZWN0dWFsIFByb3BlcnR5IE9mZmljZcKgTWluaXN0cnkgb2YgVG91cmlzbSBhbmQgbGVnYWwgQWZmYWlyc1wiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiQ0lQT1wiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIyIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjUgbW9udGhzIC0gMSB5ZWFyXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogZmFsc2VcbiAgfSxcbiAgXCJET1wiOiB7XG4gICAgXCJuYW1lXCI6IFwiRG9taW5pY2FuIFJlcHVibGljXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIk5hdGlvbmFsIE9mZmljZSBvZiBJbmR1c3RyaWFsIFByb3BlcnR5IChPTkFQSSnCoFN0YXRlIFNlY3JldGFyaWF0IGZvciBJbmR1c3RyeSBhbmQgQ29tbWVyY2VcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIlwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCI0NSBkYXlzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiMSBtb250aFwiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IGZhbHNlXG4gIH0sXG4gIFwiRUNcIjoge1xuICAgIFwibmFtZVwiOiBcIkVjdWFkb3JcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiSW5zdGl0dXRvIEVjdWF0b3JpYW5vIGRlIFByb3BpZWRhZCBJbmR1c3RyaWFsIEludGVsZWN0dWFsXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJJLkUuUC5JLlwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIxIG1vbnRoXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiMiBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiBmYWxzZVxuICB9LFxuICBcIlNWXCI6IHtcbiAgICBcIm5hbWVcIjogXCJFbCBTYWx2YWRvclwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJOYXRpb25hbCBDZW50ZXIgb2YgUmVnaXN0cmllc1wiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiQ05SXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjIgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiNy05IG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IGZhbHNlXG4gIH0sXG4gIFwiRVRcIjoge1xuICAgIFwibmFtZVwiOiBcIkV0aGlvcGlhXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIkV0aGlvcGlhbiBJbnRlbGxlY3R1YWwgUHJvcGVydHkgT2ZmaWNlXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJFSVBPXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjIgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiNCBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiBmYWxzZVxuICB9LFxuICBcIkdaXCI6IHtcbiAgICBcIm5hbWVcIjogXCJHYXphXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIkFidS1HaGF6YWxlaCBJbnRlbGxlY3R1YWwgUHJvcGVydHlcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIkFHSVBcIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMyBtb250aHNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCIxOC0yNCBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiBmYWxzZVxuICB9LFxuICBcIkdIXCI6IHtcbiAgICBcIm5hbWVcIjogXCJHaGFuYVwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJSZWdpc3RyYXIgR2VuZXJhbCdzIERlcGFydG1lbnTCoE1pbmlzdHJ5IG9mIEp1c3RpY2VcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIlwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIyIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjEgeWVhclwiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IGZhbHNlXG4gIH0sXG4gIFwiR0RcIjoge1xuICAgIFwibmFtZVwiOiBcIkdyZW5hZGFcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiSW50ZWxsZWN0dWFsIFByb3BlcnR5IE9mZmljZVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiQ0FJUE9cIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMSBtb250aHNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCIxIHllYXJcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiBmYWxzZVxuICB9LFxuICBcIkdUXCI6IHtcbiAgICBcIm5hbWVcIjogXCJHdWF0ZW1hbGFcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiUmVnaXN0cnkgb2YgSW50ZWxsZWN0dWFsIFByb3BlcnR5wqBNaW5pc3RyeSBvZiBFY29ub21pYyBBZmZhaXJzXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJcIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMiBtb250aHNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCIyLTMgbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogZmFsc2VcbiAgfSxcbiAgXCJHWVwiOiB7XG4gICAgXCJuYW1lXCI6IFwiR3V5YW5hXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIkRlZWRzIFJlZ2lzdHJ5wqBNaW5pc3RyeSBvZiBMZWdhbCBBZmZhaXJzXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJcIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMSBtb250aFwiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjEgeWVhclwiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IGZhbHNlXG4gIH0sXG4gIFwiSFRcIjoge1xuICAgIFwibmFtZVwiOiBcIkhhaXRpXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIkludGVsbGVjdHVhbCBQcm9wZXJ0eSBTZXJ2aWNlwqBNaW5pc3RyeSBvZiBUcmFkZSBhbmQgSW5kdXN0cnlcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIk1DSVwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIyIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjgtMTIgbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogZmFsc2VcbiAgfSxcbiAgXCJITlwiOiB7XG4gICAgXCJuYW1lXCI6IFwiSG9uZHVyYXNcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiRGlyZWN0b3JhdGUgR2VuZXJhbCBvZiBJbnRlbGxlY3R1YWwgUHJvcGVydHlcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIkRJR0VQSUhcIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMSBtb250aFwiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjMgdG8gNCBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiBmYWxzZVxuICB9LFxuICBcIkhLXCI6IHtcbiAgICBcIm5hbWVcIjogXCJIb25nIEtvbmdcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiSW50ZWxsZWN0dWFsIFByb3BlcnR5IERlcGFydG1lbnRcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIklQRFwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIzIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjkgdG8gMTIgbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogZmFsc2VcbiAgfSxcbiAgXCJJRFwiOiB7XG4gICAgXCJuYW1lXCI6IFwiSW5kb25lc2lhXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIkRpcmVjdG9yYXRlIEdlbmVyYWwgb2YgSW50ZWxsZWN0dWFsIFByb3BlcnR5IFJpZ2h0c8KgTWluaXN0cnkgb2YgTGF3IGFuZCBIdW1hbiBSaWdodHNcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIkRHSVBcIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMyBtb250aHNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCIxLjUgeWVhcnNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiBmYWxzZVxuICB9LFxuICBcIklRXCI6IHtcbiAgICBcIm5hbWVcIjogXCJJcmFxXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIkluZHVzdHJpYWwgUHJvcGVydHkgRGVwYXJ0bWVudMKgQ2VudHJhbCBPcmdhbml6YXRpb24gZm9yIFN0YW5kYXJkaXphdGlvbiAmIFF1YWxpdHkgQ29udHJvbMKgTWluaXN0cnkgb2YgUGxhbm5pbmdcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIkNPU1FDXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjMgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiMS41IHRvIDIgeWVhcnNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiBmYWxzZVxuICB9LFxuICBcIkpNXCI6IHtcbiAgICBcIm5hbWVcIjogXCJKYW1haWNhXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIkphbWFpY2EgSW50ZWxsZWN0dWFsIFByb3BlcnR5IE9mZmljZVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiSklQT1wiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIyIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjEyIHRvIDE4IG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IGZhbHNlXG4gIH0sXG4gIFwiSk9cIjoge1xuICAgIFwibmFtZVwiOiBcIkpvcmRhblwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJJbmR1c3RyaWFsIFByb3BlcnR5IFByb3RlY3Rpb24gRGlyZWN0b3JhdGUgTWluaXN0cnkgb2YgSW5kdXN0cnkgYW5kIFRyYWRlXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJNSVRcIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMyBtb250aHNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCI4LTEyIG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IGZhbHNlXG4gIH0sXG4gIFwiS09cIjoge1xuICAgIFwibmFtZVwiOiBcIktvc292b1wiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJLb3Nvdm8gSW50ZWxsZWN0dWFsIFByb3BlcnR5XCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJcIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMyBtb250aHNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCIxMiBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiBmYWxzZVxuICB9LFxuICBcIktXXCI6IHtcbiAgICBcIm5hbWVcIjogXCJLdXdhaXRcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiTWluaXN0cnkgb2YgVHJhZGUgYW5kIEluZHVzdHJ5wqBUcmFkZW1hcmtzIGFuZCBQYXRlbnRzIERlcGFydG1lbnRcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIlwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIxIG1vbnRoXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiOC0xMCBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiBmYWxzZVxuICB9LFxuICBcIkxCXCI6IHtcbiAgICBcIm5hbWVcIjogXCJMZWJhbm9uXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIk9mZmljZSBvZiBJbnRlbGxlY3R1YWwgUHJvcGVydHnCoERlcGFydG1lbnQgb2YgSW50ZWxsZWN0dWFsIFByb3BlcnR5XCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJcIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiP1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjQtNiBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiBmYWxzZVxuICB9LFxuICBcIk1PXCI6IHtcbiAgICBcIm5hbWVcIjogXCJNYWNhdVwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJNYWNhdSBFY29ub21pYyBTZXJ2aWNlcyAtIEludGVsbGVjdHVhbCBQcm9wZXJ0eSBEZXBhcnRtZW50XCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJcIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMiBtb250aHNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCI2LTggbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogZmFsc2VcbiAgfSxcbiAgXCJNWVwiOiB7XG4gICAgXCJuYW1lXCI6IFwiTWFsYXlzaWFcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiSW50ZWxsZWN0dWFsIFByb3BlcnR5IENvcnBvcmF0aW9uIG9mIE1hbGF5c2lhXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJNeUlQT1wiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIyIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjIgeWVhcnNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiBmYWxzZVxuICB9LFxuICBcIk1WXCI6IHtcbiAgICBcIm5hbWVcIjogXCJNYWxkaXZlc1wiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJJbnRlbGxlY3R1YWwgUHJvcGVydHkgVW5pdMKgTWluaXN0cnkgb2YgRWNvbm9taWMgRGV2ZWxvcG1lbnRcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIlwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCI/XCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiMy00IHdlZWtzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogZmFsc2VcbiAgfSxcbiAgXCJNVVwiOiB7XG4gICAgXCJuYW1lXCI6IFwiTWF1cml0aXVzXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIkluZHVzdHJpYWwgUHJvcGVydHkgT2ZmaWNlXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJJUE9cIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMiBtb250aHNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCIxMiBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiBmYWxzZVxuICB9LFxuICBcIk1NXCI6IHtcbiAgICBcIm5hbWVcIjogXCJNeWFubWFyXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIkRlcGFydG1lbnQgb2YgVGVjaG5pY2FsIGFuZCBWb2NhdGlvbmFsIEVkdWNhdGlvbsKgTWluaXN0cnkgb2YgU2NpZW5jZSBhbmQgVGVjaG5vbG9neVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIj9cIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCIzIG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IGZhbHNlXG4gIH0sXG4gIFwiTlBcIjoge1xuICAgIFwibmFtZVwiOiBcIk5lcGFsXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIkRlcGFydG1lbnQgb2YgSW5kdXN0cnnCoE1pbmlzdHJ5IG9mIEluZHVzdHJ5XCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJET0lORFwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIzIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjItMyBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiBmYWxzZVxuICB9LFxuICBcIk5JXCI6IHtcbiAgICBcIm5hbWVcIjogXCJOaWNhcmFndWFcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiSW50ZWxsZWN0dWFsIFByb3BlcnR5IFJlZ2lzdHJ5IE1pbmlzdHJ5IG9mIERldmVsb3BtZW50IEluZHVzdHJ5IGFuZCBUcmFkZVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiUlBJXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjIgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiMTAgbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogZmFsc2VcbiAgfSxcbiAgXCJOR1wiOiB7XG4gICAgXCJuYW1lXCI6IFwiTmlnZXJpYVwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJUcmFkZW1hcmtzXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCIgUGF0ZW50cyBhbmQgRGVzaWducyBSZWdpc3RyecKgRmVkZXJhbCBNaW5pc3RyeSBvZiBUcmFkZSBhbmQgSW52ZXN0bWVudFxcXCJcIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiJydcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCIyIG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IGZhbHNlXG4gIH0sXG4gIFwiU1RcIjoge1xuICAgIFwibmFtZVwiOiBcIlNhbyBUb21lIGFuZCBQcmluY2lwZVwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJJbmR1c3RyaWFsIFByb3BlcnR5IEluc3RpdHV0ZVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiU0VOQVBJXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjMgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiMyBtb250aHMgLSAxIHllYXJcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiBmYWxzZVxuICB9LFxuICBcIlNDXCI6IHtcbiAgICBcIm5hbWVcIjogXCJTZXljaGVsbGVzXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIlJlZ2lzdHJhdGlvbiBEaXZpc2lvbsKgRGVwYXJ0bWVudCBvZiBMZWdhbCBBZmZhaXJzwqBQcmVzaWRlbnQncyBPZmZpY2VcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIlwiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIyIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjEyLTI0IG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IGZhbHNlXG4gIH0sXG4gIFwiU1JcIjoge1xuICAgIFwibmFtZVwiOiBcIlN1cmluYW1lXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIkJ1cmVhdSBvZiBJbnRlbGxlY3R1YWwgUHJvcGVydHnCoE1pbmlzdHJ5IG9mIEp1c3RpY2UgYW5kIFBvbGljZVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjYgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiMTggbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogZmFsc2VcbiAgfSxcbiAgXCJUWlwiOiB7XG4gICAgXCJuYW1lXCI6IFwiVGFuemFuaWFcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiVHJhZGUgYW5kIFNlcnZpY2UgTWFya3MgT2ZmaWNlXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJcIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMiBtb250aHNcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCIxMi0xOCBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiBmYWxzZVxuICB9LFxuICBcIlRIXCI6IHtcbiAgICBcIm5hbWVcIjogXCJUaGFpbGFuZFwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJEZXBhcnRtZW50IG9mIEludGVsbGVjdHVhbCBQcm9wZXJ0eSBNaW5pc3RyeSBvZiBDb21tZXJjZVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiRElQXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjMgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiOC0yNCBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiBmYWxzZVxuICB9LFxuICBcIlRUXCI6IHtcbiAgICBcIm5hbWVcIjogXCJUcmluaWRhZCBhbmQgVG9iYWdvXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIkludGVsbGVjdHVhbCBQcm9wZXJ0eSBPZmZpY2XCoE1pbmlzdHJ5IG9mIExlZ2FsIEFmZmFpcnNcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIklQT1wiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIzIG1vbnRoc1wiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjggbW9udGhzLTIgeWVhcnNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiBmYWxzZVxuICB9LFxuICBcIlRDXCI6IHtcbiAgICBcIm5hbWVcIjogXCJUdXJrcyBhbmQgQ2FpY29zIElzbGFuZHNcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiSW50ZWxsZWN0dWFsIFByb3BlcnR5IE9mZmljZVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiSVBPXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjEgbW9udGhcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCI4LTEyIG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IGZhbHNlXG4gIH0sXG4gIFwiQUVcIjoge1xuICAgIFwibmFtZVwiOiBcIlVuaXRlZCBBcmFiIEVtaXJhdGVzXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIkRlcGFydG1lbnQgb2YgSW5kdXN0cmlhbCBQcm9wZXJ0ecKgTWluaXN0cnkgb2YgRWNvbm9teVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjEgbW9udGhcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCIxMi0xNSBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiBmYWxzZVxuICB9LFxuICBcIkVVXCI6IHtcbiAgICBcIm5hbWVcIjogXCJ0aGUgRXVyb3BlYW4gVW5pb25cIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiRXVyb3BlYW4gVW5pb24gSW50ZWxsZWN0dWFsIFByb3BlcnR5IE9mZmljZVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiRVVJUE9cIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwidGhyZWUgbW9udGhzXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwic2V2ZW4gbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogdHJ1ZVxuICB9LFxuXG4gIFwiVExcIjoge1xuICAgIFwibmFtZVwiOiBcIkVhc3QgVGltb3JcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiSW50ZWxsZWN0dWFsIFByb3BlcnR5IE9mZmljZVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiSVBPXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjEgbW9udGhcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCI4LTEyIG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IGZhbHNlXG4gIH0sXG5cblxuICBcIlNCXCI6IHtcbiAgICBcIm5hbWVcIjogXCJTb2xvbW9uIElzbGFuZHNcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiSW50ZWxsZWN0dWFsIFByb3BlcnR5IE9mZmljZVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiSVBPXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjEgbW9udGhcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCI4LTEyIG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IGZhbHNlXG4gIH0sXG5cblxuICBcIkpFXCI6IHtcbiAgICBcIm5hbWVcIjogXCJKZXJzZXlcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiSW50ZWxsZWN0dWFsIFByb3BlcnR5IE9mZmljZVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiSVBPXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjEgbW9udGhcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCI4LTEyIG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IGZhbHNlXG4gIH0sXG5cbiAgXG5cbiAgXCJCWFwiOiB7XG4gICAgXCJuYW1lXCI6IFwiQmVuZWx1eFwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJJbnRlbGxlY3R1YWwgUHJvcGVydHkgT2ZmaWNlXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJJUE9cIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMSBtb250aFwiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjgtMTIgbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogZmFsc2VcbiAgfSxcblxuICBcblxuXG4gIFwiU1hcIjoge1xuICAgIFwibmFtZVwiOiBcIlNpbnQgTWFhcnRlblwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJJbnRlbGxlY3R1YWwgUHJvcGVydHkgT2ZmaWNlXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJJUE9cIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMSBtb250aFwiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjgtMTIgbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogZmFsc2VcbiAgfSxcblxuXG4gIFwiS1JEXCI6IHtcbiAgICBcIm5hbWVcIjogXCJLdXJkaXN0YW4gUmVnaW9uXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIkludGVsbGVjdHVhbCBQcm9wZXJ0eSBPZmZpY2VcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIklQT1wiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIxIG1vbnRoXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiOC0xMiBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiBmYWxzZVxuICB9LFxuXG5cbiAgXCJFQVpcIjoge1xuICAgIFwibmFtZVwiOiBcIlphbnppYmFyXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIkludGVsbGVjdHVhbCBQcm9wZXJ0eSBPZmZpY2VcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIklQT1wiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIxIG1vbnRoXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiOC0xMiBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiBmYWxzZVxuICB9LFxuXG5cbiAgXCJNQUZcIjoge1xuICAgIFwibmFtZVwiOiBcIlNhaW50IE1hcnRpblwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJJbnRlbGxlY3R1YWwgUHJvcGVydHkgT2ZmaWNlXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJJUE9cIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMSBtb250aFwiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjgtMTIgbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogZmFsc2VcbiAgfSxcblxuXG5cbiAgXCJORVwiOiB7XG4gICAgXCJuYW1lXCI6IFwiTmlnZXJpYVwiLFxuICAgIFwiYWdlbmN5X25hbWVcIjogXCJJbnRlbGxlY3R1YWwgUHJvcGVydHkgT2ZmaWNlXCIsXG4gICAgXCJhZ2VuY3lfc2hvcnRcIjogXCJJUE9cIixcbiAgICBcIm9wcG9zaXRpb25fcGVyaW9kXCI6IFwiMSBtb250aFwiLFxuICAgIFwicmVnaXN0cmF0aW9uX3BlcmlvZFwiOiBcIjgtMTIgbW9udGhzXCIsXG4gICAgXCJtYWRyaWRfcHJvdG9jb2xcIjogZmFsc2VcbiAgfSxcblxuXG5cbiAgXCJDSVwiOiB7XG4gICAgXCJuYW1lXCI6IFwiSXZvcnkgQ29hc3RcIixcbiAgICBcImFnZW5jeV9uYW1lXCI6IFwiSW50ZWxsZWN0dWFsIFByb3BlcnR5IE9mZmljZVwiLFxuICAgIFwiYWdlbmN5X3Nob3J0XCI6IFwiSVBPXCIsXG4gICAgXCJvcHBvc2l0aW9uX3BlcmlvZFwiOiBcIjEgbW9udGhcIixcbiAgICBcInJlZ2lzdHJhdGlvbl9wZXJpb2RcIjogXCI4LTEyIG1vbnRoc1wiLFxuICAgIFwibWFkcmlkX3Byb3RvY29sXCI6IGZhbHNlXG4gIH0sXG5cblxuXG4gIFwiTkxcIjoge1xuICAgIFwibmFtZVwiOiBcIk5ldGhlcmxhbmRzXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIkludGVsbGVjdHVhbCBQcm9wZXJ0eSBPZmZpY2VcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIklQT1wiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIxIG1vbnRoXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiOC0xMiBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiBmYWxzZVxuICB9LFxuXG5cblxuICBcIkxVXCI6IHtcbiAgICBcIm5hbWVcIjogXCJMdXhlbWJvdXJnXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIkludGVsbGVjdHVhbCBQcm9wZXJ0eSBPZmZpY2VcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIklQT1wiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIxIG1vbnRoXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiOC0xMiBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiBmYWxzZVxuICB9LFxuXG5cbiAgXCJWQ1RcIjoge1xuICAgIFwibmFtZVwiOiBcIlNhaW50IFZpbmNlbnQgYW5kIHRoZSBHcmVuYWRpbmVzXCIsXG4gICAgXCJhZ2VuY3lfbmFtZVwiOiBcIkludGVsbGVjdHVhbCBQcm9wZXJ0eSBPZmZpY2VcIixcbiAgICBcImFnZW5jeV9zaG9ydFwiOiBcIklQT1wiLFxuICAgIFwib3Bwb3NpdGlvbl9wZXJpb2RcIjogXCIxIG1vbnRoXCIsXG4gICAgXCJyZWdpc3RyYXRpb25fcGVyaW9kXCI6IFwiOC0xMiBtb250aHNcIixcbiAgICBcIm1hZHJpZF9wcm90b2NvbFwiOiBmYWxzZVxuICB9XG5cblxuXG5cblxuXG59IiwibW9kdWxlLmV4cG9ydHM9e1xuICBcIkFUXCI6XCJBdXN0cmlhXCIsXG4gIFwiQkdcIjpcIkJ1bGdhcmlhXCIsXG4gIFwiQkVcIjpcIkJlbGdpdW1cIixcbiAgXCJDWlwiOlwiQ3plY2ggUmVwdWJsaWNcIixcbiAgXCJDWVwiOlwiQ3lwcnVzXCIsXG4gIFwiREtcIjpcIkRlbm1hcmtcIixcbiAgXCJFRVwiOlwiRXN0b25pYVwiLFxuICBcIkZJXCI6XCJGaW5sYW5kXCIsXG4gIFwiRlJcIjpcIkZyYW5jZVwiLFxuICBcIkRFXCI6XCJHZXJtYW55XCIsXG4gIFwiR1JcIjpcIkdyZWVjZVwiLFxuICBcIkhVXCI6XCJIdW5nYXJ5XCIsXG4gIFwiSUVcIjpcIklyZWxhbmRcIixcbiAgXCJJVFwiOlwiSXRhbHlcIixcbiAgXCJMVlwiOlwiTGF0dmlhXCIsXG4gIFwiTFRcIjpcIkxpdGh1YW5pYVwiLFxuICBcIkxVXCI6XCJMdXhlbWJvdXJnXCIsXG4gIFwiTVRcIjpcIk1hbHRhXCIsXG4gIFwiTkxcIjpcIk5ldGhlcmxhbmRzXCIsXG4gIFwiUExcIjpcIlBvbGFuZFwiLFxuICBcIlBUXCI6XCJQb3J0dWdhbFwiLFxuICBcIlJPXCI6XCJSb21hbmlhXCIsXG4gIFwiU0xcIjpcIlNsb3Zha2lhXCIsXG4gIFwiU0lcIjpcIlNsb3ZlbmlhXCIsXG4gIFwiRVNcIjpcIlNwYWluXCIsXG4gIFwiU0VcIjpcIlN3ZWRlblwiLFxuICBcIkdCXCI6XCJVbml0ZWQgS2luZ2RvbVwiXG59IiwibW9kdWxlLmV4cG9ydHM9e1xuICBcIlVTXCI6IHtcImlucHV0X3R5cGVcIjogMCwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcIndcIiwgXCJ3b3JkX2xhYmVsXCI6IFwiV29yZCBtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAwLCAgXCJzdHVkeV90eXBlXCI6IC0xLCBcInN0dWR5X3R5cGUyXCI6IFwiMFwifSxcbiAgXCJJTlwiOiB7XCJpbnB1dF90eXBlXCI6IDAsIFwiUE9BX3JlcXVpcmVkXCI6LTEsIFwidHJhZGVfdHlwZVwiOiBcIndcIiwgXCJ3b3JkX2xhYmVsXCI6IFwiV29yZCBtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAwLCBcInN0dWR5X3R5cGVcIjogLTF9LFxuICBcIlNHXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xfSxcbiAgXCJHUlwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMX0sXG4gIFwiTFRcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTF9LFxuICBcIkhLXCI6IHtcImlucHV0X3R5cGVcIjogMCwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcIndcIiwgXCJ3b3JkX2xhYmVsXCI6IFwiV29yZCBtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xfSxcbiAgXCJKUFwiOiB7XCJpbnB1dF90eXBlXCI6IDAsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ3XCIsIFwid29yZF9sYWJlbFwiOiBcIldvcmQgbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogMCwgXCJzdHVkeV90eXBlXCI6IC0xfSxcbiAgXCJUV1wiOiB7XCJpbnB1dF90eXBlXCI6IDAsIFwiUE9BX3JlcXVpcmVkXCI6IDEsIFwidHJhZGVfdHlwZVwiOiBcIndcIiwgXCJ3b3JkX2xhYmVsXCI6IFwiV29yZCBtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAwLCBcInN0dWR5X3R5cGVcIjogLTF9LFxuICBcIkFSXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xfSxcbiAgXCJBWlwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IDEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMX0sXG4gIFwiQVVcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTF9LFxuICBcIk5aXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMX0sXG4gIFwiR0JcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xfSxcbiAgXCJERVwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTF9LFxuICBcIklFXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMX0sXG4gIFwiQVNcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xfSxcbiAgXCJGSlwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTF9LFxuICBcIlBHXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMX0sXG4gIFwiV1NcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xfSxcbiAgXCJUT1wiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTF9LFxuICBcIkJIXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMX0sXG4gIFwiQllcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAyLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTF9LFxuICBcIkJaXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogMiwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xfSxcbiAgXCJCT1wiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IDEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMX0sXG4gIFwiQkVcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAxLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTF9LFxuICBcIkNBXCI6IHtcImlucHV0X3R5cGVcIjogMCwgXCJQT0FfcmVxdWlyZWRcIjogMSwgXCJ0cmFkZV90eXBlXCI6IFwid1wiLCBcIndvcmRfbGFiZWxcIjogXCJXb3JkIG1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IDAsIFwic3R1ZHlfdHlwZVwiOiAtMX0sXG4gIFwiQ09cIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAxLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTF9LFxuICBcIkVVXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMX0sXG4gIFwiQ0hcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xfSxcbiAgXCJTWlwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMX0sXG4gIFwiTk9cIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAxLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTF9LFxuICBcIk1YXCI6IHtcImlucHV0X3R5cGVcIjogMCwgXCJQT0FfcmVxdWlyZWRcIjogMSwgXCJ0cmFkZV90eXBlXCI6IFwid1wiLCBcIndvcmRfbGFiZWxcIjogXCJXb3JrbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMX0sXG4gIFwiQlJcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAxLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTF9LFxuICBcIkNOXCI6IHtcImlucHV0X3R5cGVcIjogMCwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcIndcIiwgXCJ3b3JkX2xhYmVsXCI6IFwiV29yZCBtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xfSxcbiAgXCJSVVwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IDEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMX0sXG4gIFwiS1JcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTF9LFxuICBcIlRIXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xfSxcbiAgXCJQSFwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IDEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMX0sXG4gIFwiVk5cIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAxLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTF9LFxuICBcIkFGXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJBTVwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiQkRcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkJUXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJCTlwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiS0hcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIlRMXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJHRVwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiSURcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIklRXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJJTFwiOiB7XCJpbnB1dF90eXBlXCI6IDAsIFwiUE9BX3JlcXVpcmVkXCI6IDEsIFwidHJhZGVfdHlwZVwiOiBcIndcIiwgXCJ3b3JkX2xhYmVsXCI6IFwiV29yZCBtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAwLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwid1wifSxcbiAgXCJKT1wiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiS1dcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIktHXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJMQVwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiTEJcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIk1PXCI6IHtcImlucHV0X3R5cGVcIjogMCwgXCJQT0FfcmVxdWlyZWRcIjogMSwgXCJ0cmFkZV90eXBlXCI6IFwid1wiLCBcIndvcmRfbGFiZWxcIjogXCJXb3JkIG1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IDAsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ3XCJ9LFxuICBcIk1ZXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJNVlwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiTU5cIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIk1NXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJOUFwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiT01cIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIlBLXCI6IHtcImlucHV0X3R5cGVcIjogMCwgXCJQT0FfcmVxdWlyZWRcIjogMSwgXCJ0cmFkZV90eXBlXCI6IFwid1wiLCBcIndvcmRfbGFiZWxcIjogXCJXb3JkIG1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IDAsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ3XCJ9LFxuICBcIlBTXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJRQVwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiU0FcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkxLXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJUSlwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiVFJcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIlRNXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJBRVwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiVVpcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIllFXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJUR1wiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiQUxcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkFEXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJBVFwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiQkFcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkJHXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJIUlwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiQ1lcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkNaXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJES1wiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiRUVcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkZJXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJGUlwiOiB7XCJpbnB1dF90eXBlXCI6IDAsIFwiUE9BX3JlcXVpcmVkXCI6IDEsIFwidHJhZGVfdHlwZVwiOiBcIndcIiwgXCJ3b3JkX2xhYmVsXCI6IFwiV29yZCBtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAwLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwid1wifSxcbiAgXCJHSVwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiR0dcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkhVXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJJU1wiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiSVRcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkxWXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJMSVwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiTUtcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIk1UXCI6IHtcImlucHV0X3R5cGVcIjogMCwgXCJQT0FfcmVxdWlyZWRcIjogMSwgXCJ0cmFkZV90eXBlXCI6IFwid1wiLCBcIndvcmRfbGFiZWxcIjogXCJXb3JkIG1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IDAsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ3XCJ9LFxuICBcIk1EXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJNQ1wiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiUExcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIlBUXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJST1wiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiU01cIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIlNLXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJTSVwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiRVNcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIlNFXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJVQVwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiQUlcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkFHXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJBV1wiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiQkJcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkJNXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJWR1wiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiS1lcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkNMXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJDUlwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiQ1dcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkRNXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJET1wiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiRUNcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIlNWXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJHRFwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiR1RcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkdZXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJIVFwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiSE5cIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkpNXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJNU1wiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiTklcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIlBBXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJQWVwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiUEVcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIlBSXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJWQ1wiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiS05cIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkxDXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJTUlwiOiB7XCJpbnB1dF90eXBlXCI6IDAsIFwiUE9BX3JlcXVpcmVkXCI6IDEsIFwidHJhZGVfdHlwZVwiOiBcIndcIiwgXCJ3b3JkX2xhYmVsXCI6IFwiV29yZCBtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAwLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwid1wifSxcbiAgXCJUVFwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiVVlcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIlZFXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJEWlwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiQU9cIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkJJXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJDVlwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiQ0dcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkRKXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJFR1wiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiRVJcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkVUXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJHTVwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiR0hcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIktFXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJMUlwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiTFlcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIk1HXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJNV1wiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiTVVcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIk1BXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJNWlwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiTkFcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIlJXXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJTVFwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiU0NcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIlNMXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJaQVwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiVFpcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIlROXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJVR1wiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiWk1cIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIlpXXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJSU1wiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiTUVcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkJTXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJLT1wiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiVENcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIlNCXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSwgIFxuICBcIkxTXCI6IHtcImlucHV0X3R5cGVcIjogMCwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcIndcIiwgXCJ3b3JkX2xhYmVsXCI6IFwiV29yZCBtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJzdHVkeV90eXBlMlwiOiBcIjBcIn0sXG4gIFwiSkVcIjoge1wiaW5wdXRfdHlwZVwiOiAwLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwid1wiLCBcIndvcmRfbGFiZWxcIjogXCJXb3JkIG1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCAgXCJzdHVkeV90eXBlXCI6IC0xLCBcInN0dWR5X3R5cGUyXCI6IFwiMFwifSxcbiAgXCJCWFwiOiB7XCJpbnB1dF90eXBlXCI6IDAsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ3XCIsIFwid29yZF9sYWJlbFwiOiBcIldvcmQgbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsICBcInN0dWR5X3R5cGVcIjogLTEsIFwic3R1ZHlfdHlwZTJcIjogXCIwXCJ9LFxuICBcIlNYXCI6IHtcImlucHV0X3R5cGVcIjogMCwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcIndcIiwgXCJ3b3JkX2xhYmVsXCI6IFwiV29yZCBtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJzdHVkeV90eXBlMlwiOiBcIjBcIn0sXG4gIFwiTkVcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkNJXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJOTFwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTF9LFxuICBcIkxVXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMX0sXG4gIFwiT0FQSVwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiS1JEXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJFQVpcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIlZDVFwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiTUFGXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJBUklQT1wiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn1cbn0iLCJtb2R1bGUuZXhwb3J0cz17XG4gIFwiQkpcIjpcIkJlbmluXCIsXG4gIFwiQkZcIjpcIkJ1cmtpbmEgRmFzb1wiLFxuICBcIkNNXCI6XCJDYW1lcm9vblwiLFxuICBcIkNGXCI6XCJDZW50cmFsIEFmcmljYW5cIixcbiAgXCJURFwiOlwiQ2hhZFwiLFxuICBcIktNXCI6XCJDb21vcm9zXCIsXG4gIFwiQ0RcIjpcIkNvbmdvIFJlcHVibGljXCIsXG4gIFwiR1FcIjpcIkVxdWF0b3JpYWwgR3VpbmVhXCIsXG4gIFwiR0FcIjpcIkdhYm9uXCIsXG4gIFwiR05cIjpcIkd1aW5lYVwiLFxuICBcIkdXXCI6XCJHdWluZWEtQmlzc2F1XCIsXG4gIFwiTUxcIjpcIk1hbGlcIixcbiAgXCJNUlwiOlwiTWF1cml0YW5pYVwiLFxuICBcIk5HXCI6XCJOaWdlcmlhXCIsXG4gIFwiU05cIjpcIlNlbmVnYWxcIixcbiAgXCJUT1wiOlwiVG9uZ2FcIlxufSIsIm1vZHVsZS5leHBvcnRzPXtcbiAgXCJPQVBJXCI6W1wiT0FQSSB0cmVhdHkgaW5jbHVkZXMgdHJhZGVtYXJrIHByb3RlY3Rpb24gaW4gdGhlIGZvbGxvd2luZyBjb3VudHJpZXM6IDxiPkJlbmluLCBCdXJraW5hIEZhc28sIENhbWVyb29uLCBDZW50cmFsIEFmcmljYW4gUmVwdWJsaWMsIENoYWQsIENvbW9yb3MsIENvbmdvIFJlcHVibGljLCBFcXVhdG9yaWFsIEd1aW5lYSwgR2Fib24sIEd1aW5lYSwgR3VpbmVhLUJpc3NhdSwgSXZvcnkgQ29hc3QsIE1hbGksIE1hdXJpdGFuaWEsIE5pZ2VyLCBTZW5lZ2FsLCBhbmQgVG9nby48L2I+XCJdLFxuICBcIkFSSVBPXCI6ICBbIFwiQVJJUE8gdHJlYXR5ICBpbmNsdWRlcyB0cmFkZW1hcmsgcHJvdGVjdGlvbiBpbiB0aGUgZm9sbG93aW5nIGNvdW50cmllczogPGI+Qm90c3dhbmEsIExlc290aG8sIExpYmVyaWEsIE1hbGF3aSwgTmFtaWJpYSwgU3dhemlsYW5kLCBUYW56YW5pYSwgVWdhbmRhIGFuZCBaaW1iYWJ3ZS48L2I+XCIsXG4gICAgXCJJbiBvcmRlciB0byBvYnRhaW4gdHJhZGVtYXJrIHByb3RlY3Rpb24gaW4gX2NvdW50cnlfIHlvdSBjYW4gcmVnaXN0ZXIgeW91ciB0cmFkZW1hcmsgaW4gdHdvIHdheXM6IDxici8+IDxiPkZpcnN0IG9wdGlvbjwvYj4gaXMgdGhhdCB5b3UgcmVxdWVzdCByZWdpc3RyYXRpb24gZm9yIHRoZSBBUklQTyB0cmFkZW1hcms7IEFSSVBPIHRyZWF0eSBpbmNsdWRlcyB0cmFkZW1hcmsgcHJvdGVjdGlvbiBpbiA8Yj5Cb3Rzd2FuYSwgTGVzb3RobywgTGliZXJpYSwgTWFsYXdpLCBOYW1pYmlhLCBSd2FuZGEsIFN3YXppbGFuZCwgVGFuemFuaWEsIFVnYW5kYSBhbmQgWmltYmFid2UuPC9iPiBUbyBwcm9jZWVkIHdpdGggdHJhZGVtYXJrIGFwcGxpY2F0aW9uIGluIEFSSVBPLCBjbGljayA8YSBocmVmPScvdHJhZGVtYXJrL0FSSVBPJz5oZXJlPC9hPi48YnIvPiAgPGI+U2Vjb25kIG9wdGlvbjwvYj4gaXMgdGhhdCB5b3UgcmVnaXN0ZXIgZGlyZWN0bHkgeW91ciB0cmFkZW1hcmsgaW4gX2NvdW50cnlfLiBJZiB5b3Ugd2FudCB0byBwcm9jZWVkIHRoaXMgd2F5IHBsZWFzZSBmb2xsb3cgdGhlIHN0ZXBzIGRlc2NyaWJlZCBiZWxvdy5cIl0sXG4gIFwiRVVcIjpbXG4gICAgICAgICAgXCJUaGUgQ29tbXVuaXR5IFRyYWRlbWFyayBncmFudHMgcHJvdGVjdGlvbiBvdmVyIHRoZSBlbnRpcmUgRXVyb3BlYW4gVW5pb24gd2l0aCBvbmUgcmVnaXN0cmF0aW9uLiBUaGlzIFRyYWRlbWFyayBSZWdpc3RyYXRpb24gaW5jbHVkZXMgcHJvdGVjdGlvbiBpbjogPGI+QXVzdHJpYSwgQnVsZ2FyaWEsIEJlbGdpdW0sIEN6ZWNoIFJlcHVibGljLCBDeXBydXMsIERlbm1hcmssIEVzdG9uaWEsIEZpbmxhbmQsIEZyYW5jZSwgR2VybWFueSwgR3JlZWNlLCBIdW5nYXJ5LCBJcmVsYW5kLCBJdGFseSwgTGF0dmlhLCBMaXRodWFuaWEsIEx1eGVtYm91cmcsIE1hbHRhLCBUaGUgTmV0aGVybGFuZHMsIFBvbGFuZCwgUG9ydHVnYWwsIFJvbWFuaWEsIFNsb3Zha2lhLCBTbG92ZW5pYSwgU3BhaW4sIFN3ZWRlbiBhbmQgVW5pdGVkIEtpbmdkb20uPC9iPlwiLFxuICAgICAgICAgIFwiPHA+SW4gb3JkZXIgdG8gb2J0YWluIHRyYWRlbWFyayBwcm90ZWN0aW9uIGluIDxiPl9jb3VudHJ5XzwvYj4geW91IGNhbiByZWdpc3RlciB5b3VyIHRyYWRlbWFyayBpbiB0d28gd2F5czo8L3A+PHA+PGI+Rmlyc3Qgb3B0aW9uPC9iPiBpcyB0aGF0IHlvdSByZXF1ZXN0IHJlZ2lzdHJhdGlvbiBpbiB0aGUgZW50aXJlIEV1cm9wZWFuIFVuaW9uIHdpdGggb25lIHNpbmdsZSBhcHBsaWNhdGlvbjsgdGhpcyBjYW4gYmUgZG9uZSB2aWEgdGhlIENvbW11bml0eSBUcmFkZW1hcmsgYWdyZWVtZW50IHdoaWNoIGdyYW50cyB0cmFkZW1hcmsgcHJvdGVjdGlvbiBpbiB0aGUgMjcgY291bnRyeSBtZW1iZXJzIG9mIHRoZSBFVS4gPGEgaHJlZj0nL3RyYWRlbWFyay9FVSc+Q2xpY2sgaGVyZTwvYT4uPC9wPjxwPjxiPlNlY29uZCBvcHRpb248L2I+IGlzIHRoYXQgeW91IHJlZ2lzdGVyIGRpcmVjdGx5IHlvdXIgdHJhZGVtYXJrIGluIDxiPl9jb3VudHJ5XzwvYj4uIElmIHlvdSB3YW50IHRvIHByb2NlZWQgdGhpcyB3YXkgcGxlYXNlIGZvbGxvdyB0aGUgc3RlcHMgZGVzY3JpYmVkIGJlbG93OjwvcD5cIlxuICBdXG59IiwiLy8gc2hpbSBmb3IgdXNpbmcgcHJvY2VzcyBpbiBicm93c2VyXG5cbnZhciBwcm9jZXNzID0gbW9kdWxlLmV4cG9ydHMgPSB7fTtcblxucHJvY2Vzcy5uZXh0VGljayA9IChmdW5jdGlvbiAoKSB7XG4gICAgdmFyIGNhblNldEltbWVkaWF0ZSA9IHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnXG4gICAgJiYgd2luZG93LnNldEltbWVkaWF0ZTtcbiAgICB2YXIgY2FuUG9zdCA9IHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnXG4gICAgJiYgd2luZG93LnBvc3RNZXNzYWdlICYmIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyXG4gICAgO1xuXG4gICAgaWYgKGNhblNldEltbWVkaWF0ZSkge1xuICAgICAgICByZXR1cm4gZnVuY3Rpb24gKGYpIHsgcmV0dXJuIHdpbmRvdy5zZXRJbW1lZGlhdGUoZikgfTtcbiAgICB9XG5cbiAgICBpZiAoY2FuUG9zdCkge1xuICAgICAgICB2YXIgcXVldWUgPSBbXTtcbiAgICAgICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ21lc3NhZ2UnLCBmdW5jdGlvbiAoZXYpIHtcbiAgICAgICAgICAgIHZhciBzb3VyY2UgPSBldi5zb3VyY2U7XG4gICAgICAgICAgICBpZiAoKHNvdXJjZSA9PT0gd2luZG93IHx8IHNvdXJjZSA9PT0gbnVsbCkgJiYgZXYuZGF0YSA9PT0gJ3Byb2Nlc3MtdGljaycpIHtcbiAgICAgICAgICAgICAgICBldi5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgICAgICAgICAgICBpZiAocXVldWUubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICAgICAgICB2YXIgZm4gPSBxdWV1ZS5zaGlmdCgpO1xuICAgICAgICAgICAgICAgICAgICBmbigpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSwgdHJ1ZSk7XG5cbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uIG5leHRUaWNrKGZuKSB7XG4gICAgICAgICAgICBxdWV1ZS5wdXNoKGZuKTtcbiAgICAgICAgICAgIHdpbmRvdy5wb3N0TWVzc2FnZSgncHJvY2Vzcy10aWNrJywgJyonKTtcbiAgICAgICAgfTtcbiAgICB9XG5cbiAgICByZXR1cm4gZnVuY3Rpb24gbmV4dFRpY2soZm4pIHtcbiAgICAgICAgc2V0VGltZW91dChmbiwgMCk7XG4gICAgfTtcbn0pKCk7XG5cbnByb2Nlc3MudGl0bGUgPSAnYnJvd3Nlcic7XG5wcm9jZXNzLmJyb3dzZXIgPSB0cnVlO1xucHJvY2Vzcy5lbnYgPSB7fTtcbnByb2Nlc3MuYXJndiA9IFtdO1xuXG5mdW5jdGlvbiBub29wKCkge31cblxucHJvY2Vzcy5vbiA9IG5vb3A7XG5wcm9jZXNzLmFkZExpc3RlbmVyID0gbm9vcDtcbnByb2Nlc3Mub25jZSA9IG5vb3A7XG5wcm9jZXNzLm9mZiA9IG5vb3A7XG5wcm9jZXNzLnJlbW92ZUxpc3RlbmVyID0gbm9vcDtcbnByb2Nlc3MucmVtb3ZlQWxsTGlzdGVuZXJzID0gbm9vcDtcbnByb2Nlc3MuZW1pdCA9IG5vb3A7XG5cbnByb2Nlc3MuYmluZGluZyA9IGZ1bmN0aW9uIChuYW1lKSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKCdwcm9jZXNzLmJpbmRpbmcgaXMgbm90IHN1cHBvcnRlZCcpO1xufVxuXG4vLyBUT0RPKHNodHlsbWFuKVxucHJvY2Vzcy5jd2QgPSBmdW5jdGlvbiAoKSB7IHJldHVybiAnLycgfTtcbnByb2Nlc3MuY2hkaXIgPSBmdW5jdGlvbiAoZGlyKSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKCdwcm9jZXNzLmNoZGlyIGlzIG5vdCBzdXBwb3J0ZWQnKTtcbn07XG4iLCJtb2R1bGUuZXhwb3J0cz1bIFxuICB7XCJuYW1lXCI6XCJBZmdoYW5pc3RhblwiLFwiY29kZVwiOlwiQUZcIixcImNvbnRpbmVudFwiOlwiTUVcIn0sXG4gIHtcIm5hbWVcIjpcIsOFbGFuZCBJc2xhbmRzXCIsXCJjb2RlXCI6XCJBWFwiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiQWxiYW5pYVwiLFwiY29kZVwiOlwiQUxcIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIkFsZ2VyaWFcIixcImNvZGVcIjpcIkRaXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJBbWVyaWNhbiBTYW1vYVwiLFwiY29kZVwiOlwiQVNcIixcImNvbnRpbmVudFwiOlwiT0NcIn0sXG4gIHtcIm5hbWVcIjpcIkFuZG9ycmFcIixcImNvZGVcIjpcIkFEXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJBbmdvbGFcIixcImNvZGVcIjpcIkFPXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJBbmd1aWxsYVwiLFwiY29kZVwiOlwiQUlcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIkFudGFyY3RpY2FcIixcImNvZGVcIjpcIkFRXCIsXCJjb250aW5lbnRcIjpcIkFOXCJ9LFxuICB7XCJuYW1lXCI6XCJBbnRpZ3VhIGFuZCBCYXJidWRhXCIsXCJjb2RlXCI6XCJBR1wiLFwiY29udGluZW50XCI6XCJDQVwifSxcbiAge1wibmFtZVwiOlwiQXJnZW50aW5hXCIsXCJjb2RlXCI6XCJBUlwiLFwiY29udGluZW50XCI6XCJTQVwifSxcbiAge1wibmFtZVwiOlwiQXJtZW5pYVwiLFwiY29kZVwiOlwiQU1cIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIkFydWJhXCIsXCJjb2RlXCI6XCJBV1wiLFwiY29udGluZW50XCI6XCJDQVwifSxcbiAge1wibmFtZVwiOlwiQXVzdHJhbGlhXCIsXCJjb2RlXCI6XCJBVVwiLFwiY29udGluZW50XCI6XCJPQ1wifSxcbiAge1wibmFtZVwiOlwiQXVzdHJpYVwiLFwiY29kZVwiOlwiQVRcIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIkF6ZXJiYWlqYW5cIixcImNvZGVcIjpcIkFaXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJCYWhhbWFzXCIsXCJjb2RlXCI6XCJCU1wiLFwiY29udGluZW50XCI6XCJDQVwifSxcbiAge1wibmFtZVwiOlwiQmFocmFpblwiLFwiY29kZVwiOlwiQkhcIixcImNvbnRpbmVudFwiOlwiTUVcIn0sXG4gIHtcIm5hbWVcIjpcIkJhbmdsYWRlc2hcIixcImNvZGVcIjpcIkJEXCIsXCJjb250aW5lbnRcIjpcIkFTXCJ9LFxuICB7XCJuYW1lXCI6XCJCYXJiYWRvc1wiLFwiY29kZVwiOlwiQkJcIixcImNvbnRpbmVudFwiOlwiQ0FcIn0sXG4gIHtcIm5hbWVcIjpcIkJlbGFydXNcIixcImNvZGVcIjpcIkJZXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJCZWxnaXVtXCIsXCJjb2RlXCI6XCJCRVwiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiQmVuZWx1eFwiLFwiY29kZVwiOlwiQlhcIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIkJlbGl6ZVwiLFwiY29kZVwiOlwiQlpcIixcImNvbnRpbmVudFwiOlwiQ0FcIn0sXG4gIHtcIm5hbWVcIjpcIkJlbmluXCIsXCJjb2RlXCI6XCJCSlwiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiQmVybXVkYVwiLFwiY29kZVwiOlwiQk1cIixcImNvbnRpbmVudFwiOlwiQVNcIn0sXG4gIHtcIm5hbWVcIjpcIkJodXRhblwiLFwiY29kZVwiOlwiQlRcIixcImNvbnRpbmVudFwiOlwiQVNcIn0sXG4gIHtcIm5hbWVcIjpcIkJvbGl2aWFcIixcImNvZGVcIjpcIkJPXCIsXCJjb250aW5lbnRcIjpcIlNBXCJ9LFxuICB7XCJuYW1lXCI6XCJCb3NuaWFcIixcImNvZGVcIjpcIkJBXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJCb3Rzd2FuYVwiLFwiY29kZVwiOlwiQldcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIkJvdXZldCBJc2xhbmRcIixcImNvZGVcIjpcIkJWXCIsXCJjb250aW5lbnRcIjpcIkFOXCJ9LFxuICB7XCJuYW1lXCI6XCJCcmF6aWxcIixcImNvZGVcIjpcIkJSXCIsXCJjb250aW5lbnRcIjpcIlNBXCJ9LFxuICB7XCJuYW1lXCI6XCJCcml0aXNoIEluZGlhblwiLFwiY29kZVwiOlwiSU9cIixcImNvbnRpbmVudFwiOlwiQVNcIn0sXG4gIHtcIm5hbWVcIjpcIkJydW5laSBEYXJ1c3NhbGFtXCIsXCJjb2RlXCI6XCJCTlwiLFwiY29udGluZW50XCI6XCJBU1wifSxcbiAge1wibmFtZVwiOlwiQnVsZ2FyaWFcIixcImNvZGVcIjpcIkJHXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJCdXJraW5hIEZhc29cIixcImNvZGVcIjpcIkJGXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJCdXJ1bmRpXCIsXCJjb2RlXCI6XCJCSVwiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiQ2FtYm9kaWFcIixcImNvZGVcIjpcIktIXCIsXCJjb250aW5lbnRcIjpcIkFTXCJ9LFxuICB7XCJuYW1lXCI6XCJDYW1lcm9vblwiLFwiY29kZVwiOlwiQ01cIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIkNhbmFkYVwiLFwiY29kZVwiOlwiQ0FcIixcImNvbnRpbmVudFwiOlwiTkFcIn0sXG4gIHtcIm5hbWVcIjpcIkNhcGUgVmVyZGVcIixcImNvZGVcIjpcIkNWXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJDYXltYW4gSXNsYW5kc1wiLFwiY29kZVwiOlwiS1lcIixcImNvbnRpbmVudFwiOlwiQ0FcIn0sXG4gIHtcIm5hbWVcIjpcIkNlbnRyYWwgQWZyaWNhbiBSZXB1YmxpY1wiLFwiY29kZVwiOlwiQ0ZcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIkNoYWRcIixcImNvZGVcIjpcIlREXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJDaGlsZVwiLFwiY29kZVwiOlwiQ0xcIixcImNvbnRpbmVudFwiOlwiU0FcIn0sXG4gIHtcIm5hbWVcIjpcIkNoaW5hXCIsXCJjb2RlXCI6XCJDTlwiLFwiY29udGluZW50XCI6XCJBU1wifSxcbiAge1wibmFtZVwiOlwiQ2hyaXN0bWFzIElzbGFuZFwiLFwiY29kZVwiOlwiQ1hcIixcImNvbnRpbmVudFwiOlwiQVNcIn0sXG4gIHtcIm5hbWVcIjpcIkNvY29zIChLZWVsaW5nKVwiLFwiY29kZVwiOlwiQ0NcIixcImNvbnRpbmVudFwiOlwiQVNcIn0sXG4gIHtcIm5hbWVcIjpcIkNvbG9tYmlhXCIsXCJjb2RlXCI6XCJDT1wiLFwiY29udGluZW50XCI6XCJTQVwifSxcbiAge1wibmFtZVwiOlwiQ29tb3Jvc1wiLFwiY29kZVwiOlwiS01cIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIlJlcHVibGljIG9mIHRoZSBDb25nb1wiLFwiY29kZVwiOlwiQ0dcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIkRlbW9jcmF0aWMgUmVwdWJsaWMgb2YgdGhlIENvbmdvXCIsXCJjb2RlXCI6XCJDRFwiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiQ29vayBJc2xhbmRzXCIsXCJjb2RlXCI6XCJDS1wiLFwiY29udGluZW50XCI6XCJPQ1wifSxcbiAge1wibmFtZVwiOlwiQ29zdGEgUmljYVwiLFwiY29kZVwiOlwiQ1JcIixcImNvbnRpbmVudFwiOlwiQ0FcIn0sXG4gIHtcIm5hbWVcIjogXCJJdm9yeSBDb2FzdFwiLCBcImNvZGVcIjogXCJDSVwiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiQ3JvYXRpYVwiLFwiY29kZVwiOlwiSFJcIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIkN1YmFcIixcImNvZGVcIjpcIkNVXCIsXCJjb250aW5lbnRcIjpcIkNBXCJ9LFxuICB7XCJuYW1lXCI6XCJDeXBydXNcIixcImNvZGVcIjpcIkNZXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJDemVjaCBSZXB1YmxpY1wiLFwiY29kZVwiOlwiQ1pcIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIkRlbm1hcmtcIixcImNvZGVcIjpcIkRLXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJEamlib3V0aVwiLFwiY29kZVwiOlwiREpcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIkRvbWluaWNhXCIsXCJjb2RlXCI6XCJETVwiLFwiY29udGluZW50XCI6XCJDQVwifSxcbiAge1wibmFtZVwiOlwiRG9taW5pY2FuIFJlcHVibGljXCIsXCJjb2RlXCI6XCJET1wiLFwiY29udGluZW50XCI6XCJDQVwifSxcbiAge1wibmFtZVwiOlwiRWN1YWRvclwiLFwiY29kZVwiOlwiRUNcIixcImNvbnRpbmVudFwiOlwiU0FcIn0sXG4gIHtcIm5hbWVcIjpcIkVneXB0XCIsXCJjb2RlXCI6XCJFR1wiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiRWwgU2FsdmFkb3JcIixcImNvZGVcIjpcIlNWXCIsXCJjb250aW5lbnRcIjpcIkNBXCJ9LFxuICB7XCJuYW1lXCI6XCJFcXVhdG9yaWFsIEd1aW5lYVwiLFwiY29kZVwiOlwiR1FcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIkVyaXRyZWFcIixcImNvZGVcIjpcIkVSXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJFc3RvbmlhXCIsXCJjb2RlXCI6XCJFRVwiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiRXRoaW9waWFcIixcImNvZGVcIjpcIkVUXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJFdXJvcGVhbiBVbmlvblwiLFwiY29kZVwiOlwiRVVcIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIkZhbGtsYW5kIElzbGFuZHNcIixcImNvZGVcIjpcIkZLXCIsXCJjb250aW5lbnRcIjpcIlNBXCJ9LFxuICB7XCJuYW1lXCI6XCJGYXJvZSBJc2xhbmRzXCIsXCJjb2RlXCI6XCJGT1wiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiRmlqaVwiLFwiY29kZVwiOlwiRkpcIixcImNvbnRpbmVudFwiOlwiT0NcIn0sXG4gIHtcIm5hbWVcIjpcIkZpbmxhbmRcIixcImNvZGVcIjpcIkZJXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJGcmFuY2VcIixcImNvZGVcIjpcIkZSXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJGcmVuY2ggR3VpYW5hXCIsXCJjb2RlXCI6XCJHRlwiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiRnJlbmNoIFBvbHluZXNpYVwiLFwiY29kZVwiOlwiUEZcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIkZyZW5jaCBTb3V0aGVyblwiLFwiY29kZVwiOlwiVEZcIixcImNvbnRpbmVudFwiOlwiQU5cIn0sXG4gIHtcIm5hbWVcIjpcIkdhYm9uXCIsXCJjb2RlXCI6XCJHQVwiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiR2FtYmlhXCIsXCJjb2RlXCI6XCJHTVwiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiR2VvcmdpYVwiLFwiY29kZVwiOlwiR0VcIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIkdlcm1hbnlcIixcImNvZGVcIjpcIkRFXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJHaGFuYVwiLFwiY29kZVwiOlwiR0hcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIkdpYnJhbHRhclwiLFwiY29kZVwiOlwiR0lcIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIkdyZWVjZVwiLFwiY29kZVwiOlwiR1JcIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIkdyZWVubGFuZFwiLFwiY29kZVwiOlwiR0xcIixcImNvbnRpbmVudFwiOlwiQ0FcIn0sXG4gIHtcIm5hbWVcIjpcIkdyZW5hZGFcIixcImNvZGVcIjpcIkdEXCIsXCJjb250aW5lbnRcIjpcIkNBXCJ9LFxuICB7XCJuYW1lXCI6XCJHdWFkZWxvdXBlXCIsXCJjb2RlXCI6XCJHUFwiLFwiY29udGluZW50XCI6XCJDQVwifSxcbiAge1wibmFtZVwiOlwiR3VhbVwiLFwiY29kZVwiOlwiR1VcIixcImNvbnRpbmVudFwiOlwiT0NcIn0sXG4gIHtcIm5hbWVcIjpcIkd1YXRlbWFsYVwiLFwiY29kZVwiOlwiR1RcIixcImNvbnRpbmVudFwiOlwiQ0FcIn0sXG4gIHtcIm5hbWVcIjpcIkd1ZXJuc2V5XCIsXCJjb2RlXCI6XCJHR1wiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiR3VpbmVhXCIsXCJjb2RlXCI6XCJHTlwiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiR3VpbmVhLUJpc3NhdVwiLFwiY29kZVwiOlwiR1dcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIkd1eWFuYVwiLFwiY29kZVwiOlwiR1lcIixcImNvbnRpbmVudFwiOlwiU0FcIn0sXG4gIHtcIm5hbWVcIjpcIkhhaXRpXCIsXCJjb2RlXCI6XCJIVFwiLFwiY29udGluZW50XCI6XCJDQVwifSxcbiAge1wibmFtZVwiOlwiSGVhcmQgYW5kIE1jZG9uYWxkXCIsXCJjb2RlXCI6XCJITVwiLFwiY29udGluZW50XCI6XCJBTlwifSxcbiAge1wibmFtZVwiOlwiSG9seSBTZWVcIixcImNvZGVcIjpcIlZBXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJIb25kdXJhc1wiLFwiY29kZVwiOlwiSE5cIixcImNvbnRpbmVudFwiOlwiQ0FcIn0sXG4gIHtcIm5hbWVcIjpcIkhvbmcgS29uZ1wiLFwiY29kZVwiOlwiSEtcIixcImNvbnRpbmVudFwiOlwiQVNcIn0sXG4gIHtcIm5hbWVcIjpcIkh1bmdhcnlcIixcImNvZGVcIjpcIkhVXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJJY2VsYW5kXCIsXCJjb2RlXCI6XCJJU1wiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiSW5kaWFcIixcImNvZGVcIjpcIklOXCIsXCJjb250aW5lbnRcIjpcIkFTXCJ9LFxuICB7XCJuYW1lXCI6XCJJbmRvbmVzaWFcIixcImNvZGVcIjpcIklEXCIsXCJjb250aW5lbnRcIjpcIkFTXCJ9LFxuICB7XCJuYW1lXCI6XCJJcmFuXCIsXCJjb2RlXCI6XCJJUlwiLFwiY29udGluZW50XCI6XCJBU1wifSxcbiAge1wibmFtZVwiOlwiSXJhcVwiLFwiY29kZVwiOlwiSVFcIixcImNvbnRpbmVudFwiOlwiTUVcIn0sXG4gIHtcIm5hbWVcIjpcIklyZWxhbmRcIixcImNvZGVcIjpcIklFXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJJc2xlIG9mIE1hblwiLFwiY29kZVwiOlwiSU1cIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIklzcmFlbFwiLFwiY29kZVwiOlwiSUxcIixcImNvbnRpbmVudFwiOlwiTUVcIn0sXG4gIHtcIm5hbWVcIjpcIkl0YWx5XCIsXCJjb2RlXCI6XCJJVFwiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiSmFtYWljYVwiLFwiY29kZVwiOlwiSk1cIixcImNvbnRpbmVudFwiOlwiQ0FcIn0sXG4gIHtcIm5hbWVcIjpcIkphcGFuXCIsXCJjb2RlXCI6XCJKUFwiLFwiY29udGluZW50XCI6XCJBU1wifSxcbiAge1wibmFtZVwiOlwiSmVyc2V5XCIsXCJjb2RlXCI6XCJKRVwiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiSm9yZGFuXCIsXCJjb2RlXCI6XCJKT1wiLFwiY29udGluZW50XCI6XCJNRVwifSxcbiAge1wibmFtZVwiOlwiS2F6YWtoc3RhblwiLFwiY29kZVwiOlwiS1pcIixcImNvbnRpbmVudFwiOlwiQVNcIn0sXG4gIHtcIm5hbWVcIjpcIktlbnlhXCIsXCJjb2RlXCI6XCJLRVwiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiS2lyaWJhdGlcIixcImNvZGVcIjpcIktJXCIsXCJjb250aW5lbnRcIjpcIk9DXCJ9LFxuICB7XCJuYW1lXCI6XCJOb3J0aCBLb3JlYVwiLFwiY29kZVwiOlwiS1BcIixcImNvbnRpbmVudFwiOlwiQVNcIn0sXG4gIHtcIm5hbWVcIjpcIlNvdXRoIEtvcmVhXCIsXCJjb2RlXCI6XCJLUlwiLFwiY29udGluZW50XCI6XCJBU1wifSxcbiAge1wibmFtZVwiOlwiS29zb3ZvXCIsXCJjb2RlXCI6XCJLT1wiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiS3V3YWl0XCIsXCJjb2RlXCI6XCJLV1wiLFwiY29udGluZW50XCI6XCJNRVwifSxcbiAge1wibmFtZVwiOlwiS3lyZ3l6c3RhblwiLFwiY29kZVwiOlwiS0dcIixcImNvbnRpbmVudFwiOlwiQVNcIn0sXG4gIHtcIm5hbWVcIjpcIkxhb3NcIixcImNvZGVcIjpcIkxBXCIsXCJjb250aW5lbnRcIjpcIkFTXCJ9LFxuICB7XCJuYW1lXCI6XCJMYXR2aWFcIixcImNvZGVcIjpcIkxWXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJMZWJhbm9uXCIsXCJjb2RlXCI6XCJMQlwiLFwiY29udGluZW50XCI6XCJNRVwifSxcbiAge1wibmFtZVwiOlwiTGVzb3Rob1wiLFwiY29kZVwiOlwiTFNcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIkxpYmVyaWFcIixcImNvZGVcIjpcIkxSXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJMaWJ5YW5cIixcImNvZGVcIjpcIkxZXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJMaWVjaHRlbnN0ZWluXCIsXCJjb2RlXCI6XCJMSVwiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiTGl0aHVhbmlhXCIsXCJjb2RlXCI6XCJMVFwiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiTHV4ZW1ib3VyZ1wiLFwiY29kZVwiOlwiTFVcIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIk1hY2FvXCIsXCJjb2RlXCI6XCJNT1wiLFwiY29udGluZW50XCI6XCJBU1wifSxcbiAge1wibmFtZVwiOlwiTWFjZWRvbmlhXCIsXCJjb2RlXCI6XCJNS1wiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiTWFkYWdhc2NhclwiLFwiY29kZVwiOlwiTUdcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIk1hbGF3aVwiLFwiY29kZVwiOlwiTVdcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIk1hbGF5c2lhXCIsXCJjb2RlXCI6XCJNWVwiLFwiY29udGluZW50XCI6XCJBU1wifSxcbiAge1wibmFtZVwiOlwiTWFsZGl2ZXNcIixcImNvZGVcIjpcIk1WXCIsXCJjb250aW5lbnRcIjpcIkFTXCJ9LFxuICB7XCJuYW1lXCI6XCJNYWxpXCIsXCJjb2RlXCI6XCJNTFwiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiTWFsdGFcIixcImNvZGVcIjpcIk1UXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJNYXJzaGFsbCBJc2xhbmRzXCIsXCJjb2RlXCI6XCJNSFwiLFwiY29udGluZW50XCI6XCJPQ1wifSxcbiAge1wibmFtZVwiOlwiTWFydGluaXF1ZVwiLFwiY29kZVwiOlwiTVFcIixcImNvbnRpbmVudFwiOlwiQ0FcIn0sXG4gIHtcIm5hbWVcIjpcIk1hdXJpdGFuaWFcIixcImNvZGVcIjpcIk1SXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJNYXVyaXRpdXNcIixcImNvZGVcIjpcIk1VXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJNYXlvdHRlXCIsXCJjb2RlXCI6XCJZVFwiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiTWV4aWNvXCIsXCJjb2RlXCI6XCJNWFwiLFwiY29udGluZW50XCI6XCJOQVwifSxcbiAge1wibmFtZVwiOlwiTWljcm9uZXNpYVwiLFwiY29kZVwiOlwiRk1cIixcImNvbnRpbmVudFwiOlwiT0NcIn0sXG4gIHtcIm5hbWVcIjpcIk1vbGRvdmFcIixcImNvZGVcIjpcIk1EXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJNb25hY29cIixcImNvZGVcIjpcIk1DXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJNb25nb2xpYVwiLFwiY29kZVwiOlwiTU5cIixcImNvbnRpbmVudFwiOlwiQVNcIn0sXG4gIHtcIm5hbWVcIjpcIk1vbnRzZXJyYXRcIixcImNvZGVcIjpcIk1TXCIsXCJjb250aW5lbnRcIjpcIkNBXCJ9LFxuICB7XCJuYW1lXCI6XCJNb3JvY2NvXCIsXCJjb2RlXCI6XCJNQVwiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiTW96YW1iaXF1ZVwiLFwiY29kZVwiOlwiTVpcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIk15YW5tYXJcIixcImNvZGVcIjpcIk1NXCIsXCJjb250aW5lbnRcIjpcIkFTXCJ9LFxuICB7XCJuYW1lXCI6XCJOYW1pYmlhXCIsXCJjb2RlXCI6XCJOQVwiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiTmF1cnVcIixcImNvZGVcIjpcIk5SXCIsXCJjb250aW5lbnRcIjpcIk9DXCJ9LFxuICB7XCJuYW1lXCI6XCJOZXBhbFwiLFwiY29kZVwiOlwiTlBcIixcImNvbnRpbmVudFwiOlwiQVNcIn0sXG4gIHtcIm5hbWVcIjpcIk5ldGhlcmxhbmRzXCIsXCJjb2RlXCI6XCJOTFwiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiTmV0aGVybGFuZHMgQW50aWxsZXNcIixcImNvZGVcIjpcIkFOXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJOZXcgQ2FsZWRvbmlhXCIsXCJjb2RlXCI6XCJOQ1wiLFwiY29udGluZW50XCI6XCJPQ1wifSxcbiAge1wibmFtZVwiOlwiTmV3IFplYWxhbmRcIixcImNvZGVcIjpcIk5aXCIsXCJjb250aW5lbnRcIjpcIk9DXCJ9LFxuICB7XCJuYW1lXCI6XCJOaWNhcmFndWFcIixcImNvZGVcIjpcIk5JXCIsXCJjb250aW5lbnRcIjpcIkNBXCJ9LFxuICB7XCJuYW1lXCI6XCJOaWdlclwiLFwiY29kZVwiOlwiTkVcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIk5pZ2VyaWFcIixcImNvZGVcIjpcIk5HXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJOaXVlXCIsXCJjb2RlXCI6XCJOVVwiLFwiY29udGluZW50XCI6XCJPQ1wifSxcbiAge1wibmFtZVwiOlwiTm9yZm9sayBJc2xhbmRcIixcImNvZGVcIjpcIk5GXCIsXCJjb250aW5lbnRcIjpcIk9DXCJ9LFxuICB7XCJuYW1lXCI6XCJOb3J0aGVybiBNYXJpYW5hXCIsXCJjb2RlXCI6XCJNUFwiLFwiY29udGluZW50XCI6XCJPQ1wifSxcbiAge1wibmFtZVwiOlwiTm9yd2F5XCIsXCJjb2RlXCI6XCJOT1wiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiT21hblwiLFwiY29kZVwiOlwiT01cIixcImNvbnRpbmVudFwiOlwiTUVcIn0sXG4gIHtcIm5hbWVcIjpcIlBha2lzdGFuXCIsXCJjb2RlXCI6XCJQS1wiLFwiY29udGluZW50XCI6XCJBU1wifSxcbiAge1wibmFtZVwiOlwiUGFsYXVcIixcImNvZGVcIjpcIlBXXCIsXCJjb250aW5lbnRcIjpcIk9DXCJ9LFxuICB7XCJuYW1lXCI6XCJQYWxlc3RpbmVcIixcImNvZGVcIjpcIlBTXCIsXCJjb250aW5lbnRcIjpcIk1FXCJ9LFxuICB7XCJuYW1lXCI6XCJQYW5hbWFcIixcImNvZGVcIjpcIlBBXCIsXCJjb250aW5lbnRcIjpcIkNBXCJ9LFxuICB7XCJuYW1lXCI6XCJQYXB1YSBOZXcgR3VpbmVhXCIsXCJjb2RlXCI6XCJQR1wiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiUGFyYWd1YXlcIixcImNvZGVcIjpcIlBZXCIsXCJjb250aW5lbnRcIjpcIlNBXCJ9LFxuICB7XCJuYW1lXCI6XCJQZXJ1XCIsXCJjb2RlXCI6XCJQRVwiLFwiY29udGluZW50XCI6XCJTQVwifSxcbiAge1wibmFtZVwiOlwiUGhpbGlwcGluZXNcIixcImNvZGVcIjpcIlBIXCIsXCJjb250aW5lbnRcIjpcIkFTXCJ9LFxuICB7XCJuYW1lXCI6XCJQaXRjYWlyblwiLFwiY29kZVwiOlwiUE5cIixcImNvbnRpbmVudFwiOlwiT0NcIn0sXG4gIHtcIm5hbWVcIjpcIlBvbGFuZFwiLFwiY29kZVwiOlwiUExcIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIlBvcnR1Z2FsXCIsXCJjb2RlXCI6XCJQVFwiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiUHVlcnRvIFJpY29cIixcImNvZGVcIjpcIlBSXCIsXCJjb250aW5lbnRcIjpcIkNBXCJ9LFxuICB7XCJuYW1lXCI6XCJRYXRhclwiLFwiY29kZVwiOlwiUUFcIixcImNvbnRpbmVudFwiOlwiTUVcIn0sXG4gIHtcIm5hbWVcIjpcIlJldW5pb25cIixcImNvZGVcIjpcIlJFXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJSb21hbmlhXCIsXCJjb2RlXCI6XCJST1wiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiUnVzc2lhbiBGZWRlcmF0aW9uXCIsXCJjb2RlXCI6XCJSVVwiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiUldBTkRBXCIsXCJjb2RlXCI6XCJSV1wiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiU2FpbnQgSGVsZW5hXCIsXCJjb2RlXCI6XCJTSFwiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiU2FpbnQgS2l0dHMgYW5kIE5ldmlzXCIsXCJjb2RlXCI6XCJLTlwiLFwiY29udGluZW50XCI6XCJDQVwifSxcbiAge1wibmFtZVwiOlwiU2FpbnQgTHVjaWFcIixcImNvZGVcIjpcIkxDXCIsXCJjb250aW5lbnRcIjpcIkNBXCJ9LFxuICB7XCJuYW1lXCI6XCJTYWludCBQaWVycmUgYW5kIE1pcXVlbG9uXCIsXCJjb2RlXCI6XCJQTVwiLFwiY29udGluZW50XCI6XCJDQVwifSxcbiAge1wibmFtZVwiOlwiU2FpbnQgVmluY2VudFwiLFwiY29kZVwiOlwiVkNcIixcImNvbnRpbmVudFwiOlwiQ0FcIn0sXG4gIHtcIm5hbWVcIjpcIlNhbW9hXCIsXCJjb2RlXCI6XCJXU1wiLFwiY29udGluZW50XCI6XCJPQ1wifSxcbiAge1wibmFtZVwiOlwiU2FuIE1hcmlub1wiLFwiY29kZVwiOlwiU01cIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIlNhbyBUb21lIGFuZCBQcmluY2lwZVwiLFwiY29kZVwiOlwiU1RcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIlNhdWRpIEFyYWJpYVwiLFwiY29kZVwiOlwiU0FcIixcImNvbnRpbmVudFwiOlwiTUVcIn0sXG4gIHtcIm5hbWVcIjpcIlNlbmVnYWxcIixcImNvZGVcIjpcIlNOXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJTZXJiaWEgYW5kIE1vbnRlbmVncm9cIixcImNvZGVcIjpcIkNTXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJTZXJiaWFcIixcImNvZGVcIjpcIlJTXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJNb250ZW5lZ3JvXCIsXCJjb2RlXCI6XCJNRVwiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiU2V5Y2hlbGxlc1wiLFwiY29kZVwiOlwiU0NcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIlNpZXJyYSBMZW9uZVwiLFwiY29kZVwiOlwiU0xcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIlNpbnQgTWFhcnRlblwiLFwiY29kZVwiOlwiU1hcIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIlNpbmdhcG9yZVwiLFwiY29kZVwiOlwiU0dcIixcImNvbnRpbmVudFwiOlwiQVNcIn0sXG4gIHtcIm5hbWVcIjpcIlNsb3Zha2lhXCIsXCJjb2RlXCI6XCJTS1wiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiU2xvdmVuaWFcIixcImNvZGVcIjpcIlNJXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJTb2xvbW9uIElzbGFuZHNcIixcImNvZGVcIjpcIlNCXCIsXCJjb250aW5lbnRcIjpcIk9DXCJ9LFxuICB7XCJuYW1lXCI6XCJTb21hbGlhXCIsXCJjb2RlXCI6XCJTT1wiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiU291dGggQWZyaWNhXCIsXCJjb2RlXCI6XCJaQVwiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiU291dGggR2VvcmdpYVwiLFwiY29kZVwiOlwiR1NcIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIlNwYWluXCIsXCJjb2RlXCI6XCJFU1wiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiU3JpIExhbmthXCIsXCJjb2RlXCI6XCJMS1wiLFwiY29udGluZW50XCI6XCJBU1wifSxcbiAge1wibmFtZVwiOlwiU3VkYW5cIixcImNvZGVcIjpcIlNEXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJTdXJpIG5hbWVcIixcImNvZGVcIjpcIlNSXCIsXCJjb250aW5lbnRcIjpcIlNBXCJ9LFxuICB7XCJuYW1lXCI6XCJTdmFsYmFyZCBhbmQgSmFuIE1heWVuXCIsXCJjb2RlXCI6XCJTSlwiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiU3dhemlsYW5kXCIsXCJjb2RlXCI6XCJTWlwiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiU3dlZGVuXCIsXCJjb2RlXCI6XCJTRVwiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiU3dpdHplcmxhbmRcIixcImNvZGVcIjpcIkNIXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJTeXJpYW4gQXJhYiBSZXB1YmxpY1wiLFwiY29kZVwiOlwiU1lcIixcImNvbnRpbmVudFwiOlwiQVNcIn0sXG4gIHtcIm5hbWVcIjpcIlRhaXdhblwiLFwiY29kZVwiOlwiVFdcIixcImNvbnRpbmVudFwiOlwiQVNcIn0sXG4gIHtcIm5hbWVcIjpcIlRhamlraXN0YW5cIixcImNvZGVcIjpcIlRKXCIsXCJjb250aW5lbnRcIjpcIkFTXCJ9LFxuICB7XCJuYW1lXCI6XCJUYW56YW5pYVwiLFwiY29kZVwiOlwiVFpcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIlRoYWlsYW5kXCIsXCJjb2RlXCI6XCJUSFwiLFwiY29udGluZW50XCI6XCJBU1wifSxcbiAge1wibmFtZVwiOlwiRWFzdCBUaW1vclwiLFwiY29kZVwiOlwiVExcIixcImNvbnRpbmVudFwiOlwiQVNcIn0sXG4gIHtcIm5hbWVcIjpcIlRvZ29cIixcImNvZGVcIjpcIlRHXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJUb2tlbGF1XCIsXCJjb2RlXCI6XCJUS1wiLFwiY29udGluZW50XCI6XCJPQ1wifSxcbiAge1wibmFtZVwiOlwiVG9uZ2FcIixcImNvZGVcIjpcIlRPXCIsXCJjb250aW5lbnRcIjpcIk9DXCJ9LFxuICB7XCJuYW1lXCI6XCJUcmluaWRhZCBhbmQgVG9iYWdvXCIsXCJjb2RlXCI6XCJUVFwiLFwiY29udGluZW50XCI6XCJDQVwifSxcbiAge1wibmFtZVwiOlwiVHVuaXNpYVwiLFwiY29kZVwiOlwiVE5cIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIlR1cmtleVwiLFwiY29kZVwiOlwiVFJcIixcImNvbnRpbmVudFwiOlwiTUVcIn0sXG4gIHtcIm5hbWVcIjpcIlR1cmttZW5pc3RhblwiLFwiY29kZVwiOlwiVE1cIixcImNvbnRpbmVudFwiOlwiQVNcIn0sXG4gIHtcIm5hbWVcIjpcIlR1cmtzIGFuZCBDYWljb3MgSXNsYW5kc1wiLFwiY29kZVwiOlwiVENcIixcImNvbnRpbmVudFwiOlwiQ0FcIn0sXG4gIHtcIm5hbWVcIjpcIlR1dmFsdVwiLFwiY29kZVwiOlwiVFZcIixcImNvbnRpbmVudFwiOlwiT0NcIn0sXG4gIHtcIm5hbWVcIjpcIlVnYW5kYVwiLFwiY29kZVwiOlwiVUdcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIlVrcmFpbmVcIixcImNvZGVcIjpcIlVBXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJVbml0ZWQgQXJhYiBFbWlyYXRlc1wiLFwiY29kZVwiOlwiQUVcIixcImNvbnRpbmVudFwiOlwiTUVcIn0sXG4gIHtcIm5hbWVcIjpcIlVuaXRlZCBLaW5nZG9tXCIsXCJjb2RlXCI6XCJHQlwiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiVW5pdGVkIFN0YXRlc1wiLFwiY29kZVwiOlwiVVNcIixcImNvbnRpbmVudFwiOlwiTkFcIn0sXG4gIHtcIm5hbWVcIjpcIlVydWd1YXlcIixcImNvZGVcIjpcIlVZXCIsXCJjb250aW5lbnRcIjpcIlNBXCJ9LFxuICB7XCJuYW1lXCI6XCJVemJla2lzdGFuXCIsXCJjb2RlXCI6XCJVWlwiLFwiY29udGluZW50XCI6XCJBU1wifSxcbiAge1wibmFtZVwiOlwiVmFudWF0dVwiLFwiY29kZVwiOlwiVlVcIixcImNvbnRpbmVudFwiOlwiT0NcIn0sXG4gIHtcIm5hbWVcIjpcIlZlbmV6dWVsYVwiLFwiY29kZVwiOlwiVkVcIixcImNvbnRpbmVudFwiOlwiU0FcIn0sXG4gIHtcIm5hbWVcIjpcIlZpZXRuYW1cIixcImNvZGVcIjpcIlZOXCIsXCJjb250aW5lbnRcIjpcIkFTXCJ9LFxuICB7XCJuYW1lXCI6XCJCcml0aXNoIFZpcmdpbiBJc2xhbmRzXCIsXCJjb2RlXCI6XCJWR1wiLFwiY29udGluZW50XCI6XCJDQVwifSxcbiAge1wibmFtZVwiOlwiVmlyZ2luIElzbGFuZHMsIFUuUy5cIixcImNvZGVcIjpcIlZJXCIsXCJjb250aW5lbnRcIjpcIkNBXCJ9LFxuICB7XCJuYW1lXCI6XCJXYWxsaXMgYW5kIEZ1dHVuYVwiLFwiY29kZVwiOlwiV0ZcIixcImNvbnRpbmVudFwiOlwiT0NcIn0sXG4gIHtcIm5hbWVcIjpcIldlc3Rlcm4gU2FoYXJhXCIsXCJjb2RlXCI6XCJFSFwiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiWWVtZW5cIixcImNvZGVcIjpcIllFXCIsXCJjb250aW5lbnRcIjpcIk1FXCJ9LFxuICB7XCJuYW1lXCI6XCJaYW1iaWFcIixcImNvZGVcIjpcIlpNXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJaaW1iYWJ3ZVwiLFwiY29kZVwiOlwiWldcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIkFSSVBPIFRyZWF0eVwiLFwiY29kZVwiOlwiQVJJUE9cIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIk9BUEkgVHJlYXR5XCIsXCJjb2RlXCI6XCJPQVBJXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJLdXJkaXN0YW4gUmVnaW9uXCIsXCJjb2RlXCI6XCJLUkRcIixcImNvbnRpbmVudFwiOlwiTUVcIn0sXG4gIHtcIm5hbWVcIjpcIlphbnppYmFyXCIsXCJjb2RlXCI6XCJFQVpcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIlNhaW50IE1hcnRpblwiLFwiY29kZVwiOlwiTUFGXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LCBcbiAge1wibmFtZVwiOlwiU2FpbnQgVmluY2VudCBhbmQgdGhlIEdyZW5hZGluZXNcIixcImNvZGVcIjpcIlZDVFwiLFwiY29udGluZW50XCI6XCJPQ1wifVxuXVxuIiwidmFyIGNvdW50cmllcyA9IHJlcXVpcmUoJy4vY291bnRyaWVzLmpzb24nKVxudmFyIHN0YXRlcyA9IHJlcXVpcmUoJy4vc3RhdGVzLmpzb24nKVxuXG4vL2NvbnNvbGUubG9nKGNvdW50cmllcylcbi8qXG4gQUZcdEFmcmljYVxuIEFOXHRBbnRhcmN0aWNhXG4gQVNcdEFzaWFcbiBFVVx0RXVyb3BlXG4gTkFcdE5vcnRoIGFtZXJpY2FcbiBPQ1x0T2NlYW5pYVxuIFNBXHRTb3V0aCBhbWVyaWNhXG4gKi9cblxuZnVuY3Rpb24gYWxsX2NvdW50cmllcygpIHtcbiAgcmV0dXJuIGNvdW50cmllc1xufVxuXG5mdW5jdGlvbiBnZXRfY291bnRyeShjb2RlKSB7XG4gIGZvciAodmFyIGkgPSAwOyBpIDwgY291bnRyaWVzLmxlbmd0aDsgaSsrKSB7XG4gICAgaWYgKGNvdW50cmllc1tpXS5jb2RlID09PSBjb2RlKSB7XG4gICAgICByZXR1cm4gY291bnRyaWVzW2ldXG4gICAgfVxuICB9XG59XG5cbnZhciBjb3VudHJ5X2FsaWFzID0ge1xuICBcIlRIRSBHQU1CSUFcIjogXCJHQU1CSUFcIixcbiAgXCJHVUlORUEgQklTU0FVXCI6IFwiR1VJTkVBLUJJU1NBVVwiLFxuICBcIlNBSU5UIFZJTkNFTlQgQU5EIEdSRU5BRElORVNcIjogXCJTQUlOVCBWSU5DRU5UXCIsXG4gIFwiUEFMRVNUSU5JQU4gVEVSUklUT1JZXCI6IFwiUEFMRVNUSU5FXCJcbn1cblxuZnVuY3Rpb24gZ2V0X2NvdW50cnlfYnlfbmFtZShuYW1lKSB7XG4gIHZhciBrbmFtZSA9IG5hbWUucmVwbGFjZSgvLXxffCUyMC9nLCAnICcpLnRvVXBwZXJDYXNlKCkgLy8gaGFuZGxlIEFtZXJpY2FuLVNhbW9hIG9yIEFtXG5cbiAgY29uc29sZS5sb2coY291bnRyeV9hbGlhc1trbmFtZV0sIGtuYW1lKVxuICBpZiAoY291bnRyeV9hbGlhc1trbmFtZV0pXG4gICAga25hbWUgPSBjb3VudHJ5X2FsaWFzW2tuYW1lXVxuXG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBjb3VudHJpZXMubGVuZ3RoOyBpKyspIHtcbiAgICBpZiAoY291bnRyaWVzW2ldLm5hbWUudG9VcHBlckNhc2UoKSA9PT0ga25hbWUpXG4gICAgICByZXR1cm4gY291bnRyaWVzW2ldXG4gIH1cbn1cblxuLy8gZ2V0X2NvdW50cnlfYnlfcmVnZXgoL15cXC9yZWdpc3RyYXRpb24taW4tKFxcdyspL2ksICcvcmVnaXN0cmF0aW9uLWluLUNoaW5hJylcblxuZnVuY3Rpb24gZ2V0X2NvdW50cnlfYnlfcmVnZXgocmVneCwgc3RyaW5nKSB7XG5cbiAgdmFyIHJzbHQgPSByZWd4LmV4ZWMoc3RyaW5nKVxuICBpZiAocnNsdCA9PT0gbnVsbClcbiAgICByZXR1cm4gdW5kZWZpbmVkXG4gIGVsc2Uge1xuICAgIHJldHVybiBnZXRfY291bnRyeV9ieV9uYW1lKHJzbHRbMV0ucmVwbGFjZSgvXy9nLCAnICcpKVxuICB9XG59XG5cblxuXG5cbi8vIGlmIHBhcmFtIGlzIHVuZGVmaW5lZCwgYWxsIHJldHVyblxuLy8gaWYgcGFyYW0gaXMgYW4gYXJyYXksIG9ubHkgdGhvc2UgaW4gdGhlIGxpc3QgcmV0dXJuZWRcbmZ1bmN0aW9uIGdldF9jb3VudHJpZXMocGFyYW0pIHtcbiAgaWYgKHBhcmFtID09PSB1bmRlZmluZWQpXG4gICAgcmV0dXJuIGNvdW50cmllc1xuICBlbHNlIHtcbiAgICB2YXIgcmV0ID0gW11cbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IHBhcmFtLmxlbmd0aDsgaSsrKSB7XG4gICAgICB2YXIgY291bnRyeSA9IGdldF9jb3VudHJ5KHBhcmFtW2ldKVxuICAgICAgaWYgKGNvdW50cnkgIT09IHVuZGVmaW5lZClcbiAgICAgICAgcmV0LnB1c2goY291bnRyeSlcbiAgICB9XG4gICAgcmV0dXJuIHJldFxuICB9XG59XG5cbmZ1bmN0aW9uIGdldF9jb3VudHJpZXNfYnlfY29udG5lbnQoY29kZSkge1xuICByZXR1cm4gY291bnRyaWVzLmZpbHRlcihmdW5jdGlvbiAodmFsdWUsIGluZGV4LCBhcikge1xuICAgIHJldHVybiB2YWx1ZS5jb250aW5lbnQgPT09IGNvZGVcbiAgfSlcbn1cblxuXG4vL2NvbnNvbGUubG9nKGdldF9jb3VudHJpZXMoWydVUycsICdDTicsJ3h4J10pKVxuXG5leHBvcnRzLmdldF9jb3VudHJpZXMgPSBmdW5jdGlvbiAoYVBhcmFtKSB7XG4gIHJldHVybiBnZXRfY291bnRyaWVzKGFQYXJhbSlcbn1cblxuXG5leHBvcnRzLmdldF9jb3VudHJ5ID0gZnVuY3Rpb24gKGFQYXJhbSkge1xuICByZXR1cm4gZ2V0X2NvdW50cnkoYVBhcmFtKVxufVxuXG5cbmV4cG9ydHMuZ2V0X2NvdW50cnlfYnlfbmFtZSA9IGZ1bmN0aW9uIChuYW1lKSB7XG4gIHJldHVybiBnZXRfY291bnRyeV9ieV9uYW1lKG5hbWUpXG59XG5cblxuZXhwb3J0cy5nZXRfY291bnRyeV9ieV9yZWdleCA9IGZ1bmN0aW9uIChyZWd4LCBzdHJpbmcpIHtcbiAgcmV0dXJuIGdldF9jb3VudHJ5X2J5X3JlZ2V4KHJlZ3gsIHN0cmluZylcbn1cblxuXG5leHBvcnRzLmdldF9jb3VudHJpZXNfYnlfY29udG5lbnQgPSBmdW5jdGlvbiAoY29kZSkge1xuICByZXR1cm4gZ2V0X2NvdW50cmllc19ieV9jb250bmVudChjb2RlKVxufVxuXG4vLyByZXR1cm5zIHN0YXRlcyBvZiB0aGUgY291bnRyeV9jb2RlXG5cbmV4cG9ydHMuZ2V0X3N0YXRlcyA9IGZ1bmN0aW9uIChjb2RlKSB7XG4gIHJldHVybiBzdGF0ZXNbY29kZV1cbn1cblxuXG5leHBvcnRzLmdldF9zdGF0ZV9uYW1lID0gZnVuY3Rpb24gKGNvZGUpIHtcbiAgdmFyIGNvdW50cnkgPSBjb2RlLnN1YnN0cmluZygwLCAyKVxuICBjb25zb2xlLmxvZygnY291bnRyeScsIGNvdW50cnkpXG4gIGlmIChzdGF0ZXNbY291bnRyeV0pXG4gICAgcmV0dXJuIHN0YXRlc1tjb3VudHJ5XVtjb2RlXVxuICBlbHNlXG4gICAgcmV0dXJuIHVuZGVmaW5lZFxufVxuXG5cblxuXG4iLCJtb2R1bGUuZXhwb3J0cz17XG4gIFwiVVNcIjoge1xuICAgIFwiVVMuQUxcIjogXCJBbGFiYW1hXCIsXG4gICAgXCJVUy5BS1wiOiBcIkFsYXNrYVwiLFxuICAgIFwiVVMuQVpcIjogXCJBcml6b25hXCIsXG4gICAgXCJVUy5BUlwiOiBcIkFya2Fuc2FzXCIsXG4gICAgXCJVUy5DQVwiOiBcIkNhbGlmb3JuaWFcIixcbiAgICBcIlVTLkNPXCI6IFwiQ29sb3JhZG9cIixcbiAgICBcIlVTLkNUXCI6IFwiQ29ubmVjdGljdXRcIixcbiAgICBcIlVTLkRFXCI6IFwiRGVsYXdhcmVcIixcbiAgICBcIlVTLkRDXCI6IFwiRGlzdHJpY3Qgb2YgQ29sdW1iaWFcIixcbiAgICBcIlVTLkZMXCI6IFwiRmxvcmlkYVwiLFxuICAgIFwiVVMuR0FcIjogXCJHZW9yZ2lhXCIsXG4gICAgXCJVUy5ISVwiOiBcIkhhd2FpaVwiLFxuICAgIFwiVVMuSURcIjogXCJJZGFob1wiLFxuICAgIFwiVVMuSUxcIjogXCJJbGxpbm9pc1wiLFxuICAgIFwiVVMuSU5cIjogXCJJbmRpYW5hXCIsXG4gICAgXCJVUy5JQVwiOiBcIklvd2FcIixcbiAgICBcIlVTLktTXCI6IFwiS2Fuc2FzXCIsXG4gICAgXCJVUy5LWVwiOiBcIktlbnR1Y2t5XCIsXG4gICAgXCJVUy5MQVwiOiBcIkxvdWlzaWFuYVwiLFxuICAgIFwiVVMuTUVcIjogXCJNYWluZVwiLFxuICAgIFwiVVMuTURcIjogXCJNYXJ5bGFuZFwiLFxuICAgIFwiVVMuTUFcIjogXCJNYXNzYWNodXNldHRzXCIsXG4gICAgXCJVUy5NSVwiOiBcIk1pY2hpZ2FuXCIsXG4gICAgXCJVUy5NTlwiOiBcIk1pbm5lc290YVwiLFxuICAgIFwiVVMuTVNcIjogXCJNaXNzaXNzaXBwaVwiLFxuICAgIFwiVVMuTU9cIjogXCJNaXNzb3VyaVwiLFxuICAgIFwiVVMuTVRcIjogXCJNb250YW5hXCIsXG4gICAgXCJVUy5ORVwiOiBcIk5lYnJhc2thXCIsXG4gICAgXCJVUy5OVlwiOiBcIk5ldmFkYVwiLFxuICAgIFwiVVMuTkhcIjogXCJOZXcgSGFtcHNoaXJlXCIsXG4gICAgXCJVUy5OSlwiOiBcIk5ldyBKZXJzZXlcIixcbiAgICBcIlVTLk5NXCI6IFwiTmV3IE1leGljb1wiLFxuICAgIFwiVVMuTllcIjogXCJOZXcgWW9ya1wiLFxuICAgIFwiVVMuTkNcIjogXCJOb3J0aCBDYXJvbGluYVwiLFxuICAgIFwiVVMuTkRcIjogXCJOb3J0aCBEYWtvdGFcIixcbiAgICBcIlVTLk9IXCI6IFwiT2hpb1wiLFxuICAgIFwiVVMuT0tcIjogXCJPa2xhaG9tYVwiLFxuICAgIFwiVVMuT1JcIjogXCJPcmVnb25cIixcbiAgICBcIlVTLlBBXCI6IFwiUGVubnN5bHZhbmlhXCIsXG4gICAgXCJVUy5SSVwiOiBcIlJob2RlIElzbGFuZFwiLFxuICAgIFwiVVMuU0NcIjogXCJTb3V0aCBDYXJvbGluYVwiLFxuICAgIFwiVVMuU0RcIjogXCJTb3V0aCBEYWtvdGFcIixcbiAgICBcIlVTLlROXCI6IFwiVGVubmVzc2VlXCIsXG4gICAgXCJVUy5UWFwiOiBcIlRleGFzXCIsXG4gICAgXCJVUy5VVFwiOiBcIlV0YWhcIixcbiAgICBcIlVTLlZUXCI6IFwiVmVybW9udFwiLFxuICAgIFwiVVMuVkFcIjogXCJWaXJnaW5pYVwiLFxuICAgIFwiVVMuV0FcIjogXCJXYXNoaW5ndG9uXCIsXG4gICAgXCJVUy5XVlwiOiBcIldlc3QgVmlyZ2luaWFcIixcbiAgICBcIlVTLldJXCI6IFwiV2lzY29uc2luXCIsXG4gICAgXCJVUy5XWVwiOiBcIld5b21pbmdcIlxuICB9LFxuICBcIkNOXCIgOiB7XG4gICAgXCJDTi5BSFwiOiBcIkFuaHVpXCIsXG4gICAgXCJDTi5CSlwiOiBcIkJlaWppbmdcIixcbiAgICBcIkNOLkNRXCI6IFwiQ2hvbmdxaW5nXCIsXG4gICAgXCJDTi5GSlwiOiBcIkZ1amlhblwiLFxuICAgIFwiQ04uR1NcIjogXCJHYW5zdVwiLFxuICAgIFwiQ04uR0RcIjogXCJHdWFuZG9uZ1wiLFxuICAgIFwiQ04uR1hcIjogXCJHdWFuZ3hpXCIsXG4gICAgXCJDTi5IQVwiOiBcIkhhaW5hblwiLFxuICAgIFwiQ04uSEJcIjogXCJIZWJlaVwiLFxuICAgIFwiQ04uSExcIjogXCJIZWlsb25namlhblwiLFxuICAgIFwiQ04uSEFcIjogXCJIZW5hblwiLFxuICAgIFwiQ04uSEJcIjogXCJIdWJlaVwiLFxuICAgIFwiQ04uSE5cIjogXCJIdW5hblwiLFxuICAgIFwiQ04uSlNcIjogXCJKaWFuZ3N1XCIsXG4gICAgXCJDTi5KWFwiOiBcIkppYW5neGlcIixcbiAgICBcIkNOLkpMXCI6IFwiSmlsaW5cIixcbiAgICBcIkNOLkxOXCI6IFwiTGlhb25pbmdcIixcbiAgICBcIkNOLk5NXCI6IFwiTmVpIE1vbmdvbFwiLFxuICAgIFwiQ04uTlhcIjogXCJOaW5neGlhXCIsXG4gICAgXCJDTi5RSFwiOiBcIlFpbmdoYWlcIixcbiAgICBcIkNOLlNBXCI6IFwiU2hhYW54aVwiLFxuICAgIFwiQ04uU0RcIjogXCJTaGFuZG9uZ1wiLFxuICAgIFwiQ04uU0hcIjogXCJTaGFuZ2hhaVwiLFxuICAgIFwiQ04uU1hcIjogXCJTaGFueGlcIixcbiAgICBcIkNOLlNDXCI6IFwiU2ljaHVhblwiLFxuICAgIFwiQ04uVEpcIjogXCJUaWFuamluXCIsXG4gICAgXCJDTi5YSlwiOiBcIlhpbmp1YW5cIixcbiAgICBcIkNOLlhaXCI6IFwiWGl6YW5nXCIsXG4gICAgXCJDTi5ZTlwiOiBcIll1bm5hblwiLFxuICAgIFwiQ04uWkpcIjogXCJaaGVqaWFuXCJcbiAgfSxcbiAgXCJIS1wiIDoge1xuICAgIFwiSEsuQ1dcIjogXCJDZW50cmFsIGFuZCBXZXN0ZXJuXCIsXG4gICAgXCJISy5FQVwiOiBcIkVhc3Rlcm5cIixcbiAgICBcIkhLLklTXCI6IFwiSXNsYW5kc1wiLFxuICAgIFwiSEsuS0NcIjogXCJLb3dsb29uIENpdHlcIixcbiAgICBcIkhLLktJXCI6IFwiS3dhaSBUc2luZ1wiLFxuICAgIFwiSEsuS1VcIjogXCJLd3VuIFRvbmdcIixcbiAgICBcIkhLLk5PXCI6IFwiTm9ydGhcIixcbiAgICBcIkhLLlNLXCI6IFwiU2FpIEt1bmdcIixcbiAgICBcIkhLLlNTXCI6IFwiU2hhbSBTaHVpIFBvXCIsXG4gICAgXCJISy5TVFwiOiBcIlNoYSBUaW5cIixcbiAgICBcIkhLLlNPXCI6IFwiU291dGhlcm5cIixcbiAgICBcIkhLLlRQXCI6IFwiVGFpIFBvXCIsXG4gICAgXCJISy5UV1wiOiBcIlRzdWVuIFdhblwiLFxuICAgIFwiSEsuVE1cIjogXCJUdWVuIE11blwiLFxuICAgIFwiSEsuV0NcIjogXCJXYW4gQ2hhaVwiLFxuICAgIFwiSEsuV1RcIjogXCJXb25nIFRhaSBTaW5cIixcbiAgICBcIkhLLllUXCI6IFwiWWF1IFRzaW0gTW9uZ1wiLFxuICAgIFwiSEsuWUxcIjogXCJZdWVuIExvbmdcIlxuICB9LFxuICBcIlBIXCIgOiB7XG4gICAgXCJQSC5BQlwiOiBcIkFicmFcIixcbiAgICBcIlBILkFOXCI6IFwiQWd1c2FuIGRlbCBOb3J0ZVwiLFxuICAgIFwiUEguQVNcIjogXCJBZ3VzYW4gZGVsIFN1clwiLFxuICAgIFwiUEguQUtcIjogXCJBa2xhblwiLFxuICAgIFwiUEguQUxcIjogXCJBbGJheVwiLFxuICAgIFwiUEguQVFcIjogXCJBbnRpcXVlXCIsXG4gICAgXCJQSC5BUFwiOiBcIkFwYXlhb1wiLFxuICAgIFwiUEguQVVcIjogXCJBdXJvcmFcIixcbiAgICBcIlBILkJTXCI6IFwiQmFzaWxhblwiLFxuICAgIFwiUEguQkFcIjogXCJCYXRhYW5cIixcbiAgICBcIlBILkJOXCI6IFwiQmF0YW5lc1wiLFxuICAgIFwiUEguQlRcIjogXCJCYXRhbmdhc1wiLFxuICAgIFwiUEguQkdcIjogXCJCZW5ndWV0XCIsXG4gICAgXCJQSC5CSVwiOiBcIkJpbGlyYW5cIixcbiAgICBcIlBILkJPXCI6IFwiQm9ob2xcIixcbiAgICBcIlBILkJLXCI6IFwiQnVraWRub25cIixcbiAgICBcIlBILkJVXCI6IFwiQnVsYWNhblwiLFxuICAgIFwiUEguQ0dcIjogXCJDYWdheWFuXCIsXG4gICAgXCJQSC5DTlwiOiBcIkNhbWFyaW5lcyBOb3J0ZVwiLFxuICAgIFwiUEguQ1NcIjogXCJDYW1hcmluZXMgU3VyXCIsXG4gICAgXCJQSC5DTVwiOiBcIkNhbWlndWluXCIsXG4gICAgXCJQSC5DUFwiOiBcIkNhcGl6XCIsXG4gICAgXCJQSC5DVFwiOiBcIkNhdGFuZHVhbmVzXCIsXG4gICAgXCJQSC5DVlwiOiBcIkNhdml0ZVwiLFxuICAgIFwiUEguQ0JcIjogXCJDZWJ1XCIsXG4gICAgXCJQSC5DTFwiOiBcIkNvbXBvc3RlbGEgVmFsbGV5XCIsXG4gICAgXCJQSC5OQ1wiOiBcIkNvdGFiYXRvXCIsXG4gICAgXCJQSC5EVlwiOiBcIkRhdmFvIGRlbCBOb3J0ZVwiLFxuICAgIFwiUEguRFNcIjogXCJEYXZhbyBkZWwgU3VyXCIsXG4gICAgXCJQSC5ET1wiOiBcIkRhdmFvIE9yaWVudGFsXCIsXG4gICAgXCJQSC5ESVwiOiBcIkRpbmFnYXQgSXNsYW5kc1wiLFxuICAgIFwiUEguRVNcIjogXCJFYXN0ZXJuIFNhbWFyXCIsXG4gICAgXCJQSC5HVVwiOiBcIkd1aW1hcmFzXCIsXG4gICAgXCJQSC5JRlwiOiBcIklmdWdhb1wiLFxuICAgIFwiUEguSU5cIjogXCJJbG9jb3MgTm9ydGVcIixcbiAgICBcIlBILklTXCI6IFwiSWxvY29zIFN1clwiLFxuICAgIFwiUEguSUlcIjogXCJJbG9pbG9cIixcbiAgICBcIlBILklCXCI6IFwiSXNhYmVsYVwiLFxuICAgIFwiUEguS0FcIjogXCJLYWxpbmdhXCIsXG4gICAgXCJQSC5MR1wiOiBcIkxhZ3VuYVwiLFxuICAgIFwiUEguTE5cIjogXCJMYW5hbyBkZWwgTm9ydGVcIixcbiAgICBcIlBILkxTXCI6IFwiTGFuYW8gZGVsIFN1clwiLFxuICAgIFwiUEguTFVcIjogXCJMYSBVbmlvblwiLFxuICAgIFwiUEguTEVcIjogXCJMZXl0ZVwiLFxuICAgIFwiUEguTUdcIjogXCJNYWd1aW5kYW5hb1wiLFxuICAgIFwiUEguTVFcIjogXCJNYXJpbmR1cXVlXCIsXG4gICAgXCJQSC5NQlwiOiBcIk1hc2JhdGVcIixcbiAgICBcIlBILk1NXCI6IFwiTWV0cm9wb2xpdGFuIE1hbmlsYVwiLFxuICAgIFwiUEguTURcIjogXCJNaXNhbWlzIE9jY2lkZW50YWxcIixcbiAgICBcIlBILk1OXCI6IFwiTWlzYW1pcyBPcmllbnRhbFwiLFxuICAgIFwiUEguTVRcIjogXCJNb3VudGFpblwiLFxuICAgIFwiUEguTkRcIjogXCJOZWdyb3MgT2NjaWRlbnRhbFwiLFxuICAgIFwiUEguTlJcIjogXCJOZWdyb3MgT3JpZW50YWxcIixcbiAgICBcIlBILk5TXCI6IFwiTm9ydGhlcm4gU2FtYXJcIixcbiAgICBcIlBILk5FXCI6IFwiTnVldmEgRWNpamFcIixcbiAgICBcIlBILk5WXCI6IFwiTnVldmEgVml6Y2F5YVwiLFxuICAgIFwiUEguTUNcIjogXCJPY2NpZGVudGFsIE1pbmRvcm9cIixcbiAgICBcIlBILk1SXCI6IFwiT3JpZW50YWwgTWluZG9yb1wiLFxuICAgIFwiUEguUExcIjogXCJQYWxhd2FuXCIsXG4gICAgXCJQSC5QTVwiOiBcIlBhbXBhbmdhXCIsXG4gICAgXCJQSC5QTlwiOiBcIlBhbmdhc2luYW5cIixcbiAgICBcIlBILlFaXCI6IFwiUXVlem9uXCIsXG4gICAgXCJQSC5RUlwiOiBcIlF1aXJpbm9cIixcbiAgICBcIlBILlJJXCI6IFwiUml6YWxcIixcbiAgICBcIlBILlJPXCI6IFwiUm9tYmxvblwiLFxuICAgIFwiUEguU01cIjogXCJTYW1hclwiLFxuICAgIFwiUEguU0dcIjogXCJTYXJhbmdhbmlcIixcbiAgICBcIlBILlNRXCI6IFwiU2lxdWlqb3JcIixcbiAgICBcIlBILlNSXCI6IFwiU29yc29nb25cIixcbiAgICBcIlBILlNDXCI6IFwiU291dGggQ290YWJhdG9cIixcbiAgICBcIlBILlNMXCI6IFwiU291dGhlcm4gTGV5dGVcIixcbiAgICBcIlBILlNLXCI6IFwiU3VsdGFuIEt1ZGFyYXRcIixcbiAgICBcIlBILlNVXCI6IFwiU3VsdVwiLFxuICAgIFwiUEguU1RcIjogXCJTdXJpZ2FvIGRlbCBOb3J0ZVwiLFxuICAgIFwiUEguU1NcIjogXCJTdXJpZ2FvIGRlbCBTdXJcIixcbiAgICBcIlBILlRSXCI6IFwiVGFybGFjXCIsXG4gICAgXCJQSC5UVFwiOiBcIlRhd2ktVGF3aVwiLFxuICAgIFwiUEguWk1cIjogXCJaYW1iYWxlc1wiLFxuICAgIFwiUEguWk5cIjogXCJaYW1ib2FuZ2EgZGVsIE5vcnRlXCIsXG4gICAgXCJQSC5aU1wiOiBcIlphbWJvYW5nYSBkZWwgU3VyXCIsXG4gICAgXCJQSC5aWVwiOiBcIlphbWJvYW5nYS1TaWJ1Z2F5XCJcbiAgfSxcbiAgXCJDQVwiIDoge1xuICAgIFwiQ0EuQUJcIjogXCJBbGJlcnRhXCIsXG4gICAgXCJDQS5CQ1wiOiBcIkJyaXRpc2ggQ29sdW1iaWFcIixcbiAgICBcIkNBLk1CXCI6IFwiTWFuaXRvYmFcIixcbiAgICBcIkNBLk5CXCI6IFwiTmV3IEJydW5zd2lja1wiLFxuICAgIFwiQ0EuTkZcIjogXCJOZXdmb3VuZGxhbmQgYW5kIExhYnJhZG9yXCIsXG4gICAgXCJDQS5OVFwiOiBcIk5vcnRod2VzdCBUZXJyaXRvcmllc1wiLFxuICAgIFwiQ0EuTlNcIjogXCJOb3ZhIFNjb3RpYVwiLFxuICAgIFwiQ0EuTlVcIjogXCJOdW5hdnV0XCIsXG4gICAgXCJDQS5PTlwiOiBcIk9udGFyaW9cIixcbiAgICBcIkNBLlBFXCI6IFwiUHJpbmNlIEVkd2FyZCBJc2xhbmRcIixcbiAgICBcIkNBLlFDXCI6IFwiUXVlYmVjXCIsXG4gICAgXCJDQS5TS1wiOiBcIlNhc2thdGNoZXdhblwiLFxuICAgIFwiQ0EuWVRcIjogXCJZdWtvbiBUZXJyaXRvcnlcIlxuICB9LFxuICBcIlRIXCIgOiB7XG4gICAgXCJUSC5BQ1wiOiBcIkFtbmF0IENoYXJvZW5cIixcbiAgICBcIlRILkFUXCI6IFwiQW5nIFRob25nXCIsXG4gICAgXCJUSC5CTVwiOiBcIkJhbmdrb2sgTWV0cm9wb2xpc1wiLFxuICAgIFwiVEguQlJcIjogXCJCdXJpIFJhbVwiLFxuICAgIFwiVEguQ0NcIjogXCJDaGFjaG9lbmdzYW9cIixcbiAgICBcIlRILkNOXCI6IFwiQ2hhaSBOYXRcIixcbiAgICBcIlRILkNZXCI6IFwiQ2hhaXlhcGh1bVwiLFxuICAgIFwiVEguQ1RcIjogXCJDaGFudGhhYnVyaVwiLFxuICAgIFwiVEguQ01cIjogXCJDaGlhbmcgTWFpXCIsXG4gICAgXCJUSC5DUlwiOiBcIkNoaWFuZyBSYWlcIixcbiAgICBcIlRILkNCXCI6IFwiQ2hvbiBCdXJpXCIsXG4gICAgXCJUSC5DUFwiOiBcIkNodW1waG9uXCIsXG4gICAgXCJUSC5LTFwiOiBcIkthbGFzaW5cIixcbiAgICBcIlRILktQXCI6IFwiS2FtcGhhZW5nIFBoZXRcIixcbiAgICBcIlRILktOXCI6IFwiS2FuY2hhbmFidXJpXCIsXG4gICAgXCJUSC5LS1wiOiBcIktob24gS2FlblwiLFxuICAgIFwiVEguS1JcIjogXCJLcmFiaVwiLFxuICAgIFwiVEguTEdcIjogXCJMYW1wYW5nXCIsXG4gICAgXCJUSC5MTlwiOiBcIkxhbXBodW5cIixcbiAgICBcIlRILkxFXCI6IFwiTG9laVwiLFxuICAgIFwiVEguTEJcIjogXCJMb3AgQnVyaVwiLFxuICAgIFwiVEguTUhcIjogXCJNYWUgSG9uZyBTb25cIixcbiAgICBcIlRILk1TXCI6IFwiTWFoYSBTYXJha2hhbVwiLFxuICAgIFwiVEguTURcIjogXCJNdWtkYWhhblwiLFxuICAgIFwiVEguTk5cIjogXCJOYWtob24gTmF5b2tcIixcbiAgICBcIlRILk5QXCI6IFwiTmFraG9uIFBhdGhvbVwiLFxuICAgIFwiVEguTkZcIjogXCJOYWtob24gUGhhbm9tXCIsXG4gICAgXCJUSC5OUlwiOiBcIk5ha2hvbiBSYXRjaGFzaW1hXCIsXG4gICAgXCJUSC5OU1wiOiBcIk5ha2hvbiBTYXdhblwiLFxuICAgIFwiVEguTlRcIjogXCJOYWtob24gU2kgVGhhbW1hcmF0XCIsXG4gICAgXCJUSC5OQVwiOiBcIk5hblwiLFxuICAgIFwiVEguTldcIjogXCJOYXJhdGhpd2F0XCIsXG4gICAgXCJUSC5OQlwiOiBcIk5vbmcgQnVhIExhbSBQaHVcIixcbiAgICBcIlRILk5LXCI6IFwiTm9uZyBLaGFpXCIsXG4gICAgXCJUSC5OT1wiOiBcIk5vbnRoYWJ1cmlcIixcbiAgICBcIlRILlBUXCI6IFwiUGF0aHVtIFRoYW5pXCIsXG4gICAgXCJUSC5QSVwiOiBcIlBhdHRhbmlcIixcbiAgICBcIlRILlBHXCI6IFwiUGhhbmduZ2FcIixcbiAgICBcIlRILlBMXCI6IFwiUGhhdHRoYWx1bmdcIixcbiAgICBcIlRILlBZXCI6IFwiUGhheWFvXCIsXG4gICAgXCJUSC5QSFwiOiBcIlBoZXRjaGFidW5cIixcbiAgICBcIlRILlBFXCI6IFwiUGhldGNoYWJ1cmlcIixcbiAgICBcIlRILlBDXCI6IFwiUGhpY2hpdFwiLFxuICAgIFwiVEguUFNcIjogXCJQaGl0c2FudWxva1wiLFxuICAgIFwiVEguUFJcIjogXCJQaHJhZVwiLFxuICAgIFwiVEguUEFcIjogXCJQaHJhIE5ha2hvbiBTaSBBeXV0dGhheWFcIixcbiAgICBcIlRILlBVXCI6IFwiUGh1a2V0XCIsXG4gICAgXCJUSC5QQlwiOiBcIlByYWNoaW4gQnVyaVwiLFxuICAgIFwiVEguUEtcIjogXCJQcmFjaHVhcCBLaGlyaSBLaGFuXCIsXG4gICAgXCJUSC5STlwiOiBcIlJhbm9uZ1wiLFxuICAgIFwiVEguUlRcIjogXCJSYXRjaGFidXJpXCIsXG4gICAgXCJUSC5SWVwiOiBcIlJheW9uZ1wiLFxuICAgIFwiVEguUkVcIjogXCJSb2kgRXRcIixcbiAgICBcIlRILlNLXCI6IFwiU2EgS2Flb1wiLFxuICAgIFwiVEguU05cIjogXCJTYWtvbiBOYWtob25cIixcbiAgICBcIlRILlNQXCI6IFwiU2FtdXQgUHJha2FuXCIsXG4gICAgXCJUSC5TU1wiOiBcIlNhbXV0IFNha2hvblwiLFxuICAgIFwiVEguU01cIjogXCJTYW11dCBTb25na2hyYW1cIixcbiAgICBcIlRILlNSXCI6IFwiU2FyYWJ1cmlcIixcbiAgICBcIlRILlNBXCI6IFwiU2F0dW5cIixcbiAgICBcIlRILlNCXCI6IFwiU2luZyBCdXJpXCIsXG4gICAgXCJUSC5TSVwiOiBcIlNpIFNhIEtldFwiLFxuICAgIFwiVEguU0dcIjogXCJTb25na2hsYVwiLFxuICAgIFwiVEguU09cIjogXCJTdWtob3RoYWlcIixcbiAgICBcIlRILlNIXCI6IFwiU3VwaGFuIEJ1cmlcIixcbiAgICBcIlRILlNUXCI6IFwiU3VyYXQgVGhhbmlcIixcbiAgICBcIlRILlNVXCI6IFwiU3VyaW5cIixcbiAgICBcIlRILlRLXCI6IFwiVGFrXCIsXG4gICAgXCJUSC5UR1wiOiBcIlRyYW5nXCIsXG4gICAgXCJUSC5UVFwiOiBcIlRyYXRcIixcbiAgICBcIlRILlVSXCI6IFwiVWJvbiBSYXRjaGF0aGFuaVwiLFxuICAgIFwiVEguVU5cIjogXCJVZG9uIFRoYW5pXCIsXG4gICAgXCJUSC5VVFwiOiBcIlV0aGFpIFRoYW5pXCIsXG4gICAgXCJUSC5VRFwiOiBcIlV0dGFyYWRpdFwiLFxuICAgIFwiVEguWUxcIjogXCJZYWxhXCIsXG4gICAgXCJUSC5ZU1wiOiBcIllhc290aG9uXCJcbiAgfVxufVxuIiwiKGZ1bmN0aW9uIChwcm9jZXNzKXtcbihmdW5jdGlvbiAoZ2xvYmFsLCB1bmRlZmluZWQpIHtcbiAgICBcInVzZSBzdHJpY3RcIjtcblxuICAgIGlmIChnbG9iYWwuc2V0SW1tZWRpYXRlKSB7XG4gICAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB2YXIgbmV4dEhhbmRsZSA9IDE7IC8vIFNwZWMgc2F5cyBncmVhdGVyIHRoYW4gemVyb1xuICAgIHZhciB0YXNrc0J5SGFuZGxlID0ge307XG4gICAgdmFyIGN1cnJlbnRseVJ1bm5pbmdBVGFzayA9IGZhbHNlO1xuICAgIHZhciBkb2MgPSBnbG9iYWwuZG9jdW1lbnQ7XG4gICAgdmFyIHNldEltbWVkaWF0ZTtcblxuICAgIGZ1bmN0aW9uIGFkZEZyb21TZXRJbW1lZGlhdGVBcmd1bWVudHMoYXJncykge1xuICAgICAgICB0YXNrc0J5SGFuZGxlW25leHRIYW5kbGVdID0gcGFydGlhbGx5QXBwbGllZC5hcHBseSh1bmRlZmluZWQsIGFyZ3MpO1xuICAgICAgICByZXR1cm4gbmV4dEhhbmRsZSsrO1xuICAgIH1cblxuICAgIC8vIFRoaXMgZnVuY3Rpb24gYWNjZXB0cyB0aGUgc2FtZSBhcmd1bWVudHMgYXMgc2V0SW1tZWRpYXRlLCBidXRcbiAgICAvLyByZXR1cm5zIGEgZnVuY3Rpb24gdGhhdCByZXF1aXJlcyBubyBhcmd1bWVudHMuXG4gICAgZnVuY3Rpb24gcGFydGlhbGx5QXBwbGllZChoYW5kbGVyKSB7XG4gICAgICAgIHZhciBhcmdzID0gW10uc2xpY2UuY2FsbChhcmd1bWVudHMsIDEpO1xuICAgICAgICByZXR1cm4gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpZiAodHlwZW9mIGhhbmRsZXIgPT09IFwiZnVuY3Rpb25cIikge1xuICAgICAgICAgICAgICAgIGhhbmRsZXIuYXBwbHkodW5kZWZpbmVkLCBhcmdzKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgKG5ldyBGdW5jdGlvbihcIlwiICsgaGFuZGxlcikpKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gcnVuSWZQcmVzZW50KGhhbmRsZSkge1xuICAgICAgICAvLyBGcm9tIHRoZSBzcGVjOiBcIldhaXQgdW50aWwgYW55IGludm9jYXRpb25zIG9mIHRoaXMgYWxnb3JpdGhtIHN0YXJ0ZWQgYmVmb3JlIHRoaXMgb25lIGhhdmUgY29tcGxldGVkLlwiXG4gICAgICAgIC8vIFNvIGlmIHdlJ3JlIGN1cnJlbnRseSBydW5uaW5nIGEgdGFzaywgd2UnbGwgbmVlZCB0byBkZWxheSB0aGlzIGludm9jYXRpb24uXG4gICAgICAgIGlmIChjdXJyZW50bHlSdW5uaW5nQVRhc2spIHtcbiAgICAgICAgICAgIC8vIERlbGF5IGJ5IGRvaW5nIGEgc2V0VGltZW91dC4gc2V0SW1tZWRpYXRlIHdhcyB0cmllZCBpbnN0ZWFkLCBidXQgaW4gRmlyZWZveCA3IGl0IGdlbmVyYXRlZCBhXG4gICAgICAgICAgICAvLyBcInRvbyBtdWNoIHJlY3Vyc2lvblwiIGVycm9yLlxuICAgICAgICAgICAgc2V0VGltZW91dChwYXJ0aWFsbHlBcHBsaWVkKHJ1bklmUHJlc2VudCwgaGFuZGxlKSwgMCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB2YXIgdGFzayA9IHRhc2tzQnlIYW5kbGVbaGFuZGxlXTtcbiAgICAgICAgICAgIGlmICh0YXNrKSB7XG4gICAgICAgICAgICAgICAgY3VycmVudGx5UnVubmluZ0FUYXNrID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgICAgICB0YXNrKCk7XG4gICAgICAgICAgICAgICAgfSBmaW5hbGx5IHtcbiAgICAgICAgICAgICAgICAgICAgY2xlYXJJbW1lZGlhdGUoaGFuZGxlKTtcbiAgICAgICAgICAgICAgICAgICAgY3VycmVudGx5UnVubmluZ0FUYXNrID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gY2xlYXJJbW1lZGlhdGUoaGFuZGxlKSB7XG4gICAgICAgIGRlbGV0ZSB0YXNrc0J5SGFuZGxlW2hhbmRsZV07XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gaW5zdGFsbE5leHRUaWNrSW1wbGVtZW50YXRpb24oKSB7XG4gICAgICAgIHNldEltbWVkaWF0ZSA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyIGhhbmRsZSA9IGFkZEZyb21TZXRJbW1lZGlhdGVBcmd1bWVudHMoYXJndW1lbnRzKTtcbiAgICAgICAgICAgIHByb2Nlc3MubmV4dFRpY2socGFydGlhbGx5QXBwbGllZChydW5JZlByZXNlbnQsIGhhbmRsZSkpO1xuICAgICAgICAgICAgcmV0dXJuIGhhbmRsZTtcbiAgICAgICAgfTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBjYW5Vc2VQb3N0TWVzc2FnZSgpIHtcbiAgICAgICAgLy8gVGhlIHRlc3QgYWdhaW5zdCBgaW1wb3J0U2NyaXB0c2AgcHJldmVudHMgdGhpcyBpbXBsZW1lbnRhdGlvbiBmcm9tIGJlaW5nIGluc3RhbGxlZCBpbnNpZGUgYSB3ZWIgd29ya2VyLFxuICAgICAgICAvLyB3aGVyZSBgZ2xvYmFsLnBvc3RNZXNzYWdlYCBtZWFucyBzb21ldGhpbmcgY29tcGxldGVseSBkaWZmZXJlbnQgYW5kIGNhbid0IGJlIHVzZWQgZm9yIHRoaXMgcHVycG9zZS5cbiAgICAgICAgaWYgKGdsb2JhbC5wb3N0TWVzc2FnZSAmJiAhZ2xvYmFsLmltcG9ydFNjcmlwdHMpIHtcbiAgICAgICAgICAgIHZhciBwb3N0TWVzc2FnZUlzQXN5bmNocm9ub3VzID0gdHJ1ZTtcbiAgICAgICAgICAgIHZhciBvbGRPbk1lc3NhZ2UgPSBnbG9iYWwub25tZXNzYWdlO1xuICAgICAgICAgICAgZ2xvYmFsLm9ubWVzc2FnZSA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgIHBvc3RNZXNzYWdlSXNBc3luY2hyb25vdXMgPSBmYWxzZTtcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICBnbG9iYWwucG9zdE1lc3NhZ2UoXCJcIiwgXCIqXCIpO1xuICAgICAgICAgICAgZ2xvYmFsLm9ubWVzc2FnZSA9IG9sZE9uTWVzc2FnZTtcbiAgICAgICAgICAgIHJldHVybiBwb3N0TWVzc2FnZUlzQXN5bmNocm9ub3VzO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gaW5zdGFsbFBvc3RNZXNzYWdlSW1wbGVtZW50YXRpb24oKSB7XG4gICAgICAgIC8vIEluc3RhbGxzIGFuIGV2ZW50IGhhbmRsZXIgb24gYGdsb2JhbGAgZm9yIHRoZSBgbWVzc2FnZWAgZXZlbnQ6IHNlZVxuICAgICAgICAvLyAqIGh0dHBzOi8vZGV2ZWxvcGVyLm1vemlsbGEub3JnL2VuL0RPTS93aW5kb3cucG9zdE1lc3NhZ2VcbiAgICAgICAgLy8gKiBodHRwOi8vd3d3LndoYXR3Zy5vcmcvc3BlY3Mvd2ViLWFwcHMvY3VycmVudC13b3JrL211bHRpcGFnZS9jb21tcy5odG1sI2Nyb3NzRG9jdW1lbnRNZXNzYWdlc1xuXG4gICAgICAgIHZhciBtZXNzYWdlUHJlZml4ID0gXCJzZXRJbW1lZGlhdGUkXCIgKyBNYXRoLnJhbmRvbSgpICsgXCIkXCI7XG4gICAgICAgIHZhciBvbkdsb2JhbE1lc3NhZ2UgPSBmdW5jdGlvbihldmVudCkge1xuICAgICAgICAgICAgaWYgKGV2ZW50LnNvdXJjZSA9PT0gZ2xvYmFsICYmXG4gICAgICAgICAgICAgICAgdHlwZW9mIGV2ZW50LmRhdGEgPT09IFwic3RyaW5nXCIgJiZcbiAgICAgICAgICAgICAgICBldmVudC5kYXRhLmluZGV4T2YobWVzc2FnZVByZWZpeCkgPT09IDApIHtcbiAgICAgICAgICAgICAgICBydW5JZlByZXNlbnQoK2V2ZW50LmRhdGEuc2xpY2UobWVzc2FnZVByZWZpeC5sZW5ndGgpKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcblxuICAgICAgICBpZiAoZ2xvYmFsLmFkZEV2ZW50TGlzdGVuZXIpIHtcbiAgICAgICAgICAgIGdsb2JhbC5hZGRFdmVudExpc3RlbmVyKFwibWVzc2FnZVwiLCBvbkdsb2JhbE1lc3NhZ2UsIGZhbHNlKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGdsb2JhbC5hdHRhY2hFdmVudChcIm9ubWVzc2FnZVwiLCBvbkdsb2JhbE1lc3NhZ2UpO1xuICAgICAgICB9XG5cbiAgICAgICAgc2V0SW1tZWRpYXRlID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXIgaGFuZGxlID0gYWRkRnJvbVNldEltbWVkaWF0ZUFyZ3VtZW50cyhhcmd1bWVudHMpO1xuICAgICAgICAgICAgZ2xvYmFsLnBvc3RNZXNzYWdlKG1lc3NhZ2VQcmVmaXggKyBoYW5kbGUsIFwiKlwiKTtcbiAgICAgICAgICAgIHJldHVybiBoYW5kbGU7XG4gICAgICAgIH07XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gaW5zdGFsbE1lc3NhZ2VDaGFubmVsSW1wbGVtZW50YXRpb24oKSB7XG4gICAgICAgIHZhciBjaGFubmVsID0gbmV3IE1lc3NhZ2VDaGFubmVsKCk7XG4gICAgICAgIGNoYW5uZWwucG9ydDEub25tZXNzYWdlID0gZnVuY3Rpb24oZXZlbnQpIHtcbiAgICAgICAgICAgIHZhciBoYW5kbGUgPSBldmVudC5kYXRhO1xuICAgICAgICAgICAgcnVuSWZQcmVzZW50KGhhbmRsZSk7XG4gICAgICAgIH07XG5cbiAgICAgICAgc2V0SW1tZWRpYXRlID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXIgaGFuZGxlID0gYWRkRnJvbVNldEltbWVkaWF0ZUFyZ3VtZW50cyhhcmd1bWVudHMpO1xuICAgICAgICAgICAgY2hhbm5lbC5wb3J0Mi5wb3N0TWVzc2FnZShoYW5kbGUpO1xuICAgICAgICAgICAgcmV0dXJuIGhhbmRsZTtcbiAgICAgICAgfTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBpbnN0YWxsUmVhZHlTdGF0ZUNoYW5nZUltcGxlbWVudGF0aW9uKCkge1xuICAgICAgICB2YXIgaHRtbCA9IGRvYy5kb2N1bWVudEVsZW1lbnQ7XG4gICAgICAgIHNldEltbWVkaWF0ZSA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyIGhhbmRsZSA9IGFkZEZyb21TZXRJbW1lZGlhdGVBcmd1bWVudHMoYXJndW1lbnRzKTtcbiAgICAgICAgICAgIC8vIENyZWF0ZSBhIDxzY3JpcHQ+IGVsZW1lbnQ7IGl0cyByZWFkeXN0YXRlY2hhbmdlIGV2ZW50IHdpbGwgYmUgZmlyZWQgYXN5bmNocm9ub3VzbHkgb25jZSBpdCBpcyBpbnNlcnRlZFxuICAgICAgICAgICAgLy8gaW50byB0aGUgZG9jdW1lbnQuIERvIHNvLCB0aHVzIHF1ZXVpbmcgdXAgdGhlIHRhc2suIFJlbWVtYmVyIHRvIGNsZWFuIHVwIG9uY2UgaXQncyBiZWVuIGNhbGxlZC5cbiAgICAgICAgICAgIHZhciBzY3JpcHQgPSBkb2MuY3JlYXRlRWxlbWVudChcInNjcmlwdFwiKTtcbiAgICAgICAgICAgIHNjcmlwdC5vbnJlYWR5c3RhdGVjaGFuZ2UgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgcnVuSWZQcmVzZW50KGhhbmRsZSk7XG4gICAgICAgICAgICAgICAgc2NyaXB0Lm9ucmVhZHlzdGF0ZWNoYW5nZSA9IG51bGw7XG4gICAgICAgICAgICAgICAgaHRtbC5yZW1vdmVDaGlsZChzY3JpcHQpO1xuICAgICAgICAgICAgICAgIHNjcmlwdCA9IG51bGw7XG4gICAgICAgICAgICB9O1xuICAgICAgICAgICAgaHRtbC5hcHBlbmRDaGlsZChzY3JpcHQpO1xuICAgICAgICAgICAgcmV0dXJuIGhhbmRsZTtcbiAgICAgICAgfTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBpbnN0YWxsU2V0VGltZW91dEltcGxlbWVudGF0aW9uKCkge1xuICAgICAgICBzZXRJbW1lZGlhdGUgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhciBoYW5kbGUgPSBhZGRGcm9tU2V0SW1tZWRpYXRlQXJndW1lbnRzKGFyZ3VtZW50cyk7XG4gICAgICAgICAgICBzZXRUaW1lb3V0KHBhcnRpYWxseUFwcGxpZWQocnVuSWZQcmVzZW50LCBoYW5kbGUpLCAwKTtcbiAgICAgICAgICAgIHJldHVybiBoYW5kbGU7XG4gICAgICAgIH07XG4gICAgfVxuXG4gICAgLy8gSWYgc3VwcG9ydGVkLCB3ZSBzaG91bGQgYXR0YWNoIHRvIHRoZSBwcm90b3R5cGUgb2YgZ2xvYmFsLCBzaW5jZSB0aGF0IGlzIHdoZXJlIHNldFRpbWVvdXQgZXQgYWwuIGxpdmUuXG4gICAgdmFyIGF0dGFjaFRvID0gT2JqZWN0LmdldFByb3RvdHlwZU9mICYmIE9iamVjdC5nZXRQcm90b3R5cGVPZihnbG9iYWwpO1xuICAgIGF0dGFjaFRvID0gYXR0YWNoVG8gJiYgYXR0YWNoVG8uc2V0VGltZW91dCA/IGF0dGFjaFRvIDogZ2xvYmFsO1xuXG4gICAgLy8gRG9uJ3QgZ2V0IGZvb2xlZCBieSBlLmcuIGJyb3dzZXJpZnkgZW52aXJvbm1lbnRzLlxuICAgIGlmICh7fS50b1N0cmluZy5jYWxsKGdsb2JhbC5wcm9jZXNzKSA9PT0gXCJbb2JqZWN0IHByb2Nlc3NdXCIpIHtcbiAgICAgICAgLy8gRm9yIE5vZGUuanMgYmVmb3JlIDAuOVxuICAgICAgICBpbnN0YWxsTmV4dFRpY2tJbXBsZW1lbnRhdGlvbigpO1xuXG4gICAgfSBlbHNlIGlmIChjYW5Vc2VQb3N0TWVzc2FnZSgpKSB7XG4gICAgICAgIC8vIEZvciBub24tSUUxMCBtb2Rlcm4gYnJvd3NlcnNcbiAgICAgICAgaW5zdGFsbFBvc3RNZXNzYWdlSW1wbGVtZW50YXRpb24oKTtcblxuICAgIH0gZWxzZSBpZiAoZ2xvYmFsLk1lc3NhZ2VDaGFubmVsKSB7XG4gICAgICAgIC8vIEZvciB3ZWIgd29ya2Vycywgd2hlcmUgc3VwcG9ydGVkXG4gICAgICAgIGluc3RhbGxNZXNzYWdlQ2hhbm5lbEltcGxlbWVudGF0aW9uKCk7XG5cbiAgICB9IGVsc2UgaWYgKGRvYyAmJiBcIm9ucmVhZHlzdGF0ZWNoYW5nZVwiIGluIGRvYy5jcmVhdGVFbGVtZW50KFwic2NyaXB0XCIpKSB7XG4gICAgICAgIC8vIEZvciBJRSA24oCTOFxuICAgICAgICBpbnN0YWxsUmVhZHlTdGF0ZUNoYW5nZUltcGxlbWVudGF0aW9uKCk7XG5cbiAgICB9IGVsc2Uge1xuICAgICAgICAvLyBGb3Igb2xkZXIgYnJvd3NlcnNcbiAgICAgICAgaW5zdGFsbFNldFRpbWVvdXRJbXBsZW1lbnRhdGlvbigpO1xuICAgIH1cblxuICAgIGF0dGFjaFRvLnNldEltbWVkaWF0ZSA9IHNldEltbWVkaWF0ZTtcbiAgICBhdHRhY2hUby5jbGVhckltbWVkaWF0ZSA9IGNsZWFySW1tZWRpYXRlO1xufShuZXcgRnVuY3Rpb24oXCJyZXR1cm4gdGhpc1wiKSgpKSk7XG5cbn0pLmNhbGwodGhpcyxyZXF1aXJlKCdfcHJvY2VzcycpKVxuLy8jIHNvdXJjZU1hcHBpbmdVUkw9ZGF0YTphcHBsaWNhdGlvbi9qc29uO2NoYXJzZXQ6dXRmLTg7YmFzZTY0LGV5SjJaWEp6YVc5dUlqb3pMQ0p6YjNWeVkyVnpJanBiSW01dlpHVmZiVzlrZFd4bGN5OXpaWFJwYlcxbFpHbGhkR1V2YzJWMFNXMXRaV1JwWVhSbExtcHpJbDBzSW01aGJXVnpJanBiWFN3aWJXRndjR2x1WjNNaU9pSTdRVUZCUVR0QlFVTkJPMEZCUTBFN1FVRkRRVHRCUVVOQk8wRkJRMEU3UVVGRFFUdEJRVU5CTzBGQlEwRTdRVUZEUVR0QlFVTkJPMEZCUTBFN1FVRkRRVHRCUVVOQk8wRkJRMEU3UVVGRFFUdEJRVU5CTzBGQlEwRTdRVUZEUVR0QlFVTkJPMEZCUTBFN1FVRkRRVHRCUVVOQk8wRkJRMEU3UVVGRFFUdEJRVU5CTzBGQlEwRTdRVUZEUVR0QlFVTkJPMEZCUTBFN1FVRkRRVHRCUVVOQk8wRkJRMEU3UVVGRFFUdEJRVU5CTzBGQlEwRTdRVUZEUVR0QlFVTkJPMEZCUTBFN1FVRkRRVHRCUVVOQk8wRkJRMEU3UVVGRFFUdEJRVU5CTzBGQlEwRTdRVUZEUVR0QlFVTkJPMEZCUTBFN1FVRkRRVHRCUVVOQk8wRkJRMEU3UVVGRFFUdEJRVU5CTzBGQlEwRTdRVUZEUVR0QlFVTkJPMEZCUTBFN1FVRkRRVHRCUVVOQk8wRkJRMEU3UVVGRFFUdEJRVU5CTzBGQlEwRTdRVUZEUVR0QlFVTkJPMEZCUTBFN1FVRkRRVHRCUVVOQk8wRkJRMEU3UVVGRFFUdEJRVU5CTzBGQlEwRTdRVUZEUVR0QlFVTkJPMEZCUTBFN1FVRkRRVHRCUVVOQk8wRkJRMEU3UVVGRFFUdEJRVU5CTzBGQlEwRTdRVUZEUVR0QlFVTkJPMEZCUTBFN1FVRkRRVHRCUVVOQk8wRkJRMEU3UVVGRFFUdEJRVU5CTzBGQlEwRTdRVUZEUVR0QlFVTkJPMEZCUTBFN1FVRkRRVHRCUVVOQk8wRkJRMEU3UVVGRFFUdEJRVU5CTzBGQlEwRTdRVUZEUVR0QlFVTkJPMEZCUTBFN1FVRkRRVHRCUVVOQk8wRkJRMEU3UVVGRFFUdEJRVU5CTzBGQlEwRTdRVUZEUVR0QlFVTkJPMEZCUTBFN1FVRkRRVHRCUVVOQk8wRkJRMEU3UVVGRFFUdEJRVU5CTzBGQlEwRTdRVUZEUVR0QlFVTkJPMEZCUTBFN1FVRkRRVHRCUVVOQk8wRkJRMEU3UVVGRFFUdEJRVU5CTzBGQlEwRTdRVUZEUVR0QlFVTkJPMEZCUTBFN1FVRkRRVHRCUVVOQk8wRkJRMEU3UVVGRFFUdEJRVU5CTzBGQlEwRTdRVUZEUVR0QlFVTkJPMEZCUTBFN1FVRkRRVHRCUVVOQk8wRkJRMEU3UVVGRFFUdEJRVU5CTzBGQlEwRTdRVUZEUVR0QlFVTkJPMEZCUTBFN1FVRkRRVHRCUVVOQk8wRkJRMEU3UVVGRFFUdEJRVU5CTzBGQlEwRTdRVUZEUVR0QlFVTkJPMEZCUTBFN1FVRkRRVHRCUVVOQk8wRkJRMEU3UVVGRFFUdEJRVU5CTzBGQlEwRTdRVUZEUVR0QlFVTkJPMEZCUTBFN1FVRkRRVHRCUVVOQk8wRkJRMEU3UVVGRFFUdEJRVU5CTzBGQlEwRTdRVUZEUVR0QlFVTkJPMEZCUTBFN1FVRkRRVHRCUVVOQklpd2labWxzWlNJNkltZGxibVZ5WVhSbFpDNXFjeUlzSW5OdmRYSmpaVkp2YjNRaU9pSWlMQ0p6YjNWeVkyVnpRMjl1ZEdWdWRDSTZXeUlvWm5WdVkzUnBiMjRnS0dkc2IySmhiQ3dnZFc1a1pXWnBibVZrS1NCN1hHNGdJQ0FnWENKMWMyVWdjM1J5YVdOMFhDSTdYRzVjYmlBZ0lDQnBaaUFvWjJ4dlltRnNMbk5sZEVsdGJXVmthV0YwWlNrZ2UxeHVJQ0FnSUNBZ0lDQnlaWFIxY200N1hHNGdJQ0FnZlZ4dVhHNGdJQ0FnZG1GeUlHNWxlSFJJWVc1a2JHVWdQU0F4T3lBdkx5QlRjR1ZqSUhOaGVYTWdaM0psWVhSbGNpQjBhR0Z1SUhwbGNtOWNiaUFnSUNCMllYSWdkR0Z6YTNOQ2VVaGhibVJzWlNBOUlIdDlPMXh1SUNBZ0lIWmhjaUJqZFhKeVpXNTBiSGxTZFc1dWFXNW5RVlJoYzJzZ1BTQm1ZV3h6WlR0Y2JpQWdJQ0IyWVhJZ1pHOWpJRDBnWjJ4dlltRnNMbVJ2WTNWdFpXNTBPMXh1SUNBZ0lIWmhjaUJ6WlhSSmJXMWxaR2xoZEdVN1hHNWNiaUFnSUNCbWRXNWpkR2x2YmlCaFpHUkdjbTl0VTJWMFNXMXRaV1JwWVhSbFFYSm5kVzFsYm5SektHRnlaM01wSUh0Y2JpQWdJQ0FnSUNBZ2RHRnphM05DZVVoaGJtUnNaVnR1WlhoMFNHRnVaR3hsWFNBOUlIQmhjblJwWVd4c2VVRndjR3hwWldRdVlYQndiSGtvZFc1a1pXWnBibVZrTENCaGNtZHpLVHRjYmlBZ0lDQWdJQ0FnY21WMGRYSnVJRzVsZUhSSVlXNWtiR1VyS3p0Y2JpQWdJQ0I5WEc1Y2JpQWdJQ0F2THlCVWFHbHpJR1oxYm1OMGFXOXVJR0ZqWTJWd2RITWdkR2hsSUhOaGJXVWdZWEpuZFcxbGJuUnpJR0Z6SUhObGRFbHRiV1ZrYVdGMFpTd2dZblYwWEc0Z0lDQWdMeThnY21WMGRYSnVjeUJoSUdaMWJtTjBhVzl1SUhSb1lYUWdjbVZ4ZFdseVpYTWdibThnWVhKbmRXMWxiblJ6TGx4dUlDQWdJR1oxYm1OMGFXOXVJSEJoY25ScFlXeHNlVUZ3Y0d4cFpXUW9hR0Z1Wkd4bGNpa2dlMXh1SUNBZ0lDQWdJQ0IyWVhJZ1lYSm5jeUE5SUZ0ZExuTnNhV05sTG1OaGJHd29ZWEpuZFcxbGJuUnpMQ0F4S1R0Y2JpQWdJQ0FnSUNBZ2NtVjBkWEp1SUdaMWJtTjBhVzl1S0NrZ2UxeHVJQ0FnSUNBZ0lDQWdJQ0FnYVdZZ0tIUjVjR1Z2WmlCb1lXNWtiR1Z5SUQwOVBTQmNJbVoxYm1OMGFXOXVYQ0lwSUh0Y2JpQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNCb1lXNWtiR1Z5TG1Gd2NHeDVLSFZ1WkdWbWFXNWxaQ3dnWVhKbmN5azdYRzRnSUNBZ0lDQWdJQ0FnSUNCOUlHVnNjMlVnZTF4dUlDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNodVpYY2dSblZ1WTNScGIyNG9YQ0pjSWlBcklHaGhibVJzWlhJcEtTZ3BPMXh1SUNBZ0lDQWdJQ0FnSUNBZ2ZWeHVJQ0FnSUNBZ0lDQjlPMXh1SUNBZ0lIMWNibHh1SUNBZ0lHWjFibU4wYVc5dUlISjFia2xtVUhKbGMyVnVkQ2hvWVc1a2JHVXBJSHRjYmlBZ0lDQWdJQ0FnTHk4Z1JuSnZiU0IwYUdVZ2MzQmxZem9nWENKWFlXbDBJSFZ1ZEdsc0lHRnVlU0JwYm5adlkyRjBhVzl1Y3lCdlppQjBhR2x6SUdGc1oyOXlhWFJvYlNCemRHRnlkR1ZrSUdKbFptOXlaU0IwYUdseklHOXVaU0JvWVhabElHTnZiWEJzWlhSbFpDNWNJbHh1SUNBZ0lDQWdJQ0F2THlCVGJ5QnBaaUIzWlNkeVpTQmpkWEp5Wlc1MGJIa2djblZ1Ym1sdVp5QmhJSFJoYzJzc0lIZGxKMnhzSUc1bFpXUWdkRzhnWkdWc1lYa2dkR2hwY3lCcGJuWnZZMkYwYVc5dUxseHVJQ0FnSUNBZ0lDQnBaaUFvWTNWeWNtVnVkR3g1VW5WdWJtbHVaMEZVWVhOcktTQjdYRzRnSUNBZ0lDQWdJQ0FnSUNBdkx5QkVaV3hoZVNCaWVTQmtiMmx1WnlCaElITmxkRlJwYldWdmRYUXVJSE5sZEVsdGJXVmthV0YwWlNCM1lYTWdkSEpwWldRZ2FXNXpkR1ZoWkN3Z1luVjBJR2x1SUVacGNtVm1iM2dnTnlCcGRDQm5aVzVsY21GMFpXUWdZVnh1SUNBZ0lDQWdJQ0FnSUNBZ0x5OGdYQ0owYjI4Z2JYVmphQ0J5WldOMWNuTnBiMjVjSWlCbGNuSnZjaTVjYmlBZ0lDQWdJQ0FnSUNBZ0lITmxkRlJwYldWdmRYUW9jR0Z5ZEdsaGJHeDVRWEJ3YkdsbFpDaHlkVzVKWmxCeVpYTmxiblFzSUdoaGJtUnNaU2tzSURBcE8xeHVJQ0FnSUNBZ0lDQjlJR1ZzYzJVZ2UxeHVJQ0FnSUNBZ0lDQWdJQ0FnZG1GeUlIUmhjMnNnUFNCMFlYTnJjMEo1U0dGdVpHeGxXMmhoYm1Sc1pWMDdYRzRnSUNBZ0lDQWdJQ0FnSUNCcFppQW9kR0Z6YXlrZ2UxeHVJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lHTjFjbkpsYm5Sc2VWSjFibTVwYm1kQlZHRnpheUE5SUhSeWRXVTdYRzRnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdkSEo1SUh0Y2JpQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdkR0Z6YXlncE8xeHVJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lIMGdabWx1WVd4c2VTQjdYRzRnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUdOc1pXRnlTVzF0WldScFlYUmxLR2hoYm1Sc1pTazdYRzRnSUNBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUdOMWNuSmxiblJzZVZKMWJtNXBibWRCVkdGemF5QTlJR1poYkhObE8xeHVJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lIMWNiaUFnSUNBZ0lDQWdJQ0FnSUgxY2JpQWdJQ0FnSUNBZ2ZWeHVJQ0FnSUgxY2JseHVJQ0FnSUdaMWJtTjBhVzl1SUdOc1pXRnlTVzF0WldScFlYUmxLR2hoYm1Sc1pTa2dlMXh1SUNBZ0lDQWdJQ0JrWld4bGRHVWdkR0Z6YTNOQ2VVaGhibVJzWlZ0b1lXNWtiR1ZkTzF4dUlDQWdJSDFjYmx4dUlDQWdJR1oxYm1OMGFXOXVJR2x1YzNSaGJHeE9aWGgwVkdsamEwbHRjR3hsYldWdWRHRjBhVzl1S0NrZ2UxeHVJQ0FnSUNBZ0lDQnpaWFJKYlcxbFpHbGhkR1VnUFNCbWRXNWpkR2x2YmlncElIdGNiaUFnSUNBZ0lDQWdJQ0FnSUhaaGNpQm9ZVzVrYkdVZ1BTQmhaR1JHY205dFUyVjBTVzF0WldScFlYUmxRWEpuZFcxbGJuUnpLR0Z5WjNWdFpXNTBjeWs3WEc0Z0lDQWdJQ0FnSUNBZ0lDQndjbTlqWlhOekxtNWxlSFJVYVdOcktIQmhjblJwWVd4c2VVRndjR3hwWldRb2NuVnVTV1pRY21WelpXNTBMQ0JvWVc1a2JHVXBLVHRjYmlBZ0lDQWdJQ0FnSUNBZ0lISmxkSFZ5YmlCb1lXNWtiR1U3WEc0Z0lDQWdJQ0FnSUgwN1hHNGdJQ0FnZlZ4dVhHNGdJQ0FnWm5WdVkzUnBiMjRnWTJGdVZYTmxVRzl6ZEUxbGMzTmhaMlVvS1NCN1hHNGdJQ0FnSUNBZ0lDOHZJRlJvWlNCMFpYTjBJR0ZuWVdsdWMzUWdZR2x0Y0c5eWRGTmpjbWx3ZEhOZ0lIQnlaWFpsYm5SeklIUm9hWE1nYVcxd2JHVnRaVzUwWVhScGIyNGdabkp2YlNCaVpXbHVaeUJwYm5OMFlXeHNaV1FnYVc1emFXUmxJR0VnZDJWaUlIZHZjbXRsY2l4Y2JpQWdJQ0FnSUNBZ0x5OGdkMmhsY21VZ1lHZHNiMkpoYkM1d2IzTjBUV1Z6YzJGblpXQWdiV1ZoYm5NZ2MyOXRaWFJvYVc1bklHTnZiWEJzWlhSbGJIa2daR2xtWm1WeVpXNTBJR0Z1WkNCallXNG5kQ0JpWlNCMWMyVmtJR1p2Y2lCMGFHbHpJSEIxY25CdmMyVXVYRzRnSUNBZ0lDQWdJR2xtSUNobmJHOWlZV3d1Y0c5emRFMWxjM05oWjJVZ0ppWWdJV2RzYjJKaGJDNXBiWEJ2Y25SVFkzSnBjSFJ6S1NCN1hHNGdJQ0FnSUNBZ0lDQWdJQ0IyWVhJZ2NHOXpkRTFsYzNOaFoyVkpjMEZ6ZVc1amFISnZibTkxY3lBOUlIUnlkV1U3WEc0Z0lDQWdJQ0FnSUNBZ0lDQjJZWElnYjJ4a1QyNU5aWE56WVdkbElEMGdaMnh2WW1Gc0xtOXViV1Z6YzJGblpUdGNiaUFnSUNBZ0lDQWdJQ0FnSUdkc2IySmhiQzV2Ym0xbGMzTmhaMlVnUFNCbWRXNWpkR2x2YmlncElIdGNiaUFnSUNBZ0lDQWdJQ0FnSUNBZ0lDQndiM04wVFdWemMyRm5aVWx6UVhONWJtTm9jbTl1YjNWeklEMGdabUZzYzJVN1hHNGdJQ0FnSUNBZ0lDQWdJQ0I5TzF4dUlDQWdJQ0FnSUNBZ0lDQWdaMnh2WW1Gc0xuQnZjM1JOWlhOellXZGxLRndpWENJc0lGd2lLbHdpS1R0Y2JpQWdJQ0FnSUNBZ0lDQWdJR2RzYjJKaGJDNXZibTFsYzNOaFoyVWdQU0J2YkdSUGJrMWxjM05oWjJVN1hHNGdJQ0FnSUNBZ0lDQWdJQ0J5WlhSMWNtNGdjRzl6ZEUxbGMzTmhaMlZKYzBGemVXNWphSEp2Ym05MWN6dGNiaUFnSUNBZ0lDQWdmVnh1SUNBZ0lIMWNibHh1SUNBZ0lHWjFibU4wYVc5dUlHbHVjM1JoYkd4UWIzTjBUV1Z6YzJGblpVbHRjR3hsYldWdWRHRjBhVzl1S0NrZ2UxeHVJQ0FnSUNBZ0lDQXZMeUJKYm5OMFlXeHNjeUJoYmlCbGRtVnVkQ0JvWVc1a2JHVnlJRzl1SUdCbmJHOWlZV3hnSUdadmNpQjBhR1VnWUcxbGMzTmhaMlZnSUdWMlpXNTBPaUJ6WldWY2JpQWdJQ0FnSUNBZ0x5OGdLaUJvZEhSd2N6b3ZMMlJsZG1Wc2IzQmxjaTV0YjNwcGJHeGhMbTl5Wnk5bGJpOUVUMDB2ZDJsdVpHOTNMbkJ2YzNSTlpYTnpZV2RsWEc0Z0lDQWdJQ0FnSUM4dklDb2dhSFIwY0RvdkwzZDNkeTUzYUdGMGQyY3ViM0puTDNOd1pXTnpMM2RsWWkxaGNIQnpMMk4xY25KbGJuUXRkMjl5YXk5dGRXeDBhWEJoWjJVdlkyOXRiWE11YUhSdGJDTmpjbTl6YzBSdlkzVnRaVzUwVFdWemMyRm5aWE5jYmx4dUlDQWdJQ0FnSUNCMllYSWdiV1Z6YzJGblpWQnlaV1pwZUNBOUlGd2ljMlYwU1cxdFpXUnBZWFJsSkZ3aUlDc2dUV0YwYUM1eVlXNWtiMjBvS1NBcklGd2lKRndpTzF4dUlDQWdJQ0FnSUNCMllYSWdiMjVIYkc5aVlXeE5aWE56WVdkbElEMGdablZ1WTNScGIyNG9aWFpsYm5RcElIdGNiaUFnSUNBZ0lDQWdJQ0FnSUdsbUlDaGxkbVZ1ZEM1emIzVnlZMlVnUFQwOUlHZHNiMkpoYkNBbUpseHVJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lIUjVjR1Z2WmlCbGRtVnVkQzVrWVhSaElEMDlQU0JjSW5OMGNtbHVaMXdpSUNZbVhHNGdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ1pYWmxiblF1WkdGMFlTNXBibVJsZUU5bUtHMWxjM05oWjJWUWNtVm1hWGdwSUQwOVBTQXdLU0I3WEc0Z0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnY25WdVNXWlFjbVZ6Wlc1MEtDdGxkbVZ1ZEM1a1lYUmhMbk5zYVdObEtHMWxjM05oWjJWUWNtVm1hWGd1YkdWdVozUm9LU2s3WEc0Z0lDQWdJQ0FnSUNBZ0lDQjlYRzRnSUNBZ0lDQWdJSDA3WEc1Y2JpQWdJQ0FnSUNBZ2FXWWdLR2RzYjJKaGJDNWhaR1JGZG1WdWRFeHBjM1JsYm1WeUtTQjdYRzRnSUNBZ0lDQWdJQ0FnSUNCbmJHOWlZV3d1WVdSa1JYWmxiblJNYVhOMFpXNWxjaWhjSW0xbGMzTmhaMlZjSWl3Z2IyNUhiRzlpWVd4TlpYTnpZV2RsTENCbVlXeHpaU2s3WEc0Z0lDQWdJQ0FnSUgwZ1pXeHpaU0I3WEc0Z0lDQWdJQ0FnSUNBZ0lDQm5iRzlpWVd3dVlYUjBZV05vUlhabGJuUW9YQ0p2Ym0xbGMzTmhaMlZjSWl3Z2IyNUhiRzlpWVd4TlpYTnpZV2RsS1R0Y2JpQWdJQ0FnSUNBZ2ZWeHVYRzRnSUNBZ0lDQWdJSE5sZEVsdGJXVmthV0YwWlNBOUlHWjFibU4wYVc5dUtDa2dlMXh1SUNBZ0lDQWdJQ0FnSUNBZ2RtRnlJR2hoYm1Sc1pTQTlJR0ZrWkVaeWIyMVRaWFJKYlcxbFpHbGhkR1ZCY21kMWJXVnVkSE1vWVhKbmRXMWxiblJ6S1R0Y2JpQWdJQ0FnSUNBZ0lDQWdJR2RzYjJKaGJDNXdiM04wVFdWemMyRm5aU2h0WlhOellXZGxVSEpsWm1sNElDc2dhR0Z1Wkd4bExDQmNJaXBjSWlrN1hHNGdJQ0FnSUNBZ0lDQWdJQ0J5WlhSMWNtNGdhR0Z1Wkd4bE8xeHVJQ0FnSUNBZ0lDQjlPMXh1SUNBZ0lIMWNibHh1SUNBZ0lHWjFibU4wYVc5dUlHbHVjM1JoYkd4TlpYTnpZV2RsUTJoaGJtNWxiRWx0Y0d4bGJXVnVkR0YwYVc5dUtDa2dlMXh1SUNBZ0lDQWdJQ0IyWVhJZ1kyaGhibTVsYkNBOUlHNWxkeUJOWlhOellXZGxRMmhoYm01bGJDZ3BPMXh1SUNBZ0lDQWdJQ0JqYUdGdWJtVnNMbkJ2Y25ReExtOXViV1Z6YzJGblpTQTlJR1oxYm1OMGFXOXVLR1YyWlc1MEtTQjdYRzRnSUNBZ0lDQWdJQ0FnSUNCMllYSWdhR0Z1Wkd4bElEMGdaWFpsYm5RdVpHRjBZVHRjYmlBZ0lDQWdJQ0FnSUNBZ0lISjFia2xtVUhKbGMyVnVkQ2hvWVc1a2JHVXBPMXh1SUNBZ0lDQWdJQ0I5TzF4dVhHNGdJQ0FnSUNBZ0lITmxkRWx0YldWa2FXRjBaU0E5SUdaMWJtTjBhVzl1S0NrZ2UxeHVJQ0FnSUNBZ0lDQWdJQ0FnZG1GeUlHaGhibVJzWlNBOUlHRmtaRVp5YjIxVFpYUkpiVzFsWkdsaGRHVkJjbWQxYldWdWRITW9ZWEpuZFcxbGJuUnpLVHRjYmlBZ0lDQWdJQ0FnSUNBZ0lHTm9ZVzV1Wld3dWNHOXlkREl1Y0c5emRFMWxjM05oWjJVb2FHRnVaR3hsS1R0Y2JpQWdJQ0FnSUNBZ0lDQWdJSEpsZEhWeWJpQm9ZVzVrYkdVN1hHNGdJQ0FnSUNBZ0lIMDdYRzRnSUNBZ2ZWeHVYRzRnSUNBZ1puVnVZM1JwYjI0Z2FXNXpkR0ZzYkZKbFlXUjVVM1JoZEdWRGFHRnVaMlZKYlhCc1pXMWxiblJoZEdsdmJpZ3BJSHRjYmlBZ0lDQWdJQ0FnZG1GeUlHaDBiV3dnUFNCa2IyTXVaRzlqZFcxbGJuUkZiR1Z0Wlc1ME8xeHVJQ0FnSUNBZ0lDQnpaWFJKYlcxbFpHbGhkR1VnUFNCbWRXNWpkR2x2YmlncElIdGNiaUFnSUNBZ0lDQWdJQ0FnSUhaaGNpQm9ZVzVrYkdVZ1BTQmhaR1JHY205dFUyVjBTVzF0WldScFlYUmxRWEpuZFcxbGJuUnpLR0Z5WjNWdFpXNTBjeWs3WEc0Z0lDQWdJQ0FnSUNBZ0lDQXZMeUJEY21WaGRHVWdZU0E4YzJOeWFYQjBQaUJsYkdWdFpXNTBPeUJwZEhNZ2NtVmhaSGx6ZEdGMFpXTm9ZVzVuWlNCbGRtVnVkQ0IzYVd4c0lHSmxJR1pwY21Wa0lHRnplVzVqYUhKdmJtOTFjMng1SUc5dVkyVWdhWFFnYVhNZ2FXNXpaWEowWldSY2JpQWdJQ0FnSUNBZ0lDQWdJQzh2SUdsdWRHOGdkR2hsSUdSdlkzVnRaVzUwTGlCRWJ5QnpieXdnZEdoMWN5QnhkV1YxYVc1bklIVndJSFJvWlNCMFlYTnJMaUJTWlcxbGJXSmxjaUIwYnlCamJHVmhiaUIxY0NCdmJtTmxJR2wwSjNNZ1ltVmxiaUJqWVd4c1pXUXVYRzRnSUNBZ0lDQWdJQ0FnSUNCMllYSWdjMk55YVhCMElEMGdaRzlqTG1OeVpXRjBaVVZzWlcxbGJuUW9YQ0p6WTNKcGNIUmNJaWs3WEc0Z0lDQWdJQ0FnSUNBZ0lDQnpZM0pwY0hRdWIyNXlaV0ZrZVhOMFlYUmxZMmhoYm1kbElEMGdablZ1WTNScGIyNGdLQ2tnZTF4dUlDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUhKMWJrbG1VSEpsYzJWdWRDaG9ZVzVrYkdVcE8xeHVJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ0lITmpjbWx3ZEM1dmJuSmxZV1I1YzNSaGRHVmphR0Z1WjJVZ1BTQnVkV3hzTzF4dUlDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUdoMGJXd3VjbVZ0YjNabFEyaHBiR1FvYzJOeWFYQjBLVHRjYmlBZ0lDQWdJQ0FnSUNBZ0lDQWdJQ0J6WTNKcGNIUWdQU0J1ZFd4c08xeHVJQ0FnSUNBZ0lDQWdJQ0FnZlR0Y2JpQWdJQ0FnSUNBZ0lDQWdJR2gwYld3dVlYQndaVzVrUTJocGJHUW9jMk55YVhCMEtUdGNiaUFnSUNBZ0lDQWdJQ0FnSUhKbGRIVnliaUJvWVc1a2JHVTdYRzRnSUNBZ0lDQWdJSDA3WEc0Z0lDQWdmVnh1WEc0Z0lDQWdablZ1WTNScGIyNGdhVzV6ZEdGc2JGTmxkRlJwYldWdmRYUkpiWEJzWlcxbGJuUmhkR2x2YmlncElIdGNiaUFnSUNBZ0lDQWdjMlYwU1cxdFpXUnBZWFJsSUQwZ1puVnVZM1JwYjI0b0tTQjdYRzRnSUNBZ0lDQWdJQ0FnSUNCMllYSWdhR0Z1Wkd4bElEMGdZV1JrUm5KdmJWTmxkRWx0YldWa2FXRjBaVUZ5WjNWdFpXNTBjeWhoY21kMWJXVnVkSE1wTzF4dUlDQWdJQ0FnSUNBZ0lDQWdjMlYwVkdsdFpXOTFkQ2h3WVhKMGFXRnNiSGxCY0hCc2FXVmtLSEoxYmtsbVVISmxjMlZ1ZEN3Z2FHRnVaR3hsS1N3Z01DazdYRzRnSUNBZ0lDQWdJQ0FnSUNCeVpYUjFjbTRnYUdGdVpHeGxPMXh1SUNBZ0lDQWdJQ0I5TzF4dUlDQWdJSDFjYmx4dUlDQWdJQzh2SUVsbUlITjFjSEJ2Y25SbFpDd2dkMlVnYzJodmRXeGtJR0YwZEdGamFDQjBieUIwYUdVZ2NISnZkRzkwZVhCbElHOW1JR2RzYjJKaGJDd2djMmx1WTJVZ2RHaGhkQ0JwY3lCM2FHVnlaU0J6WlhSVWFXMWxiM1YwSUdWMElHRnNMaUJzYVhabExseHVJQ0FnSUhaaGNpQmhkSFJoWTJoVWJ5QTlJRTlpYW1WamRDNW5aWFJRY205MGIzUjVjR1ZQWmlBbUppQlBZbXBsWTNRdVoyVjBVSEp2ZEc5MGVYQmxUMllvWjJ4dlltRnNLVHRjYmlBZ0lDQmhkSFJoWTJoVWJ5QTlJR0YwZEdGamFGUnZJQ1ltSUdGMGRHRmphRlJ2TG5ObGRGUnBiV1Z2ZFhRZ1B5QmhkSFJoWTJoVWJ5QTZJR2RzYjJKaGJEdGNibHh1SUNBZ0lDOHZJRVJ2YmlkMElHZGxkQ0JtYjI5c1pXUWdZbmtnWlM1bkxpQmljbTkzYzJWeWFXWjVJR1Z1ZG1seWIyNXRaVzUwY3k1Y2JpQWdJQ0JwWmlBb2UzMHVkRzlUZEhKcGJtY3VZMkZzYkNobmJHOWlZV3d1Y0hKdlkyVnpjeWtnUFQwOUlGd2lXMjlpYW1WamRDQndjbTlqWlhOelhWd2lLU0I3WEc0Z0lDQWdJQ0FnSUM4dklFWnZjaUJPYjJSbExtcHpJR0psWm05eVpTQXdMamxjYmlBZ0lDQWdJQ0FnYVc1emRHRnNiRTVsZUhSVWFXTnJTVzF3YkdWdFpXNTBZWFJwYjI0b0tUdGNibHh1SUNBZ0lIMGdaV3h6WlNCcFppQW9ZMkZ1VlhObFVHOXpkRTFsYzNOaFoyVW9LU2tnZTF4dUlDQWdJQ0FnSUNBdkx5QkdiM0lnYm05dUxVbEZNVEFnYlc5a1pYSnVJR0p5YjNkelpYSnpYRzRnSUNBZ0lDQWdJR2x1YzNSaGJHeFFiM04wVFdWemMyRm5aVWx0Y0d4bGJXVnVkR0YwYVc5dUtDazdYRzVjYmlBZ0lDQjlJR1ZzYzJVZ2FXWWdLR2RzYjJKaGJDNU5aWE56WVdkbFEyaGhibTVsYkNrZ2UxeHVJQ0FnSUNBZ0lDQXZMeUJHYjNJZ2QyVmlJSGR2Y210bGNuTXNJSGRvWlhKbElITjFjSEJ2Y25SbFpGeHVJQ0FnSUNBZ0lDQnBibk4wWVd4c1RXVnpjMkZuWlVOb1lXNXVaV3hKYlhCc1pXMWxiblJoZEdsdmJpZ3BPMXh1WEc0Z0lDQWdmU0JsYkhObElHbG1JQ2hrYjJNZ0ppWWdYQ0p2Ym5KbFlXUjVjM1JoZEdWamFHRnVaMlZjSWlCcGJpQmtiMk11WTNKbFlYUmxSV3hsYldWdWRDaGNJbk5qY21sd2RGd2lLU2tnZTF4dUlDQWdJQ0FnSUNBdkx5QkdiM0lnU1VVZ051S0FremhjYmlBZ0lDQWdJQ0FnYVc1emRHRnNiRkpsWVdSNVUzUmhkR1ZEYUdGdVoyVkpiWEJzWlcxbGJuUmhkR2x2YmlncE8xeHVYRzRnSUNBZ2ZTQmxiSE5sSUh0Y2JpQWdJQ0FnSUNBZ0x5OGdSbTl5SUc5c1pHVnlJR0p5YjNkelpYSnpYRzRnSUNBZ0lDQWdJR2x1YzNSaGJHeFRaWFJVYVcxbGIzVjBTVzF3YkdWdFpXNTBZWFJwYjI0b0tUdGNiaUFnSUNCOVhHNWNiaUFnSUNCaGRIUmhZMmhVYnk1elpYUkpiVzFsWkdsaGRHVWdQU0J6WlhSSmJXMWxaR2xoZEdVN1hHNGdJQ0FnWVhSMFlXTm9WRzh1WTJ4bFlYSkpiVzFsWkdsaGRHVWdQU0JqYkdWaGNrbHRiV1ZrYVdGMFpUdGNibjBvYm1WM0lFWjFibU4wYVc5dUtGd2ljbVYwZFhKdUlIUm9hWE5jSWlrb0tTa3BPMXh1SWwxOSJdfQ==
