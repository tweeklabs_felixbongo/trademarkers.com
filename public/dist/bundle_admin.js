(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({"./app/js/admin/admin_app.js":[function(require,module,exports){
'use strict';
/*global angular, moment, io, console */

require('./admin_controllers.js');
require('./admin_services.js');
require('../modules/modules.js');

angular.module('tks.admin', ['ngResource', 'ngRoute', 'ngCookies', 'ui.bootstrap', 'mod.pager', 'mod.authorize', 'mod.countries', 'admin_tks.services', 'tks.services2', 'admin_tks.controllers', 'tks.filters', 'tks.directives', 'tks.directives2']).config(require('./admin_routes.js')).run(["$rootScope", "$location", "authService", function ($rootScope, $location, authService) {
  console.log('haha');
  console.log('hello');
  $rootScope.$on("$routeChangeStart", function (event, next, current) {

    console.log('changing');
    console.log(next.$$route);

    if (next.$$route !== undefined) {
      if (next.$$route.isPriv !== undefined) {
        if (next.$$route.isPriv) {
          if (!authService.isSignedIn()) $location.path("/signin");
        }
      }
    }
  });
}]).factory('authInterceptor', ['$q', '$location', 'Session', function ($q, $location, Session) {
  return {
    request: function request(config) {
      config.headers = config.headers || {};
      if (Session.getSessionID()) {
        config.headers.Authorization = 'Bearer ' + Session.getSessionID();
      }
      return config;
    },
    response: function response(_response) {
      console.log('resp', _response.status);
      return _response || $q.when(_response);
    },
    responseError: function responseError(rejection) {
      console.log('resp', rejection.status);
      if (rejection.status === 401) {
        if (rejection.data.indexOf("jwt expired") !== -1) {
          Session.destroy();
          $location.path('/signin');
        } else {
          $location.path('/');
        }
      }
      return $q.reject(rejection);

      //        return response || $q.when(response);
    }
  };
}]).config(['$httpProvider', function ($httpProvider) {
  $httpProvider.interceptors.push('authInterceptor');
}]);

/*
 var app = angular.module('tks', ['ngResource', 'ngRoute', 'mod.pager', 'admin_tks.controllers','admin_tks.services', 'tks.services', 'tks.services2', 'tks.filters',
 'tks.countries','tks.directives', 'ui.bootstrap'])
 */

/*
 var app = angular.module('tks', ['ngResource', 'ngRoute', 'mod.pager', 'mod.authorize', 'admin_tks.controllers','admin_tks.services',
 'ui.bootstrap'])
 .config(['$routeProvider', function ($routeProvider) {

 $routeProvider.
 when('/home', {templateUrl: 'partials/admin_home.html', controller: 'homeCtrl'}).

 when('/about', {templateUrl: 'partials/about.html', controller: 'homeCtrl'}).
 when('/signups', {templateUrl: 'partials/admin_signups.html', controller: 'signupsCtrl'}).
 when('/registrations', {templateUrl: 'partials/admin_registrations.html', controller: 'registrationsCtrl'}).
 when('/trademarks', {templateUrl: 'partials/admin_trademarks.html', controller: 'trademarksCtrl'}).
 when('/signin', {templateUrl: 'partials/signin.html', controller: 'admin_UserController'}).
 when('/dashboard', {templateUrl: 'partials/dashboard.html', controller: 'UserController'}).
 when('/signout', {templateUrl: 'partials/comming.html', controller: 'UserController'}).
 when('/classes', {templateUrl: 'partials/classes.html', controller: 'classesCtrl'}).
 when('/search_classes', {templateUrl: 'partials/search_classes.html', controller: 'search_classesController'}).
 when('/supported_browsers', {templateUrl: 'partials/supported_browsers.html', controller: 'signupCtrl'}).
 otherwise({redirectTo: '/home'});
 }]).run(["$rootScope", "$location", "authService",   function ($rootScope, $location, authService) {
 console.log('haha')
 console.log('hello')
 $rootScope.$on("$routeChangeStart", function (event, next, current) {

 console.log('changing')
 console.log(next.$$route)

 if (next.$$route !== undefined) {
 if (next.$$route.isPriv !== undefined) {
 if (next.$$route.isPriv) {
 if (!authService.isSignedIn())
 $location.path("/signin");
 }
 }
 }
 });
 }])
 */

/*
 .factory('authInterceptor', ['$q', '$location', 'Session', function ($q, $location, Session) {
 return {
 request: function (config) {
 config.headers = config.headers || {};
 if (Session.getSessionID()) {
 config.headers.Authorization = 'Bearer ' + Session.getSessionID()
 }
 return config;
 },
 response: function (response) {
 console.log('resp', response.status)
 return response || $q.when(response);
 },
 responseError: function (response) {
 console.log('resp', response.status)
 if (response.status === 401) {
 if (response.data.indexOf("jwt expired") !== -1) {
 Session.destroy()
 $location.path('/signin')
 return $q.reject(response);
 } else {
 $location.path('/')
 return $q.reject(response);
 }
 }
 return response || $q.when(response);
 }
 }
 }])

 app.config(['$httpProvider', function ($httpProvider) {
 $httpProvider.interceptors.push('authInterceptor');
 }])


 app.factory('langs', function () {

 var langs = [
 { "value": "01", "name": "Class 01", "iso": "ar"},
 { "value": "02", "name": "Class 02", "iso": "zh-CHS"},
 { "value": "03", "name": "Class 03", "iso": "zh-CHT"},
 { "value": "04", "name": "Class 04", "iso": "zh-CHT"},
 { "value": "05", "name": "Class 05", "iso": "zh-CHT"},
 { "value": "06", "name": "Class 06", "iso": "zh-CHT"},
 { "value": "07", "name": "Class 07", "iso": "zh-CHT"},
 { "value": "08", "name": "Class 08", "iso": "zh-CHT"},
 { "value": "09", "name": "Class 09", "iso": "zh-CHT"},
 { "value": "10", "name": "Class 10", "iso": "zh-CHT"},
 { "value": "11", "name": "Class 11", "iso": "zh-CHT"}

 ]
 return langs;
 });

 */

},{"../modules/modules.js":"/home/wang/projects/tks_project/tks/app/js/modules/modules.js","./admin_controllers.js":"/home/wang/projects/tks_project/tks/app/js/admin/admin_controllers.js","./admin_routes.js":"/home/wang/projects/tks_project/tks/app/js/admin/admin_routes.js","./admin_services.js":"/home/wang/projects/tks_project/tks/app/js/admin/admin_services.js"}],"/home/wang/projects/tks_project/tks/app/js/admin/admin_controllers.js":[function(require,module,exports){
"use strict";

/*global angular */
var utilities = require('../modules/utilities.js');

// returns a promise, used in resolve for staffs

var get_staffs = ['$route', '$http', function ($route, $http) {

    return $http.get('/admin_api/get_staffs/').then(function success(response) {
        return response.data;
    }, function error(reason) {
        return false;
    });
}];

angular.module('admin_tks.controllers', []).controller('help_info', function () {}).controller('homeCtrl', ["$scope", function ($scope) {
    $scope.thanks = 'thanks';

    $scope.goto_case = function (case_code) {
        console.log('case code is ', case_code);
    };
}]).controller('HeaderController', ["$scope", "$location", "$rootScope", "authService", "AUTH_EVENTS", function ($scope, $location, $rootScope, authService, AUTH_EVENTS) {

    $scope.init = function () {
        console.log('initting');
        if (authService.isSignedIn()) {
            if ($scope.userName === null) $scope.userName = authService.userName();
        }
    };

    $scope.isBrowserSupported = function () {
        //        return  /chrome/.test(navigator.userAgent.toLowerCase());
        return utilities.supportedBrowser(navigator.userAgent, 'angular');
    };

    $scope.isActive = function (viewLocation) {
        return viewLocation === $location.path();
    };

    $scope.isLoggedIn = function () {
        return authService.isSignedIn();
    };

    $scope.logOut = function () {
        authService.logOut();
        $rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);
        $location.path('/');
    };

    $scope.userName = null;
    $scope.cartCount = 0;

    $scope.$on(AUTH_EVENTS.loginSuccess, function (e) {
        $scope.userName = authService.userName();
    });

    $scope.$on(AUTH_EVENTS.logoutSuccess, function (e) {
        $scope.userName = authService.userName();
    });

    $scope.init();
}]).controller('classesCtrl', ['$scope', '$http', function ($scope, $http) {

    $scope.ready = true;

    $http.get('/api2/cr_classes/').success(function (data) {
        $scope.classes = data;
        $scope.ready = true;
    });
}]).controller('admin_UserController', ['$scope', '$http', '$location', '$rootScope', 'authService', 'AUTH_EVENTS', function ($scope, $http, $location, $rootScope, authService, AUTH_EVENTS) {

    $scope.alerts = [];

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.authenticate = function (user) {

        authService.signIn(user).then(function (status) {
            if (status.authenticated === true) {

                $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                $location.path('/registrations');
            } else {
                $scope.alerts = [];
                $scope.alerts.push({ type: 'warning', msg: status.message });
            }
        });
    };

    $scope.forgot_password = function (user) {
        if (user === undefined || !user.email) {
            $scope.alerts = [];
            $scope.alerts.push({ type: 'warning', msg: 'Please enter an email address first' });
        } else {
            authService.request_password_reset(user.email).then(function (result) {
                $scope.alerts = [];
                if (result.status) $scope.alerts.push({
                    type: 'success',
                    msg: 'We have sent an email with password reset instruction, please check your inbox'
                });else $scope.alerts.push({ type: 'warning', msg: result.message });
            });
        }
    };
}]).controller('signupsCtrl', ['$scope', '$http', '$routeParams', 'Signups', function ($scope, $http, $routeParams, Signups) {
    console.log('signups');
    /*
     $scope.s_items = Signups.query(function () {
     console.log($scope.s_items)
     console.log('signups')
     });
      console.log('reg list')
     */
    $scope.pageSize = 10;

    /*
     $scope.s_items = Registrations.query(function() {
     console.log('reg list2')
     console.log($scope.s_items)
     });
     */

    $scope.setDataSource = function (page_no) {
        console.log(page_no);
        //var offset = req.query.page_no * req.query.page_size;
        $scope.s_items = Signups.query({ page_no: page_no, page_size: 10 }, function () {
            console.log($scope.s_items);
        });
    };

    /*
     $scope.s_items = Registrations.query(function() {
     console.log($scope.s_items)
     });
     */
    $scope.currentPage = 0;

    $scope.setNumberOfPages = function () {
        console.log('init');
        //      $scope.numberOfPages = 20
        $http.get('/admin_api/signups_count').success(function (data) {
            $scope.numberOfPages = Math.ceil(data.count / $scope.pageSize);
            console.log($scope.numberOfPages);
        });
    };

    if ($routeParams.page_no) {
        $scope.currentPage = $routeParams.page_no - 1;
    }

    $scope.setDataSource($scope.currentPage); // good for first page
}]).controller('registrationsCtrl', ['$scope', '$http', '$routeParams', 'Registrations', function ($scope, $http, $routeParams, Registrations) {

    console.log('reg list');

    $scope.pageSize = 10;

    /*
     $scope.s_items = Registrations.query(function() {
     console.log('reg list2')
     console.log($scope.s_items)
     });
     */

    $scope.setDataSource = function (page_no) {
        console.log(page_no);
        //var offset = req.query.page_no * req.query.page_size;
        $scope.s_items = Registrations.query({ page_no: page_no, page_size: 10 }, function () {
            console.log($scope.s_items);
        });
    };

    /*
     $scope.s_items = Registrations.query(function() {
     console.log($scope.s_items)
     });
     */
    $scope.currentPage = 0;

    $scope.setNumberOfPages = function () {
        console.log('init');
        //      $scope.numberOfPages = 20
        $http.get('/admin_api/registrations_count').success(function (data) {
            $scope.numberOfPages = Math.ceil(data.count / $scope.pageSize);
            console.log($scope.numberOfPages);
        });
    };

    if ($routeParams.page_no) {
        $scope.currentPage = $routeParams.page_no - 1;
    }

    $scope.setDataSource($scope.currentPage); // good for first page
}]).controller('trademarksCtrl', ['$scope', '$http', 'Trademarks', function ($scope, $http, Trademarks) {
    $scope.s_items = Trademarks.query(function () {});
}]).controller('profileController', ['$scope', '$http', '$routeParams', '$location', 'countries', 'DBStore', function ($scope, $http, $routeParams, $location, countries, DBStore) {

    console.log($routeParams);
    $scope.natures = [{ name: 'Company', code: 'C' }, { name: 'Individual', code: 'I' }];
    $scope.countries = countries;

    if ($routeParams.backlink) $scope.backlink = DBStore.get($routeParams.backlink);

    $scope.used_in_admin = true;

    console.log($routeParams);
    if ($routeParams.id === 'new') {
        $scope.ready = true;
    } else {

        $http.get('/admin_api/profile/' + $routeParams.id).then(function (data) {
            console.log(data);
            $scope.profile = data.data;
            $scope.ready = true;
            return $http.get('/admin_api/account/' + $scope.profile.user_id);
        }).then(function (account) {
            console.log(account);
            $scope.user = account.data;
            return $http.get('/admin_api/profiles/' + $scope.user._id);
        }).then(function (profiles) {

            console.log('uu', profiles);
            $scope.profiles = profiles.data;
            console.log($scope.profiles);
        });
    }

    $scope.cancel = function () {
        if ($scope.backlink) $location.path($scope.backlink);
    };

    $scope.update_profile = function (profile) {

        $http.post("/admin_api/update_profile", profile).success(function (data) {
            console.log(data);
            if (data.status === 'ok') {
                $scope.show_msg = "Your profile has been updated.";
                if ($scope.backlink) {
                    $location.path($scope.backlink);
                }
            } else {
                $scope.show_msg = data.msg;
            }
        });
    };
}]).controller('registrationDetailController', ['$scope', '$route', '$http', '$rootScope', '$routeParams', '$modal', 'AdminServices', 'regstrn', function ($scope, $route, $http, $rootScope, $routeParams, $modal, AdminServices, regstrn) {

    console.log(regstrn);

    $scope.ref = $routeParams.id;
    $scope.details = regstrn.data;

    $scope.create_ticket = function (_details) {
        console.log('create_tickets');
        console.log(_details);
        console.log('create_ticketsxx');

        var modalInstance = $modal.open({
            templateUrl: 'partials/admin_create_ticket.html',
            windowClass: 'app-modal-window',
            size: 'sm',
            controller: 'ModalCreateTicket', resolve: {
                staffs: get_staffs,
                details: function details() {
                    return _details;
                }
            }
        });

        modalInstance.result.then(function (ticket_info) {
            console.log('select', ticket_info);
            console.log(ticket_info.staff);
            if (ticket_info.staff !== undefined) {

                ticket_info.registration_id = _details._id;

                console.log(ticket_info);

                AdminServices.create_ticket(ticket_info).then(function (data) {
                    console.log('back ', data);
                    console.log('backx', data.status);
                    if (data.status) {
                        console.log('asf');
                        $route.reload();
                    }
                });
            }
        }, function () {
            // $log.info('Modal dismissed at: ' + new Date());
        });
    };
}]).controller('ModalCreateTicket', ['$scope', '$modalInstance', 'details', 'staffs', function ($scope, $modalInstance, details, staffs) {

    $scope.details = details;
    $scope.staffs = staffs;

    console.log('staffs', staffs);
    //      $scope.ticket = { remark:'remark'}
    $scope.ticket = {};

    $scope.ok = function () {
        console.log($scope.ticket);
        $modalInstance.close($scope.ticket);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]).controller('ticketController', ['$scope', '$route', '$http', '$rootScope', '$routeParams', '$modal', 'AdminServices', 'ticket', function ($scope, $route, $http, $rootScope, $routeParams, $modal, AdminServices, ticket) {

    var vm = this;
    console.log(ticket);
    console.log('w');
    console.log('y');
    this.ticket = ticket;
    this.msg = 'ok';

    AdminServices.get_registration(ticket.registration_id).then(function (data) {
        console.log('123', data);
        // $scope.this.reg = data
        vm.registration = data;
        // this.registration = data
    });

    $scope.append_note = function (note, action) {
        console.log('append ');
        console.log(note);

        AdminServices.append_ticket_element({
            ticket_id: vm.ticket._id,
            msg: note,
            action: action
        }).then(function (data) {
            console.log('xyz', data);
            $route.reload();
        });
    };

    $scope.delete_element = function (ele) {

        console.log(vm.ticket);
        console.log('delete e', ele);

        if (confirm('Do you really want to delete this item?')) {

            AdminServices.delete_ticket_element(vm.ticket._id, ele).then(function (data) {
                console.log(data);
                $route.reload();
            });
        }
    };
}]).controller('accountController', ['$scope', '$http', '$routeParams', 'countries', function ($scope, $http, $routeParams, countries) {

    $scope.alerts = [];
    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.used_in_admin = true;
    $scope.countries = countries;

    $http.get('/admin_api/account/' + $routeParams.id).then(function (data) {

        $scope.user = data.data;

        if ($scope.user.admin) $scope.admin = $scope.user.admin;

        $scope.ready = true;

        return $http.get('/admin_api/profiles/' + $routeParams.id);
    }).then(function (profiles) {

        $scope.profiles = profiles.data;
    });

    $scope.update_admin = function (admin) {
        console.log(admin);
        $http.post("/admin_api/update_admin", {
            user_id: $scope.user._id,
            admin: admin
        }).success(function (data) {
            console.log(data);
            if (data.status === 'ok') {

                $scope.show_msg = 'Updated';
            } else {
                $scope.show_msg = data.show_message;
            }
        });
    };

    $scope.update_user = function (user) {

        $http.post("/admin_api/update_user", user).success(function (data) {
            console.log(data);
            if (data.status === 'ok') {
                $scope.show_msg = "This account has been updated.";
            } else {
                $scope.show_msg = data.msg;
            }
        });
    };

    $scope.clear_msg = function () {
        $scope.show_msg = '';
    };
}]).controller('cartsCtrl', ['$scope', '$http', '$routeParams', 'Carts', require('./controllers/carts').carts]).controller('cartController', ['$scope', '$http', '$routeParams', '$modal', 'countries', require('./controllers/cart').cart]).controller('cartAddController', ['$scope', '$http', '$routeParams', '$location', 'countries', 'AdminServices', require('./controllers/cart_add').cart_add]).controller('cartCopyController', ['$scope', '$http', '$routeParams', '$location', 'countries', 'AdminServices', 'Carts', require('./controllers/cart_copy').cart_copy]).controller('admin_MessagesController', ['$scope', '$http', function ($scope, $http) {

    $scope.messages = null;

    $http.get('/admin_api/messages').success(function (data) {
        console.log(data);
        $scope.messages = data;
        $scope.ready = true;
    });
}]).controller('ordersCtrl', ['$scope', '$http', '$routeParams', 'Orders', function ($scope, $http, $routeParams, Orders) {

    console.log('orders list');

    $scope.pageSize = 10;

    $scope.setDataSource = function (page_no) {
        console.log(page_no);
        $scope.s_items = Orders.query({ page_no: page_no, page_size: 10 }, function () {
            console.log($scope.s_items);
        });
    };

    $scope.currentPage = 0;

    $scope.setNumberOfPages = function () {
        $http.get('/admin_api/orders_count').success(function (data) {
            $scope.numberOfPages = Math.ceil(data.count / $scope.pageSize);
            console.log($scope.numberOfPages);
        });
    };

    if ($routeParams.page_no) {
        $scope.currentPage = $routeParams.page_no - 1;
    }

    $scope.setDataSource($scope.currentPage); // good for first page
}]).controller('orderDetailController', ['$scope', '$http', '$routeParams', function ($scope, $http, $routeParams) {
    $scope.ref = $routeParams.id;
    $http.get('/api/order/' + $routeParams.id).success(function (data) {
        console.log(data);
        $scope.details = data.data;
        console.log('details');
        console.log($scope.details);
    });

    $scope.info = { email: '' };

    $scope.canSend = true;

    $scope.emailOrder = function () {

        console.log('email order');
        console.log($scope.info);

        $scope.canSend = false;

        $http.get('/admin_api/email_order/' + $routeParams.id + '/' + $scope.info.email).success(function (data) {

            console.log('back');
            console.log(data);
            //   $scope.details = data.data
            //  console.log('details')
            //  console.log($scope.details)
        });
    };
}]).controller('logsCtrl', ['$scope', '$http', '$routeParams', 'Logs', function ($scope, $http, $routeParams, Logs) {

    console.log('orders list');

    $scope.pageSize = 10;

    $scope.setDataSource = function (page_no) {
        console.log(page_no);
        $scope.s_items = Logs.query({ page_no: page_no, page_size: 10 }, function () {
            console.log($scope.s_items);
        });
    };

    $scope.currentPage = 0;

    $scope.setNumberOfPages = function () {
        $http.get('/admin_api/logs_count').success(function (data) {
            $scope.numberOfPages = Math.ceil(data.count / $scope.pageSize);
            console.log($scope.numberOfPages);
        });
    };

    if ($routeParams.page_no) {
        $scope.currentPage = $routeParams.page_no - 1;
    }

    $scope.setDataSource($scope.currentPage); // good for first page
}]).controller('customPriceController', ['$scope', '$http', '$routeParams', 'countries', function ($scope, $http, $routeParams, countries) {

    $scope.prices = {};

    $scope.alerts = [];
    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.used_in_admin = true;
    $scope.countries = countries;

    $scope.buf = { country: undefined };

    $scope.$watch('buf.country', function (newVal, oldVal) {
        console.log('changed', newVal, oldVal);
        if (newVal !== undefined) {
            $http.get('/api2/get_default_price/' + newVal).success(function (price) {
                console.log(price);
                if (price instanceof Array) {

                    console.log(price);
                    $scope.buf.study1_A = price[0].study1;
                    $scope.buf.study2_A = price[0].study2;

                    $scope.buf.reg1_A = price[0].reg1;
                    $scope.buf.reg2_A = price[0].reg2;

                    $scope.buf.cert1_A = price[0].cert1;
                    $scope.buf.cert2_A = price[0].cert2;

                    $scope.buf.study1_B = price[1].study1;
                    $scope.buf.study2_B = price[1].study2;

                    $scope.buf.reg1_B = price[1].reg1;
                    $scope.buf.reg2_B = price[1].reg2;

                    $scope.buf.cert1_B = price[1].cert1;
                    $scope.buf.cert2_B = price[1].cert2;
                } else {
                    if (Object.keys(price).length !== 0) {
                        console.log(price);

                        $scope.buf.study1_A = price.study1;
                        $scope.buf.study2_A = price.study2;

                        $scope.buf.reg1_A = price.reg1;
                        $scope.buf.reg2_A = price.reg2;

                        $scope.buf.cert1_A = price.cert1;
                        $scope.buf.cert2_A = price.cert2;

                        $scope.buf.study1_B = undefined;
                        $scope.buf.study2_B = undefined;

                        $scope.buf.reg1_B = undefined;
                        $scope.buf.reg2_B = undefined;

                        $scope.buf.cert1_B = undefined;
                        $scope.buf.cert2_B = undefined;
                    } else {
                        console.log('empty');
                        $scope.buf = { country: $scope.buf.country };
                    }
                }
            });
        }
    });

    $http.get('/admin_api/account/' + $routeParams.id).success(function (data) {
        console.log(data);
        $scope.user = data;
        console.log($scope.user);
        if ($scope.user.admin) $scope.admin = $scope.user.admin;

        $http.get('/admin_api//get_custom_prices/' + $routeParams.id).success(function (prices) {
            $scope.prices = prices;
            $scope.ready = true;
            console.log('prices', prices);
        });
    });

    $scope.add_price = function (buf) {
        console.log(buf);
        $scope.prices[buf.country] = angular.copy(buf);
        $scope.buf = {};
    };

    $scope.del_price = function (country) {
        console.log(country);
        delete $scope.prices[country];
    };

    $scope.post_prices = function (prices) {
        console.log(prices);
        $http.post("/admin_api/update_custom_prices/" + $routeParams.id, prices).success(function (data) {
            console.log(data);
            $scope.prices_updated = true;
        });
    };
}]).controller('priceCtrl', ["$scope", '$routeParams', '$http', function ($scope, $routeParams, $http) {

    $http.get('/api2/get_price/' + $routeParams.country).success(function (prices) {
        $scope.prices = prices;
        console.log(prices);
    });
}]).controller('testCtrl', ['$scope', '$http', function ($scope, $http) {

    $scope.test = { sample: '123' };
    $scope.reg = {
        type: 'w',
        classes: [{ "name": "42", "details": "class42" }, { "name": "01", "details": "class01" }, {
            "name": "15",
            "details": "class15"
        }]
    };
}]);

},{"../modules/utilities.js":"/home/wang/projects/tks_project/tks/app/js/modules/utilities.js","./controllers/cart":"/home/wang/projects/tks_project/tks/app/js/admin/controllers/cart.js","./controllers/cart_add":"/home/wang/projects/tks_project/tks/app/js/admin/controllers/cart_add.js","./controllers/cart_copy":"/home/wang/projects/tks_project/tks/app/js/admin/controllers/cart_copy.js","./controllers/carts":"/home/wang/projects/tks_project/tks/app/js/admin/controllers/carts.js"}],"/home/wang/projects/tks_project/tks/app/js/admin/admin_routes.js":[function(require,module,exports){
"use strict";

function login_proc($location, $q, authService) {
  var deferred = $q.defer();

  if (!authService.isSignedIn()) {
    deferred.reject();
    $location.path('/signin');
  } else deferred.resolve();
  return deferred.promise;
}

var loginElement = ['$location', '$q', 'authService', login_proc];

var loginReq = {
  loginRequired: loginElement
};

// get registration details before controller, for 'resolve'

var get_registration_details = ['$route', '$http', function ($route, $http) {
  return $http.get('/api/registration/' + $route.current.params.id).then(function success(response) {
    return response.data;
  }, function error(reason) {
    return false;
  });
}];

var get_staffs = ['$route', '$http', function ($route, $http) {
  console.log('get staffs');
  return $http.get('/admin_api/get_staffs/').then(function success(response) {
    return response.data;
  }, function error(reason) {
    return false;
  });
}];

var get_ticket_details = ['$route', '$http', function ($route, $http) {
  return $http.get('/api_tickets/query_ticket/' + $route.current.params.id).then(function success(response) {
    return response.data;
  }, function error(reason) {
    return false;
  });
}];

module.exports = ['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {

  $routeProvider.when('/', { templateUrl: 'partials/admin_home.html', controller: 'homeCtrl' }).when('/about', { templateUrl: 'partials/about.html', controller: 'homeCtrl' }).when('/whyus', { templateUrl: 'partials/whyus.html', controller: 'homeCtrl' }).when('/terms', { templateUrl: 'partials/terms.html', controller: 'homeCtrl' }).when('/signups', { templateUrl: 'partials/admin_signups.html', controller: 'signupsCtrl' }).when('/registrations', { templateUrl: 'partials/admin_registrations.html', controller: 'registrationsCtrl', isPriv: true, resolve: loginReq }).when('/registration/:id', { templateUrl: 'partials/admin_registration.html', controller: 'registrationDetailController', isPriv: true,
    resolve: {
      loginReq: loginElement,
      regstrn: get_registration_details
    } }).when('/ticket/:id', { templateUrl: 'partials/admin_ticket.html', controller: 'ticketController', controllerAs: 'details', isPriv: true,
    resolve: {
      loginReq: loginElement,
      ticket: get_ticket_details
    } }).when('/orders', { templateUrl: 'partials/admin_orders.html', controller: 'ordersCtrl', isPriv: true }).when('/order/:id', { templateUrl: 'partials/admin_order.html', controller: 'orderDetailController', isPriv: true, resolve: loginReq, reloadOnSearch: false }).when('/trademarks', { templateUrl: 'partials/admin_trademarks.html', controller: 'trademarksCtrl' }).when('/logs', { templateUrl: 'partials/admin_logs.html', controller: 'logsCtrl' }).when('/signin', { templateUrl: 'partials/admin_signin.html', controller: 'admin_UserController' }).when('/dashboard', { templateUrl: 'partials/dashboard.html', controller: 'UserController' }).when('/signout', { templateUrl: 'partials/comming.html', controller: 'UserController' }).when('/classes', { templateUrl: 'partials/classes.html', controller: 'classesCtrl' }).when('/price/:country', { templateUrl: 'partials/admin_price.html', controller: 'priceCtrl' }).when('/messages', { templateUrl: 'partials/admin_messages.html', controller: 'admin_MessagesController', isPriv: true, resolve: loginReq }).when('/profile/:id/:backlink?', { templateUrl: 'partials/admin_profile.html', controller: 'profileController', isPriv: true, resolve: loginReq }).when('/account/:id', { templateUrl: 'partials/admin_account.html', controller: 'accountController', isPriv: true }).when('/custom_prices/:id', { templateUrl: 'partials/admin_custom_prices.html', controller: 'customPriceController', isPriv: true, resolve: loginReq }).when('/carts', { templateUrl: 'partials/admin_carts.html', controller: 'cartsCtrl', isPriv: true }).when('/cart/:id', { templateUrl: 'partials/admin_cart.html', controller: 'cartController', isPriv: true, resolve: loginReq }).when('/add_cart/:id/:transaction', { templateUrl: 'partials/admin_add_cart.html', controller: 'cartAddController', isPriv: true, resolve: loginReq }).when('/copy_cart/:id', { templateUrl: 'partials/admin_copy_cart.html', controller: 'cartCopyController', isPriv: true, resolve: loginReq }).when('/search_classes', { templateUrl: 'partials/search_classes.html', controller: 'search_classesController' }).when('/supported_browsers', { templateUrl: 'partials/supported_browsers.html', controller: 'signupCtrl' }).when('/test_selection', { templateUrl: 'partials/test_selection.html', controller: 'testCtrl' }).otherwise({ redirectTo: '/' });
  $locationProvider.html5Mode(true);
}];

},{}],"/home/wang/projects/tks_project/tks/app/js/admin/admin_services.js":[function(require,module,exports){
"use strict";

/*global angular */

function AdminServices($q, $http) {

  this.create_ticket = function (registration) {

    console.log('create_ticket services');

    var deferred = $q.defer();

    $http.post('/api_tickets/create_ticket', registration).success(function (data) {
      deferred.resolve(data);
    }).error(function (error) {
      deferred.resolve({});
    });

    return deferred.promise;
  };

  this.append_ticket_element = function (tkt_element) {

    var deferred = $q.defer();
    console.log(tkt_element);

    $http.post('/api_tickets/append_ticket_element', tkt_element).success(function (data) {
      deferred.resolve(data);
    }).error(function (error) {
      deferred.resolve({});
    });

    return deferred.promise;
  };

  this.delete_ticket_element = function (ticket_id, tkt_element) {

    var deferred = $q.defer();

    $http.get('/api_tickets/delete_ticket_element/' + ticket_id + '/' + tkt_element.uuid).success(function (data) {
      deferred.resolve(data);
    }).error(function (error) {
      deferred.resolve({});
    });
    return deferred.promise;
  };

  this.get_registration = function (id) {

    console.log('get reg');

    var deferred = $q.defer();

    $http.get('/api/registration/' + id + '?asdfasdf').success(function (data) {
      console.log('getget');
      console.log(data);
      deferred.resolve(data.data);
    }).error(function (error) {
      deferred.resolve({});
    });

    return deferred.promise;
  };

  this.get_profiles = function (user_id) {
    var deferred = $q.defer();

    $http.get('/admin_api/profiles/' + user_id).success(function (data) {
      console.log('getget123');
      console.log(data);
      deferred.resolve(data);
    }).error(function (error) {
      deferred.resolve({}); // TODO: trigger an err
    });

    return deferred.promise;
  };

  this.get_cart = function (user_id) {
    var deferred = $q.defer();

    $http.get('/admin_api/cart/' + user_id).success(function (data) {
      console.log('getget123');
      console.log(data);
      deferred.resolve(data);
    }).error(function (error) {
      deferred.resolve({}); // TODO: trigger an err
    });

    return deferred.promise;
  };
}

angular.module('admin_tks.services', []).value('version', '0.1').factory('Signups', ['$resource', function ($resource) {
  return $resource('/admin_api/signups');
}]).factory('Registrations', ['$resource', function ($resource) {
  return $resource('/admin_api/registrations');
}]).factory('Orders', ['$resource', function ($resource) {
  return $resource('/admin_api/orders');
}]).factory('Carts', ['$resource', function ($resource) {
  return $resource('/admin_api/carts');
}]).factory('Logs', ['$resource', function ($resource) {
  return $resource('/admin_api/logs');
}]).factory('', ['$resource', function ($resource) {
  return $resource('/admin_api/logs');
}]).service('AdminServices', ['$q', '$http', AdminServices]);

},{}],"/home/wang/projects/tks_project/tks/app/js/admin/controllers/cart.js":[function(require,module,exports){
"use strict";

exports.cart = function ($scope, $http, $routeParams, $modal, countries) {

    console.log('routeParams', $routeParams);

    $scope.get_cart = function () {
        $http.get('/admin_api/cart/' + $routeParams.id).then(function (data) {

            console.log('cart data', data);
            console.log('???', data.data === 'null');

            if (data.data !== 'null') {
                $scope.cart = data.data;
                $scope.total = 0;
                $scope.cart.items.map(function (item) {
                    $scope.total = $scope.total + item.amount;
                });
            }
        });
    };

    $scope.delete_item = function (cart_id, rec) {

        if (confirm('Do you really want to delete this item?')) {

            console.log('delete', cart_id);
            console.log(rec);

            $http.get('/admin_api/delete_cart_item/' + cart_id + '/' + rec._id).success(function (data) {

                $scope.get_cart();
            });
        }
    };

    $scope.get_cart();
};

},{}],"/home/wang/projects/tks_project/tks/app/js/admin/controllers/cart_add.js":[function(require,module,exports){
"use strict";

exports.cart_add = function ($scope, $http, $routeParams, $location, countries, AdminServices) {

    $scope.countries = countries;

    $scope.transaction = $routeParams.transaction;

    $scope.row = {
        type: 'w',
        service: $routeParams.transaction,
        amount: 0.0,
        notes: '',
        profile_id: null
    };

    AdminServices.get_cart($routeParams.id).then(function (cart) {

        $scope.cart = cart;

        if (cart.user_id) {
            AdminServices.get_profiles(cart.user_id).then(function (profiles) {
                $scope.profiles = profiles;
            });
        }
    });

    $scope.add_item = function (cart_id, row) {

        console.log('cart_id', cart_id);
        console.log('row', row);

        row.selected = true;

        $http.post("/admin_api/add_cart_item/" + cart_id, row).success(function (data) {
            console.log(data);
            //$scope.prices_updated = true

            $location.path('/cart/' + $scope.cart.user_id);
        });
    };
};

},{}],"/home/wang/projects/tks_project/tks/app/js/admin/controllers/cart_copy.js":[function(require,module,exports){
"use strict";

var _app_tsJsWebWeb_controllers_cart = require("../../../../app_ts/js/web/web_controllers_cart");

"use strict";

exports.cart_copy = function ($scope, $http, $routeParams, $location, countries, AdminServices, Carts) {

    $scope.row = {
        profile_id: null
    };

    $scope.find_cart = function (cart_id, row) {

        $http.get('/admin_api/account_by_email/' + $scope.row.from_email).success(function (user) {

            if (user) {

                AdminServices.get_cart(user._id).then(function (cart) {

                    $scope.scart = cart;

                    $scope.total = 0;
                    $scope.scart.items.map(function (item) {
                        $scope.total = $scope.total + item.amount;
                    });
                });
            } else {
                $scope.scart = null;
            }
        }).error(function (error) {});
    };

    $scope.copy_cart = function () {

        console.log('copy cart');

        // 
        if ($scope.row.profile_id) {

            var url = '/admin_api/copy_items_to_cart?cart_id=' + $scope.scart._id + '&new_cart_id=' + $scope.cart._id + '&new_profile_id=' + $scope.row.profile_id;

            $http.get(url).success(function (data) {

                $scope.msg = 'Items copied';
                $scope.scart = null;
            });
        } else $scope.msg = 'profile is empty';
    };

    $scope.isCartEmpty = function (cart) {
        if (!cart) return true;else {
            return cart.items.length === 0;
        }
    };

    AdminServices.get_cart($routeParams.id).then(function (cart) {

        $scope.cart = cart;

        if (cart.user_id) {
            AdminServices.get_profiles(cart.user_id).then(function (profiles) {
                $scope.profiles = profiles;
            });
        }
    });
};

},{"../../../../app_ts/js/web/web_controllers_cart":"/home/wang/projects/tks_project/tks/app_ts/js/web/web_controllers_cart.js"}],"/home/wang/projects/tks_project/tks/app/js/admin/controllers/carts.js":[function(require,module,exports){
"use strict";

exports.carts = function ($scope, $http, $routeParams, Carts) {

    console.log('carts list');

    $scope.pageSize = 10;

    $scope.setDataSource = function (page_no) {
        console.log(page_no);
        this.s_items = Carts.query({ page_no: page_no, page_size: 10 }, function () {});
    };

    $scope.currentPage = 0;

    $scope.setNumberOfPages = function () {

        $http.get('/admin_api/carts_count').success(function (data) {
            $scope.numberOfPages = Math.ceil(data.count / $scope.pageSize);
            console.log($scope.numberOfPages);
        });
    };

    if ($routeParams.page_no) {
        $scope.currentPage = $routeParams.page_no - 1;
    }

    $scope.setDataSource($scope.currentPage); // good for first page
};

},{}],"/home/wang/projects/tks_project/tks/app/js/common/profile_directive.js":[function(require,module,exports){
"use strict";
/* global angular */
/* Directives */

var utilities = require('../modules/utilities.js');
var countries = require('countries');

angular.module('profile.directives', []).directive('appVersion', ['version', function (version) {
  return function (scope, elm, attrs) {
    elm.text(version);
  };
}]).directive('profile123', function () {
  console.log('in profile hello');
  return {
    restrict: 'E',
    scope: {
      name: '@',
      datasource: '=' // pass a json object
    },
    template: '<span>Hello {{name}}</span>'
  };
})

// to get name, country of inc, contact and email

.directive('profileNames', ['$http', 'countries', function ($http, countries) {
  return {
    restrict: 'E',
    scope: {
      name: '@',
      datasource: '='
    },
    templateUrl: '/partials/profileNames.html',
    link: function link(scope, el, attr) {

      if (!scope.datasource.inc_country)
        //          $http.get('/api2/get_geo?test_ip=174.54.165.206').success(function (geo) {
        //          $http.get('/api2/get_geo?test_ip=112.210.15.107').success(function (geo) {
        $http.get('/api2/get_geo').success(function (geo) {
          console.log('geo', geo);
          if (geo.country) scope.datasource.inc_country = geo.country;
          scope.datasource.nature = 'C';
        });

      scope.natures = [{ name: 'Company', code: 'C' }, { name: 'Individual', code: 'I' }];
      scope.countries = countries;
    }
  };
}]).directive('profileAddress', ['$http', '$templateCache', '$compile', function ($http, $templateCache, $compile) {
  console.log('in profile3 hello');
  return {
    restrict: 'E',
    scope: {
      name: '@',
      datasource: '=',
      country: '='
    },
    //      templateUrl: '/partials/profileAddress.html',
    //      templateUrl: '/api2/server_partials/address/{{code}}',

    link: function link(scope, el, attr) {

      /*
      scope.states2 = {
        "PH.AB": "test",
        "PH.CB": "cebu"
      }
      */

      scope.states = countries.get_states(scope.country);

      console.log('attr1', attr);
      console.log('ds', scope.datasource);
      console.log('c', scope.country);
      console.log('scope', scope);

      var url = "/api2/server_partials/address/" + scope.country;
      console.log(url);
      loadTemplate("/api2/server_partials/address/" + scope.country);
      console.log('country in watch1', scope.country);

      /*
       scope.$watch(attr.country, function (value) {
        if (value) {
          var url = "/api2/server_partials/address/" + value
          console.log(url)
          loadTemplate("/api2/server_partials/address/" + value);
          console.log('country in watch', value)
        }
      });
      */

      function loadTemplate(template) {
        $http.get(template /*, { cache: $templateCache }*/).success(function (templateContent) {

          //              console.log('dd', templateContent)
          console.log('el', el);
          el.replaceWith($compile(templateContent)(scope));
          console.log('loaded', scope);
          scope.$parent.ready2show = true; // flag to inform the main form to show, it's a hack
        });
      }

      console.log('scope', scope);

      //        scope.code = 'AS'
      /*
      if (!scope.datasource.country)
        $http.get('/api2/get_geo?test_ip=174.54.165.206').success(function (geo) {
          console.log('geo', geo)
          if (geo.country) {
            scope.datasource.country = geo.country
            scope.datasource.state = geo.region
            scope.datasource.city = geo.city
          }
        })
       console.log('datasource2', scope.datasource)
      */
      scope.countries = countries.get_countries();
    }
  };
}]);

},{"../modules/utilities.js":"/home/wang/projects/tks_project/tks/app/js/modules/utilities.js","countries":"/home/wang/projects/tks_project/tks/node_modules/countries/countries.js"}],"/home/wang/projects/tks_project/tks/app/js/common/tks_directives.js":[function(require,module,exports){
"use strict";
/* global angular, jQuery */
/* Directives */

var utilities = require('../modules/utilities.js');

function append_one_word_country_name(lst) {
    var rlst = [];
    for (var i = 0; i < lst.length; i++) {
        lst[i].simple_name = lst[i].name.replace(/ /g, '_').toLowerCase();
        rlst.push(lst[i]);
    }
    return rlst;
}

angular.module('tks.directives', []).directive('appVersion', ['version', function (version) {
    return function (scope, elm, attrs) {
        elm.text(version);
    };
}]).directive('hello2', function () {
    return {
        restrict: 'E',
        scope: {
            name: '@'
        },
        template: '<span>Hello {{name}}</span>'
    };
}).directive('trademarkRequest', function () {
    return {
        restrict: 'E',
        scope: {
            name: '@',
            tmreg: '=',
            profile: '='
        },
        templateUrl: '/partials/tmrequest.html',
        /*
         controller: function ($scope) {
         console.log('ctrl')
         console.log($scope)
         console.log('ctrl2')
         }*/
        link: function link($scope, element) {
            console.log('abc');
            console.log($scope.profile_name);
            console.log($scope);
            console.log('abcx');
        }
        /*
         link: function (scope, element) {
         console.log(scope.tmreg)
         scope.name2 = 'Jeff';
          var temp
         for (var i = 0; i <scope.tmreg.classes.length; i ++) {
         var c = scope.tmreg.classes[0]
         console.log(c)
         if (i === 0)
         temp = c.name + ' (' + c.details + ')'
         else {
         temp = temp + ', ' + c.name + ' (' + c.details + ')'
         }
         }
         scope.classes = temp
         }*/

    };
}).directive('tmPhoto', function () {
    return {
        restrict: 'E',
        scope: {
            psrc: '=',
            tmtype: '='
        },
        template: '<img ng-show="show(tmtype)" ng-src="{{psrc}}" width="100px;" />',
        link: function link(scope, el, attr) {
            scope.show = function (typ) {
                return ['l', 'wl', 'tml'].indexOf(typ) !== -1;
            };
        }
    };
}).directive('code2country', function () {
    return {
        restrict: 'E',
        scope: {
            code: '='
        },
        //      template: '<img src="{{code2src(code)}}" />',
        template: '<img ng-src="{{code2src(code)}}" />',
        link: function link(scope, el, attr) {
            scope.code2src = function (code) {
                console.log(code);
                return 'images/flags/' + code.toLowerCase() + '.png';
            };
        }
    };
}).directive('infoPopover', function () {
    return {
        restrict: 'E',
        scope: {
            info: '@info'
        },
        template: ' <img ng-src="images/info.gif" style="padding-left: 3px;" popover="{{info}}" popover-trigger="mouseenter" popover-placement="left"  />'
        /*
         link: function(scope, el, attr) {
         console.log(scope)
         }*/
    };
}).directive('order123', function () {
    return {
        restrict: 'E',
        scope: {
            details: '='
        },
        template: '<p><h3>details123</h3></p>',
        link: function link(scope, el, attr) {
            console.log('asdfasdf');
            console.log(scope);
        }
    };
}).directive('orderItemDetails', function () {
    return {
        restrict: 'E',
        scope: {
            details: '='
        },
        templateUrl: '/partials/order_item_details.html'
    };
}).directive('orderItemDetailsWithNotes', function () {
    // if notes is present, it is added first by admin
    return {
        restrict: 'E',
        scope: {
            details: '='
        },
        templateUrl: '/partials/order_item_details2.html'
    };
}).directive('loading', function () {
    return {
        restrict: 'E',
        scope: {
            when: '='
        },
        template: '<span ng-show="when"><img ng-src="/images/loading.gif" /></span>'
    };
}).directive('gChart', ['$location', function ($location) {
    return {
        restrict: 'A',
        scope: {
            gid: '=',
            gdata: '=',
            gready: '=',
            dtype: '=' // 'V' videos, 'S' submissions, 'A' assignments
        },
        link: function link(scope, elm, attrs) {

            function get_data() {

                console.log(scope.gdata);
                scope.gdata.fn.then(function (d) {
                    console.log('dd1');
                    console.log(d);
                    console.log('dd2');
                });
            }

            function drawRegionsMap() {
                var data = new google.visualization.DataTable();

                data.addColumn('string', 'Country');
                data.addColumn('number', 'Reference');
                console.log('bbb', scope.gdata);
                //    TMCountries.get().then(function(cnts) {
                //            console.log('ccc', data)
                //          data.addRows(Object.keys(cnts).length)
                data.addRows(Object.keys(scope.gdata).length);

                //            console.log('',Object.keys(cnts).length);

                var k = 0;
                for (var key in scope.gdata) {
                    if (scope.gdata.hasOwnProperty(key)) {
                        //               console.log(cnts[key])
                        data.setCell(k, 0, scope.gdata[key].name);
                        data.setCell(k, 1, scope.gdata[key].price);
                        k = k + 1;
                    }
                }

                var options = {};
                //options['colors'] = ['#FADC41', '#c44949', '#d74a12', '#0e9a0e', 'red', '#7c4b91', '#fadc41', '#0d6cca', '#7c4897'];
                //options['colors'] = [ '#0e9a0e', 'red', '#7c4b91', '#fadc41', '#0d6cca', '#7c4897'];
                options.colors = ['#008bd4', '#008bd4'];
                options.legend = 'none';
                //          options.region = 'ES'

                var chart = new google.visualization.GeoChart(document.getElementById(scope.gid));

                google.visualization.events.addListener(chart, 'regionClick', function (region) {
                    scope.$apply(function () {
                        $location.path('/trademark/' + region.region);
                        /*
                         var cntry = scope.gdata[region.region]
                         if (cntry !== undefined) {
                         if (cntry.link)
                         $location.path('/trademark/' + cntry.link);
                         else
                         $location.path('/trademark/' + region.region);
                         } */
                    });
                });

                scope.gready = true;

                /*
                 google.visualization.events.addListener(chart, 'ready', function () {
                 scope.$apply(function () {
                 console.log('map is ready')
                 scope.gready = true
                 }
                 });
                 */

                chart.draw(data, options);

                //        })
                /*
                 data.addRows(scope.gdata.length)
                   for (var k = 0; k < scope.gdata.length; k ++) {
                 var c = scope.gdata[k]
                 console.log(k,c, c.price, c.code )
                 data.setCell(k, 0, scope.gdata[k].name);
                 data.setCell(k, 1, scope.gdata[k].price);
                 }
                  var options = { };
                 //          options['colors'] = ['#FADC41', '#c44949', '#d74a12', '#0e9a0e', 'red', '#7c4b91', '#fadc41', '#0d6cca', '#7c4897'];
                 //          options['colors'] = [ '#0e9a0e', 'red', '#7c4b91', '#fadc41', '#0d6cca', '#7c4897'];
                 options['legend'] = 'none';
                 //          options.region = 'ES'
                   var chart = new google.visualization.GeoChart(document.getElementById(scope.gid));
                  google.visualization.events.addListener(chart, 'regionClick',
                 function(region) {
                 scope.$apply(function() {
                 $location.path('/trademark/' + region.region);
                 });
                 });
                  google.visualization.events.addListener(chart, 'ready', function() {
                 scope.$apply(function() {
                 console.log('map is ready')
                 scope.gready = true
                 });
                  });
                  chart.draw(data, options);
                 */
            }

            drawRegionsMap();
        }
    };
}])

//  show a list of countries in the region

.directive('regionList', function () {
    return {
        restrict: 'E',
        scope: {
            code: '@',
            cols: '='
        },
        templateUrl: '/partials/regionlist.html',
        link: function link(scope, el, attr) {

            var lst = append_one_word_country_name(utilities.get_countries_by_contnent(scope.code));
            var nums = Math.ceil(lst.length / 3);
            scope.countries1 = lst.slice(0, nums); // 0 13
            scope.countries2 = lst.slice(nums, 2 * nums); // 14
            scope.countries3 = lst.slice(2 * nums, 3 * nums);
        }
    };
}).directive('stripeForm', ['$window', function ($window) {

    var directive = {
        restrict: 'A'
    };
    directive.link = function (scope, element, attributes) {
        var form = angular.element(element);
        form.bind('submit', function () {
            var button = form.find('button');
            button.prop('disabled', true);
            $window.Stripe.createToken(form[0], function () {
                var args = arguments;
                scope.$apply(function () {
                    scope[attributes.stripeForm].apply(scope, args);
                });
                button.prop('disabled', false);
            });
        });
    };
    return directive;
}]).directive('passwordReset', ['authService', function (authService) {
    return {
        restrict: 'A',
        scope: {
            flag: '@'
        },
        templateUrl: '/partials/forgot_password.html',
        //templateUrl: '/partials/test123.html',
        //template: "<h1>hello</h1>",
        link: function link(scope, el, attr) {

            console.log('in reset password');

            scope.alerts = [];

            scope.closeAlert = function (index) {
                scope.alerts.splice(index, 1);
                scope.$parent.show_reset_form = false; // the parent should use this flag to show/hide related elements
                scope.alerts = [];
            };

            scope.info = {};
            scope.reset_password = function () {
                console.log('resetting');
                console.log(scope.info);
                console.log(scope);
                authService.request_password_reset(scope.info.email).then(function (result) {
                    console.log('back reset  result ', result);
                    scope.alerts = [];
                    if (result.status) scope.alerts.push({
                        type: 'success',
                        msg: 'We have sent an email with password reset instruction, please check your inbox'
                    });else scope.alerts.push({
                        type: 'warning',
                        msg: result.message
                    });
                });
            };
            console.log(scope);
        }
    };
}])

/*
 // this assumes the parent has alerts[] defined
 // $scope.alerts.push({
 type: 'warning',
 msg: "this is a message123"
 })
 */

.directive('alertLine', ['$timeout', function ($timeout) {
    return {
        restrict: 'E',
        template: '<alert ng-repeat="alert in alerts" type="{{alert.type}}" close="closeAlert($index)">{{alert.msg}}</alert>',
        link: function link(scope, el, attr) {
            // scope.alerts = [];
            scope.closeAlert = function (index) {
                scope.alerts.splice(index, 1); // alerts is defined in the parent controller
            };
            /*
             $timeout(function(){
             //        scope.alerts.splice(0, 1)
             scope.alerts = []
             }, 3000); // maybe '}, 3000, false);' to avoid calling apply*/
        }
    };
}]).directive('ticker', ['$timeout', function ($timeout) {
    return {
        // Restrict it to be an attribute in this case
        restrict: 'A',
        // responsible for registering DOM listeners as well as updating the DOM
        link: function link(scope, element, attrs) {
            //$(element).toolbar(scope.$eval(attrs.toolbarTip));
            console.log('ticker2');
            //jQuery('#webticker').webTicker()
            jQuery('#news').easyTicker({
                direction: 'up',
                easing: 'swing',
                speed: 'slow',
                interval: 3000,
                height: 'auto',
                visible: 2,
                mousePause: 1,
                controls: {
                    up: '',
                    down: '',
                    toggle: '',
                    playText: 'Play',
                    stopText: 'Stop'
                }
            });
        }
    };
}]).directive('coverflow', ['$location', function ($location) {
    return {
        // Restrict it to be an attribute in this case
        restrict: 'A',
        // responsible for registering DOM listeners as well as updating the DOM
        link: function link(scope, element, attrs) {

            scope.goto_trademark = function (path) {
                console.log('asdfasdf', path);
                $location.path(path);
            };

            scope.mypath = 'bbbbb';

            jQuery('#preview-coverflow').coverflow({
                index: 3,
                innerAngle: -55,
                innerOffset: 9,
                confirm: function confirm(event, cover, index) {
                    console.log('confirmed', cover);
                    scope.$apply(function () {
                        //$location.path('/trademark/US');
                        var img = jQuery(cover).children().andSelf().filter('img').last();
                        console.log('img', img.data('refsrc'));
                        $location.path(img.data('refsrc'));
                    });
                    /*
                     var img = jQuery(cover).children().andSelf().filter('img').last();
                     console.log('img', img.data('refsrc'))
                     $location.path('/trademark/');*/
                },
                select: function select(event, cover) {
                    var img = jQuery(cover).children().andSelf().filter('img').last();
                    jQuery('#photos-name').text('Register ' + img.data('name') || 'unknown');
                    scope.mypath = img.data('refsrc');
                }

            });
        }
    };
}]).directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
}).directive('userLink', ['$http', function ($http) {
    return {
        restrict: 'E',
        template: '<span class="glyphicon glyphicon-user" aria-hidden="true"></span>&nbsp;&nbsp;<a ng-href="account/{{account._id}}">{{account.name}}</a>',
        link: function link(scope, el, attrs) {
            attrs.$observe('usrid', function (value) {
                if (value) {
                    $http.get('/admin_api/account/' + value).then(function (data) {
                        scope.account = data.data;
                    });
                }
            });
        }
    };
}]).directive('profileLink', ['$http', function ($http) {
    return {
        restrict: 'E',
        scope: {
            profile_id: '='
        },
        template: '<span class="glyphicon glyphicon-file" aria-hidden="true"></span>&nbsp;<a ng-href="profile/{{account2._id}}">{{account2.profile_name}}</a>',
        link: function link(scope, el, attrs) {
            console.log('scp2', scope);
            attrs.$observe('profileId', function (value) {
                console.log('profile value', value);
                if (value) {
                    $http.get('/admin_api/profile/' + value).then(function (data) {
                        scope.account2 = data.data;
                    });
                }
            });
        }
    };
}]).directive('profileName', ['$http', 'authService', function ($http, authService) {
    return {
        restrict: 'E',
        scope: {
            profile_id: '='
        },
        template: '<a>{{account2.profile_name}}</a>',
        link: function link(scope, el, attrs) {
            console.log('scp2', scope);
            attrs.$observe('profileId', function (value) {
                console.log('profile value in profileName ', value);
                if (authService.isSignedIn()) {
                    if (value) {

                        $http.get('/api/profile/' + value).then(function success(data) {
                            scope.account2 = data.data;
                        }, function error(error) {
                            console.log('we got an error');
                            console.log(error);
                        });
                    }
                }
            });
        }
    };
}]).directive('jsonld', ['$filter', '$sce', function ($filter, $sce) {
    return {
        restrict: 'E',
        template: function template() {
            return '<script type="application/ld+json" ng-bind-html="onGetJson()"></script>';
        },
        scope: {
            json: '=json'
        },
        link: function link(scope, element, attrs) {
            scope.onGetJson = function () {
                return $sce.trustAsHtml($filter('json')(scope.json));
            };
        },
        replace: true
    };
}]);

},{"../modules/utilities.js":"/home/wang/projects/tks_project/tks/app/js/modules/utilities.js"}],"/home/wang/projects/tks_project/tks/app/js/common/tks_directives2.js":[function(require,module,exports){
"use strict";
/* global angular, jQuery */
/* Directives */

var utilities = require('../modules/utilities.js');

angular.module('tks.directives2', []).directive('trademarkType', ['$http', function ($http) {

    return {
        restrict: 'E',
        scope: {
            reg: '=',
            optns: '='
        },
        templateUrl: '/partials/trademark_type.html',
        link: function link(scope, el, attrs) {
            console.log('scp2', scope);

            scope.uploadFile = function (files) {

                console.log('upload file');
                console.log(files);
                var fd = new FormData();

                //Take the first selected file
                fd.append("logo_file", files[0]);
                console.log('fd', fd);

                $http.post('/api2/upload_file', fd, {
                    withCredentials: true,
                    headers: {
                        'Content-Type': undefined
                    },
                    transformRequest: angular.identity
                }).success(function (data) {
                    scope.reg.logo_url = data.url;
                    scope.upload_status = 'Uploaded';
                }).error(function (data) {
                    console.log('failed', data);
                });
            };
        }
    };
}]).directive('trademarkClasses', ['$http', function ($http) {
    return {
        restrict: 'E',
        scope: {
            reg: '=',
            optns: '='

        },
        templateUrl: '/partials/trademark_classes.html',
        link: function link(scope, el, attrs) {

            console.log('scp2', scope.optns);

            scope.labels = {
                //class_selection : "Select the classes in which you need to register your Trademark:"
                class_selection: "Select the classes which you would like your trademark monitored for:"
            };

            /*
            scope.options = {
                enable_class_detail : false
            }
            */

            scope.classes_selected = [];

            // generate an array for UI
            for (var x = 1; x <= 45; x++) {
                var c = x.toString();
                if (c.length === 1) c = '0' + c;
                scope.classes_selected.push({
                    code: c,
                    selected: false
                });
            }

            classes2array(scope.reg.classes);

            scope.class_change = function (s) {

                if (s.selected) {

                    scope.reg.classes.push({
                        name: s.code,
                        details: ''
                    });
                } else {

                    for (var i = 0; i < scope.reg.classes.length; i++) {
                        if (scope.reg.classes[i].name === s.code) scope.reg.classes.splice(i, 1);
                    }
                }
            };

            function classes2array(classes) {

                for (var k = 0; k < scope.classes_selected.length; k++) scope.classes_selected[k].selected = false;

                for (var i = 0; i < classes.length; i++) {
                    var indx = parseInt(classes[i].name);
                    scope.classes_selected[indx - 1].selected = true;
                }
            }
        }
    };
}]);

},{"../modules/utilities.js":"/home/wang/projects/tks_project/tks/app/js/modules/utilities.js"}],"/home/wang/projects/tks_project/tks/app/js/common/tks_filters.js":[function(require,module,exports){
'use strict';
/*global angular, dump, moment, io, console */

/* Filters */

angular.module('tks.filters', []).filter('reverse', function () {
  return function (input, uppercase) {
    var out = "";
    for (var i = 0; i < input.length; i++) {
      out = input.charAt(i) + out;
    }
    // conditional based on optional argument
    dump(input);
    if (uppercase) out = out.toUpperCase();

    dump(out);
    return out;
  };
}).filter('tm_classes', function () {
  return function (input) {
    if (typeof input === 'object') {
      var ret = '';
      for (var i = 0; i < input.length; i++) {
        if (ret === '') {
          if (input[i].details) ret = input[i].name + ' (' + input[i].details + ')';else ret = input[i].name;
        } else {
          if (input[i].details) ret = ret + '-' + input[i].name + ' (' + input[i].details + ')';else ret = ret + '-' + input[i].name;
        }
      }
      return ret;
    } else return 'n/a';
  };
})

// show a classes in a line without text
.filter('tm_classesSimple', function () {
  return function (input) {
    if (typeof input === 'object') {
      var ret = '';
      for (var i = 0; i < input.length; i++) {
        if (ret === '') {
          ret = input[i].name;
        } else {
          ret = ret + '-' + input[i].name;
        }
      }
      return ret;
    } else return 'n/a';
  };
}).filter('bool2text', function () {
  return function (bool) {
    if (bool) return 'Yes';else return 'No';
  };
}).filter('regtype2text', function () {
  return function (t) {
    if (t === 'w') return 'Wordmark';else if (t === 'l') return 'Logo';else if (t === 'wl') return 'Wordmark and logo';else if (t === 'tm') return 'Trademark';else if (t === 'tml') return 'Trademark with logo';else return 'Unknown';
  };
}).filter('poa2text', ['POA_INFO', function (POA_INFO) {
  return function (p, country) {
    if (p === -1) return 'This application does not require a Power of Attorney.';else if (p === 2) return 'A power of attorney is required before registration can start.';else {
      var label = POA_INFO[country];
      if (label !== undefined) {
        return label[p]; //TODO handle invalid P
      } else {
          return 'No POA label found for ' + p;
        }
    }
  };
}]).filter('country2text', ['countries', function (countries) {
  return function (p) {
    for (var i = 0; i < countries.length; i++) {
      if (countries[i].code === p) return countries[i].name;
    }
  };
}]).filter('orderStatus', function () {
  return function (p) {

    if (p === 1) return 'PENDING';else if (p === 2) return 'Completed';else if (p === 3) return 'Cancelled';else return 'Unknown';
  };
}).filter('ticketStatus', function () {
  return function (p) {

    if (p === 0) return 'ACTIVE';else if (p === 1) return 'CLOSED';else if (p === 2) return 'Cancelled';else return 'Unknown';
  };
}).filter('code2flagsrc', function () {
  return function (code) {
    if (code !== undefined) {

      console.log(code, typeof code);
      return 'images/flags/' + code.toLowerCase() + '.png';
    } /*else
      return 'images/'*/
  };
}).filter('commerce_use', ['$filter', function ($filter) {
  return function (code, use_date) {
    if (code !== undefined) {
      if (code === -1) return 'not applicable';else if (code === 0) return 'No';else if (code === 1) return 'Yes ( starting date : ' + $filter('date')(use_date, 'mediumDate') + ')';else if (code === 2) return 'No, but registered abroad';else return 'unknow code : ' + code;
    } else return 'unknown';
  };
}]).filter('study_type2text', function () {
  return function (t) {
    if (t === "0") return 'Basic';else if (t === "1") return 'Extensive';else if (t === -1) return '';else return 'Unknown';
  };
}).filter('admin_type', function () {
  return function (t) {
    if (t !== undefined) {
      if (t === 'A') return 'Admin';else if (t === 'S') return 'Staff';
    }
  };
}).filter('authors2text', function () {
  return function (t) {
    if (t !== undefined) {
      var ret;
      for (var i = 0; i < t.length; i++) {
        if (ret) ret = ret + ',' + t[i].name;else ret = t[i].name;
      }
      return ret;
    }
  };
}).filter('amt_in_case', function () {
  return function (t) {
    if (t !== undefined) {
      return '$ ' + t;
    }
  };
});

},{}],"/home/wang/projects/tks_project/tks/app/js/common/tks_services.js":[function(require,module,exports){
"use strict";
/* global angular */
var services2 = angular.module('tks.services2', []).value('version', '0.1');

services2.constant('STATIC_INFO', {

  requirements: 'In order to apply for a Trademark in USA you need to provide <br/>' + '1. Information through our Trademark Registration Order Form, and you need to<br/> ' + '2. Demonstrate use of the Trademark. No Power of Attorney is needed for USA.<br/> More to come',
  prices: 'Prices to be provided later...',
  priority_help: 'Claiming priority is part of the Paris Convention treaty.' + 'It means that if you have filed a trademark within the last 6 months in any of the signatory countries (most countries of the world), ' + 'you can use the filing date of this first registration as the filing date in the other signatory countries.  This right may be useful ' + 'to the owner of the trademark, to protect his intellectual property, in cases of legal conflicts with other parties.',
  class_help: 'When registering a trademark you need to specify the products and services that will be associated with your trademark.' + ' The large majority of countries of the world have adopted the International Classification of Nice. This system groups all products and ' + 'services into 45 classes - 34 for products, 11 for services - permitting you to specify precise and clear classes covering your trademark. ' + 'The protection that is offered to a registered trademark covers only the classes specified at the time of registration, therefore, allowing' + ' two identical trademarks to coexist in distinct classes.',
  commerce_use_help: '“Used in commerce” means that the mark is used in the ordinary course of trade. For example, for goods it would be used when it is placed in any manner on the goods, their containers or their displays. For services it is used when the trademark is used or displayed in advertising or during the sales process.'
}).constant('POA_INFO', {

  'IN': {
    0: 'I will be sending it within 120 days; file trademark immediately.',
    1: 'No, do not file application until received the Power of Attorney.'
  },
  'AR': {
    0: 'I will be sending it within 40 days; file trademark immediately.',
    1: 'No, do not file application until received the Power of Attorney.'
  },
  'AZ': {
    0: 'I will be sending it within 30 days; file trademark immediately.',
    1: 'No, do not file application until received the Power of Attorney.'
  },
  'BO': {
    0: 'I will be sending it within 30 days; file trademark immediately.',
    1: 'No, do not file application until received the Power of Attorney.'
  },
  'CA': {
    0: 'I will be sending it within 45 days; file trademark immediately.',
    1: 'No, do not file application until received the Power of Attorney.'
  },
  'CO': {
    0: 'I will be sending it within 60 days; file trademark immediately.',
    1: 'No, do not file application until received the Power of Attorney.'
  },
  'MX': {
    0: 'I will be sending it within 30 days; file trademark immediately($151)',
    1: 'No, do not file application until received the Power of Attorney.'
  },
  'BR': {
    0: 'I will be sending it within 55 days; file trademark immediately($163)',
    1: 'No, do not file application until received the Power of Attorney.'
  },
  'NO': {
    0: 'I will be sending it within 90 days; file trademark immediately',
    1: 'No, do not file application until received the Power of Attorney.'
  },
  'RU': {
    0: 'I will be sending it within 60 days; file trademark immediately',
    1: 'No, do not file application until received the Power of Attorney.'
  },
  'VN': {
    0: 'I will be sending it within 30 days; file trademark immediately ($50)',
    1: 'No, do not file application until received the Power of Attorney.'
  },
  'TW': {
    0: 'I will be sending it within 90 days; file trademark immediately',
    1: 'No, do not file application until received the Power of Attorney.'
  },
  'CN': { 0: '', 1: '' },
  'HELP': {
    'IN': 'In INDIA it is possible to file immediately your trademark application, without Power of Attorney, provided that you will send it within 120 Days.'
  },
  'POA_MUST': 'Please note that in order to file your trademark application, we need that you send us a Power of Attorney, below you can find a Template'

});

},{}],"/home/wang/projects/tks_project/tks/app/js/modules/mod_authorize.js":[function(require,module,exports){
"use strict";
/* global angular*/
angular.module('mod.authorize', []).value('version', '0.1')

// contains logged user info, uses sessionStorage, can be changed to use localStorage if needed

.service('Session', ["$window", "$cookieStore", function ($window, $cookieStore) {
  this.create = function (sessionId, userID, name, locale) {

    $cookieStore.put('sessionID', sessionId);
    $cookieStore.put('name', name);
    $cookieStore.put('userID', userID);
    $cookieStore.put('locale', locale);
  };

  this.destroy = function () {

    $cookieStore.remove('sessionID');
    $cookieStore.remove('name');
    $cookieStore.remove('userID');
    $cookieStore.remove('locale');
  };

  this.getSessionID = function () {
    return $cookieStore.get('sessionID');
  };

  this.getName = function () {
    return $cookieStore.get('name');
  };

  this.getUserID = function () {
    return $cookieStore.get('userID');
  };

  return this;
}])

// authorize services

.factory('authService', ['$http', '$q', '$timeout', 'Session', function ($http, $q, $timeout, Session) {

  var fasad = {};

  var authenticate = function authenticate(user) {
    var deferred = $q.defer();

    $http.post('/signin/', user).success(function (data, status) {
      console.log(data);
      console.log(status);
      if (data.sessionID !== null) Session.create(data.sessionID, data.userID, data.name, data.locale);
      deferred.resolve({ authenticated: data.sessionID !== null, message: data.msg, name: data.name });
    }).error(function (error) {
      deferred.resolve({ authenticated: false, message: 'Error occurred while authenticating.' });
    });

    return deferred.promise;
  };

  var isAuthenticated = function isAuthenticated() {
    var sid = Session.getSessionID();
    if (sid === undefined || sid === null) return false;else return true;
  };

  var register = function register(user) {
    var deferred = $q.defer();

    $http.post('/signup', user).success(function (data, status) {
      deferred.resolve({ registered: data.registered, message: data.message });
    }).error(function (error) {
      deferred.resolve({ registered: false, message: 'Sorry, error occurred while signing up, please try again.' });
    });

    return deferred.promise;
  };

  var logOut = function logOut() {
    Session.destroy();
  };

  var request_password_reset = function request_password_reset(email) {

    var deferred = $q.defer();

    $http.get('/api2/password_reset?email=' + email).success(function (data) {
      console.log(data);
      deferred.resolve({ status: data.status === 'ok', message: data.message });
    }).error(function (error) {
      deferred.resolve({ status: 'err', message: 'Sorry, error occurred while accessing server, please try again.' });
    });

    return deferred.promise;
  };

  fasad.signIn = function (user) {
    return authenticate(user);
  };
  fasad.signUp = function (user) {
    return register(user);
  };
  fasad.logOut = function () {
    return logOut();
  };
  fasad.isSignedIn = function () {
    return isAuthenticated();
  };
  fasad.userName = function () {
    return Session.getName();
  };

  fasad.request_password_reset = function (email) {
    return request_password_reset(email);
  };

  return fasad;
}]).constant('AUTH_EVENTS', {
  loginSuccess: 'auth-login-success',
  loginFailed: 'auth-login-failed',
  logoutSuccess: 'auth-logout-success',
  sessionTimeout: 'auth-session-timeout',
  notAuthenticated: 'auth-not-authenticated',
  notAuthorized: 'auth-not-authorized',
  cartUpdated: 'cart-updated'
})

// a simple local db store, stores key/value in sessionStorage, ideal for keepping some data like URL in the store,
// and use the key to pass to url

.service('DBStore', ['$window', function ($window) {

  this.create = function () {};

  this.destroy = function () {
    delete $window.sessionStorage.dbStore;
  };

  this.get = function (key) {
    var qkey = 'DB_' + key;
    var ret = $window.sessionStorage[qkey];
    delete $window.sessionStorage[qkey];
    return ret;
  };

  function generateQuickGuid() {
    return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
  }

  this.add = function (obj) {
    var key = generateQuickGuid();
    $window.sessionStorage['DB_' + key] = obj;
    return key;
  };

  return this;
}]);

},{}],"/home/wang/projects/tks_project/tks/app/js/modules/mod_cards.js":[function(require,module,exports){
"use strict";
/* global angular*/

angular.module('mod.cards', []).filter('range', function () {
  var filter = function filter(arr, lower, upper) {
    for (var i = lower; i <= upper; i++) arr.push(i);
    return arr;
  };
  return filter;
}).service('Cards', ['$http', '$q', function ($http, $q) {

  this.get_shopping_cart_dpm = function (cart_id) {
    var deferred = $q.defer();

    $http.get('/api_payment/get_shopping_cart_dpm/' + cart_id).success(function (data) {
      console.log('back from cart_dpm');
      console.log(data);
      deferred.resolve(data);
    }).error(function (error) {
      deferred.resolve(error);
    });
    return deferred.promise;
  };

  // this returns an blank card info, some suggested address

  this.get_user_info = function (cart_id) {
    var deferred = $q.defer();

    $http.get('/api_payment/get_user_info/' + cart_id).success(function (data) {
      console.log('back from cart_dpm');
      console.log(data);
      deferred.resolve(data);
    }).error(function (error) {
      deferred.resolve(error);
    });

    /*
          var info  = {
            "x_card_num": "6011000000000012",
            x_exp_date: "04/17",
            x_card_code: "782",
            x_first_name: "John",
            x_last_name: "Doe",
            x_address: "123 Main Street",
            x_city: "Boston",
            x_state: "MA",
            x_zip: "02142",
            x_country: "US"
          };
          */
    return deferred.promise;
  };

  return this;
}]);

},{}],"/home/wang/projects/tks_project/tks/app/js/modules/mod_countries.js":[function(require,module,exports){
"use strict";
/* globals angular */

var countries = require('countries');

angular.module('mod.countries', []).factory("countries", function () {
    console.log('123');
    var i = 1000;
    if (i === 1000) console.log('i am here and there');
    return countries.get_countries();
});

},{"countries":"/home/wang/projects/tks_project/tks/node_modules/countries/countries.js"}],"/home/wang/projects/tks_project/tks/app/js/modules/mod_pager.js":[function(require,module,exports){
"use strict";
/* global angular*/

angular.module('mod.pager', []).directive('pager2', ['$location', function ($location) {
  return {
    restrict: 'E',
    scope: {
      current: '=',
      pageCount: '=',
      showMorePages: '@'
    },
    templateUrl: '/partials/pager.html',
    link: function link(scope, iterStartElement, attrs) {

      scope.pagerPage = 0;
      scope.pagerSize = 10;

      scope.pager_count = function () {
        if (scope.pageCount !== undefined) return Math.ceil(scope.pageCount / scope.pagerSize);else return 0;
      };

      scope.first_page = function () {
        var ret = [],
            i,
            n;
        if (scope.pageCount != undefined) {
          // pageCount will be assigned
          n = 10;
          if (scope.pageCount < n) n = scope.pageCount;
          for (i = 0; i < n; i++) ret.push(i + 1);
        }
        return ret;
      };

      scope.pages = function () {
        var ret = [],
            i;
        for (i = 0; i < scope.pageCount; i++) ret.push(i + 1);
        return ret;
      };

      // try to see if page number has been set by caller, and adjust the current pager no
      scope.re_adjust_current_page = function () {

        if (scope.pageCount !== undefined) {
          if (scope.pagerSize > 0) {
            var n = Math.floor(scope.current / scope.pagerSize);
            if (n !== scope.pagerPage) scope.pagerPage = n;
          }
        }
      };

      scope.current_pager_page = function () {
        if (scope.pageCount !== undefined) {
          var ret = [],
              i;
          scope.re_adjust_current_page();
          var offset = scope.pagerPage * scope.pagerSize;
          for (i = offset; i < offset + scope.pagerSize; i++) {
            console.log('bb', i < scope.pageCount);
            if (i < scope.pageCount) ret.push(i + 1);
          }
          return ret;
        }
      };

      scope.all_pages = function () {
        var ret = [],
            i;
        for (i = 0; i < scope.pageCount; i++) ret.push(i + 1);
        return ret;
      };

      scope.set_page_number = function (n) {
        scope.current = n;
        if (scope.$parent.setDataSource !== undefined) {
          scope.$parent.setDataSource(scope.current);
          $location.search('page_no', n + 1);
        }
      };

      scope.nextPage = function () {

        console.log('sss', scope.pager_count());
        var n = scope.pagerPage + 1;

        if (n < scope.pageCount / scope.pagerSize) scope.pagerPage = n;

        console.log(scope.current, scope.pagerPage);
        console.log('next page');

        this.set_page_number(scope.pagerPage * scope.pagerSize);
      };

      scope.prevPage = function () {
        if (scope.pagerPage > 0) scope.pagerPage = scope.pagerPage - 1;
        console.log('prev page');
        this.set_page_number(scope.pagerPage * scope.pagerSize);
      };

      scope.myclass = function (n) {
        if (n === scope.current) return 'active';else return '';
      };

      scope.show = function () {
        return scope.pageCount > 1;
      };

      scope.showMore = function () {
        return scope.showMorePages;
      };

      scope.set_show_more = function () {
        scope.showMorePages = !scope.showMorePages;
      };
    }
  };
}]);

},{}],"/home/wang/projects/tks_project/tks/app/js/modules/modules.js":[function(require,module,exports){
'use strict';

module.exports = [require('../common/tks_services.js'), require('../common/tks_filters.js'), require('../common/tks_directives.js'), require('../common/tks_directives2.js'), require('../common/profile_directive.js'), require('./mod_countries.js'), require('./mod_authorize.js'), require('./mod_pager.js'), require('./mod_cards.js')];

},{"../common/profile_directive.js":"/home/wang/projects/tks_project/tks/app/js/common/profile_directive.js","../common/tks_directives.js":"/home/wang/projects/tks_project/tks/app/js/common/tks_directives.js","../common/tks_directives2.js":"/home/wang/projects/tks_project/tks/app/js/common/tks_directives2.js","../common/tks_filters.js":"/home/wang/projects/tks_project/tks/app/js/common/tks_filters.js","../common/tks_services.js":"/home/wang/projects/tks_project/tks/app/js/common/tks_services.js","./mod_authorize.js":"/home/wang/projects/tks_project/tks/app/js/modules/mod_authorize.js","./mod_cards.js":"/home/wang/projects/tks_project/tks/app/js/modules/mod_cards.js","./mod_countries.js":"/home/wang/projects/tks_project/tks/app/js/modules/mod_countries.js","./mod_pager.js":"/home/wang/projects/tks_project/tks/app/js/modules/mod_pager.js"}],"/home/wang/projects/tks_project/tks/app/js/modules/utilities.js":[function(require,module,exports){
"use strict";
var countries = require('countries');
var input_formats = require("../../../data/formats.json");
var oapi = require("../../../data/oapi.json");
var aripo = require("../../../data/aripo.json");

function getIEVersion(agent) {
  var reg = /MSIE\s?(\d+)(?:\.(\d+))?/i;
  var matches = agent.match(reg);
  if (matches !== null) {
    return { major: matches[1], minor: matches[2] };
  }
  return { major: "-1", minor: "-1" };
}

function supportedCountriesProc() {

  var ret = {};
  var cnt = 0;
  for (var key in input_formats) {
    if (input_formats.hasOwnProperty(key)) {

      if (countries.get_country(key) !== undefined) {
        ret[key] = { name: countries.get_country(key).name, price: cnt };
        cnt = cnt + 10;
      } else {
        console.log('can not find in country ', key);
      }
    }
  }

  for (key in oapi) {
    if (oapi.hasOwnProperty(key)) {
      if (countries.get_country(key) !== undefined) {
        ret[key] = { name: countries.get_country(key).name, price: 100, link: 'OAPI' };
      }
    }
  }

  // countries in aripo has option to either register as specific country or under ARIP treaty
  for (key in aripo) {
    if (aripo.hasOwnProperty(key)) {
      if (countries.get_country(key) !== undefined) {
        ret[key] = { name: countries.get_country(key).name, price: 100, others: 'ARIPO' };
      }
    }
  }

  return ret;
}

// framework can be : 'angular', 'bootstrap3'

exports.supportedBrowser = function (userAgent, framework) {

  var user_agent = userAgent.toLowerCase();
  // Chrome supported in all platforms

  //  console.log('user agent', user_agent, !!user_agent.match(/trident.*rv[ :]*11\./))

  if (/chrome/.test(user_agent)) return true;else if (/firefox/.test(user_agent)) return true;else if (/safari/.test(user_agent)) return true;else if (/mozilla/.test(user_agent)) return true;else if (/msie 10/.test(user_agent)) return true;else if (!!user_agent.match(/trident.*rv[ :]*11\./)) return true;else return false;
};

exports.supportedCountries = function () {

  return supportedCountriesProc();
};

exports.get_countries_by_contnent = function (code) {

  var clst = countries.get_countries_by_contnent(code);
  var slst = supportedCountriesProc();

  return clst.filter(function (e) {
    return slst[e.code] !== undefined;
  });
};

},{"../../../data/aripo.json":"/home/wang/projects/tks_project/tks/data/aripo.json","../../../data/formats.json":"/home/wang/projects/tks_project/tks/data/formats.json","../../../data/oapi.json":"/home/wang/projects/tks_project/tks/data/oapi.json","countries":"/home/wang/projects/tks_project/tks/node_modules/countries/countries.js"}],"/home/wang/projects/tks_project/tks/app_ts/js/web/web_controllers_cart.js":[function(require,module,exports){
"use strict";

exports.cart = function ($scope, $http, $rootScope, $location, Cart, AUTH_EVENTS, $modal) {

  //  $scope.cart = Cart.getCart()
  //    $scope.name = Session.getName()

  /*
  $scope.alert = function (m) {
    alert(m)
  }
  */

  $scope.isCartEmpty = function (cart) {
    if (!cart) return true;else {
      return cart.items.length === 0;
    }
  };

  $scope.get_remote_cart = function () {

    Cart.getRemoteCart().then(function (data) {
      $scope.cart = data.cart;
      $rootScope.$broadcast(AUTH_EVENTS.cartUpdated);
    });
  };

  $scope.get_remote_cart();

  $scope.delete_item = function (cart_id, rec) {

    if (confirm('Do you really want to delete this item?')) {

      Cart.delete_item(cart_id, rec).then(function (data) {
        $scope.get_remote_cart();
      });
    }
  };

  $scope.upload_local_cart = function (cart_id) {
    console.log('cart_id', cart_id);

    if (confirm('Do you really want to upload this local cart?')) {

      Cart.store_to_server().then(function (status) {
        console.log('store to server status2', status);
        /*
        Cart.clean_local_store()
         register_trademark.init_item()
        $rootScope.$broadcast(AUTH_EVENTS.cartUpdated);
         $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
        Cart.getRemoteCartCount().then(function (count) {
          if (count > 0)
            $location.path('/shopping_cart')
          else
            $location.path('/dashboard')
        })
        */
      });
      /*
      Cart.delete_item(cart_id, rec).then(function (data) {
        $scope.get_remote_cart()
      })*/
    }
  };

  $scope.selection_change = function (cart_id, rec) {
    Cart.selection_change(cart_id, rec).then(function (status) {
      $scope.get_remote_cart();
    });
  };

  $scope.show_details = function (rec) {
    console.log('show_details', rec, $modal);
    console.log('show_$modal', $modal);
    var modalInstance = $modal.open({
      templateUrl: 'partials/shopping_cart_details.html',
      windowClass: 'app-modal-window',
      controller: 'ModalInstanceCtrl2',
      resolve: {
        details: function details() {
          return rec;
        }
      }
    });
  };

  // happens
  $scope.local_store_to_server = function () {
    console.log('local store to server');
    Cart.store_to_server().then(function (status) {
      console.log('store to server status', status);
      Cart.clean_local_store();
    });
  };
};

},{}],"/home/wang/projects/tks_project/tks/data/aripo.json":[function(require,module,exports){
module.exports=module.exports=module.exports={
  "BW":"Botswana",
  "LS":"Lesotho",
  "LR":"Liberia",
  "MW":"Malawi",
  "NA":"Namibia",
  "SZ":"Swaziland",
  "TZ":"Tanzania",
  "UG":"Uganda",
  "ZW":"Zimbabwe"
}
},{}],"/home/wang/projects/tks_project/tks/data/formats.json":[function(require,module,exports){
module.exports=module.exports=module.exports={
  "US": {"input_type": 0, "POA_required": -1, "trade_type": "w", "word_label": "Word mark",  "commerce_use": 0,  "study_type": -1, "study_type2": "0"},
  "IN": {"input_type": 0, "POA_required":-1, "trade_type": "w", "word_label": "Word mark",  "commerce_use": 0, "study_type": -1},
  "SG": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark", "commerce_use": -1, "study_type": -1},
  "GR": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark", "commerce_use": -1, "study_type": -1},
  "LT": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark", "commerce_use": -1, "study_type": -1},
  "HK": {"input_type": 0, "POA_required": -1, "trade_type": "w", "word_label": "Word mark",  "commerce_use": -1, "study_type": -1},
  "JP": {"input_type": 0, "POA_required": -1, "trade_type": "w", "word_label": "Word mark",  "commerce_use": 0, "study_type": -1},
  "TW": {"input_type": 0, "POA_required": 1, "trade_type": "w", "word_label": "Word mark",  "commerce_use": 0, "study_type": -1},
  "AR": {"input_type": 1, "POA_required": 1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "AZ": {"input_type": 1, "POA_required": 1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "AU": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark", "commerce_use": -1, "study_type": -1},
  "NZ": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "GB": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "DE": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "IE": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "AS": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "FJ": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "PG": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "WS": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "TO": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "BH": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "BY": {"input_type": 1, "POA_required": 2, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "BZ": {"input_type": 1, "POA_required": 2, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "BO": {"input_type": 1, "POA_required": 1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "BE": {"input_type": 1, "POA_required": 1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "CA": {"input_type": 0, "POA_required": 1, "trade_type": "w", "word_label": "Word mark",  "commerce_use": 0, "study_type": -1},
  "CO": {"input_type": 1, "POA_required": 1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "EU": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "CH": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "SZ": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark", "commerce_use": -1, "study_type": -1},
  "NO": {"input_type": 1, "POA_required": 1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "MX": {"input_type": 0, "POA_required": 1, "trade_type": "w", "word_label": "Workmark",  "commerce_use": -1, "study_type": -1},
  "BR": {"input_type": 1, "POA_required": 1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "CN": {"input_type": 0, "POA_required": -1, "trade_type": "w", "word_label": "Word mark",  "commerce_use": -1, "study_type": -1},
  "RU": {"input_type": 1, "POA_required": 1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "KR": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark", "commerce_use": -1, "study_type": -1},
  "TH": {"input_type": 1, "POA_required": 1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "PH": {"input_type": 1, "POA_required": 1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "VN": {"input_type": 1, "POA_required": 1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "AF": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "AM": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "BD": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "BT": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "BN": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "KH": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "TL": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "GE": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "ID": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "IQ": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "IL": {"input_type": 0, "POA_required": 1, "trade_type": "w", "word_label": "Word mark",  "commerce_use": 0, "study_type": -1, "default":"w"},
  "JO": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "KW": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "KG": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "LA": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "LB": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "MO": {"input_type": 0, "POA_required": 1, "trade_type": "w", "word_label": "Word mark",  "commerce_use": 0, "study_type": -1, "default":"w"},
  "MY": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "MV": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "MN": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "MM": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "NP": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "OM": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "PK": {"input_type": 0, "POA_required": 1, "trade_type": "w", "word_label": "Word mark",  "commerce_use": 0, "study_type": -1, "default":"w"},
  "PS": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "QA": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "SA": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "LK": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "TJ": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "TR": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "TM": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "AE": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "UZ": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "YE": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "TG": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "AL": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "AD": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "AT": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "BA": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "BG": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "HR": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "CY": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "CZ": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "DK": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "EE": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "FI": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "FR": {"input_type": 0, "POA_required": 1, "trade_type": "w", "word_label": "Word mark",  "commerce_use": 0, "study_type": -1, "default":"w"},
  "GI": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "GG": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "HU": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "IS": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "IT": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "LV": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "LI": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "MK": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "MT": {"input_type": 0, "POA_required": 1, "trade_type": "w", "word_label": "Word mark",  "commerce_use": 0, "study_type": -1, "default":"w"},
  "MD": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "MC": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "PL": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "PT": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "RO": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "SM": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "SK": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "SI": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "ES": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "SE": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "UA": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "AI": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "AG": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "AW": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "BB": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "BM": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "VG": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "KY": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "CL": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "CR": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "CW": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "DM": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "DO": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "EC": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "SV": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "GD": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "GT": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "GY": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "HT": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "HN": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "JM": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "MS": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "NI": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "PA": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "PY": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "PE": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "PR": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "VC": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "KN": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "LC": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "SR": {"input_type": 0, "POA_required": 1, "trade_type": "w", "word_label": "Word mark",  "commerce_use": 0, "study_type": -1, "default":"w"},
  "TT": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "UY": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "VE": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "DZ": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "AO": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "BI": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "CV": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "CG": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "DJ": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "EG": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "ER": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "ET": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "GM": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "GH": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "KE": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "LR": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "LY": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "MG": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "MW": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "MU": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "MA": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "MZ": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "NA": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "RW": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "ST": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "SC": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "SL": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "ZA": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "TZ": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "TN": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "UG": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "ZM": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "ZW": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "RS": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "ME": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "BS": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "KO": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "TC": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "SB": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},  
  "LS": {"input_type": 0, "POA_required": -1, "trade_type": "w", "word_label": "Word mark",  "commerce_use": -1,  "study_type": -1, "study_type2": "0"},
  "JE": {"input_type": 0, "POA_required": -1, "trade_type": "w", "word_label": "Word mark",  "commerce_use": -1,  "study_type": -1, "study_type2": "0"},
  "BX": {"input_type": 0, "POA_required": -1, "trade_type": "w", "word_label": "Word mark",  "commerce_use": -1,  "study_type": -1, "study_type2": "0"},
  "SX": {"input_type": 0, "POA_required": -1, "trade_type": "w", "word_label": "Word mark",  "commerce_use": -1,  "study_type": -1, "study_type2": "0"},
  "NE": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "CI": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "NL": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "LU": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1},
  "OAPI": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "KRD": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "EAZ": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "VCT": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "MAF": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"},
  "ARIPO": {"input_type": 1, "POA_required": -1, "trade_type": "tm", "word_label": "Trademark",  "commerce_use": -1, "study_type": -1, "default":"tm"}
}
},{}],"/home/wang/projects/tks_project/tks/data/oapi.json":[function(require,module,exports){
module.exports=module.exports=module.exports={
  "BJ":"Benin",
  "BF":"Burkina Faso",
  "CM":"Cameroon",
  "CF":"Central African",
  "TD":"Chad",
  "KM":"Comoros",
  "CD":"Congo Republic",
  "GQ":"Equatorial Guinea",
  "GA":"Gabon",
  "GN":"Guinea",
  "GW":"Guinea-Bissau",
  "ML":"Mali",
  "MR":"Mauritania",
  "NG":"Nigeria",
  "SN":"Senegal",
  "TO":"Tonga"
}
},{}],"/home/wang/projects/tks_project/tks/node_modules/countries/countries.json":[function(require,module,exports){
module.exports=module.exports=module.exports=[ 
  {"name":"Afghanistan","code":"AF","continent":"ME"},
  {"name":"Åland Islands","code":"AX","continent":"EU"},
  {"name":"Albania","code":"AL","continent":"EU"},
  {"name":"Algeria","code":"DZ","continent":"AF"},
  {"name":"American Samoa","code":"AS","continent":"OC"},
  {"name":"Andorra","code":"AD","continent":"EU"},
  {"name":"Angola","code":"AO","continent":"AF"},
  {"name":"Anguilla","code":"AI","continent":"AF"},
  {"name":"Antarctica","code":"AQ","continent":"AN"},
  {"name":"Antigua and Barbuda","code":"AG","continent":"CA"},
  {"name":"Argentina","code":"AR","continent":"SA"},
  {"name":"Armenia","code":"AM","continent":"EU"},
  {"name":"Aruba","code":"AW","continent":"CA"},
  {"name":"Australia","code":"AU","continent":"OC"},
  {"name":"Austria","code":"AT","continent":"EU"},
  {"name":"Azerbaijan","code":"AZ","continent":"EU"},
  {"name":"Bahamas","code":"BS","continent":"CA"},
  {"name":"Bahrain","code":"BH","continent":"ME"},
  {"name":"Bangladesh","code":"BD","continent":"AS"},
  {"name":"Barbados","code":"BB","continent":"CA"},
  {"name":"Belarus","code":"BY","continent":"EU"},
  {"name":"Belgium","code":"BE","continent":"EU"},
  {"name":"Benelux","code":"BX","continent":"EU"},
  {"name":"Belize","code":"BZ","continent":"CA"},
  {"name":"Benin","code":"BJ","continent":"AF"},
  {"name":"Bermuda","code":"BM","continent":"AS"},
  {"name":"Bhutan","code":"BT","continent":"AS"},
  {"name":"Bolivia","code":"BO","continent":"SA"},
  {"name":"Bosnia","code":"BA","continent":"EU"},
  {"name":"Botswana","code":"BW","continent":"AF"},
  {"name":"Bouvet Island","code":"BV","continent":"AN"},
  {"name":"Brazil","code":"BR","continent":"SA"},
  {"name":"British Indian","code":"IO","continent":"AS"},
  {"name":"Brunei Darussalam","code":"BN","continent":"AS"},
  {"name":"Bulgaria","code":"BG","continent":"EU"},
  {"name":"Burkina Faso","code":"BF","continent":"AF"},
  {"name":"Burundi","code":"BI","continent":"AF"},
  {"name":"Cambodia","code":"KH","continent":"AS"},
  {"name":"Cameroon","code":"CM","continent":"AF"},
  {"name":"Canada","code":"CA","continent":"NA"},
  {"name":"Cape Verde","code":"CV","continent":"AF"},
  {"name":"Cayman Islands","code":"KY","continent":"CA"},
  {"name":"Central African Republic","code":"CF","continent":"AF"},
  {"name":"Chad","code":"TD","continent":"AF"},
  {"name":"Chile","code":"CL","continent":"SA"},
  {"name":"China","code":"CN","continent":"AS"},
  {"name":"Christmas Island","code":"CX","continent":"AS"},
  {"name":"Cocos (Keeling)","code":"CC","continent":"AS"},
  {"name":"Colombia","code":"CO","continent":"SA"},
  {"name":"Comoros","code":"KM","continent":"AF"},
  {"name":"Republic of the Congo","code":"CG","continent":"AF"},
  {"name":"Democratic Republic of the Congo","code":"CD","continent":"AF"},
  {"name":"Cook Islands","code":"CK","continent":"OC"},
  {"name":"Costa Rica","code":"CR","continent":"CA"},
  {"name": "Ivory Coast", "code": "CI","continent":"AF"},
  {"name":"Croatia","code":"HR","continent":"EU"},
  {"name":"Cuba","code":"CU","continent":"CA"},
  {"name":"Cyprus","code":"CY","continent":"EU"},
  {"name":"Czech Republic","code":"CZ","continent":"EU"},
  {"name":"Denmark","code":"DK","continent":"EU"},
  {"name":"Djibouti","code":"DJ","continent":"AF"},
  {"name":"Dominica","code":"DM","continent":"CA"},
  {"name":"Dominican Republic","code":"DO","continent":"CA"},
  {"name":"Ecuador","code":"EC","continent":"SA"},
  {"name":"Egypt","code":"EG","continent":"AF"},
  {"name":"El Salvador","code":"SV","continent":"CA"},
  {"name":"Equatorial Guinea","code":"GQ","continent":"AF"},
  {"name":"Eritrea","code":"ER","continent":"AF"},
  {"name":"Estonia","code":"EE","continent":"EU"},
  {"name":"Ethiopia","code":"ET","continent":"AF"},
  {"name":"European Union","code":"EU","continent":"EU"},
  {"name":"Falkland Islands","code":"FK","continent":"SA"},
  {"name":"Faroe Islands","code":"FO","continent":"EU"},
  {"name":"Fiji","code":"FJ","continent":"OC"},
  {"name":"Finland","code":"FI","continent":"EU"},
  {"name":"France","code":"FR","continent":"EU"},
  {"name":"French Guiana","code":"GF","continent":"AF"},
  {"name":"French Polynesia","code":"PF","continent":"AF"},
  {"name":"French Southern","code":"TF","continent":"AN"},
  {"name":"Gabon","code":"GA","continent":"AF"},
  {"name":"Gambia","code":"GM","continent":"AF"},
  {"name":"Georgia","code":"GE","continent":"EU"},
  {"name":"Germany","code":"DE","continent":"EU"},
  {"name":"Ghana","code":"GH","continent":"AF"},
  {"name":"Gibraltar","code":"GI","continent":"EU"},
  {"name":"Greece","code":"GR","continent":"EU"},
  {"name":"Greenland","code":"GL","continent":"CA"},
  {"name":"Grenada","code":"GD","continent":"CA"},
  {"name":"Guadeloupe","code":"GP","continent":"CA"},
  {"name":"Guam","code":"GU","continent":"OC"},
  {"name":"Guatemala","code":"GT","continent":"CA"},
  {"name":"Guernsey","code":"GG","continent":"EU"},
  {"name":"Guinea","code":"GN","continent":"AF"},
  {"name":"Guinea-Bissau","code":"GW","continent":"AF"},
  {"name":"Guyana","code":"GY","continent":"SA"},
  {"name":"Haiti","code":"HT","continent":"CA"},
  {"name":"Heard and Mcdonald","code":"HM","continent":"AN"},
  {"name":"Holy See","code":"VA","continent":"EU"},
  {"name":"Honduras","code":"HN","continent":"CA"},
  {"name":"Hong Kong","code":"HK","continent":"AS"},
  {"name":"Hungary","code":"HU","continent":"EU"},
  {"name":"Iceland","code":"IS","continent":"EU"},
  {"name":"India","code":"IN","continent":"AS"},
  {"name":"Indonesia","code":"ID","continent":"AS"},
  {"name":"Iran","code":"IR","continent":"AS"},
  {"name":"Iraq","code":"IQ","continent":"ME"},
  {"name":"Ireland","code":"IE","continent":"EU"},
  {"name":"Isle of Man","code":"IM","continent":"EU"},
  {"name":"Israel","code":"IL","continent":"ME"},
  {"name":"Italy","code":"IT","continent":"EU"},
  {"name":"Jamaica","code":"JM","continent":"CA"},
  {"name":"Japan","code":"JP","continent":"AS"},
  {"name":"Jersey","code":"JE","continent":"EU"},
  {"name":"Jordan","code":"JO","continent":"ME"},
  {"name":"Kazakhstan","code":"KZ","continent":"AS"},
  {"name":"Kenya","code":"KE","continent":"AF"},
  {"name":"Kiribati","code":"KI","continent":"OC"},
  {"name":"North Korea","code":"KP","continent":"AS"},
  {"name":"South Korea","code":"KR","continent":"AS"},
  {"name":"Kosovo","code":"KO","continent":"EU"},
  {"name":"Kuwait","code":"KW","continent":"ME"},
  {"name":"Kyrgyzstan","code":"KG","continent":"AS"},
  {"name":"Laos","code":"LA","continent":"AS"},
  {"name":"Latvia","code":"LV","continent":"EU"},
  {"name":"Lebanon","code":"LB","continent":"ME"},
  {"name":"Lesotho","code":"LS","continent":"AF"},
  {"name":"Liberia","code":"LR","continent":"AF"},
  {"name":"Libyan","code":"LY","continent":"AF"},
  {"name":"Liechtenstein","code":"LI","continent":"EU"},
  {"name":"Lithuania","code":"LT","continent":"EU"},
  {"name":"Luxembourg","code":"LU","continent":"EU"},
  {"name":"Macao","code":"MO","continent":"AS"},
  {"name":"Macedonia","code":"MK","continent":"EU"},
  {"name":"Madagascar","code":"MG","continent":"AF"},
  {"name":"Malawi","code":"MW","continent":"AF"},
  {"name":"Malaysia","code":"MY","continent":"AS"},
  {"name":"Maldives","code":"MV","continent":"AS"},
  {"name":"Mali","code":"ML","continent":"AF"},
  {"name":"Malta","code":"MT","continent":"EU"},
  {"name":"Marshall Islands","code":"MH","continent":"OC"},
  {"name":"Martinique","code":"MQ","continent":"CA"},
  {"name":"Mauritania","code":"MR","continent":"AF"},
  {"name":"Mauritius","code":"MU","continent":"AF"},
  {"name":"Mayotte","code":"YT","continent":"AF"},
  {"name":"Mexico","code":"MX","continent":"NA"},
  {"name":"Micronesia","code":"FM","continent":"OC"},
  {"name":"Moldova","code":"MD","continent":"EU"},
  {"name":"Monaco","code":"MC","continent":"EU"},
  {"name":"Mongolia","code":"MN","continent":"AS"},
  {"name":"Montserrat","code":"MS","continent":"CA"},
  {"name":"Morocco","code":"MA","continent":"AF"},
  {"name":"Mozambique","code":"MZ","continent":"AF"},
  {"name":"Myanmar","code":"MM","continent":"AS"},
  {"name":"Namibia","code":"NA","continent":"AF"},
  {"name":"Nauru","code":"NR","continent":"OC"},
  {"name":"Nepal","code":"NP","continent":"AS"},
  {"name":"Netherlands","code":"NL","continent":"EU"},
  {"name":"Netherlands Antilles","code":"AN","continent":"EU"},
  {"name":"New Caledonia","code":"NC","continent":"OC"},
  {"name":"New Zealand","code":"NZ","continent":"OC"},
  {"name":"Nicaragua","code":"NI","continent":"CA"},
  {"name":"Niger","code":"NE","continent":"AF"},
  {"name":"Nigeria","code":"NG","continent":"AF"},
  {"name":"Niue","code":"NU","continent":"OC"},
  {"name":"Norfolk Island","code":"NF","continent":"OC"},
  {"name":"Northern Mariana","code":"MP","continent":"OC"},
  {"name":"Norway","code":"NO","continent":"EU"},
  {"name":"Oman","code":"OM","continent":"ME"},
  {"name":"Pakistan","code":"PK","continent":"AS"},
  {"name":"Palau","code":"PW","continent":"OC"},
  {"name":"Palestine","code":"PS","continent":"ME"},
  {"name":"Panama","code":"PA","continent":"CA"},
  {"name":"Papua New Guinea","code":"PG","continent":"AF"},
  {"name":"Paraguay","code":"PY","continent":"SA"},
  {"name":"Peru","code":"PE","continent":"SA"},
  {"name":"Philippines","code":"PH","continent":"AS"},
  {"name":"Pitcairn","code":"PN","continent":"OC"},
  {"name":"Poland","code":"PL","continent":"EU"},
  {"name":"Portugal","code":"PT","continent":"EU"},
  {"name":"Puerto Rico","code":"PR","continent":"CA"},
  {"name":"Qatar","code":"QA","continent":"ME"},
  {"name":"Reunion","code":"RE","continent":"AF"},
  {"name":"Romania","code":"RO","continent":"EU"},
  {"name":"Russian Federation","code":"RU","continent":"EU"},
  {"name":"RWANDA","code":"RW","continent":"AF"},
  {"name":"Saint Helena","code":"SH","continent":"AF"},
  {"name":"Saint Kitts and Nevis","code":"KN","continent":"CA"},
  {"name":"Saint Lucia","code":"LC","continent":"CA"},
  {"name":"Saint Pierre and Miquelon","code":"PM","continent":"CA"},
  {"name":"Saint Vincent","code":"VC","continent":"CA"},
  {"name":"Samoa","code":"WS","continent":"OC"},
  {"name":"San Marino","code":"SM","continent":"EU"},
  {"name":"Sao Tome and Principe","code":"ST","continent":"AF"},
  {"name":"Saudi Arabia","code":"SA","continent":"ME"},
  {"name":"Senegal","code":"SN","continent":"AF"},
  {"name":"Serbia and Montenegro","code":"CS","continent":"EU"},
  {"name":"Serbia","code":"RS","continent":"EU"},
  {"name":"Montenegro","code":"ME","continent":"EU"},
  {"name":"Seychelles","code":"SC","continent":"AF"},
  {"name":"Sierra Leone","code":"SL","continent":"AF"},
  {"name":"Sint Maarten","code":"SX","continent":"EU"},
  {"name":"Singapore","code":"SG","continent":"AS"},
  {"name":"Slovakia","code":"SK","continent":"EU"},
  {"name":"Slovenia","code":"SI","continent":"EU"},
  {"name":"Solomon Islands","code":"SB","continent":"OC"},
  {"name":"Somalia","code":"SO","continent":"AF"},
  {"name":"South Africa","code":"ZA","continent":"AF"},
  {"name":"South Georgia","code":"GS","continent":"EU"},
  {"name":"Spain","code":"ES","continent":"EU"},
  {"name":"Sri Lanka","code":"LK","continent":"AS"},
  {"name":"Sudan","code":"SD","continent":"AF"},
  {"name":"Suri name","code":"SR","continent":"SA"},
  {"name":"Svalbard and Jan Mayen","code":"SJ","continent":"EU"},
  {"name":"Swaziland","code":"SZ","continent":"AF"},
  {"name":"Sweden","code":"SE","continent":"EU"},
  {"name":"Switzerland","code":"CH","continent":"EU"},
  {"name":"Syrian Arab Republic","code":"SY","continent":"AS"},
  {"name":"Taiwan","code":"TW","continent":"AS"},
  {"name":"Tajikistan","code":"TJ","continent":"AS"},
  {"name":"Tanzania","code":"TZ","continent":"AF"},
  {"name":"Thailand","code":"TH","continent":"AS"},
  {"name":"East Timor","code":"TL","continent":"AS"},
  {"name":"Togo","code":"TG","continent":"AF"},
  {"name":"Tokelau","code":"TK","continent":"OC"},
  {"name":"Tonga","code":"TO","continent":"OC"},
  {"name":"Trinidad and Tobago","code":"TT","continent":"CA"},
  {"name":"Tunisia","code":"TN","continent":"AF"},
  {"name":"Turkey","code":"TR","continent":"ME"},
  {"name":"Turkmenistan","code":"TM","continent":"AS"},
  {"name":"Turks and Caicos Islands","code":"TC","continent":"CA"},
  {"name":"Tuvalu","code":"TV","continent":"OC"},
  {"name":"Uganda","code":"UG","continent":"AF"},
  {"name":"Ukraine","code":"UA","continent":"EU"},
  {"name":"United Arab Emirates","code":"AE","continent":"ME"},
  {"name":"United Kingdom","code":"GB","continent":"EU"},
  {"name":"United States","code":"US","continent":"NA"},
  {"name":"Uruguay","code":"UY","continent":"SA"},
  {"name":"Uzbekistan","code":"UZ","continent":"AS"},
  {"name":"Vanuatu","code":"VU","continent":"OC"},
  {"name":"Venezuela","code":"VE","continent":"SA"},
  {"name":"Vietnam","code":"VN","continent":"AS"},
  {"name":"British Virgin Islands","code":"VG","continent":"CA"},
  {"name":"Virgin Islands, U.S.","code":"VI","continent":"CA"},
  {"name":"Wallis and Futuna","code":"WF","continent":"OC"},
  {"name":"Western Sahara","code":"EH","continent":"AF"},
  {"name":"Yemen","code":"YE","continent":"ME"},
  {"name":"Zambia","code":"ZM","continent":"AF"},
  {"name":"Zimbabwe","code":"ZW","continent":"AF"},
  {"name":"ARIPO Treaty","code":"ARIPO","continent":"AF"},
  {"name":"OAPI Treaty","code":"OAPI","continent":"AF"},
  {"name":"Kurdistan Region","code":"KRD","continent":"ME"},
  {"name":"Zanzibar","code":"EAZ","continent":"AF"},
  {"name":"Saint Martin","code":"MAF","continent":"EU"}, 
  {"name":"Saint Vincent and the Grenadines","code":"VCT","continent":"OC"}
]

},{}],"/home/wang/projects/tks_project/tks/node_modules/countries/countries.js":[function(require,module,exports){
var countries = require('./countries.json')
var states = require('./states.json')

//console.log(countries)
/*
 AF	Africa
 AN	Antarctica
 AS	Asia
 EU	Europe
 NA	North america
 OC	Oceania
 SA	South america
 */

function all_countries() {
  return countries
}

function get_country(code) {
  for (var i = 0; i < countries.length; i++) {
    if (countries[i].code === code) {
      return countries[i]
    }
  }
}

var country_alias = {
  "THE GAMBIA": "GAMBIA",
  "GUINEA BISSAU": "GUINEA-BISSAU",
  "SAINT VINCENT AND GRENADINES": "SAINT VINCENT",
  "PALESTINIAN TERRITORY": "PALESTINE"
}

function get_country_by_name(name) {
  var kname = name.replace(/-|_|%20/g, ' ').toUpperCase() // handle American-Samoa or Am

  console.log(country_alias[kname], kname)
  if (country_alias[kname])
    kname = country_alias[kname]


  for (var i = 0; i < countries.length; i++) {
    if (countries[i].name.toUpperCase() === kname)
      return countries[i]
  }
}

// get_country_by_regex(/^\/registration-in-(\w+)/i, '/registration-in-China')

function get_country_by_regex(regx, string) {

  var rslt = regx.exec(string)
  if (rslt === null)
    return undefined
  else {
    return get_country_by_name(rslt[1].replace(/_/g, ' '))
  }
}




// if param is undefined, all return
// if param is an array, only those in the list returned
function get_countries(param) {
  if (param === undefined)
    return countries
  else {
    var ret = []
    for (var i = 0; i < param.length; i++) {
      var country = get_country(param[i])
      if (country !== undefined)
        ret.push(country)
    }
    return ret
  }
}

function get_countries_by_contnent(code) {
  return countries.filter(function (value, index, ar) {
    return value.continent === code
  })
}


//console.log(get_countries(['US', 'CN','xx']))

exports.get_countries = function (aParam) {
  return get_countries(aParam)
}


exports.get_country = function (aParam) {
  return get_country(aParam)
}


exports.get_country_by_name = function (name) {
  return get_country_by_name(name)
}


exports.get_country_by_regex = function (regx, string) {
  return get_country_by_regex(regx, string)
}


exports.get_countries_by_contnent = function (code) {
  return get_countries_by_contnent(code)
}

// returns states of the country_code

exports.get_states = function (code) {
  return states[code]
}


exports.get_state_name = function (code) {
  var country = code.substring(0, 2)
  console.log('country', country)
  if (states[country])
    return states[country][code]
  else
    return undefined
}





},{"./countries.json":"/home/wang/projects/tks_project/tks/node_modules/countries/countries.json","./states.json":"/home/wang/projects/tks_project/tks/node_modules/countries/states.json"}],"/home/wang/projects/tks_project/tks/node_modules/countries/states.json":[function(require,module,exports){
module.exports=module.exports=module.exports={
  "US": {
    "US.AL": "Alabama",
    "US.AK": "Alaska",
    "US.AZ": "Arizona",
    "US.AR": "Arkansas",
    "US.CA": "California",
    "US.CO": "Colorado",
    "US.CT": "Connecticut",
    "US.DE": "Delaware",
    "US.DC": "District of Columbia",
    "US.FL": "Florida",
    "US.GA": "Georgia",
    "US.HI": "Hawaii",
    "US.ID": "Idaho",
    "US.IL": "Illinois",
    "US.IN": "Indiana",
    "US.IA": "Iowa",
    "US.KS": "Kansas",
    "US.KY": "Kentucky",
    "US.LA": "Louisiana",
    "US.ME": "Maine",
    "US.MD": "Maryland",
    "US.MA": "Massachusetts",
    "US.MI": "Michigan",
    "US.MN": "Minnesota",
    "US.MS": "Mississippi",
    "US.MO": "Missouri",
    "US.MT": "Montana",
    "US.NE": "Nebraska",
    "US.NV": "Nevada",
    "US.NH": "New Hampshire",
    "US.NJ": "New Jersey",
    "US.NM": "New Mexico",
    "US.NY": "New York",
    "US.NC": "North Carolina",
    "US.ND": "North Dakota",
    "US.OH": "Ohio",
    "US.OK": "Oklahoma",
    "US.OR": "Oregon",
    "US.PA": "Pennsylvania",
    "US.RI": "Rhode Island",
    "US.SC": "South Carolina",
    "US.SD": "South Dakota",
    "US.TN": "Tennessee",
    "US.TX": "Texas",
    "US.UT": "Utah",
    "US.VT": "Vermont",
    "US.VA": "Virginia",
    "US.WA": "Washington",
    "US.WV": "West Virginia",
    "US.WI": "Wisconsin",
    "US.WY": "Wyoming"
  },
  "CN" : {
    "CN.AH": "Anhui",
    "CN.BJ": "Beijing",
    "CN.CQ": "Chongqing",
    "CN.FJ": "Fujian",
    "CN.GS": "Gansu",
    "CN.GD": "Guandong",
    "CN.GX": "Guangxi",
    "CN.HA": "Hainan",
    "CN.HB": "Hebei",
    "CN.HL": "Heilongjian",
    "CN.HA": "Henan",
    "CN.HB": "Hubei",
    "CN.HN": "Hunan",
    "CN.JS": "Jiangsu",
    "CN.JX": "Jiangxi",
    "CN.JL": "Jilin",
    "CN.LN": "Liaoning",
    "CN.NM": "Nei Mongol",
    "CN.NX": "Ningxia",
    "CN.QH": "Qinghai",
    "CN.SA": "Shaanxi",
    "CN.SD": "Shandong",
    "CN.SH": "Shanghai",
    "CN.SX": "Shanxi",
    "CN.SC": "Sichuan",
    "CN.TJ": "Tianjin",
    "CN.XJ": "Xinjuan",
    "CN.XZ": "Xizang",
    "CN.YN": "Yunnan",
    "CN.ZJ": "Zhejian"
  },
  "HK" : {
    "HK.CW": "Central and Western",
    "HK.EA": "Eastern",
    "HK.IS": "Islands",
    "HK.KC": "Kowloon City",
    "HK.KI": "Kwai Tsing",
    "HK.KU": "Kwun Tong",
    "HK.NO": "North",
    "HK.SK": "Sai Kung",
    "HK.SS": "Sham Shui Po",
    "HK.ST": "Sha Tin",
    "HK.SO": "Southern",
    "HK.TP": "Tai Po",
    "HK.TW": "Tsuen Wan",
    "HK.TM": "Tuen Mun",
    "HK.WC": "Wan Chai",
    "HK.WT": "Wong Tai Sin",
    "HK.YT": "Yau Tsim Mong",
    "HK.YL": "Yuen Long"
  },
  "PH" : {
    "PH.AB": "Abra",
    "PH.AN": "Agusan del Norte",
    "PH.AS": "Agusan del Sur",
    "PH.AK": "Aklan",
    "PH.AL": "Albay",
    "PH.AQ": "Antique",
    "PH.AP": "Apayao",
    "PH.AU": "Aurora",
    "PH.BS": "Basilan",
    "PH.BA": "Bataan",
    "PH.BN": "Batanes",
    "PH.BT": "Batangas",
    "PH.BG": "Benguet",
    "PH.BI": "Biliran",
    "PH.BO": "Bohol",
    "PH.BK": "Bukidnon",
    "PH.BU": "Bulacan",
    "PH.CG": "Cagayan",
    "PH.CN": "Camarines Norte",
    "PH.CS": "Camarines Sur",
    "PH.CM": "Camiguin",
    "PH.CP": "Capiz",
    "PH.CT": "Catanduanes",
    "PH.CV": "Cavite",
    "PH.CB": "Cebu",
    "PH.CL": "Compostela Valley",
    "PH.NC": "Cotabato",
    "PH.DV": "Davao del Norte",
    "PH.DS": "Davao del Sur",
    "PH.DO": "Davao Oriental",
    "PH.DI": "Dinagat Islands",
    "PH.ES": "Eastern Samar",
    "PH.GU": "Guimaras",
    "PH.IF": "Ifugao",
    "PH.IN": "Ilocos Norte",
    "PH.IS": "Ilocos Sur",
    "PH.II": "Iloilo",
    "PH.IB": "Isabela",
    "PH.KA": "Kalinga",
    "PH.LG": "Laguna",
    "PH.LN": "Lanao del Norte",
    "PH.LS": "Lanao del Sur",
    "PH.LU": "La Union",
    "PH.LE": "Leyte",
    "PH.MG": "Maguindanao",
    "PH.MQ": "Marinduque",
    "PH.MB": "Masbate",
    "PH.MM": "Metropolitan Manila",
    "PH.MD": "Misamis Occidental",
    "PH.MN": "Misamis Oriental",
    "PH.MT": "Mountain",
    "PH.ND": "Negros Occidental",
    "PH.NR": "Negros Oriental",
    "PH.NS": "Northern Samar",
    "PH.NE": "Nueva Ecija",
    "PH.NV": "Nueva Vizcaya",
    "PH.MC": "Occidental Mindoro",
    "PH.MR": "Oriental Mindoro",
    "PH.PL": "Palawan",
    "PH.PM": "Pampanga",
    "PH.PN": "Pangasinan",
    "PH.QZ": "Quezon",
    "PH.QR": "Quirino",
    "PH.RI": "Rizal",
    "PH.RO": "Romblon",
    "PH.SM": "Samar",
    "PH.SG": "Sarangani",
    "PH.SQ": "Siquijor",
    "PH.SR": "Sorsogon",
    "PH.SC": "South Cotabato",
    "PH.SL": "Southern Leyte",
    "PH.SK": "Sultan Kudarat",
    "PH.SU": "Sulu",
    "PH.ST": "Surigao del Norte",
    "PH.SS": "Surigao del Sur",
    "PH.TR": "Tarlac",
    "PH.TT": "Tawi-Tawi",
    "PH.ZM": "Zambales",
    "PH.ZN": "Zamboanga del Norte",
    "PH.ZS": "Zamboanga del Sur",
    "PH.ZY": "Zamboanga-Sibugay"
  },
  "CA" : {
    "CA.AB": "Alberta",
    "CA.BC": "British Columbia",
    "CA.MB": "Manitoba",
    "CA.NB": "New Brunswick",
    "CA.NF": "Newfoundland and Labrador",
    "CA.NT": "Northwest Territories",
    "CA.NS": "Nova Scotia",
    "CA.NU": "Nunavut",
    "CA.ON": "Ontario",
    "CA.PE": "Prince Edward Island",
    "CA.QC": "Quebec",
    "CA.SK": "Saskatchewan",
    "CA.YT": "Yukon Territory"
  },
  "TH" : {
    "TH.AC": "Amnat Charoen",
    "TH.AT": "Ang Thong",
    "TH.BM": "Bangkok Metropolis",
    "TH.BR": "Buri Ram",
    "TH.CC": "Chachoengsao",
    "TH.CN": "Chai Nat",
    "TH.CY": "Chaiyaphum",
    "TH.CT": "Chanthaburi",
    "TH.CM": "Chiang Mai",
    "TH.CR": "Chiang Rai",
    "TH.CB": "Chon Buri",
    "TH.CP": "Chumphon",
    "TH.KL": "Kalasin",
    "TH.KP": "Kamphaeng Phet",
    "TH.KN": "Kanchanaburi",
    "TH.KK": "Khon Kaen",
    "TH.KR": "Krabi",
    "TH.LG": "Lampang",
    "TH.LN": "Lamphun",
    "TH.LE": "Loei",
    "TH.LB": "Lop Buri",
    "TH.MH": "Mae Hong Son",
    "TH.MS": "Maha Sarakham",
    "TH.MD": "Mukdahan",
    "TH.NN": "Nakhon Nayok",
    "TH.NP": "Nakhon Pathom",
    "TH.NF": "Nakhon Phanom",
    "TH.NR": "Nakhon Ratchasima",
    "TH.NS": "Nakhon Sawan",
    "TH.NT": "Nakhon Si Thammarat",
    "TH.NA": "Nan",
    "TH.NW": "Narathiwat",
    "TH.NB": "Nong Bua Lam Phu",
    "TH.NK": "Nong Khai",
    "TH.NO": "Nonthaburi",
    "TH.PT": "Pathum Thani",
    "TH.PI": "Pattani",
    "TH.PG": "Phangnga",
    "TH.PL": "Phatthalung",
    "TH.PY": "Phayao",
    "TH.PH": "Phetchabun",
    "TH.PE": "Phetchaburi",
    "TH.PC": "Phichit",
    "TH.PS": "Phitsanulok",
    "TH.PR": "Phrae",
    "TH.PA": "Phra Nakhon Si Ayutthaya",
    "TH.PU": "Phuket",
    "TH.PB": "Prachin Buri",
    "TH.PK": "Prachuap Khiri Khan",
    "TH.RN": "Ranong",
    "TH.RT": "Ratchaburi",
    "TH.RY": "Rayong",
    "TH.RE": "Roi Et",
    "TH.SK": "Sa Kaeo",
    "TH.SN": "Sakon Nakhon",
    "TH.SP": "Samut Prakan",
    "TH.SS": "Samut Sakhon",
    "TH.SM": "Samut Songkhram",
    "TH.SR": "Saraburi",
    "TH.SA": "Satun",
    "TH.SB": "Sing Buri",
    "TH.SI": "Si Sa Ket",
    "TH.SG": "Songkhla",
    "TH.SO": "Sukhothai",
    "TH.SH": "Suphan Buri",
    "TH.ST": "Surat Thani",
    "TH.SU": "Surin",
    "TH.TK": "Tak",
    "TH.TG": "Trang",
    "TH.TT": "Trat",
    "TH.UR": "Ubon Ratchathani",
    "TH.UN": "Udon Thani",
    "TH.UT": "Uthai Thani",
    "TH.UD": "Uttaradit",
    "TH.YL": "Yala",
    "TH.YS": "Yasothon"
  }
}

},{}]},{},["./app/js/admin/admin_app.js"])
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCIvaG9tZS93YW5nL3Byb2plY3RzL3Rrc19wcm9qZWN0L3Rrcy9hcHAvanMvYWRtaW4vYWRtaW5fYXBwLmpzIiwiL2hvbWUvd2FuZy9wcm9qZWN0cy90a3NfcHJvamVjdC90a3MvYXBwL2pzL2FkbWluL2FkbWluX2NvbnRyb2xsZXJzLmpzIiwiL2hvbWUvd2FuZy9wcm9qZWN0cy90a3NfcHJvamVjdC90a3MvYXBwL2pzL2FkbWluL2FkbWluX3JvdXRlcy5qcyIsIi9ob21lL3dhbmcvcHJvamVjdHMvdGtzX3Byb2plY3QvdGtzL2FwcC9qcy9hZG1pbi9hZG1pbl9zZXJ2aWNlcy5qcyIsIi9ob21lL3dhbmcvcHJvamVjdHMvdGtzX3Byb2plY3QvdGtzL2FwcC9qcy9hZG1pbi9jb250cm9sbGVycy9jYXJ0LmpzIiwiL2hvbWUvd2FuZy9wcm9qZWN0cy90a3NfcHJvamVjdC90a3MvYXBwL2pzL2FkbWluL2NvbnRyb2xsZXJzL2NhcnRfYWRkLmpzIiwiL2hvbWUvd2FuZy9wcm9qZWN0cy90a3NfcHJvamVjdC90a3MvYXBwL2pzL2FkbWluL2NvbnRyb2xsZXJzL2NhcnRfY29weS5qcyIsIi9ob21lL3dhbmcvcHJvamVjdHMvdGtzX3Byb2plY3QvdGtzL2FwcC9qcy9hZG1pbi9jb250cm9sbGVycy9jYXJ0cy5qcyIsIi9ob21lL3dhbmcvcHJvamVjdHMvdGtzX3Byb2plY3QvdGtzL2FwcC9qcy9jb21tb24vcHJvZmlsZV9kaXJlY3RpdmUuanMiLCIvaG9tZS93YW5nL3Byb2plY3RzL3Rrc19wcm9qZWN0L3Rrcy9hcHAvanMvY29tbW9uL3Rrc19kaXJlY3RpdmVzLmpzIiwiL2hvbWUvd2FuZy9wcm9qZWN0cy90a3NfcHJvamVjdC90a3MvYXBwL2pzL2NvbW1vbi90a3NfZGlyZWN0aXZlczIuanMiLCIvaG9tZS93YW5nL3Byb2plY3RzL3Rrc19wcm9qZWN0L3Rrcy9hcHAvanMvY29tbW9uL3Rrc19maWx0ZXJzLmpzIiwiL2hvbWUvd2FuZy9wcm9qZWN0cy90a3NfcHJvamVjdC90a3MvYXBwL2pzL2NvbW1vbi90a3Nfc2VydmljZXMuanMiLCIvaG9tZS93YW5nL3Byb2plY3RzL3Rrc19wcm9qZWN0L3Rrcy9hcHAvanMvbW9kdWxlcy9tb2RfYXV0aG9yaXplLmpzIiwiL2hvbWUvd2FuZy9wcm9qZWN0cy90a3NfcHJvamVjdC90a3MvYXBwL2pzL21vZHVsZXMvbW9kX2NhcmRzLmpzIiwiL2hvbWUvd2FuZy9wcm9qZWN0cy90a3NfcHJvamVjdC90a3MvYXBwL2pzL21vZHVsZXMvbW9kX2NvdW50cmllcy5qcyIsIi9ob21lL3dhbmcvcHJvamVjdHMvdGtzX3Byb2plY3QvdGtzL2FwcC9qcy9tb2R1bGVzL21vZF9wYWdlci5qcyIsIi9ob21lL3dhbmcvcHJvamVjdHMvdGtzX3Byb2plY3QvdGtzL2FwcC9qcy9tb2R1bGVzL21vZHVsZXMuanMiLCIvaG9tZS93YW5nL3Byb2plY3RzL3Rrc19wcm9qZWN0L3Rrcy9hcHAvanMvbW9kdWxlcy91dGlsaXRpZXMuanMiLCIvaG9tZS93YW5nL3Byb2plY3RzL3Rrc19wcm9qZWN0L3Rrcy9hcHBfdHMvanMvd2ViL3dlYl9jb250cm9sbGVyc19jYXJ0LmpzIiwiL2hvbWUvd2FuZy9wcm9qZWN0cy90a3NfcHJvamVjdC90a3MvZGF0YS9hcmlwby5qc29uIiwiL2hvbWUvd2FuZy9wcm9qZWN0cy90a3NfcHJvamVjdC90a3MvZGF0YS9mb3JtYXRzLmpzb24iLCIvaG9tZS93YW5nL3Byb2plY3RzL3Rrc19wcm9qZWN0L3Rrcy9kYXRhL29hcGkuanNvbiIsIi9ob21lL3dhbmcvcHJvamVjdHMvdGtzX3Byb2plY3QvdGtzL25vZGVfbW9kdWxlcy9jb3VudHJpZXMvY291bnRyaWVzLmpzb24iLCIvaG9tZS93YW5nL3Byb2plY3RzL3Rrc19wcm9qZWN0L3Rrcy9ub2RlX21vZHVsZXMvY291bnRyaWVzL2NvdW50cmllcy5qcyIsIi9ob21lL3dhbmcvcHJvamVjdHMvdGtzX3Byb2plY3QvdGtzL25vZGVfbW9kdWxlcy9jb3VudHJpZXMvc3RhdGVzLmpzb24iXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUNBQSxZQUFZLENBQUM7OztBQUdiLE9BQU8sQ0FBQyx3QkFBd0IsQ0FBQyxDQUFBO0FBQ2pDLE9BQU8sQ0FBQyxxQkFBcUIsQ0FBQyxDQUFBO0FBQzlCLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxDQUFBOztBQUdoQyxPQUFPLENBQUMsTUFBTSxDQUFDLFdBQVcsRUFBRSxDQUFDLFlBQVksRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLGNBQWMsRUFBRSxXQUFXLEVBQzFGLGVBQWUsRUFBRSxlQUFlLEVBQUMsb0JBQW9CLEVBQUUsZUFBZSxFQUFDLHVCQUF1QixFQUFFLGFBQWEsRUFBRSxnQkFBZ0IsRUFBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQ3BKLE1BQU0sQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUVwQyxHQUFHLENBQUMsQ0FBQyxZQUFZLEVBQUUsV0FBVyxFQUFFLGFBQWEsRUFBRSxVQUFVLFVBQVUsRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFO0FBQzVGLFNBQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUE7QUFDbkIsU0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQTtBQUNwQixZQUFVLENBQUMsR0FBRyxDQUFDLG1CQUFtQixFQUFFLFVBQVUsS0FBSyxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUU7O0FBRWxFLFdBQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUE7QUFDdkIsV0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUE7O0FBRXpCLFFBQUksSUFBSSxDQUFDLE9BQU8sS0FBSyxTQUFTLEVBQUU7QUFDOUIsVUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sS0FBSyxTQUFTLEVBQUU7QUFDckMsWUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRTtBQUN2QixjQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsRUFBRSxFQUMzQixTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQzdCO09BQ0Y7S0FDRjtHQUNGLENBQUMsQ0FBQztDQUNKLENBQUMsQ0FBQyxDQUVGLE9BQU8sQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLElBQUksRUFBRSxXQUFXLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxFQUFFLFNBQVMsRUFBRSxPQUFPLEVBQUU7QUFDM0YsU0FBTztBQUNMLFdBQU8sRUFBRSxpQkFBVSxNQUFNLEVBQUU7QUFDekIsWUFBTSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUMsT0FBTyxJQUFJLEVBQUUsQ0FBQztBQUN0QyxVQUFJLE9BQU8sQ0FBQyxZQUFZLEVBQUUsRUFBRTtBQUMxQixjQUFNLENBQUMsT0FBTyxDQUFDLGFBQWEsR0FBRyxTQUFTLEdBQUcsT0FBTyxDQUFDLFlBQVksRUFBRSxDQUFBO09BQ2xFO0FBQ0QsYUFBTyxNQUFNLENBQUM7S0FDZjtBQUNELFlBQVEsRUFBRSxrQkFBVSxTQUFRLEVBQUU7QUFDNUIsYUFBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsU0FBUSxDQUFDLE1BQU0sQ0FBQyxDQUFBO0FBQ3BDLGFBQU8sU0FBUSxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsU0FBUSxDQUFDLENBQUM7S0FDdEM7QUFDRCxpQkFBYSxFQUFFLHVCQUFVLFNBQVMsRUFBRTtBQUNsQyxhQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUE7QUFDckMsVUFBSSxTQUFTLENBQUMsTUFBTSxLQUFLLEdBQUcsRUFBRTtBQUM1QixZQUFJLFNBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO0FBQ2hELGlCQUFPLENBQUMsT0FBTyxFQUFFLENBQUE7QUFDakIsbUJBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUE7U0FDMUIsTUFBTTtBQUNMLG1CQUFTLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFBO1NBQ3BCO09BQ0Y7QUFDRCxhQUFPLEVBQUUsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUM7OztLQUc3QjtHQUNGLENBQUE7Q0FDRixDQUFDLENBQUMsQ0FFRixNQUFNLENBQUMsQ0FBQyxlQUFlLEVBQUUsVUFBVSxhQUFhLEVBQUU7QUFDakQsZUFBYSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztDQUNwRCxDQUFDLENBQUMsQ0FBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMvREwsWUFBWSxDQUFDOzs7QUFHYixJQUFJLFNBQVMsR0FBRyxPQUFPLENBQUMseUJBQXlCLENBQUMsQ0FBQTs7OztBQUlsRCxJQUFJLFVBQVUsR0FBRyxDQUFDLFFBQVEsRUFBRSxPQUFPLEVBQUUsVUFBVSxNQUFNLEVBQUUsS0FBSyxFQUFFOztBQUUxRCxXQUFPLEtBQUssQ0FBQyxHQUFHLENBQUMsd0JBQXdCLENBQUMsQ0FDckMsSUFBSSxDQUNMLFNBQVMsT0FBTyxDQUFDLFFBQVEsRUFBRTtBQUN2QixlQUFPLFFBQVEsQ0FBQyxJQUFJLENBQUE7S0FDdkIsRUFDRCxTQUFTLEtBQUssQ0FBQyxNQUFNLEVBQUU7QUFDbkIsZUFBTyxLQUFLLENBQUM7S0FDaEIsQ0FDQSxDQUFDO0NBQ1QsQ0FBQyxDQUFBOztBQUdGLE9BQU8sQ0FBQyxNQUFNLENBQUMsdUJBQXVCLEVBQUUsRUFBRSxDQUFDLENBQ3RDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsWUFBWSxFQUVwQyxDQUFDLENBRUQsVUFBVSxDQUFDLFVBQVUsRUFBRSxDQUFDLFFBQVEsRUFDN0IsVUFBVSxNQUFNLEVBQUU7QUFDZCxVQUFNLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQTs7QUFFeEIsVUFBTSxDQUFDLFNBQVMsR0FBRyxVQUFVLFNBQVMsRUFBRTtBQUNwQyxlQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsRUFBRSxTQUFTLENBQUMsQ0FBQTtLQUMxQyxDQUFBO0NBRUosQ0FBQyxDQUFDLENBRU4sVUFBVSxDQUFDLGtCQUFrQixFQUFFLENBQUMsUUFBUSxFQUFFLFdBQVcsRUFBRSxZQUFZLEVBQUUsYUFBYSxFQUFFLGFBQWEsRUFDOUYsVUFBVSxNQUFNLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQUUsV0FBVyxFQUFFOztBQUUvRCxVQUFNLENBQUMsSUFBSSxHQUFHLFlBQVk7QUFDdEIsZUFBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQTtBQUN2QixZQUFJLFdBQVcsQ0FBQyxVQUFVLEVBQUUsRUFBRTtBQUMxQixnQkFBSSxNQUFNLENBQUMsUUFBUSxLQUFLLElBQUksRUFDeEIsTUFBTSxDQUFDLFFBQVEsR0FBRyxXQUFXLENBQUMsUUFBUSxFQUFFLENBQUE7U0FDL0M7S0FDSixDQUFBOztBQUVELFVBQU0sQ0FBQyxrQkFBa0IsR0FBRyxZQUFZOztBQUVwQyxlQUFPLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLENBQUMsU0FBUyxFQUFFLFNBQVMsQ0FBQyxDQUFBO0tBQ3BFLENBQUE7O0FBRUQsVUFBTSxDQUFDLFFBQVEsR0FBRyxVQUFVLFlBQVksRUFBRTtBQUN0QyxlQUFPLFlBQVksS0FBSyxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUM7S0FDNUMsQ0FBQzs7QUFFRixVQUFNLENBQUMsVUFBVSxHQUFHLFlBQVk7QUFDNUIsZUFBTyxXQUFXLENBQUMsVUFBVSxFQUFFLENBQUE7S0FDbEMsQ0FBQTs7QUFFRCxVQUFNLENBQUMsTUFBTSxHQUFHLFlBQVk7QUFDeEIsbUJBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQTtBQUNwQixrQkFBVSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLENBQUM7QUFDakQsaUJBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUE7S0FDdEIsQ0FBQTs7QUFFRCxVQUFNLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQTtBQUN0QixVQUFNLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQTs7QUFFcEIsVUFBTSxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsWUFBWSxFQUFFLFVBQVUsQ0FBQyxFQUFFO0FBQzlDLGNBQU0sQ0FBQyxRQUFRLEdBQUcsV0FBVyxDQUFDLFFBQVEsRUFBRSxDQUFBO0tBQzNDLENBQUMsQ0FBQzs7QUFFSCxVQUFNLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsVUFBVSxDQUFDLEVBQUU7QUFDL0MsY0FBTSxDQUFDLFFBQVEsR0FBRyxXQUFXLENBQUMsUUFBUSxFQUFFLENBQUE7S0FDM0MsQ0FBQyxDQUFDOztBQUVILFVBQU0sQ0FBQyxJQUFJLEVBQUUsQ0FBQTtDQUVoQixDQUFDLENBQUMsQ0FHTixVQUFVLENBQUMsYUFBYSxFQUFFLENBQUMsUUFBUSxFQUFFLE9BQU8sRUFBRSxVQUFVLE1BQU0sRUFBRSxLQUFLLEVBQUU7O0FBRXBFLFVBQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFBOztBQUVuQixTQUFLLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsSUFBSSxFQUFFO0FBQ25ELGNBQU0sQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFBO0FBQ3JCLGNBQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFBO0tBQ3RCLENBQUMsQ0FBQTtDQUVMLENBQUMsQ0FBQyxDQUVGLFVBQVUsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDLFFBQVEsRUFBRSxPQUFPLEVBQUUsV0FBVyxFQUFFLFlBQVksRUFBRSxhQUFhLEVBQUUsYUFBYSxFQUMzRyxVQUFVLE1BQU0sRUFBRSxLQUFLLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQUUsV0FBVyxFQUFFOztBQUV0RSxVQUFNLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQzs7QUFFbkIsVUFBTSxDQUFDLFVBQVUsR0FBRyxVQUFVLEtBQUssRUFBRTtBQUNqQyxjQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7S0FDbEMsQ0FBQzs7QUFFRixVQUFNLENBQUMsWUFBWSxHQUFHLFVBQVUsSUFBSSxFQUFFOztBQUVsQyxtQkFBVyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxNQUFNLEVBQUU7QUFDNUMsZ0JBQUksTUFBTSxDQUFDLGFBQWEsS0FBSyxJQUFJLEVBQUU7O0FBRS9CLDBCQUFVLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsQ0FBQztBQUNoRCx5QkFBUyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFBO2FBRW5DLE1BQ0k7QUFDRCxzQkFBTSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUE7QUFDbEIsc0JBQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxHQUFHLEVBQUUsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7YUFDaEU7U0FDSixDQUFDLENBQUM7S0FDTixDQUFDOztBQUVGLFVBQU0sQ0FBQyxlQUFlLEdBQUcsVUFBVSxJQUFJLEVBQUU7QUFDckMsWUFBSSxBQUFDLElBQUksS0FBSyxTQUFTLElBQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxBQUFDLEVBQUU7QUFDdkMsa0JBQU0sQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFBO0FBQ2xCLGtCQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsR0FBRyxFQUFFLHFDQUFxQyxFQUFFLENBQUMsQ0FBQztTQUN2RixNQUFNO0FBQ0gsdUJBQVcsQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsTUFBTSxFQUFFO0FBQ2xFLHNCQUFNLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQTtBQUNsQixvQkFBSSxNQUFNLENBQUMsTUFBTSxFQUNiLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDO0FBQ2Ysd0JBQUksRUFBRSxTQUFTO0FBQ2YsdUJBQUcsRUFBRSxnRkFBZ0Y7aUJBQ3hGLENBQUMsQ0FBQSxLQUVGLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxHQUFHLEVBQUUsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUE7YUFDbkUsQ0FBQyxDQUFBO1NBQ0w7S0FDSixDQUFBO0NBQ0osQ0FBQyxDQUFDLENBRU4sVUFBVSxDQUFDLGFBQWEsRUFBRSxDQUFDLFFBQVEsRUFBRSxPQUFPLEVBQUUsY0FBYyxFQUFFLFNBQVMsRUFBRSxVQUFVLE1BQU0sRUFBRSxLQUFLLEVBQUUsWUFBWSxFQUFFLE9BQU8sRUFBRTtBQUN0SCxXQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFBOzs7Ozs7OztBQVN0QixVQUFNLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQzs7Ozs7Ozs7O0FBU3JCLFVBQU0sQ0FBQyxhQUFhLEdBQUcsVUFBVSxPQUFPLEVBQUU7QUFDdEMsZUFBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQTs7QUFFcEIsY0FBTSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxTQUFTLEVBQUUsRUFBRSxFQUFFLEVBQUUsWUFBWTtBQUM1RSxtQkFBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUE7U0FDOUIsQ0FBQyxDQUFDO0tBQ04sQ0FBQTs7Ozs7OztBQU9ELFVBQU0sQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFBOztBQUV0QixVQUFNLENBQUMsZ0JBQWdCLEdBQUcsWUFBWTtBQUNsQyxlQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFBOztBQUVuQixhQUFLLENBQUMsR0FBRyxDQUFDLDBCQUEwQixDQUFDLENBQ2hDLE9BQU8sQ0FBQyxVQUFVLElBQUksRUFBRTtBQUNyQixrQkFBTSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0FBQy9ELG1CQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQTtTQUNwQyxDQUFDLENBQUE7S0FDVCxDQUFBOztBQUVELFFBQUksWUFBWSxDQUFDLE9BQU8sRUFBRTtBQUN0QixjQUFNLENBQUMsV0FBVyxHQUFHLFlBQVksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFBO0tBQ2hEOztBQUVELFVBQU0sQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFBO0NBRzNDLENBQUMsQ0FBQyxDQUVGLFVBQVUsQ0FBQyxtQkFBbUIsRUFBRSxDQUFDLFFBQVEsRUFBRSxPQUFPLEVBQUUsY0FBYyxFQUFFLGVBQWUsRUFBRSxVQUFVLE1BQU0sRUFBRSxLQUFLLEVBQUUsWUFBWSxFQUFFLGFBQWEsRUFBRTs7QUFFeEksV0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQTs7QUFFdkIsVUFBTSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7Ozs7Ozs7OztBQVNyQixVQUFNLENBQUMsYUFBYSxHQUFHLFVBQVUsT0FBTyxFQUFFO0FBQ3RDLGVBQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUE7O0FBRXBCLGNBQU0sQ0FBQyxPQUFPLEdBQUcsYUFBYSxDQUFDLEtBQUssQ0FBQyxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsU0FBUyxFQUFFLEVBQUUsRUFBRSxFQUFFLFlBQVk7QUFDbEYsbUJBQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFBO1NBQzlCLENBQUMsQ0FBQztLQUNOLENBQUE7Ozs7Ozs7QUFPRCxVQUFNLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQTs7QUFFdEIsVUFBTSxDQUFDLGdCQUFnQixHQUFHLFlBQVk7QUFDbEMsZUFBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQTs7QUFFbkIsYUFBSyxDQUFDLEdBQUcsQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUN0QyxPQUFPLENBQUMsVUFBVSxJQUFJLEVBQUU7QUFDckIsa0JBQU0sQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQztBQUMvRCxtQkFBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUE7U0FDcEMsQ0FBQyxDQUFBO0tBQ1QsQ0FBQTs7QUFFRCxRQUFJLFlBQVksQ0FBQyxPQUFPLEVBQUU7QUFDdEIsY0FBTSxDQUFDLFdBQVcsR0FBRyxZQUFZLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQTtLQUNoRDs7QUFFRCxVQUFNLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQTtDQUUzQyxDQUFDLENBQUMsQ0FFRixVQUFVLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxRQUFRLEVBQUUsT0FBTyxFQUFFLFlBQVksRUFBRSxVQUFVLE1BQU0sRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFFO0FBQ2pHLFVBQU0sQ0FBQyxPQUFPLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxZQUFZLEVBQzdDLENBQUMsQ0FBQztDQUVOLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxtQkFBbUIsRUFBRSxDQUFDLFFBQVEsRUFBRSxPQUFPLEVBQUUsY0FBYyxFQUFFLFdBQVcsRUFBRSxXQUFXLEVBQUUsU0FBUyxFQUN2RyxVQUFVLE1BQU0sRUFBRSxLQUFLLEVBQUUsWUFBWSxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsT0FBTyxFQUFFOztBQUdsRSxXQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFBO0FBQ3pCLFVBQU0sQ0FBQyxPQUFPLEdBQUcsQ0FDYixFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxFQUM5QixFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxDQUNwQyxDQUFBO0FBQ0QsVUFBTSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUE7O0FBRTVCLFFBQUksWUFBWSxDQUFDLFFBQVEsRUFDckIsTUFBTSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQTs7QUFHeEQsVUFBTSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUE7O0FBRTNCLFdBQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUE7QUFDekIsUUFBSSxZQUFZLENBQUMsRUFBRSxLQUFLLEtBQUssRUFBRTtBQUMzQixjQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQTtLQUV0QixNQUVJOztBQUVELGFBQUssQ0FBQyxHQUFHLENBQUMscUJBQXFCLEdBQUcsWUFBWSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksRUFBRTtBQUNwRSxtQkFBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQTtBQUNqQixrQkFBTSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFBO0FBQzFCLGtCQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQTtBQUNuQixtQkFBTyxLQUFLLENBQUMsR0FBRyxDQUFDLHFCQUFxQixHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUE7U0FDbkUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLE9BQU8sRUFBRTtBQUN2QixtQkFBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQTtBQUNwQixrQkFBTSxDQUFDLElBQUksR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFBO0FBQzFCLG1CQUFPLEtBQUssQ0FBQyxHQUFHLENBQUMsc0JBQXNCLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQTtTQUU3RCxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsUUFBUSxFQUFFOztBQUV4QixtQkFBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUE7QUFDM0Isa0JBQU0sQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQTtBQUMvQixtQkFBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUE7U0FDL0IsQ0FBQyxDQUFBO0tBQ0w7O0FBR0QsVUFBTSxDQUFDLE1BQU0sR0FBRyxZQUFZO0FBQ3hCLFlBQUksTUFBTSxDQUFDLFFBQVEsRUFDZixTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQTtLQUN0QyxDQUFBOztBQUVELFVBQU0sQ0FBQyxjQUFjLEdBQUcsVUFBVSxPQUFPLEVBQUU7O0FBR3ZDLGFBQUssQ0FBQyxJQUFJLENBQUMsMkJBQTJCLEVBQUUsT0FBTyxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsSUFBSSxFQUFFO0FBQ3JFLG1CQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFBO0FBQ2pCLGdCQUFJLElBQUksQ0FBQyxNQUFNLEtBQUssSUFBSSxFQUFFO0FBQ3RCLHNCQUFNLENBQUMsUUFBUSxHQUFHLGdDQUFnQyxDQUFBO0FBQ2xELG9CQUFJLE1BQU0sQ0FBQyxRQUFRLEVBQUU7QUFDakIsNkJBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFBO2lCQUNsQzthQUVKLE1BQU07QUFDSCxzQkFBTSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFBO2FBQzdCO1NBRUosQ0FBQyxDQUFDO0tBRU4sQ0FBQTtDQUVKLENBQUMsQ0FBQyxDQUVOLFVBQVUsQ0FBQyw4QkFBOEIsRUFBRSxDQUFDLFFBQVEsRUFBRSxRQUFRLEVBQUUsT0FBTyxFQUFFLFlBQVksRUFBRSxjQUFjLEVBQUUsUUFBUSxFQUFFLGVBQWUsRUFBRSxTQUFTLEVBQ3hJLFVBQVUsTUFBTSxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQUUsYUFBYSxFQUFFLE9BQU8sRUFBRTs7QUFHdkYsV0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQTs7QUFFcEIsVUFBTSxDQUFDLEdBQUcsR0FBRyxZQUFZLENBQUMsRUFBRSxDQUFBO0FBQzVCLFVBQU0sQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQTs7QUFFN0IsVUFBTSxDQUFDLGFBQWEsR0FBRyxVQUFVLFFBQU8sRUFBRTtBQUN0QyxlQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUE7QUFDN0IsZUFBTyxDQUFDLEdBQUcsQ0FBQyxRQUFPLENBQUMsQ0FBQTtBQUNwQixlQUFPLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFDLENBQUE7O0FBRy9CLFlBQUksYUFBYSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUM7QUFDNUIsdUJBQVcsRUFBRSxtQ0FBbUM7QUFDaEQsdUJBQVcsRUFBRSxrQkFBa0I7QUFDL0IsZ0JBQUksRUFBRSxJQUFJO0FBQ1Ysc0JBQVUsRUFBRSxtQkFBbUIsRUFBRSxPQUFPLEVBQUU7QUFDdEMsc0JBQU0sRUFBRSxVQUFVO0FBQ2xCLHVCQUFPLEVBQUUsbUJBQVk7QUFDakIsMkJBQU8sUUFBTyxDQUFBO2lCQUNqQjthQUNKO1NBQ0osQ0FBQyxDQUFDOztBQUVILHFCQUFhLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLFdBQVcsRUFBRTtBQUM3QyxtQkFBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsV0FBVyxDQUFDLENBQUE7QUFDbEMsbUJBQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFBO0FBQzlCLGdCQUFJLFdBQVcsQ0FBQyxLQUFLLEtBQUssU0FBUyxFQUFFOztBQUVqQywyQkFBVyxDQUFDLGVBQWUsR0FBRyxRQUFPLENBQUMsR0FBRyxDQUFBOztBQUV6Qyx1QkFBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQTs7QUFFeEIsNkJBQWEsQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsSUFBSSxFQUFFO0FBQzFELDJCQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQTtBQUMxQiwyQkFBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFBO0FBQ2pDLHdCQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7QUFDYiwrQkFBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtBQUNsQiw4QkFBTSxDQUFDLE1BQU0sRUFBRSxDQUFBO3FCQUNsQjtpQkFDSixDQUFDLENBQUE7YUFDTDtTQUdKLEVBQUUsWUFBWTs7U0FFZCxDQUFDLENBQUM7S0FFTixDQUFBO0NBRUosQ0FBQyxDQUFDLENBRU4sVUFBVSxDQUFDLG1CQUFtQixFQUFFLENBQUMsUUFBUSxFQUFFLGdCQUFnQixFQUFFLFNBQVMsRUFBRSxRQUFRLEVBQzdFLFVBQVUsTUFBTSxFQUFFLGNBQWMsRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFOztBQUUvQyxVQUFNLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztBQUN6QixVQUFNLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQzs7QUFFdkIsV0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUE7O0FBRTdCLFVBQU0sQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFBOztBQUVsQixVQUFNLENBQUMsRUFBRSxHQUFHLFlBQVk7QUFDcEIsZUFBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUE7QUFDMUIsc0JBQWMsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0tBQ3ZDLENBQUM7O0FBRUYsVUFBTSxDQUFDLE1BQU0sR0FBRyxZQUFZO0FBQ3hCLHNCQUFjLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0tBQ3BDLENBQUM7Q0FFTCxDQUFDLENBQUMsQ0FHTixVQUFVLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxRQUFRLEVBQUUsUUFBUSxFQUFFLE9BQU8sRUFBRSxZQUFZLEVBQUUsY0FBYyxFQUFFLFFBQVEsRUFBRSxlQUFlLEVBQUUsUUFBUSxFQUMzSCxVQUFVLE1BQU0sRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsTUFBTSxFQUFFLGFBQWEsRUFBRSxNQUFNLEVBQUU7O0FBRXRGLFFBQUksRUFBRSxHQUFHLElBQUksQ0FBQTtBQUNiLFdBQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUE7QUFDbkIsV0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQTtBQUNoQixXQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFBO0FBQ2hCLFFBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFBO0FBQ3BCLFFBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFBOztBQUVmLGlCQUFhLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksRUFBRTtBQUN4RSxlQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQTs7QUFFeEIsVUFBRSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUE7O0tBRXpCLENBQUMsQ0FBQTs7QUFHRixVQUFNLENBQUMsV0FBVyxHQUFHLFVBQVUsSUFBSSxFQUFFLE1BQU0sRUFBRTtBQUN6QyxlQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFBO0FBQ3RCLGVBQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUE7O0FBR2pCLHFCQUFhLENBQUMscUJBQXFCLENBQUM7QUFDaEMscUJBQVMsRUFBRSxFQUFFLENBQUMsTUFBTSxDQUFDLEdBQUc7QUFDeEIsZUFBRyxFQUFFLElBQUk7QUFDVCxrQkFBTSxFQUFFLE1BQU07U0FDakIsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksRUFBRTtBQUNwQixtQkFBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUE7QUFDeEIsa0JBQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQTtTQUNsQixDQUFDLENBQUE7S0FDTCxDQUFBOztBQUVELFVBQU0sQ0FBQyxjQUFjLEdBQUcsVUFBVSxHQUFHLEVBQUU7O0FBRW5DLGVBQU8sQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFBO0FBQ3RCLGVBQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLEdBQUcsQ0FBQyxDQUFBOztBQUU1QixZQUFJLE9BQU8sQ0FBQyx5Q0FBeUMsQ0FBQyxFQUFFOztBQUVwRCx5QkFBYSxDQUFDLHFCQUFxQixDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksRUFBRTtBQUN6RSx1QkFBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQTtBQUNqQixzQkFBTSxDQUFDLE1BQU0sRUFBRSxDQUFBO2FBQ2xCLENBQUMsQ0FBQTtTQUVMO0tBQ0osQ0FBQTtDQUdKLENBQUMsQ0FBQyxDQUdOLFVBQVUsQ0FBQyxtQkFBbUIsRUFBRSxDQUFDLFFBQVEsRUFBRSxPQUFPLEVBQUUsY0FBYyxFQUFFLFdBQVcsRUFDNUUsVUFBVSxNQUFNLEVBQUUsS0FBSyxFQUFFLFlBQVksRUFBRSxTQUFTLEVBQUU7O0FBRTlDLFVBQU0sQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDO0FBQ25CLFVBQU0sQ0FBQyxVQUFVLEdBQUcsVUFBVSxLQUFLLEVBQUU7QUFDakMsY0FBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO0tBQ2xDLENBQUM7O0FBRUYsVUFBTSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUE7QUFDM0IsVUFBTSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUE7O0FBRTVCLFNBQUssQ0FBQyxHQUFHLENBQUMscUJBQXFCLEdBQUcsWUFBWSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksRUFBRTs7QUFFcEUsY0FBTSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFBOztBQUV2QixZQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUNqQixNQUFNLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFBOztBQUVwQyxjQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQTs7QUFFbkIsZUFBTyxLQUFLLENBQUMsR0FBRyxDQUFDLHNCQUFzQixHQUFHLFlBQVksQ0FBQyxFQUFFLENBQUMsQ0FBQTtLQUc3RCxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsUUFBUSxFQUFFOztBQUV4QixjQUFNLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUE7S0FFbEMsQ0FBQyxDQUFBOztBQUVGLFVBQU0sQ0FBQyxZQUFZLEdBQUcsVUFBVSxLQUFLLEVBQUU7QUFDbkMsZUFBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtBQUNsQixhQUFLLENBQUMsSUFBSSxDQUFDLHlCQUF5QixFQUFFO0FBQ2xDLG1CQUFPLEVBQUUsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHO0FBQ3hCLGlCQUFLLEVBQUUsS0FBSztTQUNmLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxJQUFJLEVBQUU7QUFDdkIsbUJBQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUE7QUFDakIsZ0JBQUksSUFBSSxDQUFDLE1BQU0sS0FBSyxJQUFJLEVBQUU7O0FBRXRCLHNCQUFNLENBQUMsUUFBUSxHQUFHLFNBQVMsQ0FBQTthQUU5QixNQUFNO0FBQ0gsc0JBQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQTthQUN0QztTQUNKLENBQUMsQ0FBQztLQUNOLENBQUE7O0FBRUQsVUFBTSxDQUFDLFdBQVcsR0FBRyxVQUFVLElBQUksRUFBRTs7QUFFakMsYUFBSyxDQUFDLElBQUksQ0FBQyx3QkFBd0IsRUFBRSxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxJQUFJLEVBQUU7QUFDL0QsbUJBQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUE7QUFDakIsZ0JBQUksSUFBSSxDQUFDLE1BQU0sS0FBSyxJQUFJLEVBQUU7QUFDdEIsc0JBQU0sQ0FBQyxRQUFRLEdBQUcsZ0NBQWdDLENBQUE7YUFDckQsTUFBTTtBQUNILHNCQUFNLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUE7YUFDN0I7U0FDSixDQUFDLENBQUM7S0FDTixDQUFBOztBQUVELFVBQU0sQ0FBQyxTQUFTLEdBQUcsWUFBWTtBQUMzQixjQUFNLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQTtLQUN2QixDQUFBO0NBR0osQ0FBQyxDQUFDLENBRU4sVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsRUFBRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE9BQU8sRUFBRSxPQUFPLENBQUMscUJBQXFCLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUUzRyxVQUFVLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxRQUFRLEVBQUUsT0FBTyxFQUFFLGNBQWMsRUFBRSxRQUFRLEVBQUUsV0FBVyxFQUFFLE9BQU8sQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBRTVILFVBQVUsQ0FBQyxtQkFBbUIsRUFBRSxDQUFDLFFBQVEsRUFBRSxPQUFPLEVBQUUsY0FBYyxFQUFFLFdBQVcsRUFBRSxXQUFXLEVBQUUsZUFBZSxFQUMxRyxPQUFPLENBQUMsd0JBQXdCLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUUvQyxVQUFVLENBQUMsb0JBQW9CLEVBQUUsQ0FBQyxRQUFRLEVBQUUsT0FBTyxFQUFFLGNBQWMsRUFBRSxXQUFXLEVBQUUsV0FBVyxFQUFFLGVBQWUsRUFBRSxPQUFPLEVBQ3BILE9BQU8sQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBR2pELFVBQVUsQ0FBQywwQkFBMEIsRUFBRSxDQUFDLFFBQVEsRUFBRSxPQUFPLEVBQUUsVUFBVSxNQUFNLEVBQUUsS0FBSyxFQUFFOztBQUVqRixVQUFNLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQTs7QUFFdEIsU0FBSyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUksRUFBRTtBQUNyRCxlQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFBO0FBQ2pCLGNBQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFBO0FBQ3RCLGNBQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFBO0tBQ3RCLENBQUMsQ0FBQTtDQUVMLENBQUMsQ0FBQyxDQUVGLFVBQVUsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxRQUFRLEVBQUUsT0FBTyxFQUFFLGNBQWMsRUFBRSxRQUFRLEVBQUUsVUFBVSxNQUFNLEVBQUUsS0FBSyxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQUU7O0FBRW5ILFdBQU8sQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDLENBQUE7O0FBRTFCLFVBQU0sQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDOztBQUVyQixVQUFNLENBQUMsYUFBYSxHQUFHLFVBQVUsT0FBTyxFQUFFO0FBQ3RDLGVBQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUE7QUFDcEIsY0FBTSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxTQUFTLEVBQUUsRUFBRSxFQUFFLEVBQUUsWUFBWTtBQUMzRSxtQkFBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUE7U0FDOUIsQ0FBQyxDQUFDO0tBQ04sQ0FBQTs7QUFFRCxVQUFNLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQTs7QUFFdEIsVUFBTSxDQUFDLGdCQUFnQixHQUFHLFlBQVk7QUFDbEMsYUFBSyxDQUFDLEdBQUcsQ0FBQyx5QkFBeUIsQ0FBQyxDQUMvQixPQUFPLENBQUMsVUFBVSxJQUFJLEVBQUU7QUFDckIsa0JBQU0sQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQztBQUMvRCxtQkFBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUE7U0FDcEMsQ0FBQyxDQUFBO0tBQ1QsQ0FBQTs7QUFFRCxRQUFJLFlBQVksQ0FBQyxPQUFPLEVBQUU7QUFDdEIsY0FBTSxDQUFDLFdBQVcsR0FBRyxZQUFZLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQTtLQUNoRDs7QUFFRCxVQUFNLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQTtDQUUzQyxDQUFDLENBQUMsQ0FFRixVQUFVLENBQUMsdUJBQXVCLEVBQUUsQ0FBQyxRQUFRLEVBQUUsT0FBTyxFQUFFLGNBQWMsRUFBRSxVQUFVLE1BQU0sRUFBRSxLQUFLLEVBQUUsWUFBWSxFQUFFO0FBQzVHLFVBQU0sQ0FBQyxHQUFHLEdBQUcsWUFBWSxDQUFDLEVBQUUsQ0FBQTtBQUM1QixTQUFLLENBQUMsR0FBRyxDQUFDLGFBQWEsR0FBRyxZQUFZLENBQUMsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsSUFBSSxFQUFFO0FBQy9ELGVBQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUE7QUFDakIsY0FBTSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFBO0FBQzFCLGVBQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUE7QUFDdEIsZUFBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUE7S0FDOUIsQ0FBQyxDQUFBOztBQUVGLFVBQU0sQ0FBQyxJQUFJLEdBQUcsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLENBQUE7O0FBRTNCLFVBQU0sQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFBOztBQUVyQixVQUFNLENBQUMsVUFBVSxHQUFHLFlBQVk7O0FBRTVCLGVBQU8sQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDLENBQUE7QUFDMUIsZUFBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUE7O0FBRXhCLGNBQU0sQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFBOztBQUd0QixhQUFLLENBQUMsR0FBRyxDQUFDLHlCQUF5QixHQUFHLFlBQVksQ0FBQyxFQUFFLEdBQUcsR0FBRyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsSUFBSSxFQUFFOztBQUVyRyxtQkFBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQTtBQUNuQixtQkFBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQTs7OztTQUlwQixDQUFDLENBQUE7S0FHTCxDQUFBO0NBRUosQ0FBQyxDQUFDLENBRUYsVUFBVSxDQUFDLFVBQVUsRUFBRSxDQUFDLFFBQVEsRUFBRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sRUFBRSxVQUFVLE1BQU0sRUFBRSxLQUFLLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRTs7QUFFN0csV0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsQ0FBQTs7QUFFMUIsVUFBTSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7O0FBRXJCLFVBQU0sQ0FBQyxhQUFhLEdBQUcsVUFBVSxPQUFPLEVBQUU7QUFDdEMsZUFBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQTtBQUNwQixjQUFNLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFNBQVMsRUFBRSxFQUFFLEVBQUUsRUFBRSxZQUFZO0FBQ3pFLG1CQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQTtTQUM5QixDQUFDLENBQUM7S0FDTixDQUFBOztBQUVELFVBQU0sQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFBOztBQUV0QixVQUFNLENBQUMsZ0JBQWdCLEdBQUcsWUFBWTtBQUNsQyxhQUFLLENBQUMsR0FBRyxDQUFDLHVCQUF1QixDQUFDLENBQzdCLE9BQU8sQ0FBQyxVQUFVLElBQUksRUFBRTtBQUNyQixrQkFBTSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0FBQy9ELG1CQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQTtTQUNwQyxDQUFDLENBQUE7S0FDVCxDQUFBOztBQUVELFFBQUksWUFBWSxDQUFDLE9BQU8sRUFBRTtBQUN0QixjQUFNLENBQUMsV0FBVyxHQUFHLFlBQVksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFBO0tBQ2hEOztBQUVELFVBQU0sQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFBO0NBRTNDLENBQUMsQ0FBQyxDQUVGLFVBQVUsQ0FBQyx1QkFBdUIsRUFBRSxDQUFDLFFBQVEsRUFBRSxPQUFPLEVBQUUsY0FBYyxFQUFFLFdBQVcsRUFDaEYsVUFBVSxNQUFNLEVBQUUsS0FBSyxFQUFFLFlBQVksRUFBRSxTQUFTLEVBQUU7O0FBRzlDLFVBQU0sQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFBOztBQUVsQixVQUFNLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQztBQUNuQixVQUFNLENBQUMsVUFBVSxHQUFHLFVBQVUsS0FBSyxFQUFFO0FBQ2pDLGNBQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztLQUNsQyxDQUFDOztBQUVGLFVBQU0sQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFBO0FBQzNCLFVBQU0sQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFBOztBQUU1QixVQUFNLENBQUMsR0FBRyxHQUFHLEVBQUUsT0FBTyxFQUFFLFNBQVMsRUFBRSxDQUFBOztBQUVuQyxVQUFNLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxVQUFVLE1BQU0sRUFBRSxNQUFNLEVBQUU7QUFDbkQsZUFBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFBO0FBQ3RDLFlBQUksTUFBTSxLQUFLLFNBQVMsRUFBRTtBQUN0QixpQkFBSyxDQUFDLEdBQUcsQ0FBQywwQkFBMEIsR0FBRyxNQUFNLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxLQUFLLEVBQUU7QUFDcEUsdUJBQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUE7QUFDbEIsb0JBQUksS0FBSyxZQUFZLEtBQUssRUFBRTs7QUFFeEIsMkJBQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUE7QUFDbEIsMEJBQU0sQ0FBQyxHQUFHLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUE7QUFDckMsMEJBQU0sQ0FBQyxHQUFHLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUE7O0FBRXJDLDBCQUFNLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFBO0FBQ2pDLDBCQUFNLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFBOztBQUVqQywwQkFBTSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQTtBQUNuQywwQkFBTSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQTs7QUFFbkMsMEJBQU0sQ0FBQyxHQUFHLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUE7QUFDckMsMEJBQU0sQ0FBQyxHQUFHLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUE7O0FBRXJDLDBCQUFNLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFBO0FBQ2pDLDBCQUFNLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFBOztBQUVqQywwQkFBTSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQTtBQUNuQywwQkFBTSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQTtpQkFHdEMsTUFBTTtBQUNILHdCQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtBQUNqQywrQkFBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTs7QUFFbEIsOEJBQU0sQ0FBQyxHQUFHLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUE7QUFDbEMsOEJBQU0sQ0FBQyxHQUFHLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUE7O0FBRWxDLDhCQUFNLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFBO0FBQzlCLDhCQUFNLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFBOztBQUU5Qiw4QkFBTSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQTtBQUNoQyw4QkFBTSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQTs7QUFFaEMsOEJBQU0sQ0FBQyxHQUFHLENBQUMsUUFBUSxHQUFHLFNBQVMsQ0FBQTtBQUMvQiw4QkFBTSxDQUFDLEdBQUcsQ0FBQyxRQUFRLEdBQUcsU0FBUyxDQUFBOztBQUUvQiw4QkFBTSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsU0FBUyxDQUFBO0FBQzdCLDhCQUFNLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxTQUFTLENBQUE7O0FBRTdCLDhCQUFNLENBQUMsR0FBRyxDQUFDLE9BQU8sR0FBRyxTQUFTLENBQUE7QUFDOUIsOEJBQU0sQ0FBQyxHQUFHLENBQUMsT0FBTyxHQUFHLFNBQVMsQ0FBQTtxQkFFakMsTUFBTTtBQUNILCtCQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFBO0FBQ3BCLDhCQUFNLENBQUMsR0FBRyxHQUFHLEVBQUUsT0FBTyxFQUFFLE1BQU0sQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLENBQUE7cUJBQy9DO2lCQUNKO2FBQ0osQ0FBQyxDQUFBO1NBQ0w7S0FDSixDQUFDLENBQUM7O0FBRUgsU0FBSyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsR0FBRyxZQUFZLENBQUMsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsSUFBSSxFQUFFO0FBQ3ZFLGVBQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUE7QUFDakIsY0FBTSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUE7QUFDbEIsZUFBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUE7QUFDeEIsWUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssRUFDakIsTUFBTSxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQTs7QUFFcEMsYUFBSyxDQUFDLEdBQUcsQ0FBQyxnQ0FBZ0MsR0FBRyxZQUFZLENBQUMsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsTUFBTSxFQUFFO0FBQ3BGLGtCQUFNLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQTtBQUN0QixrQkFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUE7QUFDbkIsbUJBQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFBO1NBQ2hDLENBQUMsQ0FBQTtLQUNMLENBQUMsQ0FBQTs7QUFFRixVQUFNLENBQUMsU0FBUyxHQUFHLFVBQVUsR0FBRyxFQUFFO0FBQzlCLGVBQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUE7QUFDaEIsY0FBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQTtBQUM5QyxjQUFNLENBQUMsR0FBRyxHQUFHLEVBQUUsQ0FBQTtLQUNsQixDQUFBOztBQUVELFVBQU0sQ0FBQyxTQUFTLEdBQUcsVUFBVSxPQUFPLEVBQUU7QUFDbEMsZUFBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQTtBQUNwQixlQUFPLE1BQU0sQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUE7S0FDaEMsQ0FBQTs7QUFFRCxVQUFNLENBQUMsV0FBVyxHQUFHLFVBQVUsTUFBTSxFQUFFO0FBQ25DLGVBQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUE7QUFDbkIsYUFBSyxDQUFDLElBQUksQ0FBQyxrQ0FBa0MsR0FBRyxZQUFZLENBQUMsRUFBRSxFQUFFLE1BQU0sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUksRUFBRTtBQUM3RixtQkFBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQTtBQUNqQixrQkFBTSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUE7U0FDL0IsQ0FBQyxDQUFDO0tBQ04sQ0FBQTtDQUVKLENBQUMsQ0FBQyxDQUVOLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxRQUFRLEVBQUUsY0FBYyxFQUFFLE9BQU8sRUFDdkQsVUFBVSxNQUFNLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBRTs7QUFHbkMsU0FBSyxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsTUFBTSxFQUFFO0FBQzNFLGNBQU0sQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFBO0FBQ3RCLGVBQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUE7S0FDdEIsQ0FBQyxDQUFBO0NBRUwsQ0FBQyxDQUFDLENBRU4sVUFBVSxDQUFDLFVBQVUsRUFBRSxDQUFDLFFBQVEsRUFBRSxPQUFPLEVBQUUsVUFBVSxNQUFNLEVBQUUsS0FBSyxFQUFFOztBQUVqRSxVQUFNLENBQUMsSUFBSSxHQUFHLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxDQUFBO0FBQy9CLFVBQU0sQ0FBQyxHQUFHLEdBQUc7QUFDVCxZQUFJLEVBQUUsR0FBRztBQUNULGVBQU8sRUFBRSxDQUFDLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLEVBQUUsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsRUFBRTtBQUN0RixrQkFBTSxFQUFFLElBQUk7QUFDWixxQkFBUyxFQUFFLFNBQVM7U0FDdkIsQ0FBQztLQUNMLENBQUE7Q0FHSixDQUFDLENBQUMsQ0FBQTs7O0FDcHZCUCxZQUFZLENBQUM7O0FBR2IsU0FBUyxVQUFVLENBQUMsU0FBUyxFQUFFLEVBQUUsRUFBRSxXQUFXLEVBQUU7QUFDOUMsTUFBSSxRQUFRLEdBQUcsRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDOztBQUUxQixNQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsRUFBRSxFQUFFO0FBQzdCLFlBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQTtBQUNqQixhQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0dBQzNCLE1BQ0MsUUFBUSxDQUFDLE9BQU8sRUFBRSxDQUFBO0FBQ3BCLFNBQU8sUUFBUSxDQUFDLE9BQU8sQ0FBQztDQUN6Qjs7QUFFRCxJQUFJLFlBQVksR0FBRyxDQUFDLFdBQVcsRUFBRSxJQUFJLEVBQUUsYUFBYSxFQUFFLFVBQVUsQ0FBQyxDQUFBOztBQUVqRSxJQUFJLFFBQVEsR0FBRztBQUNiLGVBQWEsRUFBRSxZQUFZO0NBQzVCLENBQUE7Ozs7QUFJRCxJQUFJLHdCQUF3QixHQUFHLENBQUUsUUFBUSxFQUFFLE9BQU8sRUFBRyxVQUFVLE1BQU0sRUFBRSxLQUFLLEVBQUU7QUFDNUUsU0FBTyxLQUFLLENBQUMsR0FBRyxDQUFDLG9CQUFvQixHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUNoRSxJQUFJLENBQ0gsU0FBUyxPQUFPLENBQUMsUUFBUSxFQUFFO0FBQUUsV0FBTyxRQUFRLENBQUMsSUFBSSxDQUFBO0dBQUUsRUFDbkQsU0FBUyxLQUFLLENBQUMsTUFBTSxFQUFNO0FBQUUsV0FBTyxLQUFLLENBQUM7R0FBRSxDQUM3QyxDQUFDO0NBQ0gsQ0FBQyxDQUFBOztBQUVGLElBQUksVUFBVSxHQUFHLENBQUUsUUFBUSxFQUFFLE9BQU8sRUFBRyxVQUFVLE1BQU0sRUFBRSxLQUFLLEVBQUU7QUFDOUQsU0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQTtBQUN6QixTQUFPLEtBQUssQ0FBQyxHQUFHLENBQUMsd0JBQXdCLENBQUMsQ0FDdkMsSUFBSSxDQUNMLFNBQVMsT0FBTyxDQUFDLFFBQVEsRUFBRTtBQUFFLFdBQU8sUUFBUSxDQUFDLElBQUksQ0FBQTtHQUFFLEVBQ25ELFNBQVMsS0FBSyxDQUFDLE1BQU0sRUFBTTtBQUFFLFdBQU8sS0FBSyxDQUFDO0dBQUUsQ0FDN0MsQ0FBQztDQUVILENBQUMsQ0FBQTs7QUFHRixJQUFJLGtCQUFrQixHQUFHLENBQUUsUUFBUSxFQUFFLE9BQU8sRUFBRyxVQUFVLE1BQU0sRUFBRSxLQUFLLEVBQUU7QUFDdEUsU0FBTyxLQUFLLENBQUMsR0FBRyxDQUFDLDRCQUE0QixHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUN0RSxJQUFJLENBQ0wsU0FBUyxPQUFPLENBQUMsUUFBUSxFQUFFO0FBQUUsV0FBTyxRQUFRLENBQUMsSUFBSSxDQUFBO0dBQUUsRUFDbkQsU0FBUyxLQUFLLENBQUMsTUFBTSxFQUFNO0FBQUUsV0FBTyxLQUFLLENBQUM7R0FBRSxDQUM3QyxDQUFDO0NBQ0gsQ0FBQyxDQUFBOztBQUdGLE1BQU0sQ0FBQyxPQUFPLEdBQUcsQ0FBQyxnQkFBZ0IsRUFBRSxtQkFBbUIsRUFBRSxVQUFVLGNBQWMsRUFBRSxpQkFBaUIsRUFBRTs7QUFFcEcsZ0JBQWMsQ0FDWixJQUFJLENBQUMsR0FBRyxFQUFFLEVBQUMsV0FBVyxFQUFFLDBCQUEwQixFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUMsQ0FBQyxDQUM1RSxJQUFJLENBQUMsUUFBUSxFQUFFLEVBQUMsV0FBVyxFQUFFLHFCQUFxQixFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUMsQ0FBQyxDQUM1RSxJQUFJLENBQUMsUUFBUSxFQUFFLEVBQUMsV0FBVyxFQUFFLHFCQUFxQixFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUMsQ0FBQyxDQUM1RSxJQUFJLENBQUMsUUFBUSxFQUFFLEVBQUMsV0FBVyxFQUFFLHFCQUFxQixFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUMsQ0FBQyxDQUM1RSxJQUFJLENBQUMsVUFBVSxFQUFFLEVBQUMsV0FBVyxFQUFFLDZCQUE2QixFQUFFLFVBQVUsRUFBRSxhQUFhLEVBQUMsQ0FBQyxDQUN6RixJQUFJLENBQUMsZ0JBQWdCLEVBQUUsRUFBQyxXQUFXLEVBQUUsbUNBQW1DLEVBQUUsVUFBVSxFQUFFLG1CQUFtQixFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLFFBQVEsRUFBQyxDQUFDLENBQzVJLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxFQUFDLFdBQVcsRUFBRSxrQ0FBa0MsRUFBRSxVQUFVLEVBQUUsOEJBQThCLEVBQUUsTUFBTSxFQUFFLElBQUk7QUFDaEksV0FBTyxFQUFFO0FBQ0wsY0FBUSxFQUFDLFlBQVk7QUFDckIsYUFBTyxFQUFFLHdCQUF3QjtLQUNsQyxFQUFDLENBQUMsQ0FFVCxJQUFJLENBQUMsYUFBYSxFQUFFLEVBQUMsV0FBVyxFQUFFLDRCQUE0QixFQUFFLFVBQVUsRUFBRSxrQkFBa0IsRUFBRSxZQUFZLEVBQUMsU0FBUyxFQUFHLE1BQU0sRUFBRSxJQUFJO0FBQ25JLFdBQU8sRUFBRTtBQUNQLGNBQVEsRUFBQyxZQUFZO0FBQ3JCLFlBQU0sRUFBRSxrQkFBa0I7S0FDM0IsRUFBQyxDQUFDLENBRUwsSUFBSSxDQUFDLFNBQVMsRUFBRSxFQUFDLFdBQVcsRUFBRSw0QkFBNEIsRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUMsQ0FBQyxDQUNwRyxJQUFJLENBQUMsWUFBWSxFQUFFLEVBQUMsV0FBVyxFQUFFLDJCQUEyQixFQUFFLFVBQVUsRUFBRSx1QkFBdUIsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxRQUFRLEVBQUUsY0FBYyxFQUFFLEtBQUssRUFBQyxDQUFDLENBQzNKLElBQUksQ0FBQyxhQUFhLEVBQUUsRUFBQyxXQUFXLEVBQUUsZ0NBQWdDLEVBQUUsVUFBVSxFQUFFLGdCQUFnQixFQUFDLENBQUMsQ0FDbEcsSUFBSSxDQUFDLE9BQU8sRUFBRSxFQUFDLFdBQVcsRUFBRSwwQkFBMEIsRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFDLENBQUMsQ0FDaEYsSUFBSSxDQUFDLFNBQVMsRUFBRSxFQUFDLFdBQVcsRUFBRSw0QkFBNEIsRUFBRSxVQUFVLEVBQUUsc0JBQXNCLEVBQUMsQ0FBQyxDQUNoRyxJQUFJLENBQUMsWUFBWSxFQUFFLEVBQUMsV0FBVyxFQUFFLHlCQUF5QixFQUFFLFVBQVUsRUFBRSxnQkFBZ0IsRUFBQyxDQUFDLENBQzFGLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBQyxXQUFXLEVBQUUsdUJBQXVCLEVBQUUsVUFBVSxFQUFFLGdCQUFnQixFQUFDLENBQUMsQ0FDdEYsSUFBSSxDQUFDLFVBQVUsRUFBRSxFQUFDLFdBQVcsRUFBRSx1QkFBdUIsRUFBRSxVQUFVLEVBQUUsYUFBYSxFQUFDLENBQUMsQ0FDbkYsSUFBSSxDQUFDLGlCQUFpQixFQUFFLEVBQUMsV0FBVyxFQUFFLDJCQUEyQixFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQUMsQ0FBQyxDQUM1RixJQUFJLENBQUMsV0FBVyxFQUFFLEVBQUMsV0FBVyxFQUFFLDhCQUE4QixFQUFFLFVBQVUsRUFBRSwwQkFBMEIsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxRQUFRLEVBQUMsQ0FBQyxDQUN6SSxJQUFJLENBQUMseUJBQXlCLEVBQUUsRUFBQyxXQUFXLEVBQUUsNkJBQTZCLEVBQUUsVUFBVSxFQUFFLG1CQUFtQixFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLFFBQVEsRUFBQyxDQUFDLENBQy9JLElBQUksQ0FBQyxjQUFjLEVBQUUsRUFBQyxXQUFXLEVBQUUsNkJBQTZCLEVBQUUsVUFBVSxFQUFFLG1CQUFtQixFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUMsQ0FBQyxDQUNqSCxJQUFJLENBQUMsb0JBQW9CLEVBQUUsRUFBQyxXQUFXLEVBQUUsbUNBQW1DLEVBQUUsVUFBVSxFQUFFLHVCQUF1QixFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLFFBQVEsRUFBQyxDQUFDLENBQ3BKLElBQUksQ0FBQyxRQUFRLEVBQUUsRUFBQyxXQUFXLEVBQUUsMkJBQTJCLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFBSSxNQUFNLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FDbkcsSUFBSSxDQUFDLFdBQVcsRUFBRSxFQUFDLFdBQVcsRUFBRSwwQkFBMEIsRUFBRSxVQUFVLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsUUFBUSxFQUFDLENBQUMsQ0FDM0gsSUFBSSxDQUFDLDRCQUE0QixFQUFFLEVBQUMsV0FBVyxFQUFFLDhCQUE4QixFQUFFLFVBQVUsRUFBRSxtQkFBbUIsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxRQUFRLEVBQUMsQ0FBQyxDQUNuSixJQUFJLENBQUMsZ0JBQWdCLEVBQUUsRUFBQyxXQUFXLEVBQUUsK0JBQStCLEVBQUUsVUFBVSxFQUFFLG9CQUFvQixFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLFFBQVEsRUFBQyxDQUFDLENBR3pJLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxFQUFDLFdBQVcsRUFBRSw4QkFBOEIsRUFBRSxVQUFVLEVBQUUsMEJBQTBCLEVBQUMsQ0FBQyxDQUM5RyxJQUFJLENBQUMscUJBQXFCLEVBQUUsRUFBQyxXQUFXLEVBQUUsa0NBQWtDLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBQyxDQUFDLENBQ3hHLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxFQUFDLFdBQVcsRUFBRSw4QkFBOEIsRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFDLENBQUMsQ0FDOUYsU0FBUyxDQUFDLEVBQUMsVUFBVSxFQUFFLEdBQUcsRUFBQyxDQUFDLENBQUM7QUFDN0IsbUJBQWlCLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFBO0NBQ3BDLENBQUMsQ0FBQTs7O0FDL0ZGLFlBQVksQ0FBQzs7OztBQUtiLFNBQVMsYUFBYSxDQUFDLEVBQUUsRUFBRSxLQUFLLEVBQUU7O0FBRWhDLE1BQUksQ0FBQyxhQUFhLEdBQUcsVUFBVSxZQUFZLEVBQUU7O0FBRTNDLFdBQU8sQ0FBQyxHQUFHLENBQUMsd0JBQXdCLENBQUMsQ0FBQTs7QUFFckMsUUFBSSxRQUFRLEdBQUcsRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDOztBQUUxQixTQUFLLENBQUMsSUFBSSxDQUFDLDRCQUE0QixFQUFFLFlBQVksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUksRUFBRTtBQUM3RSxjQUFRLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFBO0tBRXZCLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBVSxLQUFLLEVBQUU7QUFDeEIsY0FBUSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQztLQUN0QixDQUFDLENBQUM7O0FBRUgsV0FBTyxRQUFRLENBQUMsT0FBTyxDQUFDO0dBRXpCLENBQUM7O0FBRUYsTUFBSSxDQUFDLHFCQUFxQixHQUFHLFVBQVUsV0FBVyxFQUFFOztBQUVsRCxRQUFJLFFBQVEsR0FBRyxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7QUFDMUIsV0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQTs7QUFFeEIsU0FBSyxDQUFDLElBQUksQ0FBQyxvQ0FBb0MsRUFBRSxXQUFXLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxJQUFJLEVBQUU7QUFDcEYsY0FBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQTtLQUN2QixDQUFDLENBQUMsS0FBSyxDQUFDLFVBQVUsS0FBSyxFQUFFO0FBQ3hCLGNBQVEsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUM7S0FDdEIsQ0FBQyxDQUFDOztBQUVILFdBQU8sUUFBUSxDQUFDLE9BQU8sQ0FBQztHQUV6QixDQUFBOztBQUVELE1BQUksQ0FBQyxxQkFBcUIsR0FBRyxVQUFVLFNBQVMsRUFBRSxXQUFXLEVBQUU7O0FBRTdELFFBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQzs7QUFFMUIsU0FBSyxDQUFDLEdBQUcsQ0FBQyxxQ0FBcUMsR0FBRyxTQUFTLEdBQUcsR0FBRyxHQUFHLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxJQUFJLEVBQUU7QUFDNUcsY0FBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQTtLQUN2QixDQUFDLENBQUMsS0FBSyxDQUFDLFVBQVUsS0FBSyxFQUFFO0FBQ3hCLGNBQVEsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUM7S0FDdEIsQ0FBQyxDQUFDO0FBQ0gsV0FBTyxRQUFRLENBQUMsT0FBTyxDQUFDO0dBR3pCLENBQUE7O0FBSUQsTUFBSSxDQUFDLGdCQUFnQixHQUFHLFVBQVUsRUFBRSxFQUFFOztBQUVwQyxXQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFBOztBQUV0QixRQUFJLFFBQVEsR0FBRyxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7O0FBRTFCLFNBQUssQ0FBQyxHQUFHLENBQUMsb0JBQW9CLEdBQUcsRUFBRSxHQUFHLFdBQVcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUksRUFBRTtBQUN6RSxhQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFBO0FBQ3JCLGFBQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUE7QUFDakIsY0FBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7S0FDNUIsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFVLEtBQUssRUFBRTtBQUN4QixjQUFRLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0tBQ3RCLENBQUMsQ0FBQzs7QUFFSCxXQUFPLFFBQVEsQ0FBQyxPQUFPLENBQUM7R0FFekIsQ0FBQTs7QUFFRCxNQUFJLENBQUMsWUFBWSxHQUFHLFVBQVUsT0FBTyxFQUFFO0FBQ3JDLFFBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQTs7QUFFekIsU0FBSyxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsR0FBRyxPQUFPLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxJQUFJLEVBQUU7QUFDbEUsYUFBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQTtBQUN4QixhQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFBO0FBQ2pCLGNBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUE7S0FDdkIsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFVLEtBQUssRUFBRTtBQUN4QixjQUFRLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0tBQ3RCLENBQUMsQ0FBQzs7QUFFSCxXQUFPLFFBQVEsQ0FBQyxPQUFPLENBQUE7R0FDeEIsQ0FBQTs7QUFHRCxNQUFJLENBQUMsUUFBUSxHQUFHLFVBQVUsT0FBTyxFQUFFO0FBQ2pDLFFBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQTs7QUFFekIsU0FBSyxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsR0FBRyxPQUFPLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxJQUFJLEVBQUU7QUFDOUQsYUFBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQTtBQUN4QixhQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFBO0FBQ2pCLGNBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUE7S0FDdkIsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFVLEtBQUssRUFBRTtBQUN4QixjQUFRLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0tBQ3RCLENBQUMsQ0FBQzs7QUFFSCxXQUFPLFFBQVEsQ0FBQyxPQUFPLENBQUE7R0FDeEIsQ0FBQTtDQUdGOztBQUVELE9BQU8sQ0FBQyxNQUFNLENBQUMsb0JBQW9CLEVBQUUsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUMsQ0FFN0QsT0FBTyxDQUFDLFNBQVMsRUFBRSxDQUFDLFdBQVcsRUFBRSxVQUFVLFNBQVMsRUFBRTtBQUNyRCxTQUFPLFNBQVMsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFBO0NBQ3ZDLENBQUMsQ0FBQyxDQUVGLE9BQU8sQ0FBQyxlQUFlLEVBQUUsQ0FBQyxXQUFXLEVBQUUsVUFBVSxTQUFTLEVBQUU7QUFDM0QsU0FBTyxTQUFTLENBQUMsMEJBQTBCLENBQUMsQ0FBQTtDQUM3QyxDQUFDLENBQUMsQ0FFRixPQUFPLENBQUMsUUFBUSxFQUFFLENBQUMsV0FBVyxFQUFFLFVBQVUsU0FBUyxFQUFFO0FBQ3BELFNBQU8sU0FBUyxDQUFDLG1CQUFtQixDQUFDLENBQUE7Q0FDdEMsQ0FBQyxDQUFDLENBRUYsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDLFdBQVcsRUFBRSxVQUFVLFNBQVMsRUFBRTtBQUNuRCxTQUFPLFNBQVMsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFBO0NBQ3JDLENBQUMsQ0FBQyxDQUdGLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxXQUFXLEVBQUUsVUFBVSxTQUFTLEVBQUU7QUFDbEQsU0FBTyxTQUFTLENBQUMsaUJBQWlCLENBQUMsQ0FBQTtDQUNwQyxDQUFDLENBQUMsQ0FFRixPQUFPLENBQUMsRUFBRSxFQUFFLENBQUMsV0FBVyxFQUFFLFVBQVUsU0FBUyxFQUFFO0FBQzlDLFNBQU8sU0FBUyxDQUFDLGlCQUFpQixDQUFDLENBQUE7Q0FDcEMsQ0FBQyxDQUFDLENBRUYsT0FBTyxDQUFDLGVBQWUsRUFBRSxDQUFDLElBQUksRUFBRSxPQUFPLEVBQUUsYUFBYSxDQUFDLENBQUMsQ0FBQzs7O0FDcEk1RCxZQUFZLENBQUM7O0FBRWIsT0FBTyxDQUFDLElBQUksR0FBRyxVQUFVLE1BQU0sRUFBRSxLQUFLLEVBQUUsWUFBWSxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUU7O0FBRXJFLFdBQU8sQ0FBQyxHQUFHLENBQUMsYUFBYSxFQUFFLFlBQVksQ0FBQyxDQUFBOztBQUV4QyxVQUFNLENBQUMsUUFBUSxHQUFHLFlBQVk7QUFDMUIsYUFBSyxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsR0FBRyxZQUFZLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsSUFBSSxFQUFFOztBQUVqRSxtQkFBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUE7QUFDOUIsbUJBQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxJQUFJLEtBQUssTUFBTSxDQUFDLENBQUE7O0FBRXhDLGdCQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssTUFBTSxFQUFFO0FBQ3RCLHNCQUFNLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUE7QUFDdkIsc0JBQU0sQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFBO0FBQ2hCLHNCQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsVUFBVSxJQUFJLEVBQUU7QUFDbEMsMEJBQU0sQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFBO2lCQUM1QyxDQUFDLENBQUE7YUFDTDtTQUNKLENBQUMsQ0FBQTtLQUNMLENBQUE7O0FBRUQsVUFBTSxDQUFDLFdBQVcsR0FBRyxVQUFVLE9BQU8sRUFBRSxHQUFHLEVBQUU7O0FBRXpDLFlBQUksT0FBTyxDQUFDLHlDQUF5QyxDQUFDLEVBQUU7O0FBRXBELG1CQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxPQUFPLENBQUMsQ0FBQTtBQUM5QixtQkFBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQTs7QUFFaEIsaUJBQUssQ0FBQyxHQUFHLENBQUMsOEJBQThCLEdBQUcsT0FBTyxHQUFHLEdBQUcsR0FBRyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsSUFBSSxFQUFFOztBQUV4RixzQkFBTSxDQUFDLFFBQVEsRUFBRSxDQUFBO2FBRXBCLENBQUMsQ0FBQTtTQUVMO0tBQ0osQ0FBQTs7QUFFRCxVQUFNLENBQUMsUUFBUSxFQUFFLENBQUE7Q0FFcEIsQ0FBQTs7O0FDeENELFlBQVksQ0FBQzs7QUFFYixPQUFPLENBQUMsUUFBUSxHQUFHLFVBQVUsTUFBTSxFQUFFLEtBQUssRUFBRSxZQUFZLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxhQUFhLEVBQUU7O0FBRTNGLFVBQU0sQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFBOztBQUU1QixVQUFNLENBQUMsV0FBVyxHQUFHLFlBQVksQ0FBQyxXQUFXLENBQUE7O0FBRTdDLFVBQU0sQ0FBQyxHQUFHLEdBQUc7QUFDVCxZQUFJLEVBQUUsR0FBRztBQUNULGVBQU8sRUFBRSxZQUFZLENBQUMsV0FBVztBQUNqQyxjQUFNLEVBQUUsR0FBRztBQUNYLGFBQUssRUFBRSxFQUFFO0FBQ1Qsa0JBQVUsRUFBRSxJQUFJO0tBQ25CLENBQUE7O0FBRUQsaUJBQWEsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksRUFBRTs7QUFFekQsY0FBTSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUE7O0FBRWxCLFlBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtBQUNkLHlCQUFhLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxRQUFRLEVBQUU7QUFDOUQsc0JBQU0sQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFBO2FBQzdCLENBQUMsQ0FBQTtTQUNMO0tBQ0osQ0FBQyxDQUFBOztBQUVGLFVBQU0sQ0FBQyxRQUFRLEdBQUcsVUFBVSxPQUFPLEVBQUUsR0FBRyxFQUFFOztBQUV0QyxlQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxPQUFPLENBQUMsQ0FBQTtBQUMvQixlQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsQ0FBQTs7QUFFdkIsV0FBRyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUE7O0FBRW5CLGFBQUssQ0FBQyxJQUFJLENBQUMsMkJBQTJCLEdBQUcsT0FBTyxFQUFFLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUksRUFBRTtBQUMzRSxtQkFBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQTs7O0FBR2pCLHFCQUFTLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFBO1NBQ2pELENBQUMsQ0FBQztLQUVOLENBQUE7Q0FDSixDQUFBOzs7OzsrQ0MxQ29CLGdEQUFnRDs7QUFFckUsWUFBWSxDQUFDOztBQUViLE9BQU8sQ0FBQyxTQUFTLEdBQUcsVUFBVSxNQUFNLEVBQUUsS0FBSyxFQUFFLFlBQVksRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLGFBQWEsRUFBRSxLQUFLLEVBQUU7O0FBRW5HLFVBQU0sQ0FBQyxHQUFHLEdBQUc7QUFDVCxrQkFBVSxFQUFFLElBQUk7S0FDbkIsQ0FBQTs7QUFFRCxVQUFNLENBQUMsU0FBUyxHQUFHLFVBQVUsT0FBTyxFQUFFLEdBQUcsRUFBRTs7QUFFdkMsYUFBSyxDQUFDLEdBQUcsQ0FBQyw4QkFBOEIsR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUksRUFBRTs7QUFFdEYsZ0JBQUksSUFBSSxFQUFFOztBQUVOLDZCQUFhLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxJQUFJLEVBQUU7O0FBRWxELDBCQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQTs7QUFFbkIsMEJBQU0sQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFBO0FBQ2hCLDBCQUFNLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsVUFBVSxJQUFJLEVBQUU7QUFDbkMsOEJBQU0sQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFBO3FCQUM1QyxDQUFDLENBQUE7aUJBQ0wsQ0FBQyxDQUFBO2FBQ0wsTUFBTTtBQUNILHNCQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQTthQUN0QjtTQUVKLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBVSxLQUFLLEVBQUUsRUFFekIsQ0FBQyxDQUFDO0tBQ04sQ0FBQTs7QUFFRCxVQUFNLENBQUMsU0FBUyxHQUFHLFlBQVk7O0FBRTNCLGVBQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUE7OztBQUd4QixZQUFJLE1BQU0sQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFOztBQUV2QixnQkFBSSxHQUFHLEdBQUcsd0NBQXdDLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsZUFBZSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHLGtCQUFrQixHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFBOztBQUV0SixpQkFBSyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FDVCxPQUFPLENBQUMsVUFBVSxJQUFJLEVBQUU7O0FBRXJCLHNCQUFNLENBQUMsR0FBRyxHQUFHLGNBQWMsQ0FBQTtBQUMzQixzQkFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUE7YUFFdEIsQ0FBQyxDQUFBO1NBRVQsTUFDRyxNQUFNLENBQUMsR0FBRyxHQUFHLGtCQUFrQixDQUFBO0tBRXRDLENBQUE7O0FBR0QsVUFBTSxDQUFDLFdBQVcsR0FBRyxVQUFVLElBQUksRUFBRTtBQUNqQyxZQUFJLENBQUMsSUFBSSxFQUNMLE9BQU8sSUFBSSxDQUFBLEtBQ1Y7QUFDRCxtQkFBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUE7U0FDakM7S0FDSixDQUFBOztBQUVELGlCQUFhLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxJQUFJLEVBQUU7O0FBRXpELGNBQU0sQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFBOztBQUVsQixZQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7QUFDZCx5QkFBYSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsUUFBUSxFQUFFO0FBQzlELHNCQUFNLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQTthQUM3QixDQUFDLENBQUE7U0FDTDtLQUVKLENBQUMsQ0FBQTtDQUNMLENBQUE7OztBQzVFRCxZQUFZLENBQUM7O0FBRWIsT0FBTyxDQUFDLEtBQUssR0FBRyxVQUFVLE1BQU0sRUFBRSxLQUFLLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBRTs7QUFFMUQsV0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQTs7QUFFekIsVUFBTSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7O0FBRXJCLFVBQU0sQ0FBQyxhQUFhLEdBQUcsVUFBVSxPQUFPLEVBQUU7QUFDdEMsZUFBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQTtBQUNwQixZQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFNBQVMsRUFBRSxFQUFFLEVBQUUsRUFBRSxZQUFZLEVBQzNFLENBQUMsQ0FBQztLQUNOLENBQUE7O0FBRUQsVUFBTSxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUE7O0FBRXRCLFVBQU0sQ0FBQyxnQkFBZ0IsR0FBRyxZQUFZOztBQUVsQyxhQUFLLENBQUMsR0FBRyxDQUFDLHdCQUF3QixDQUFDLENBQzlCLE9BQU8sQ0FBQyxVQUFVLElBQUksRUFBRTtBQUNyQixrQkFBTSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0FBQy9ELG1CQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQTtTQUNwQyxDQUFDLENBQUE7S0FDVCxDQUFBOztBQUVELFFBQUksWUFBWSxDQUFDLE9BQU8sRUFBRTtBQUN0QixjQUFNLENBQUMsV0FBVyxHQUFHLFlBQVksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFBO0tBQ2hEOztBQUVELFVBQU0sQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFBO0NBRTNDLENBQUE7OztBQy9CRCxZQUFZLENBQUM7Ozs7QUFJYixJQUFJLFNBQVMsR0FBRyxPQUFPLENBQUMseUJBQXlCLENBQUMsQ0FBQTtBQUNsRCxJQUFJLFNBQVMsR0FBRyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUE7O0FBRXBDLE9BQU8sQ0FBQyxNQUFNLENBQUMsb0JBQW9CLEVBQUUsRUFBRSxDQUFDLENBQ3RDLFNBQVMsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxTQUFTLEVBQUUsVUFBVSxPQUFPLEVBQUU7QUFDckQsU0FBTyxVQUFVLEtBQUssRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFO0FBQ2xDLE9BQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7R0FDbkIsQ0FBQztDQUNILENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxZQUFZLEVBQUUsWUFBWTtBQUN0QyxTQUFPLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFDLENBQUE7QUFDL0IsU0FBTztBQUNMLFlBQVEsRUFBRSxHQUFHO0FBQ2IsU0FBSyxFQUFFO0FBQ0wsVUFBSSxFQUFFLEdBQUc7QUFDVCxnQkFBVSxFQUFHLEdBQUc7S0FDakI7QUFDRCxZQUFRLEVBQUUsNkJBQTZCO0dBQ3hDLENBQUE7Q0FDRixDQUFDOzs7O0NBSUQsU0FBUyxDQUFDLGNBQWMsRUFBRSxDQUFDLE9BQU8sRUFBRSxXQUFXLEVBQUUsVUFBVSxLQUFLLEVBQUUsU0FBUyxFQUFFO0FBQzVFLFNBQU87QUFDTCxZQUFRLEVBQUUsR0FBRztBQUNiLFNBQUssRUFBRTtBQUNMLFVBQUksRUFBRSxHQUFHO0FBQ1QsZ0JBQVUsRUFBRSxHQUFHO0tBQ2hCO0FBQ0QsZUFBVyxFQUFFLDZCQUE2QjtBQUMxQyxRQUFJLEVBQUUsY0FBVSxLQUFLLEVBQUUsRUFBRSxFQUFFLElBQUksRUFBRTs7QUFFL0IsVUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsV0FBVzs7O0FBRy9CLGFBQUssQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsR0FBRyxFQUFFO0FBQ2hELGlCQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsQ0FBQTtBQUN2QixjQUFJLEdBQUcsQ0FBQyxPQUFPLEVBQ2IsS0FBSyxDQUFDLFVBQVUsQ0FBQyxXQUFXLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQTtBQUM1QyxlQUFLLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUE7U0FDOUIsQ0FBQyxDQUFBOztBQUVKLFdBQUssQ0FBQyxPQUFPLEdBQUcsQ0FDZCxFQUFDLElBQUksRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBQyxFQUM1QixFQUFDLElBQUksRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBQyxDQUNoQyxDQUFBO0FBQ0QsV0FBSyxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUE7S0FDNUI7R0FDRixDQUFBO0NBQ0YsQ0FBQyxDQUFDLENBRUYsU0FBUyxDQUFDLGdCQUFnQixFQUFFLENBQUMsT0FBTyxFQUFFLGdCQUFnQixFQUFFLFVBQVUsRUFBRSxVQUFVLEtBQUssRUFBRSxjQUFjLEVBQUUsUUFBUSxFQUFFO0FBQzlHLFNBQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsQ0FBQTtBQUNoQyxTQUFPO0FBQ0wsWUFBUSxFQUFFLEdBQUc7QUFDYixTQUFLLEVBQUU7QUFDTCxVQUFJLEVBQUUsR0FBRztBQUNULGdCQUFVLEVBQUcsR0FBRztBQUNoQixhQUFPLEVBQUcsR0FBRztLQUNkOzs7O0FBSUQsUUFBSSxFQUFFLGNBQVUsS0FBSyxFQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUU7Ozs7Ozs7OztBQVMvQixXQUFLLENBQUMsTUFBTSxHQUFHLFNBQVMsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFBOztBQUVsRCxhQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBQyxJQUFJLENBQUMsQ0FBQTtBQUN6QixhQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUE7QUFDbkMsYUFBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFBO0FBQy9CLGFBQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFBOztBQUczQixVQUFJLEdBQUcsR0FBRyxnQ0FBZ0MsR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFBO0FBQzFELGFBQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUE7QUFDaEIsa0JBQVksQ0FBQyxnQ0FBZ0MsR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7QUFDL0QsYUFBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUE7Ozs7Ozs7Ozs7Ozs7QUFlL0MsZUFBUyxZQUFZLENBQUMsUUFBUSxFQUFFO0FBQzlCLGFBQUssQ0FBQyxHQUFHLENBQUMsUUFBUSxpQ0FBaUMsQ0FDaEQsT0FBTyxDQUFDLFVBQVMsZUFBZSxFQUFFOzs7QUFHakMsaUJBQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFDLEVBQUUsQ0FBQyxDQUFBO0FBQ3BCLFlBQUUsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7QUFDakQsaUJBQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFBO0FBQzVCLGVBQUssQ0FBQyxPQUFPLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQTtTQUNoQyxDQUFDLENBQUM7T0FDTjs7QUFFRCxhQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBQyxLQUFLLENBQUMsQ0FBQTs7Ozs7Ozs7Ozs7Ozs7O0FBaUIxQixXQUFLLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQyxhQUFhLEVBQUUsQ0FBQTtLQUM1QztHQUNGLENBQUE7Q0FDRixDQUFDLENBQUMsQ0FBQTs7O0FDdElMLFlBQVksQ0FBQzs7OztBQUliLElBQUksU0FBUyxHQUFHLE9BQU8sQ0FBQyx5QkFBeUIsQ0FBQyxDQUFBOztBQUdsRCxTQUFTLDRCQUE0QixDQUFDLEdBQUcsRUFBRTtBQUN2QyxRQUFJLElBQUksR0FBRyxFQUFFLENBQUE7QUFDYixTQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsR0FBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtBQUNqQyxXQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxHQUFHLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQTtBQUNqRSxZQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBO0tBQ3BCO0FBQ0QsV0FBTyxJQUFJLENBQUE7Q0FDZDs7QUFHRCxPQUFPLENBQUMsTUFBTSxDQUFDLGdCQUFnQixFQUFFLEVBQUUsQ0FBQyxDQUNoQyxTQUFTLENBQUMsWUFBWSxFQUFFLENBQUMsU0FBUyxFQUM5QixVQUFVLE9BQU8sRUFBRTtBQUNmLFdBQU8sVUFBVSxLQUFLLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRTtBQUNoQyxXQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0tBQ3JCLENBQUM7Q0FDTCxDQUNKLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxFQUFFLFlBQVk7QUFDL0IsV0FBTztBQUNILGdCQUFRLEVBQUUsR0FBRztBQUNiLGFBQUssRUFBRTtBQUNILGdCQUFJLEVBQUUsR0FBRztTQUNaO0FBQ0QsZ0JBQVEsRUFBRSw2QkFBNkI7S0FDMUMsQ0FBQTtDQUNKLENBQUMsQ0FBQyxTQUFTLENBQUMsa0JBQWtCLEVBQUUsWUFBWTtBQUN6QyxXQUFPO0FBQ0gsZ0JBQVEsRUFBRSxHQUFHO0FBQ2IsYUFBSyxFQUFFO0FBQ0gsZ0JBQUksRUFBRSxHQUFHO0FBQ1QsaUJBQUssRUFBRSxHQUFHO0FBQ1YsbUJBQU8sRUFBRSxHQUFHO1NBQ2Y7QUFDRCxtQkFBVyxFQUFFLDBCQUEwQjs7Ozs7OztBQU92QyxZQUFJLEVBQUUsY0FBVSxNQUFNLEVBQUUsT0FBTyxFQUFFO0FBQzdCLG1CQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFBO0FBQ2xCLG1CQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQTtBQUNoQyxtQkFBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQTtBQUNuQixtQkFBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQTtTQUN0Qjs7Ozs7Ozs7Ozs7Ozs7Ozs7O0tBbUJKLENBQUE7Q0FDSixDQUFDLENBQUMsU0FBUyxDQUFDLFNBQVMsRUFBRSxZQUFZO0FBQ2hDLFdBQU87QUFDSCxnQkFBUSxFQUFFLEdBQUc7QUFDYixhQUFLLEVBQUU7QUFDSCxnQkFBSSxFQUFFLEdBQUc7QUFDVCxrQkFBTSxFQUFFLEdBQUc7U0FDZDtBQUNELGdCQUFRLEVBQUUsaUVBQWlFO0FBQzNFLFlBQUksRUFBRSxjQUFVLEtBQUssRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFO0FBQzdCLGlCQUFLLENBQUMsSUFBSSxHQUFHLFVBQVUsR0FBRyxFQUFFO0FBQ3hCLHVCQUFRLENBQUMsR0FBRyxFQUFFLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7YUFDbEQsQ0FBQTtTQUNKO0tBQ0osQ0FBQTtDQUNKLENBQUMsQ0FBQyxTQUFTLENBQUMsY0FBYyxFQUFFLFlBQVk7QUFDckMsV0FBTztBQUNILGdCQUFRLEVBQUUsR0FBRztBQUNiLGFBQUssRUFBRTtBQUNILGdCQUFJLEVBQUUsR0FBRztTQUNaOztBQUVELGdCQUFRLEVBQUUscUNBQXFDO0FBQy9DLFlBQUksRUFBRSxjQUFVLEtBQUssRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFO0FBQzdCLGlCQUFLLENBQUMsUUFBUSxHQUFHLFVBQVUsSUFBSSxFQUFFO0FBQzdCLHVCQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFBO0FBQ2pCLHVCQUFPLGVBQWUsR0FBRyxJQUFJLENBQUMsV0FBVyxFQUFFLEdBQUcsTUFBTSxDQUFBO2FBQ3ZELENBQUE7U0FDSjtLQUNKLENBQUE7Q0FDSixDQUFDLENBQUMsU0FBUyxDQUFDLGFBQWEsRUFBRSxZQUFZO0FBQ3BDLFdBQU87QUFDSCxnQkFBUSxFQUFFLEdBQUc7QUFDYixhQUFLLEVBQUU7QUFDSCxnQkFBSSxFQUFFLE9BQU87U0FDaEI7QUFDRCxnQkFBUSxFQUFFLHdJQUF3STs7Ozs7S0FLckosQ0FBQTtDQUNKLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBVSxFQUFFLFlBQVk7QUFDakMsV0FBTztBQUNILGdCQUFRLEVBQUUsR0FBRztBQUNiLGFBQUssRUFBRTtBQUNILG1CQUFPLEVBQUUsR0FBRztTQUNmO0FBQ0QsZ0JBQVEsRUFBRSw0QkFBNEI7QUFDdEMsWUFBSSxFQUFFLGNBQVUsS0FBSyxFQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUU7QUFDN0IsbUJBQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUE7QUFDdkIsbUJBQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUE7U0FDckI7S0FDSixDQUFBO0NBQ0osQ0FBQyxDQUFDLFNBQVMsQ0FBQyxrQkFBa0IsRUFBRSxZQUFZO0FBQ3pDLFdBQU87QUFDSCxnQkFBUSxFQUFFLEdBQUc7QUFDYixhQUFLLEVBQUU7QUFDSCxtQkFBTyxFQUFFLEdBQUc7U0FDZjtBQUNELG1CQUFXLEVBQUUsbUNBQW1DO0tBQ25ELENBQUM7Q0FDTCxDQUFDLENBRUQsU0FBUyxDQUFDLDJCQUEyQixFQUFFLFlBQVk7O0FBQ2hELFdBQU87QUFDSCxnQkFBUSxFQUFFLEdBQUc7QUFDYixhQUFLLEVBQUU7QUFDSCxtQkFBTyxFQUFFLEdBQUc7U0FDZjtBQUNELG1CQUFXLEVBQUUsb0NBQW9DO0tBQ3BELENBQUM7Q0FDTCxDQUFDLENBRUQsU0FBUyxDQUFDLFNBQVMsRUFBRSxZQUFZO0FBQzlCLFdBQU87QUFDSCxnQkFBUSxFQUFFLEdBQUc7QUFDYixhQUFLLEVBQUU7QUFDSCxnQkFBSSxFQUFFLEdBQUc7U0FDWjtBQUNELGdCQUFRLEVBQUUsa0VBQWtFO0tBQy9FLENBQUE7Q0FDSixDQUFDLENBRUQsU0FBUyxDQUFDLFFBQVEsRUFBRSxDQUFDLFdBQVcsRUFDN0IsVUFBVSxTQUFTLEVBQUU7QUFDakIsV0FBTztBQUNILGdCQUFRLEVBQUUsR0FBRztBQUNiLGFBQUssRUFBRTtBQUNILGVBQUcsRUFBRSxHQUFHO0FBQ1IsaUJBQUssRUFBRSxHQUFHO0FBQ1Ysa0JBQU0sRUFBRSxHQUFHO0FBQ1gsaUJBQUssRUFBRSxHQUFHO1NBQ2I7QUFDRCxZQUFJLEVBQUUsY0FBVSxLQUFLLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRTs7QUFFL0IscUJBQVMsUUFBUSxHQUFHOztBQUVoQix1QkFBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUE7QUFDeEIscUJBQUssQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRTtBQUM3QiwyQkFBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtBQUNsQiwyQkFBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQTtBQUNkLDJCQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFBO2lCQUNyQixDQUFDLENBQUE7YUFDTDs7QUFFRCxxQkFBUyxjQUFjLEdBQUc7QUFDdEIsb0JBQUksSUFBSSxHQUFHLElBQUksTUFBTSxDQUFDLGFBQWEsQ0FBQyxTQUFTLEVBQUUsQ0FBQzs7QUFFaEQsb0JBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxFQUFFLFNBQVMsQ0FBQyxDQUFDO0FBQ3BDLG9CQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFBRSxXQUFXLENBQUMsQ0FBQztBQUN0Qyx1QkFBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFBOzs7O0FBSS9CLG9CQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFBOzs7O0FBSTdDLG9CQUFJLENBQUMsR0FBRyxDQUFDLENBQUE7QUFDVCxxQkFBSyxJQUFJLEdBQUcsSUFBSSxLQUFLLENBQUMsS0FBSyxFQUFFO0FBQ3pCLHdCQUFJLEtBQUssQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxFQUFFOztBQUVqQyw0QkFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7QUFDMUMsNEJBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDO0FBQzNDLHlCQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQTtxQkFDWjtpQkFDSjs7QUFFRCxvQkFBSSxPQUFPLEdBQUcsRUFBRSxDQUFDOzs7QUFHakIsdUJBQU8sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxTQUFTLEVBQUUsU0FBUyxDQUFDLENBQUM7QUFDeEMsdUJBQU8sQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDOzs7QUFJeEIsb0JBQUksS0FBSyxHQUFHLElBQUksTUFBTSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQzs7QUFFbEYsc0JBQU0sQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsYUFBYSxFQUN4RCxVQUFVLE1BQU0sRUFBRTtBQUNkLHlCQUFLLENBQUMsTUFBTSxDQUFDLFlBQVk7QUFDckIsaUNBQVMsQ0FBQyxJQUFJLENBQUMsYUFBYSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQzs7Ozs7Ozs7O3FCQVNqRCxDQUFDLENBQUM7aUJBQ04sQ0FBQyxDQUFDOztBQUVQLHFCQUFLLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQTs7Ozs7Ozs7Ozs7QUFXbkIscUJBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O2FBeUM3Qjs7QUFFRCwwQkFBYyxFQUFFLENBQUE7U0FFbkI7S0FDSixDQUFDO0NBQ0wsQ0FDSixDQUFDOzs7O0NBSUQsU0FBUyxDQUFDLFlBQVksRUFBRSxZQUFZO0FBQ2pDLFdBQU87QUFDSCxnQkFBUSxFQUFFLEdBQUc7QUFDYixhQUFLLEVBQUU7QUFDSCxnQkFBSSxFQUFFLEdBQUc7QUFDVCxnQkFBSSxFQUFFLEdBQUc7U0FDWjtBQUNELG1CQUFXLEVBQUUsMkJBQTJCO0FBQ3hDLFlBQUksRUFBRSxjQUFVLEtBQUssRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFOztBQUU3QixnQkFBSSxHQUFHLEdBQUcsNEJBQTRCLENBQUMsU0FBUyxDQUFDLHlCQUF5QixDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFBO0FBQ3ZGLGdCQUFJLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUE7QUFDcEMsaUJBQUssQ0FBQyxVQUFVLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUE7QUFDckMsaUJBQUssQ0FBQyxVQUFVLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFBO0FBQzVDLGlCQUFLLENBQUMsVUFBVSxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLElBQUksRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUE7U0FFbkQ7S0FDSixDQUFBO0NBQ0osQ0FBQyxDQUVELFNBQVMsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxTQUFTLEVBQy9CLFVBQVUsT0FBTyxFQUFFOztBQUVmLFFBQUksU0FBUyxHQUFHO0FBQ1osZ0JBQVEsRUFBRSxHQUFHO0tBQ2hCLENBQUM7QUFDRixhQUFTLENBQUMsSUFBSSxHQUFHLFVBQVUsS0FBSyxFQUFFLE9BQU8sRUFBRSxVQUFVLEVBQUU7QUFDbkQsWUFBSSxJQUFJLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztBQUNwQyxZQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxZQUFZO0FBQzVCLGdCQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0FBQ2pDLGtCQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsQ0FBQztBQUM5QixtQkFBTyxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLFlBQVk7QUFDNUMsb0JBQUksSUFBSSxHQUFHLFNBQVMsQ0FBQztBQUNyQixxQkFBSyxDQUFDLE1BQU0sQ0FBQyxZQUFZO0FBQ3JCLHlCQUFLLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7aUJBQ25ELENBQUMsQ0FBQztBQUNILHNCQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsQ0FBQzthQUNsQyxDQUFDLENBQUM7U0FDTixDQUFDLENBQUM7S0FDTixDQUFDO0FBQ0YsV0FBTyxTQUFTLENBQUM7Q0FFcEIsQ0FDSixDQUFDLENBRUQsU0FBUyxDQUFDLGVBQWUsRUFBRSxDQUFDLGFBQWEsRUFDdEMsVUFBVSxXQUFXLEVBQUU7QUFDbkIsV0FBTztBQUNILGdCQUFRLEVBQUUsR0FBRztBQUNiLGFBQUssRUFBRTtBQUNILGdCQUFJLEVBQUUsR0FBRztTQUNaO0FBQ0QsbUJBQVcsRUFBRSxnQ0FBZ0M7OztBQUc3QyxZQUFJLEVBQUUsY0FBVSxLQUFLLEVBQUUsRUFBRSxFQUFFLElBQUksRUFBRTs7QUFHN0IsbUJBQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsQ0FBQTs7QUFFaEMsaUJBQUssQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDOztBQUVsQixpQkFBSyxDQUFDLFVBQVUsR0FBRyxVQUFVLEtBQUssRUFBRTtBQUNoQyxxQkFBSyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO0FBQzlCLHFCQUFLLENBQUMsT0FBTyxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUE7QUFDckMscUJBQUssQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDO2FBRXJCLENBQUM7O0FBRUYsaUJBQUssQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFBO0FBQ2YsaUJBQUssQ0FBQyxjQUFjLEdBQUcsWUFBWTtBQUMvQix1QkFBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQTtBQUN4Qix1QkFBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUE7QUFDdkIsdUJBQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUE7QUFDbEIsMkJBQVcsQ0FBQyxzQkFBc0IsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLE1BQU0sRUFBRTtBQUN4RSwyQkFBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsRUFBRSxNQUFNLENBQUMsQ0FBQTtBQUMxQyx5QkFBSyxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUE7QUFDakIsd0JBQUksTUFBTSxDQUFDLE1BQU0sRUFDYixLQUFLLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztBQUNkLDRCQUFJLEVBQUUsU0FBUztBQUNmLDJCQUFHLEVBQUUsZ0ZBQWdGO3FCQUN4RixDQUFDLENBQUEsS0FFRixLQUFLLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztBQUNkLDRCQUFJLEVBQUUsU0FBUztBQUNmLDJCQUFHLEVBQUUsTUFBTSxDQUFDLE9BQU87cUJBQ3RCLENBQUMsQ0FBQTtpQkFDVCxDQUFDLENBQUE7YUFDTCxDQUFBO0FBQ0QsbUJBQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUE7U0FDckI7S0FDSixDQUFBO0NBQ0osQ0FDSixDQUFDOzs7Ozs7Ozs7O0NBVUQsU0FBUyxDQUFDLFdBQVcsRUFBRSxDQUFDLFVBQVUsRUFBRSxVQUFVLFFBQVEsRUFBRTtBQUNyRCxXQUFPO0FBQ0gsZ0JBQVEsRUFBRSxHQUFHO0FBQ2IsZ0JBQVEsRUFBRSwyR0FBMkc7QUFDckgsWUFBSSxFQUFFLGNBQVUsS0FBSyxFQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUU7O0FBRTdCLGlCQUFLLENBQUMsVUFBVSxHQUFHLFVBQVUsS0FBSyxFQUFFO0FBQ2hDLHFCQUFLLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7YUFDakMsQ0FBQzs7Ozs7O1NBTUw7S0FDSixDQUFBO0NBQ0osQ0FBQyxDQUFDLENBRUYsU0FBUyxDQUFDLFFBQVEsRUFBRSxDQUFDLFVBQVUsRUFBRSxVQUFVLFFBQVEsRUFBRTtBQUNsRCxXQUFPOztBQUVILGdCQUFRLEVBQUUsR0FBRzs7QUFFYixZQUFJLEVBQUUsY0FBVSxLQUFLLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRTs7QUFFbkMsbUJBQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUE7O0FBRXRCLGtCQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsVUFBVSxDQUFDO0FBQ3ZCLHlCQUFTLEVBQUUsSUFBSTtBQUNmLHNCQUFNLEVBQUUsT0FBTztBQUNmLHFCQUFLLEVBQUUsTUFBTTtBQUNiLHdCQUFRLEVBQUUsSUFBSTtBQUNkLHNCQUFNLEVBQUUsTUFBTTtBQUNkLHVCQUFPLEVBQUUsQ0FBQztBQUNWLDBCQUFVLEVBQUUsQ0FBQztBQUNiLHdCQUFRLEVBQUU7QUFDTixzQkFBRSxFQUFFLEVBQUU7QUFDTix3QkFBSSxFQUFFLEVBQUU7QUFDUiwwQkFBTSxFQUFFLEVBQUU7QUFDViw0QkFBUSxFQUFFLE1BQU07QUFDaEIsNEJBQVEsRUFBRSxNQUFNO2lCQUNuQjthQUNKLENBQUMsQ0FBQztTQUNOO0tBQ0osQ0FBQztDQUNMLENBQUMsQ0FBQyxDQUVGLFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxXQUFXLEVBQUUsVUFBVSxTQUFTLEVBQUU7QUFDdkQsV0FBTzs7QUFFSCxnQkFBUSxFQUFFLEdBQUc7O0FBRWIsWUFBSSxFQUFFLGNBQVUsS0FBSyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUU7O0FBRW5DLGlCQUFLLENBQUMsY0FBYyxHQUFHLFVBQVUsSUFBSSxFQUFFO0FBQ25DLHVCQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsQ0FBQTtBQUM3Qix5QkFBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQTthQUN2QixDQUFBOztBQUVELGlCQUFLLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQTs7QUFFdEIsa0JBQU0sQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLFNBQVMsQ0FDbEM7QUFDSSxxQkFBSyxFQUFFLENBQUM7QUFDUiwwQkFBVSxFQUFFLENBQUMsRUFBRTtBQUNmLDJCQUFXLEVBQUUsQ0FBQztBQUNkLHVCQUFPLEVBQUUsaUJBQVUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUU7QUFDcEMsMkJBQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxDQUFBO0FBQy9CLHlCQUFLLENBQUMsTUFBTSxDQUFDLFlBQVk7O0FBRXJCLDRCQUFJLEdBQUcsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUMsT0FBTyxFQUFFLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO0FBQ2xFLCtCQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUE7QUFDdEMsaUNBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFBO3FCQUNyQyxDQUFDLENBQUM7Ozs7O2lCQUtOO0FBQ0Qsc0JBQU0sRUFBRSxnQkFBVSxLQUFLLEVBQUUsS0FBSyxFQUFFO0FBQzVCLHdCQUFJLEdBQUcsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUMsT0FBTyxFQUFFLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO0FBQ2xFLDBCQUFNLENBQUMsY0FBYyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLFNBQVMsQ0FBQyxDQUFDO0FBQ3pFLHlCQUFLLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUE7aUJBQ3BDOzthQUdKLENBQ0osQ0FBQTtTQUNKO0tBQ0osQ0FBQztDQUNMLENBQUMsQ0FBQyxDQUVGLFNBQVMsQ0FBQyxTQUFTLEVBQUUsWUFBWTtBQUM5QixXQUFPLFVBQVUsS0FBSyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUU7QUFDcEMsZUFBTyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxVQUFVLEtBQUssRUFBRTtBQUM5QyxnQkFBSSxLQUFLLENBQUMsS0FBSyxLQUFLLEVBQUUsRUFBRTtBQUNwQixxQkFBSyxDQUFDLE1BQU0sQ0FBQyxZQUFZO0FBQ3JCLHlCQUFLLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztpQkFDOUIsQ0FBQyxDQUFDOztBQUVILHFCQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7YUFDMUI7U0FDSixDQUFDLENBQUM7S0FDTixDQUFDO0NBQ0wsQ0FBQyxDQUVELFNBQVMsQ0FBQyxVQUFVLEVBQUUsQ0FBQyxPQUFPLEVBQUUsVUFBVSxLQUFLLEVBQUU7QUFDOUMsV0FBTztBQUNILGdCQUFRLEVBQUUsR0FBRztBQUNiLGdCQUFRLEVBQUUsd0lBQXdJO0FBQ2xKLFlBQUksRUFBRSxjQUFVLEtBQUssRUFBRSxFQUFFLEVBQUUsS0FBSyxFQUFFO0FBQzlCLGlCQUFLLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxVQUFVLEtBQUssRUFBRTtBQUNyQyxvQkFBSSxLQUFLLEVBQUU7QUFDUCx5QkFBSyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsR0FBRyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxJQUFJLEVBQUU7QUFDMUQsNkJBQUssQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQTtxQkFFNUIsQ0FBQyxDQUFBO2lCQUNMO2FBQ0osQ0FBQyxDQUFDO1NBQ047S0FDSixDQUFBO0NBQ0osQ0FBQyxDQUFDLENBRUYsU0FBUyxDQUFDLGFBQWEsRUFBRSxDQUFDLE9BQU8sRUFBRSxVQUFVLEtBQUssRUFBRTtBQUNqRCxXQUFPO0FBQ0gsZ0JBQVEsRUFBRSxHQUFHO0FBQ2IsYUFBSyxFQUFFO0FBQ0gsc0JBQVUsRUFBRSxHQUFHO1NBQ2xCO0FBQ0QsZ0JBQVEsRUFBRSw0SUFBNEk7QUFDdEosWUFBSSxFQUFFLGNBQVUsS0FBSyxFQUFFLEVBQUUsRUFBRSxLQUFLLEVBQUU7QUFDOUIsbUJBQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxDQUFBO0FBQzFCLGlCQUFLLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxVQUFVLEtBQUssRUFBRTtBQUN6Qyx1QkFBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLEVBQUUsS0FBSyxDQUFDLENBQUE7QUFDbkMsb0JBQUksS0FBSyxFQUFFO0FBQ1AseUJBQUssQ0FBQyxHQUFHLENBQUMscUJBQXFCLEdBQUcsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsSUFBSSxFQUFFO0FBQzFELDZCQUFLLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUE7cUJBRTdCLENBQUMsQ0FBQTtpQkFDTDthQUVKLENBQUMsQ0FBQztTQUNOO0tBQ0osQ0FBQTtDQUNKLENBQUMsQ0FBQyxDQUVGLFNBQVMsQ0FBQyxhQUFhLEVBQUUsQ0FBQyxPQUFPLEVBQUUsYUFBYSxFQUFFLFVBQVUsS0FBSyxFQUFFLFdBQVcsRUFBRTtBQUM3RSxXQUFPO0FBQ0gsZ0JBQVEsRUFBRSxHQUFHO0FBQ2IsYUFBSyxFQUFFO0FBQ0gsc0JBQVUsRUFBRSxHQUFHO1NBQ2xCO0FBQ0QsZ0JBQVEsRUFBRSxrQ0FBa0M7QUFDNUMsWUFBSSxFQUFFLGNBQVUsS0FBSyxFQUFFLEVBQUUsRUFBRSxLQUFLLEVBQUU7QUFDOUIsbUJBQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxDQUFBO0FBQzFCLGlCQUFLLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxVQUFVLEtBQUssRUFBRTtBQUN6Qyx1QkFBTyxDQUFDLEdBQUcsQ0FBQywrQkFBK0IsRUFBRSxLQUFLLENBQUMsQ0FBQTtBQUNuRCxvQkFBSSxXQUFXLENBQUMsVUFBVSxFQUFFLEVBQUU7QUFDMUIsd0JBQUksS0FBSyxFQUFFOztBQUVQLDZCQUFLLENBQUMsR0FBRyxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxPQUFPLENBQUMsSUFBSSxFQUFFO0FBQzNELGlDQUFLLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUE7eUJBQzdCLEVBQUUsU0FBUyxLQUFLLENBQUMsS0FBSyxFQUFFO0FBQ3JCLG1DQUFPLENBQUMsR0FBRyxDQUFDLGlCQUFpQixDQUFDLENBQUE7QUFDOUIsbUNBQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUE7eUJBQ3JCLENBQUMsQ0FBQTtxQkFFTDtpQkFFSjthQUdKLENBQUMsQ0FBQztTQUNOO0tBQ0osQ0FBQTtDQUNKLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxTQUFTLEVBQUUsTUFBTSxFQUFFLFVBQVUsT0FBTyxFQUFFLElBQUksRUFBRTtBQUNqRSxXQUFPO0FBQ0gsZ0JBQVEsRUFBRSxHQUFHO0FBQ2IsZ0JBQVEsRUFBRSxvQkFBWTtBQUNsQixtQkFBTyx5RUFBeUUsQ0FBQztTQUNwRjtBQUNELGFBQUssRUFBRTtBQUNILGdCQUFJLEVBQUUsT0FBTztTQUNoQjtBQUNELFlBQUksRUFBRSxjQUFVLEtBQUssRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFO0FBQ25DLGlCQUFLLENBQUMsU0FBUyxHQUFHLFlBQVk7QUFDMUIsdUJBQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7YUFDeEQsQ0FBQTtTQUNKO0FBQ0QsZUFBTyxFQUFFLElBQUk7S0FDaEIsQ0FBQztDQUNMLENBQUMsQ0FBQyxDQUFDOzs7QUN0a0JSLFlBQVksQ0FBQzs7OztBQUliLElBQUksU0FBUyxHQUFHLE9BQU8sQ0FBQyx5QkFBeUIsQ0FBQyxDQUFBOztBQUVsRCxPQUFPLENBQUMsTUFBTSxDQUFDLGlCQUFpQixFQUFFLEVBQUUsQ0FBQyxDQUVoQyxTQUFTLENBQUMsZUFBZSxFQUFFLENBQUMsT0FBTyxFQUFFLFVBQVUsS0FBSyxFQUFFOztBQUVuRCxXQUFPO0FBQ0gsZ0JBQVEsRUFBRSxHQUFHO0FBQ2IsYUFBSyxFQUFFO0FBQ0gsZUFBRyxFQUFFLEdBQUc7QUFDUixpQkFBSyxFQUFHLEdBQUc7U0FDZDtBQUNELG1CQUFXLEVBQUUsK0JBQStCO0FBQzVDLFlBQUksRUFBRSxjQUFVLEtBQUssRUFBRSxFQUFFLEVBQUUsS0FBSyxFQUFFO0FBQzlCLG1CQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsQ0FBQTs7QUFFMUIsaUJBQUssQ0FBQyxVQUFVLEdBQUcsVUFBVSxLQUFLLEVBQUU7O0FBRWhDLHVCQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxDQUFBO0FBQzFCLHVCQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFBO0FBQ2xCLG9CQUFJLEVBQUUsR0FBRyxJQUFJLFFBQVEsRUFBRSxDQUFDOzs7QUFHeEIsa0JBQUUsQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0FBQ2pDLHVCQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQTs7QUFFckIscUJBQUssQ0FBQyxJQUFJLENBQUMsbUJBQW1CLEVBQUUsRUFBRSxFQUFFO0FBQ2hDLG1DQUFlLEVBQUUsSUFBSTtBQUNyQiwyQkFBTyxFQUFFO0FBQ0wsc0NBQWMsRUFBRSxTQUFTO3FCQUM1QjtBQUNELG9DQUFnQixFQUFFLE9BQU8sQ0FBQyxRQUFRO2lCQUNyQyxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsSUFBSSxFQUFFO0FBQ3ZCLHlCQUFLLENBQUMsR0FBRyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFBO0FBQzdCLHlCQUFLLENBQUMsYUFBYSxHQUFHLFVBQVUsQ0FBQTtpQkFDbkMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFVLElBQUksRUFBRTtBQUNyQiwyQkFBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQUE7aUJBQzlCLENBQUMsQ0FBQzthQUNOLENBQUE7U0FFSjtLQUNKLENBQUE7Q0FDSixDQUFDLENBQUMsQ0FFRixTQUFTLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxPQUFPLEVBQUUsVUFBVSxLQUFLLEVBQUU7QUFDdEQsV0FBTztBQUNILGdCQUFRLEVBQUUsR0FBRztBQUNiLGFBQUssRUFBRTtBQUNILGVBQUcsRUFBRSxHQUFHO0FBQ1IsaUJBQUssRUFBRyxHQUFHOztTQUVkO0FBQ0QsbUJBQVcsRUFBRSxrQ0FBa0M7QUFDL0MsWUFBSSxFQUFFLGNBQVUsS0FBSyxFQUFFLEVBQUUsRUFBRSxLQUFLLEVBQUU7O0FBRTlCLG1CQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUE7O0FBR2hDLGlCQUFLLENBQUMsTUFBTSxHQUFHOztBQUVYLCtCQUFlLEVBQUcsdUVBQXVFO2FBQzVGLENBQUE7Ozs7Ozs7O0FBU0QsaUJBQUssQ0FBQyxnQkFBZ0IsR0FBRyxFQUFFLENBQUE7OztBQUczQixpQkFBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLEVBQUUsRUFBRTtBQUMxQixvQkFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFBO0FBQ3BCLG9CQUFJLENBQUMsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUNkLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxDQUFBO0FBQ2YscUJBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUM7QUFDeEIsd0JBQUksRUFBRSxDQUFDO0FBQ1AsNEJBQVEsRUFBRSxLQUFLO2lCQUNsQixDQUFDLENBQUE7YUFDTDs7QUFFRCx5QkFBYSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUE7O0FBRWhDLGlCQUFLLENBQUMsWUFBWSxHQUFHLFVBQVUsQ0FBQyxFQUFFOztBQUU5QixvQkFBSSxDQUFDLENBQUMsUUFBUSxFQUFFOztBQUVaLHlCQUFLLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUM7QUFDbkIsNEJBQUksRUFBRSxDQUFDLENBQUMsSUFBSTtBQUNaLCtCQUFPLEVBQUUsRUFBRTtxQkFDZCxDQUFDLENBQUE7aUJBRUwsTUFBTTs7QUFFSCx5QkFBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtBQUMvQyw0QkFBSSxLQUFLLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssQ0FBQyxDQUFDLElBQUksRUFDcEMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQTtxQkFDckM7aUJBQ0o7YUFFSixDQUFBOztBQUVELHFCQUFTLGFBQWEsQ0FBQyxPQUFPLEVBQUU7O0FBRTVCLHFCQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLGdCQUFnQixDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFDbEQsS0FBSyxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUE7O0FBRTlDLHFCQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtBQUNyQyx3QkFBSSxJQUFJLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQTtBQUNwQyx5QkFBSyxDQUFDLGdCQUFnQixDQUFDLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFBO2lCQUNuRDthQUNKO1NBRUo7S0FDSixDQUFBO0NBQ0osQ0FBQyxDQUFDLENBQUE7OztBQ3pIUCxZQUFZLENBQUM7Ozs7O0FBS2IsT0FBTyxDQUFDLE1BQU0sQ0FBQyxhQUFhLEVBQUUsRUFBRSxDQUFDLENBQy9CLE1BQU0sQ0FBQyxTQUFTLEVBQUUsWUFBWTtBQUM1QixTQUFPLFVBQVUsS0FBSyxFQUFFLFNBQVMsRUFBRTtBQUNqQyxRQUFJLEdBQUcsR0FBRyxFQUFFLENBQUM7QUFDYixTQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtBQUNyQyxTQUFHLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUM7S0FDN0I7O0FBRUQsUUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFBO0FBQ1gsUUFBSSxTQUFTLEVBQ1gsR0FBRyxHQUFHLEdBQUcsQ0FBQyxXQUFXLEVBQUUsQ0FBQzs7QUFFMUIsUUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFBO0FBQ1QsV0FBTyxHQUFHLENBQUM7R0FDWixDQUFDO0NBQ0gsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxZQUFZLEVBQUUsWUFBWTtBQUNsQyxTQUFPLFVBQVUsS0FBSyxFQUFFO0FBQ3RCLFFBQUksT0FBTyxLQUFLLEFBQUMsS0FBSyxRQUFRLEVBQUU7QUFDOUIsVUFBSSxHQUFHLEdBQUcsRUFBRSxDQUFBO0FBQ1osV0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7QUFDckMsWUFBSSxHQUFHLEtBQUssRUFBRSxFQUFFO0FBQ2QsY0FBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxFQUNsQixHQUFHLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUEsS0FFbkQsR0FBRyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUE7U0FDdEIsTUFDSTtBQUNILGNBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sRUFDbEIsR0FBRyxHQUFHLEdBQUcsR0FBRyxHQUFHLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUEsS0FFL0QsR0FBRyxHQUFHLEdBQUcsR0FBRyxHQUFHLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQTtTQUNsQztPQUNGO0FBQ0QsYUFBTyxHQUFHLENBQUE7S0FDWCxNQUNDLE9BQU8sS0FBSyxDQUFBO0dBQ2YsQ0FBQTtDQUNGLENBQUM7OztDQUdELE1BQU0sQ0FBQyxrQkFBa0IsRUFBRSxZQUFZO0FBQ3RDLFNBQU8sVUFBVSxLQUFLLEVBQUU7QUFDdEIsUUFBSSxPQUFPLEtBQUssQUFBQyxLQUFLLFFBQVEsRUFBRTtBQUM5QixVQUFJLEdBQUcsR0FBRyxFQUFFLENBQUE7QUFDWixXQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtBQUNyQyxZQUFJLEdBQUcsS0FBSyxFQUFFLEVBQUU7QUFDWixhQUFHLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQTtTQUN0QixNQUNJO0FBQ0QsYUFBRyxHQUFHLEdBQUcsR0FBRyxHQUFHLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQTtTQUNsQztPQUNGO0FBQ0QsYUFBTyxHQUFHLENBQUE7S0FDWCxNQUNDLE9BQU8sS0FBSyxDQUFBO0dBQ2YsQ0FBQTtDQUNGLENBQUMsQ0FHRCxNQUFNLENBQUMsV0FBVyxFQUFFLFlBQVk7QUFDL0IsU0FBTyxVQUFVLElBQUksRUFBRTtBQUNyQixRQUFJLElBQUksRUFDTixPQUFPLEtBQUssQ0FBQSxLQUVaLE9BQU8sSUFBSSxDQUFBO0dBQ2QsQ0FBQTtDQUNGLENBQUMsQ0FBQyxNQUFNLENBQUMsY0FBYyxFQUFFLFlBQVk7QUFDcEMsU0FBTyxVQUFVLENBQUMsRUFBRTtBQUNsQixRQUFJLENBQUMsS0FBSyxHQUFHLEVBQ1gsT0FBTyxVQUFVLENBQUEsS0FDZCxJQUFJLENBQUMsS0FBSyxHQUFHLEVBQ2hCLE9BQU8sTUFBTSxDQUFBLEtBQ1YsSUFBSSxDQUFDLEtBQUssSUFBSSxFQUNqQixPQUFPLG1CQUFtQixDQUFBLEtBQ3ZCLElBQUksQ0FBQyxLQUFLLElBQUksRUFDakIsT0FBTyxXQUFXLENBQUEsS0FDZixJQUFJLENBQUMsS0FBSyxLQUFLLEVBQ2xCLE9BQU8scUJBQXFCLENBQUEsS0FFNUIsT0FBTyxTQUFTLENBQUE7R0FDbkIsQ0FBQTtDQUNGLENBQUMsQ0FBQyxNQUFNLENBQUMsVUFBVSxFQUFFLENBQUMsVUFBVSxFQUFDLFVBQVUsUUFBUSxFQUFFO0FBQ3BELFNBQU8sVUFBVSxDQUFDLEVBQUUsT0FBTyxFQUFFO0FBQzNCLFFBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUNWLE9BQU8sd0RBQXdELENBQUEsS0FDNUQsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUNkLE9BQU8sZ0VBQWdFLENBQUEsS0FDcEU7QUFDSCxVQUFJLEtBQUssR0FBRyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUE7QUFDN0IsVUFBSSxLQUFLLEtBQUssU0FBUyxFQUFFO0FBQ3ZCLGVBQU8sS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFBO09BQ2hCLE1BQU07QUFDTCxpQkFBTyx5QkFBeUIsR0FBRyxDQUFDLENBQUM7U0FDdEM7S0FDRjtHQUNGLENBQUE7Q0FDRixDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsY0FBYyxFQUFFLENBQUMsV0FBVyxFQUFDLFVBQVUsU0FBUyxFQUFFO0FBQzNELFNBQU8sVUFBVSxDQUFDLEVBQUU7QUFDbEIsU0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFNBQVMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFHLEVBQUU7QUFDMUMsVUFBSSxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLENBQUMsRUFDekIsT0FBTyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFBO0tBQzNCO0dBRUYsQ0FBQTtDQUNGLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxhQUFhLEVBQUUsWUFBWTtBQUNwQyxTQUFPLFVBQVUsQ0FBQyxFQUFFOztBQUVsQixRQUFJLENBQUMsS0FBSyxDQUFDLEVBQ1QsT0FBTyxTQUFTLENBQUEsS0FDYixJQUFJLENBQUMsS0FBSyxDQUFDLEVBQ2QsT0FBTyxXQUFXLENBQUEsS0FDZixJQUFJLENBQUMsS0FBSyxDQUFDLEVBQ2QsT0FBTyxXQUFXLENBQUEsS0FFbEIsT0FBTyxTQUFTLENBQUE7R0FDbkIsQ0FBQTtDQUNGLENBQUMsQ0FFRCxNQUFNLENBQUMsY0FBYyxFQUFFLFlBQVk7QUFDbEMsU0FBTyxVQUFVLENBQUMsRUFBRTs7QUFFbEIsUUFBSSxDQUFDLEtBQUssQ0FBQyxFQUNULE9BQU8sUUFBUSxDQUFBLEtBQ1osSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUNkLE9BQU8sUUFBUSxDQUFBLEtBQ1osSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUNkLE9BQU8sV0FBVyxDQUFBLEtBRWxCLE9BQU8sU0FBUyxDQUFBO0dBQ25CLENBQUE7Q0FDRixDQUFDLENBRUYsTUFBTSxDQUFDLGNBQWMsRUFBRSxZQUFZO0FBQ2pDLFNBQU8sVUFBVSxJQUFJLEVBQUU7QUFDckIsUUFBSSxJQUFJLEtBQUssU0FBUyxFQUFFOztBQUV0QixhQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxPQUFPLElBQUksQ0FBQyxDQUFBO0FBQzlCLGFBQU8sZUFBZSxHQUFHLElBQUksQ0FBQyxXQUFXLEVBQUUsR0FBRyxNQUFNLENBQUE7S0FDckQ7O0dBRUYsQ0FBQTtDQUNGLENBQUMsQ0FBQyxNQUFNLENBQUMsY0FBYyxFQUFFLENBQUMsU0FBUyxFQUFDLFVBQVUsT0FBTyxFQUFFO0FBQ3RELFNBQU8sVUFBVSxJQUFJLEVBQUUsUUFBUSxFQUFFO0FBQy9CLFFBQUksSUFBSSxLQUFLLFNBQVMsRUFBRTtBQUNwQixVQUFJLElBQUksS0FBSyxDQUFDLENBQUMsRUFDYixPQUFPLGdCQUFnQixDQUFBLEtBQ3BCLElBQUksSUFBSSxLQUFLLENBQUMsRUFDakIsT0FBTyxJQUFJLENBQUEsS0FDUixJQUFJLElBQUksS0FBSyxDQUFDLEVBQ2pCLE9BQU8sd0JBQXdCLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLFFBQVEsRUFBRSxZQUFZLENBQUMsR0FBRSxHQUFHLENBQUEsS0FDM0UsSUFBSSxJQUFJLEtBQUssQ0FBQyxFQUNqQixPQUFPLDJCQUEyQixDQUFBLEtBRWxDLE9BQU8sZ0JBQWdCLEdBQUcsSUFBSSxDQUFBO0tBQ25DLE1BQ0MsT0FBTyxTQUFTLENBQUE7R0FDbkIsQ0FBQTtDQUNGLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsRUFBRSxZQUFZO0FBQ3hDLFNBQU8sVUFBVSxDQUFDLEVBQUU7QUFDbEIsUUFBSSxDQUFDLEtBQUssR0FBRyxFQUNYLE9BQU8sT0FBTyxDQUFBLEtBQ1gsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUNoQixPQUFPLFdBQVcsQ0FBQSxLQUNmLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUNmLE9BQU8sRUFBRSxDQUFBLEtBRVQsT0FBTyxTQUFTLENBQUE7R0FDbkIsQ0FBQTtDQUNGLENBQUMsQ0FBQyxNQUFNLENBQUMsWUFBWSxFQUFFLFlBQVk7QUFDbEMsU0FBTyxVQUFVLENBQUMsRUFBRTtBQUNsQixRQUFJLENBQUMsS0FBSyxTQUFTLEVBQUU7QUFDbkIsVUFBSSxDQUFDLEtBQUssR0FBRyxFQUNYLE9BQU8sT0FBTyxDQUFBLEtBQ1gsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUNoQixPQUFPLE9BQU8sQ0FBQTtLQUNqQjtHQUNGLENBQUE7Q0FDRixDQUFDLENBQ0QsTUFBTSxDQUFDLGNBQWMsRUFBRSxZQUFZO0FBQ2xDLFNBQU8sVUFBVSxDQUFDLEVBQUU7QUFDbEIsUUFBSSxDQUFDLEtBQUssU0FBUyxFQUFFO0FBQ25CLFVBQUksR0FBRyxDQUFBO0FBQ1AsV0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFHLEVBQUU7QUFDbEMsWUFBSSxHQUFHLEVBQ0wsR0FBRyxHQUFHLEdBQUcsR0FBRyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQSxLQUUzQixHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQTtPQUNsQjtBQUNELGFBQU8sR0FBRyxDQUFBO0tBRVg7R0FDRixDQUFBO0NBQ0YsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxhQUFhLEVBQUUsWUFBWTtBQUNqQyxTQUFPLFVBQVUsQ0FBQyxFQUFFO0FBQ2xCLFFBQUksQ0FBQyxLQUFLLFNBQVMsRUFBRTtBQUNqQixhQUFPLElBQUksR0FBRyxDQUFDLENBQUE7S0FDbEI7R0FDRixDQUFBO0NBQ0YsQ0FBQyxDQUFBOzs7QUMzTU4sWUFBWSxDQUFDOztBQUViLElBQUksU0FBUyxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsZUFBZSxFQUFFLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDLENBQUM7O0FBRTVFLFNBQVMsQ0FBQyxRQUFRLENBQUMsYUFBYSxFQUFFOztBQUVoQyxjQUFZLEVBQUUsb0VBQW9FLEdBQ2hGLHFGQUFxRixHQUNyRixnR0FBZ0c7QUFDbEcsUUFBTSxFQUFFLGdDQUFnQztBQUN4QyxlQUFhLEVBQUMsMkRBQTJELEdBQ3ZFLHdJQUF3SSxHQUN4SSx3SUFBd0ksR0FDeEksc0hBQXNIO0FBQ3hILFlBQVUsRUFBRyx5SEFBeUgsR0FDcEksMklBQTJJLEdBQzNJLDZJQUE2SSxHQUM3SSw2SUFBNkksR0FDN0ksMkRBQTJEO0FBQzdELG1CQUFpQixFQUFHLHVUQUF1VDtDQUM1VSxDQUFDLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRTs7QUFFdEIsTUFBSSxFQUFFO0FBQ0UsS0FBQyxFQUFFLG1FQUFtRTtBQUN0RSxLQUFDLEVBQUUsbUVBQW1FO0dBQ3ZFO0FBQ1AsTUFBSSxFQUFFO0FBQ0osS0FBQyxFQUFFLGtFQUFrRTtBQUNyRSxLQUFDLEVBQUUsbUVBQW1FO0dBQ3ZFO0FBQ0QsTUFBSSxFQUFFO0FBQ0osS0FBQyxFQUFFLGtFQUFrRTtBQUNyRSxLQUFDLEVBQUUsbUVBQW1FO0dBQ3ZFO0FBQ0QsTUFBSSxFQUFFO0FBQ0osS0FBQyxFQUFFLGtFQUFrRTtBQUNyRSxLQUFDLEVBQUUsbUVBQW1FO0dBQ3ZFO0FBQ0QsTUFBSSxFQUFFO0FBQ0osS0FBQyxFQUFFLGtFQUFrRTtBQUNyRSxLQUFDLEVBQUUsbUVBQW1FO0dBQ3ZFO0FBQ0QsTUFBSSxFQUFFO0FBQ0osS0FBQyxFQUFFLGtFQUFrRTtBQUNyRSxLQUFDLEVBQUUsbUVBQW1FO0dBQ3ZFO0FBQ0QsTUFBSSxFQUFFO0FBQ0osS0FBQyxFQUFFLHVFQUF1RTtBQUMxRSxLQUFDLEVBQUUsbUVBQW1FO0dBQ3ZFO0FBQ0QsTUFBSSxFQUFFO0FBQ0osS0FBQyxFQUFFLHVFQUF1RTtBQUMxRSxLQUFDLEVBQUUsbUVBQW1FO0dBQ3ZFO0FBQ0QsTUFBSSxFQUFFO0FBQ0osS0FBQyxFQUFFLGlFQUFpRTtBQUNwRSxLQUFDLEVBQUUsbUVBQW1FO0dBQ3ZFO0FBQ0QsTUFBSSxFQUFFO0FBQ0osS0FBQyxFQUFFLGlFQUFpRTtBQUNwRSxLQUFDLEVBQUUsbUVBQW1FO0dBQ3ZFO0FBQ0QsTUFBSSxFQUFFO0FBQ0osS0FBQyxFQUFFLHVFQUF1RTtBQUMxRSxLQUFDLEVBQUUsbUVBQW1FO0dBQ3ZFO0FBQ0QsTUFBSSxFQUFFO0FBQ0osS0FBQyxFQUFFLGlFQUFpRTtBQUNwRSxLQUFDLEVBQUUsbUVBQW1FO0dBQ3ZFO0FBQ0QsTUFBSSxFQUFFLEVBQUMsQ0FBQyxFQUFFLEVBQUUsRUFBRSxDQUFDLEVBQUUsRUFBRSxFQUFDO0FBQ3BCLFFBQU0sRUFBQztBQUNMLFFBQUksRUFBQyxvSkFBb0o7R0FDMUo7QUFDRCxZQUFVLEVBQUUsMklBQTJJOztDQUV4SixDQUFDLENBQUE7OztBQzVFRixZQUFZLENBQUM7O0FBRWIsT0FBTyxDQUFDLE1BQU0sQ0FBQyxlQUFlLEVBQUUsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUM7Ozs7Q0FJeEQsT0FBTyxDQUFDLFNBQVMsRUFBRSxDQUFDLFNBQVMsRUFBRSxjQUFjLEVBQUUsVUFBVSxPQUFPLEVBQUUsWUFBWSxFQUFFO0FBQy9FLE1BQUksQ0FBQyxNQUFNLEdBQUcsVUFBVSxTQUFTLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUU7O0FBRXZELGdCQUFZLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxTQUFTLENBQUMsQ0FBQTtBQUN4QyxnQkFBWSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUE7QUFDOUIsZ0JBQVksQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFBO0FBQ2xDLGdCQUFZLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQTtHQUVuQyxDQUFDOztBQUVGLE1BQUksQ0FBQyxPQUFPLEdBQUcsWUFBWTs7QUFFekIsZ0JBQVksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUE7QUFDaEMsZ0JBQVksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUE7QUFDM0IsZ0JBQVksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUE7QUFDN0IsZ0JBQVksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUE7R0FDOUIsQ0FBQzs7QUFFRixNQUFJLENBQUMsWUFBWSxHQUFHLFlBQVk7QUFDOUIsV0FBTyxZQUFZLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFBO0dBQ3JDLENBQUE7O0FBRUQsTUFBSSxDQUFDLE9BQU8sR0FBRyxZQUFZO0FBQ3pCLFdBQU8sWUFBWSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQTtHQUNoQyxDQUFBOztBQUVELE1BQUksQ0FBQyxTQUFTLEdBQUcsWUFBWTtBQUMzQixXQUFPLFlBQVksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUE7R0FDbEMsQ0FBQTs7QUFFRCxTQUFPLElBQUksQ0FBQztDQUNiLENBQUMsQ0FBQzs7OztDQUlGLE9BQU8sQ0FBQyxhQUFhLEVBQUUsQ0FBQyxPQUFPLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUUsVUFBVSxLQUFLLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxPQUFPLEVBQUU7O0FBRXJHLE1BQUksS0FBSyxHQUFHLEVBQUUsQ0FBQzs7QUFFZixNQUFJLFlBQVksR0FBRyxTQUFmLFlBQVksQ0FBYSxJQUFJLEVBQUU7QUFDakMsUUFBSSxRQUFRLEdBQUcsRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDOztBQUUxQixTQUFLLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsQ0FDMUIsT0FBTyxDQUFDLFVBQVUsSUFBSSxFQUFFLE1BQU0sRUFBRTtBQUM5QixhQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFBO0FBQ2pCLGFBQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUE7QUFDbkIsVUFBSSxJQUFJLENBQUMsU0FBUyxLQUFLLElBQUksRUFDekIsT0FBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7QUFDdEUsY0FBUSxDQUFDLE9BQU8sQ0FBQyxFQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsU0FBUyxLQUFLLElBQUksRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLEdBQUcsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksRUFBQyxDQUFDLENBQUM7S0FDaEcsQ0FBQyxDQUNGLEtBQUssQ0FBQyxVQUFVLEtBQUssRUFBRTtBQUNyQixjQUFRLENBQUMsT0FBTyxDQUFDLEVBQUMsYUFBYSxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsc0NBQXNDLEVBQUMsQ0FBQyxDQUFDO0tBQzNGLENBQUMsQ0FBQzs7QUFFTCxXQUFPLFFBQVEsQ0FBQyxPQUFPLENBQUM7R0FDekIsQ0FBQzs7QUFFRixNQUFJLGVBQWUsR0FBRyxTQUFsQixlQUFlLEdBQWU7QUFDaEMsUUFBSSxHQUFHLEdBQUcsT0FBTyxDQUFDLFlBQVksRUFBRSxDQUFBO0FBQ2hDLFFBQUksQUFBQyxHQUFHLEtBQUssU0FBUyxJQUFNLEdBQUcsS0FBSyxJQUFJLEFBQUMsRUFDdkMsT0FBTyxLQUFLLENBQUEsS0FFWixPQUFPLElBQUksQ0FBQTtHQUVkLENBQUM7O0FBRUYsTUFBSSxRQUFRLEdBQUcsU0FBWCxRQUFRLENBQWEsSUFBSSxFQUFFO0FBQzdCLFFBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQzs7QUFFMUIsU0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQ3pCLE9BQU8sQ0FBQyxVQUFVLElBQUksRUFBRSxNQUFNLEVBQUU7QUFDOUIsY0FBUSxDQUFDLE9BQU8sQ0FBQyxFQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFDLENBQUMsQ0FBQztLQUN4RSxDQUFDLENBQ0YsS0FBSyxDQUFDLFVBQVUsS0FBSyxFQUFFO0FBQ3JCLGNBQVEsQ0FBQyxPQUFPLENBQUMsRUFBQyxVQUFVLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSwyREFBMkQsRUFBQyxDQUFDLENBQUM7S0FDN0csQ0FBQyxDQUFDOztBQUVMLFdBQU8sUUFBUSxDQUFDLE9BQU8sQ0FBQztHQUN6QixDQUFDOztBQUVGLE1BQUksTUFBTSxHQUFHLFNBQVQsTUFBTSxHQUFlO0FBQ3ZCLFdBQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQTtHQUNsQixDQUFDOztBQUVGLE1BQUksc0JBQXNCLEdBQUcsU0FBekIsc0JBQXNCLENBQWEsS0FBSyxFQUFFOztBQUU1QyxRQUFJLFFBQVEsR0FBRyxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7O0FBRTFCLFNBQUssQ0FBQyxHQUFHLENBQUMsNkJBQTZCLEdBQUcsS0FBSyxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsSUFBSSxFQUFFO0FBQ3ZFLGFBQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUE7QUFDakIsY0FBUSxDQUFDLE9BQU8sQ0FBQyxFQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTSxLQUFLLElBQUksRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU8sRUFBQyxDQUFDLENBQUM7S0FDekUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFVLEtBQUssRUFBRTtBQUN4QixjQUFRLENBQUMsT0FBTyxDQUFDLEVBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsaUVBQWlFLEVBQUMsQ0FBQyxDQUFDO0tBQy9HLENBQUMsQ0FBQzs7QUFFSCxXQUFPLFFBQVEsQ0FBQyxPQUFPLENBQUM7R0FDekIsQ0FBQzs7QUFHRixPQUFLLENBQUMsTUFBTSxHQUFHLFVBQVUsSUFBSSxFQUFFO0FBQzdCLFdBQU8sWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO0dBQzNCLENBQUM7QUFDRixPQUFLLENBQUMsTUFBTSxHQUFHLFVBQVUsSUFBSSxFQUFFO0FBQzdCLFdBQU8sUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO0dBQ3ZCLENBQUM7QUFDRixPQUFLLENBQUMsTUFBTSxHQUFHLFlBQVk7QUFDekIsV0FBTyxNQUFNLEVBQUUsQ0FBQTtHQUNoQixDQUFDO0FBQ0YsT0FBSyxDQUFDLFVBQVUsR0FBRyxZQUFZO0FBQzdCLFdBQU8sZUFBZSxFQUFFLENBQUE7R0FDekIsQ0FBQztBQUNGLE9BQUssQ0FBQyxRQUFRLEdBQUcsWUFBWTtBQUMzQixXQUFPLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQTtHQUN6QixDQUFDOztBQUVGLE9BQUssQ0FBQyxzQkFBc0IsR0FBRyxVQUFVLEtBQUssRUFBRTtBQUM5QyxXQUFPLHNCQUFzQixDQUFDLEtBQUssQ0FBQyxDQUFBO0dBQ3JDLENBQUE7O0FBRUQsU0FBTyxLQUFLLENBQUM7Q0FFZCxDQUFDLENBQUMsQ0FFRixRQUFRLENBQUMsYUFBYSxFQUFFO0FBQ3ZCLGNBQVksRUFBRSxvQkFBb0I7QUFDbEMsYUFBVyxFQUFFLG1CQUFtQjtBQUNoQyxlQUFhLEVBQUUscUJBQXFCO0FBQ3BDLGdCQUFjLEVBQUUsc0JBQXNCO0FBQ3RDLGtCQUFnQixFQUFFLHdCQUF3QjtBQUMxQyxlQUFhLEVBQUUscUJBQXFCO0FBQ3BDLGFBQVcsRUFBRSxjQUFjO0NBQzVCLENBQUM7Ozs7O0NBS0QsT0FBTyxDQUFDLFNBQVMsRUFBQyxDQUFDLFNBQVMsRUFBRSxVQUFVLE9BQU8sRUFBRTs7QUFFaEQsTUFBSSxDQUFDLE1BQU0sR0FBRyxZQUFZLEVBQ3pCLENBQUM7O0FBRUYsTUFBSSxDQUFDLE9BQU8sR0FBRyxZQUFZO0FBQ3pCLFdBQU8sT0FBTyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUE7R0FDdEMsQ0FBQzs7QUFFRixNQUFJLENBQUMsR0FBRyxHQUFHLFVBQVUsR0FBRyxFQUFFO0FBQ3hCLFFBQUksSUFBSSxHQUFHLEtBQUssR0FBRyxHQUFHLENBQUE7QUFDdEIsUUFBSSxHQUFHLEdBQUcsT0FBTyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQTtBQUN0QyxXQUFPLE9BQU8sQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUE7QUFDbkMsV0FBTyxHQUFHLENBQUE7R0FDWCxDQUFBOztBQUVELFdBQVMsaUJBQWlCLEdBQUc7QUFDM0IsV0FBTyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLEdBQ2hELElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQztHQUMvQzs7QUFFRCxNQUFJLENBQUMsR0FBRyxHQUFHLFVBQVUsR0FBRyxFQUFFO0FBQ3hCLFFBQUksR0FBRyxHQUFHLGlCQUFpQixFQUFFLENBQUE7QUFDN0IsV0FBTyxDQUFDLGNBQWMsQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDLEdBQUcsR0FBRyxDQUFBO0FBQ3pDLFdBQU8sR0FBRyxDQUFBO0dBQ1gsQ0FBQTs7QUFFRCxTQUFPLElBQUksQ0FBQztDQUNiLENBQUMsQ0FBQyxDQUFBOzs7QUMxS0wsWUFBWSxDQUFDOzs7QUFHYixPQUFPLENBQUMsTUFBTSxDQUFDLFdBQVcsRUFBRSxFQUFFLENBQUMsQ0FDNUIsTUFBTSxDQUNQLE9BQU8sRUFBRSxZQUFZO0FBQ25CLE1BQUksTUFBTSxHQUNSLFNBREUsTUFBTSxDQUNFLEdBQUcsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFO0FBQzNCLFNBQUssSUFBSSxDQUFDLEdBQUcsS0FBSyxFQUFFLENBQUMsSUFBSSxLQUFLLEVBQUUsQ0FBQyxFQUFFLEVBQUUsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQTtBQUNoRCxXQUFPLEdBQUcsQ0FBQTtHQUNYLENBQUE7QUFDSCxTQUFPLE1BQU0sQ0FBQTtDQUNkLENBQ0YsQ0FDRSxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUMsT0FBTyxFQUFFLElBQUksRUFBRSxVQUFVLEtBQUssRUFBRSxFQUFFLEVBQUU7O0FBRXJELE1BQUksQ0FBQyxxQkFBcUIsR0FBRyxVQUFVLE9BQU8sRUFBRTtBQUM5QyxRQUFJLFFBQVEsR0FBRyxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7O0FBRTFCLFNBQUssQ0FBQyxHQUFHLENBQUMscUNBQXFDLEdBQUcsT0FBTyxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsSUFBSSxFQUFFO0FBQ2pGLGFBQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLENBQUMsQ0FBQTtBQUNqQyxhQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFBO0FBQ2pCLGNBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7S0FDeEIsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFVLEtBQUssRUFBRTtBQUN4QixjQUFRLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO0tBQ3pCLENBQUMsQ0FBQztBQUNILFdBQU8sUUFBUSxDQUFDLE9BQU8sQ0FBQztHQUN6QixDQUFBOzs7O0FBSUQsTUFBSSxDQUFDLGFBQWEsR0FBRyxVQUFVLE9BQU8sRUFBRTtBQUN0QyxRQUFJLFFBQVEsR0FBRyxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7O0FBRTFCLFNBQUssQ0FBQyxHQUFHLENBQUMsNkJBQTZCLEdBQUcsT0FBTyxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsSUFBSSxFQUFFO0FBQ3pFLGFBQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLENBQUMsQ0FBQTtBQUNqQyxhQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFBO0FBQ2pCLGNBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7S0FDeEIsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFVLEtBQUssRUFBRTtBQUN4QixjQUFRLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO0tBQ3pCLENBQUMsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7OztBQWdCSCxXQUFPLFFBQVEsQ0FBQyxPQUFPLENBQUM7R0FDekIsQ0FBQTs7QUFHRCxTQUFPLElBQUksQ0FBQztDQUViLENBQUMsQ0FBQyxDQUFBOzs7QUM5REwsWUFBWSxDQUFDOzs7QUFHYixJQUFJLFNBQVMsR0FBRyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUE7O0FBRXBDLE9BQU8sQ0FBQyxNQUFNLENBQUMsZUFBZSxFQUFFLEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsWUFBVztBQUNoRSxXQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFBO0FBQ2xCLFFBQUksQ0FBQyxHQUFHLElBQUksQ0FBQTtBQUNaLFFBQUksQ0FBQyxLQUFLLElBQUksRUFDWixPQUFPLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDLENBQUE7QUFDcEMsV0FBTyxTQUFTLENBQUMsYUFBYSxFQUFFLENBQUE7Q0FDbkMsQ0FBQyxDQUFBOzs7QUNYRixZQUFZLENBQUM7OztBQUdiLE9BQU8sQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxXQUFXLEVBQUUsVUFBVSxTQUFTLEVBQUU7QUFDckYsU0FBTztBQUNMLFlBQVEsRUFBRSxHQUFHO0FBQ2IsU0FBSyxFQUFFO0FBQ0wsYUFBTyxFQUFFLEdBQUc7QUFDWixlQUFTLEVBQUUsR0FBRztBQUNkLG1CQUFhLEVBQUUsR0FBRztLQUNuQjtBQUNELGVBQVcsRUFBRSxzQkFBc0I7QUFDbkMsUUFBSSxFQUFFLGNBQVUsS0FBSyxFQUFFLGdCQUFnQixFQUFFLEtBQUssRUFBRTs7QUFFOUMsV0FBSyxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUE7QUFDbkIsV0FBSyxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUE7O0FBRXBCLFdBQUssQ0FBQyxXQUFXLEdBQUcsWUFBWTtBQUM5QixZQUFJLEtBQUssQ0FBQyxTQUFTLEtBQUssU0FBUyxFQUMvQixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUEsS0FFbkQsT0FBTyxDQUFDLENBQUM7T0FDWixDQUFBOztBQUVELFdBQUssQ0FBQyxVQUFVLEdBQUcsWUFBWTtBQUM3QixZQUFJLEdBQUcsR0FBRyxFQUFFO1lBQUUsQ0FBQztZQUFFLENBQUMsQ0FBQTtBQUNsQixZQUFJLEtBQUssQ0FBQyxTQUFTLElBQUksU0FBUyxFQUFFOztBQUNoQyxXQUFDLEdBQUcsRUFBRSxDQUFBO0FBQ04sY0FBSSxLQUFLLENBQUMsU0FBUyxHQUFHLENBQUMsRUFDckIsQ0FBQyxHQUFHLEtBQUssQ0FBQyxTQUFTLENBQUE7QUFDckIsZUFBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQ3BCLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFBO1NBQ2xCO0FBQ0QsZUFBTyxHQUFHLENBQUE7T0FDWCxDQUFBOztBQUVELFdBQUssQ0FBQyxLQUFLLEdBQUcsWUFBWTtBQUN4QixZQUFJLEdBQUcsR0FBRyxFQUFFO1lBQUUsQ0FBQyxDQUFDO0FBQ2hCLGFBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLFNBQVMsRUFBRSxDQUFDLEVBQUUsRUFDbEMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUE7QUFDakIsZUFBTyxHQUFHLENBQUE7T0FDWCxDQUFBOzs7QUFHRCxXQUFLLENBQUMsc0JBQXNCLEdBQUcsWUFBWTs7QUFFekMsWUFBSSxLQUFLLENBQUMsU0FBUyxLQUFLLFNBQVMsRUFBRTtBQUNqQyxjQUFJLEtBQUssQ0FBQyxTQUFTLEdBQUcsQ0FBQyxFQUFFO0FBQ3ZCLGdCQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFBO0FBQ25ELGdCQUFJLENBQUMsS0FBSyxLQUFLLENBQUMsU0FBUyxFQUN2QixLQUFLLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQTtXQUN0QjtTQUNGO09BRUYsQ0FBQTs7QUFFRCxXQUFLLENBQUMsa0JBQWtCLEdBQUcsWUFBWTtBQUNyQyxZQUFJLEtBQUssQ0FBQyxTQUFTLEtBQUssU0FBUyxFQUFFO0FBQ2pDLGNBQUksR0FBRyxHQUFHLEVBQUU7Y0FBRSxDQUFDLENBQUM7QUFDaEIsZUFBSyxDQUFDLHNCQUFzQixFQUFFLENBQUE7QUFDOUIsY0FBSSxNQUFNLEdBQUcsS0FBSyxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUMsU0FBUyxDQUFBO0FBQzlDLGVBQUssQ0FBQyxHQUFHLE1BQU0sRUFBRSxDQUFDLEdBQUcsTUFBTSxHQUFHLEtBQUssQ0FBQyxTQUFTLEVBQUUsQ0FBQyxFQUFFLEVBQUU7QUFDbEQsbUJBQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUE7QUFDdEMsZ0JBQUksQ0FBQyxHQUFHLEtBQUssQ0FBQyxTQUFTLEVBQ3JCLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFBO1dBQ2xCO0FBQ0QsaUJBQU8sR0FBRyxDQUFBO1NBQ1g7T0FDRixDQUFBOztBQUVELFdBQUssQ0FBQyxTQUFTLEdBQUcsWUFBWTtBQUM1QixZQUFJLEdBQUcsR0FBRyxFQUFFO1lBQUUsQ0FBQyxDQUFDO0FBQ2hCLGFBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLFNBQVMsRUFBRSxDQUFDLEVBQUUsRUFDbEMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUE7QUFDakIsZUFBTyxHQUFHLENBQUE7T0FDWCxDQUFBOztBQUVELFdBQUssQ0FBQyxlQUFlLEdBQUcsVUFBVSxDQUFDLEVBQUU7QUFDbkMsYUFBSyxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUE7QUFDakIsWUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLGFBQWEsS0FBSyxTQUFTLEVBQUU7QUFDN0MsZUFBSyxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFBO0FBQzFDLG1CQUFTLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUE7U0FDbkM7T0FDRixDQUFBOztBQUVELFdBQUssQ0FBQyxRQUFRLEdBQUcsWUFBWTs7QUFFM0IsZUFBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUE7QUFDdkMsWUFBSSxDQUFDLEdBQUcsS0FBSyxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUE7O0FBRTNCLFlBQUksQ0FBQyxHQUFHLEtBQUssQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDLFNBQVMsRUFDdkMsS0FBSyxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUE7O0FBRXJCLGVBQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUE7QUFDM0MsZUFBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQTs7QUFFeEIsWUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQTtPQUN4RCxDQUFBOztBQUVELFdBQUssQ0FBQyxRQUFRLEdBQUcsWUFBWTtBQUMzQixZQUFJLEtBQUssQ0FBQyxTQUFTLEdBQUcsQ0FBQyxFQUNyQixLQUFLLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFBO0FBQ3ZDLGVBQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUE7QUFDeEIsWUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQTtPQUN4RCxDQUFBOztBQUdELFdBQUssQ0FBQyxPQUFPLEdBQUcsVUFBVSxDQUFDLEVBQUU7QUFDM0IsWUFBSSxDQUFDLEtBQUssS0FBSyxDQUFDLE9BQU8sRUFDckIsT0FBTyxRQUFRLENBQUEsS0FFZixPQUFPLEVBQUUsQ0FBQTtPQUNaLENBQUE7O0FBRUQsV0FBSyxDQUFDLElBQUksR0FBRyxZQUFZO0FBQ3ZCLGVBQU8sS0FBSyxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUM7T0FDNUIsQ0FBQTs7QUFFRCxXQUFLLENBQUMsUUFBUSxHQUFHLFlBQVk7QUFDM0IsZUFBTyxLQUFLLENBQUMsYUFBYSxDQUFDO09BQzVCLENBQUE7O0FBRUQsV0FBSyxDQUFDLGFBQWEsR0FBRyxZQUFZO0FBQ2hDLGFBQUssQ0FBQyxhQUFhLEdBQUcsQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFBO09BQzNDLENBQUE7S0FDRjtHQUNGLENBQUE7Q0FDRixDQUFDLENBQUMsQ0FBQTs7Ozs7QUMvSEgsTUFBTSxDQUFDLE9BQU8sR0FBRyxDQUNmLE9BQU8sQ0FBQywyQkFBMkIsQ0FBQyxFQUNwQyxPQUFPLENBQUMsMEJBQTBCLENBQUMsRUFDbkMsT0FBTyxDQUFDLDZCQUE2QixDQUFDLEVBQ3RDLE9BQU8sQ0FBQyw4QkFBOEIsQ0FBQyxFQUN2QyxPQUFPLENBQUMsZ0NBQWdDLENBQUMsRUFDekMsT0FBTyxDQUFDLG9CQUFvQixDQUFDLEVBQzdCLE9BQU8sQ0FBQyxvQkFBb0IsQ0FBQyxFQUM3QixPQUFPLENBQUMsZ0JBQWdCLENBQUMsRUFDekIsT0FBTyxDQUFDLGdCQUFnQixDQUFDLENBQzFCLENBQUE7OztBQ1ZELFlBQVksQ0FBQztBQUNiLElBQUksU0FBUyxHQUFHLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQTtBQUNwQyxJQUFJLGFBQWEsR0FBRyxPQUFPLENBQUMsNEJBQTRCLENBQUMsQ0FBQTtBQUN6RCxJQUFJLElBQUksR0FBRyxPQUFPLENBQUMseUJBQXlCLENBQUMsQ0FBQTtBQUM3QyxJQUFJLEtBQUssR0FBRyxPQUFPLENBQUMsMEJBQTBCLENBQUMsQ0FBQTs7QUFFL0MsU0FBUyxZQUFZLENBQUMsS0FBSyxFQUFDO0FBQzFCLE1BQUksR0FBRyxHQUFHLDJCQUEyQixDQUFDO0FBQ3RDLE1BQUksT0FBTyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7QUFDL0IsTUFBSSxPQUFPLEtBQUssSUFBSSxFQUFFO0FBQ3BCLFdBQU8sRUFBRSxLQUFLLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFFLEtBQUssRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztHQUNqRDtBQUNELFNBQU8sRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsQ0FBQztDQUNyQzs7QUFFRCxTQUFTLHNCQUFzQixHQUFJOztBQUVqQyxNQUFJLEdBQUcsR0FBRyxFQUFFLENBQUE7QUFDWixNQUFJLEdBQUcsR0FBRyxDQUFDLENBQUM7QUFDWixPQUFLLElBQUksR0FBRyxJQUFJLGFBQWEsRUFBRTtBQUM3QixRQUFJLGFBQWEsQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEVBQUU7O0FBRXJDLFVBQUssU0FBUyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsS0FBSyxTQUFTLEVBQUU7QUFDN0MsV0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUMsSUFBSSxFQUFFLFNBQVMsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUMsQ0FBQTtBQUM5RCxXQUFHLEdBQUcsR0FBRyxHQUFHLEVBQUUsQ0FBQTtPQUNmLE1BQU07QUFDTCxlQUFPLENBQUMsR0FBRyxDQUFDLDBCQUEwQixFQUFFLEdBQUcsQ0FBQyxDQUFBO09BQzdDO0tBQ0Y7R0FDRjs7QUFFRCxPQUFNLEdBQUcsSUFBSSxJQUFJLEVBQUU7QUFDakIsUUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxFQUFFO0FBQzVCLFVBQUssU0FBUyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsS0FBSyxTQUFTLEVBQUU7QUFDN0MsV0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUMsSUFBSSxFQUFFLFNBQVMsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFDLE1BQU0sRUFBQyxDQUFBO09BQzVFO0tBQ0Y7R0FDRjs7O0FBSUQsT0FBTSxHQUFHLElBQUksS0FBSyxFQUFFO0FBQ2xCLFFBQUksS0FBSyxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsRUFBRTtBQUM3QixVQUFLLFNBQVMsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEtBQUssU0FBUyxFQUFFO0FBQzdDLFdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFDLElBQUksRUFBRSxTQUFTLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBQyxPQUFPLEVBQUMsQ0FBQTtPQUMvRTtLQUNGO0dBQ0Y7O0FBRUQsU0FBTyxHQUFHLENBQUE7Q0FFWDs7OztBQUlELE9BQU8sQ0FBQyxnQkFBZ0IsR0FBRyxVQUFVLFNBQVMsRUFBRSxTQUFTLEVBQUU7O0FBRXpELE1BQUksVUFBVSxHQUFHLFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQTs7Ozs7QUFLeEMsTUFBSSxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUMzQixPQUFPLElBQUksQ0FBQyxLQUNULElBQUksU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsRUFDakMsT0FBTyxJQUFJLENBQUEsS0FDUixJQUFJLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEVBQ2hDLE9BQU8sSUFBSSxDQUFBLEtBQ1IsSUFBSSxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUNqQyxPQUFPLElBQUksQ0FBQSxLQUNSLElBQUksU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsRUFDakMsT0FBTyxJQUFJLENBQUEsS0FDUixJQUFJLENBQUMsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLHNCQUFzQixDQUFDLEVBQ2pELE9BQU8sSUFBSSxDQUFBLEtBRVgsT0FBTyxLQUFLLENBQUE7Q0FFZixDQUFBOztBQUVELE9BQU8sQ0FBQyxrQkFBa0IsR0FBRyxZQUFZOztBQUV2QyxTQUFPLHNCQUFzQixFQUFFLENBQUE7Q0FFaEMsQ0FBQTs7QUFFRCxPQUFPLENBQUMseUJBQXlCLEdBQUcsVUFBUyxJQUFJLEVBQUU7O0FBRWpELE1BQUksSUFBSSxHQUFHLFNBQVMsQ0FBQyx5QkFBeUIsQ0FBQyxJQUFJLENBQUMsQ0FBQTtBQUNwRCxNQUFJLElBQUksR0FBRyxzQkFBc0IsRUFBRSxDQUFBOztBQUVuQyxTQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBUyxDQUFDLEVBQUU7QUFDN0IsV0FBTyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLFNBQVMsQ0FBQTtHQUNsQyxDQUFDLENBQUE7Q0FHSCxDQUFBOzs7QUMvRkQsWUFBWSxDQUFDOztBQUViLE9BQU8sQ0FBQyxJQUFJLEdBQUcsVUFBVSxNQUFNLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFFLFdBQVcsRUFBRSxNQUFNLEVBQUU7Ozs7Ozs7Ozs7O0FBV3hGLFFBQU0sQ0FBQyxXQUFXLEdBQUcsVUFBVSxJQUFJLEVBQUU7QUFDbkMsUUFBSSxDQUFDLElBQUksRUFDUCxPQUFPLElBQUksQ0FBQSxLQUNSO0FBQ0gsYUFBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUE7S0FDL0I7R0FDRixDQUFBOztBQUVELFFBQU0sQ0FBQyxlQUFlLEdBQUcsWUFBWTs7QUFFbkMsUUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksRUFBRTtBQUN4QyxZQUFNLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUE7QUFDdkIsZ0JBQVUsQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxDQUFDO0tBQ2hELENBQUMsQ0FBQTtHQUVILENBQUE7O0FBRUQsUUFBTSxDQUFDLGVBQWUsRUFBRSxDQUFBOztBQUV4QixRQUFNLENBQUMsV0FBVyxHQUFHLFVBQVUsT0FBTyxFQUFFLEdBQUcsRUFBRTs7QUFFM0MsUUFBSSxPQUFPLENBQUMseUNBQXlDLENBQUMsRUFBRTs7QUFFdEQsVUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsSUFBSSxFQUFFO0FBQ2xELGNBQU0sQ0FBQyxlQUFlLEVBQUUsQ0FBQTtPQUN6QixDQUFDLENBQUE7S0FDSDtHQUNGLENBQUE7O0FBR0QsUUFBTSxDQUFDLGlCQUFpQixHQUFHLFVBQVUsT0FBTyxFQUFFO0FBQzVDLFdBQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFBOztBQUUvQixRQUFJLE9BQU8sQ0FBQywrQ0FBK0MsQ0FBQyxFQUFFOztBQUU1RCxVQUFJLENBQUMsZUFBZSxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsTUFBTSxFQUFFO0FBQzVDLGVBQU8sQ0FBQyxHQUFHLENBQUMseUJBQXlCLEVBQUUsTUFBTSxDQUFDLENBQUE7Ozs7Ozs7Ozs7Ozs7T0FlL0MsQ0FBQyxDQUFBOzs7OztLQUtIO0dBQ0YsQ0FBQTs7QUFFRCxRQUFNLENBQUMsZ0JBQWdCLEdBQUcsVUFBVSxPQUFPLEVBQUUsR0FBRyxFQUFFO0FBQ2hELFFBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsTUFBTSxFQUFFO0FBQ3pELFlBQU0sQ0FBQyxlQUFlLEVBQUUsQ0FBQTtLQUN6QixDQUFDLENBQUE7R0FDSCxDQUFBOztBQUVELFFBQU0sQ0FBQyxZQUFZLEdBQUcsVUFBVSxHQUFHLEVBQUU7QUFDbkMsV0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLEVBQUUsR0FBRyxFQUFFLE1BQU0sQ0FBQyxDQUFBO0FBQ3hDLFdBQU8sQ0FBQyxHQUFHLENBQUMsYUFBYSxFQUFFLE1BQU0sQ0FBQyxDQUFBO0FBQ2xDLFFBQUksYUFBYSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUM7QUFDOUIsaUJBQVcsRUFBRSxxQ0FBcUM7QUFDbEQsaUJBQVcsRUFBRSxrQkFBa0I7QUFDL0IsZ0JBQVUsRUFBRSxvQkFBb0I7QUFDaEMsYUFBTyxFQUFFO0FBQ1AsZUFBTyxFQUFFLG1CQUFZO0FBQ25CLGlCQUFPLEdBQUcsQ0FBQTtTQUNYO09BQ0Y7S0FDRixDQUFDLENBQUM7R0FDSixDQUFBOzs7QUFHRCxRQUFNLENBQUMscUJBQXFCLEdBQUcsWUFBWTtBQUN6QyxXQUFPLENBQUMsR0FBRyxDQUFDLHVCQUF1QixDQUFDLENBQUE7QUFDcEMsUUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLE1BQU0sRUFBRTtBQUM1QyxhQUFPLENBQUMsR0FBRyxDQUFDLHdCQUF3QixFQUFFLE1BQU0sQ0FBQyxDQUFBO0FBQzdDLFVBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFBO0tBQ3pCLENBQUMsQ0FBQTtHQUNILENBQUE7Q0FFRixDQUFBOzs7QUN0R0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNWQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzlMQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDakJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDaFFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDbElBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dmFyIGY9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKTt0aHJvdyBmLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsZn12YXIgbD1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwobC5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxsLGwuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc30pIiwiJ3VzZSBzdHJpY3QnO1xuLypnbG9iYWwgYW5ndWxhciwgbW9tZW50LCBpbywgY29uc29sZSAqL1xuXG5yZXF1aXJlKCcuL2FkbWluX2NvbnRyb2xsZXJzLmpzJylcbnJlcXVpcmUoJy4vYWRtaW5fc2VydmljZXMuanMnKVxucmVxdWlyZSgnLi4vbW9kdWxlcy9tb2R1bGVzLmpzJylcblxuXG5hbmd1bGFyLm1vZHVsZSgndGtzLmFkbWluJywgWyduZ1Jlc291cmNlJywgJ25nUm91dGUnLCAnbmdDb29raWVzJywgJ3VpLmJvb3RzdHJhcCcsICdtb2QucGFnZXInLFxuICAgICdtb2QuYXV0aG9yaXplJywgJ21vZC5jb3VudHJpZXMnLCdhZG1pbl90a3Muc2VydmljZXMnLCAndGtzLnNlcnZpY2VzMicsJ2FkbWluX3Rrcy5jb250cm9sbGVycycsICd0a3MuZmlsdGVycycsICd0a3MuZGlyZWN0aXZlcycsJ3Rrcy5kaXJlY3RpdmVzMiddKVxuICAuY29uZmlnKHJlcXVpcmUoJy4vYWRtaW5fcm91dGVzLmpzJykpXG5cbiAgLnJ1bihbXCIkcm9vdFNjb3BlXCIsIFwiJGxvY2F0aW9uXCIsIFwiYXV0aFNlcnZpY2VcIiwgZnVuY3Rpb24gKCRyb290U2NvcGUsICRsb2NhdGlvbiwgYXV0aFNlcnZpY2UpIHtcbiAgICBjb25zb2xlLmxvZygnaGFoYScpXG4gICAgY29uc29sZS5sb2coJ2hlbGxvJylcbiAgICAkcm9vdFNjb3BlLiRvbihcIiRyb3V0ZUNoYW5nZVN0YXJ0XCIsIGZ1bmN0aW9uIChldmVudCwgbmV4dCwgY3VycmVudCkge1xuXG4gICAgICBjb25zb2xlLmxvZygnY2hhbmdpbmcnKVxuICAgICAgY29uc29sZS5sb2cobmV4dC4kJHJvdXRlKVxuXG4gICAgICBpZiAobmV4dC4kJHJvdXRlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgaWYgKG5leHQuJCRyb3V0ZS5pc1ByaXYgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgIGlmIChuZXh0LiQkcm91dGUuaXNQcml2KSB7XG4gICAgICAgICAgICBpZiAoIWF1dGhTZXJ2aWNlLmlzU2lnbmVkSW4oKSlcbiAgICAgICAgICAgICAgJGxvY2F0aW9uLnBhdGgoXCIvc2lnbmluXCIpO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0pO1xuICB9XSlcblxuICAuZmFjdG9yeSgnYXV0aEludGVyY2VwdG9yJywgWyckcScsICckbG9jYXRpb24nLCAnU2Vzc2lvbicsIGZ1bmN0aW9uICgkcSwgJGxvY2F0aW9uLCBTZXNzaW9uKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIHJlcXVlc3Q6IGZ1bmN0aW9uIChjb25maWcpIHtcbiAgICAgICAgY29uZmlnLmhlYWRlcnMgPSBjb25maWcuaGVhZGVycyB8fCB7fTtcbiAgICAgICAgaWYgKFNlc3Npb24uZ2V0U2Vzc2lvbklEKCkpIHtcbiAgICAgICAgICBjb25maWcuaGVhZGVycy5BdXRob3JpemF0aW9uID0gJ0JlYXJlciAnICsgU2Vzc2lvbi5nZXRTZXNzaW9uSUQoKVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBjb25maWc7XG4gICAgICB9LFxuICAgICAgcmVzcG9uc2U6IGZ1bmN0aW9uIChyZXNwb25zZSkge1xuICAgICAgICBjb25zb2xlLmxvZygncmVzcCcsIHJlc3BvbnNlLnN0YXR1cylcbiAgICAgICAgcmV0dXJuIHJlc3BvbnNlIHx8ICRxLndoZW4ocmVzcG9uc2UpO1xuICAgICAgfSxcbiAgICAgIHJlc3BvbnNlRXJyb3I6IGZ1bmN0aW9uIChyZWplY3Rpb24pIHtcbiAgICAgICAgY29uc29sZS5sb2coJ3Jlc3AnLCByZWplY3Rpb24uc3RhdHVzKVxuICAgICAgICBpZiAocmVqZWN0aW9uLnN0YXR1cyA9PT0gNDAxKSB7XG4gICAgICAgICAgaWYgKHJlamVjdGlvbi5kYXRhLmluZGV4T2YoXCJqd3QgZXhwaXJlZFwiKSAhPT0gLTEpIHtcbiAgICAgICAgICAgIFNlc3Npb24uZGVzdHJveSgpXG4gICAgICAgICAgICAkbG9jYXRpb24ucGF0aCgnL3NpZ25pbicpXG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICRsb2NhdGlvbi5wYXRoKCcvJylcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuICRxLnJlamVjdChyZWplY3Rpb24pO1xuXG4vLyAgICAgICAgcmV0dXJuIHJlc3BvbnNlIHx8ICRxLndoZW4ocmVzcG9uc2UpO1xuICAgICAgfVxuICAgIH1cbiAgfV0pXG5cbiAgLmNvbmZpZyhbJyRodHRwUHJvdmlkZXInLCBmdW5jdGlvbiAoJGh0dHBQcm92aWRlcikge1xuICAgICRodHRwUHJvdmlkZXIuaW50ZXJjZXB0b3JzLnB1c2goJ2F1dGhJbnRlcmNlcHRvcicpO1xuICB9XSlcblxuXG4vKlxuIHZhciBhcHAgPSBhbmd1bGFyLm1vZHVsZSgndGtzJywgWyduZ1Jlc291cmNlJywgJ25nUm91dGUnLCAnbW9kLnBhZ2VyJywgJ2FkbWluX3Rrcy5jb250cm9sbGVycycsJ2FkbWluX3Rrcy5zZXJ2aWNlcycsICd0a3Muc2VydmljZXMnLCAndGtzLnNlcnZpY2VzMicsICd0a3MuZmlsdGVycycsXG4gJ3Rrcy5jb3VudHJpZXMnLCd0a3MuZGlyZWN0aXZlcycsICd1aS5ib290c3RyYXAnXSlcbiAqL1xuXG4vKlxuIHZhciBhcHAgPSBhbmd1bGFyLm1vZHVsZSgndGtzJywgWyduZ1Jlc291cmNlJywgJ25nUm91dGUnLCAnbW9kLnBhZ2VyJywgJ21vZC5hdXRob3JpemUnLCAnYWRtaW5fdGtzLmNvbnRyb2xsZXJzJywnYWRtaW5fdGtzLnNlcnZpY2VzJyxcbiAndWkuYm9vdHN0cmFwJ10pXG4gLmNvbmZpZyhbJyRyb3V0ZVByb3ZpZGVyJywgZnVuY3Rpb24gKCRyb3V0ZVByb3ZpZGVyKSB7XG5cbiAkcm91dGVQcm92aWRlci5cbiB3aGVuKCcvaG9tZScsIHt0ZW1wbGF0ZVVybDogJ3BhcnRpYWxzL2FkbWluX2hvbWUuaHRtbCcsIGNvbnRyb2xsZXI6ICdob21lQ3RybCd9KS5cblxuIHdoZW4oJy9hYm91dCcsIHt0ZW1wbGF0ZVVybDogJ3BhcnRpYWxzL2Fib3V0Lmh0bWwnLCBjb250cm9sbGVyOiAnaG9tZUN0cmwnfSkuXG4gd2hlbignL3NpZ251cHMnLCB7dGVtcGxhdGVVcmw6ICdwYXJ0aWFscy9hZG1pbl9zaWdudXBzLmh0bWwnLCBjb250cm9sbGVyOiAnc2lnbnVwc0N0cmwnfSkuXG4gd2hlbignL3JlZ2lzdHJhdGlvbnMnLCB7dGVtcGxhdGVVcmw6ICdwYXJ0aWFscy9hZG1pbl9yZWdpc3RyYXRpb25zLmh0bWwnLCBjb250cm9sbGVyOiAncmVnaXN0cmF0aW9uc0N0cmwnfSkuXG4gd2hlbignL3RyYWRlbWFya3MnLCB7dGVtcGxhdGVVcmw6ICdwYXJ0aWFscy9hZG1pbl90cmFkZW1hcmtzLmh0bWwnLCBjb250cm9sbGVyOiAndHJhZGVtYXJrc0N0cmwnfSkuXG4gd2hlbignL3NpZ25pbicsIHt0ZW1wbGF0ZVVybDogJ3BhcnRpYWxzL3NpZ25pbi5odG1sJywgY29udHJvbGxlcjogJ2FkbWluX1VzZXJDb250cm9sbGVyJ30pLlxuIHdoZW4oJy9kYXNoYm9hcmQnLCB7dGVtcGxhdGVVcmw6ICdwYXJ0aWFscy9kYXNoYm9hcmQuaHRtbCcsIGNvbnRyb2xsZXI6ICdVc2VyQ29udHJvbGxlcid9KS5cbiB3aGVuKCcvc2lnbm91dCcsIHt0ZW1wbGF0ZVVybDogJ3BhcnRpYWxzL2NvbW1pbmcuaHRtbCcsIGNvbnRyb2xsZXI6ICdVc2VyQ29udHJvbGxlcid9KS5cbiB3aGVuKCcvY2xhc3NlcycsIHt0ZW1wbGF0ZVVybDogJ3BhcnRpYWxzL2NsYXNzZXMuaHRtbCcsIGNvbnRyb2xsZXI6ICdjbGFzc2VzQ3RybCd9KS5cbiB3aGVuKCcvc2VhcmNoX2NsYXNzZXMnLCB7dGVtcGxhdGVVcmw6ICdwYXJ0aWFscy9zZWFyY2hfY2xhc3Nlcy5odG1sJywgY29udHJvbGxlcjogJ3NlYXJjaF9jbGFzc2VzQ29udHJvbGxlcid9KS5cbiB3aGVuKCcvc3VwcG9ydGVkX2Jyb3dzZXJzJywge3RlbXBsYXRlVXJsOiAncGFydGlhbHMvc3VwcG9ydGVkX2Jyb3dzZXJzLmh0bWwnLCBjb250cm9sbGVyOiAnc2lnbnVwQ3RybCd9KS5cbiBvdGhlcndpc2Uoe3JlZGlyZWN0VG86ICcvaG9tZSd9KTtcbiB9XSkucnVuKFtcIiRyb290U2NvcGVcIiwgXCIkbG9jYXRpb25cIiwgXCJhdXRoU2VydmljZVwiLCAgIGZ1bmN0aW9uICgkcm9vdFNjb3BlLCAkbG9jYXRpb24sIGF1dGhTZXJ2aWNlKSB7XG4gY29uc29sZS5sb2coJ2hhaGEnKVxuIGNvbnNvbGUubG9nKCdoZWxsbycpXG4gJHJvb3RTY29wZS4kb24oXCIkcm91dGVDaGFuZ2VTdGFydFwiLCBmdW5jdGlvbiAoZXZlbnQsIG5leHQsIGN1cnJlbnQpIHtcblxuIGNvbnNvbGUubG9nKCdjaGFuZ2luZycpXG4gY29uc29sZS5sb2cobmV4dC4kJHJvdXRlKVxuXG4gaWYgKG5leHQuJCRyb3V0ZSAhPT0gdW5kZWZpbmVkKSB7XG4gaWYgKG5leHQuJCRyb3V0ZS5pc1ByaXYgIT09IHVuZGVmaW5lZCkge1xuIGlmIChuZXh0LiQkcm91dGUuaXNQcml2KSB7XG4gaWYgKCFhdXRoU2VydmljZS5pc1NpZ25lZEluKCkpXG4gJGxvY2F0aW9uLnBhdGgoXCIvc2lnbmluXCIpO1xuIH1cbiB9XG4gfVxuIH0pO1xuIH1dKVxuICovXG5cblxuXG4vKlxuIC5mYWN0b3J5KCdhdXRoSW50ZXJjZXB0b3InLCBbJyRxJywgJyRsb2NhdGlvbicsICdTZXNzaW9uJywgZnVuY3Rpb24gKCRxLCAkbG9jYXRpb24sIFNlc3Npb24pIHtcbiByZXR1cm4ge1xuIHJlcXVlc3Q6IGZ1bmN0aW9uIChjb25maWcpIHtcbiBjb25maWcuaGVhZGVycyA9IGNvbmZpZy5oZWFkZXJzIHx8IHt9O1xuIGlmIChTZXNzaW9uLmdldFNlc3Npb25JRCgpKSB7XG4gY29uZmlnLmhlYWRlcnMuQXV0aG9yaXphdGlvbiA9ICdCZWFyZXIgJyArIFNlc3Npb24uZ2V0U2Vzc2lvbklEKClcbiB9XG4gcmV0dXJuIGNvbmZpZztcbiB9LFxuIHJlc3BvbnNlOiBmdW5jdGlvbiAocmVzcG9uc2UpIHtcbiBjb25zb2xlLmxvZygncmVzcCcsIHJlc3BvbnNlLnN0YXR1cylcbiByZXR1cm4gcmVzcG9uc2UgfHwgJHEud2hlbihyZXNwb25zZSk7XG4gfSxcbiByZXNwb25zZUVycm9yOiBmdW5jdGlvbiAocmVzcG9uc2UpIHtcbiBjb25zb2xlLmxvZygncmVzcCcsIHJlc3BvbnNlLnN0YXR1cylcbiBpZiAocmVzcG9uc2Uuc3RhdHVzID09PSA0MDEpIHtcbiBpZiAocmVzcG9uc2UuZGF0YS5pbmRleE9mKFwiand0IGV4cGlyZWRcIikgIT09IC0xKSB7XG4gU2Vzc2lvbi5kZXN0cm95KClcbiAkbG9jYXRpb24ucGF0aCgnL3NpZ25pbicpXG4gcmV0dXJuICRxLnJlamVjdChyZXNwb25zZSk7XG4gfSBlbHNlIHtcbiAkbG9jYXRpb24ucGF0aCgnLycpXG4gcmV0dXJuICRxLnJlamVjdChyZXNwb25zZSk7XG4gfVxuIH1cbiByZXR1cm4gcmVzcG9uc2UgfHwgJHEud2hlbihyZXNwb25zZSk7XG4gfVxuIH1cbiB9XSlcblxuIGFwcC5jb25maWcoWyckaHR0cFByb3ZpZGVyJywgZnVuY3Rpb24gKCRodHRwUHJvdmlkZXIpIHtcbiAkaHR0cFByb3ZpZGVyLmludGVyY2VwdG9ycy5wdXNoKCdhdXRoSW50ZXJjZXB0b3InKTtcbiB9XSlcblxuXG4gYXBwLmZhY3RvcnkoJ2xhbmdzJywgZnVuY3Rpb24gKCkge1xuXG4gdmFyIGxhbmdzID0gW1xuIHsgXCJ2YWx1ZVwiOiBcIjAxXCIsIFwibmFtZVwiOiBcIkNsYXNzIDAxXCIsIFwiaXNvXCI6IFwiYXJcIn0sXG4geyBcInZhbHVlXCI6IFwiMDJcIiwgXCJuYW1lXCI6IFwiQ2xhc3MgMDJcIiwgXCJpc29cIjogXCJ6aC1DSFNcIn0sXG4geyBcInZhbHVlXCI6IFwiMDNcIiwgXCJuYW1lXCI6IFwiQ2xhc3MgMDNcIiwgXCJpc29cIjogXCJ6aC1DSFRcIn0sXG4geyBcInZhbHVlXCI6IFwiMDRcIiwgXCJuYW1lXCI6IFwiQ2xhc3MgMDRcIiwgXCJpc29cIjogXCJ6aC1DSFRcIn0sXG4geyBcInZhbHVlXCI6IFwiMDVcIiwgXCJuYW1lXCI6IFwiQ2xhc3MgMDVcIiwgXCJpc29cIjogXCJ6aC1DSFRcIn0sXG4geyBcInZhbHVlXCI6IFwiMDZcIiwgXCJuYW1lXCI6IFwiQ2xhc3MgMDZcIiwgXCJpc29cIjogXCJ6aC1DSFRcIn0sXG4geyBcInZhbHVlXCI6IFwiMDdcIiwgXCJuYW1lXCI6IFwiQ2xhc3MgMDdcIiwgXCJpc29cIjogXCJ6aC1DSFRcIn0sXG4geyBcInZhbHVlXCI6IFwiMDhcIiwgXCJuYW1lXCI6IFwiQ2xhc3MgMDhcIiwgXCJpc29cIjogXCJ6aC1DSFRcIn0sXG4geyBcInZhbHVlXCI6IFwiMDlcIiwgXCJuYW1lXCI6IFwiQ2xhc3MgMDlcIiwgXCJpc29cIjogXCJ6aC1DSFRcIn0sXG4geyBcInZhbHVlXCI6IFwiMTBcIiwgXCJuYW1lXCI6IFwiQ2xhc3MgMTBcIiwgXCJpc29cIjogXCJ6aC1DSFRcIn0sXG4geyBcInZhbHVlXCI6IFwiMTFcIiwgXCJuYW1lXCI6IFwiQ2xhc3MgMTFcIiwgXCJpc29cIjogXCJ6aC1DSFRcIn1cblxuIF1cbiByZXR1cm4gbGFuZ3M7XG4gfSk7XG5cbiAqLyIsIlwidXNlIHN0cmljdFwiO1xuXG4vKmdsb2JhbCBhbmd1bGFyICovXG52YXIgdXRpbGl0aWVzID0gcmVxdWlyZSgnLi4vbW9kdWxlcy91dGlsaXRpZXMuanMnKVxuXG4vLyByZXR1cm5zIGEgcHJvbWlzZSwgdXNlZCBpbiByZXNvbHZlIGZvciBzdGFmZnNcblxudmFyIGdldF9zdGFmZnMgPSBbJyRyb3V0ZScsICckaHR0cCcsIGZ1bmN0aW9uICgkcm91dGUsICRodHRwKSB7XG5cbiAgICByZXR1cm4gJGh0dHAuZ2V0KCcvYWRtaW5fYXBpL2dldF9zdGFmZnMvJylcbiAgICAgICAgLnRoZW4oXG4gICAgICAgIGZ1bmN0aW9uIHN1Y2Nlc3MocmVzcG9uc2UpIHtcbiAgICAgICAgICAgIHJldHVybiByZXNwb25zZS5kYXRhXG4gICAgICAgIH0sXG4gICAgICAgIGZ1bmN0aW9uIGVycm9yKHJlYXNvbikge1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG4gICAgICAgICk7XG59XVxuXG5cbmFuZ3VsYXIubW9kdWxlKCdhZG1pbl90a3MuY29udHJvbGxlcnMnLCBbXSlcbiAgICAuY29udHJvbGxlcignaGVscF9pbmZvJywgZnVuY3Rpb24gKCkge1xuXG4gICAgfSlcblxuICAgIC5jb250cm9sbGVyKCdob21lQ3RybCcsIFtcIiRzY29wZVwiLFxuICAgICAgICBmdW5jdGlvbiAoJHNjb3BlKSB7XG4gICAgICAgICAgICAkc2NvcGUudGhhbmtzID0gJ3RoYW5rcydcblxuICAgICAgICAgICAgJHNjb3BlLmdvdG9fY2FzZSA9IGZ1bmN0aW9uIChjYXNlX2NvZGUpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnY2FzZSBjb2RlIGlzICcsIGNhc2VfY29kZSlcbiAgICAgICAgICAgIH1cblxuICAgICAgICB9XSlcblxuICAgIC5jb250cm9sbGVyKCdIZWFkZXJDb250cm9sbGVyJywgW1wiJHNjb3BlXCIsIFwiJGxvY2F0aW9uXCIsIFwiJHJvb3RTY29wZVwiLCBcImF1dGhTZXJ2aWNlXCIsIFwiQVVUSF9FVkVOVFNcIixcbiAgICAgICAgZnVuY3Rpb24gKCRzY29wZSwgJGxvY2F0aW9uLCAkcm9vdFNjb3BlLCBhdXRoU2VydmljZSwgQVVUSF9FVkVOVFMpIHtcblxuICAgICAgICAgICAgJHNjb3BlLmluaXQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ2luaXR0aW5nJylcbiAgICAgICAgICAgICAgICBpZiAoYXV0aFNlcnZpY2UuaXNTaWduZWRJbigpKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmICgkc2NvcGUudXNlck5hbWUgPT09IG51bGwpXG4gICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUudXNlck5hbWUgPSBhdXRoU2VydmljZS51c2VyTmFtZSgpXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAkc2NvcGUuaXNCcm93c2VyU3VwcG9ydGVkID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIC8vICAgICAgICByZXR1cm4gIC9jaHJvbWUvLnRlc3QobmF2aWdhdG9yLnVzZXJBZ2VudC50b0xvd2VyQ2FzZSgpKTtcbiAgICAgICAgICAgICAgICByZXR1cm4gdXRpbGl0aWVzLnN1cHBvcnRlZEJyb3dzZXIobmF2aWdhdG9yLnVzZXJBZ2VudCwgJ2FuZ3VsYXInKVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAkc2NvcGUuaXNBY3RpdmUgPSBmdW5jdGlvbiAodmlld0xvY2F0aW9uKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHZpZXdMb2NhdGlvbiA9PT0gJGxvY2F0aW9uLnBhdGgoKTtcbiAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgICRzY29wZS5pc0xvZ2dlZEluID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIHJldHVybiBhdXRoU2VydmljZS5pc1NpZ25lZEluKClcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgJHNjb3BlLmxvZ091dCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICBhdXRoU2VydmljZS5sb2dPdXQoKVxuICAgICAgICAgICAgICAgICRyb290U2NvcGUuJGJyb2FkY2FzdChBVVRIX0VWRU5UUy5sb2dvdXRTdWNjZXNzKTtcbiAgICAgICAgICAgICAgICAkbG9jYXRpb24ucGF0aCgnLycpXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICRzY29wZS51c2VyTmFtZSA9IG51bGxcbiAgICAgICAgICAgICRzY29wZS5jYXJ0Q291bnQgPSAwXG5cbiAgICAgICAgICAgICRzY29wZS4kb24oQVVUSF9FVkVOVFMubG9naW5TdWNjZXNzLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgICAgICRzY29wZS51c2VyTmFtZSA9IGF1dGhTZXJ2aWNlLnVzZXJOYW1lKClcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAkc2NvcGUuJG9uKEFVVEhfRVZFTlRTLmxvZ291dFN1Y2Nlc3MsIGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLnVzZXJOYW1lID0gYXV0aFNlcnZpY2UudXNlck5hbWUoKVxuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICRzY29wZS5pbml0KClcblxuICAgICAgICB9XSlcblxuXG4gICAgLmNvbnRyb2xsZXIoJ2NsYXNzZXNDdHJsJywgWyckc2NvcGUnLCAnJGh0dHAnLCBmdW5jdGlvbiAoJHNjb3BlLCAkaHR0cCkge1xuXG4gICAgICAgICRzY29wZS5yZWFkeSA9IHRydWVcblxuICAgICAgICAkaHR0cC5nZXQoJy9hcGkyL2NyX2NsYXNzZXMvJykuc3VjY2VzcyhmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgJHNjb3BlLmNsYXNzZXMgPSBkYXRhXG4gICAgICAgICAgICAkc2NvcGUucmVhZHkgPSB0cnVlXG4gICAgICAgIH0pXG5cbiAgICB9XSlcblxuICAgIC5jb250cm9sbGVyKCdhZG1pbl9Vc2VyQ29udHJvbGxlcicsIFsnJHNjb3BlJywgJyRodHRwJywgJyRsb2NhdGlvbicsICckcm9vdFNjb3BlJywgJ2F1dGhTZXJ2aWNlJywgJ0FVVEhfRVZFTlRTJyxcbiAgICAgICAgZnVuY3Rpb24gKCRzY29wZSwgJGh0dHAsICRsb2NhdGlvbiwgJHJvb3RTY29wZSwgYXV0aFNlcnZpY2UsIEFVVEhfRVZFTlRTKSB7XG5cbiAgICAgICAgICAgICRzY29wZS5hbGVydHMgPSBbXTtcblxuICAgICAgICAgICAgJHNjb3BlLmNsb3NlQWxlcnQgPSBmdW5jdGlvbiAoaW5kZXgpIHtcbiAgICAgICAgICAgICAgICAkc2NvcGUuYWxlcnRzLnNwbGljZShpbmRleCwgMSk7XG4gICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICAkc2NvcGUuYXV0aGVudGljYXRlID0gZnVuY3Rpb24gKHVzZXIpIHtcblxuICAgICAgICAgICAgICAgIGF1dGhTZXJ2aWNlLnNpZ25Jbih1c2VyKS50aGVuKGZ1bmN0aW9uIChzdGF0dXMpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHN0YXR1cy5hdXRoZW50aWNhdGVkID09PSB0cnVlKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICRyb290U2NvcGUuJGJyb2FkY2FzdChBVVRIX0VWRU5UUy5sb2dpblN1Y2Nlc3MpO1xuICAgICAgICAgICAgICAgICAgICAgICAgJGxvY2F0aW9uLnBhdGgoJy9yZWdpc3RyYXRpb25zJylcblxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmFsZXJ0cyA9IFtdXG4gICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUuYWxlcnRzLnB1c2goeyB0eXBlOiAnd2FybmluZycsIG1zZzogc3RhdHVzLm1lc3NhZ2UgfSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgICRzY29wZS5mb3Jnb3RfcGFzc3dvcmQgPSBmdW5jdGlvbiAodXNlcikge1xuICAgICAgICAgICAgICAgIGlmICgodXNlciA9PT0gdW5kZWZpbmVkKSB8fCAoIXVzZXIuZW1haWwpKSB7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5hbGVydHMgPSBbXVxuICAgICAgICAgICAgICAgICAgICAkc2NvcGUuYWxlcnRzLnB1c2goeyB0eXBlOiAnd2FybmluZycsIG1zZzogJ1BsZWFzZSBlbnRlciBhbiBlbWFpbCBhZGRyZXNzIGZpcnN0JyB9KTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBhdXRoU2VydmljZS5yZXF1ZXN0X3Bhc3N3b3JkX3Jlc2V0KHVzZXIuZW1haWwpLnRoZW4oZnVuY3Rpb24gKHJlc3VsdCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmFsZXJ0cyA9IFtdXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzdWx0LnN0YXR1cylcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUuYWxlcnRzLnB1c2goe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAnc3VjY2VzcycsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1zZzogJ1dlIGhhdmUgc2VudCBhbiBlbWFpbCB3aXRoIHBhc3N3b3JkIHJlc2V0IGluc3RydWN0aW9uLCBwbGVhc2UgY2hlY2sgeW91ciBpbmJveCdcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5hbGVydHMucHVzaCh7IHR5cGU6ICd3YXJuaW5nJywgbXNnOiByZXN1bHQubWVzc2FnZSB9KVxuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfV0pXG5cbiAgICAuY29udHJvbGxlcignc2lnbnVwc0N0cmwnLCBbJyRzY29wZScsICckaHR0cCcsICckcm91dGVQYXJhbXMnLCAnU2lnbnVwcycsIGZ1bmN0aW9uICgkc2NvcGUsICRodHRwLCAkcm91dGVQYXJhbXMsIFNpZ251cHMpIHtcbiAgICAgICAgY29uc29sZS5sb2coJ3NpZ251cHMnKVxuICAgICAgICAvKlxuICAgICAgICAgJHNjb3BlLnNfaXRlbXMgPSBTaWdudXBzLnF1ZXJ5KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgIGNvbnNvbGUubG9nKCRzY29wZS5zX2l0ZW1zKVxuICAgICAgICAgY29uc29sZS5sb2coJ3NpZ251cHMnKVxuICAgICAgICAgfSk7XG5cbiAgICAgICAgIGNvbnNvbGUubG9nKCdyZWcgbGlzdCcpXG4gICAgICAgICAqL1xuICAgICAgICAkc2NvcGUucGFnZVNpemUgPSAxMDtcblxuICAgICAgICAvKlxuICAgICAgICAgJHNjb3BlLnNfaXRlbXMgPSBSZWdpc3RyYXRpb25zLnF1ZXJ5KGZ1bmN0aW9uKCkge1xuICAgICAgICAgY29uc29sZS5sb2coJ3JlZyBsaXN0MicpXG4gICAgICAgICBjb25zb2xlLmxvZygkc2NvcGUuc19pdGVtcylcbiAgICAgICAgIH0pO1xuICAgICAgICAgKi9cblxuICAgICAgICAkc2NvcGUuc2V0RGF0YVNvdXJjZSA9IGZ1bmN0aW9uIChwYWdlX25vKSB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhwYWdlX25vKVxuICAgICAgICAgICAgLy92YXIgb2Zmc2V0ID0gcmVxLnF1ZXJ5LnBhZ2Vfbm8gKiByZXEucXVlcnkucGFnZV9zaXplO1xuICAgICAgICAgICAgJHNjb3BlLnNfaXRlbXMgPSBTaWdudXBzLnF1ZXJ5KHsgcGFnZV9ubzogcGFnZV9ubywgcGFnZV9zaXplOiAxMCB9LCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coJHNjb3BlLnNfaXRlbXMpXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuXG4gICAgICAgIC8qXG4gICAgICAgICAkc2NvcGUuc19pdGVtcyA9IFJlZ2lzdHJhdGlvbnMucXVlcnkoZnVuY3Rpb24oKSB7XG4gICAgICAgICBjb25zb2xlLmxvZygkc2NvcGUuc19pdGVtcylcbiAgICAgICAgIH0pO1xuICAgICAgICAgKi9cbiAgICAgICAgJHNjb3BlLmN1cnJlbnRQYWdlID0gMFxuXG4gICAgICAgICRzY29wZS5zZXROdW1iZXJPZlBhZ2VzID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgY29uc29sZS5sb2coJ2luaXQnKVxuICAgICAgICAgICAgLy8gICAgICAkc2NvcGUubnVtYmVyT2ZQYWdlcyA9IDIwXG4gICAgICAgICAgICAkaHR0cC5nZXQoJy9hZG1pbl9hcGkvc2lnbnVwc19jb3VudCcpXG4gICAgICAgICAgICAgICAgLnN1Y2Nlc3MoZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm51bWJlck9mUGFnZXMgPSBNYXRoLmNlaWwoZGF0YS5jb3VudCAvICRzY29wZS5wYWdlU2l6ZSk7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCRzY29wZS5udW1iZXJPZlBhZ2VzKVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgIH1cblxuICAgICAgICBpZiAoJHJvdXRlUGFyYW1zLnBhZ2Vfbm8pIHtcbiAgICAgICAgICAgICRzY29wZS5jdXJyZW50UGFnZSA9ICRyb3V0ZVBhcmFtcy5wYWdlX25vIC0gMVxuICAgICAgICB9XG5cbiAgICAgICAgJHNjb3BlLnNldERhdGFTb3VyY2UoJHNjb3BlLmN1cnJlbnRQYWdlKSAgLy8gZ29vZCBmb3IgZmlyc3QgcGFnZVxuXG5cbiAgICB9XSlcblxuICAgIC5jb250cm9sbGVyKCdyZWdpc3RyYXRpb25zQ3RybCcsIFsnJHNjb3BlJywgJyRodHRwJywgJyRyb3V0ZVBhcmFtcycsICdSZWdpc3RyYXRpb25zJywgZnVuY3Rpb24gKCRzY29wZSwgJGh0dHAsICRyb3V0ZVBhcmFtcywgUmVnaXN0cmF0aW9ucykge1xuXG4gICAgICAgIGNvbnNvbGUubG9nKCdyZWcgbGlzdCcpXG5cbiAgICAgICAgJHNjb3BlLnBhZ2VTaXplID0gMTA7XG5cbiAgICAgICAgLypcbiAgICAgICAgICRzY29wZS5zX2l0ZW1zID0gUmVnaXN0cmF0aW9ucy5xdWVyeShmdW5jdGlvbigpIHtcbiAgICAgICAgIGNvbnNvbGUubG9nKCdyZWcgbGlzdDInKVxuICAgICAgICAgY29uc29sZS5sb2coJHNjb3BlLnNfaXRlbXMpXG4gICAgICAgICB9KTtcbiAgICAgICAgICovXG5cbiAgICAgICAgJHNjb3BlLnNldERhdGFTb3VyY2UgPSBmdW5jdGlvbiAocGFnZV9ubykge1xuICAgICAgICAgICAgY29uc29sZS5sb2cocGFnZV9ubylcbiAgICAgICAgICAgIC8vdmFyIG9mZnNldCA9IHJlcS5xdWVyeS5wYWdlX25vICogcmVxLnF1ZXJ5LnBhZ2Vfc2l6ZTtcbiAgICAgICAgICAgICRzY29wZS5zX2l0ZW1zID0gUmVnaXN0cmF0aW9ucy5xdWVyeSh7IHBhZ2Vfbm86IHBhZ2Vfbm8sIHBhZ2Vfc2l6ZTogMTAgfSwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCRzY29wZS5zX2l0ZW1zKVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cblxuICAgICAgICAvKlxuICAgICAgICAgJHNjb3BlLnNfaXRlbXMgPSBSZWdpc3RyYXRpb25zLnF1ZXJ5KGZ1bmN0aW9uKCkge1xuICAgICAgICAgY29uc29sZS5sb2coJHNjb3BlLnNfaXRlbXMpXG4gICAgICAgICB9KTtcbiAgICAgICAgICovXG4gICAgICAgICRzY29wZS5jdXJyZW50UGFnZSA9IDBcblxuICAgICAgICAkc2NvcGUuc2V0TnVtYmVyT2ZQYWdlcyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdpbml0JylcbiAgICAgICAgICAgIC8vICAgICAgJHNjb3BlLm51bWJlck9mUGFnZXMgPSAyMFxuICAgICAgICAgICAgJGh0dHAuZ2V0KCcvYWRtaW5fYXBpL3JlZ2lzdHJhdGlvbnNfY291bnQnKVxuICAgICAgICAgICAgICAgIC5zdWNjZXNzKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5udW1iZXJPZlBhZ2VzID0gTWF0aC5jZWlsKGRhdGEuY291bnQgLyAkc2NvcGUucGFnZVNpemUpO1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygkc2NvcGUubnVtYmVyT2ZQYWdlcylcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICB9XG5cbiAgICAgICAgaWYgKCRyb3V0ZVBhcmFtcy5wYWdlX25vKSB7XG4gICAgICAgICAgICAkc2NvcGUuY3VycmVudFBhZ2UgPSAkcm91dGVQYXJhbXMucGFnZV9ubyAtIDFcbiAgICAgICAgfVxuXG4gICAgICAgICRzY29wZS5zZXREYXRhU291cmNlKCRzY29wZS5jdXJyZW50UGFnZSkgIC8vIGdvb2QgZm9yIGZpcnN0IHBhZ2VcblxuICAgIH1dKVxuXG4gICAgLmNvbnRyb2xsZXIoJ3RyYWRlbWFya3NDdHJsJywgWyckc2NvcGUnLCAnJGh0dHAnLCAnVHJhZGVtYXJrcycsIGZ1bmN0aW9uICgkc2NvcGUsICRodHRwLCBUcmFkZW1hcmtzKSB7XG4gICAgICAgICRzY29wZS5zX2l0ZW1zID0gVHJhZGVtYXJrcy5xdWVyeShmdW5jdGlvbiAoKSB7XG4gICAgICAgIH0pO1xuXG4gICAgfV0pLmNvbnRyb2xsZXIoJ3Byb2ZpbGVDb250cm9sbGVyJywgWyckc2NvcGUnLCAnJGh0dHAnLCAnJHJvdXRlUGFyYW1zJywgJyRsb2NhdGlvbicsICdjb3VudHJpZXMnLCAnREJTdG9yZScsXG4gICAgICAgIGZ1bmN0aW9uICgkc2NvcGUsICRodHRwLCAkcm91dGVQYXJhbXMsICRsb2NhdGlvbiwgY291bnRyaWVzLCBEQlN0b3JlKSB7XG5cblxuICAgICAgICAgICAgY29uc29sZS5sb2coJHJvdXRlUGFyYW1zKVxuICAgICAgICAgICAgJHNjb3BlLm5hdHVyZXMgPSBbXG4gICAgICAgICAgICAgICAgeyBuYW1lOiAnQ29tcGFueScsIGNvZGU6ICdDJyB9LFxuICAgICAgICAgICAgICAgIHsgbmFtZTogJ0luZGl2aWR1YWwnLCBjb2RlOiAnSScgfVxuICAgICAgICAgICAgXVxuICAgICAgICAgICAgJHNjb3BlLmNvdW50cmllcyA9IGNvdW50cmllc1xuXG4gICAgICAgICAgICBpZiAoJHJvdXRlUGFyYW1zLmJhY2tsaW5rKVxuICAgICAgICAgICAgICAgICRzY29wZS5iYWNrbGluayA9IERCU3RvcmUuZ2V0KCRyb3V0ZVBhcmFtcy5iYWNrbGluaylcblxuXG4gICAgICAgICAgICAkc2NvcGUudXNlZF9pbl9hZG1pbiA9IHRydWVcblxuICAgICAgICAgICAgY29uc29sZS5sb2coJHJvdXRlUGFyYW1zKVxuICAgICAgICAgICAgaWYgKCRyb3V0ZVBhcmFtcy5pZCA9PT0gJ25ldycpIHtcbiAgICAgICAgICAgICAgICAkc2NvcGUucmVhZHkgPSB0cnVlXG5cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgZWxzZSB7XG5cbiAgICAgICAgICAgICAgICAkaHR0cC5nZXQoJy9hZG1pbl9hcGkvcHJvZmlsZS8nICsgJHJvdXRlUGFyYW1zLmlkKS50aGVuKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGEpXG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5wcm9maWxlID0gZGF0YS5kYXRhXG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5yZWFkeSA9IHRydWVcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuICRodHRwLmdldCgnL2FkbWluX2FwaS9hY2NvdW50LycgKyAkc2NvcGUucHJvZmlsZS51c2VyX2lkKVxuICAgICAgICAgICAgICAgIH0pLnRoZW4oZnVuY3Rpb24gKGFjY291bnQpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coYWNjb3VudClcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnVzZXIgPSBhY2NvdW50LmRhdGFcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuICRodHRwLmdldCgnL2FkbWluX2FwaS9wcm9maWxlcy8nICsgJHNjb3BlLnVzZXIuX2lkKVxuXG4gICAgICAgICAgICAgICAgfSkudGhlbihmdW5jdGlvbiAocHJvZmlsZXMpIHtcblxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygndXUnLCBwcm9maWxlcylcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnByb2ZpbGVzID0gcHJvZmlsZXMuZGF0YVxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygkc2NvcGUucHJvZmlsZXMpXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH1cblxuXG4gICAgICAgICAgICAkc2NvcGUuY2FuY2VsID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIGlmICgkc2NvcGUuYmFja2xpbmspXG4gICAgICAgICAgICAgICAgICAgICRsb2NhdGlvbi5wYXRoKCRzY29wZS5iYWNrbGluaylcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgJHNjb3BlLnVwZGF0ZV9wcm9maWxlID0gZnVuY3Rpb24gKHByb2ZpbGUpIHtcblxuXG4gICAgICAgICAgICAgICAgJGh0dHAucG9zdChcIi9hZG1pbl9hcGkvdXBkYXRlX3Byb2ZpbGVcIiwgcHJvZmlsZSkuc3VjY2VzcyhmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhKVxuICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YS5zdGF0dXMgPT09ICdvaycpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5zaG93X21zZyA9IFwiWW91ciBwcm9maWxlIGhhcyBiZWVuIHVwZGF0ZWQuXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICgkc2NvcGUuYmFja2xpbmspIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkbG9jYXRpb24ucGF0aCgkc2NvcGUuYmFja2xpbmspXG4gICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5zaG93X21zZyA9IGRhdGEubXNnXG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgfV0pXG5cbiAgICAuY29udHJvbGxlcigncmVnaXN0cmF0aW9uRGV0YWlsQ29udHJvbGxlcicsIFsnJHNjb3BlJywgJyRyb3V0ZScsICckaHR0cCcsICckcm9vdFNjb3BlJywgJyRyb3V0ZVBhcmFtcycsICckbW9kYWwnLCAnQWRtaW5TZXJ2aWNlcycsICdyZWdzdHJuJyxcbiAgICAgICAgZnVuY3Rpb24gKCRzY29wZSwgJHJvdXRlLCAkaHR0cCwgJHJvb3RTY29wZSwgJHJvdXRlUGFyYW1zLCAkbW9kYWwsIEFkbWluU2VydmljZXMsIHJlZ3N0cm4pIHtcblxuXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhyZWdzdHJuKVxuXG4gICAgICAgICAgICAkc2NvcGUucmVmID0gJHJvdXRlUGFyYW1zLmlkXG4gICAgICAgICAgICAkc2NvcGUuZGV0YWlscyA9IHJlZ3N0cm4uZGF0YVxuXG4gICAgICAgICAgICAkc2NvcGUuY3JlYXRlX3RpY2tldCA9IGZ1bmN0aW9uIChkZXRhaWxzKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ2NyZWF0ZV90aWNrZXRzJylcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhkZXRhaWxzKVxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdjcmVhdGVfdGlja2V0c3h4JylcblxuXG4gICAgICAgICAgICAgICAgdmFyIG1vZGFsSW5zdGFuY2UgPSAkbW9kYWwub3Blbih7XG4gICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlVXJsOiAncGFydGlhbHMvYWRtaW5fY3JlYXRlX3RpY2tldC5odG1sJyxcbiAgICAgICAgICAgICAgICAgICAgd2luZG93Q2xhc3M6ICdhcHAtbW9kYWwtd2luZG93JyxcbiAgICAgICAgICAgICAgICAgICAgc2l6ZTogJ3NtJyxcbiAgICAgICAgICAgICAgICAgICAgY29udHJvbGxlcjogJ01vZGFsQ3JlYXRlVGlja2V0JywgcmVzb2x2ZToge1xuICAgICAgICAgICAgICAgICAgICAgICAgc3RhZmZzOiBnZXRfc3RhZmZzLFxuICAgICAgICAgICAgICAgICAgICAgICAgZGV0YWlsczogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBkZXRhaWxzXG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgIG1vZGFsSW5zdGFuY2UucmVzdWx0LnRoZW4oZnVuY3Rpb24gKHRpY2tldF9pbmZvKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdzZWxlY3QnLCB0aWNrZXRfaW5mbylcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2codGlja2V0X2luZm8uc3RhZmYpXG4gICAgICAgICAgICAgICAgICAgIGlmICh0aWNrZXRfaW5mby5zdGFmZiAhPT0gdW5kZWZpbmVkKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHRpY2tldF9pbmZvLnJlZ2lzdHJhdGlvbl9pZCA9IGRldGFpbHMuX2lkXG5cbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHRpY2tldF9pbmZvKVxuXG4gICAgICAgICAgICAgICAgICAgICAgICBBZG1pblNlcnZpY2VzLmNyZWF0ZV90aWNrZXQodGlja2V0X2luZm8pLnRoZW4oZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnYmFjayAnLCBkYXRhKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdiYWNreCcsIGRhdGEuc3RhdHVzKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChkYXRhLnN0YXR1cykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnYXNmJylcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJHJvdXRlLnJlbG9hZCgpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgfVxuXG5cbiAgICAgICAgICAgICAgICB9LCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIC8vICRsb2cuaW5mbygnTW9kYWwgZGlzbWlzc2VkIGF0OiAnICsgbmV3IERhdGUoKSk7XG4gICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIH1cblxuICAgICAgICB9XSlcblxuICAgIC5jb250cm9sbGVyKCdNb2RhbENyZWF0ZVRpY2tldCcsIFsnJHNjb3BlJywgJyRtb2RhbEluc3RhbmNlJywgJ2RldGFpbHMnLCAnc3RhZmZzJyxcbiAgICAgICAgZnVuY3Rpb24gKCRzY29wZSwgJG1vZGFsSW5zdGFuY2UsIGRldGFpbHMsIHN0YWZmcykge1xuXG4gICAgICAgICAgICAkc2NvcGUuZGV0YWlscyA9IGRldGFpbHM7XG4gICAgICAgICAgICAkc2NvcGUuc3RhZmZzID0gc3RhZmZzO1xuXG4gICAgICAgICAgICBjb25zb2xlLmxvZygnc3RhZmZzJywgc3RhZmZzKVxuICAgICAgICAgICAgLy8gICAgICAkc2NvcGUudGlja2V0ID0geyByZW1hcms6J3JlbWFyayd9XG4gICAgICAgICAgICAkc2NvcGUudGlja2V0ID0ge31cblxuICAgICAgICAgICAgJHNjb3BlLm9rID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCRzY29wZS50aWNrZXQpXG4gICAgICAgICAgICAgICAgJG1vZGFsSW5zdGFuY2UuY2xvc2UoJHNjb3BlLnRpY2tldCk7XG4gICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICAkc2NvcGUuY2FuY2VsID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICRtb2RhbEluc3RhbmNlLmRpc21pc3MoJ2NhbmNlbCcpO1xuICAgICAgICAgICAgfTtcblxuICAgICAgICB9XSlcblxuXG4gICAgLmNvbnRyb2xsZXIoJ3RpY2tldENvbnRyb2xsZXInLCBbJyRzY29wZScsICckcm91dGUnLCAnJGh0dHAnLCAnJHJvb3RTY29wZScsICckcm91dGVQYXJhbXMnLCAnJG1vZGFsJywgJ0FkbWluU2VydmljZXMnLCAndGlja2V0JyxcbiAgICAgICAgZnVuY3Rpb24gKCRzY29wZSwgJHJvdXRlLCAkaHR0cCwgJHJvb3RTY29wZSwgJHJvdXRlUGFyYW1zLCAkbW9kYWwsIEFkbWluU2VydmljZXMsIHRpY2tldCkge1xuXG4gICAgICAgICAgICB2YXIgdm0gPSB0aGlzXG4gICAgICAgICAgICBjb25zb2xlLmxvZyh0aWNrZXQpXG4gICAgICAgICAgICBjb25zb2xlLmxvZygndycpXG4gICAgICAgICAgICBjb25zb2xlLmxvZygneScpXG4gICAgICAgICAgICB0aGlzLnRpY2tldCA9IHRpY2tldFxuICAgICAgICAgICAgdGhpcy5tc2cgPSAnb2snXG5cbiAgICAgICAgICAgIEFkbWluU2VydmljZXMuZ2V0X3JlZ2lzdHJhdGlvbih0aWNrZXQucmVnaXN0cmF0aW9uX2lkKS50aGVuKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coJzEyMycsIGRhdGEpXG4gICAgICAgICAgICAgICAgLy8gJHNjb3BlLnRoaXMucmVnID0gZGF0YVxuICAgICAgICAgICAgICAgIHZtLnJlZ2lzdHJhdGlvbiA9IGRhdGFcbiAgICAgICAgICAgICAgICAvLyB0aGlzLnJlZ2lzdHJhdGlvbiA9IGRhdGFcbiAgICAgICAgICAgIH0pXG5cblxuICAgICAgICAgICAgJHNjb3BlLmFwcGVuZF9ub3RlID0gZnVuY3Rpb24gKG5vdGUsIGFjdGlvbikge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdhcHBlbmQgJylcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhub3RlKVxuXG5cbiAgICAgICAgICAgICAgICBBZG1pblNlcnZpY2VzLmFwcGVuZF90aWNrZXRfZWxlbWVudCh7XG4gICAgICAgICAgICAgICAgICAgIHRpY2tldF9pZDogdm0udGlja2V0Ll9pZCxcbiAgICAgICAgICAgICAgICAgICAgbXNnOiBub3RlLFxuICAgICAgICAgICAgICAgICAgICBhY3Rpb246IGFjdGlvblxuICAgICAgICAgICAgICAgIH0pLnRoZW4oZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ3h5eicsIGRhdGEpXG4gICAgICAgICAgICAgICAgICAgICRyb3V0ZS5yZWxvYWQoKVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICRzY29wZS5kZWxldGVfZWxlbWVudCA9IGZ1bmN0aW9uIChlbGUpIHtcblxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHZtLnRpY2tldClcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnZGVsZXRlIGUnLCBlbGUpXG5cbiAgICAgICAgICAgICAgICBpZiAoY29uZmlybSgnRG8geW91IHJlYWxseSB3YW50IHRvIGRlbGV0ZSB0aGlzIGl0ZW0/JykpIHtcblxuICAgICAgICAgICAgICAgICAgICBBZG1pblNlcnZpY2VzLmRlbGV0ZV90aWNrZXRfZWxlbWVudCh2bS50aWNrZXQuX2lkLCBlbGUpLnRoZW4oZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGEpXG4gICAgICAgICAgICAgICAgICAgICAgICAkcm91dGUucmVsb2FkKClcbiAgICAgICAgICAgICAgICAgICAgfSlcblxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuXG4gICAgICAgIH1dKVxuXG5cbiAgICAuY29udHJvbGxlcignYWNjb3VudENvbnRyb2xsZXInLCBbJyRzY29wZScsICckaHR0cCcsICckcm91dGVQYXJhbXMnLCAnY291bnRyaWVzJyxcbiAgICAgICAgZnVuY3Rpb24gKCRzY29wZSwgJGh0dHAsICRyb3V0ZVBhcmFtcywgY291bnRyaWVzKSB7XG5cbiAgICAgICAgICAgICRzY29wZS5hbGVydHMgPSBbXTtcbiAgICAgICAgICAgICRzY29wZS5jbG9zZUFsZXJ0ID0gZnVuY3Rpb24gKGluZGV4KSB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLmFsZXJ0cy5zcGxpY2UoaW5kZXgsIDEpO1xuICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgJHNjb3BlLnVzZWRfaW5fYWRtaW4gPSB0cnVlXG4gICAgICAgICAgICAkc2NvcGUuY291bnRyaWVzID0gY291bnRyaWVzXG5cbiAgICAgICAgICAgICRodHRwLmdldCgnL2FkbWluX2FwaS9hY2NvdW50LycgKyAkcm91dGVQYXJhbXMuaWQpLnRoZW4oZnVuY3Rpb24gKGRhdGEpIHtcblxuICAgICAgICAgICAgICAgICRzY29wZS51c2VyID0gZGF0YS5kYXRhXG5cbiAgICAgICAgICAgICAgICBpZiAoJHNjb3BlLnVzZXIuYWRtaW4pXG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5hZG1pbiA9ICRzY29wZS51c2VyLmFkbWluXG5cbiAgICAgICAgICAgICAgICAkc2NvcGUucmVhZHkgPSB0cnVlXG5cbiAgICAgICAgICAgICAgICByZXR1cm4gJGh0dHAuZ2V0KCcvYWRtaW5fYXBpL3Byb2ZpbGVzLycgKyAkcm91dGVQYXJhbXMuaWQpXG5cblxuICAgICAgICAgICAgfSkudGhlbihmdW5jdGlvbiAocHJvZmlsZXMpIHtcblxuICAgICAgICAgICAgICAgICRzY29wZS5wcm9maWxlcyA9IHByb2ZpbGVzLmRhdGFcblxuICAgICAgICAgICAgfSlcblxuICAgICAgICAgICAgJHNjb3BlLnVwZGF0ZV9hZG1pbiA9IGZ1bmN0aW9uIChhZG1pbikge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGFkbWluKVxuICAgICAgICAgICAgICAgICRodHRwLnBvc3QoXCIvYWRtaW5fYXBpL3VwZGF0ZV9hZG1pblwiLCB7XG4gICAgICAgICAgICAgICAgICAgIHVzZXJfaWQ6ICRzY29wZS51c2VyLl9pZCxcbiAgICAgICAgICAgICAgICAgICAgYWRtaW46IGFkbWluXG4gICAgICAgICAgICAgICAgfSkuc3VjY2VzcyhmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhKVxuICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YS5zdGF0dXMgPT09ICdvaycpIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnNob3dfbXNnID0gJ1VwZGF0ZWQnXG5cbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5zaG93X21zZyA9IGRhdGEuc2hvd19tZXNzYWdlXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgJHNjb3BlLnVwZGF0ZV91c2VyID0gZnVuY3Rpb24gKHVzZXIpIHtcblxuICAgICAgICAgICAgICAgICRodHRwLnBvc3QoXCIvYWRtaW5fYXBpL3VwZGF0ZV91c2VyXCIsIHVzZXIpLnN1Y2Nlc3MoZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZGF0YSlcbiAgICAgICAgICAgICAgICAgICAgaWYgKGRhdGEuc3RhdHVzID09PSAnb2snKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUuc2hvd19tc2cgPSBcIlRoaXMgYWNjb3VudCBoYXMgYmVlbiB1cGRhdGVkLlwiXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUuc2hvd19tc2cgPSBkYXRhLm1zZ1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICRzY29wZS5jbGVhcl9tc2cgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLnNob3dfbXNnID0gJydcbiAgICAgICAgICAgIH1cblxuXG4gICAgICAgIH1dKVxuXG4gICAgLmNvbnRyb2xsZXIoJ2NhcnRzQ3RybCcsIFsnJHNjb3BlJywgJyRodHRwJywgJyRyb3V0ZVBhcmFtcycsICdDYXJ0cycsIHJlcXVpcmUoJy4vY29udHJvbGxlcnMvY2FydHMnKS5jYXJ0c10pXG5cbiAgICAuY29udHJvbGxlcignY2FydENvbnRyb2xsZXInLCBbJyRzY29wZScsICckaHR0cCcsICckcm91dGVQYXJhbXMnLCAnJG1vZGFsJywgJ2NvdW50cmllcycsIHJlcXVpcmUoJy4vY29udHJvbGxlcnMvY2FydCcpLmNhcnRdKVxuXG4gICAgLmNvbnRyb2xsZXIoJ2NhcnRBZGRDb250cm9sbGVyJywgWyckc2NvcGUnLCAnJGh0dHAnLCAnJHJvdXRlUGFyYW1zJywgJyRsb2NhdGlvbicsICdjb3VudHJpZXMnLCAnQWRtaW5TZXJ2aWNlcycsXG4gICAgICAgIHJlcXVpcmUoJy4vY29udHJvbGxlcnMvY2FydF9hZGQnKS5jYXJ0X2FkZF0pXG5cbiAgICAuY29udHJvbGxlcignY2FydENvcHlDb250cm9sbGVyJywgWyckc2NvcGUnLCAnJGh0dHAnLCAnJHJvdXRlUGFyYW1zJywgJyRsb2NhdGlvbicsICdjb3VudHJpZXMnLCAnQWRtaW5TZXJ2aWNlcycsICdDYXJ0cycsXG4gICAgICAgIHJlcXVpcmUoJy4vY29udHJvbGxlcnMvY2FydF9jb3B5JykuY2FydF9jb3B5XSlcblxuXG4gICAgLmNvbnRyb2xsZXIoJ2FkbWluX01lc3NhZ2VzQ29udHJvbGxlcicsIFsnJHNjb3BlJywgJyRodHRwJywgZnVuY3Rpb24gKCRzY29wZSwgJGh0dHApIHtcblxuICAgICAgICAkc2NvcGUubWVzc2FnZXMgPSBudWxsXG5cbiAgICAgICAgJGh0dHAuZ2V0KCcvYWRtaW5fYXBpL21lc3NhZ2VzJykuc3VjY2VzcyhmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgY29uc29sZS5sb2coZGF0YSlcbiAgICAgICAgICAgICRzY29wZS5tZXNzYWdlcyA9IGRhdGFcbiAgICAgICAgICAgICRzY29wZS5yZWFkeSA9IHRydWVcbiAgICAgICAgfSlcblxuICAgIH1dKVxuXG4gICAgLmNvbnRyb2xsZXIoJ29yZGVyc0N0cmwnLCBbJyRzY29wZScsICckaHR0cCcsICckcm91dGVQYXJhbXMnLCAnT3JkZXJzJywgZnVuY3Rpb24gKCRzY29wZSwgJGh0dHAsICRyb3V0ZVBhcmFtcywgT3JkZXJzKSB7XG5cbiAgICAgICAgY29uc29sZS5sb2coJ29yZGVycyBsaXN0JylcblxuICAgICAgICAkc2NvcGUucGFnZVNpemUgPSAxMDtcblxuICAgICAgICAkc2NvcGUuc2V0RGF0YVNvdXJjZSA9IGZ1bmN0aW9uIChwYWdlX25vKSB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhwYWdlX25vKVxuICAgICAgICAgICAgJHNjb3BlLnNfaXRlbXMgPSBPcmRlcnMucXVlcnkoeyBwYWdlX25vOiBwYWdlX25vLCBwYWdlX3NpemU6IDEwIH0sIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygkc2NvcGUuc19pdGVtcylcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG5cbiAgICAgICAgJHNjb3BlLmN1cnJlbnRQYWdlID0gMFxuXG4gICAgICAgICRzY29wZS5zZXROdW1iZXJPZlBhZ2VzID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgJGh0dHAuZ2V0KCcvYWRtaW5fYXBpL29yZGVyc19jb3VudCcpXG4gICAgICAgICAgICAgICAgLnN1Y2Nlc3MoZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm51bWJlck9mUGFnZXMgPSBNYXRoLmNlaWwoZGF0YS5jb3VudCAvICRzY29wZS5wYWdlU2l6ZSk7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCRzY29wZS5udW1iZXJPZlBhZ2VzKVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgIH1cblxuICAgICAgICBpZiAoJHJvdXRlUGFyYW1zLnBhZ2Vfbm8pIHtcbiAgICAgICAgICAgICRzY29wZS5jdXJyZW50UGFnZSA9ICRyb3V0ZVBhcmFtcy5wYWdlX25vIC0gMVxuICAgICAgICB9XG5cbiAgICAgICAgJHNjb3BlLnNldERhdGFTb3VyY2UoJHNjb3BlLmN1cnJlbnRQYWdlKSAgLy8gZ29vZCBmb3IgZmlyc3QgcGFnZVxuXG4gICAgfV0pXG5cbiAgICAuY29udHJvbGxlcignb3JkZXJEZXRhaWxDb250cm9sbGVyJywgWyckc2NvcGUnLCAnJGh0dHAnLCAnJHJvdXRlUGFyYW1zJywgZnVuY3Rpb24gKCRzY29wZSwgJGh0dHAsICRyb3V0ZVBhcmFtcykge1xuICAgICAgICAkc2NvcGUucmVmID0gJHJvdXRlUGFyYW1zLmlkXG4gICAgICAgICRodHRwLmdldCgnL2FwaS9vcmRlci8nICsgJHJvdXRlUGFyYW1zLmlkKS5zdWNjZXNzKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhKVxuICAgICAgICAgICAgJHNjb3BlLmRldGFpbHMgPSBkYXRhLmRhdGFcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdkZXRhaWxzJylcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCRzY29wZS5kZXRhaWxzKVxuICAgICAgICB9KVxuXG4gICAgICAgICRzY29wZS5pbmZvID0geyBlbWFpbDogJycgfVxuXG4gICAgICAgICRzY29wZS5jYW5TZW5kID0gdHJ1ZVxuXG4gICAgICAgICRzY29wZS5lbWFpbE9yZGVyID0gZnVuY3Rpb24gKCkge1xuXG4gICAgICAgICAgICBjb25zb2xlLmxvZygnZW1haWwgb3JkZXInKVxuICAgICAgICAgICAgY29uc29sZS5sb2coJHNjb3BlLmluZm8pXG5cbiAgICAgICAgICAgICRzY29wZS5jYW5TZW5kID0gZmFsc2VcblxuXG4gICAgICAgICAgICAkaHR0cC5nZXQoJy9hZG1pbl9hcGkvZW1haWxfb3JkZXIvJyArICRyb3V0ZVBhcmFtcy5pZCArICcvJyArICRzY29wZS5pbmZvLmVtYWlsKS5zdWNjZXNzKGZ1bmN0aW9uIChkYXRhKSB7XG5cbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnYmFjaycpXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coZGF0YSlcbiAgICAgICAgICAgICAgICAvLyAgICRzY29wZS5kZXRhaWxzID0gZGF0YS5kYXRhXG4gICAgICAgICAgICAgICAgLy8gIGNvbnNvbGUubG9nKCdkZXRhaWxzJylcbiAgICAgICAgICAgICAgICAvLyAgY29uc29sZS5sb2coJHNjb3BlLmRldGFpbHMpXG4gICAgICAgICAgICB9KVxuXG5cbiAgICAgICAgfVxuXG4gICAgfV0pXG5cbiAgICAuY29udHJvbGxlcignbG9nc0N0cmwnLCBbJyRzY29wZScsICckaHR0cCcsICckcm91dGVQYXJhbXMnLCAnTG9ncycsIGZ1bmN0aW9uICgkc2NvcGUsICRodHRwLCAkcm91dGVQYXJhbXMsIExvZ3MpIHtcblxuICAgICAgICBjb25zb2xlLmxvZygnb3JkZXJzIGxpc3QnKVxuXG4gICAgICAgICRzY29wZS5wYWdlU2l6ZSA9IDEwO1xuXG4gICAgICAgICRzY29wZS5zZXREYXRhU291cmNlID0gZnVuY3Rpb24gKHBhZ2Vfbm8pIHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKHBhZ2Vfbm8pXG4gICAgICAgICAgICAkc2NvcGUuc19pdGVtcyA9IExvZ3MucXVlcnkoeyBwYWdlX25vOiBwYWdlX25vLCBwYWdlX3NpemU6IDEwIH0sIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygkc2NvcGUuc19pdGVtcylcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG5cbiAgICAgICAgJHNjb3BlLmN1cnJlbnRQYWdlID0gMFxuXG4gICAgICAgICRzY29wZS5zZXROdW1iZXJPZlBhZ2VzID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgJGh0dHAuZ2V0KCcvYWRtaW5fYXBpL2xvZ3NfY291bnQnKVxuICAgICAgICAgICAgICAgIC5zdWNjZXNzKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5udW1iZXJPZlBhZ2VzID0gTWF0aC5jZWlsKGRhdGEuY291bnQgLyAkc2NvcGUucGFnZVNpemUpO1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygkc2NvcGUubnVtYmVyT2ZQYWdlcylcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICB9XG5cbiAgICAgICAgaWYgKCRyb3V0ZVBhcmFtcy5wYWdlX25vKSB7XG4gICAgICAgICAgICAkc2NvcGUuY3VycmVudFBhZ2UgPSAkcm91dGVQYXJhbXMucGFnZV9ubyAtIDFcbiAgICAgICAgfVxuXG4gICAgICAgICRzY29wZS5zZXREYXRhU291cmNlKCRzY29wZS5jdXJyZW50UGFnZSkgIC8vIGdvb2QgZm9yIGZpcnN0IHBhZ2VcblxuICAgIH1dKVxuXG4gICAgLmNvbnRyb2xsZXIoJ2N1c3RvbVByaWNlQ29udHJvbGxlcicsIFsnJHNjb3BlJywgJyRodHRwJywgJyRyb3V0ZVBhcmFtcycsICdjb3VudHJpZXMnLFxuICAgICAgICBmdW5jdGlvbiAoJHNjb3BlLCAkaHR0cCwgJHJvdXRlUGFyYW1zLCBjb3VudHJpZXMpIHtcblxuXG4gICAgICAgICAgICAkc2NvcGUucHJpY2VzID0ge31cblxuICAgICAgICAgICAgJHNjb3BlLmFsZXJ0cyA9IFtdO1xuICAgICAgICAgICAgJHNjb3BlLmNsb3NlQWxlcnQgPSBmdW5jdGlvbiAoaW5kZXgpIHtcbiAgICAgICAgICAgICAgICAkc2NvcGUuYWxlcnRzLnNwbGljZShpbmRleCwgMSk7XG4gICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICAkc2NvcGUudXNlZF9pbl9hZG1pbiA9IHRydWVcbiAgICAgICAgICAgICRzY29wZS5jb3VudHJpZXMgPSBjb3VudHJpZXNcblxuICAgICAgICAgICAgJHNjb3BlLmJ1ZiA9IHsgY291bnRyeTogdW5kZWZpbmVkIH1cblxuICAgICAgICAgICAgJHNjb3BlLiR3YXRjaCgnYnVmLmNvdW50cnknLCBmdW5jdGlvbiAobmV3VmFsLCBvbGRWYWwpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnY2hhbmdlZCcsIG5ld1ZhbCwgb2xkVmFsKVxuICAgICAgICAgICAgICAgIGlmIChuZXdWYWwgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgICAgICAkaHR0cC5nZXQoJy9hcGkyL2dldF9kZWZhdWx0X3ByaWNlLycgKyBuZXdWYWwpLnN1Y2Nlc3MoZnVuY3Rpb24gKHByaWNlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhwcmljZSlcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChwcmljZSBpbnN0YW5jZW9mIEFycmF5KSB7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhwcmljZSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUuYnVmLnN0dWR5MV9BID0gcHJpY2VbMF0uc3R1ZHkxXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmJ1Zi5zdHVkeTJfQSA9IHByaWNlWzBdLnN0dWR5MlxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmJ1Zi5yZWcxX0EgPSBwcmljZVswXS5yZWcxXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmJ1Zi5yZWcyX0EgPSBwcmljZVswXS5yZWcyXG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUuYnVmLmNlcnQxX0EgPSBwcmljZVswXS5jZXJ0MVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5idWYuY2VydDJfQSA9IHByaWNlWzBdLmNlcnQyXG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUuYnVmLnN0dWR5MV9CID0gcHJpY2VbMV0uc3R1ZHkxXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmJ1Zi5zdHVkeTJfQiA9IHByaWNlWzFdLnN0dWR5MlxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmJ1Zi5yZWcxX0IgPSBwcmljZVsxXS5yZWcxXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmJ1Zi5yZWcyX0IgPSBwcmljZVsxXS5yZWcyXG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUuYnVmLmNlcnQxX0IgPSBwcmljZVsxXS5jZXJ0MVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5idWYuY2VydDJfQiA9IHByaWNlWzFdLmNlcnQyXG5cblxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoT2JqZWN0LmtleXMocHJpY2UpLmxlbmd0aCAhPT0gMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhwcmljZSlcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUuYnVmLnN0dWR5MV9BID0gcHJpY2Uuc3R1ZHkxXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5idWYuc3R1ZHkyX0EgPSBwcmljZS5zdHVkeTJcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUuYnVmLnJlZzFfQSA9IHByaWNlLnJlZzFcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmJ1Zi5yZWcyX0EgPSBwcmljZS5yZWcyXG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmJ1Zi5jZXJ0MV9BID0gcHJpY2UuY2VydDFcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmJ1Zi5jZXJ0Ml9BID0gcHJpY2UuY2VydDJcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUuYnVmLnN0dWR5MV9CID0gdW5kZWZpbmVkXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5idWYuc3R1ZHkyX0IgPSB1bmRlZmluZWRcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUuYnVmLnJlZzFfQiA9IHVuZGVmaW5lZFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUuYnVmLnJlZzJfQiA9IHVuZGVmaW5lZFxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5idWYuY2VydDFfQiA9IHVuZGVmaW5lZFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUuYnVmLmNlcnQyX0IgPSB1bmRlZmluZWRcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdlbXB0eScpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5idWYgPSB7IGNvdW50cnk6ICRzY29wZS5idWYuY291bnRyeSB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAkaHR0cC5nZXQoJy9hZG1pbl9hcGkvYWNjb3VudC8nICsgJHJvdXRlUGFyYW1zLmlkKS5zdWNjZXNzKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coZGF0YSlcbiAgICAgICAgICAgICAgICAkc2NvcGUudXNlciA9IGRhdGFcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygkc2NvcGUudXNlcilcbiAgICAgICAgICAgICAgICBpZiAoJHNjb3BlLnVzZXIuYWRtaW4pXG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5hZG1pbiA9ICRzY29wZS51c2VyLmFkbWluXG5cbiAgICAgICAgICAgICAgICAkaHR0cC5nZXQoJy9hZG1pbl9hcGkvL2dldF9jdXN0b21fcHJpY2VzLycgKyAkcm91dGVQYXJhbXMuaWQpLnN1Y2Nlc3MoZnVuY3Rpb24gKHByaWNlcykge1xuICAgICAgICAgICAgICAgICAgICAkc2NvcGUucHJpY2VzID0gcHJpY2VzXG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5yZWFkeSA9IHRydWVcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ3ByaWNlcycsIHByaWNlcylcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgfSlcblxuICAgICAgICAgICAgJHNjb3BlLmFkZF9wcmljZSA9IGZ1bmN0aW9uIChidWYpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhidWYpXG4gICAgICAgICAgICAgICAgJHNjb3BlLnByaWNlc1tidWYuY291bnRyeV0gPSBhbmd1bGFyLmNvcHkoYnVmKVxuICAgICAgICAgICAgICAgICRzY29wZS5idWYgPSB7fVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAkc2NvcGUuZGVsX3ByaWNlID0gZnVuY3Rpb24gKGNvdW50cnkpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhjb3VudHJ5KVxuICAgICAgICAgICAgICAgIGRlbGV0ZSAkc2NvcGUucHJpY2VzW2NvdW50cnldXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICRzY29wZS5wb3N0X3ByaWNlcyA9IGZ1bmN0aW9uIChwcmljZXMpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhwcmljZXMpXG4gICAgICAgICAgICAgICAgJGh0dHAucG9zdChcIi9hZG1pbl9hcGkvdXBkYXRlX2N1c3RvbV9wcmljZXMvXCIgKyAkcm91dGVQYXJhbXMuaWQsIHByaWNlcykuc3VjY2VzcyhmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhKVxuICAgICAgICAgICAgICAgICAgICAkc2NvcGUucHJpY2VzX3VwZGF0ZWQgPSB0cnVlXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgfV0pXG5cbiAgICAuY29udHJvbGxlcigncHJpY2VDdHJsJywgW1wiJHNjb3BlXCIsICckcm91dGVQYXJhbXMnLCAnJGh0dHAnLFxuICAgICAgICBmdW5jdGlvbiAoJHNjb3BlLCAkcm91dGVQYXJhbXMsICRodHRwKSB7XG5cblxuICAgICAgICAgICAgJGh0dHAuZ2V0KCcvYXBpMi9nZXRfcHJpY2UvJyArICRyb3V0ZVBhcmFtcy5jb3VudHJ5KS5zdWNjZXNzKGZ1bmN0aW9uIChwcmljZXMpIHtcbiAgICAgICAgICAgICAgICAkc2NvcGUucHJpY2VzID0gcHJpY2VzXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2cocHJpY2VzKVxuICAgICAgICAgICAgfSlcblxuICAgICAgICB9XSlcblxuICAgIC5jb250cm9sbGVyKCd0ZXN0Q3RybCcsIFsnJHNjb3BlJywgJyRodHRwJywgZnVuY3Rpb24gKCRzY29wZSwgJGh0dHApIHtcblxuICAgICAgICAkc2NvcGUudGVzdCA9IHsgc2FtcGxlOiAnMTIzJyB9XG4gICAgICAgICRzY29wZS5yZWcgPSB7XG4gICAgICAgICAgICB0eXBlOiAndycsXG4gICAgICAgICAgICBjbGFzc2VzOiBbeyBcIm5hbWVcIjogXCI0MlwiLCBcImRldGFpbHNcIjogXCJjbGFzczQyXCIgfSwgeyBcIm5hbWVcIjogXCIwMVwiLCBcImRldGFpbHNcIjogXCJjbGFzczAxXCIgfSwge1xuICAgICAgICAgICAgICAgIFwibmFtZVwiOiBcIjE1XCIsXG4gICAgICAgICAgICAgICAgXCJkZXRhaWxzXCI6IFwiY2xhc3MxNVwiXG4gICAgICAgICAgICB9XVxuICAgICAgICB9XG5cblxuICAgIH1dKVxuXG5cbiIsIlwidXNlIHN0cmljdFwiO1xuXG5cbmZ1bmN0aW9uIGxvZ2luX3Byb2MoJGxvY2F0aW9uLCAkcSwgYXV0aFNlcnZpY2UpIHtcbiAgdmFyIGRlZmVycmVkID0gJHEuZGVmZXIoKTtcblxuICBpZiAoIWF1dGhTZXJ2aWNlLmlzU2lnbmVkSW4oKSkge1xuICAgIGRlZmVycmVkLnJlamVjdCgpXG4gICAgJGxvY2F0aW9uLnBhdGgoJy9zaWduaW4nKTtcbiAgfSBlbHNlXG4gICAgZGVmZXJyZWQucmVzb2x2ZSgpXG4gIHJldHVybiBkZWZlcnJlZC5wcm9taXNlO1xufVxuXG52YXIgbG9naW5FbGVtZW50ID0gWyckbG9jYXRpb24nLCAnJHEnLCAnYXV0aFNlcnZpY2UnLCBsb2dpbl9wcm9jXVxuXG52YXIgbG9naW5SZXEgPSB7XG4gIGxvZ2luUmVxdWlyZWQ6IGxvZ2luRWxlbWVudFxufVxuXG4vLyBnZXQgcmVnaXN0cmF0aW9uIGRldGFpbHMgYmVmb3JlIGNvbnRyb2xsZXIsIGZvciAncmVzb2x2ZSdcblxudmFyIGdldF9yZWdpc3RyYXRpb25fZGV0YWlscyA9IFsgJyRyb3V0ZScsICckaHR0cCcsICBmdW5jdGlvbiAoJHJvdXRlLCAkaHR0cCkge1xuICByZXR1cm4gJGh0dHAuZ2V0KCcvYXBpL3JlZ2lzdHJhdGlvbi8nICsgJHJvdXRlLmN1cnJlbnQucGFyYW1zLmlkKVxuICAudGhlbihcbiAgICBmdW5jdGlvbiBzdWNjZXNzKHJlc3BvbnNlKSB7IHJldHVybiByZXNwb25zZS5kYXRhIH0sXG4gICAgZnVuY3Rpb24gZXJyb3IocmVhc29uKSAgICAgeyByZXR1cm4gZmFsc2U7IH1cbiAgKTtcbn1dXG5cbnZhciBnZXRfc3RhZmZzID0gWyAnJHJvdXRlJywgJyRodHRwJywgIGZ1bmN0aW9uICgkcm91dGUsICRodHRwKSB7XG4gIGNvbnNvbGUubG9nKCdnZXQgc3RhZmZzJylcbiAgcmV0dXJuICRodHRwLmdldCgnL2FkbWluX2FwaS9nZXRfc3RhZmZzLycpXG4gICAgLnRoZW4oXG4gICAgZnVuY3Rpb24gc3VjY2VzcyhyZXNwb25zZSkgeyByZXR1cm4gcmVzcG9uc2UuZGF0YSB9LFxuICAgIGZ1bmN0aW9uIGVycm9yKHJlYXNvbikgICAgIHsgcmV0dXJuIGZhbHNlOyB9XG4gICk7XG5cbn1dXG5cblxudmFyIGdldF90aWNrZXRfZGV0YWlscyA9IFsgJyRyb3V0ZScsICckaHR0cCcsICBmdW5jdGlvbiAoJHJvdXRlLCAkaHR0cCkge1xuICByZXR1cm4gJGh0dHAuZ2V0KCcvYXBpX3RpY2tldHMvcXVlcnlfdGlja2V0LycgKyAkcm91dGUuY3VycmVudC5wYXJhbXMuaWQpXG4gICAgLnRoZW4oXG4gICAgZnVuY3Rpb24gc3VjY2VzcyhyZXNwb25zZSkgeyByZXR1cm4gcmVzcG9uc2UuZGF0YSB9LFxuICAgIGZ1bmN0aW9uIGVycm9yKHJlYXNvbikgICAgIHsgcmV0dXJuIGZhbHNlOyB9XG4gICk7XG59XVxuXG5cbm1vZHVsZS5leHBvcnRzID0gWyckcm91dGVQcm92aWRlcicsICckbG9jYXRpb25Qcm92aWRlcicsIGZ1bmN0aW9uICgkcm91dGVQcm92aWRlciwgJGxvY2F0aW9uUHJvdmlkZXIpIHtcblxuICAkcm91dGVQcm92aWRlci5cbiAgICB3aGVuKCcvJywge3RlbXBsYXRlVXJsOiAncGFydGlhbHMvYWRtaW5faG9tZS5odG1sJywgY29udHJvbGxlcjogJ2hvbWVDdHJsJ30pLlxuICAgIHdoZW4oJy9hYm91dCcsIHt0ZW1wbGF0ZVVybDogJ3BhcnRpYWxzL2Fib3V0Lmh0bWwnLCBjb250cm9sbGVyOiAnaG9tZUN0cmwnfSkuXG4gICAgd2hlbignL3doeXVzJywge3RlbXBsYXRlVXJsOiAncGFydGlhbHMvd2h5dXMuaHRtbCcsIGNvbnRyb2xsZXI6ICdob21lQ3RybCd9KS5cbiAgICB3aGVuKCcvdGVybXMnLCB7dGVtcGxhdGVVcmw6ICdwYXJ0aWFscy90ZXJtcy5odG1sJywgY29udHJvbGxlcjogJ2hvbWVDdHJsJ30pLlxuICAgIHdoZW4oJy9zaWdudXBzJywge3RlbXBsYXRlVXJsOiAncGFydGlhbHMvYWRtaW5fc2lnbnVwcy5odG1sJywgY29udHJvbGxlcjogJ3NpZ251cHNDdHJsJ30pLlxuICAgIHdoZW4oJy9yZWdpc3RyYXRpb25zJywge3RlbXBsYXRlVXJsOiAncGFydGlhbHMvYWRtaW5fcmVnaXN0cmF0aW9ucy5odG1sJywgY29udHJvbGxlcjogJ3JlZ2lzdHJhdGlvbnNDdHJsJywgaXNQcml2OiB0cnVlLCByZXNvbHZlOiBsb2dpblJlcX0pLlxuICAgIHdoZW4oJy9yZWdpc3RyYXRpb24vOmlkJywge3RlbXBsYXRlVXJsOiAncGFydGlhbHMvYWRtaW5fcmVnaXN0cmF0aW9uLmh0bWwnLCBjb250cm9sbGVyOiAncmVnaXN0cmF0aW9uRGV0YWlsQ29udHJvbGxlcicsIGlzUHJpdjogdHJ1ZSxcbiAgICAgICAgcmVzb2x2ZToge1xuICAgICAgICAgICAgbG9naW5SZXE6bG9naW5FbGVtZW50LFxuICAgICAgICAgICAgcmVnc3RybiA6Z2V0X3JlZ2lzdHJhdGlvbl9kZXRhaWxzXG4gICAgICAgICAgfX0pLlxuXG4gICAgd2hlbignL3RpY2tldC86aWQnLCB7dGVtcGxhdGVVcmw6ICdwYXJ0aWFscy9hZG1pbl90aWNrZXQuaHRtbCcsIGNvbnRyb2xsZXI6ICd0aWNrZXRDb250cm9sbGVyJywgY29udHJvbGxlckFzOidkZXRhaWxzJyAsIGlzUHJpdjogdHJ1ZSxcbiAgICAgIHJlc29sdmU6IHtcbiAgICAgICAgbG9naW5SZXE6bG9naW5FbGVtZW50LFxuICAgICAgICB0aWNrZXQgOmdldF90aWNrZXRfZGV0YWlsc1xuICAgICAgfX0pLlxuXG4gICAgd2hlbignL29yZGVycycsIHt0ZW1wbGF0ZVVybDogJ3BhcnRpYWxzL2FkbWluX29yZGVycy5odG1sJywgY29udHJvbGxlcjogJ29yZGVyc0N0cmwnLCBpc1ByaXY6IHRydWV9KS5cbiAgICB3aGVuKCcvb3JkZXIvOmlkJywge3RlbXBsYXRlVXJsOiAncGFydGlhbHMvYWRtaW5fb3JkZXIuaHRtbCcsIGNvbnRyb2xsZXI6ICdvcmRlckRldGFpbENvbnRyb2xsZXInLCBpc1ByaXY6IHRydWUsIHJlc29sdmU6IGxvZ2luUmVxLCByZWxvYWRPblNlYXJjaDogZmFsc2V9KS5cbiAgICB3aGVuKCcvdHJhZGVtYXJrcycsIHt0ZW1wbGF0ZVVybDogJ3BhcnRpYWxzL2FkbWluX3RyYWRlbWFya3MuaHRtbCcsIGNvbnRyb2xsZXI6ICd0cmFkZW1hcmtzQ3RybCd9KS5cbiAgICB3aGVuKCcvbG9ncycsIHt0ZW1wbGF0ZVVybDogJ3BhcnRpYWxzL2FkbWluX2xvZ3MuaHRtbCcsIGNvbnRyb2xsZXI6ICdsb2dzQ3RybCd9KS5cbiAgICB3aGVuKCcvc2lnbmluJywge3RlbXBsYXRlVXJsOiAncGFydGlhbHMvYWRtaW5fc2lnbmluLmh0bWwnLCBjb250cm9sbGVyOiAnYWRtaW5fVXNlckNvbnRyb2xsZXInfSkuXG4gICAgd2hlbignL2Rhc2hib2FyZCcsIHt0ZW1wbGF0ZVVybDogJ3BhcnRpYWxzL2Rhc2hib2FyZC5odG1sJywgY29udHJvbGxlcjogJ1VzZXJDb250cm9sbGVyJ30pLlxuICAgIHdoZW4oJy9zaWdub3V0Jywge3RlbXBsYXRlVXJsOiAncGFydGlhbHMvY29tbWluZy5odG1sJywgY29udHJvbGxlcjogJ1VzZXJDb250cm9sbGVyJ30pLlxuICAgIHdoZW4oJy9jbGFzc2VzJywge3RlbXBsYXRlVXJsOiAncGFydGlhbHMvY2xhc3Nlcy5odG1sJywgY29udHJvbGxlcjogJ2NsYXNzZXNDdHJsJ30pLlxuICAgIHdoZW4oJy9wcmljZS86Y291bnRyeScsIHt0ZW1wbGF0ZVVybDogJ3BhcnRpYWxzL2FkbWluX3ByaWNlLmh0bWwnLCBjb250cm9sbGVyOiAncHJpY2VDdHJsJ30pLlxuICAgIHdoZW4oJy9tZXNzYWdlcycsIHt0ZW1wbGF0ZVVybDogJ3BhcnRpYWxzL2FkbWluX21lc3NhZ2VzLmh0bWwnLCBjb250cm9sbGVyOiAnYWRtaW5fTWVzc2FnZXNDb250cm9sbGVyJywgaXNQcml2OiB0cnVlLCByZXNvbHZlOiBsb2dpblJlcX0pLlxuICAgIHdoZW4oJy9wcm9maWxlLzppZC86YmFja2xpbms/Jywge3RlbXBsYXRlVXJsOiAncGFydGlhbHMvYWRtaW5fcHJvZmlsZS5odG1sJywgY29udHJvbGxlcjogJ3Byb2ZpbGVDb250cm9sbGVyJywgaXNQcml2OiB0cnVlLCByZXNvbHZlOiBsb2dpblJlcX0pLlxuICAgIHdoZW4oJy9hY2NvdW50LzppZCcsIHt0ZW1wbGF0ZVVybDogJ3BhcnRpYWxzL2FkbWluX2FjY291bnQuaHRtbCcsIGNvbnRyb2xsZXI6ICdhY2NvdW50Q29udHJvbGxlcicsIGlzUHJpdjogdHJ1ZX0pLlxuICAgIHdoZW4oJy9jdXN0b21fcHJpY2VzLzppZCcsIHt0ZW1wbGF0ZVVybDogJ3BhcnRpYWxzL2FkbWluX2N1c3RvbV9wcmljZXMuaHRtbCcsIGNvbnRyb2xsZXI6ICdjdXN0b21QcmljZUNvbnRyb2xsZXInLCBpc1ByaXY6IHRydWUsIHJlc29sdmU6IGxvZ2luUmVxfSkuXG4gICAgd2hlbignL2NhcnRzJywge3RlbXBsYXRlVXJsOiAncGFydGlhbHMvYWRtaW5fY2FydHMuaHRtbCcsIGNvbnRyb2xsZXI6ICdjYXJ0c0N0cmwnLCAgIGlzUHJpdjogdHJ1ZX0pLlxuICAgIHdoZW4oJy9jYXJ0LzppZCcsIHt0ZW1wbGF0ZVVybDogJ3BhcnRpYWxzL2FkbWluX2NhcnQuaHRtbCcsIGNvbnRyb2xsZXI6ICdjYXJ0Q29udHJvbGxlcicsIGlzUHJpdjogdHJ1ZSwgcmVzb2x2ZTogbG9naW5SZXF9KS5cbiAgICB3aGVuKCcvYWRkX2NhcnQvOmlkLzp0cmFuc2FjdGlvbicsIHt0ZW1wbGF0ZVVybDogJ3BhcnRpYWxzL2FkbWluX2FkZF9jYXJ0Lmh0bWwnLCBjb250cm9sbGVyOiAnY2FydEFkZENvbnRyb2xsZXInLCBpc1ByaXY6IHRydWUsIHJlc29sdmU6IGxvZ2luUmVxfSkuXG4gICAgd2hlbignL2NvcHlfY2FydC86aWQnLCB7dGVtcGxhdGVVcmw6ICdwYXJ0aWFscy9hZG1pbl9jb3B5X2NhcnQuaHRtbCcsIGNvbnRyb2xsZXI6ICdjYXJ0Q29weUNvbnRyb2xsZXInLCBpc1ByaXY6IHRydWUsIHJlc29sdmU6IGxvZ2luUmVxfSkuXG5cblxuICAgIHdoZW4oJy9zZWFyY2hfY2xhc3NlcycsIHt0ZW1wbGF0ZVVybDogJ3BhcnRpYWxzL3NlYXJjaF9jbGFzc2VzLmh0bWwnLCBjb250cm9sbGVyOiAnc2VhcmNoX2NsYXNzZXNDb250cm9sbGVyJ30pLlxuICAgIHdoZW4oJy9zdXBwb3J0ZWRfYnJvd3NlcnMnLCB7dGVtcGxhdGVVcmw6ICdwYXJ0aWFscy9zdXBwb3J0ZWRfYnJvd3NlcnMuaHRtbCcsIGNvbnRyb2xsZXI6ICdzaWdudXBDdHJsJ30pLlxuICAgIHdoZW4oJy90ZXN0X3NlbGVjdGlvbicsIHt0ZW1wbGF0ZVVybDogJ3BhcnRpYWxzL3Rlc3Rfc2VsZWN0aW9uLmh0bWwnLCBjb250cm9sbGVyOiAndGVzdEN0cmwnfSkuXG4gICAgb3RoZXJ3aXNlKHtyZWRpcmVjdFRvOiAnLyd9KTtcbiAgICAkbG9jYXRpb25Qcm92aWRlci5odG1sNU1vZGUodHJ1ZSlcbn1dXG4iLCJcInVzZSBzdHJpY3RcIjtcblxuLypnbG9iYWwgYW5ndWxhciAqL1xuXG5cbmZ1bmN0aW9uIEFkbWluU2VydmljZXMoJHEsICRodHRwKSB7XG5cbiAgdGhpcy5jcmVhdGVfdGlja2V0ID0gZnVuY3Rpb24gKHJlZ2lzdHJhdGlvbikge1xuXG4gICAgY29uc29sZS5sb2coJ2NyZWF0ZV90aWNrZXQgc2VydmljZXMnKVxuXG4gICAgdmFyIGRlZmVycmVkID0gJHEuZGVmZXIoKTtcblxuICAgICRodHRwLnBvc3QoJy9hcGlfdGlja2V0cy9jcmVhdGVfdGlja2V0JywgcmVnaXN0cmF0aW9uKS5zdWNjZXNzKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICBkZWZlcnJlZC5yZXNvbHZlKGRhdGEpXG5cbiAgICB9KS5lcnJvcihmdW5jdGlvbiAoZXJyb3IpIHtcbiAgICAgIGRlZmVycmVkLnJlc29sdmUoe30pO1xuICAgIH0pO1xuXG4gICAgcmV0dXJuIGRlZmVycmVkLnByb21pc2U7XG5cbiAgfTtcblxuICB0aGlzLmFwcGVuZF90aWNrZXRfZWxlbWVudCA9IGZ1bmN0aW9uICh0a3RfZWxlbWVudCkge1xuXG4gICAgdmFyIGRlZmVycmVkID0gJHEuZGVmZXIoKTtcbiAgICBjb25zb2xlLmxvZyh0a3RfZWxlbWVudClcblxuICAgICRodHRwLnBvc3QoJy9hcGlfdGlja2V0cy9hcHBlbmRfdGlja2V0X2VsZW1lbnQnLCB0a3RfZWxlbWVudCkuc3VjY2VzcyhmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgZGVmZXJyZWQucmVzb2x2ZShkYXRhKVxuICAgIH0pLmVycm9yKGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgZGVmZXJyZWQucmVzb2x2ZSh7fSk7XG4gICAgfSk7XG5cbiAgICByZXR1cm4gZGVmZXJyZWQucHJvbWlzZTtcblxuICB9XG5cbiAgdGhpcy5kZWxldGVfdGlja2V0X2VsZW1lbnQgPSBmdW5jdGlvbiAodGlja2V0X2lkLCB0a3RfZWxlbWVudCkge1xuXG4gICAgdmFyIGRlZmVycmVkID0gJHEuZGVmZXIoKTtcblxuICAgICRodHRwLmdldCgnL2FwaV90aWNrZXRzL2RlbGV0ZV90aWNrZXRfZWxlbWVudC8nICsgdGlja2V0X2lkICsgJy8nICsgdGt0X2VsZW1lbnQudXVpZCkuc3VjY2VzcyhmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgZGVmZXJyZWQucmVzb2x2ZShkYXRhKVxuICAgIH0pLmVycm9yKGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgZGVmZXJyZWQucmVzb2x2ZSh7fSk7XG4gICAgfSk7XG4gICAgcmV0dXJuIGRlZmVycmVkLnByb21pc2U7XG5cblxuICB9XG5cblxuXG4gIHRoaXMuZ2V0X3JlZ2lzdHJhdGlvbiA9IGZ1bmN0aW9uIChpZCkge1xuXG4gICAgY29uc29sZS5sb2coJ2dldCByZWcnKVxuXG4gICAgdmFyIGRlZmVycmVkID0gJHEuZGVmZXIoKTtcblxuICAgICRodHRwLmdldCgnL2FwaS9yZWdpc3RyYXRpb24vJyArIGlkICsgJz9hc2RmYXNkZicpLnN1Y2Nlc3MoZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgIGNvbnNvbGUubG9nKCdnZXRnZXQnKVxuICAgICAgY29uc29sZS5sb2coZGF0YSlcbiAgICAgIGRlZmVycmVkLnJlc29sdmUoZGF0YS5kYXRhKVxuICAgIH0pLmVycm9yKGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgZGVmZXJyZWQucmVzb2x2ZSh7fSk7XG4gICAgfSk7XG5cbiAgICByZXR1cm4gZGVmZXJyZWQucHJvbWlzZTtcblxuICB9XG5cbiAgdGhpcy5nZXRfcHJvZmlsZXMgPSBmdW5jdGlvbiAodXNlcl9pZCkge1xuICAgIHZhciBkZWZlcnJlZCA9ICRxLmRlZmVyKClcblxuICAgICRodHRwLmdldCgnL2FkbWluX2FwaS9wcm9maWxlcy8nICsgdXNlcl9pZCkuc3VjY2VzcyhmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgY29uc29sZS5sb2coJ2dldGdldDEyMycpXG4gICAgICBjb25zb2xlLmxvZyhkYXRhKVxuICAgICAgZGVmZXJyZWQucmVzb2x2ZShkYXRhKVxuICAgIH0pLmVycm9yKGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgZGVmZXJyZWQucmVzb2x2ZSh7fSk7IC8vIFRPRE86IHRyaWdnZXIgYW4gZXJyXG4gICAgfSk7XG5cbiAgICByZXR1cm4gZGVmZXJyZWQucHJvbWlzZVxuICB9XG5cblxuICB0aGlzLmdldF9jYXJ0ID0gZnVuY3Rpb24gKHVzZXJfaWQpIHtcbiAgICB2YXIgZGVmZXJyZWQgPSAkcS5kZWZlcigpXG5cbiAgICAkaHR0cC5nZXQoJy9hZG1pbl9hcGkvY2FydC8nICsgdXNlcl9pZCkuc3VjY2VzcyhmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgY29uc29sZS5sb2coJ2dldGdldDEyMycpXG4gICAgICBjb25zb2xlLmxvZyhkYXRhKVxuICAgICAgZGVmZXJyZWQucmVzb2x2ZShkYXRhKVxuICAgIH0pLmVycm9yKGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgZGVmZXJyZWQucmVzb2x2ZSh7fSk7IC8vIFRPRE86IHRyaWdnZXIgYW4gZXJyXG4gICAgfSk7XG5cbiAgICByZXR1cm4gZGVmZXJyZWQucHJvbWlzZVxuICB9XG5cblxufVxuXG5hbmd1bGFyLm1vZHVsZSgnYWRtaW5fdGtzLnNlcnZpY2VzJywgW10pLnZhbHVlKCd2ZXJzaW9uJywgJzAuMScpXG5cbiAgLmZhY3RvcnkoJ1NpZ251cHMnLCBbJyRyZXNvdXJjZScsIGZ1bmN0aW9uICgkcmVzb3VyY2UpIHtcbiAgICByZXR1cm4gJHJlc291cmNlKCcvYWRtaW5fYXBpL3NpZ251cHMnKVxuICB9XSlcblxuICAuZmFjdG9yeSgnUmVnaXN0cmF0aW9ucycsIFsnJHJlc291cmNlJywgZnVuY3Rpb24gKCRyZXNvdXJjZSkge1xuICAgIHJldHVybiAkcmVzb3VyY2UoJy9hZG1pbl9hcGkvcmVnaXN0cmF0aW9ucycpXG4gIH1dKVxuXG4gIC5mYWN0b3J5KCdPcmRlcnMnLCBbJyRyZXNvdXJjZScsIGZ1bmN0aW9uICgkcmVzb3VyY2UpIHtcbiAgICByZXR1cm4gJHJlc291cmNlKCcvYWRtaW5fYXBpL29yZGVycycpXG4gIH1dKVxuXG4gIC5mYWN0b3J5KCdDYXJ0cycsIFsnJHJlc291cmNlJywgZnVuY3Rpb24gKCRyZXNvdXJjZSkge1xuICAgIHJldHVybiAkcmVzb3VyY2UoJy9hZG1pbl9hcGkvY2FydHMnKVxuICB9XSlcblxuXG4gIC5mYWN0b3J5KCdMb2dzJywgWyckcmVzb3VyY2UnLCBmdW5jdGlvbiAoJHJlc291cmNlKSB7XG4gICAgcmV0dXJuICRyZXNvdXJjZSgnL2FkbWluX2FwaS9sb2dzJylcbiAgfV0pXG5cbiAgLmZhY3RvcnkoJycsIFsnJHJlc291cmNlJywgZnVuY3Rpb24gKCRyZXNvdXJjZSkge1xuICAgIHJldHVybiAkcmVzb3VyY2UoJy9hZG1pbl9hcGkvbG9ncycpXG4gIH1dKVxuXG4gIC5zZXJ2aWNlKCdBZG1pblNlcnZpY2VzJywgWyckcScsICckaHR0cCcsIEFkbWluU2VydmljZXNdKTtcbiIsIlwidXNlIHN0cmljdFwiO1xuXG5leHBvcnRzLmNhcnQgPSBmdW5jdGlvbiAoJHNjb3BlLCAkaHR0cCwgJHJvdXRlUGFyYW1zLCAkbW9kYWwsIGNvdW50cmllcykge1xuXG4gICAgY29uc29sZS5sb2coJ3JvdXRlUGFyYW1zJywgJHJvdXRlUGFyYW1zKVxuXG4gICAgJHNjb3BlLmdldF9jYXJ0ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAkaHR0cC5nZXQoJy9hZG1pbl9hcGkvY2FydC8nICsgJHJvdXRlUGFyYW1zLmlkKS50aGVuKGZ1bmN0aW9uIChkYXRhKSB7XG5cbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdjYXJ0IGRhdGEnLCBkYXRhKVxuICAgICAgICAgICAgY29uc29sZS5sb2coJz8/PycsIGRhdGEuZGF0YSA9PT0gJ251bGwnKVxuXG4gICAgICAgICAgICBpZiAoZGF0YS5kYXRhICE9PSAnbnVsbCcpIHtcbiAgICAgICAgICAgICAgICAkc2NvcGUuY2FydCA9IGRhdGEuZGF0YVxuICAgICAgICAgICAgICAgICRzY29wZS50b3RhbCA9IDBcbiAgICAgICAgICAgICAgICAkc2NvcGUuY2FydC5pdGVtcy5tYXAoZnVuY3Rpb24gKGl0ZW0pIHtcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnRvdGFsID0gJHNjb3BlLnRvdGFsICsgaXRlbS5hbW91bnRcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgfVxuICAgICAgICB9KVxuICAgIH1cblxuICAgICRzY29wZS5kZWxldGVfaXRlbSA9IGZ1bmN0aW9uIChjYXJ0X2lkLCByZWMpIHtcblxuICAgICAgICBpZiAoY29uZmlybSgnRG8geW91IHJlYWxseSB3YW50IHRvIGRlbGV0ZSB0aGlzIGl0ZW0/JykpIHtcblxuICAgICAgICAgICAgY29uc29sZS5sb2coJ2RlbGV0ZScsIGNhcnRfaWQpXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhyZWMpXG5cbiAgICAgICAgICAgICRodHRwLmdldCgnL2FkbWluX2FwaS9kZWxldGVfY2FydF9pdGVtLycgKyBjYXJ0X2lkICsgJy8nICsgcmVjLl9pZCkuc3VjY2VzcyhmdW5jdGlvbiAoZGF0YSkge1xuXG4gICAgICAgICAgICAgICAgJHNjb3BlLmdldF9jYXJ0KClcblxuICAgICAgICAgICAgfSlcblxuICAgICAgICB9XG4gICAgfVxuXG4gICAgJHNjb3BlLmdldF9jYXJ0KClcblxufVxuIiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbmV4cG9ydHMuY2FydF9hZGQgPSBmdW5jdGlvbiAoJHNjb3BlLCAkaHR0cCwgJHJvdXRlUGFyYW1zLCAkbG9jYXRpb24sIGNvdW50cmllcywgQWRtaW5TZXJ2aWNlcykge1xuXG4gICAgJHNjb3BlLmNvdW50cmllcyA9IGNvdW50cmllc1xuXG4gICAgJHNjb3BlLnRyYW5zYWN0aW9uID0gJHJvdXRlUGFyYW1zLnRyYW5zYWN0aW9uXG5cbiAgICAkc2NvcGUucm93ID0ge1xuICAgICAgICB0eXBlOiAndycsXG4gICAgICAgIHNlcnZpY2U6ICRyb3V0ZVBhcmFtcy50cmFuc2FjdGlvbixcbiAgICAgICAgYW1vdW50OiAwLjAsXG4gICAgICAgIG5vdGVzOiAnJyxcbiAgICAgICAgcHJvZmlsZV9pZDogbnVsbFxuICAgIH1cblxuICAgIEFkbWluU2VydmljZXMuZ2V0X2NhcnQoJHJvdXRlUGFyYW1zLmlkKS50aGVuKGZ1bmN0aW9uIChjYXJ0KSB7XG5cbiAgICAgICAgJHNjb3BlLmNhcnQgPSBjYXJ0XG5cbiAgICAgICAgaWYgKGNhcnQudXNlcl9pZCkge1xuICAgICAgICAgICAgQWRtaW5TZXJ2aWNlcy5nZXRfcHJvZmlsZXMoY2FydC51c2VyX2lkKS50aGVuKGZ1bmN0aW9uIChwcm9maWxlcykge1xuICAgICAgICAgICAgICAgICRzY29wZS5wcm9maWxlcyA9IHByb2ZpbGVzXG4gICAgICAgICAgICB9KVxuICAgICAgICB9XG4gICAgfSlcblxuICAgICRzY29wZS5hZGRfaXRlbSA9IGZ1bmN0aW9uIChjYXJ0X2lkLCByb3cpIHtcblxuICAgICAgICBjb25zb2xlLmxvZygnY2FydF9pZCcsIGNhcnRfaWQpXG4gICAgICAgIGNvbnNvbGUubG9nKCdyb3cnLCByb3cpXG5cbiAgICAgICAgcm93LnNlbGVjdGVkID0gdHJ1ZVxuXG4gICAgICAgICRodHRwLnBvc3QoXCIvYWRtaW5fYXBpL2FkZF9jYXJ0X2l0ZW0vXCIgKyBjYXJ0X2lkLCByb3cpLnN1Y2Nlc3MoZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGEpXG4gICAgICAgICAgICAvLyRzY29wZS5wcmljZXNfdXBkYXRlZCA9IHRydWVcblxuICAgICAgICAgICAgJGxvY2F0aW9uLnBhdGgoJy9jYXJ0LycgKyAkc2NvcGUuY2FydC51c2VyX2lkKVxuICAgICAgICB9KTtcblxuICAgIH1cbn1cbiIsImltcG9ydCB7IGNhcnQgfSBmcm9tIFwiLi4vLi4vLi4vLi4vYXBwX3RzL2pzL3dlYi93ZWJfY29udHJvbGxlcnNfY2FydFwiO1xuXG5cInVzZSBzdHJpY3RcIjtcblxuZXhwb3J0cy5jYXJ0X2NvcHkgPSBmdW5jdGlvbiAoJHNjb3BlLCAkaHR0cCwgJHJvdXRlUGFyYW1zLCAkbG9jYXRpb24sIGNvdW50cmllcywgQWRtaW5TZXJ2aWNlcywgQ2FydHMpIHtcblxuICAgICRzY29wZS5yb3cgPSB7XG4gICAgICAgIHByb2ZpbGVfaWQ6IG51bGxcbiAgICB9XG5cbiAgICAkc2NvcGUuZmluZF9jYXJ0ID0gZnVuY3Rpb24gKGNhcnRfaWQsIHJvdykge1xuXG4gICAgICAgICRodHRwLmdldCgnL2FkbWluX2FwaS9hY2NvdW50X2J5X2VtYWlsLycgKyAkc2NvcGUucm93LmZyb21fZW1haWwpLnN1Y2Nlc3MoZnVuY3Rpb24gKHVzZXIpIHtcblxuICAgICAgICAgICAgaWYgKHVzZXIpIHtcblxuICAgICAgICAgICAgICAgIEFkbWluU2VydmljZXMuZ2V0X2NhcnQodXNlci5faWQpLnRoZW4oZnVuY3Rpb24gKGNhcnQpIHtcblxuICAgICAgICAgICAgICAgICAgICAkc2NvcGUuc2NhcnQgPSBjYXJ0XG5cbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnRvdGFsID0gMFxuICAgICAgICAgICAgICAgICAgICAkc2NvcGUuc2NhcnQuaXRlbXMubWFwKGZ1bmN0aW9uIChpdGVtKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUudG90YWwgPSAkc2NvcGUudG90YWwgKyBpdGVtLmFtb3VudFxuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICRzY29wZS5zY2FydCA9IG51bGxcbiAgICAgICAgICAgIH1cblxuICAgICAgICB9KS5lcnJvcihmdW5jdGlvbiAoZXJyb3IpIHtcblxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICAkc2NvcGUuY29weV9jYXJ0ID0gZnVuY3Rpb24gKCkge1xuXG4gICAgICAgIGNvbnNvbGUubG9nKCdjb3B5IGNhcnQnKVxuXG4gICAgICAgIC8vICBcbiAgICAgICAgaWYgKCRzY29wZS5yb3cucHJvZmlsZV9pZCkge1xuXG4gICAgICAgICAgICB2YXIgdXJsID0gJy9hZG1pbl9hcGkvY29weV9pdGVtc190b19jYXJ0P2NhcnRfaWQ9JyArICRzY29wZS5zY2FydC5faWQgKyAnJm5ld19jYXJ0X2lkPScgKyAkc2NvcGUuY2FydC5faWQgKyAnJm5ld19wcm9maWxlX2lkPScgKyAkc2NvcGUucm93LnByb2ZpbGVfaWRcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgJGh0dHAuZ2V0KHVybClcbiAgICAgICAgICAgICAgICAuc3VjY2VzcyhmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5tc2cgPSAnSXRlbXMgY29waWVkJ1xuICAgICAgICAgICAgICAgICAgICAkc2NvcGUuc2NhcnQgPSBudWxsXG5cbiAgICAgICAgICAgICAgICB9KVxuXG4gICAgICAgIH0gZWxzZVxuICAgICAgICAgICAgJHNjb3BlLm1zZyA9ICdwcm9maWxlIGlzIGVtcHR5J1xuXG4gICAgfVxuXG5cbiAgICAkc2NvcGUuaXNDYXJ0RW1wdHkgPSBmdW5jdGlvbiAoY2FydCkge1xuICAgICAgICBpZiAoIWNhcnQpXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiBjYXJ0Lml0ZW1zLmxlbmd0aCA9PT0gMFxuICAgICAgICB9XG4gICAgfVxuXG4gICAgQWRtaW5TZXJ2aWNlcy5nZXRfY2FydCgkcm91dGVQYXJhbXMuaWQpLnRoZW4oZnVuY3Rpb24gKGNhcnQpIHtcblxuICAgICAgICAkc2NvcGUuY2FydCA9IGNhcnRcblxuICAgICAgICBpZiAoY2FydC51c2VyX2lkKSB7XG4gICAgICAgICAgICBBZG1pblNlcnZpY2VzLmdldF9wcm9maWxlcyhjYXJ0LnVzZXJfaWQpLnRoZW4oZnVuY3Rpb24gKHByb2ZpbGVzKSB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLnByb2ZpbGVzID0gcHJvZmlsZXNcbiAgICAgICAgICAgIH0pXG4gICAgICAgIH1cblxuICAgIH0pXG59XG4iLCJcInVzZSBzdHJpY3RcIjtcblxuZXhwb3J0cy5jYXJ0cyA9IGZ1bmN0aW9uICgkc2NvcGUsICRodHRwLCAkcm91dGVQYXJhbXMsIENhcnRzKSB7XG5cbiAgICBjb25zb2xlLmxvZygnY2FydHMgbGlzdCcpXG5cbiAgICAkc2NvcGUucGFnZVNpemUgPSAxMDtcblxuICAgICRzY29wZS5zZXREYXRhU291cmNlID0gZnVuY3Rpb24gKHBhZ2Vfbm8pIHtcbiAgICAgICAgY29uc29sZS5sb2cocGFnZV9ubylcbiAgICAgICAgdGhpcy5zX2l0ZW1zID0gQ2FydHMucXVlcnkoeyBwYWdlX25vOiBwYWdlX25vLCBwYWdlX3NpemU6IDEwIH0sIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgJHNjb3BlLmN1cnJlbnRQYWdlID0gMFxuXG4gICAgJHNjb3BlLnNldE51bWJlck9mUGFnZXMgPSBmdW5jdGlvbiAoKSB7XG5cbiAgICAgICAgJGh0dHAuZ2V0KCcvYWRtaW5fYXBpL2NhcnRzX2NvdW50JylcbiAgICAgICAgICAgIC5zdWNjZXNzKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLm51bWJlck9mUGFnZXMgPSBNYXRoLmNlaWwoZGF0YS5jb3VudCAvICRzY29wZS5wYWdlU2l6ZSk7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coJHNjb3BlLm51bWJlck9mUGFnZXMpXG4gICAgICAgICAgICB9KVxuICAgIH1cblxuICAgIGlmICgkcm91dGVQYXJhbXMucGFnZV9ubykge1xuICAgICAgICAkc2NvcGUuY3VycmVudFBhZ2UgPSAkcm91dGVQYXJhbXMucGFnZV9ubyAtIDFcbiAgICB9XG5cbiAgICAkc2NvcGUuc2V0RGF0YVNvdXJjZSgkc2NvcGUuY3VycmVudFBhZ2UpICAvLyBnb29kIGZvciBmaXJzdCBwYWdlXG5cbn0iLCJcInVzZSBzdHJpY3RcIjtcbi8qIGdsb2JhbCBhbmd1bGFyICovXG4vKiBEaXJlY3RpdmVzICovXG5cbnZhciB1dGlsaXRpZXMgPSByZXF1aXJlKCcuLi9tb2R1bGVzL3V0aWxpdGllcy5qcycpXG52YXIgY291bnRyaWVzID0gcmVxdWlyZSgnY291bnRyaWVzJylcblxuYW5ndWxhci5tb2R1bGUoJ3Byb2ZpbGUuZGlyZWN0aXZlcycsIFtdKS5cbiAgZGlyZWN0aXZlKCdhcHBWZXJzaW9uJywgWyd2ZXJzaW9uJywgZnVuY3Rpb24gKHZlcnNpb24pIHtcbiAgICByZXR1cm4gZnVuY3Rpb24gKHNjb3BlLCBlbG0sIGF0dHJzKSB7XG4gICAgICBlbG0udGV4dCh2ZXJzaW9uKTtcbiAgICB9O1xuICB9XSkuZGlyZWN0aXZlKCdwcm9maWxlMTIzJywgZnVuY3Rpb24gKCkge1xuICAgIGNvbnNvbGUubG9nKCdpbiBwcm9maWxlIGhlbGxvJylcbiAgICByZXR1cm4ge1xuICAgICAgcmVzdHJpY3Q6ICdFJyxcbiAgICAgIHNjb3BlOiB7XG4gICAgICAgIG5hbWU6ICdAJyxcbiAgICAgICAgZGF0YXNvdXJjZSA6ICc9JyAgICAgICAvLyBwYXNzIGEganNvbiBvYmplY3RcbiAgICAgIH0sXG4gICAgICB0ZW1wbGF0ZTogJzxzcGFuPkhlbGxvIHt7bmFtZX19PC9zcGFuPidcbiAgICB9XG4gIH0pXG5cbiAgLy8gdG8gZ2V0IG5hbWUsIGNvdW50cnkgb2YgaW5jLCBjb250YWN0IGFuZCBlbWFpbFxuXG4gIC5kaXJlY3RpdmUoJ3Byb2ZpbGVOYW1lcycsIFsnJGh0dHAnLCAnY291bnRyaWVzJywgZnVuY3Rpb24gKCRodHRwLCBjb3VudHJpZXMpIHtcbiAgICByZXR1cm4ge1xuICAgICAgcmVzdHJpY3Q6ICdFJyxcbiAgICAgIHNjb3BlOiB7XG4gICAgICAgIG5hbWU6ICdAJyxcbiAgICAgICAgZGF0YXNvdXJjZTogJz0nXG4gICAgICB9LFxuICAgICAgdGVtcGxhdGVVcmw6ICcvcGFydGlhbHMvcHJvZmlsZU5hbWVzLmh0bWwnLFxuICAgICAgbGluazogZnVuY3Rpb24gKHNjb3BlLCBlbCwgYXR0cikge1xuXG4gICAgICAgIGlmICghc2NvcGUuZGF0YXNvdXJjZS5pbmNfY291bnRyeSlcbi8vICAgICAgICAgICRodHRwLmdldCgnL2FwaTIvZ2V0X2dlbz90ZXN0X2lwPTE3NC41NC4xNjUuMjA2Jykuc3VjY2VzcyhmdW5jdGlvbiAoZ2VvKSB7XG4vLyAgICAgICAgICAkaHR0cC5nZXQoJy9hcGkyL2dldF9nZW8/dGVzdF9pcD0xMTIuMjEwLjE1LjEwNycpLnN1Y2Nlc3MoZnVuY3Rpb24gKGdlbykge1xuICAgICAgICAgICRodHRwLmdldCgnL2FwaTIvZ2V0X2dlbycpLnN1Y2Nlc3MoZnVuY3Rpb24gKGdlbykge1xuICAgICAgICAgICAgY29uc29sZS5sb2coJ2dlbycsIGdlbylcbiAgICAgICAgICAgIGlmIChnZW8uY291bnRyeSlcbiAgICAgICAgICAgICAgc2NvcGUuZGF0YXNvdXJjZS5pbmNfY291bnRyeSA9IGdlby5jb3VudHJ5XG4gICAgICAgICAgICBzY29wZS5kYXRhc291cmNlLm5hdHVyZSA9ICdDJ1xuICAgICAgICAgIH0pXG5cbiAgICAgICAgc2NvcGUubmF0dXJlcyA9IFtcbiAgICAgICAgICB7bmFtZTogJ0NvbXBhbnknLCBjb2RlOiAnQyd9LFxuICAgICAgICAgIHtuYW1lOiAnSW5kaXZpZHVhbCcsIGNvZGU6ICdJJ31cbiAgICAgICAgXVxuICAgICAgICBzY29wZS5jb3VudHJpZXMgPSBjb3VudHJpZXNcbiAgICAgIH1cbiAgICB9XG4gIH1dKVxuXG4gIC5kaXJlY3RpdmUoJ3Byb2ZpbGVBZGRyZXNzJywgWyckaHR0cCcsICckdGVtcGxhdGVDYWNoZScsICckY29tcGlsZScsIGZ1bmN0aW9uICgkaHR0cCwgJHRlbXBsYXRlQ2FjaGUsICRjb21waWxlKSB7XG4gICAgY29uc29sZS5sb2coJ2luIHByb2ZpbGUzIGhlbGxvJylcbiAgICByZXR1cm4ge1xuICAgICAgcmVzdHJpY3Q6ICdFJyxcbiAgICAgIHNjb3BlOiB7XG4gICAgICAgIG5hbWU6ICdAJyxcbiAgICAgICAgZGF0YXNvdXJjZSA6ICc9JyxcbiAgICAgICAgY291bnRyeSA6ICc9J1xuICAgICAgfSxcbi8vICAgICAgdGVtcGxhdGVVcmw6ICcvcGFydGlhbHMvcHJvZmlsZUFkZHJlc3MuaHRtbCcsXG4vLyAgICAgIHRlbXBsYXRlVXJsOiAnL2FwaTIvc2VydmVyX3BhcnRpYWxzL2FkZHJlc3Mve3tjb2RlfX0nLFxuXG4gICAgICBsaW5rOiBmdW5jdGlvbiAoc2NvcGUsIGVsLCBhdHRyKSB7XG5cbiAgICAgICAgLypcbiAgICAgICAgc2NvcGUuc3RhdGVzMiA9IHtcbiAgICAgICAgICBcIlBILkFCXCI6IFwidGVzdFwiLFxuICAgICAgICAgIFwiUEguQ0JcIjogXCJjZWJ1XCJcbiAgICAgICAgfVxuICAgICAgICAqL1xuXG4gICAgICAgIHNjb3BlLnN0YXRlcyA9IGNvdW50cmllcy5nZXRfc3RhdGVzKHNjb3BlLmNvdW50cnkpXG5cbiAgICAgICAgY29uc29sZS5sb2coJ2F0dHIxJyxhdHRyKVxuICAgICAgICBjb25zb2xlLmxvZygnZHMnLCBzY29wZS5kYXRhc291cmNlKVxuICAgICAgICBjb25zb2xlLmxvZygnYycsIHNjb3BlLmNvdW50cnkpXG4gICAgICAgIGNvbnNvbGUubG9nKCdzY29wZScsIHNjb3BlKVxuXG5cbiAgICAgICAgdmFyIHVybCA9IFwiL2FwaTIvc2VydmVyX3BhcnRpYWxzL2FkZHJlc3MvXCIgKyBzY29wZS5jb3VudHJ5XG4gICAgICAgIGNvbnNvbGUubG9nKHVybClcbiAgICAgICAgbG9hZFRlbXBsYXRlKFwiL2FwaTIvc2VydmVyX3BhcnRpYWxzL2FkZHJlc3MvXCIgKyBzY29wZS5jb3VudHJ5KTtcbiAgICAgICAgY29uc29sZS5sb2coJ2NvdW50cnkgaW4gd2F0Y2gxJywgc2NvcGUuY291bnRyeSlcblxuICAgICAgICAvKlxuXG4gICAgICAgIHNjb3BlLiR3YXRjaChhdHRyLmNvdW50cnksIGZ1bmN0aW9uICh2YWx1ZSkge1xuICAgICAgICAgIGlmICh2YWx1ZSkge1xuICAgICAgICAgICAgdmFyIHVybCA9IFwiL2FwaTIvc2VydmVyX3BhcnRpYWxzL2FkZHJlc3MvXCIgKyB2YWx1ZVxuICAgICAgICAgICAgY29uc29sZS5sb2codXJsKVxuICAgICAgICAgICAgbG9hZFRlbXBsYXRlKFwiL2FwaTIvc2VydmVyX3BhcnRpYWxzL2FkZHJlc3MvXCIgKyB2YWx1ZSk7XG4gICAgICAgICAgICBjb25zb2xlLmxvZygnY291bnRyeSBpbiB3YXRjaCcsIHZhbHVlKVxuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICAgICovXG5cblxuICAgICAgICBmdW5jdGlvbiBsb2FkVGVtcGxhdGUodGVtcGxhdGUpIHtcbiAgICAgICAgICAkaHR0cC5nZXQodGVtcGxhdGUgLyosIHsgY2FjaGU6ICR0ZW1wbGF0ZUNhY2hlIH0qLylcbiAgICAgICAgICAgIC5zdWNjZXNzKGZ1bmN0aW9uKHRlbXBsYXRlQ29udGVudCkge1xuXG4vLyAgICAgICAgICAgICAgY29uc29sZS5sb2coJ2RkJywgdGVtcGxhdGVDb250ZW50KVxuICAgICAgICAgICAgICBjb25zb2xlLmxvZygnZWwnLGVsKVxuICAgICAgICAgICAgICBlbC5yZXBsYWNlV2l0aCgkY29tcGlsZSh0ZW1wbGF0ZUNvbnRlbnQpKHNjb3BlKSk7XG4gICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdsb2FkZWQnLCBzY29wZSlcbiAgICAgICAgICAgICAgc2NvcGUuJHBhcmVudC5yZWFkeTJzaG93ID0gdHJ1ZSAgLy8gZmxhZyB0byBpbmZvcm0gdGhlIG1haW4gZm9ybSB0byBzaG93LCBpdCdzIGEgaGFja1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cblxuICAgICAgICBjb25zb2xlLmxvZygnc2NvcGUnLHNjb3BlKVxuXG5cbi8vICAgICAgICBzY29wZS5jb2RlID0gJ0FTJ1xuICAgICAgICAvKlxuICAgICAgICBpZiAoIXNjb3BlLmRhdGFzb3VyY2UuY291bnRyeSlcbiAgICAgICAgICAkaHR0cC5nZXQoJy9hcGkyL2dldF9nZW8/dGVzdF9pcD0xNzQuNTQuMTY1LjIwNicpLnN1Y2Nlc3MoZnVuY3Rpb24gKGdlbykge1xuICAgICAgICAgICAgY29uc29sZS5sb2coJ2dlbycsIGdlbylcbiAgICAgICAgICAgIGlmIChnZW8uY291bnRyeSkge1xuICAgICAgICAgICAgICBzY29wZS5kYXRhc291cmNlLmNvdW50cnkgPSBnZW8uY291bnRyeVxuICAgICAgICAgICAgICBzY29wZS5kYXRhc291cmNlLnN0YXRlID0gZ2VvLnJlZ2lvblxuICAgICAgICAgICAgICBzY29wZS5kYXRhc291cmNlLmNpdHkgPSBnZW8uY2l0eVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH0pXG5cbiAgICAgICAgY29uc29sZS5sb2coJ2RhdGFzb3VyY2UyJywgc2NvcGUuZGF0YXNvdXJjZSlcbiAgICAgICAgKi9cbiAgICAgICAgc2NvcGUuY291bnRyaWVzID0gY291bnRyaWVzLmdldF9jb3VudHJpZXMoKVxuICAgICAgfVxuICAgIH1cbiAgfV0pXG5cblxuXG4iLCJcInVzZSBzdHJpY3RcIjtcbi8qIGdsb2JhbCBhbmd1bGFyLCBqUXVlcnkgKi9cbi8qIERpcmVjdGl2ZXMgKi9cblxudmFyIHV0aWxpdGllcyA9IHJlcXVpcmUoJy4uL21vZHVsZXMvdXRpbGl0aWVzLmpzJylcblxuXG5mdW5jdGlvbiBhcHBlbmRfb25lX3dvcmRfY291bnRyeV9uYW1lKGxzdCkge1xuICAgIHZhciBybHN0ID0gW11cbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGxzdC5sZW5ndGg7IGkrKykge1xuICAgICAgICBsc3RbaV0uc2ltcGxlX25hbWUgPSBsc3RbaV0ubmFtZS5yZXBsYWNlKC8gL2csICdfJykudG9Mb3dlckNhc2UoKVxuICAgICAgICBybHN0LnB1c2gobHN0W2ldKVxuICAgIH1cbiAgICByZXR1cm4gcmxzdFxufVxuXG5cbmFuZ3VsYXIubW9kdWxlKCd0a3MuZGlyZWN0aXZlcycsIFtdKS5cbiAgICBkaXJlY3RpdmUoJ2FwcFZlcnNpb24nLCBbJ3ZlcnNpb24nLFxuICAgICAgICBmdW5jdGlvbiAodmVyc2lvbikge1xuICAgICAgICAgICAgcmV0dXJuIGZ1bmN0aW9uIChzY29wZSwgZWxtLCBhdHRycykge1xuICAgICAgICAgICAgICAgIGVsbS50ZXh0KHZlcnNpb24pO1xuICAgICAgICAgICAgfTtcbiAgICAgICAgfVxuICAgIF0pLmRpcmVjdGl2ZSgnaGVsbG8yJywgZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgcmVzdHJpY3Q6ICdFJyxcbiAgICAgICAgICAgIHNjb3BlOiB7XG4gICAgICAgICAgICAgICAgbmFtZTogJ0AnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgdGVtcGxhdGU6ICc8c3Bhbj5IZWxsbyB7e25hbWV9fTwvc3Bhbj4nXG4gICAgICAgIH1cbiAgICB9KS5kaXJlY3RpdmUoJ3RyYWRlbWFya1JlcXVlc3QnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICByZXN0cmljdDogJ0UnLFxuICAgICAgICAgICAgc2NvcGU6IHtcbiAgICAgICAgICAgICAgICBuYW1lOiAnQCcsXG4gICAgICAgICAgICAgICAgdG1yZWc6ICc9JyxcbiAgICAgICAgICAgICAgICBwcm9maWxlOiAnPSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB0ZW1wbGF0ZVVybDogJy9wYXJ0aWFscy90bXJlcXVlc3QuaHRtbCcsXG4gICAgICAgICAgICAvKlxuICAgICAgICAgICAgIGNvbnRyb2xsZXI6IGZ1bmN0aW9uICgkc2NvcGUpIHtcbiAgICAgICAgICAgICBjb25zb2xlLmxvZygnY3RybCcpXG4gICAgICAgICAgICAgY29uc29sZS5sb2coJHNjb3BlKVxuICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdjdHJsMicpXG4gICAgICAgICAgICAgfSovXG4gICAgICAgICAgICBsaW5rOiBmdW5jdGlvbiAoJHNjb3BlLCBlbGVtZW50KSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ2FiYycpXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coJHNjb3BlLnByb2ZpbGVfbmFtZSlcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygkc2NvcGUpXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ2FiY3gnKVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLypcbiAgICAgICAgICAgICBsaW5rOiBmdW5jdGlvbiAoc2NvcGUsIGVsZW1lbnQpIHtcbiAgICAgICAgICAgICBjb25zb2xlLmxvZyhzY29wZS50bXJlZylcbiAgICAgICAgICAgICBzY29wZS5uYW1lMiA9ICdKZWZmJztcblxuICAgICAgICAgICAgIHZhciB0ZW1wXG4gICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPHNjb3BlLnRtcmVnLmNsYXNzZXMubGVuZ3RoOyBpICsrKSB7XG4gICAgICAgICAgICAgdmFyIGMgPSBzY29wZS50bXJlZy5jbGFzc2VzWzBdXG4gICAgICAgICAgICAgY29uc29sZS5sb2coYylcbiAgICAgICAgICAgICBpZiAoaSA9PT0gMClcbiAgICAgICAgICAgICB0ZW1wID0gYy5uYW1lICsgJyAoJyArIGMuZGV0YWlscyArICcpJ1xuICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgIHRlbXAgPSB0ZW1wICsgJywgJyArIGMubmFtZSArICcgKCcgKyBjLmRldGFpbHMgKyAnKSdcbiAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgfVxuICAgICAgICAgICAgIHNjb3BlLmNsYXNzZXMgPSB0ZW1wXG4gICAgICAgICAgICAgfSovXG5cbiAgICAgICAgfVxuICAgIH0pLmRpcmVjdGl2ZSgndG1QaG90bycsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIHJlc3RyaWN0OiAnRScsXG4gICAgICAgICAgICBzY29wZToge1xuICAgICAgICAgICAgICAgIHBzcmM6ICc9JyxcbiAgICAgICAgICAgICAgICB0bXR5cGU6ICc9J1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHRlbXBsYXRlOiAnPGltZyBuZy1zaG93PVwic2hvdyh0bXR5cGUpXCIgbmctc3JjPVwie3twc3JjfX1cIiB3aWR0aD1cIjEwMHB4O1wiIC8+JyxcbiAgICAgICAgICAgIGxpbms6IGZ1bmN0aW9uIChzY29wZSwgZWwsIGF0dHIpIHtcbiAgICAgICAgICAgICAgICBzY29wZS5zaG93ID0gZnVuY3Rpb24gKHR5cCkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gKFsnbCcsICd3bCcsICd0bWwnXS5pbmRleE9mKHR5cCkgIT09IC0xKVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH0pLmRpcmVjdGl2ZSgnY29kZTJjb3VudHJ5JywgZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgcmVzdHJpY3Q6ICdFJyxcbiAgICAgICAgICAgIHNjb3BlOiB7XG4gICAgICAgICAgICAgICAgY29kZTogJz0nXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgLy8gICAgICB0ZW1wbGF0ZTogJzxpbWcgc3JjPVwie3tjb2RlMnNyYyhjb2RlKX19XCIgLz4nLFxuICAgICAgICAgICAgdGVtcGxhdGU6ICc8aW1nIG5nLXNyYz1cInt7Y29kZTJzcmMoY29kZSl9fVwiIC8+JyxcbiAgICAgICAgICAgIGxpbms6IGZ1bmN0aW9uIChzY29wZSwgZWwsIGF0dHIpIHtcbiAgICAgICAgICAgICAgICBzY29wZS5jb2RlMnNyYyA9IGZ1bmN0aW9uIChjb2RlKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGNvZGUpXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAnaW1hZ2VzL2ZsYWdzLycgKyBjb2RlLnRvTG93ZXJDYXNlKCkgKyAnLnBuZydcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9KS5kaXJlY3RpdmUoJ2luZm9Qb3BvdmVyJywgZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgcmVzdHJpY3Q6ICdFJyxcbiAgICAgICAgICAgIHNjb3BlOiB7XG4gICAgICAgICAgICAgICAgaW5mbzogJ0BpbmZvJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHRlbXBsYXRlOiAnIDxpbWcgbmctc3JjPVwiaW1hZ2VzL2luZm8uZ2lmXCIgc3R5bGU9XCJwYWRkaW5nLWxlZnQ6IDNweDtcIiBwb3BvdmVyPVwie3tpbmZvfX1cIiBwb3BvdmVyLXRyaWdnZXI9XCJtb3VzZWVudGVyXCIgcG9wb3Zlci1wbGFjZW1lbnQ9XCJsZWZ0XCIgIC8+J1xuICAgICAgICAgICAgLypcbiAgICAgICAgICAgICBsaW5rOiBmdW5jdGlvbihzY29wZSwgZWwsIGF0dHIpIHtcbiAgICAgICAgICAgICBjb25zb2xlLmxvZyhzY29wZSlcbiAgICAgICAgICAgICB9Ki9cbiAgICAgICAgfVxuICAgIH0pLmRpcmVjdGl2ZSgnb3JkZXIxMjMnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICByZXN0cmljdDogJ0UnLFxuICAgICAgICAgICAgc2NvcGU6IHtcbiAgICAgICAgICAgICAgICBkZXRhaWxzOiAnPSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB0ZW1wbGF0ZTogJzxwPjxoMz5kZXRhaWxzMTIzPC9oMz48L3A+JyxcbiAgICAgICAgICAgIGxpbms6IGZ1bmN0aW9uIChzY29wZSwgZWwsIGF0dHIpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnYXNkZmFzZGYnKVxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHNjb3BlKVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfSkuZGlyZWN0aXZlKCdvcmRlckl0ZW1EZXRhaWxzJywgZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgcmVzdHJpY3Q6ICdFJyxcbiAgICAgICAgICAgIHNjb3BlOiB7XG4gICAgICAgICAgICAgICAgZGV0YWlsczogJz0nXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgdGVtcGxhdGVVcmw6ICcvcGFydGlhbHMvb3JkZXJfaXRlbV9kZXRhaWxzLmh0bWwnXG4gICAgICAgIH07XG4gICAgfSlcblxuICAgIC5kaXJlY3RpdmUoJ29yZGVySXRlbURldGFpbHNXaXRoTm90ZXMnLCBmdW5jdGlvbiAoKSB7ICAvLyBpZiBub3RlcyBpcyBwcmVzZW50LCBpdCBpcyBhZGRlZCBmaXJzdCBieSBhZG1pblxuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgcmVzdHJpY3Q6ICdFJyxcbiAgICAgICAgICAgIHNjb3BlOiB7XG4gICAgICAgICAgICAgICAgZGV0YWlsczogJz0nXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgdGVtcGxhdGVVcmw6ICcvcGFydGlhbHMvb3JkZXJfaXRlbV9kZXRhaWxzMi5odG1sJ1xuICAgICAgICB9O1xuICAgIH0pXG5cbiAgICAuZGlyZWN0aXZlKCdsb2FkaW5nJywgZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgcmVzdHJpY3Q6ICdFJyxcbiAgICAgICAgICAgIHNjb3BlOiB7XG4gICAgICAgICAgICAgICAgd2hlbjogJz0nXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgdGVtcGxhdGU6ICc8c3BhbiBuZy1zaG93PVwid2hlblwiPjxpbWcgbmctc3JjPVwiL2ltYWdlcy9sb2FkaW5nLmdpZlwiIC8+PC9zcGFuPidcbiAgICAgICAgfVxuICAgIH0pXG5cbiAgICAuZGlyZWN0aXZlKCdnQ2hhcnQnLCBbJyRsb2NhdGlvbicsXG4gICAgICAgIGZ1bmN0aW9uICgkbG9jYXRpb24pIHtcbiAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgcmVzdHJpY3Q6ICdBJyxcbiAgICAgICAgICAgICAgICBzY29wZToge1xuICAgICAgICAgICAgICAgICAgICBnaWQ6ICc9JyxcbiAgICAgICAgICAgICAgICAgICAgZ2RhdGE6ICc9JyxcbiAgICAgICAgICAgICAgICAgICAgZ3JlYWR5OiAnPScsXG4gICAgICAgICAgICAgICAgICAgIGR0eXBlOiAnPScgLy8gJ1YnIHZpZGVvcywgJ1MnIHN1Ym1pc3Npb25zLCAnQScgYXNzaWdubWVudHNcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGxpbms6IGZ1bmN0aW9uIChzY29wZSwgZWxtLCBhdHRycykge1xuXG4gICAgICAgICAgICAgICAgICAgIGZ1bmN0aW9uIGdldF9kYXRhKCkge1xuXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhzY29wZS5nZGF0YSlcbiAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLmdkYXRhLmZuLnRoZW4oZnVuY3Rpb24gKGQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnZGQxJylcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhkKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdkZDInKVxuICAgICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgIGZ1bmN0aW9uIGRyYXdSZWdpb25zTWFwKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGRhdGEgPSBuZXcgZ29vZ2xlLnZpc3VhbGl6YXRpb24uRGF0YVRhYmxlKCk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGEuYWRkQ29sdW1uKCdzdHJpbmcnLCAnQ291bnRyeScpO1xuICAgICAgICAgICAgICAgICAgICAgICAgZGF0YS5hZGRDb2x1bW4oJ251bWJlcicsICdSZWZlcmVuY2UnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdiYmInLCBzY29wZS5nZGF0YSlcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vICAgIFRNQ291bnRyaWVzLmdldCgpLnRoZW4oZnVuY3Rpb24oY250cykge1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gICAgICAgICAgICBjb25zb2xlLmxvZygnY2NjJywgZGF0YSlcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vICAgICAgICAgIGRhdGEuYWRkUm93cyhPYmplY3Qua2V5cyhjbnRzKS5sZW5ndGgpXG4gICAgICAgICAgICAgICAgICAgICAgICBkYXRhLmFkZFJvd3MoT2JqZWN0LmtleXMoc2NvcGUuZ2RhdGEpLmxlbmd0aClcblxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gICAgICAgICAgICBjb25zb2xlLmxvZygnJyxPYmplY3Qua2V5cyhjbnRzKS5sZW5ndGgpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgayA9IDBcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvciAodmFyIGtleSBpbiBzY29wZS5nZGF0YSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzY29wZS5nZGF0YS5oYXNPd25Qcm9wZXJ0eShrZXkpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vICAgICAgICAgICAgICAgY29uc29sZS5sb2coY250c1trZXldKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYXRhLnNldENlbGwoaywgMCwgc2NvcGUuZ2RhdGFba2V5XS5uYW1lKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YS5zZXRDZWxsKGssIDEsIHNjb3BlLmdkYXRhW2tleV0ucHJpY2UpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBrID0gayArIDFcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBvcHRpb25zID0ge307XG4gICAgICAgICAgICAgICAgICAgICAgICAvL29wdGlvbnNbJ2NvbG9ycyddID0gWycjRkFEQzQxJywgJyNjNDQ5NDknLCAnI2Q3NGExMicsICcjMGU5YTBlJywgJ3JlZCcsICcjN2M0YjkxJywgJyNmYWRjNDEnLCAnIzBkNmNjYScsICcjN2M0ODk3J107XG4gICAgICAgICAgICAgICAgICAgICAgICAvL29wdGlvbnNbJ2NvbG9ycyddID0gWyAnIzBlOWEwZScsICdyZWQnLCAnIzdjNGI5MScsICcjZmFkYzQxJywgJyMwZDZjY2EnLCAnIzdjNDg5NyddO1xuICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9ucy5jb2xvcnMgPSBbJyMwMDhiZDQnLCAnIzAwOGJkNCddO1xuICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9ucy5sZWdlbmQgPSAnbm9uZSc7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyAgICAgICAgICBvcHRpb25zLnJlZ2lvbiA9ICdFUydcblxuXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgY2hhcnQgPSBuZXcgZ29vZ2xlLnZpc3VhbGl6YXRpb24uR2VvQ2hhcnQoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoc2NvcGUuZ2lkKSk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIGdvb2dsZS52aXN1YWxpemF0aW9uLmV2ZW50cy5hZGRMaXN0ZW5lcihjaGFydCwgJ3JlZ2lvbkNsaWNrJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmdW5jdGlvbiAocmVnaW9uKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLiRhcHBseShmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkbG9jYXRpb24ucGF0aCgnL3RyYWRlbWFyay8nICsgcmVnaW9uLnJlZ2lvbik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvKlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBjbnRyeSA9IHNjb3BlLmdkYXRhW3JlZ2lvbi5yZWdpb25dXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGNudHJ5ICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoY250cnkubGluaylcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkbG9jYXRpb24ucGF0aCgnL3RyYWRlbWFyay8nICsgY250cnkubGluayk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRsb2NhdGlvbi5wYXRoKCcvdHJhZGVtYXJrLycgKyByZWdpb24ucmVnaW9uKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9ICovXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICBzY29wZS5ncmVhZHkgPSB0cnVlXG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8qXG4gICAgICAgICAgICAgICAgICAgICAgICAgZ29vZ2xlLnZpc3VhbGl6YXRpb24uZXZlbnRzLmFkZExpc3RlbmVyKGNoYXJ0LCAncmVhZHknLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUuJGFwcGx5KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnbWFwIGlzIHJlYWR5JylcbiAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZS5ncmVhZHkgPSB0cnVlXG4gICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICovXG5cbiAgICAgICAgICAgICAgICAgICAgICAgIGNoYXJ0LmRyYXcoZGF0YSwgb3B0aW9ucyk7XG5cblxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICAvKlxuICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGEuYWRkUm93cyhzY29wZS5nZGF0YS5sZW5ndGgpXG5cblxuICAgICAgICAgICAgICAgICAgICAgICAgIGZvciAodmFyIGsgPSAwOyBrIDwgc2NvcGUuZ2RhdGEubGVuZ3RoOyBrICsrKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGMgPSBzY29wZS5nZGF0YVtrXVxuICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGssYywgYy5wcmljZSwgYy5jb2RlIClcbiAgICAgICAgICAgICAgICAgICAgICAgICBkYXRhLnNldENlbGwoaywgMCwgc2NvcGUuZ2RhdGFba10ubmFtZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgZGF0YS5zZXRDZWxsKGssIDEsIHNjb3BlLmdkYXRhW2tdLnByaWNlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgb3B0aW9ucyA9IHsgfTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAvLyAgICAgICAgICBvcHRpb25zWydjb2xvcnMnXSA9IFsnI0ZBREM0MScsICcjYzQ0OTQ5JywgJyNkNzRhMTInLCAnIzBlOWEwZScsICdyZWQnLCAnIzdjNGI5MScsICcjZmFkYzQxJywgJyMwZDZjY2EnLCAnIzdjNDg5NyddO1xuICAgICAgICAgICAgICAgICAgICAgICAgIC8vICAgICAgICAgIG9wdGlvbnNbJ2NvbG9ycyddID0gWyAnIzBlOWEwZScsICdyZWQnLCAnIzdjNGI5MScsICcjZmFkYzQxJywgJyMwZDZjY2EnLCAnIzdjNDg5NyddO1xuICAgICAgICAgICAgICAgICAgICAgICAgIG9wdGlvbnNbJ2xlZ2VuZCddID0gJ25vbmUnO1xuICAgICAgICAgICAgICAgICAgICAgICAgIC8vICAgICAgICAgIG9wdGlvbnMucmVnaW9uID0gJ0VTJ1xuXG5cbiAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgY2hhcnQgPSBuZXcgZ29vZ2xlLnZpc3VhbGl6YXRpb24uR2VvQ2hhcnQoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoc2NvcGUuZ2lkKSk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICBnb29nbGUudmlzdWFsaXphdGlvbi5ldmVudHMuYWRkTGlzdGVuZXIoY2hhcnQsICdyZWdpb25DbGljaycsXG4gICAgICAgICAgICAgICAgICAgICAgICAgZnVuY3Rpb24ocmVnaW9uKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUuJGFwcGx5KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICRsb2NhdGlvbi5wYXRoKCcvdHJhZGVtYXJrLycgKyByZWdpb24ucmVnaW9uKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgIGdvb2dsZS52aXN1YWxpemF0aW9uLmV2ZW50cy5hZGRMaXN0ZW5lcihjaGFydCwgJ3JlYWR5JywgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUuJGFwcGx5KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdtYXAgaXMgcmVhZHknKVxuICAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLmdyZWFkeSA9IHRydWVcbiAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgY2hhcnQuZHJhdyhkYXRhLCBvcHRpb25zKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgZHJhd1JlZ2lvbnNNYXAoKVxuXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfTtcbiAgICAgICAgfVxuICAgIF0pXG5cbiAgICAvLyAgc2hvdyBhIGxpc3Qgb2YgY291bnRyaWVzIGluIHRoZSByZWdpb25cblxuICAgIC5kaXJlY3RpdmUoJ3JlZ2lvbkxpc3QnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICByZXN0cmljdDogJ0UnLFxuICAgICAgICAgICAgc2NvcGU6IHtcbiAgICAgICAgICAgICAgICBjb2RlOiAnQCcsXG4gICAgICAgICAgICAgICAgY29sczogJz0nXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgdGVtcGxhdGVVcmw6ICcvcGFydGlhbHMvcmVnaW9ubGlzdC5odG1sJyxcbiAgICAgICAgICAgIGxpbms6IGZ1bmN0aW9uIChzY29wZSwgZWwsIGF0dHIpIHtcblxuICAgICAgICAgICAgICAgIHZhciBsc3QgPSBhcHBlbmRfb25lX3dvcmRfY291bnRyeV9uYW1lKHV0aWxpdGllcy5nZXRfY291bnRyaWVzX2J5X2NvbnRuZW50KHNjb3BlLmNvZGUpKVxuICAgICAgICAgICAgICAgIHZhciBudW1zID0gTWF0aC5jZWlsKGxzdC5sZW5ndGggLyAzKVxuICAgICAgICAgICAgICAgIHNjb3BlLmNvdW50cmllczEgPSBsc3Quc2xpY2UoMCwgbnVtcykgLy8gMCAxM1xuICAgICAgICAgICAgICAgIHNjb3BlLmNvdW50cmllczIgPSBsc3Quc2xpY2UobnVtcywgMiAqIG51bXMpIC8vIDE0XG4gICAgICAgICAgICAgICAgc2NvcGUuY291bnRyaWVzMyA9IGxzdC5zbGljZSgyICogbnVtcywgMyAqIG51bXMpXG5cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH0pXG5cbiAgICAuZGlyZWN0aXZlKCdzdHJpcGVGb3JtJywgWyckd2luZG93JyxcbiAgICAgICAgZnVuY3Rpb24gKCR3aW5kb3cpIHtcblxuICAgICAgICAgICAgdmFyIGRpcmVjdGl2ZSA9IHtcbiAgICAgICAgICAgICAgICByZXN0cmljdDogJ0EnXG4gICAgICAgICAgICB9O1xuICAgICAgICAgICAgZGlyZWN0aXZlLmxpbmsgPSBmdW5jdGlvbiAoc2NvcGUsIGVsZW1lbnQsIGF0dHJpYnV0ZXMpIHtcbiAgICAgICAgICAgICAgICB2YXIgZm9ybSA9IGFuZ3VsYXIuZWxlbWVudChlbGVtZW50KTtcbiAgICAgICAgICAgICAgICBmb3JtLmJpbmQoJ3N1Ym1pdCcsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGJ1dHRvbiA9IGZvcm0uZmluZCgnYnV0dG9uJyk7XG4gICAgICAgICAgICAgICAgICAgIGJ1dHRvbi5wcm9wKCdkaXNhYmxlZCcsIHRydWUpO1xuICAgICAgICAgICAgICAgICAgICAkd2luZG93LlN0cmlwZS5jcmVhdGVUb2tlbihmb3JtWzBdLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgYXJncyA9IGFyZ3VtZW50cztcbiAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLiRhcHBseShmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGVbYXR0cmlidXRlcy5zdHJpcGVGb3JtXS5hcHBseShzY29wZSwgYXJncyk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJ1dHRvbi5wcm9wKCdkaXNhYmxlZCcsIGZhbHNlKTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9O1xuICAgICAgICAgICAgcmV0dXJuIGRpcmVjdGl2ZTtcblxuICAgICAgICB9XG4gICAgXSlcblxuICAgIC5kaXJlY3RpdmUoJ3Bhc3N3b3JkUmVzZXQnLCBbJ2F1dGhTZXJ2aWNlJyxcbiAgICAgICAgZnVuY3Rpb24gKGF1dGhTZXJ2aWNlKSB7XG4gICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAgIHJlc3RyaWN0OiAnQScsXG4gICAgICAgICAgICAgICAgc2NvcGU6IHtcbiAgICAgICAgICAgICAgICAgICAgZmxhZzogJ0AnXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB0ZW1wbGF0ZVVybDogJy9wYXJ0aWFscy9mb3Jnb3RfcGFzc3dvcmQuaHRtbCcsXG4gICAgICAgICAgICAgICAgLy90ZW1wbGF0ZVVybDogJy9wYXJ0aWFscy90ZXN0MTIzLmh0bWwnLFxuICAgICAgICAgICAgICAgIC8vdGVtcGxhdGU6IFwiPGgxPmhlbGxvPC9oMT5cIixcbiAgICAgICAgICAgICAgICBsaW5rOiBmdW5jdGlvbiAoc2NvcGUsIGVsLCBhdHRyKSB7XG5cblxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnaW4gcmVzZXQgcGFzc3dvcmQnKVxuXG4gICAgICAgICAgICAgICAgICAgIHNjb3BlLmFsZXJ0cyA9IFtdO1xuXG4gICAgICAgICAgICAgICAgICAgIHNjb3BlLmNsb3NlQWxlcnQgPSBmdW5jdGlvbiAoaW5kZXgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLmFsZXJ0cy5zcGxpY2UoaW5kZXgsIDEpO1xuICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUuJHBhcmVudC5zaG93X3Jlc2V0X2Zvcm0gPSBmYWxzZSAvLyB0aGUgcGFyZW50IHNob3VsZCB1c2UgdGhpcyBmbGFnIHRvIHNob3cvaGlkZSByZWxhdGVkIGVsZW1lbnRzXG4gICAgICAgICAgICAgICAgICAgICAgICBzY29wZS5hbGVydHMgPSBbXTtcblxuICAgICAgICAgICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICAgICAgICAgIHNjb3BlLmluZm8gPSB7fVxuICAgICAgICAgICAgICAgICAgICBzY29wZS5yZXNldF9wYXNzd29yZCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdyZXNldHRpbmcnKVxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coc2NvcGUuaW5mbylcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHNjb3BlKVxuICAgICAgICAgICAgICAgICAgICAgICAgYXV0aFNlcnZpY2UucmVxdWVzdF9wYXNzd29yZF9yZXNldChzY29wZS5pbmZvLmVtYWlsKS50aGVuKGZ1bmN0aW9uIChyZXN1bHQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnYmFjayByZXNldCAgcmVzdWx0ICcsIHJlc3VsdClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZS5hbGVydHMgPSBbXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXN1bHQuc3RhdHVzKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZS5hbGVydHMucHVzaCh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAnc3VjY2VzcycsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtc2c6ICdXZSBoYXZlIHNlbnQgYW4gZW1haWwgd2l0aCBwYXNzd29yZCByZXNldCBpbnN0cnVjdGlvbiwgcGxlYXNlIGNoZWNrIHlvdXIgaW5ib3gnXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZS5hbGVydHMucHVzaCh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAnd2FybmluZycsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtc2c6IHJlc3VsdC5tZXNzYWdlXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHNjb3BlKVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIF0pXG5cbiAgICAvKlxuICAgICAvLyB0aGlzIGFzc3VtZXMgdGhlIHBhcmVudCBoYXMgYWxlcnRzW10gZGVmaW5lZFxuICAgICAvLyAkc2NvcGUuYWxlcnRzLnB1c2goe1xuICAgICB0eXBlOiAnd2FybmluZycsXG4gICAgIG1zZzogXCJ0aGlzIGlzIGEgbWVzc2FnZTEyM1wiXG4gICAgIH0pXG4gICAgICovXG5cbiAgICAuZGlyZWN0aXZlKCdhbGVydExpbmUnLCBbJyR0aW1lb3V0JywgZnVuY3Rpb24gKCR0aW1lb3V0KSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICByZXN0cmljdDogJ0UnLFxuICAgICAgICAgICAgdGVtcGxhdGU6ICc8YWxlcnQgbmctcmVwZWF0PVwiYWxlcnQgaW4gYWxlcnRzXCIgdHlwZT1cInt7YWxlcnQudHlwZX19XCIgY2xvc2U9XCJjbG9zZUFsZXJ0KCRpbmRleClcIj57e2FsZXJ0Lm1zZ319PC9hbGVydD4nLFxuICAgICAgICAgICAgbGluazogZnVuY3Rpb24gKHNjb3BlLCBlbCwgYXR0cikge1xuICAgICAgICAgICAgICAgIC8vIHNjb3BlLmFsZXJ0cyA9IFtdO1xuICAgICAgICAgICAgICAgIHNjb3BlLmNsb3NlQWxlcnQgPSBmdW5jdGlvbiAoaW5kZXgpIHtcbiAgICAgICAgICAgICAgICAgICAgc2NvcGUuYWxlcnRzLnNwbGljZShpbmRleCwgMSk7ICAgIC8vIGFsZXJ0cyBpcyBkZWZpbmVkIGluIHRoZSBwYXJlbnQgY29udHJvbGxlclxuICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgLypcbiAgICAgICAgICAgICAgICAgJHRpbWVvdXQoZnVuY3Rpb24oKXtcbiAgICAgICAgICAgICAgICAgLy8gICAgICAgIHNjb3BlLmFsZXJ0cy5zcGxpY2UoMCwgMSlcbiAgICAgICAgICAgICAgICAgc2NvcGUuYWxlcnRzID0gW11cbiAgICAgICAgICAgICAgICAgfSwgMzAwMCk7IC8vIG1heWJlICd9LCAzMDAwLCBmYWxzZSk7JyB0byBhdm9pZCBjYWxsaW5nIGFwcGx5Ki9cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1dKVxuXG4gICAgLmRpcmVjdGl2ZSgndGlja2VyJywgWyckdGltZW91dCcsIGZ1bmN0aW9uICgkdGltZW91dCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgLy8gUmVzdHJpY3QgaXQgdG8gYmUgYW4gYXR0cmlidXRlIGluIHRoaXMgY2FzZVxuICAgICAgICAgICAgcmVzdHJpY3Q6ICdBJyxcbiAgICAgICAgICAgIC8vIHJlc3BvbnNpYmxlIGZvciByZWdpc3RlcmluZyBET00gbGlzdGVuZXJzIGFzIHdlbGwgYXMgdXBkYXRpbmcgdGhlIERPTVxuICAgICAgICAgICAgbGluazogZnVuY3Rpb24gKHNjb3BlLCBlbGVtZW50LCBhdHRycykge1xuICAgICAgICAgICAgICAgIC8vJChlbGVtZW50KS50b29sYmFyKHNjb3BlLiRldmFsKGF0dHJzLnRvb2xiYXJUaXApKTtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygndGlja2VyMicpXG4gICAgICAgICAgICAgICAgLy9qUXVlcnkoJyN3ZWJ0aWNrZXInKS53ZWJUaWNrZXIoKVxuICAgICAgICAgICAgICAgIGpRdWVyeSgnI25ld3MnKS5lYXN5VGlja2VyKHtcbiAgICAgICAgICAgICAgICAgICAgZGlyZWN0aW9uOiAndXAnLFxuICAgICAgICAgICAgICAgICAgICBlYXNpbmc6ICdzd2luZycsXG4gICAgICAgICAgICAgICAgICAgIHNwZWVkOiAnc2xvdycsXG4gICAgICAgICAgICAgICAgICAgIGludGVydmFsOiAzMDAwLFxuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6ICdhdXRvJyxcbiAgICAgICAgICAgICAgICAgICAgdmlzaWJsZTogMixcbiAgICAgICAgICAgICAgICAgICAgbW91c2VQYXVzZTogMSxcbiAgICAgICAgICAgICAgICAgICAgY29udHJvbHM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHVwOiAnJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIGRvd246ICcnLFxuICAgICAgICAgICAgICAgICAgICAgICAgdG9nZ2xlOiAnJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIHBsYXlUZXh0OiAnUGxheScsXG4gICAgICAgICAgICAgICAgICAgICAgICBzdG9wVGV4dDogJ1N0b3AnXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcbiAgICB9XSlcblxuICAgIC5kaXJlY3RpdmUoJ2NvdmVyZmxvdycsIFsnJGxvY2F0aW9uJywgZnVuY3Rpb24gKCRsb2NhdGlvbikge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgLy8gUmVzdHJpY3QgaXQgdG8gYmUgYW4gYXR0cmlidXRlIGluIHRoaXMgY2FzZVxuICAgICAgICAgICAgcmVzdHJpY3Q6ICdBJyxcbiAgICAgICAgICAgIC8vIHJlc3BvbnNpYmxlIGZvciByZWdpc3RlcmluZyBET00gbGlzdGVuZXJzIGFzIHdlbGwgYXMgdXBkYXRpbmcgdGhlIERPTVxuICAgICAgICAgICAgbGluazogZnVuY3Rpb24gKHNjb3BlLCBlbGVtZW50LCBhdHRycykge1xuXG4gICAgICAgICAgICAgICAgc2NvcGUuZ290b190cmFkZW1hcmsgPSBmdW5jdGlvbiAocGF0aCkge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnYXNkZmFzZGYnLCBwYXRoKVxuICAgICAgICAgICAgICAgICAgICAkbG9jYXRpb24ucGF0aChwYXRoKVxuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIHNjb3BlLm15cGF0aCA9ICdiYmJiYidcblxuICAgICAgICAgICAgICAgIGpRdWVyeSgnI3ByZXZpZXctY292ZXJmbG93JykuY292ZXJmbG93KFxuICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpbmRleDogMyxcbiAgICAgICAgICAgICAgICAgICAgICAgIGlubmVyQW5nbGU6IC01NSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGlubmVyT2Zmc2V0OiA5LFxuICAgICAgICAgICAgICAgICAgICAgICAgY29uZmlybTogZnVuY3Rpb24gKGV2ZW50LCBjb3ZlciwgaW5kZXgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnY29uZmlybWVkJywgY292ZXIpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUuJGFwcGx5KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8kbG9jYXRpb24ucGF0aCgnL3RyYWRlbWFyay9VUycpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgaW1nID0galF1ZXJ5KGNvdmVyKS5jaGlsZHJlbigpLmFuZFNlbGYoKS5maWx0ZXIoJ2ltZycpLmxhc3QoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ2ltZycsIGltZy5kYXRhKCdyZWZzcmMnKSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJGxvY2F0aW9uLnBhdGgoaW1nLmRhdGEoJ3JlZnNyYycpKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8qXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBpbWcgPSBqUXVlcnkoY292ZXIpLmNoaWxkcmVuKCkuYW5kU2VsZigpLmZpbHRlcignaW1nJykubGFzdCgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnaW1nJywgaW1nLmRhdGEoJ3JlZnNyYycpKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkbG9jYXRpb24ucGF0aCgnL3RyYWRlbWFyay8nKTsqL1xuICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgIHNlbGVjdDogZnVuY3Rpb24gKGV2ZW50LCBjb3Zlcikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBpbWcgPSBqUXVlcnkoY292ZXIpLmNoaWxkcmVuKCkuYW5kU2VsZigpLmZpbHRlcignaW1nJykubGFzdCgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGpRdWVyeSgnI3Bob3Rvcy1uYW1lJykudGV4dCgnUmVnaXN0ZXIgJyArIGltZy5kYXRhKCduYW1lJykgfHwgJ3Vua25vd24nKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZS5teXBhdGggPSBpbWcuZGF0YSgncmVmc3JjJylcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgfV0pXG5cbiAgICAuZGlyZWN0aXZlKCduZ0VudGVyJywgZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4gZnVuY3Rpb24gKHNjb3BlLCBlbGVtZW50LCBhdHRycykge1xuICAgICAgICAgICAgZWxlbWVudC5iaW5kKFwia2V5ZG93biBrZXlwcmVzc1wiLCBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgICAgICAgICBpZiAoZXZlbnQud2hpY2ggPT09IDEzKSB7XG4gICAgICAgICAgICAgICAgICAgIHNjb3BlLiRhcHBseShmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzY29wZS4kZXZhbChhdHRycy5uZ0VudGVyKTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfTtcbiAgICB9KVxuXG4gICAgLmRpcmVjdGl2ZSgndXNlckxpbmsnLCBbJyRodHRwJywgZnVuY3Rpb24gKCRodHRwKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICByZXN0cmljdDogJ0UnLFxuICAgICAgICAgICAgdGVtcGxhdGU6ICc8c3BhbiBjbGFzcz1cImdseXBoaWNvbiBnbHlwaGljb24tdXNlclwiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvc3Bhbj4mbmJzcDsmbmJzcDs8YSBuZy1ocmVmPVwiYWNjb3VudC97e2FjY291bnQuX2lkfX1cIj57e2FjY291bnQubmFtZX19PC9hPicsXG4gICAgICAgICAgICBsaW5rOiBmdW5jdGlvbiAoc2NvcGUsIGVsLCBhdHRycykge1xuICAgICAgICAgICAgICAgIGF0dHJzLiRvYnNlcnZlKCd1c3JpZCcsIGZ1bmN0aW9uICh2YWx1ZSkge1xuICAgICAgICAgICAgICAgICAgICBpZiAodmFsdWUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICRodHRwLmdldCgnL2FkbWluX2FwaS9hY2NvdW50LycgKyB2YWx1ZSkudGhlbihmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLmFjY291bnQgPSBkYXRhLmRhdGFcblxuICAgICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfV0pXG5cbiAgICAuZGlyZWN0aXZlKCdwcm9maWxlTGluaycsIFsnJGh0dHAnLCBmdW5jdGlvbiAoJGh0dHApIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIHJlc3RyaWN0OiAnRScsXG4gICAgICAgICAgICBzY29wZToge1xuICAgICAgICAgICAgICAgIHByb2ZpbGVfaWQ6ICc9J1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHRlbXBsYXRlOiAnPHNwYW4gY2xhc3M9XCJnbHlwaGljb24gZ2x5cGhpY29uLWZpbGVcIiBhcmlhLWhpZGRlbj1cInRydWVcIj48L3NwYW4+Jm5ic3A7PGEgbmctaHJlZj1cInByb2ZpbGUve3thY2NvdW50Mi5faWR9fVwiPnt7YWNjb3VudDIucHJvZmlsZV9uYW1lfX08L2E+JyxcbiAgICAgICAgICAgIGxpbms6IGZ1bmN0aW9uIChzY29wZSwgZWwsIGF0dHJzKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ3NjcDInLCBzY29wZSlcbiAgICAgICAgICAgICAgICBhdHRycy4kb2JzZXJ2ZSgncHJvZmlsZUlkJywgZnVuY3Rpb24gKHZhbHVlKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdwcm9maWxlIHZhbHVlJywgdmFsdWUpXG4gICAgICAgICAgICAgICAgICAgIGlmICh2YWx1ZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgJGh0dHAuZ2V0KCcvYWRtaW5fYXBpL3Byb2ZpbGUvJyArIHZhbHVlKS50aGVuKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUuYWNjb3VudDIgPSBkYXRhLmRhdGFcblxuICAgICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XSlcblxuICAgIC5kaXJlY3RpdmUoJ3Byb2ZpbGVOYW1lJywgWyckaHR0cCcsICdhdXRoU2VydmljZScsIGZ1bmN0aW9uICgkaHR0cCwgYXV0aFNlcnZpY2UpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIHJlc3RyaWN0OiAnRScsXG4gICAgICAgICAgICBzY29wZToge1xuICAgICAgICAgICAgICAgIHByb2ZpbGVfaWQ6ICc9J1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHRlbXBsYXRlOiAnPGE+e3thY2NvdW50Mi5wcm9maWxlX25hbWV9fTwvYT4nLFxuICAgICAgICAgICAgbGluazogZnVuY3Rpb24gKHNjb3BlLCBlbCwgYXR0cnMpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnc2NwMicsIHNjb3BlKVxuICAgICAgICAgICAgICAgIGF0dHJzLiRvYnNlcnZlKCdwcm9maWxlSWQnLCBmdW5jdGlvbiAodmFsdWUpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ3Byb2ZpbGUgdmFsdWUgaW4gcHJvZmlsZU5hbWUgJywgdmFsdWUpXG4gICAgICAgICAgICAgICAgICAgIGlmIChhdXRoU2VydmljZS5pc1NpZ25lZEluKCkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh2YWx1ZSkge1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJGh0dHAuZ2V0KCcvYXBpL3Byb2ZpbGUvJyArIHZhbHVlKS50aGVuKGZ1bmN0aW9uIHN1Y2Nlc3MoZGF0YSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZS5hY2NvdW50MiA9IGRhdGEuZGF0YVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIGZ1bmN0aW9uIGVycm9yKGVycm9yKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCd3ZSBnb3QgYW4gZXJyb3InKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlcnJvcilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KVxuXG4gICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgfVxuXG5cbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1dKS5kaXJlY3RpdmUoJ2pzb25sZCcsIFsnJGZpbHRlcicsICckc2NlJywgZnVuY3Rpb24gKCRmaWx0ZXIsICRzY2UpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIHJlc3RyaWN0OiAnRScsXG4gICAgICAgICAgICB0ZW1wbGF0ZTogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIHJldHVybiAnPHNjcmlwdCB0eXBlPVwiYXBwbGljYXRpb24vbGQranNvblwiIG5nLWJpbmQtaHRtbD1cIm9uR2V0SnNvbigpXCI+PC9zY3JpcHQ+JztcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBzY29wZToge1xuICAgICAgICAgICAgICAgIGpzb246ICc9anNvbidcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBsaW5rOiBmdW5jdGlvbiAoc2NvcGUsIGVsZW1lbnQsIGF0dHJzKSB7XG4gICAgICAgICAgICAgICAgc2NvcGUub25HZXRKc29uID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gJHNjZS50cnVzdEFzSHRtbCgkZmlsdGVyKCdqc29uJykoc2NvcGUuanNvbikpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICByZXBsYWNlOiB0cnVlXG4gICAgICAgIH07XG4gICAgfV0pO1xuXG5cbiIsIlwidXNlIHN0cmljdFwiO1xuLyogZ2xvYmFsIGFuZ3VsYXIsIGpRdWVyeSAqL1xuLyogRGlyZWN0aXZlcyAqL1xuXG52YXIgdXRpbGl0aWVzID0gcmVxdWlyZSgnLi4vbW9kdWxlcy91dGlsaXRpZXMuanMnKVxuXG5hbmd1bGFyLm1vZHVsZSgndGtzLmRpcmVjdGl2ZXMyJywgW10pXG5cbiAgICAuZGlyZWN0aXZlKCd0cmFkZW1hcmtUeXBlJywgWyckaHR0cCcsIGZ1bmN0aW9uICgkaHR0cCkge1xuXG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICByZXN0cmljdDogJ0UnLFxuICAgICAgICAgICAgc2NvcGU6IHtcbiAgICAgICAgICAgICAgICByZWc6ICc9JyxcbiAgICAgICAgICAgICAgICBvcHRucyA6ICc9J1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHRlbXBsYXRlVXJsOiAnL3BhcnRpYWxzL3RyYWRlbWFya190eXBlLmh0bWwnLFxuICAgICAgICAgICAgbGluazogZnVuY3Rpb24gKHNjb3BlLCBlbCwgYXR0cnMpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnc2NwMicsIHNjb3BlKVxuXG4gICAgICAgICAgICAgICAgc2NvcGUudXBsb2FkRmlsZSA9IGZ1bmN0aW9uIChmaWxlcykge1xuXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCd1cGxvYWQgZmlsZScpXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGZpbGVzKVxuICAgICAgICAgICAgICAgICAgICBsZXQgZmQgPSBuZXcgRm9ybURhdGEoKTtcblxuICAgICAgICAgICAgICAgICAgICAvL1Rha2UgdGhlIGZpcnN0IHNlbGVjdGVkIGZpbGVcbiAgICAgICAgICAgICAgICAgICAgZmQuYXBwZW5kKFwibG9nb19maWxlXCIsIGZpbGVzWzBdKTtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ2ZkJywgZmQpXG5cbiAgICAgICAgICAgICAgICAgICAgJGh0dHAucG9zdCgnL2FwaTIvdXBsb2FkX2ZpbGUnLCBmZCwge1xuICAgICAgICAgICAgICAgICAgICAgICAgd2l0aENyZWRlbnRpYWxzOiB0cnVlLFxuICAgICAgICAgICAgICAgICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICdDb250ZW50LVR5cGUnOiB1bmRlZmluZWRcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICB0cmFuc2Zvcm1SZXF1ZXN0OiBhbmd1bGFyLmlkZW50aXR5XG4gICAgICAgICAgICAgICAgICAgIH0pLnN1Y2Nlc3MoZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLnJlZy5sb2dvX3VybCA9IGRhdGEudXJsXG4gICAgICAgICAgICAgICAgICAgICAgICBzY29wZS51cGxvYWRfc3RhdHVzID0gJ1VwbG9hZGVkJ1xuICAgICAgICAgICAgICAgICAgICB9KS5lcnJvcihmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ2ZhaWxlZCcsIGRhdGEpXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfV0pXG5cbiAgICAuZGlyZWN0aXZlKCd0cmFkZW1hcmtDbGFzc2VzJywgWyckaHR0cCcsIGZ1bmN0aW9uICgkaHR0cCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgcmVzdHJpY3Q6ICdFJyxcbiAgICAgICAgICAgIHNjb3BlOiB7XG4gICAgICAgICAgICAgICAgcmVnOiAnPScsXG4gICAgICAgICAgICAgICAgb3B0bnMgOiAnPSdcblxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHRlbXBsYXRlVXJsOiAnL3BhcnRpYWxzL3RyYWRlbWFya19jbGFzc2VzLmh0bWwnLFxuICAgICAgICAgICAgbGluazogZnVuY3Rpb24gKHNjb3BlLCBlbCwgYXR0cnMpIHtcblxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdzY3AyJywgc2NvcGUub3B0bnMpXG5cblxuICAgICAgICAgICAgICAgIHNjb3BlLmxhYmVscyA9IHtcbiAgICAgICAgICAgICAgICAgICAgLy9jbGFzc19zZWxlY3Rpb24gOiBcIlNlbGVjdCB0aGUgY2xhc3NlcyBpbiB3aGljaCB5b3UgbmVlZCB0byByZWdpc3RlciB5b3VyIFRyYWRlbWFyazpcIlxuICAgICAgICAgICAgICAgICAgICBjbGFzc19zZWxlY3Rpb24gOiBcIlNlbGVjdCB0aGUgY2xhc3NlcyB3aGljaCB5b3Ugd291bGQgbGlrZSB5b3VyIHRyYWRlbWFyayBtb25pdG9yZWQgZm9yOlwiXG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgLypcbiAgICAgICAgICAgICAgICBzY29wZS5vcHRpb25zID0ge1xuICAgICAgICAgICAgICAgICAgICBlbmFibGVfY2xhc3NfZGV0YWlsIDogZmFsc2VcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgKi9cblxuXG4gICAgICAgICAgICAgICAgc2NvcGUuY2xhc3Nlc19zZWxlY3RlZCA9IFtdXG5cbiAgICAgICAgICAgICAgICAvLyBnZW5lcmF0ZSBhbiBhcnJheSBmb3IgVUlcbiAgICAgICAgICAgICAgICBmb3IgKHZhciB4ID0gMTsgeCA8PSA0NTsgeCsrKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciBjID0geC50b1N0cmluZygpXG4gICAgICAgICAgICAgICAgICAgIGlmIChjLmxlbmd0aCA9PT0gMSlcbiAgICAgICAgICAgICAgICAgICAgICAgIGMgPSAnMCcgKyBjXG4gICAgICAgICAgICAgICAgICAgIHNjb3BlLmNsYXNzZXNfc2VsZWN0ZWQucHVzaCh7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb2RlOiBjLFxuICAgICAgICAgICAgICAgICAgICAgICAgc2VsZWN0ZWQ6IGZhbHNlXG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgY2xhc3NlczJhcnJheShzY29wZS5yZWcuY2xhc3NlcylcblxuICAgICAgICAgICAgICAgIHNjb3BlLmNsYXNzX2NoYW5nZSA9IGZ1bmN0aW9uIChzKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgaWYgKHMuc2VsZWN0ZWQpIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUucmVnLmNsYXNzZXMucHVzaCh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogcy5jb2RlLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRldGFpbHM6ICcnXG4gICAgICAgICAgICAgICAgICAgICAgICB9KVxuXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgc2NvcGUucmVnLmNsYXNzZXMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoc2NvcGUucmVnLmNsYXNzZXNbaV0ubmFtZSA9PT0gcy5jb2RlKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZS5yZWcuY2xhc3Nlcy5zcGxpY2UoaSwgMSlcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgZnVuY3Rpb24gY2xhc3NlczJhcnJheShjbGFzc2VzKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgZm9yICh2YXIgayA9IDA7IGsgPCBzY29wZS5jbGFzc2VzX3NlbGVjdGVkLmxlbmd0aDsgaysrKVxuICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUuY2xhc3Nlc19zZWxlY3RlZFtrXS5zZWxlY3RlZCA9IGZhbHNlXG5cbiAgICAgICAgICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBjbGFzc2VzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgaW5keCA9IHBhcnNlSW50KGNsYXNzZXNbaV0ubmFtZSlcbiAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLmNsYXNzZXNfc2VsZWN0ZWRbaW5keCAtIDFdLnNlbGVjdGVkID0gdHJ1ZVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XSlcblxuXG5cbiIsIid1c2Ugc3RyaWN0Jztcbi8qZ2xvYmFsIGFuZ3VsYXIsIGR1bXAsIG1vbWVudCwgaW8sIGNvbnNvbGUgKi9cblxuLyogRmlsdGVycyAqL1xuXG5hbmd1bGFyLm1vZHVsZSgndGtzLmZpbHRlcnMnLCBbXSlcbiAuZmlsdGVyKCdyZXZlcnNlJywgZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiBmdW5jdGlvbiAoaW5wdXQsIHVwcGVyY2FzZSkge1xuICAgICAgdmFyIG91dCA9IFwiXCI7XG4gICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGlucHV0Lmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIG91dCA9IGlucHV0LmNoYXJBdChpKSArIG91dDtcbiAgICAgIH1cbiAgICAgIC8vIGNvbmRpdGlvbmFsIGJhc2VkIG9uIG9wdGlvbmFsIGFyZ3VtZW50XG4gICAgICBkdW1wKGlucHV0KVxuICAgICAgaWYgKHVwcGVyY2FzZSlcbiAgICAgICAgb3V0ID0gb3V0LnRvVXBwZXJDYXNlKCk7XG5cbiAgICAgIGR1bXAob3V0KVxuICAgICAgcmV0dXJuIG91dDtcbiAgICB9O1xuICB9KS5maWx0ZXIoJ3RtX2NsYXNzZXMnLCBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uIChpbnB1dCkge1xuICAgICAgaWYgKHR5cGVvZihpbnB1dCkgPT09ICdvYmplY3QnKSB7XG4gICAgICAgIHZhciByZXQgPSAnJ1xuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGlucHV0Lmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgaWYgKHJldCA9PT0gJycpIHtcbiAgICAgICAgICAgIGlmIChpbnB1dFtpXS5kZXRhaWxzKVxuICAgICAgICAgICAgICByZXQgPSBpbnB1dFtpXS5uYW1lICsgJyAoJyArIGlucHV0W2ldLmRldGFpbHMgKyAnKSdcbiAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgICAgcmV0ID0gaW5wdXRbaV0ubmFtZVxuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIGlmIChpbnB1dFtpXS5kZXRhaWxzKVxuICAgICAgICAgICAgICByZXQgPSByZXQgKyAnLScgKyBpbnB1dFtpXS5uYW1lICsgJyAoJyArIGlucHV0W2ldLmRldGFpbHMgKyAnKSdcbiAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgICAgcmV0ID0gcmV0ICsgJy0nICsgaW5wdXRbaV0ubmFtZVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gcmV0XG4gICAgICB9IGVsc2VcbiAgICAgICAgcmV0dXJuICduL2EnXG4gICAgfVxuICB9KVxuXG4gIC8vIHNob3cgYSBjbGFzc2VzIGluIGEgbGluZSB3aXRob3V0IHRleHRcbiAgLmZpbHRlcigndG1fY2xhc3Nlc1NpbXBsZScsIGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gZnVuY3Rpb24gKGlucHV0KSB7XG4gICAgICBpZiAodHlwZW9mKGlucHV0KSA9PT0gJ29iamVjdCcpIHtcbiAgICAgICAgdmFyIHJldCA9ICcnXG4gICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgaW5wdXQubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICBpZiAocmV0ID09PSAnJykge1xuICAgICAgICAgICAgICByZXQgPSBpbnB1dFtpXS5uYW1lXG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICByZXQgPSByZXQgKyAnLScgKyBpbnB1dFtpXS5uYW1lXG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiByZXRcbiAgICAgIH0gZWxzZVxuICAgICAgICByZXR1cm4gJ24vYSdcbiAgICB9XG4gIH0pXG5cblxuICAuZmlsdGVyKCdib29sMnRleHQnLCBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uIChib29sKSB7XG4gICAgICBpZiAoYm9vbClcbiAgICAgICAgcmV0dXJuICdZZXMnXG4gICAgICBlbHNlXG4gICAgICAgIHJldHVybiAnTm8nXG4gICAgfVxuICB9KS5maWx0ZXIoJ3JlZ3R5cGUydGV4dCcsIGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gZnVuY3Rpb24gKHQpIHtcbiAgICAgIGlmICh0ID09PSAndycpXG4gICAgICAgIHJldHVybiAnV29yZG1hcmsnXG4gICAgICBlbHNlIGlmICh0ID09PSAnbCcpXG4gICAgICAgIHJldHVybiAnTG9nbydcbiAgICAgIGVsc2UgaWYgKHQgPT09ICd3bCcpXG4gICAgICAgIHJldHVybiAnV29yZG1hcmsgYW5kIGxvZ28nXG4gICAgICBlbHNlIGlmICh0ID09PSAndG0nKVxuICAgICAgICByZXR1cm4gJ1RyYWRlbWFyaydcbiAgICAgIGVsc2UgaWYgKHQgPT09ICd0bWwnKVxuICAgICAgICByZXR1cm4gJ1RyYWRlbWFyayB3aXRoIGxvZ28nXG4gICAgICBlbHNlXG4gICAgICAgIHJldHVybiAnVW5rbm93bidcbiAgICB9XG4gIH0pLmZpbHRlcigncG9hMnRleHQnLCBbJ1BPQV9JTkZPJyxmdW5jdGlvbiAoUE9BX0lORk8pIHtcbiAgICByZXR1cm4gZnVuY3Rpb24gKHAsIGNvdW50cnkpIHtcbiAgICAgIGlmIChwID09PSAtMSlcbiAgICAgICAgcmV0dXJuICdUaGlzIGFwcGxpY2F0aW9uIGRvZXMgbm90IHJlcXVpcmUgYSBQb3dlciBvZiBBdHRvcm5leS4nXG4gICAgICBlbHNlIGlmIChwID09PSAyKVxuICAgICAgICByZXR1cm4gJ0EgcG93ZXIgb2YgYXR0b3JuZXkgaXMgcmVxdWlyZWQgYmVmb3JlIHJlZ2lzdHJhdGlvbiBjYW4gc3RhcnQuJ1xuICAgICAgZWxzZSB7XG4gICAgICAgIHZhciBsYWJlbCA9IFBPQV9JTkZPW2NvdW50cnldXG4gICAgICAgIGlmIChsYWJlbCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgcmV0dXJuIGxhYmVsW3BdIC8vVE9ETyBoYW5kbGUgaW52YWxpZCBQXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgcmV0dXJuICdObyBQT0EgbGFiZWwgZm91bmQgZm9yICcgKyBwO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XSkuZmlsdGVyKCdjb3VudHJ5MnRleHQnLCBbJ2NvdW50cmllcycsZnVuY3Rpb24gKGNvdW50cmllcykge1xuICAgIHJldHVybiBmdW5jdGlvbiAocCkge1xuICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBjb3VudHJpZXMubGVuZ3RoOyBpICsrKSB7XG4gICAgICAgIGlmIChjb3VudHJpZXNbaV0uY29kZSA9PT0gcClcbiAgICAgICAgICByZXR1cm4gY291bnRyaWVzW2ldLm5hbWVcbiAgICAgIH1cblxuICAgIH1cbiAgfV0pLmZpbHRlcignb3JkZXJTdGF0dXMnLCBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uIChwKSB7XG5cbiAgICAgIGlmIChwID09PSAxKVxuICAgICAgICByZXR1cm4gJ1BFTkRJTkcnXG4gICAgICBlbHNlIGlmIChwID09PSAyKVxuICAgICAgICByZXR1cm4gJ0NvbXBsZXRlZCdcbiAgICAgIGVsc2UgaWYgKHAgPT09IDMpXG4gICAgICAgIHJldHVybiAnQ2FuY2VsbGVkJ1xuICAgICAgZWxzZVxuICAgICAgICByZXR1cm4gJ1Vua25vd24nXG4gICAgfVxuICB9KVxuXG4gIC5maWx0ZXIoJ3RpY2tldFN0YXR1cycsIGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gZnVuY3Rpb24gKHApIHtcblxuICAgICAgaWYgKHAgPT09IDApXG4gICAgICAgIHJldHVybiAnQUNUSVZFJ1xuICAgICAgZWxzZSBpZiAocCA9PT0gMSlcbiAgICAgICAgcmV0dXJuICdDTE9TRUQnXG4gICAgICBlbHNlIGlmIChwID09PSAyKVxuICAgICAgICByZXR1cm4gJ0NhbmNlbGxlZCdcbiAgICAgIGVsc2VcbiAgICAgICAgcmV0dXJuICdVbmtub3duJ1xuICAgIH1cbiAgfSkuXG5cbiAgZmlsdGVyKCdjb2RlMmZsYWdzcmMnLCBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uIChjb2RlKSB7XG4gICAgICBpZiAoY29kZSAhPT0gdW5kZWZpbmVkKSB7XG5cbiAgICAgICAgY29uc29sZS5sb2coY29kZSwgdHlwZW9mIGNvZGUpXG4gICAgICAgIHJldHVybiAnaW1hZ2VzL2ZsYWdzLycgKyBjb2RlLnRvTG93ZXJDYXNlKCkgKyAnLnBuZydcbiAgICAgIH0gLyplbHNlXG4gICAgICAgIHJldHVybiAnaW1hZ2VzLycqL1xuICAgIH1cbiAgfSkuZmlsdGVyKCdjb21tZXJjZV91c2UnLCBbJyRmaWx0ZXInLGZ1bmN0aW9uICgkZmlsdGVyKSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uIChjb2RlLCB1c2VfZGF0ZSkge1xuICAgICAgaWYgKGNvZGUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgIGlmIChjb2RlID09PSAtMSlcbiAgICAgICAgICAgIHJldHVybiAnbm90IGFwcGxpY2FibGUnXG4gICAgICAgICAgZWxzZSBpZiAoY29kZSA9PT0gMClcbiAgICAgICAgICAgIHJldHVybiAnTm8nXG4gICAgICAgICAgZWxzZSBpZiAoY29kZSA9PT0gMSlcbiAgICAgICAgICAgIHJldHVybiAnWWVzICggc3RhcnRpbmcgZGF0ZSA6ICcgKyAkZmlsdGVyKCdkYXRlJykodXNlX2RhdGUsICdtZWRpdW1EYXRlJykgKycpJ1xuICAgICAgICAgIGVsc2UgaWYgKGNvZGUgPT09IDIpXG4gICAgICAgICAgICByZXR1cm4gJ05vLCBidXQgcmVnaXN0ZXJlZCBhYnJvYWQnXG4gICAgICAgICAgZWxzZVxuICAgICAgICAgICAgcmV0dXJuICd1bmtub3cgY29kZSA6ICcgKyBjb2RlXG4gICAgICB9IGVsc2VcbiAgICAgICAgcmV0dXJuICd1bmtub3duJ1xuICAgIH1cbiAgfV0pLmZpbHRlcignc3R1ZHlfdHlwZTJ0ZXh0JywgZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiBmdW5jdGlvbiAodCkge1xuICAgICAgaWYgKHQgPT09IFwiMFwiKVxuICAgICAgICByZXR1cm4gJ0Jhc2ljJ1xuICAgICAgZWxzZSBpZiAodCA9PT0gXCIxXCIpXG4gICAgICAgIHJldHVybiAnRXh0ZW5zaXZlJ1xuICAgICAgZWxzZSBpZiAodCA9PT0gLTEpXG4gICAgICAgIHJldHVybiAnJ1xuICAgICAgZWxzZVxuICAgICAgICByZXR1cm4gJ1Vua25vd24nXG4gICAgfVxuICB9KS5maWx0ZXIoJ2FkbWluX3R5cGUnLCBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uICh0KSB7XG4gICAgICBpZiAodCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIGlmICh0ID09PSAnQScpXG4gICAgICAgICAgcmV0dXJuICdBZG1pbidcbiAgICAgICAgZWxzZSBpZiAodCA9PT0gJ1MnKVxuICAgICAgICAgIHJldHVybiAnU3RhZmYnXG4gICAgICB9XG4gICAgfVxuICB9KVxuICAuZmlsdGVyKCdhdXRob3JzMnRleHQnLCBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uICh0KSB7XG4gICAgICBpZiAodCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIHZhciByZXRcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0Lmxlbmd0aDsgaSArKykge1xuICAgICAgICAgIGlmIChyZXQpXG4gICAgICAgICAgICByZXQgPSByZXQgKyAnLCcgKyB0W2ldLm5hbWVcbiAgICAgICAgICBlbHNlXG4gICAgICAgICAgICByZXQgPSB0W2ldLm5hbWVcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gcmV0XG5cbiAgICAgIH1cbiAgICB9XG4gIH0pLmZpbHRlcignYW10X2luX2Nhc2UnLCBmdW5jdGlvbiAoKSB7XG4gICAgICByZXR1cm4gZnVuY3Rpb24gKHQpIHtcbiAgICAgICAgaWYgKHQgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcmV0dXJuICckICcgKyB0XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9KVxuXG5cblxuXG5cbiIsIlwidXNlIHN0cmljdFwiO1xuLyogZ2xvYmFsIGFuZ3VsYXIgKi9cbnZhciBzZXJ2aWNlczIgPSBhbmd1bGFyLm1vZHVsZSgndGtzLnNlcnZpY2VzMicsIFtdKS52YWx1ZSgndmVyc2lvbicsICcwLjEnKTtcblxuc2VydmljZXMyLmNvbnN0YW50KCdTVEFUSUNfSU5GTycsIHtcblxuICByZXF1aXJlbWVudHM6ICdJbiBvcmRlciB0byBhcHBseSBmb3IgYSBUcmFkZW1hcmsgaW4gVVNBIHlvdSBuZWVkIHRvIHByb3ZpZGUgPGJyLz4nICtcbiAgICAnMS4gSW5mb3JtYXRpb24gdGhyb3VnaCBvdXIgVHJhZGVtYXJrIFJlZ2lzdHJhdGlvbiBPcmRlciBGb3JtLCBhbmQgeW91IG5lZWQgdG88YnIvPiAnICtcbiAgICAnMi4gRGVtb25zdHJhdGUgdXNlIG9mIHRoZSBUcmFkZW1hcmsuIE5vIFBvd2VyIG9mIEF0dG9ybmV5IGlzIG5lZWRlZCBmb3IgVVNBLjxici8+IE1vcmUgdG8gY29tZScsXG4gIHByaWNlczogJ1ByaWNlcyB0byBiZSBwcm92aWRlZCBsYXRlci4uLicsXG4gIHByaW9yaXR5X2hlbHA6J0NsYWltaW5nIHByaW9yaXR5IGlzIHBhcnQgb2YgdGhlIFBhcmlzIENvbnZlbnRpb24gdHJlYXR5LicgK1xuICAgICdJdCBtZWFucyB0aGF0IGlmIHlvdSBoYXZlIGZpbGVkIGEgdHJhZGVtYXJrIHdpdGhpbiB0aGUgbGFzdCA2IG1vbnRocyBpbiBhbnkgb2YgdGhlIHNpZ25hdG9yeSBjb3VudHJpZXMgKG1vc3QgY291bnRyaWVzIG9mIHRoZSB3b3JsZCksICcgK1xuICAgICd5b3UgY2FuIHVzZSB0aGUgZmlsaW5nIGRhdGUgb2YgdGhpcyBmaXJzdCByZWdpc3RyYXRpb24gYXMgdGhlIGZpbGluZyBkYXRlIGluIHRoZSBvdGhlciBzaWduYXRvcnkgY291bnRyaWVzLiAgVGhpcyByaWdodCBtYXkgYmUgdXNlZnVsICcgK1xuICAgICd0byB0aGUgb3duZXIgb2YgdGhlIHRyYWRlbWFyaywgdG8gcHJvdGVjdCBoaXMgaW50ZWxsZWN0dWFsIHByb3BlcnR5LCBpbiBjYXNlcyBvZiBsZWdhbCBjb25mbGljdHMgd2l0aCBvdGhlciBwYXJ0aWVzLicsXG4gIGNsYXNzX2hlbHAgOiAnV2hlbiByZWdpc3RlcmluZyBhIHRyYWRlbWFyayB5b3UgbmVlZCB0byBzcGVjaWZ5IHRoZSBwcm9kdWN0cyBhbmQgc2VydmljZXMgdGhhdCB3aWxsIGJlIGFzc29jaWF0ZWQgd2l0aCB5b3VyIHRyYWRlbWFyay4nICtcbiAgICAnIFRoZSBsYXJnZSBtYWpvcml0eSBvZiBjb3VudHJpZXMgb2YgdGhlIHdvcmxkIGhhdmUgYWRvcHRlZCB0aGUgSW50ZXJuYXRpb25hbCBDbGFzc2lmaWNhdGlvbiBvZiBOaWNlLiBUaGlzIHN5c3RlbSBncm91cHMgYWxsIHByb2R1Y3RzIGFuZCAnICtcbiAgICAnc2VydmljZXMgaW50byA0NSBjbGFzc2VzIC0gMzQgZm9yIHByb2R1Y3RzLCAxMSBmb3Igc2VydmljZXMgLSBwZXJtaXR0aW5nIHlvdSB0byBzcGVjaWZ5IHByZWNpc2UgYW5kIGNsZWFyIGNsYXNzZXMgY292ZXJpbmcgeW91ciB0cmFkZW1hcmsuICcgK1xuICAgICdUaGUgcHJvdGVjdGlvbiB0aGF0IGlzIG9mZmVyZWQgdG8gYSByZWdpc3RlcmVkIHRyYWRlbWFyayBjb3ZlcnMgb25seSB0aGUgY2xhc3NlcyBzcGVjaWZpZWQgYXQgdGhlIHRpbWUgb2YgcmVnaXN0cmF0aW9uLCB0aGVyZWZvcmUsIGFsbG93aW5nJyArXG4gICAgJyB0d28gaWRlbnRpY2FsIHRyYWRlbWFya3MgdG8gY29leGlzdCBpbiBkaXN0aW5jdCBjbGFzc2VzLicsXG4gIGNvbW1lcmNlX3VzZV9oZWxwIDogJ+KAnFVzZWQgaW4gY29tbWVyY2XigJ0gbWVhbnMgdGhhdCB0aGUgbWFyayBpcyB1c2VkIGluIHRoZSBvcmRpbmFyeSBjb3Vyc2Ugb2YgdHJhZGUuIEZvciBleGFtcGxlLCBmb3IgZ29vZHMgaXQgd291bGQgYmUgdXNlZCB3aGVuIGl0IGlzIHBsYWNlZCBpbiBhbnkgbWFubmVyIG9uIHRoZSBnb29kcywgdGhlaXIgY29udGFpbmVycyBvciB0aGVpciBkaXNwbGF5cy4gRm9yIHNlcnZpY2VzIGl0IGlzIHVzZWQgd2hlbiB0aGUgdHJhZGVtYXJrIGlzIHVzZWQgb3IgZGlzcGxheWVkIGluIGFkdmVydGlzaW5nIG9yIGR1cmluZyB0aGUgc2FsZXMgcHJvY2Vzcy4nXG59KS5jb25zdGFudCgnUE9BX0lORk8nLCB7XG5cbiAgJ0lOJzoge1xuICAgICAgICAgIDA6ICdJIHdpbGwgYmUgc2VuZGluZyBpdCB3aXRoaW4gMTIwIGRheXM7IGZpbGUgdHJhZGVtYXJrIGltbWVkaWF0ZWx5LicsXG4gICAgICAgICAgMTogJ05vLCBkbyBub3QgZmlsZSBhcHBsaWNhdGlvbiB1bnRpbCByZWNlaXZlZCB0aGUgUG93ZXIgb2YgQXR0b3JuZXkuJ1xuICAgICAgICB9LFxuICAnQVInOiB7XG4gICAgMDogJ0kgd2lsbCBiZSBzZW5kaW5nIGl0IHdpdGhpbiA0MCBkYXlzOyBmaWxlIHRyYWRlbWFyayBpbW1lZGlhdGVseS4nLFxuICAgIDE6ICdObywgZG8gbm90IGZpbGUgYXBwbGljYXRpb24gdW50aWwgcmVjZWl2ZWQgdGhlIFBvd2VyIG9mIEF0dG9ybmV5LidcbiAgfSxcbiAgJ0FaJzoge1xuICAgIDA6ICdJIHdpbGwgYmUgc2VuZGluZyBpdCB3aXRoaW4gMzAgZGF5czsgZmlsZSB0cmFkZW1hcmsgaW1tZWRpYXRlbHkuJyxcbiAgICAxOiAnTm8sIGRvIG5vdCBmaWxlIGFwcGxpY2F0aW9uIHVudGlsIHJlY2VpdmVkIHRoZSBQb3dlciBvZiBBdHRvcm5leS4nXG4gIH0sXG4gICdCTyc6IHtcbiAgICAwOiAnSSB3aWxsIGJlIHNlbmRpbmcgaXQgd2l0aGluIDMwIGRheXM7IGZpbGUgdHJhZGVtYXJrIGltbWVkaWF0ZWx5LicsXG4gICAgMTogJ05vLCBkbyBub3QgZmlsZSBhcHBsaWNhdGlvbiB1bnRpbCByZWNlaXZlZCB0aGUgUG93ZXIgb2YgQXR0b3JuZXkuJ1xuICB9LFxuICAnQ0EnOiB7XG4gICAgMDogJ0kgd2lsbCBiZSBzZW5kaW5nIGl0IHdpdGhpbiA0NSBkYXlzOyBmaWxlIHRyYWRlbWFyayBpbW1lZGlhdGVseS4nLFxuICAgIDE6ICdObywgZG8gbm90IGZpbGUgYXBwbGljYXRpb24gdW50aWwgcmVjZWl2ZWQgdGhlIFBvd2VyIG9mIEF0dG9ybmV5LidcbiAgfSxcbiAgJ0NPJzoge1xuICAgIDA6ICdJIHdpbGwgYmUgc2VuZGluZyBpdCB3aXRoaW4gNjAgZGF5czsgZmlsZSB0cmFkZW1hcmsgaW1tZWRpYXRlbHkuJyxcbiAgICAxOiAnTm8sIGRvIG5vdCBmaWxlIGFwcGxpY2F0aW9uIHVudGlsIHJlY2VpdmVkIHRoZSBQb3dlciBvZiBBdHRvcm5leS4nXG4gIH0sXG4gICdNWCc6IHtcbiAgICAwOiAnSSB3aWxsIGJlIHNlbmRpbmcgaXQgd2l0aGluIDMwIGRheXM7IGZpbGUgdHJhZGVtYXJrIGltbWVkaWF0ZWx5KCQxNTEpJyxcbiAgICAxOiAnTm8sIGRvIG5vdCBmaWxlIGFwcGxpY2F0aW9uIHVudGlsIHJlY2VpdmVkIHRoZSBQb3dlciBvZiBBdHRvcm5leS4nXG4gIH0sXG4gICdCUic6IHtcbiAgICAwOiAnSSB3aWxsIGJlIHNlbmRpbmcgaXQgd2l0aGluIDU1IGRheXM7IGZpbGUgdHJhZGVtYXJrIGltbWVkaWF0ZWx5KCQxNjMpJyxcbiAgICAxOiAnTm8sIGRvIG5vdCBmaWxlIGFwcGxpY2F0aW9uIHVudGlsIHJlY2VpdmVkIHRoZSBQb3dlciBvZiBBdHRvcm5leS4nXG4gIH0sXG4gICdOTyc6IHtcbiAgICAwOiAnSSB3aWxsIGJlIHNlbmRpbmcgaXQgd2l0aGluIDkwIGRheXM7IGZpbGUgdHJhZGVtYXJrIGltbWVkaWF0ZWx5JyxcbiAgICAxOiAnTm8sIGRvIG5vdCBmaWxlIGFwcGxpY2F0aW9uIHVudGlsIHJlY2VpdmVkIHRoZSBQb3dlciBvZiBBdHRvcm5leS4nXG4gIH0sXG4gICdSVSc6IHtcbiAgICAwOiAnSSB3aWxsIGJlIHNlbmRpbmcgaXQgd2l0aGluIDYwIGRheXM7IGZpbGUgdHJhZGVtYXJrIGltbWVkaWF0ZWx5JyxcbiAgICAxOiAnTm8sIGRvIG5vdCBmaWxlIGFwcGxpY2F0aW9uIHVudGlsIHJlY2VpdmVkIHRoZSBQb3dlciBvZiBBdHRvcm5leS4nXG4gIH0sXG4gICdWTic6IHtcbiAgICAwOiAnSSB3aWxsIGJlIHNlbmRpbmcgaXQgd2l0aGluIDMwIGRheXM7IGZpbGUgdHJhZGVtYXJrIGltbWVkaWF0ZWx5ICgkNTApJyxcbiAgICAxOiAnTm8sIGRvIG5vdCBmaWxlIGFwcGxpY2F0aW9uIHVudGlsIHJlY2VpdmVkIHRoZSBQb3dlciBvZiBBdHRvcm5leS4nXG4gIH0sXG4gICdUVyc6IHtcbiAgICAwOiAnSSB3aWxsIGJlIHNlbmRpbmcgaXQgd2l0aGluIDkwIGRheXM7IGZpbGUgdHJhZGVtYXJrIGltbWVkaWF0ZWx5JyxcbiAgICAxOiAnTm8sIGRvIG5vdCBmaWxlIGFwcGxpY2F0aW9uIHVudGlsIHJlY2VpdmVkIHRoZSBQb3dlciBvZiBBdHRvcm5leS4nXG4gIH0sXG4gICdDTic6IHswOiAnJywgMTogJyd9LFxuICAnSEVMUCc6e1xuICAgICdJTic6J0luIElORElBIGl0IGlzIHBvc3NpYmxlIHRvIGZpbGUgaW1tZWRpYXRlbHkgeW91ciB0cmFkZW1hcmsgYXBwbGljYXRpb24sIHdpdGhvdXQgUG93ZXIgb2YgQXR0b3JuZXksIHByb3ZpZGVkIHRoYXQgeW91IHdpbGwgc2VuZCBpdCB3aXRoaW4gMTIwIERheXMuJ1xuICB9LFxuICAnUE9BX01VU1QnOiAnUGxlYXNlIG5vdGUgdGhhdCBpbiBvcmRlciB0byBmaWxlIHlvdXIgdHJhZGVtYXJrIGFwcGxpY2F0aW9uLCB3ZSBuZWVkIHRoYXQgeW91IHNlbmQgdXMgYSBQb3dlciBvZiBBdHRvcm5leSwgYmVsb3cgeW91IGNhbiBmaW5kIGEgVGVtcGxhdGUnXG5cbn0pXG5cbiIsIlwidXNlIHN0cmljdFwiO1xuLyogZ2xvYmFsIGFuZ3VsYXIqL1xuYW5ndWxhci5tb2R1bGUoJ21vZC5hdXRob3JpemUnLCBbXSkudmFsdWUoJ3ZlcnNpb24nLCAnMC4xJylcblxuICAvLyBjb250YWlucyBsb2dnZWQgdXNlciBpbmZvLCB1c2VzIHNlc3Npb25TdG9yYWdlLCBjYW4gYmUgY2hhbmdlZCB0byB1c2UgbG9jYWxTdG9yYWdlIGlmIG5lZWRlZFxuXG4gIC5zZXJ2aWNlKCdTZXNzaW9uJywgW1wiJHdpbmRvd1wiLCBcIiRjb29raWVTdG9yZVwiLCBmdW5jdGlvbiAoJHdpbmRvdywgJGNvb2tpZVN0b3JlKSB7XG4gICAgdGhpcy5jcmVhdGUgPSBmdW5jdGlvbiAoc2Vzc2lvbklkLCB1c2VySUQsIG5hbWUsIGxvY2FsZSkge1xuXG4gICAgICAkY29va2llU3RvcmUucHV0KCdzZXNzaW9uSUQnLCBzZXNzaW9uSWQpXG4gICAgICAkY29va2llU3RvcmUucHV0KCduYW1lJywgbmFtZSlcbiAgICAgICRjb29raWVTdG9yZS5wdXQoJ3VzZXJJRCcsIHVzZXJJRClcbiAgICAgICRjb29raWVTdG9yZS5wdXQoJ2xvY2FsZScsIGxvY2FsZSlcblxuICAgIH07XG5cbiAgICB0aGlzLmRlc3Ryb3kgPSBmdW5jdGlvbiAoKSB7XG5cbiAgICAgICRjb29raWVTdG9yZS5yZW1vdmUoJ3Nlc3Npb25JRCcpXG4gICAgICAkY29va2llU3RvcmUucmVtb3ZlKCduYW1lJylcbiAgICAgICRjb29raWVTdG9yZS5yZW1vdmUoJ3VzZXJJRCcpXG4gICAgICAkY29va2llU3RvcmUucmVtb3ZlKCdsb2NhbGUnKVxuICAgIH07XG5cbiAgICB0aGlzLmdldFNlc3Npb25JRCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIHJldHVybiAkY29va2llU3RvcmUuZ2V0KCdzZXNzaW9uSUQnKVxuICAgIH1cblxuICAgIHRoaXMuZ2V0TmFtZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIHJldHVybiAkY29va2llU3RvcmUuZ2V0KCduYW1lJylcbiAgICB9XG5cbiAgICB0aGlzLmdldFVzZXJJRCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIHJldHVybiAkY29va2llU3RvcmUuZ2V0KCd1c2VySUQnKVxuICAgIH1cblxuICAgIHJldHVybiB0aGlzO1xuICB9XSlcblxuICAvLyBhdXRob3JpemUgc2VydmljZXNcblxuICAuZmFjdG9yeSgnYXV0aFNlcnZpY2UnLCBbJyRodHRwJywgJyRxJywgJyR0aW1lb3V0JywgJ1Nlc3Npb24nLCBmdW5jdGlvbiAoJGh0dHAsICRxLCAkdGltZW91dCwgU2Vzc2lvbikge1xuXG4gICAgdmFyIGZhc2FkID0ge307XG5cbiAgICB2YXIgYXV0aGVudGljYXRlID0gZnVuY3Rpb24gKHVzZXIpIHtcbiAgICAgIHZhciBkZWZlcnJlZCA9ICRxLmRlZmVyKCk7XG5cbiAgICAgICRodHRwLnBvc3QoJy9zaWduaW4vJywgdXNlcikuXG4gICAgICAgIHN1Y2Nlc3MoZnVuY3Rpb24gKGRhdGEsIHN0YXR1cykge1xuICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGEpXG4gICAgICAgICAgY29uc29sZS5sb2coc3RhdHVzKVxuICAgICAgICAgIGlmIChkYXRhLnNlc3Npb25JRCAhPT0gbnVsbClcbiAgICAgICAgICAgIFNlc3Npb24uY3JlYXRlKGRhdGEuc2Vzc2lvbklELCBkYXRhLnVzZXJJRCwgZGF0YS5uYW1lLCBkYXRhLmxvY2FsZSk7XG4gICAgICAgICAgZGVmZXJyZWQucmVzb2x2ZSh7YXV0aGVudGljYXRlZDogZGF0YS5zZXNzaW9uSUQgIT09IG51bGwsIG1lc3NhZ2U6IGRhdGEubXNnLCBuYW1lOiBkYXRhLm5hbWV9KTtcbiAgICAgICAgfSkuXG4gICAgICAgIGVycm9yKGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgICAgIGRlZmVycmVkLnJlc29sdmUoe2F1dGhlbnRpY2F0ZWQ6IGZhbHNlLCBtZXNzYWdlOiAnRXJyb3Igb2NjdXJyZWQgd2hpbGUgYXV0aGVudGljYXRpbmcuJ30pO1xuICAgICAgICB9KTtcblxuICAgICAgcmV0dXJuIGRlZmVycmVkLnByb21pc2U7XG4gICAgfTtcblxuICAgIHZhciBpc0F1dGhlbnRpY2F0ZWQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICB2YXIgc2lkID0gU2Vzc2lvbi5nZXRTZXNzaW9uSUQoKVxuICAgICAgaWYgKChzaWQgPT09IHVuZGVmaW5lZCkgfHwgKHNpZCA9PT0gbnVsbCkpXG4gICAgICAgIHJldHVybiBmYWxzZVxuICAgICAgZWxzZVxuICAgICAgICByZXR1cm4gdHJ1ZVxuXG4gICAgfTtcblxuICAgIHZhciByZWdpc3RlciA9IGZ1bmN0aW9uICh1c2VyKSB7XG4gICAgICB2YXIgZGVmZXJyZWQgPSAkcS5kZWZlcigpO1xuXG4gICAgICAkaHR0cC5wb3N0KCcvc2lnbnVwJywgdXNlcikuXG4gICAgICAgIHN1Y2Nlc3MoZnVuY3Rpb24gKGRhdGEsIHN0YXR1cykge1xuICAgICAgICAgIGRlZmVycmVkLnJlc29sdmUoe3JlZ2lzdGVyZWQ6IGRhdGEucmVnaXN0ZXJlZCwgbWVzc2FnZTogZGF0YS5tZXNzYWdlfSk7XG4gICAgICAgIH0pLlxuICAgICAgICBlcnJvcihmdW5jdGlvbiAoZXJyb3IpIHtcbiAgICAgICAgICBkZWZlcnJlZC5yZXNvbHZlKHtyZWdpc3RlcmVkOiBmYWxzZSwgbWVzc2FnZTogJ1NvcnJ5LCBlcnJvciBvY2N1cnJlZCB3aGlsZSBzaWduaW5nIHVwLCBwbGVhc2UgdHJ5IGFnYWluLid9KTtcbiAgICAgICAgfSk7XG5cbiAgICAgIHJldHVybiBkZWZlcnJlZC5wcm9taXNlO1xuICAgIH07XG5cbiAgICB2YXIgbG9nT3V0ID0gZnVuY3Rpb24gKCkge1xuICAgICAgU2Vzc2lvbi5kZXN0cm95KClcbiAgICB9O1xuXG4gICAgdmFyIHJlcXVlc3RfcGFzc3dvcmRfcmVzZXQgPSBmdW5jdGlvbiAoZW1haWwpIHtcblxuICAgICAgdmFyIGRlZmVycmVkID0gJHEuZGVmZXIoKTtcblxuICAgICAgJGh0dHAuZ2V0KCcvYXBpMi9wYXNzd29yZF9yZXNldD9lbWFpbD0nICsgZW1haWwpLnN1Y2Nlc3MoZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgY29uc29sZS5sb2coZGF0YSlcbiAgICAgICAgZGVmZXJyZWQucmVzb2x2ZSh7c3RhdHVzOiBkYXRhLnN0YXR1cyA9PT0gJ29rJywgbWVzc2FnZTogZGF0YS5tZXNzYWdlfSk7XG4gICAgICB9KS5lcnJvcihmdW5jdGlvbiAoZXJyb3IpIHtcbiAgICAgICAgZGVmZXJyZWQucmVzb2x2ZSh7c3RhdHVzOiAnZXJyJywgbWVzc2FnZTogJ1NvcnJ5LCBlcnJvciBvY2N1cnJlZCB3aGlsZSBhY2Nlc3Npbmcgc2VydmVyLCBwbGVhc2UgdHJ5IGFnYWluLid9KTtcbiAgICAgIH0pO1xuXG4gICAgICByZXR1cm4gZGVmZXJyZWQucHJvbWlzZTtcbiAgICB9O1xuXG5cbiAgICBmYXNhZC5zaWduSW4gPSBmdW5jdGlvbiAodXNlcikge1xuICAgICAgcmV0dXJuIGF1dGhlbnRpY2F0ZSh1c2VyKTtcbiAgICB9O1xuICAgIGZhc2FkLnNpZ25VcCA9IGZ1bmN0aW9uICh1c2VyKSB7XG4gICAgICByZXR1cm4gcmVnaXN0ZXIodXNlcik7XG4gICAgfTtcbiAgICBmYXNhZC5sb2dPdXQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICByZXR1cm4gbG9nT3V0KClcbiAgICB9O1xuICAgIGZhc2FkLmlzU2lnbmVkSW4gPSBmdW5jdGlvbiAoKSB7XG4gICAgICByZXR1cm4gaXNBdXRoZW50aWNhdGVkKClcbiAgICB9O1xuICAgIGZhc2FkLnVzZXJOYW1lID0gZnVuY3Rpb24gKCkge1xuICAgICAgcmV0dXJuIFNlc3Npb24uZ2V0TmFtZSgpXG4gICAgfTtcblxuICAgIGZhc2FkLnJlcXVlc3RfcGFzc3dvcmRfcmVzZXQgPSBmdW5jdGlvbiAoZW1haWwpIHtcbiAgICAgIHJldHVybiByZXF1ZXN0X3Bhc3N3b3JkX3Jlc2V0KGVtYWlsKVxuICAgIH1cblxuICAgIHJldHVybiBmYXNhZDtcblxuICB9XSlcblxuICAuY29uc3RhbnQoJ0FVVEhfRVZFTlRTJywge1xuICAgIGxvZ2luU3VjY2VzczogJ2F1dGgtbG9naW4tc3VjY2VzcycsXG4gICAgbG9naW5GYWlsZWQ6ICdhdXRoLWxvZ2luLWZhaWxlZCcsXG4gICAgbG9nb3V0U3VjY2VzczogJ2F1dGgtbG9nb3V0LXN1Y2Nlc3MnLFxuICAgIHNlc3Npb25UaW1lb3V0OiAnYXV0aC1zZXNzaW9uLXRpbWVvdXQnLFxuICAgIG5vdEF1dGhlbnRpY2F0ZWQ6ICdhdXRoLW5vdC1hdXRoZW50aWNhdGVkJyxcbiAgICBub3RBdXRob3JpemVkOiAnYXV0aC1ub3QtYXV0aG9yaXplZCcsXG4gICAgY2FydFVwZGF0ZWQ6ICdjYXJ0LXVwZGF0ZWQnXG4gIH0pXG5cbiAgLy8gYSBzaW1wbGUgbG9jYWwgZGIgc3RvcmUsIHN0b3JlcyBrZXkvdmFsdWUgaW4gc2Vzc2lvblN0b3JhZ2UsIGlkZWFsIGZvciBrZWVwcGluZyBzb21lIGRhdGEgbGlrZSBVUkwgaW4gdGhlIHN0b3JlLFxuICAvLyBhbmQgdXNlIHRoZSBrZXkgdG8gcGFzcyB0byB1cmxcblxuICAuc2VydmljZSgnREJTdG9yZScsWyckd2luZG93JywgZnVuY3Rpb24gKCR3aW5kb3cpIHtcblxuICAgIHRoaXMuY3JlYXRlID0gZnVuY3Rpb24gKCkge1xuICAgIH07XG5cbiAgICB0aGlzLmRlc3Ryb3kgPSBmdW5jdGlvbiAoKSB7XG4gICAgICBkZWxldGUgJHdpbmRvdy5zZXNzaW9uU3RvcmFnZS5kYlN0b3JlXG4gICAgfTtcblxuICAgIHRoaXMuZ2V0ID0gZnVuY3Rpb24gKGtleSkge1xuICAgICAgdmFyIHFrZXkgPSAnREJfJyArIGtleVxuICAgICAgdmFyIHJldCA9ICR3aW5kb3cuc2Vzc2lvblN0b3JhZ2VbcWtleV1cbiAgICAgIGRlbGV0ZSAkd2luZG93LnNlc3Npb25TdG9yYWdlW3FrZXldXG4gICAgICByZXR1cm4gcmV0XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gZ2VuZXJhdGVRdWlja0d1aWQoKSB7XG4gICAgICByZXR1cm4gTWF0aC5yYW5kb20oKS50b1N0cmluZygzNikuc3Vic3RyaW5nKDIsIDE1KSArXG4gICAgICAgIE1hdGgucmFuZG9tKCkudG9TdHJpbmcoMzYpLnN1YnN0cmluZygyLCAxNSk7XG4gICAgfVxuXG4gICAgdGhpcy5hZGQgPSBmdW5jdGlvbiAob2JqKSB7XG4gICAgICB2YXIga2V5ID0gZ2VuZXJhdGVRdWlja0d1aWQoKVxuICAgICAgJHdpbmRvdy5zZXNzaW9uU3RvcmFnZVsnREJfJyArIGtleV0gPSBvYmpcbiAgICAgIHJldHVybiBrZXlcbiAgICB9XG5cbiAgICByZXR1cm4gdGhpcztcbiAgfV0pXG5cbiIsIlwidXNlIHN0cmljdFwiO1xuLyogZ2xvYmFsIGFuZ3VsYXIqL1xuXG5hbmd1bGFyLm1vZHVsZSgnbW9kLmNhcmRzJywgW10pXG4gIC5maWx0ZXIoXG4gICdyYW5nZScsIGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgZmlsdGVyID1cbiAgICAgIGZ1bmN0aW9uIChhcnIsIGxvd2VyLCB1cHBlcikge1xuICAgICAgICBmb3IgKHZhciBpID0gbG93ZXI7IGkgPD0gdXBwZXI7IGkrKykgYXJyLnB1c2goaSlcbiAgICAgICAgcmV0dXJuIGFyclxuICAgICAgfVxuICAgIHJldHVybiBmaWx0ZXJcbiAgfVxuKVxuICAuc2VydmljZSgnQ2FyZHMnLCBbJyRodHRwJywgJyRxJywgZnVuY3Rpb24gKCRodHRwLCAkcSkge1xuXG4gICAgdGhpcy5nZXRfc2hvcHBpbmdfY2FydF9kcG0gPSBmdW5jdGlvbiAoY2FydF9pZCkge1xuICAgICAgdmFyIGRlZmVycmVkID0gJHEuZGVmZXIoKTtcblxuICAgICAgJGh0dHAuZ2V0KCcvYXBpX3BheW1lbnQvZ2V0X3Nob3BwaW5nX2NhcnRfZHBtLycgKyBjYXJ0X2lkKS5zdWNjZXNzKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdiYWNrIGZyb20gY2FydF9kcG0nKVxuICAgICAgICBjb25zb2xlLmxvZyhkYXRhKVxuICAgICAgICBkZWZlcnJlZC5yZXNvbHZlKGRhdGEpO1xuICAgICAgfSkuZXJyb3IoZnVuY3Rpb24gKGVycm9yKSB7XG4gICAgICAgIGRlZmVycmVkLnJlc29sdmUoZXJyb3IpO1xuICAgICAgfSk7XG4gICAgICByZXR1cm4gZGVmZXJyZWQucHJvbWlzZTtcbiAgICB9XG5cbiAgICAvLyB0aGlzIHJldHVybnMgYW4gYmxhbmsgY2FyZCBpbmZvLCBzb21lIHN1Z2dlc3RlZCBhZGRyZXNzXG5cbiAgICB0aGlzLmdldF91c2VyX2luZm8gPSBmdW5jdGlvbiAoY2FydF9pZCkge1xuICAgICAgdmFyIGRlZmVycmVkID0gJHEuZGVmZXIoKTtcblxuICAgICAgJGh0dHAuZ2V0KCcvYXBpX3BheW1lbnQvZ2V0X3VzZXJfaW5mby8nICsgY2FydF9pZCkuc3VjY2VzcyhmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICBjb25zb2xlLmxvZygnYmFjayBmcm9tIGNhcnRfZHBtJylcbiAgICAgICAgY29uc29sZS5sb2coZGF0YSlcbiAgICAgICAgZGVmZXJyZWQucmVzb2x2ZShkYXRhKTtcbiAgICAgIH0pLmVycm9yKGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgICBkZWZlcnJlZC5yZXNvbHZlKGVycm9yKTtcbiAgICAgIH0pO1xuXG4vKlxuICAgICAgdmFyIGluZm8gID0ge1xuICAgICAgICBcInhfY2FyZF9udW1cIjogXCI2MDExMDAwMDAwMDAwMDEyXCIsXG4gICAgICAgIHhfZXhwX2RhdGU6IFwiMDQvMTdcIixcbiAgICAgICAgeF9jYXJkX2NvZGU6IFwiNzgyXCIsXG4gICAgICAgIHhfZmlyc3RfbmFtZTogXCJKb2huXCIsXG4gICAgICAgIHhfbGFzdF9uYW1lOiBcIkRvZVwiLFxuICAgICAgICB4X2FkZHJlc3M6IFwiMTIzIE1haW4gU3RyZWV0XCIsXG4gICAgICAgIHhfY2l0eTogXCJCb3N0b25cIixcbiAgICAgICAgeF9zdGF0ZTogXCJNQVwiLFxuICAgICAgICB4X3ppcDogXCIwMjE0MlwiLFxuICAgICAgICB4X2NvdW50cnk6IFwiVVNcIlxuICAgICAgfTtcbiAgICAgICovXG4gICAgICByZXR1cm4gZGVmZXJyZWQucHJvbWlzZTtcbiAgICB9XG5cblxuICAgIHJldHVybiB0aGlzO1xuXG4gIH1dKSIsIlwidXNlIHN0cmljdFwiO1xuLyogZ2xvYmFscyBhbmd1bGFyICovXG5cbnZhciBjb3VudHJpZXMgPSByZXF1aXJlKCdjb3VudHJpZXMnKVxuXG5hbmd1bGFyLm1vZHVsZSgnbW9kLmNvdW50cmllcycsIFtdKS5mYWN0b3J5KFwiY291bnRyaWVzXCIsIGZ1bmN0aW9uKCkge1xuICAgIGNvbnNvbGUubG9nKCcxMjMnKVxuICAgIHZhciBpID0gMTAwMFxuICAgIGlmIChpID09PSAxMDAwKVxuICAgICAgY29uc29sZS5sb2coJ2kgYW0gaGVyZSBhbmQgdGhlcmUnKVxuICAgIHJldHVybiBjb3VudHJpZXMuZ2V0X2NvdW50cmllcygpXG59KVxuXG4iLCJcInVzZSBzdHJpY3RcIjtcbi8qIGdsb2JhbCBhbmd1bGFyKi9cblxuYW5ndWxhci5tb2R1bGUoJ21vZC5wYWdlcicsIFtdKS5kaXJlY3RpdmUoJ3BhZ2VyMicsIFsnJGxvY2F0aW9uJywgZnVuY3Rpb24gKCRsb2NhdGlvbikge1xuICByZXR1cm4ge1xuICAgIHJlc3RyaWN0OiAnRScsXG4gICAgc2NvcGU6IHtcbiAgICAgIGN1cnJlbnQ6ICc9JyxcbiAgICAgIHBhZ2VDb3VudDogJz0nLFxuICAgICAgc2hvd01vcmVQYWdlczogJ0AnXG4gICAgfSxcbiAgICB0ZW1wbGF0ZVVybDogJy9wYXJ0aWFscy9wYWdlci5odG1sJyxcbiAgICBsaW5rOiBmdW5jdGlvbiAoc2NvcGUsIGl0ZXJTdGFydEVsZW1lbnQsIGF0dHJzKSB7XG5cbiAgICAgIHNjb3BlLnBhZ2VyUGFnZSA9IDBcbiAgICAgIHNjb3BlLnBhZ2VyU2l6ZSA9IDEwXG5cbiAgICAgIHNjb3BlLnBhZ2VyX2NvdW50ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAoc2NvcGUucGFnZUNvdW50ICE9PSB1bmRlZmluZWQpXG4gICAgICAgICAgcmV0dXJuIE1hdGguY2VpbChzY29wZS5wYWdlQ291bnQgLyBzY29wZS5wYWdlclNpemUpXG4gICAgICAgIGVsc2VcbiAgICAgICAgICByZXR1cm4gMDtcbiAgICAgIH1cblxuICAgICAgc2NvcGUuZmlyc3RfcGFnZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIHJldCA9IFtdLCBpLCBuXG4gICAgICAgIGlmIChzY29wZS5wYWdlQ291bnQgIT0gdW5kZWZpbmVkKSB7IC8vIHBhZ2VDb3VudCB3aWxsIGJlIGFzc2lnbmVkXG4gICAgICAgICAgbiA9IDEwXG4gICAgICAgICAgaWYgKHNjb3BlLnBhZ2VDb3VudCA8IG4pXG4gICAgICAgICAgICBuID0gc2NvcGUucGFnZUNvdW50XG4gICAgICAgICAgZm9yIChpID0gMDsgaSA8IG47IGkrKylcbiAgICAgICAgICAgIHJldC5wdXNoKGkgKyAxKVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiByZXRcbiAgICAgIH1cblxuICAgICAgc2NvcGUucGFnZXMgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciByZXQgPSBbXSwgaTtcbiAgICAgICAgZm9yIChpID0gMDsgaSA8IHNjb3BlLnBhZ2VDb3VudDsgaSsrKVxuICAgICAgICAgIHJldC5wdXNoKGkgKyAxKVxuICAgICAgICByZXR1cm4gcmV0XG4gICAgICB9XG5cbiAgICAgIC8vIHRyeSB0byBzZWUgaWYgcGFnZSBudW1iZXIgaGFzIGJlZW4gc2V0IGJ5IGNhbGxlciwgYW5kIGFkanVzdCB0aGUgY3VycmVudCBwYWdlciBub1xuICAgICAgc2NvcGUucmVfYWRqdXN0X2N1cnJlbnRfcGFnZSA9IGZ1bmN0aW9uICgpIHtcblxuICAgICAgICBpZiAoc2NvcGUucGFnZUNvdW50ICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICBpZiAoc2NvcGUucGFnZXJTaXplID4gMCkge1xuICAgICAgICAgICAgdmFyIG4gPSBNYXRoLmZsb29yKHNjb3BlLmN1cnJlbnQgLyBzY29wZS5wYWdlclNpemUpXG4gICAgICAgICAgICBpZiAobiAhPT0gc2NvcGUucGFnZXJQYWdlKVxuICAgICAgICAgICAgICBzY29wZS5wYWdlclBhZ2UgPSBuXG4gICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgIH1cblxuICAgICAgc2NvcGUuY3VycmVudF9wYWdlcl9wYWdlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAoc2NvcGUucGFnZUNvdW50ICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICB2YXIgcmV0ID0gW10sIGk7XG4gICAgICAgICAgc2NvcGUucmVfYWRqdXN0X2N1cnJlbnRfcGFnZSgpXG4gICAgICAgICAgdmFyIG9mZnNldCA9IHNjb3BlLnBhZ2VyUGFnZSAqIHNjb3BlLnBhZ2VyU2l6ZVxuICAgICAgICAgIGZvciAoaSA9IG9mZnNldDsgaSA8IG9mZnNldCArIHNjb3BlLnBhZ2VyU2l6ZTsgaSsrKSB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZygnYmInLCBpIDwgc2NvcGUucGFnZUNvdW50KVxuICAgICAgICAgICAgaWYgKGkgPCBzY29wZS5wYWdlQ291bnQpXG4gICAgICAgICAgICAgIHJldC5wdXNoKGkgKyAxKVxuICAgICAgICAgIH1cbiAgICAgICAgICByZXR1cm4gcmV0XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgc2NvcGUuYWxsX3BhZ2VzID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgcmV0ID0gW10sIGk7XG4gICAgICAgIGZvciAoaSA9IDA7IGkgPCBzY29wZS5wYWdlQ291bnQ7IGkrKylcbiAgICAgICAgICByZXQucHVzaChpICsgMSlcbiAgICAgICAgcmV0dXJuIHJldFxuICAgICAgfVxuXG4gICAgICBzY29wZS5zZXRfcGFnZV9udW1iZXIgPSBmdW5jdGlvbiAobikge1xuICAgICAgICBzY29wZS5jdXJyZW50ID0gblxuICAgICAgICBpZiAoc2NvcGUuJHBhcmVudC5zZXREYXRhU291cmNlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICBzY29wZS4kcGFyZW50LnNldERhdGFTb3VyY2Uoc2NvcGUuY3VycmVudClcbiAgICAgICAgICAkbG9jYXRpb24uc2VhcmNoKCdwYWdlX25vJywgbiArIDEpXG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgc2NvcGUubmV4dFBhZ2UgPSBmdW5jdGlvbiAoKSB7XG5cbiAgICAgICAgY29uc29sZS5sb2coJ3NzcycsIHNjb3BlLnBhZ2VyX2NvdW50KCkpXG4gICAgICAgIHZhciBuID0gc2NvcGUucGFnZXJQYWdlICsgMVxuXG4gICAgICAgIGlmIChuIDwgc2NvcGUucGFnZUNvdW50IC8gc2NvcGUucGFnZXJTaXplKVxuICAgICAgICAgIHNjb3BlLnBhZ2VyUGFnZSA9IG5cblxuICAgICAgICBjb25zb2xlLmxvZyhzY29wZS5jdXJyZW50LCBzY29wZS5wYWdlclBhZ2UpXG4gICAgICAgIGNvbnNvbGUubG9nKCduZXh0IHBhZ2UnKVxuXG4gICAgICAgIHRoaXMuc2V0X3BhZ2VfbnVtYmVyKHNjb3BlLnBhZ2VyUGFnZSAqIHNjb3BlLnBhZ2VyU2l6ZSlcbiAgICAgIH1cblxuICAgICAgc2NvcGUucHJldlBhZ2UgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmIChzY29wZS5wYWdlclBhZ2UgPiAwKVxuICAgICAgICAgIHNjb3BlLnBhZ2VyUGFnZSA9IHNjb3BlLnBhZ2VyUGFnZSAtIDFcbiAgICAgICAgY29uc29sZS5sb2coJ3ByZXYgcGFnZScpXG4gICAgICAgIHRoaXMuc2V0X3BhZ2VfbnVtYmVyKHNjb3BlLnBhZ2VyUGFnZSAqIHNjb3BlLnBhZ2VyU2l6ZSlcbiAgICAgIH1cblxuXG4gICAgICBzY29wZS5teWNsYXNzID0gZnVuY3Rpb24gKG4pIHtcbiAgICAgICAgaWYgKG4gPT09IHNjb3BlLmN1cnJlbnQpXG4gICAgICAgICAgcmV0dXJuICdhY3RpdmUnXG4gICAgICAgIGVsc2VcbiAgICAgICAgICByZXR1cm4gJydcbiAgICAgIH1cblxuICAgICAgc2NvcGUuc2hvdyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHNjb3BlLnBhZ2VDb3VudCA+IDE7XG4gICAgICB9XG5cbiAgICAgIHNjb3BlLnNob3dNb3JlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4gc2NvcGUuc2hvd01vcmVQYWdlcztcbiAgICAgIH1cblxuICAgICAgc2NvcGUuc2V0X3Nob3dfbW9yZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgc2NvcGUuc2hvd01vcmVQYWdlcyA9ICFzY29wZS5zaG93TW9yZVBhZ2VzXG4gICAgICB9XG4gICAgfVxuICB9XG59XSlcbiIsIm1vZHVsZS5leHBvcnRzID0gW1xuICByZXF1aXJlKCcuLi9jb21tb24vdGtzX3NlcnZpY2VzLmpzJyksXG4gIHJlcXVpcmUoJy4uL2NvbW1vbi90a3NfZmlsdGVycy5qcycpLFxuICByZXF1aXJlKCcuLi9jb21tb24vdGtzX2RpcmVjdGl2ZXMuanMnKSxcbiAgcmVxdWlyZSgnLi4vY29tbW9uL3Rrc19kaXJlY3RpdmVzMi5qcycpLFxuICByZXF1aXJlKCcuLi9jb21tb24vcHJvZmlsZV9kaXJlY3RpdmUuanMnKSxcbiAgcmVxdWlyZSgnLi9tb2RfY291bnRyaWVzLmpzJyksXG4gIHJlcXVpcmUoJy4vbW9kX2F1dGhvcml6ZS5qcycpLFxuICByZXF1aXJlKCcuL21vZF9wYWdlci5qcycpLFxuICByZXF1aXJlKCcuL21vZF9jYXJkcy5qcycpXG5dXG4iLCJcInVzZSBzdHJpY3RcIjtcbnZhciBjb3VudHJpZXMgPSByZXF1aXJlKCdjb3VudHJpZXMnKVxudmFyIGlucHV0X2Zvcm1hdHMgPSByZXF1aXJlKFwiLi4vLi4vLi4vZGF0YS9mb3JtYXRzLmpzb25cIilcbnZhciBvYXBpID0gcmVxdWlyZShcIi4uLy4uLy4uL2RhdGEvb2FwaS5qc29uXCIpXG52YXIgYXJpcG8gPSByZXF1aXJlKFwiLi4vLi4vLi4vZGF0YS9hcmlwby5qc29uXCIpXG5cbmZ1bmN0aW9uIGdldElFVmVyc2lvbihhZ2VudCl7XG4gIHZhciByZWcgPSAvTVNJRVxccz8oXFxkKykoPzpcXC4oXFxkKykpPy9pO1xuICB2YXIgbWF0Y2hlcyA9IGFnZW50Lm1hdGNoKHJlZyk7XG4gIGlmIChtYXRjaGVzICE9PSBudWxsKSB7XG4gICAgcmV0dXJuIHsgbWFqb3I6IG1hdGNoZXNbMV0sIG1pbm9yOiBtYXRjaGVzWzJdIH07XG4gIH1cbiAgcmV0dXJuIHsgbWFqb3I6IFwiLTFcIiwgbWlub3I6IFwiLTFcIiB9O1xufVxuXG5mdW5jdGlvbiBzdXBwb3J0ZWRDb3VudHJpZXNQcm9jICgpIHtcblxuICB2YXIgcmV0ID0ge31cbiAgdmFyIGNudCA9IDA7XG4gIGZvciAodmFyIGtleSBpbiBpbnB1dF9mb3JtYXRzKSB7XG4gICAgaWYgKGlucHV0X2Zvcm1hdHMuaGFzT3duUHJvcGVydHkoa2V5KSkge1xuICBcbiAgICAgIGlmICAoY291bnRyaWVzLmdldF9jb3VudHJ5KGtleSkgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICByZXRba2V5XSA9IHtuYW1lOiBjb3VudHJpZXMuZ2V0X2NvdW50cnkoa2V5KS5uYW1lLCBwcmljZTogY250fVxuICAgICAgICBjbnQgPSBjbnQgKyAxMFxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgY29uc29sZS5sb2coJ2NhbiBub3QgZmluZCBpbiBjb3VudHJ5ICcsIGtleSlcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBmb3IgKCBrZXkgaW4gb2FwaSkge1xuICAgIGlmIChvYXBpLmhhc093blByb3BlcnR5KGtleSkpIHtcbiAgICAgIGlmICAoY291bnRyaWVzLmdldF9jb3VudHJ5KGtleSkgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICByZXRba2V5XSA9IHtuYW1lOiBjb3VudHJpZXMuZ2V0X2NvdW50cnkoa2V5KS5uYW1lLCBwcmljZTogMTAwLCBsaW5rOidPQVBJJ31cbiAgICAgIH1cbiAgICB9XG4gIH1cblxuXG4gIC8vIGNvdW50cmllcyBpbiBhcmlwbyBoYXMgb3B0aW9uIHRvIGVpdGhlciByZWdpc3RlciBhcyBzcGVjaWZpYyBjb3VudHJ5IG9yIHVuZGVyIEFSSVAgdHJlYXR5XG4gIGZvciAoIGtleSBpbiBhcmlwbykge1xuICAgIGlmIChhcmlwby5oYXNPd25Qcm9wZXJ0eShrZXkpKSB7XG4gICAgICBpZiAgKGNvdW50cmllcy5nZXRfY291bnRyeShrZXkpICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgcmV0W2tleV0gPSB7bmFtZTogY291bnRyaWVzLmdldF9jb3VudHJ5KGtleSkubmFtZSwgcHJpY2U6IDEwMCwgb3RoZXJzOidBUklQTyd9XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHJldFxuXG59XG5cbi8vIGZyYW1ld29yayBjYW4gYmUgOiAnYW5ndWxhcicsICdib290c3RyYXAzJ1xuXG5leHBvcnRzLnN1cHBvcnRlZEJyb3dzZXIgPSBmdW5jdGlvbiAodXNlckFnZW50LCBmcmFtZXdvcmspIHtcblxuICB2YXIgdXNlcl9hZ2VudCA9IHVzZXJBZ2VudC50b0xvd2VyQ2FzZSgpXG4gIC8vIENocm9tZSBzdXBwb3J0ZWQgaW4gYWxsIHBsYXRmb3Jtc1xuXG4vLyAgY29uc29sZS5sb2coJ3VzZXIgYWdlbnQnLCB1c2VyX2FnZW50LCAhIXVzZXJfYWdlbnQubWF0Y2goL3RyaWRlbnQuKnJ2WyA6XSoxMVxcLi8pKVxuXG4gIGlmICgvY2hyb21lLy50ZXN0KHVzZXJfYWdlbnQpKVxuICAgIHJldHVybiB0cnVlO1xuICBlbHNlIGlmICgvZmlyZWZveC8udGVzdCh1c2VyX2FnZW50KSlcbiAgICByZXR1cm4gdHJ1ZVxuICBlbHNlIGlmICgvc2FmYXJpLy50ZXN0KHVzZXJfYWdlbnQpKVxuICAgIHJldHVybiB0cnVlXG4gIGVsc2UgaWYgKC9tb3ppbGxhLy50ZXN0KHVzZXJfYWdlbnQpKVxuICAgIHJldHVybiB0cnVlXG4gIGVsc2UgaWYgKC9tc2llIDEwLy50ZXN0KHVzZXJfYWdlbnQpKVxuICAgIHJldHVybiB0cnVlXG4gIGVsc2UgaWYgKCEhdXNlcl9hZ2VudC5tYXRjaCgvdHJpZGVudC4qcnZbIDpdKjExXFwuLykpXG4gICAgcmV0dXJuIHRydWVcbiAgZWxzZVxuICAgIHJldHVybiBmYWxzZVxuXG59XG5cbmV4cG9ydHMuc3VwcG9ydGVkQ291bnRyaWVzID0gZnVuY3Rpb24gKCkge1xuXG4gIHJldHVybiBzdXBwb3J0ZWRDb3VudHJpZXNQcm9jKClcblxufVxuXG5leHBvcnRzLmdldF9jb3VudHJpZXNfYnlfY29udG5lbnQgPSBmdW5jdGlvbihjb2RlKSB7XG5cbiAgdmFyIGNsc3QgPSBjb3VudHJpZXMuZ2V0X2NvdW50cmllc19ieV9jb250bmVudChjb2RlKVxuICB2YXIgc2xzdCA9IHN1cHBvcnRlZENvdW50cmllc1Byb2MoKVxuXG4gIHJldHVybiBjbHN0LmZpbHRlcihmdW5jdGlvbihlKSB7XG4gICAgcmV0dXJuIHNsc3RbZS5jb2RlXSAhPT0gdW5kZWZpbmVkXG4gIH0pXG5cblxufVxuXG5cbiIsIlwidXNlIHN0cmljdFwiO1xuXG5leHBvcnRzLmNhcnQgPSBmdW5jdGlvbiAoJHNjb3BlLCAkaHR0cCwgJHJvb3RTY29wZSwgJGxvY2F0aW9uLCBDYXJ0LCBBVVRIX0VWRU5UUywgJG1vZGFsKSB7XG5cbiAgLy8gICRzY29wZS5jYXJ0ID0gQ2FydC5nZXRDYXJ0KClcbi8vICAgICRzY29wZS5uYW1lID0gU2Vzc2lvbi5nZXROYW1lKClcblxuICAvKlxuICAkc2NvcGUuYWxlcnQgPSBmdW5jdGlvbiAobSkge1xuICAgIGFsZXJ0KG0pXG4gIH1cbiovXG5cbiAgJHNjb3BlLmlzQ2FydEVtcHR5ID0gZnVuY3Rpb24gKGNhcnQpIHtcbiAgICBpZiAoIWNhcnQpXG4gICAgICByZXR1cm4gdHJ1ZVxuICAgIGVsc2Uge1xuICAgICAgcmV0dXJuIGNhcnQuaXRlbXMubGVuZ3RoID09PSAwXG4gICAgfVxuICB9XG5cbiAgJHNjb3BlLmdldF9yZW1vdGVfY2FydCA9IGZ1bmN0aW9uICgpIHtcblxuICAgIENhcnQuZ2V0UmVtb3RlQ2FydCgpLnRoZW4oZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICRzY29wZS5jYXJ0ID0gZGF0YS5jYXJ0XG4gICAgICAkcm9vdFNjb3BlLiRicm9hZGNhc3QoQVVUSF9FVkVOVFMuY2FydFVwZGF0ZWQpO1xuICAgIH0pXG5cbiAgfVxuXG4gICRzY29wZS5nZXRfcmVtb3RlX2NhcnQoKVxuXG4gICRzY29wZS5kZWxldGVfaXRlbSA9IGZ1bmN0aW9uIChjYXJ0X2lkLCByZWMpIHtcblxuICAgIGlmIChjb25maXJtKCdEbyB5b3UgcmVhbGx5IHdhbnQgdG8gZGVsZXRlIHRoaXMgaXRlbT8nKSkge1xuXG4gICAgICBDYXJ0LmRlbGV0ZV9pdGVtKGNhcnRfaWQsIHJlYykudGhlbihmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAkc2NvcGUuZ2V0X3JlbW90ZV9jYXJ0KClcbiAgICAgIH0pXG4gICAgfVxuICB9XG5cblxuICAkc2NvcGUudXBsb2FkX2xvY2FsX2NhcnQgPSBmdW5jdGlvbiAoY2FydF9pZCkge1xuICAgIGNvbnNvbGUubG9nKCdjYXJ0X2lkJywgY2FydF9pZClcblxuICAgIGlmIChjb25maXJtKCdEbyB5b3UgcmVhbGx5IHdhbnQgdG8gdXBsb2FkIHRoaXMgbG9jYWwgY2FydD8nKSkge1xuXG4gICAgICBDYXJ0LnN0b3JlX3RvX3NlcnZlcigpLnRoZW4oZnVuY3Rpb24gKHN0YXR1cykge1xuICAgICAgICBjb25zb2xlLmxvZygnc3RvcmUgdG8gc2VydmVyIHN0YXR1czInLCBzdGF0dXMpXG4gICAgICAgIC8qXG4gICAgICAgIENhcnQuY2xlYW5fbG9jYWxfc3RvcmUoKVxuXG4gICAgICAgIHJlZ2lzdGVyX3RyYWRlbWFyay5pbml0X2l0ZW0oKVxuICAgICAgICAkcm9vdFNjb3BlLiRicm9hZGNhc3QoQVVUSF9FVkVOVFMuY2FydFVwZGF0ZWQpO1xuXG4gICAgICAgICRyb290U2NvcGUuJGJyb2FkY2FzdChBVVRIX0VWRU5UUy5sb2dpblN1Y2Nlc3MpO1xuICAgICAgICBDYXJ0LmdldFJlbW90ZUNhcnRDb3VudCgpLnRoZW4oZnVuY3Rpb24gKGNvdW50KSB7XG4gICAgICAgICAgaWYgKGNvdW50ID4gMClcbiAgICAgICAgICAgICRsb2NhdGlvbi5wYXRoKCcvc2hvcHBpbmdfY2FydCcpXG4gICAgICAgICAgZWxzZVxuICAgICAgICAgICAgJGxvY2F0aW9uLnBhdGgoJy9kYXNoYm9hcmQnKVxuICAgICAgICB9KVxuKi9cbiAgICAgIH0pXG4gICAgICAvKlxuICAgICAgQ2FydC5kZWxldGVfaXRlbShjYXJ0X2lkLCByZWMpLnRoZW4oZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgJHNjb3BlLmdldF9yZW1vdGVfY2FydCgpXG4gICAgICB9KSovXG4gICAgfVxuICB9XG5cbiAgJHNjb3BlLnNlbGVjdGlvbl9jaGFuZ2UgPSBmdW5jdGlvbiAoY2FydF9pZCwgcmVjKSB7XG4gICAgQ2FydC5zZWxlY3Rpb25fY2hhbmdlKGNhcnRfaWQsIHJlYykudGhlbihmdW5jdGlvbiAoc3RhdHVzKSB7XG4gICAgICAkc2NvcGUuZ2V0X3JlbW90ZV9jYXJ0KClcbiAgICB9KVxuICB9XG5cbiAgJHNjb3BlLnNob3dfZGV0YWlscyA9IGZ1bmN0aW9uIChyZWMpIHtcbiAgICBjb25zb2xlLmxvZygnc2hvd19kZXRhaWxzJywgcmVjLCAkbW9kYWwpXG4gICAgY29uc29sZS5sb2coJ3Nob3dfJG1vZGFsJywgJG1vZGFsKVxuICAgIHZhciBtb2RhbEluc3RhbmNlID0gJG1vZGFsLm9wZW4oe1xuICAgICAgdGVtcGxhdGVVcmw6ICdwYXJ0aWFscy9zaG9wcGluZ19jYXJ0X2RldGFpbHMuaHRtbCcsXG4gICAgICB3aW5kb3dDbGFzczogJ2FwcC1tb2RhbC13aW5kb3cnLFxuICAgICAgY29udHJvbGxlcjogJ01vZGFsSW5zdGFuY2VDdHJsMicsXG4gICAgICByZXNvbHZlOiB7XG4gICAgICAgIGRldGFpbHM6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICByZXR1cm4gcmVjXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIC8vIGhhcHBlbnNcbiAgJHNjb3BlLmxvY2FsX3N0b3JlX3RvX3NlcnZlciA9IGZ1bmN0aW9uICgpIHtcbiAgICBjb25zb2xlLmxvZygnbG9jYWwgc3RvcmUgdG8gc2VydmVyJylcbiAgICBDYXJ0LnN0b3JlX3RvX3NlcnZlcigpLnRoZW4oZnVuY3Rpb24gKHN0YXR1cykge1xuICAgICAgY29uc29sZS5sb2coJ3N0b3JlIHRvIHNlcnZlciBzdGF0dXMnLCBzdGF0dXMpXG4gICAgICBDYXJ0LmNsZWFuX2xvY2FsX3N0b3JlKClcbiAgICB9KVxuICB9XG5cbn0iLCJtb2R1bGUuZXhwb3J0cz1tb2R1bGUuZXhwb3J0cz1tb2R1bGUuZXhwb3J0cz17XG4gIFwiQldcIjpcIkJvdHN3YW5hXCIsXG4gIFwiTFNcIjpcIkxlc290aG9cIixcbiAgXCJMUlwiOlwiTGliZXJpYVwiLFxuICBcIk1XXCI6XCJNYWxhd2lcIixcbiAgXCJOQVwiOlwiTmFtaWJpYVwiLFxuICBcIlNaXCI6XCJTd2F6aWxhbmRcIixcbiAgXCJUWlwiOlwiVGFuemFuaWFcIixcbiAgXCJVR1wiOlwiVWdhbmRhXCIsXG4gIFwiWldcIjpcIlppbWJhYndlXCJcbn0iLCJtb2R1bGUuZXhwb3J0cz1tb2R1bGUuZXhwb3J0cz1tb2R1bGUuZXhwb3J0cz17XG4gIFwiVVNcIjoge1wiaW5wdXRfdHlwZVwiOiAwLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwid1wiLCBcIndvcmRfbGFiZWxcIjogXCJXb3JkIG1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IDAsICBcInN0dWR5X3R5cGVcIjogLTEsIFwic3R1ZHlfdHlwZTJcIjogXCIwXCJ9LFxuICBcIklOXCI6IHtcImlucHV0X3R5cGVcIjogMCwgXCJQT0FfcmVxdWlyZWRcIjotMSwgXCJ0cmFkZV90eXBlXCI6IFwid1wiLCBcIndvcmRfbGFiZWxcIjogXCJXb3JkIG1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IDAsIFwic3R1ZHlfdHlwZVwiOiAtMX0sXG4gIFwiU0dcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTF9LFxuICBcIkdSXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xfSxcbiAgXCJMVFwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMX0sXG4gIFwiSEtcIjoge1wiaW5wdXRfdHlwZVwiOiAwLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwid1wiLCBcIndvcmRfbGFiZWxcIjogXCJXb3JkIG1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTF9LFxuICBcIkpQXCI6IHtcImlucHV0X3R5cGVcIjogMCwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcIndcIiwgXCJ3b3JkX2xhYmVsXCI6IFwiV29yZCBtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAwLCBcInN0dWR5X3R5cGVcIjogLTF9LFxuICBcIlRXXCI6IHtcImlucHV0X3R5cGVcIjogMCwgXCJQT0FfcmVxdWlyZWRcIjogMSwgXCJ0cmFkZV90eXBlXCI6IFwid1wiLCBcIndvcmRfbGFiZWxcIjogXCJXb3JkIG1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IDAsIFwic3R1ZHlfdHlwZVwiOiAtMX0sXG4gIFwiQVJcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAxLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTF9LFxuICBcIkFaXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xfSxcbiAgXCJBVVwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMX0sXG4gIFwiTlpcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xfSxcbiAgXCJHQlwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTF9LFxuICBcIkRFXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMX0sXG4gIFwiSUVcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xfSxcbiAgXCJBU1wiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTF9LFxuICBcIkZKXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMX0sXG4gIFwiUEdcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xfSxcbiAgXCJXU1wiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTF9LFxuICBcIlRPXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMX0sXG4gIFwiQkhcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xfSxcbiAgXCJCWVwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IDIsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMX0sXG4gIFwiQlpcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAyLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTF9LFxuICBcIkJPXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xfSxcbiAgXCJCRVwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IDEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMX0sXG4gIFwiQ0FcIjoge1wiaW5wdXRfdHlwZVwiOiAwLCBcIlBPQV9yZXF1aXJlZFwiOiAxLCBcInRyYWRlX3R5cGVcIjogXCJ3XCIsIFwid29yZF9sYWJlbFwiOiBcIldvcmQgbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogMCwgXCJzdHVkeV90eXBlXCI6IC0xfSxcbiAgXCJDT1wiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IDEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMX0sXG4gIFwiRVVcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xfSxcbiAgXCJDSFwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTF9LFxuICBcIlNaXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xfSxcbiAgXCJOT1wiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IDEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMX0sXG4gIFwiTVhcIjoge1wiaW5wdXRfdHlwZVwiOiAwLCBcIlBPQV9yZXF1aXJlZFwiOiAxLCBcInRyYWRlX3R5cGVcIjogXCJ3XCIsIFwid29yZF9sYWJlbFwiOiBcIldvcmttYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xfSxcbiAgXCJCUlwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IDEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMX0sXG4gIFwiQ05cIjoge1wiaW5wdXRfdHlwZVwiOiAwLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwid1wiLCBcIndvcmRfbGFiZWxcIjogXCJXb3JkIG1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTF9LFxuICBcIlJVXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xfSxcbiAgXCJLUlwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMX0sXG4gIFwiVEhcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAxLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTF9LFxuICBcIlBIXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xfSxcbiAgXCJWTlwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IDEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMX0sXG4gIFwiQUZcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkFNXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJCRFwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiQlRcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkJOXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJLSFwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiVExcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkdFXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJJRFwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiSVFcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIklMXCI6IHtcImlucHV0X3R5cGVcIjogMCwgXCJQT0FfcmVxdWlyZWRcIjogMSwgXCJ0cmFkZV90eXBlXCI6IFwid1wiLCBcIndvcmRfbGFiZWxcIjogXCJXb3JkIG1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IDAsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ3XCJ9LFxuICBcIkpPXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJLV1wiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiS0dcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkxBXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJMQlwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiTU9cIjoge1wiaW5wdXRfdHlwZVwiOiAwLCBcIlBPQV9yZXF1aXJlZFwiOiAxLCBcInRyYWRlX3R5cGVcIjogXCJ3XCIsIFwid29yZF9sYWJlbFwiOiBcIldvcmQgbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogMCwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcIndcIn0sXG4gIFwiTVlcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIk1WXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJNTlwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiTU1cIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIk5QXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJPTVwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiUEtcIjoge1wiaW5wdXRfdHlwZVwiOiAwLCBcIlBPQV9yZXF1aXJlZFwiOiAxLCBcInRyYWRlX3R5cGVcIjogXCJ3XCIsIFwid29yZF9sYWJlbFwiOiBcIldvcmQgbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogMCwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcIndcIn0sXG4gIFwiUFNcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIlFBXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJTQVwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiTEtcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIlRKXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJUUlwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiVE1cIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkFFXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJVWlwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiWUVcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIlRHXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJBTFwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiQURcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkFUXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJCQVwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiQkdcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkhSXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJDWVwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiQ1pcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkRLXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJFRVwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiRklcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkZSXCI6IHtcImlucHV0X3R5cGVcIjogMCwgXCJQT0FfcmVxdWlyZWRcIjogMSwgXCJ0cmFkZV90eXBlXCI6IFwid1wiLCBcIndvcmRfbGFiZWxcIjogXCJXb3JkIG1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IDAsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ3XCJ9LFxuICBcIkdJXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJHR1wiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiSFVcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIklTXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJJVFwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiTFZcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkxJXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJNS1wiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiTVRcIjoge1wiaW5wdXRfdHlwZVwiOiAwLCBcIlBPQV9yZXF1aXJlZFwiOiAxLCBcInRyYWRlX3R5cGVcIjogXCJ3XCIsIFwid29yZF9sYWJlbFwiOiBcIldvcmQgbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogMCwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcIndcIn0sXG4gIFwiTURcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIk1DXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJQTFwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiUFRcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIlJPXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJTTVwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiU0tcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIlNJXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJFU1wiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiU0VcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIlVBXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJBSVwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiQUdcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkFXXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJCQlwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiQk1cIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIlZHXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJLWVwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiQ0xcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkNSXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJDV1wiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiRE1cIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkRPXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJFQ1wiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiU1ZcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkdEXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJHVFwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiR1lcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkhUXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJITlwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiSk1cIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIk1TXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJOSVwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiUEFcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIlBZXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJQRVwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiUFJcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIlZDXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJLTlwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiTENcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIlNSXCI6IHtcImlucHV0X3R5cGVcIjogMCwgXCJQT0FfcmVxdWlyZWRcIjogMSwgXCJ0cmFkZV90eXBlXCI6IFwid1wiLCBcIndvcmRfbGFiZWxcIjogXCJXb3JkIG1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IDAsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ3XCJ9LFxuICBcIlRUXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJVWVwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiVkVcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkRaXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJBT1wiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiQklcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkNWXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJDR1wiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiREpcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkVHXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJFUlwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiRVRcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkdNXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJHSFwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiS0VcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkxSXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJMWVwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiTUdcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIk1XXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJNVVwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiTUFcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIk1aXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJOQVwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiUldcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIlNUXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJTQ1wiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiU0xcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIlpBXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJUWlwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiVE5cIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIlVHXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJaTVwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiWldcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIlJTXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJNRVwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiQlNcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIktPXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJUQ1wiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiU0JcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LCAgXG4gIFwiTFNcIjoge1wiaW5wdXRfdHlwZVwiOiAwLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwid1wiLCBcIndvcmRfbGFiZWxcIjogXCJXb3JkIG1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCAgXCJzdHVkeV90eXBlXCI6IC0xLCBcInN0dWR5X3R5cGUyXCI6IFwiMFwifSxcbiAgXCJKRVwiOiB7XCJpbnB1dF90eXBlXCI6IDAsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ3XCIsIFwid29yZF9sYWJlbFwiOiBcIldvcmQgbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsICBcInN0dWR5X3R5cGVcIjogLTEsIFwic3R1ZHlfdHlwZTJcIjogXCIwXCJ9LFxuICBcIkJYXCI6IHtcImlucHV0X3R5cGVcIjogMCwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcIndcIiwgXCJ3b3JkX2xhYmVsXCI6IFwiV29yZCBtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJzdHVkeV90eXBlMlwiOiBcIjBcIn0sXG4gIFwiU1hcIjoge1wiaW5wdXRfdHlwZVwiOiAwLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwid1wiLCBcIndvcmRfbGFiZWxcIjogXCJXb3JkIG1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCAgXCJzdHVkeV90eXBlXCI6IC0xLCBcInN0dWR5X3R5cGUyXCI6IFwiMFwifSxcbiAgXCJORVwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiQ0lcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIk5MXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMX0sXG4gIFwiTFVcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xfSxcbiAgXCJPQVBJXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJLUkRcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkVBWlwiOiB7XCJpbnB1dF90eXBlXCI6IDEsIFwiUE9BX3JlcXVpcmVkXCI6IC0xLCBcInRyYWRlX3R5cGVcIjogXCJ0bVwiLCBcIndvcmRfbGFiZWxcIjogXCJUcmFkZW1hcmtcIiwgIFwiY29tbWVyY2VfdXNlXCI6IC0xLCBcInN0dWR5X3R5cGVcIjogLTEsIFwiZGVmYXVsdFwiOlwidG1cIn0sXG4gIFwiVkNUXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifSxcbiAgXCJNQUZcIjoge1wiaW5wdXRfdHlwZVwiOiAxLCBcIlBPQV9yZXF1aXJlZFwiOiAtMSwgXCJ0cmFkZV90eXBlXCI6IFwidG1cIiwgXCJ3b3JkX2xhYmVsXCI6IFwiVHJhZGVtYXJrXCIsICBcImNvbW1lcmNlX3VzZVwiOiAtMSwgXCJzdHVkeV90eXBlXCI6IC0xLCBcImRlZmF1bHRcIjpcInRtXCJ9LFxuICBcIkFSSVBPXCI6IHtcImlucHV0X3R5cGVcIjogMSwgXCJQT0FfcmVxdWlyZWRcIjogLTEsIFwidHJhZGVfdHlwZVwiOiBcInRtXCIsIFwid29yZF9sYWJlbFwiOiBcIlRyYWRlbWFya1wiLCAgXCJjb21tZXJjZV91c2VcIjogLTEsIFwic3R1ZHlfdHlwZVwiOiAtMSwgXCJkZWZhdWx0XCI6XCJ0bVwifVxufSIsIm1vZHVsZS5leHBvcnRzPW1vZHVsZS5leHBvcnRzPW1vZHVsZS5leHBvcnRzPXtcbiAgXCJCSlwiOlwiQmVuaW5cIixcbiAgXCJCRlwiOlwiQnVya2luYSBGYXNvXCIsXG4gIFwiQ01cIjpcIkNhbWVyb29uXCIsXG4gIFwiQ0ZcIjpcIkNlbnRyYWwgQWZyaWNhblwiLFxuICBcIlREXCI6XCJDaGFkXCIsXG4gIFwiS01cIjpcIkNvbW9yb3NcIixcbiAgXCJDRFwiOlwiQ29uZ28gUmVwdWJsaWNcIixcbiAgXCJHUVwiOlwiRXF1YXRvcmlhbCBHdWluZWFcIixcbiAgXCJHQVwiOlwiR2Fib25cIixcbiAgXCJHTlwiOlwiR3VpbmVhXCIsXG4gIFwiR1dcIjpcIkd1aW5lYS1CaXNzYXVcIixcbiAgXCJNTFwiOlwiTWFsaVwiLFxuICBcIk1SXCI6XCJNYXVyaXRhbmlhXCIsXG4gIFwiTkdcIjpcIk5pZ2VyaWFcIixcbiAgXCJTTlwiOlwiU2VuZWdhbFwiLFxuICBcIlRPXCI6XCJUb25nYVwiXG59IiwibW9kdWxlLmV4cG9ydHM9bW9kdWxlLmV4cG9ydHM9bW9kdWxlLmV4cG9ydHM9WyBcbiAge1wibmFtZVwiOlwiQWZnaGFuaXN0YW5cIixcImNvZGVcIjpcIkFGXCIsXCJjb250aW5lbnRcIjpcIk1FXCJ9LFxuICB7XCJuYW1lXCI6XCLDhWxhbmQgSXNsYW5kc1wiLFwiY29kZVwiOlwiQVhcIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIkFsYmFuaWFcIixcImNvZGVcIjpcIkFMXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJBbGdlcmlhXCIsXCJjb2RlXCI6XCJEWlwiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiQW1lcmljYW4gU2Ftb2FcIixcImNvZGVcIjpcIkFTXCIsXCJjb250aW5lbnRcIjpcIk9DXCJ9LFxuICB7XCJuYW1lXCI6XCJBbmRvcnJhXCIsXCJjb2RlXCI6XCJBRFwiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiQW5nb2xhXCIsXCJjb2RlXCI6XCJBT1wiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiQW5ndWlsbGFcIixcImNvZGVcIjpcIkFJXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJBbnRhcmN0aWNhXCIsXCJjb2RlXCI6XCJBUVwiLFwiY29udGluZW50XCI6XCJBTlwifSxcbiAge1wibmFtZVwiOlwiQW50aWd1YSBhbmQgQmFyYnVkYVwiLFwiY29kZVwiOlwiQUdcIixcImNvbnRpbmVudFwiOlwiQ0FcIn0sXG4gIHtcIm5hbWVcIjpcIkFyZ2VudGluYVwiLFwiY29kZVwiOlwiQVJcIixcImNvbnRpbmVudFwiOlwiU0FcIn0sXG4gIHtcIm5hbWVcIjpcIkFybWVuaWFcIixcImNvZGVcIjpcIkFNXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJBcnViYVwiLFwiY29kZVwiOlwiQVdcIixcImNvbnRpbmVudFwiOlwiQ0FcIn0sXG4gIHtcIm5hbWVcIjpcIkF1c3RyYWxpYVwiLFwiY29kZVwiOlwiQVVcIixcImNvbnRpbmVudFwiOlwiT0NcIn0sXG4gIHtcIm5hbWVcIjpcIkF1c3RyaWFcIixcImNvZGVcIjpcIkFUXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJBemVyYmFpamFuXCIsXCJjb2RlXCI6XCJBWlwiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiQmFoYW1hc1wiLFwiY29kZVwiOlwiQlNcIixcImNvbnRpbmVudFwiOlwiQ0FcIn0sXG4gIHtcIm5hbWVcIjpcIkJhaHJhaW5cIixcImNvZGVcIjpcIkJIXCIsXCJjb250aW5lbnRcIjpcIk1FXCJ9LFxuICB7XCJuYW1lXCI6XCJCYW5nbGFkZXNoXCIsXCJjb2RlXCI6XCJCRFwiLFwiY29udGluZW50XCI6XCJBU1wifSxcbiAge1wibmFtZVwiOlwiQmFyYmFkb3NcIixcImNvZGVcIjpcIkJCXCIsXCJjb250aW5lbnRcIjpcIkNBXCJ9LFxuICB7XCJuYW1lXCI6XCJCZWxhcnVzXCIsXCJjb2RlXCI6XCJCWVwiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiQmVsZ2l1bVwiLFwiY29kZVwiOlwiQkVcIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIkJlbmVsdXhcIixcImNvZGVcIjpcIkJYXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJCZWxpemVcIixcImNvZGVcIjpcIkJaXCIsXCJjb250aW5lbnRcIjpcIkNBXCJ9LFxuICB7XCJuYW1lXCI6XCJCZW5pblwiLFwiY29kZVwiOlwiQkpcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIkJlcm11ZGFcIixcImNvZGVcIjpcIkJNXCIsXCJjb250aW5lbnRcIjpcIkFTXCJ9LFxuICB7XCJuYW1lXCI6XCJCaHV0YW5cIixcImNvZGVcIjpcIkJUXCIsXCJjb250aW5lbnRcIjpcIkFTXCJ9LFxuICB7XCJuYW1lXCI6XCJCb2xpdmlhXCIsXCJjb2RlXCI6XCJCT1wiLFwiY29udGluZW50XCI6XCJTQVwifSxcbiAge1wibmFtZVwiOlwiQm9zbmlhXCIsXCJjb2RlXCI6XCJCQVwiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiQm90c3dhbmFcIixcImNvZGVcIjpcIkJXXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJCb3V2ZXQgSXNsYW5kXCIsXCJjb2RlXCI6XCJCVlwiLFwiY29udGluZW50XCI6XCJBTlwifSxcbiAge1wibmFtZVwiOlwiQnJhemlsXCIsXCJjb2RlXCI6XCJCUlwiLFwiY29udGluZW50XCI6XCJTQVwifSxcbiAge1wibmFtZVwiOlwiQnJpdGlzaCBJbmRpYW5cIixcImNvZGVcIjpcIklPXCIsXCJjb250aW5lbnRcIjpcIkFTXCJ9LFxuICB7XCJuYW1lXCI6XCJCcnVuZWkgRGFydXNzYWxhbVwiLFwiY29kZVwiOlwiQk5cIixcImNvbnRpbmVudFwiOlwiQVNcIn0sXG4gIHtcIm5hbWVcIjpcIkJ1bGdhcmlhXCIsXCJjb2RlXCI6XCJCR1wiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiQnVya2luYSBGYXNvXCIsXCJjb2RlXCI6XCJCRlwiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiQnVydW5kaVwiLFwiY29kZVwiOlwiQklcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIkNhbWJvZGlhXCIsXCJjb2RlXCI6XCJLSFwiLFwiY29udGluZW50XCI6XCJBU1wifSxcbiAge1wibmFtZVwiOlwiQ2FtZXJvb25cIixcImNvZGVcIjpcIkNNXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJDYW5hZGFcIixcImNvZGVcIjpcIkNBXCIsXCJjb250aW5lbnRcIjpcIk5BXCJ9LFxuICB7XCJuYW1lXCI6XCJDYXBlIFZlcmRlXCIsXCJjb2RlXCI6XCJDVlwiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiQ2F5bWFuIElzbGFuZHNcIixcImNvZGVcIjpcIktZXCIsXCJjb250aW5lbnRcIjpcIkNBXCJ9LFxuICB7XCJuYW1lXCI6XCJDZW50cmFsIEFmcmljYW4gUmVwdWJsaWNcIixcImNvZGVcIjpcIkNGXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJDaGFkXCIsXCJjb2RlXCI6XCJURFwiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiQ2hpbGVcIixcImNvZGVcIjpcIkNMXCIsXCJjb250aW5lbnRcIjpcIlNBXCJ9LFxuICB7XCJuYW1lXCI6XCJDaGluYVwiLFwiY29kZVwiOlwiQ05cIixcImNvbnRpbmVudFwiOlwiQVNcIn0sXG4gIHtcIm5hbWVcIjpcIkNocmlzdG1hcyBJc2xhbmRcIixcImNvZGVcIjpcIkNYXCIsXCJjb250aW5lbnRcIjpcIkFTXCJ9LFxuICB7XCJuYW1lXCI6XCJDb2NvcyAoS2VlbGluZylcIixcImNvZGVcIjpcIkNDXCIsXCJjb250aW5lbnRcIjpcIkFTXCJ9LFxuICB7XCJuYW1lXCI6XCJDb2xvbWJpYVwiLFwiY29kZVwiOlwiQ09cIixcImNvbnRpbmVudFwiOlwiU0FcIn0sXG4gIHtcIm5hbWVcIjpcIkNvbW9yb3NcIixcImNvZGVcIjpcIktNXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJSZXB1YmxpYyBvZiB0aGUgQ29uZ29cIixcImNvZGVcIjpcIkNHXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJEZW1vY3JhdGljIFJlcHVibGljIG9mIHRoZSBDb25nb1wiLFwiY29kZVwiOlwiQ0RcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIkNvb2sgSXNsYW5kc1wiLFwiY29kZVwiOlwiQ0tcIixcImNvbnRpbmVudFwiOlwiT0NcIn0sXG4gIHtcIm5hbWVcIjpcIkNvc3RhIFJpY2FcIixcImNvZGVcIjpcIkNSXCIsXCJjb250aW5lbnRcIjpcIkNBXCJ9LFxuICB7XCJuYW1lXCI6IFwiSXZvcnkgQ29hc3RcIiwgXCJjb2RlXCI6IFwiQ0lcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIkNyb2F0aWFcIixcImNvZGVcIjpcIkhSXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJDdWJhXCIsXCJjb2RlXCI6XCJDVVwiLFwiY29udGluZW50XCI6XCJDQVwifSxcbiAge1wibmFtZVwiOlwiQ3lwcnVzXCIsXCJjb2RlXCI6XCJDWVwiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiQ3plY2ggUmVwdWJsaWNcIixcImNvZGVcIjpcIkNaXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJEZW5tYXJrXCIsXCJjb2RlXCI6XCJES1wiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiRGppYm91dGlcIixcImNvZGVcIjpcIkRKXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJEb21pbmljYVwiLFwiY29kZVwiOlwiRE1cIixcImNvbnRpbmVudFwiOlwiQ0FcIn0sXG4gIHtcIm5hbWVcIjpcIkRvbWluaWNhbiBSZXB1YmxpY1wiLFwiY29kZVwiOlwiRE9cIixcImNvbnRpbmVudFwiOlwiQ0FcIn0sXG4gIHtcIm5hbWVcIjpcIkVjdWFkb3JcIixcImNvZGVcIjpcIkVDXCIsXCJjb250aW5lbnRcIjpcIlNBXCJ9LFxuICB7XCJuYW1lXCI6XCJFZ3lwdFwiLFwiY29kZVwiOlwiRUdcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIkVsIFNhbHZhZG9yXCIsXCJjb2RlXCI6XCJTVlwiLFwiY29udGluZW50XCI6XCJDQVwifSxcbiAge1wibmFtZVwiOlwiRXF1YXRvcmlhbCBHdWluZWFcIixcImNvZGVcIjpcIkdRXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJFcml0cmVhXCIsXCJjb2RlXCI6XCJFUlwiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiRXN0b25pYVwiLFwiY29kZVwiOlwiRUVcIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIkV0aGlvcGlhXCIsXCJjb2RlXCI6XCJFVFwiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiRXVyb3BlYW4gVW5pb25cIixcImNvZGVcIjpcIkVVXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJGYWxrbGFuZCBJc2xhbmRzXCIsXCJjb2RlXCI6XCJGS1wiLFwiY29udGluZW50XCI6XCJTQVwifSxcbiAge1wibmFtZVwiOlwiRmFyb2UgSXNsYW5kc1wiLFwiY29kZVwiOlwiRk9cIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIkZpamlcIixcImNvZGVcIjpcIkZKXCIsXCJjb250aW5lbnRcIjpcIk9DXCJ9LFxuICB7XCJuYW1lXCI6XCJGaW5sYW5kXCIsXCJjb2RlXCI6XCJGSVwiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiRnJhbmNlXCIsXCJjb2RlXCI6XCJGUlwiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiRnJlbmNoIEd1aWFuYVwiLFwiY29kZVwiOlwiR0ZcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIkZyZW5jaCBQb2x5bmVzaWFcIixcImNvZGVcIjpcIlBGXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJGcmVuY2ggU291dGhlcm5cIixcImNvZGVcIjpcIlRGXCIsXCJjb250aW5lbnRcIjpcIkFOXCJ9LFxuICB7XCJuYW1lXCI6XCJHYWJvblwiLFwiY29kZVwiOlwiR0FcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIkdhbWJpYVwiLFwiY29kZVwiOlwiR01cIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIkdlb3JnaWFcIixcImNvZGVcIjpcIkdFXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJHZXJtYW55XCIsXCJjb2RlXCI6XCJERVwiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiR2hhbmFcIixcImNvZGVcIjpcIkdIXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJHaWJyYWx0YXJcIixcImNvZGVcIjpcIkdJXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJHcmVlY2VcIixcImNvZGVcIjpcIkdSXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJHcmVlbmxhbmRcIixcImNvZGVcIjpcIkdMXCIsXCJjb250aW5lbnRcIjpcIkNBXCJ9LFxuICB7XCJuYW1lXCI6XCJHcmVuYWRhXCIsXCJjb2RlXCI6XCJHRFwiLFwiY29udGluZW50XCI6XCJDQVwifSxcbiAge1wibmFtZVwiOlwiR3VhZGVsb3VwZVwiLFwiY29kZVwiOlwiR1BcIixcImNvbnRpbmVudFwiOlwiQ0FcIn0sXG4gIHtcIm5hbWVcIjpcIkd1YW1cIixcImNvZGVcIjpcIkdVXCIsXCJjb250aW5lbnRcIjpcIk9DXCJ9LFxuICB7XCJuYW1lXCI6XCJHdWF0ZW1hbGFcIixcImNvZGVcIjpcIkdUXCIsXCJjb250aW5lbnRcIjpcIkNBXCJ9LFxuICB7XCJuYW1lXCI6XCJHdWVybnNleVwiLFwiY29kZVwiOlwiR0dcIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIkd1aW5lYVwiLFwiY29kZVwiOlwiR05cIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIkd1aW5lYS1CaXNzYXVcIixcImNvZGVcIjpcIkdXXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJHdXlhbmFcIixcImNvZGVcIjpcIkdZXCIsXCJjb250aW5lbnRcIjpcIlNBXCJ9LFxuICB7XCJuYW1lXCI6XCJIYWl0aVwiLFwiY29kZVwiOlwiSFRcIixcImNvbnRpbmVudFwiOlwiQ0FcIn0sXG4gIHtcIm5hbWVcIjpcIkhlYXJkIGFuZCBNY2RvbmFsZFwiLFwiY29kZVwiOlwiSE1cIixcImNvbnRpbmVudFwiOlwiQU5cIn0sXG4gIHtcIm5hbWVcIjpcIkhvbHkgU2VlXCIsXCJjb2RlXCI6XCJWQVwiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiSG9uZHVyYXNcIixcImNvZGVcIjpcIkhOXCIsXCJjb250aW5lbnRcIjpcIkNBXCJ9LFxuICB7XCJuYW1lXCI6XCJIb25nIEtvbmdcIixcImNvZGVcIjpcIkhLXCIsXCJjb250aW5lbnRcIjpcIkFTXCJ9LFxuICB7XCJuYW1lXCI6XCJIdW5nYXJ5XCIsXCJjb2RlXCI6XCJIVVwiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiSWNlbGFuZFwiLFwiY29kZVwiOlwiSVNcIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIkluZGlhXCIsXCJjb2RlXCI6XCJJTlwiLFwiY29udGluZW50XCI6XCJBU1wifSxcbiAge1wibmFtZVwiOlwiSW5kb25lc2lhXCIsXCJjb2RlXCI6XCJJRFwiLFwiY29udGluZW50XCI6XCJBU1wifSxcbiAge1wibmFtZVwiOlwiSXJhblwiLFwiY29kZVwiOlwiSVJcIixcImNvbnRpbmVudFwiOlwiQVNcIn0sXG4gIHtcIm5hbWVcIjpcIklyYXFcIixcImNvZGVcIjpcIklRXCIsXCJjb250aW5lbnRcIjpcIk1FXCJ9LFxuICB7XCJuYW1lXCI6XCJJcmVsYW5kXCIsXCJjb2RlXCI6XCJJRVwiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiSXNsZSBvZiBNYW5cIixcImNvZGVcIjpcIklNXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJJc3JhZWxcIixcImNvZGVcIjpcIklMXCIsXCJjb250aW5lbnRcIjpcIk1FXCJ9LFxuICB7XCJuYW1lXCI6XCJJdGFseVwiLFwiY29kZVwiOlwiSVRcIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIkphbWFpY2FcIixcImNvZGVcIjpcIkpNXCIsXCJjb250aW5lbnRcIjpcIkNBXCJ9LFxuICB7XCJuYW1lXCI6XCJKYXBhblwiLFwiY29kZVwiOlwiSlBcIixcImNvbnRpbmVudFwiOlwiQVNcIn0sXG4gIHtcIm5hbWVcIjpcIkplcnNleVwiLFwiY29kZVwiOlwiSkVcIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIkpvcmRhblwiLFwiY29kZVwiOlwiSk9cIixcImNvbnRpbmVudFwiOlwiTUVcIn0sXG4gIHtcIm5hbWVcIjpcIkthemFraHN0YW5cIixcImNvZGVcIjpcIktaXCIsXCJjb250aW5lbnRcIjpcIkFTXCJ9LFxuICB7XCJuYW1lXCI6XCJLZW55YVwiLFwiY29kZVwiOlwiS0VcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIktpcmliYXRpXCIsXCJjb2RlXCI6XCJLSVwiLFwiY29udGluZW50XCI6XCJPQ1wifSxcbiAge1wibmFtZVwiOlwiTm9ydGggS29yZWFcIixcImNvZGVcIjpcIktQXCIsXCJjb250aW5lbnRcIjpcIkFTXCJ9LFxuICB7XCJuYW1lXCI6XCJTb3V0aCBLb3JlYVwiLFwiY29kZVwiOlwiS1JcIixcImNvbnRpbmVudFwiOlwiQVNcIn0sXG4gIHtcIm5hbWVcIjpcIktvc292b1wiLFwiY29kZVwiOlwiS09cIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIkt1d2FpdFwiLFwiY29kZVwiOlwiS1dcIixcImNvbnRpbmVudFwiOlwiTUVcIn0sXG4gIHtcIm5hbWVcIjpcIkt5cmd5enN0YW5cIixcImNvZGVcIjpcIktHXCIsXCJjb250aW5lbnRcIjpcIkFTXCJ9LFxuICB7XCJuYW1lXCI6XCJMYW9zXCIsXCJjb2RlXCI6XCJMQVwiLFwiY29udGluZW50XCI6XCJBU1wifSxcbiAge1wibmFtZVwiOlwiTGF0dmlhXCIsXCJjb2RlXCI6XCJMVlwiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiTGViYW5vblwiLFwiY29kZVwiOlwiTEJcIixcImNvbnRpbmVudFwiOlwiTUVcIn0sXG4gIHtcIm5hbWVcIjpcIkxlc290aG9cIixcImNvZGVcIjpcIkxTXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJMaWJlcmlhXCIsXCJjb2RlXCI6XCJMUlwiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiTGlieWFuXCIsXCJjb2RlXCI6XCJMWVwiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiTGllY2h0ZW5zdGVpblwiLFwiY29kZVwiOlwiTElcIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIkxpdGh1YW5pYVwiLFwiY29kZVwiOlwiTFRcIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIkx1eGVtYm91cmdcIixcImNvZGVcIjpcIkxVXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJNYWNhb1wiLFwiY29kZVwiOlwiTU9cIixcImNvbnRpbmVudFwiOlwiQVNcIn0sXG4gIHtcIm5hbWVcIjpcIk1hY2Vkb25pYVwiLFwiY29kZVwiOlwiTUtcIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIk1hZGFnYXNjYXJcIixcImNvZGVcIjpcIk1HXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJNYWxhd2lcIixcImNvZGVcIjpcIk1XXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJNYWxheXNpYVwiLFwiY29kZVwiOlwiTVlcIixcImNvbnRpbmVudFwiOlwiQVNcIn0sXG4gIHtcIm5hbWVcIjpcIk1hbGRpdmVzXCIsXCJjb2RlXCI6XCJNVlwiLFwiY29udGluZW50XCI6XCJBU1wifSxcbiAge1wibmFtZVwiOlwiTWFsaVwiLFwiY29kZVwiOlwiTUxcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIk1hbHRhXCIsXCJjb2RlXCI6XCJNVFwiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiTWFyc2hhbGwgSXNsYW5kc1wiLFwiY29kZVwiOlwiTUhcIixcImNvbnRpbmVudFwiOlwiT0NcIn0sXG4gIHtcIm5hbWVcIjpcIk1hcnRpbmlxdWVcIixcImNvZGVcIjpcIk1RXCIsXCJjb250aW5lbnRcIjpcIkNBXCJ9LFxuICB7XCJuYW1lXCI6XCJNYXVyaXRhbmlhXCIsXCJjb2RlXCI6XCJNUlwiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiTWF1cml0aXVzXCIsXCJjb2RlXCI6XCJNVVwiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiTWF5b3R0ZVwiLFwiY29kZVwiOlwiWVRcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIk1leGljb1wiLFwiY29kZVwiOlwiTVhcIixcImNvbnRpbmVudFwiOlwiTkFcIn0sXG4gIHtcIm5hbWVcIjpcIk1pY3JvbmVzaWFcIixcImNvZGVcIjpcIkZNXCIsXCJjb250aW5lbnRcIjpcIk9DXCJ9LFxuICB7XCJuYW1lXCI6XCJNb2xkb3ZhXCIsXCJjb2RlXCI6XCJNRFwiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiTW9uYWNvXCIsXCJjb2RlXCI6XCJNQ1wiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiTW9uZ29saWFcIixcImNvZGVcIjpcIk1OXCIsXCJjb250aW5lbnRcIjpcIkFTXCJ9LFxuICB7XCJuYW1lXCI6XCJNb250c2VycmF0XCIsXCJjb2RlXCI6XCJNU1wiLFwiY29udGluZW50XCI6XCJDQVwifSxcbiAge1wibmFtZVwiOlwiTW9yb2Njb1wiLFwiY29kZVwiOlwiTUFcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIk1vemFtYmlxdWVcIixcImNvZGVcIjpcIk1aXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJNeWFubWFyXCIsXCJjb2RlXCI6XCJNTVwiLFwiY29udGluZW50XCI6XCJBU1wifSxcbiAge1wibmFtZVwiOlwiTmFtaWJpYVwiLFwiY29kZVwiOlwiTkFcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIk5hdXJ1XCIsXCJjb2RlXCI6XCJOUlwiLFwiY29udGluZW50XCI6XCJPQ1wifSxcbiAge1wibmFtZVwiOlwiTmVwYWxcIixcImNvZGVcIjpcIk5QXCIsXCJjb250aW5lbnRcIjpcIkFTXCJ9LFxuICB7XCJuYW1lXCI6XCJOZXRoZXJsYW5kc1wiLFwiY29kZVwiOlwiTkxcIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIk5ldGhlcmxhbmRzIEFudGlsbGVzXCIsXCJjb2RlXCI6XCJBTlwiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiTmV3IENhbGVkb25pYVwiLFwiY29kZVwiOlwiTkNcIixcImNvbnRpbmVudFwiOlwiT0NcIn0sXG4gIHtcIm5hbWVcIjpcIk5ldyBaZWFsYW5kXCIsXCJjb2RlXCI6XCJOWlwiLFwiY29udGluZW50XCI6XCJPQ1wifSxcbiAge1wibmFtZVwiOlwiTmljYXJhZ3VhXCIsXCJjb2RlXCI6XCJOSVwiLFwiY29udGluZW50XCI6XCJDQVwifSxcbiAge1wibmFtZVwiOlwiTmlnZXJcIixcImNvZGVcIjpcIk5FXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJOaWdlcmlhXCIsXCJjb2RlXCI6XCJOR1wiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiTml1ZVwiLFwiY29kZVwiOlwiTlVcIixcImNvbnRpbmVudFwiOlwiT0NcIn0sXG4gIHtcIm5hbWVcIjpcIk5vcmZvbGsgSXNsYW5kXCIsXCJjb2RlXCI6XCJORlwiLFwiY29udGluZW50XCI6XCJPQ1wifSxcbiAge1wibmFtZVwiOlwiTm9ydGhlcm4gTWFyaWFuYVwiLFwiY29kZVwiOlwiTVBcIixcImNvbnRpbmVudFwiOlwiT0NcIn0sXG4gIHtcIm5hbWVcIjpcIk5vcndheVwiLFwiY29kZVwiOlwiTk9cIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIk9tYW5cIixcImNvZGVcIjpcIk9NXCIsXCJjb250aW5lbnRcIjpcIk1FXCJ9LFxuICB7XCJuYW1lXCI6XCJQYWtpc3RhblwiLFwiY29kZVwiOlwiUEtcIixcImNvbnRpbmVudFwiOlwiQVNcIn0sXG4gIHtcIm5hbWVcIjpcIlBhbGF1XCIsXCJjb2RlXCI6XCJQV1wiLFwiY29udGluZW50XCI6XCJPQ1wifSxcbiAge1wibmFtZVwiOlwiUGFsZXN0aW5lXCIsXCJjb2RlXCI6XCJQU1wiLFwiY29udGluZW50XCI6XCJNRVwifSxcbiAge1wibmFtZVwiOlwiUGFuYW1hXCIsXCJjb2RlXCI6XCJQQVwiLFwiY29udGluZW50XCI6XCJDQVwifSxcbiAge1wibmFtZVwiOlwiUGFwdWEgTmV3IEd1aW5lYVwiLFwiY29kZVwiOlwiUEdcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIlBhcmFndWF5XCIsXCJjb2RlXCI6XCJQWVwiLFwiY29udGluZW50XCI6XCJTQVwifSxcbiAge1wibmFtZVwiOlwiUGVydVwiLFwiY29kZVwiOlwiUEVcIixcImNvbnRpbmVudFwiOlwiU0FcIn0sXG4gIHtcIm5hbWVcIjpcIlBoaWxpcHBpbmVzXCIsXCJjb2RlXCI6XCJQSFwiLFwiY29udGluZW50XCI6XCJBU1wifSxcbiAge1wibmFtZVwiOlwiUGl0Y2Fpcm5cIixcImNvZGVcIjpcIlBOXCIsXCJjb250aW5lbnRcIjpcIk9DXCJ9LFxuICB7XCJuYW1lXCI6XCJQb2xhbmRcIixcImNvZGVcIjpcIlBMXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJQb3J0dWdhbFwiLFwiY29kZVwiOlwiUFRcIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIlB1ZXJ0byBSaWNvXCIsXCJjb2RlXCI6XCJQUlwiLFwiY29udGluZW50XCI6XCJDQVwifSxcbiAge1wibmFtZVwiOlwiUWF0YXJcIixcImNvZGVcIjpcIlFBXCIsXCJjb250aW5lbnRcIjpcIk1FXCJ9LFxuICB7XCJuYW1lXCI6XCJSZXVuaW9uXCIsXCJjb2RlXCI6XCJSRVwiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiUm9tYW5pYVwiLFwiY29kZVwiOlwiUk9cIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIlJ1c3NpYW4gRmVkZXJhdGlvblwiLFwiY29kZVwiOlwiUlVcIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIlJXQU5EQVwiLFwiY29kZVwiOlwiUldcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIlNhaW50IEhlbGVuYVwiLFwiY29kZVwiOlwiU0hcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIlNhaW50IEtpdHRzIGFuZCBOZXZpc1wiLFwiY29kZVwiOlwiS05cIixcImNvbnRpbmVudFwiOlwiQ0FcIn0sXG4gIHtcIm5hbWVcIjpcIlNhaW50IEx1Y2lhXCIsXCJjb2RlXCI6XCJMQ1wiLFwiY29udGluZW50XCI6XCJDQVwifSxcbiAge1wibmFtZVwiOlwiU2FpbnQgUGllcnJlIGFuZCBNaXF1ZWxvblwiLFwiY29kZVwiOlwiUE1cIixcImNvbnRpbmVudFwiOlwiQ0FcIn0sXG4gIHtcIm5hbWVcIjpcIlNhaW50IFZpbmNlbnRcIixcImNvZGVcIjpcIlZDXCIsXCJjb250aW5lbnRcIjpcIkNBXCJ9LFxuICB7XCJuYW1lXCI6XCJTYW1vYVwiLFwiY29kZVwiOlwiV1NcIixcImNvbnRpbmVudFwiOlwiT0NcIn0sXG4gIHtcIm5hbWVcIjpcIlNhbiBNYXJpbm9cIixcImNvZGVcIjpcIlNNXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJTYW8gVG9tZSBhbmQgUHJpbmNpcGVcIixcImNvZGVcIjpcIlNUXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJTYXVkaSBBcmFiaWFcIixcImNvZGVcIjpcIlNBXCIsXCJjb250aW5lbnRcIjpcIk1FXCJ9LFxuICB7XCJuYW1lXCI6XCJTZW5lZ2FsXCIsXCJjb2RlXCI6XCJTTlwiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiU2VyYmlhIGFuZCBNb250ZW5lZ3JvXCIsXCJjb2RlXCI6XCJDU1wiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiU2VyYmlhXCIsXCJjb2RlXCI6XCJSU1wiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiTW9udGVuZWdyb1wiLFwiY29kZVwiOlwiTUVcIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIlNleWNoZWxsZXNcIixcImNvZGVcIjpcIlNDXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJTaWVycmEgTGVvbmVcIixcImNvZGVcIjpcIlNMXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJTaW50IE1hYXJ0ZW5cIixcImNvZGVcIjpcIlNYXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJTaW5nYXBvcmVcIixcImNvZGVcIjpcIlNHXCIsXCJjb250aW5lbnRcIjpcIkFTXCJ9LFxuICB7XCJuYW1lXCI6XCJTbG92YWtpYVwiLFwiY29kZVwiOlwiU0tcIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIlNsb3ZlbmlhXCIsXCJjb2RlXCI6XCJTSVwiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiU29sb21vbiBJc2xhbmRzXCIsXCJjb2RlXCI6XCJTQlwiLFwiY29udGluZW50XCI6XCJPQ1wifSxcbiAge1wibmFtZVwiOlwiU29tYWxpYVwiLFwiY29kZVwiOlwiU09cIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIlNvdXRoIEFmcmljYVwiLFwiY29kZVwiOlwiWkFcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIlNvdXRoIEdlb3JnaWFcIixcImNvZGVcIjpcIkdTXCIsXCJjb250aW5lbnRcIjpcIkVVXCJ9LFxuICB7XCJuYW1lXCI6XCJTcGFpblwiLFwiY29kZVwiOlwiRVNcIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIlNyaSBMYW5rYVwiLFwiY29kZVwiOlwiTEtcIixcImNvbnRpbmVudFwiOlwiQVNcIn0sXG4gIHtcIm5hbWVcIjpcIlN1ZGFuXCIsXCJjb2RlXCI6XCJTRFwiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiU3VyaSBuYW1lXCIsXCJjb2RlXCI6XCJTUlwiLFwiY29udGluZW50XCI6XCJTQVwifSxcbiAge1wibmFtZVwiOlwiU3ZhbGJhcmQgYW5kIEphbiBNYXllblwiLFwiY29kZVwiOlwiU0pcIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIlN3YXppbGFuZFwiLFwiY29kZVwiOlwiU1pcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIlN3ZWRlblwiLFwiY29kZVwiOlwiU0VcIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIlN3aXR6ZXJsYW5kXCIsXCJjb2RlXCI6XCJDSFwiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiU3lyaWFuIEFyYWIgUmVwdWJsaWNcIixcImNvZGVcIjpcIlNZXCIsXCJjb250aW5lbnRcIjpcIkFTXCJ9LFxuICB7XCJuYW1lXCI6XCJUYWl3YW5cIixcImNvZGVcIjpcIlRXXCIsXCJjb250aW5lbnRcIjpcIkFTXCJ9LFxuICB7XCJuYW1lXCI6XCJUYWppa2lzdGFuXCIsXCJjb2RlXCI6XCJUSlwiLFwiY29udGluZW50XCI6XCJBU1wifSxcbiAge1wibmFtZVwiOlwiVGFuemFuaWFcIixcImNvZGVcIjpcIlRaXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJUaGFpbGFuZFwiLFwiY29kZVwiOlwiVEhcIixcImNvbnRpbmVudFwiOlwiQVNcIn0sXG4gIHtcIm5hbWVcIjpcIkVhc3QgVGltb3JcIixcImNvZGVcIjpcIlRMXCIsXCJjb250aW5lbnRcIjpcIkFTXCJ9LFxuICB7XCJuYW1lXCI6XCJUb2dvXCIsXCJjb2RlXCI6XCJUR1wiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiVG9rZWxhdVwiLFwiY29kZVwiOlwiVEtcIixcImNvbnRpbmVudFwiOlwiT0NcIn0sXG4gIHtcIm5hbWVcIjpcIlRvbmdhXCIsXCJjb2RlXCI6XCJUT1wiLFwiY29udGluZW50XCI6XCJPQ1wifSxcbiAge1wibmFtZVwiOlwiVHJpbmlkYWQgYW5kIFRvYmFnb1wiLFwiY29kZVwiOlwiVFRcIixcImNvbnRpbmVudFwiOlwiQ0FcIn0sXG4gIHtcIm5hbWVcIjpcIlR1bmlzaWFcIixcImNvZGVcIjpcIlROXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJUdXJrZXlcIixcImNvZGVcIjpcIlRSXCIsXCJjb250aW5lbnRcIjpcIk1FXCJ9LFxuICB7XCJuYW1lXCI6XCJUdXJrbWVuaXN0YW5cIixcImNvZGVcIjpcIlRNXCIsXCJjb250aW5lbnRcIjpcIkFTXCJ9LFxuICB7XCJuYW1lXCI6XCJUdXJrcyBhbmQgQ2FpY29zIElzbGFuZHNcIixcImNvZGVcIjpcIlRDXCIsXCJjb250aW5lbnRcIjpcIkNBXCJ9LFxuICB7XCJuYW1lXCI6XCJUdXZhbHVcIixcImNvZGVcIjpcIlRWXCIsXCJjb250aW5lbnRcIjpcIk9DXCJ9LFxuICB7XCJuYW1lXCI6XCJVZ2FuZGFcIixcImNvZGVcIjpcIlVHXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJVa3JhaW5lXCIsXCJjb2RlXCI6XCJVQVwiLFwiY29udGluZW50XCI6XCJFVVwifSxcbiAge1wibmFtZVwiOlwiVW5pdGVkIEFyYWIgRW1pcmF0ZXNcIixcImNvZGVcIjpcIkFFXCIsXCJjb250aW5lbnRcIjpcIk1FXCJ9LFxuICB7XCJuYW1lXCI6XCJVbml0ZWQgS2luZ2RvbVwiLFwiY29kZVwiOlwiR0JcIixcImNvbnRpbmVudFwiOlwiRVVcIn0sXG4gIHtcIm5hbWVcIjpcIlVuaXRlZCBTdGF0ZXNcIixcImNvZGVcIjpcIlVTXCIsXCJjb250aW5lbnRcIjpcIk5BXCJ9LFxuICB7XCJuYW1lXCI6XCJVcnVndWF5XCIsXCJjb2RlXCI6XCJVWVwiLFwiY29udGluZW50XCI6XCJTQVwifSxcbiAge1wibmFtZVwiOlwiVXpiZWtpc3RhblwiLFwiY29kZVwiOlwiVVpcIixcImNvbnRpbmVudFwiOlwiQVNcIn0sXG4gIHtcIm5hbWVcIjpcIlZhbnVhdHVcIixcImNvZGVcIjpcIlZVXCIsXCJjb250aW5lbnRcIjpcIk9DXCJ9LFxuICB7XCJuYW1lXCI6XCJWZW5lenVlbGFcIixcImNvZGVcIjpcIlZFXCIsXCJjb250aW5lbnRcIjpcIlNBXCJ9LFxuICB7XCJuYW1lXCI6XCJWaWV0bmFtXCIsXCJjb2RlXCI6XCJWTlwiLFwiY29udGluZW50XCI6XCJBU1wifSxcbiAge1wibmFtZVwiOlwiQnJpdGlzaCBWaXJnaW4gSXNsYW5kc1wiLFwiY29kZVwiOlwiVkdcIixcImNvbnRpbmVudFwiOlwiQ0FcIn0sXG4gIHtcIm5hbWVcIjpcIlZpcmdpbiBJc2xhbmRzLCBVLlMuXCIsXCJjb2RlXCI6XCJWSVwiLFwiY29udGluZW50XCI6XCJDQVwifSxcbiAge1wibmFtZVwiOlwiV2FsbGlzIGFuZCBGdXR1bmFcIixcImNvZGVcIjpcIldGXCIsXCJjb250aW5lbnRcIjpcIk9DXCJ9LFxuICB7XCJuYW1lXCI6XCJXZXN0ZXJuIFNhaGFyYVwiLFwiY29kZVwiOlwiRUhcIixcImNvbnRpbmVudFwiOlwiQUZcIn0sXG4gIHtcIm5hbWVcIjpcIlllbWVuXCIsXCJjb2RlXCI6XCJZRVwiLFwiY29udGluZW50XCI6XCJNRVwifSxcbiAge1wibmFtZVwiOlwiWmFtYmlhXCIsXCJjb2RlXCI6XCJaTVwiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiWmltYmFid2VcIixcImNvZGVcIjpcIlpXXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJBUklQTyBUcmVhdHlcIixcImNvZGVcIjpcIkFSSVBPXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJPQVBJIFRyZWF0eVwiLFwiY29kZVwiOlwiT0FQSVwiLFwiY29udGluZW50XCI6XCJBRlwifSxcbiAge1wibmFtZVwiOlwiS3VyZGlzdGFuIFJlZ2lvblwiLFwiY29kZVwiOlwiS1JEXCIsXCJjb250aW5lbnRcIjpcIk1FXCJ9LFxuICB7XCJuYW1lXCI6XCJaYW56aWJhclwiLFwiY29kZVwiOlwiRUFaXCIsXCJjb250aW5lbnRcIjpcIkFGXCJ9LFxuICB7XCJuYW1lXCI6XCJTYWludCBNYXJ0aW5cIixcImNvZGVcIjpcIk1BRlwiLFwiY29udGluZW50XCI6XCJFVVwifSwgXG4gIHtcIm5hbWVcIjpcIlNhaW50IFZpbmNlbnQgYW5kIHRoZSBHcmVuYWRpbmVzXCIsXCJjb2RlXCI6XCJWQ1RcIixcImNvbnRpbmVudFwiOlwiT0NcIn1cbl1cbiIsInZhciBjb3VudHJpZXMgPSByZXF1aXJlKCcuL2NvdW50cmllcy5qc29uJylcbnZhciBzdGF0ZXMgPSByZXF1aXJlKCcuL3N0YXRlcy5qc29uJylcblxuLy9jb25zb2xlLmxvZyhjb3VudHJpZXMpXG4vKlxuIEFGXHRBZnJpY2FcbiBBTlx0QW50YXJjdGljYVxuIEFTXHRBc2lhXG4gRVVcdEV1cm9wZVxuIE5BXHROb3J0aCBhbWVyaWNhXG4gT0NcdE9jZWFuaWFcbiBTQVx0U291dGggYW1lcmljYVxuICovXG5cbmZ1bmN0aW9uIGFsbF9jb3VudHJpZXMoKSB7XG4gIHJldHVybiBjb3VudHJpZXNcbn1cblxuZnVuY3Rpb24gZ2V0X2NvdW50cnkoY29kZSkge1xuICBmb3IgKHZhciBpID0gMDsgaSA8IGNvdW50cmllcy5sZW5ndGg7IGkrKykge1xuICAgIGlmIChjb3VudHJpZXNbaV0uY29kZSA9PT0gY29kZSkge1xuICAgICAgcmV0dXJuIGNvdW50cmllc1tpXVxuICAgIH1cbiAgfVxufVxuXG52YXIgY291bnRyeV9hbGlhcyA9IHtcbiAgXCJUSEUgR0FNQklBXCI6IFwiR0FNQklBXCIsXG4gIFwiR1VJTkVBIEJJU1NBVVwiOiBcIkdVSU5FQS1CSVNTQVVcIixcbiAgXCJTQUlOVCBWSU5DRU5UIEFORCBHUkVOQURJTkVTXCI6IFwiU0FJTlQgVklOQ0VOVFwiLFxuICBcIlBBTEVTVElOSUFOIFRFUlJJVE9SWVwiOiBcIlBBTEVTVElORVwiXG59XG5cbmZ1bmN0aW9uIGdldF9jb3VudHJ5X2J5X25hbWUobmFtZSkge1xuICB2YXIga25hbWUgPSBuYW1lLnJlcGxhY2UoLy18X3wlMjAvZywgJyAnKS50b1VwcGVyQ2FzZSgpIC8vIGhhbmRsZSBBbWVyaWNhbi1TYW1vYSBvciBBbVxuXG4gIGNvbnNvbGUubG9nKGNvdW50cnlfYWxpYXNba25hbWVdLCBrbmFtZSlcbiAgaWYgKGNvdW50cnlfYWxpYXNba25hbWVdKVxuICAgIGtuYW1lID0gY291bnRyeV9hbGlhc1trbmFtZV1cblxuXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgY291bnRyaWVzLmxlbmd0aDsgaSsrKSB7XG4gICAgaWYgKGNvdW50cmllc1tpXS5uYW1lLnRvVXBwZXJDYXNlKCkgPT09IGtuYW1lKVxuICAgICAgcmV0dXJuIGNvdW50cmllc1tpXVxuICB9XG59XG5cbi8vIGdldF9jb3VudHJ5X2J5X3JlZ2V4KC9eXFwvcmVnaXN0cmF0aW9uLWluLShcXHcrKS9pLCAnL3JlZ2lzdHJhdGlvbi1pbi1DaGluYScpXG5cbmZ1bmN0aW9uIGdldF9jb3VudHJ5X2J5X3JlZ2V4KHJlZ3gsIHN0cmluZykge1xuXG4gIHZhciByc2x0ID0gcmVneC5leGVjKHN0cmluZylcbiAgaWYgKHJzbHQgPT09IG51bGwpXG4gICAgcmV0dXJuIHVuZGVmaW5lZFxuICBlbHNlIHtcbiAgICByZXR1cm4gZ2V0X2NvdW50cnlfYnlfbmFtZShyc2x0WzFdLnJlcGxhY2UoL18vZywgJyAnKSlcbiAgfVxufVxuXG5cblxuXG4vLyBpZiBwYXJhbSBpcyB1bmRlZmluZWQsIGFsbCByZXR1cm5cbi8vIGlmIHBhcmFtIGlzIGFuIGFycmF5LCBvbmx5IHRob3NlIGluIHRoZSBsaXN0IHJldHVybmVkXG5mdW5jdGlvbiBnZXRfY291bnRyaWVzKHBhcmFtKSB7XG4gIGlmIChwYXJhbSA9PT0gdW5kZWZpbmVkKVxuICAgIHJldHVybiBjb3VudHJpZXNcbiAgZWxzZSB7XG4gICAgdmFyIHJldCA9IFtdXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBwYXJhbS5sZW5ndGg7IGkrKykge1xuICAgICAgdmFyIGNvdW50cnkgPSBnZXRfY291bnRyeShwYXJhbVtpXSlcbiAgICAgIGlmIChjb3VudHJ5ICE9PSB1bmRlZmluZWQpXG4gICAgICAgIHJldC5wdXNoKGNvdW50cnkpXG4gICAgfVxuICAgIHJldHVybiByZXRcbiAgfVxufVxuXG5mdW5jdGlvbiBnZXRfY291bnRyaWVzX2J5X2NvbnRuZW50KGNvZGUpIHtcbiAgcmV0dXJuIGNvdW50cmllcy5maWx0ZXIoZnVuY3Rpb24gKHZhbHVlLCBpbmRleCwgYXIpIHtcbiAgICByZXR1cm4gdmFsdWUuY29udGluZW50ID09PSBjb2RlXG4gIH0pXG59XG5cblxuLy9jb25zb2xlLmxvZyhnZXRfY291bnRyaWVzKFsnVVMnLCAnQ04nLCd4eCddKSlcblxuZXhwb3J0cy5nZXRfY291bnRyaWVzID0gZnVuY3Rpb24gKGFQYXJhbSkge1xuICByZXR1cm4gZ2V0X2NvdW50cmllcyhhUGFyYW0pXG59XG5cblxuZXhwb3J0cy5nZXRfY291bnRyeSA9IGZ1bmN0aW9uIChhUGFyYW0pIHtcbiAgcmV0dXJuIGdldF9jb3VudHJ5KGFQYXJhbSlcbn1cblxuXG5leHBvcnRzLmdldF9jb3VudHJ5X2J5X25hbWUgPSBmdW5jdGlvbiAobmFtZSkge1xuICByZXR1cm4gZ2V0X2NvdW50cnlfYnlfbmFtZShuYW1lKVxufVxuXG5cbmV4cG9ydHMuZ2V0X2NvdW50cnlfYnlfcmVnZXggPSBmdW5jdGlvbiAocmVneCwgc3RyaW5nKSB7XG4gIHJldHVybiBnZXRfY291bnRyeV9ieV9yZWdleChyZWd4LCBzdHJpbmcpXG59XG5cblxuZXhwb3J0cy5nZXRfY291bnRyaWVzX2J5X2NvbnRuZW50ID0gZnVuY3Rpb24gKGNvZGUpIHtcbiAgcmV0dXJuIGdldF9jb3VudHJpZXNfYnlfY29udG5lbnQoY29kZSlcbn1cblxuLy8gcmV0dXJucyBzdGF0ZXMgb2YgdGhlIGNvdW50cnlfY29kZVxuXG5leHBvcnRzLmdldF9zdGF0ZXMgPSBmdW5jdGlvbiAoY29kZSkge1xuICByZXR1cm4gc3RhdGVzW2NvZGVdXG59XG5cblxuZXhwb3J0cy5nZXRfc3RhdGVfbmFtZSA9IGZ1bmN0aW9uIChjb2RlKSB7XG4gIHZhciBjb3VudHJ5ID0gY29kZS5zdWJzdHJpbmcoMCwgMilcbiAgY29uc29sZS5sb2coJ2NvdW50cnknLCBjb3VudHJ5KVxuICBpZiAoc3RhdGVzW2NvdW50cnldKVxuICAgIHJldHVybiBzdGF0ZXNbY291bnRyeV1bY29kZV1cbiAgZWxzZVxuICAgIHJldHVybiB1bmRlZmluZWRcbn1cblxuXG5cblxuIiwibW9kdWxlLmV4cG9ydHM9bW9kdWxlLmV4cG9ydHM9bW9kdWxlLmV4cG9ydHM9e1xuICBcIlVTXCI6IHtcbiAgICBcIlVTLkFMXCI6IFwiQWxhYmFtYVwiLFxuICAgIFwiVVMuQUtcIjogXCJBbGFza2FcIixcbiAgICBcIlVTLkFaXCI6IFwiQXJpem9uYVwiLFxuICAgIFwiVVMuQVJcIjogXCJBcmthbnNhc1wiLFxuICAgIFwiVVMuQ0FcIjogXCJDYWxpZm9ybmlhXCIsXG4gICAgXCJVUy5DT1wiOiBcIkNvbG9yYWRvXCIsXG4gICAgXCJVUy5DVFwiOiBcIkNvbm5lY3RpY3V0XCIsXG4gICAgXCJVUy5ERVwiOiBcIkRlbGF3YXJlXCIsXG4gICAgXCJVUy5EQ1wiOiBcIkRpc3RyaWN0IG9mIENvbHVtYmlhXCIsXG4gICAgXCJVUy5GTFwiOiBcIkZsb3JpZGFcIixcbiAgICBcIlVTLkdBXCI6IFwiR2VvcmdpYVwiLFxuICAgIFwiVVMuSElcIjogXCJIYXdhaWlcIixcbiAgICBcIlVTLklEXCI6IFwiSWRhaG9cIixcbiAgICBcIlVTLklMXCI6IFwiSWxsaW5vaXNcIixcbiAgICBcIlVTLklOXCI6IFwiSW5kaWFuYVwiLFxuICAgIFwiVVMuSUFcIjogXCJJb3dhXCIsXG4gICAgXCJVUy5LU1wiOiBcIkthbnNhc1wiLFxuICAgIFwiVVMuS1lcIjogXCJLZW50dWNreVwiLFxuICAgIFwiVVMuTEFcIjogXCJMb3Vpc2lhbmFcIixcbiAgICBcIlVTLk1FXCI6IFwiTWFpbmVcIixcbiAgICBcIlVTLk1EXCI6IFwiTWFyeWxhbmRcIixcbiAgICBcIlVTLk1BXCI6IFwiTWFzc2FjaHVzZXR0c1wiLFxuICAgIFwiVVMuTUlcIjogXCJNaWNoaWdhblwiLFxuICAgIFwiVVMuTU5cIjogXCJNaW5uZXNvdGFcIixcbiAgICBcIlVTLk1TXCI6IFwiTWlzc2lzc2lwcGlcIixcbiAgICBcIlVTLk1PXCI6IFwiTWlzc291cmlcIixcbiAgICBcIlVTLk1UXCI6IFwiTW9udGFuYVwiLFxuICAgIFwiVVMuTkVcIjogXCJOZWJyYXNrYVwiLFxuICAgIFwiVVMuTlZcIjogXCJOZXZhZGFcIixcbiAgICBcIlVTLk5IXCI6IFwiTmV3IEhhbXBzaGlyZVwiLFxuICAgIFwiVVMuTkpcIjogXCJOZXcgSmVyc2V5XCIsXG4gICAgXCJVUy5OTVwiOiBcIk5ldyBNZXhpY29cIixcbiAgICBcIlVTLk5ZXCI6IFwiTmV3IFlvcmtcIixcbiAgICBcIlVTLk5DXCI6IFwiTm9ydGggQ2Fyb2xpbmFcIixcbiAgICBcIlVTLk5EXCI6IFwiTm9ydGggRGFrb3RhXCIsXG4gICAgXCJVUy5PSFwiOiBcIk9oaW9cIixcbiAgICBcIlVTLk9LXCI6IFwiT2tsYWhvbWFcIixcbiAgICBcIlVTLk9SXCI6IFwiT3JlZ29uXCIsXG4gICAgXCJVUy5QQVwiOiBcIlBlbm5zeWx2YW5pYVwiLFxuICAgIFwiVVMuUklcIjogXCJSaG9kZSBJc2xhbmRcIixcbiAgICBcIlVTLlNDXCI6IFwiU291dGggQ2Fyb2xpbmFcIixcbiAgICBcIlVTLlNEXCI6IFwiU291dGggRGFrb3RhXCIsXG4gICAgXCJVUy5UTlwiOiBcIlRlbm5lc3NlZVwiLFxuICAgIFwiVVMuVFhcIjogXCJUZXhhc1wiLFxuICAgIFwiVVMuVVRcIjogXCJVdGFoXCIsXG4gICAgXCJVUy5WVFwiOiBcIlZlcm1vbnRcIixcbiAgICBcIlVTLlZBXCI6IFwiVmlyZ2luaWFcIixcbiAgICBcIlVTLldBXCI6IFwiV2FzaGluZ3RvblwiLFxuICAgIFwiVVMuV1ZcIjogXCJXZXN0IFZpcmdpbmlhXCIsXG4gICAgXCJVUy5XSVwiOiBcIldpc2NvbnNpblwiLFxuICAgIFwiVVMuV1lcIjogXCJXeW9taW5nXCJcbiAgfSxcbiAgXCJDTlwiIDoge1xuICAgIFwiQ04uQUhcIjogXCJBbmh1aVwiLFxuICAgIFwiQ04uQkpcIjogXCJCZWlqaW5nXCIsXG4gICAgXCJDTi5DUVwiOiBcIkNob25ncWluZ1wiLFxuICAgIFwiQ04uRkpcIjogXCJGdWppYW5cIixcbiAgICBcIkNOLkdTXCI6IFwiR2Fuc3VcIixcbiAgICBcIkNOLkdEXCI6IFwiR3VhbmRvbmdcIixcbiAgICBcIkNOLkdYXCI6IFwiR3Vhbmd4aVwiLFxuICAgIFwiQ04uSEFcIjogXCJIYWluYW5cIixcbiAgICBcIkNOLkhCXCI6IFwiSGViZWlcIixcbiAgICBcIkNOLkhMXCI6IFwiSGVpbG9uZ2ppYW5cIixcbiAgICBcIkNOLkhBXCI6IFwiSGVuYW5cIixcbiAgICBcIkNOLkhCXCI6IFwiSHViZWlcIixcbiAgICBcIkNOLkhOXCI6IFwiSHVuYW5cIixcbiAgICBcIkNOLkpTXCI6IFwiSmlhbmdzdVwiLFxuICAgIFwiQ04uSlhcIjogXCJKaWFuZ3hpXCIsXG4gICAgXCJDTi5KTFwiOiBcIkppbGluXCIsXG4gICAgXCJDTi5MTlwiOiBcIkxpYW9uaW5nXCIsXG4gICAgXCJDTi5OTVwiOiBcIk5laSBNb25nb2xcIixcbiAgICBcIkNOLk5YXCI6IFwiTmluZ3hpYVwiLFxuICAgIFwiQ04uUUhcIjogXCJRaW5naGFpXCIsXG4gICAgXCJDTi5TQVwiOiBcIlNoYWFueGlcIixcbiAgICBcIkNOLlNEXCI6IFwiU2hhbmRvbmdcIixcbiAgICBcIkNOLlNIXCI6IFwiU2hhbmdoYWlcIixcbiAgICBcIkNOLlNYXCI6IFwiU2hhbnhpXCIsXG4gICAgXCJDTi5TQ1wiOiBcIlNpY2h1YW5cIixcbiAgICBcIkNOLlRKXCI6IFwiVGlhbmppblwiLFxuICAgIFwiQ04uWEpcIjogXCJYaW5qdWFuXCIsXG4gICAgXCJDTi5YWlwiOiBcIlhpemFuZ1wiLFxuICAgIFwiQ04uWU5cIjogXCJZdW5uYW5cIixcbiAgICBcIkNOLlpKXCI6IFwiWmhlamlhblwiXG4gIH0sXG4gIFwiSEtcIiA6IHtcbiAgICBcIkhLLkNXXCI6IFwiQ2VudHJhbCBhbmQgV2VzdGVyblwiLFxuICAgIFwiSEsuRUFcIjogXCJFYXN0ZXJuXCIsXG4gICAgXCJISy5JU1wiOiBcIklzbGFuZHNcIixcbiAgICBcIkhLLktDXCI6IFwiS293bG9vbiBDaXR5XCIsXG4gICAgXCJISy5LSVwiOiBcIkt3YWkgVHNpbmdcIixcbiAgICBcIkhLLktVXCI6IFwiS3d1biBUb25nXCIsXG4gICAgXCJISy5OT1wiOiBcIk5vcnRoXCIsXG4gICAgXCJISy5TS1wiOiBcIlNhaSBLdW5nXCIsXG4gICAgXCJISy5TU1wiOiBcIlNoYW0gU2h1aSBQb1wiLFxuICAgIFwiSEsuU1RcIjogXCJTaGEgVGluXCIsXG4gICAgXCJISy5TT1wiOiBcIlNvdXRoZXJuXCIsXG4gICAgXCJISy5UUFwiOiBcIlRhaSBQb1wiLFxuICAgIFwiSEsuVFdcIjogXCJUc3VlbiBXYW5cIixcbiAgICBcIkhLLlRNXCI6IFwiVHVlbiBNdW5cIixcbiAgICBcIkhLLldDXCI6IFwiV2FuIENoYWlcIixcbiAgICBcIkhLLldUXCI6IFwiV29uZyBUYWkgU2luXCIsXG4gICAgXCJISy5ZVFwiOiBcIllhdSBUc2ltIE1vbmdcIixcbiAgICBcIkhLLllMXCI6IFwiWXVlbiBMb25nXCJcbiAgfSxcbiAgXCJQSFwiIDoge1xuICAgIFwiUEguQUJcIjogXCJBYnJhXCIsXG4gICAgXCJQSC5BTlwiOiBcIkFndXNhbiBkZWwgTm9ydGVcIixcbiAgICBcIlBILkFTXCI6IFwiQWd1c2FuIGRlbCBTdXJcIixcbiAgICBcIlBILkFLXCI6IFwiQWtsYW5cIixcbiAgICBcIlBILkFMXCI6IFwiQWxiYXlcIixcbiAgICBcIlBILkFRXCI6IFwiQW50aXF1ZVwiLFxuICAgIFwiUEguQVBcIjogXCJBcGF5YW9cIixcbiAgICBcIlBILkFVXCI6IFwiQXVyb3JhXCIsXG4gICAgXCJQSC5CU1wiOiBcIkJhc2lsYW5cIixcbiAgICBcIlBILkJBXCI6IFwiQmF0YWFuXCIsXG4gICAgXCJQSC5CTlwiOiBcIkJhdGFuZXNcIixcbiAgICBcIlBILkJUXCI6IFwiQmF0YW5nYXNcIixcbiAgICBcIlBILkJHXCI6IFwiQmVuZ3VldFwiLFxuICAgIFwiUEguQklcIjogXCJCaWxpcmFuXCIsXG4gICAgXCJQSC5CT1wiOiBcIkJvaG9sXCIsXG4gICAgXCJQSC5CS1wiOiBcIkJ1a2lkbm9uXCIsXG4gICAgXCJQSC5CVVwiOiBcIkJ1bGFjYW5cIixcbiAgICBcIlBILkNHXCI6IFwiQ2FnYXlhblwiLFxuICAgIFwiUEguQ05cIjogXCJDYW1hcmluZXMgTm9ydGVcIixcbiAgICBcIlBILkNTXCI6IFwiQ2FtYXJpbmVzIFN1clwiLFxuICAgIFwiUEguQ01cIjogXCJDYW1pZ3VpblwiLFxuICAgIFwiUEguQ1BcIjogXCJDYXBpelwiLFxuICAgIFwiUEguQ1RcIjogXCJDYXRhbmR1YW5lc1wiLFxuICAgIFwiUEguQ1ZcIjogXCJDYXZpdGVcIixcbiAgICBcIlBILkNCXCI6IFwiQ2VidVwiLFxuICAgIFwiUEguQ0xcIjogXCJDb21wb3N0ZWxhIFZhbGxleVwiLFxuICAgIFwiUEguTkNcIjogXCJDb3RhYmF0b1wiLFxuICAgIFwiUEguRFZcIjogXCJEYXZhbyBkZWwgTm9ydGVcIixcbiAgICBcIlBILkRTXCI6IFwiRGF2YW8gZGVsIFN1clwiLFxuICAgIFwiUEguRE9cIjogXCJEYXZhbyBPcmllbnRhbFwiLFxuICAgIFwiUEguRElcIjogXCJEaW5hZ2F0IElzbGFuZHNcIixcbiAgICBcIlBILkVTXCI6IFwiRWFzdGVybiBTYW1hclwiLFxuICAgIFwiUEguR1VcIjogXCJHdWltYXJhc1wiLFxuICAgIFwiUEguSUZcIjogXCJJZnVnYW9cIixcbiAgICBcIlBILklOXCI6IFwiSWxvY29zIE5vcnRlXCIsXG4gICAgXCJQSC5JU1wiOiBcIklsb2NvcyBTdXJcIixcbiAgICBcIlBILklJXCI6IFwiSWxvaWxvXCIsXG4gICAgXCJQSC5JQlwiOiBcIklzYWJlbGFcIixcbiAgICBcIlBILktBXCI6IFwiS2FsaW5nYVwiLFxuICAgIFwiUEguTEdcIjogXCJMYWd1bmFcIixcbiAgICBcIlBILkxOXCI6IFwiTGFuYW8gZGVsIE5vcnRlXCIsXG4gICAgXCJQSC5MU1wiOiBcIkxhbmFvIGRlbCBTdXJcIixcbiAgICBcIlBILkxVXCI6IFwiTGEgVW5pb25cIixcbiAgICBcIlBILkxFXCI6IFwiTGV5dGVcIixcbiAgICBcIlBILk1HXCI6IFwiTWFndWluZGFuYW9cIixcbiAgICBcIlBILk1RXCI6IFwiTWFyaW5kdXF1ZVwiLFxuICAgIFwiUEguTUJcIjogXCJNYXNiYXRlXCIsXG4gICAgXCJQSC5NTVwiOiBcIk1ldHJvcG9saXRhbiBNYW5pbGFcIixcbiAgICBcIlBILk1EXCI6IFwiTWlzYW1pcyBPY2NpZGVudGFsXCIsXG4gICAgXCJQSC5NTlwiOiBcIk1pc2FtaXMgT3JpZW50YWxcIixcbiAgICBcIlBILk1UXCI6IFwiTW91bnRhaW5cIixcbiAgICBcIlBILk5EXCI6IFwiTmVncm9zIE9jY2lkZW50YWxcIixcbiAgICBcIlBILk5SXCI6IFwiTmVncm9zIE9yaWVudGFsXCIsXG4gICAgXCJQSC5OU1wiOiBcIk5vcnRoZXJuIFNhbWFyXCIsXG4gICAgXCJQSC5ORVwiOiBcIk51ZXZhIEVjaWphXCIsXG4gICAgXCJQSC5OVlwiOiBcIk51ZXZhIFZpemNheWFcIixcbiAgICBcIlBILk1DXCI6IFwiT2NjaWRlbnRhbCBNaW5kb3JvXCIsXG4gICAgXCJQSC5NUlwiOiBcIk9yaWVudGFsIE1pbmRvcm9cIixcbiAgICBcIlBILlBMXCI6IFwiUGFsYXdhblwiLFxuICAgIFwiUEguUE1cIjogXCJQYW1wYW5nYVwiLFxuICAgIFwiUEguUE5cIjogXCJQYW5nYXNpbmFuXCIsXG4gICAgXCJQSC5RWlwiOiBcIlF1ZXpvblwiLFxuICAgIFwiUEguUVJcIjogXCJRdWlyaW5vXCIsXG4gICAgXCJQSC5SSVwiOiBcIlJpemFsXCIsXG4gICAgXCJQSC5ST1wiOiBcIlJvbWJsb25cIixcbiAgICBcIlBILlNNXCI6IFwiU2FtYXJcIixcbiAgICBcIlBILlNHXCI6IFwiU2FyYW5nYW5pXCIsXG4gICAgXCJQSC5TUVwiOiBcIlNpcXVpam9yXCIsXG4gICAgXCJQSC5TUlwiOiBcIlNvcnNvZ29uXCIsXG4gICAgXCJQSC5TQ1wiOiBcIlNvdXRoIENvdGFiYXRvXCIsXG4gICAgXCJQSC5TTFwiOiBcIlNvdXRoZXJuIExleXRlXCIsXG4gICAgXCJQSC5TS1wiOiBcIlN1bHRhbiBLdWRhcmF0XCIsXG4gICAgXCJQSC5TVVwiOiBcIlN1bHVcIixcbiAgICBcIlBILlNUXCI6IFwiU3VyaWdhbyBkZWwgTm9ydGVcIixcbiAgICBcIlBILlNTXCI6IFwiU3VyaWdhbyBkZWwgU3VyXCIsXG4gICAgXCJQSC5UUlwiOiBcIlRhcmxhY1wiLFxuICAgIFwiUEguVFRcIjogXCJUYXdpLVRhd2lcIixcbiAgICBcIlBILlpNXCI6IFwiWmFtYmFsZXNcIixcbiAgICBcIlBILlpOXCI6IFwiWmFtYm9hbmdhIGRlbCBOb3J0ZVwiLFxuICAgIFwiUEguWlNcIjogXCJaYW1ib2FuZ2EgZGVsIFN1clwiLFxuICAgIFwiUEguWllcIjogXCJaYW1ib2FuZ2EtU2lidWdheVwiXG4gIH0sXG4gIFwiQ0FcIiA6IHtcbiAgICBcIkNBLkFCXCI6IFwiQWxiZXJ0YVwiLFxuICAgIFwiQ0EuQkNcIjogXCJCcml0aXNoIENvbHVtYmlhXCIsXG4gICAgXCJDQS5NQlwiOiBcIk1hbml0b2JhXCIsXG4gICAgXCJDQS5OQlwiOiBcIk5ldyBCcnVuc3dpY2tcIixcbiAgICBcIkNBLk5GXCI6IFwiTmV3Zm91bmRsYW5kIGFuZCBMYWJyYWRvclwiLFxuICAgIFwiQ0EuTlRcIjogXCJOb3J0aHdlc3QgVGVycml0b3JpZXNcIixcbiAgICBcIkNBLk5TXCI6IFwiTm92YSBTY290aWFcIixcbiAgICBcIkNBLk5VXCI6IFwiTnVuYXZ1dFwiLFxuICAgIFwiQ0EuT05cIjogXCJPbnRhcmlvXCIsXG4gICAgXCJDQS5QRVwiOiBcIlByaW5jZSBFZHdhcmQgSXNsYW5kXCIsXG4gICAgXCJDQS5RQ1wiOiBcIlF1ZWJlY1wiLFxuICAgIFwiQ0EuU0tcIjogXCJTYXNrYXRjaGV3YW5cIixcbiAgICBcIkNBLllUXCI6IFwiWXVrb24gVGVycml0b3J5XCJcbiAgfSxcbiAgXCJUSFwiIDoge1xuICAgIFwiVEguQUNcIjogXCJBbW5hdCBDaGFyb2VuXCIsXG4gICAgXCJUSC5BVFwiOiBcIkFuZyBUaG9uZ1wiLFxuICAgIFwiVEguQk1cIjogXCJCYW5na29rIE1ldHJvcG9saXNcIixcbiAgICBcIlRILkJSXCI6IFwiQnVyaSBSYW1cIixcbiAgICBcIlRILkNDXCI6IFwiQ2hhY2hvZW5nc2FvXCIsXG4gICAgXCJUSC5DTlwiOiBcIkNoYWkgTmF0XCIsXG4gICAgXCJUSC5DWVwiOiBcIkNoYWl5YXBodW1cIixcbiAgICBcIlRILkNUXCI6IFwiQ2hhbnRoYWJ1cmlcIixcbiAgICBcIlRILkNNXCI6IFwiQ2hpYW5nIE1haVwiLFxuICAgIFwiVEguQ1JcIjogXCJDaGlhbmcgUmFpXCIsXG4gICAgXCJUSC5DQlwiOiBcIkNob24gQnVyaVwiLFxuICAgIFwiVEguQ1BcIjogXCJDaHVtcGhvblwiLFxuICAgIFwiVEguS0xcIjogXCJLYWxhc2luXCIsXG4gICAgXCJUSC5LUFwiOiBcIkthbXBoYWVuZyBQaGV0XCIsXG4gICAgXCJUSC5LTlwiOiBcIkthbmNoYW5hYnVyaVwiLFxuICAgIFwiVEguS0tcIjogXCJLaG9uIEthZW5cIixcbiAgICBcIlRILktSXCI6IFwiS3JhYmlcIixcbiAgICBcIlRILkxHXCI6IFwiTGFtcGFuZ1wiLFxuICAgIFwiVEguTE5cIjogXCJMYW1waHVuXCIsXG4gICAgXCJUSC5MRVwiOiBcIkxvZWlcIixcbiAgICBcIlRILkxCXCI6IFwiTG9wIEJ1cmlcIixcbiAgICBcIlRILk1IXCI6IFwiTWFlIEhvbmcgU29uXCIsXG4gICAgXCJUSC5NU1wiOiBcIk1haGEgU2FyYWtoYW1cIixcbiAgICBcIlRILk1EXCI6IFwiTXVrZGFoYW5cIixcbiAgICBcIlRILk5OXCI6IFwiTmFraG9uIE5heW9rXCIsXG4gICAgXCJUSC5OUFwiOiBcIk5ha2hvbiBQYXRob21cIixcbiAgICBcIlRILk5GXCI6IFwiTmFraG9uIFBoYW5vbVwiLFxuICAgIFwiVEguTlJcIjogXCJOYWtob24gUmF0Y2hhc2ltYVwiLFxuICAgIFwiVEguTlNcIjogXCJOYWtob24gU2F3YW5cIixcbiAgICBcIlRILk5UXCI6IFwiTmFraG9uIFNpIFRoYW1tYXJhdFwiLFxuICAgIFwiVEguTkFcIjogXCJOYW5cIixcbiAgICBcIlRILk5XXCI6IFwiTmFyYXRoaXdhdFwiLFxuICAgIFwiVEguTkJcIjogXCJOb25nIEJ1YSBMYW0gUGh1XCIsXG4gICAgXCJUSC5OS1wiOiBcIk5vbmcgS2hhaVwiLFxuICAgIFwiVEguTk9cIjogXCJOb250aGFidXJpXCIsXG4gICAgXCJUSC5QVFwiOiBcIlBhdGh1bSBUaGFuaVwiLFxuICAgIFwiVEguUElcIjogXCJQYXR0YW5pXCIsXG4gICAgXCJUSC5QR1wiOiBcIlBoYW5nbmdhXCIsXG4gICAgXCJUSC5QTFwiOiBcIlBoYXR0aGFsdW5nXCIsXG4gICAgXCJUSC5QWVwiOiBcIlBoYXlhb1wiLFxuICAgIFwiVEguUEhcIjogXCJQaGV0Y2hhYnVuXCIsXG4gICAgXCJUSC5QRVwiOiBcIlBoZXRjaGFidXJpXCIsXG4gICAgXCJUSC5QQ1wiOiBcIlBoaWNoaXRcIixcbiAgICBcIlRILlBTXCI6IFwiUGhpdHNhbnVsb2tcIixcbiAgICBcIlRILlBSXCI6IFwiUGhyYWVcIixcbiAgICBcIlRILlBBXCI6IFwiUGhyYSBOYWtob24gU2kgQXl1dHRoYXlhXCIsXG4gICAgXCJUSC5QVVwiOiBcIlBodWtldFwiLFxuICAgIFwiVEguUEJcIjogXCJQcmFjaGluIEJ1cmlcIixcbiAgICBcIlRILlBLXCI6IFwiUHJhY2h1YXAgS2hpcmkgS2hhblwiLFxuICAgIFwiVEguUk5cIjogXCJSYW5vbmdcIixcbiAgICBcIlRILlJUXCI6IFwiUmF0Y2hhYnVyaVwiLFxuICAgIFwiVEguUllcIjogXCJSYXlvbmdcIixcbiAgICBcIlRILlJFXCI6IFwiUm9pIEV0XCIsXG4gICAgXCJUSC5TS1wiOiBcIlNhIEthZW9cIixcbiAgICBcIlRILlNOXCI6IFwiU2Frb24gTmFraG9uXCIsXG4gICAgXCJUSC5TUFwiOiBcIlNhbXV0IFByYWthblwiLFxuICAgIFwiVEguU1NcIjogXCJTYW11dCBTYWtob25cIixcbiAgICBcIlRILlNNXCI6IFwiU2FtdXQgU29uZ2tocmFtXCIsXG4gICAgXCJUSC5TUlwiOiBcIlNhcmFidXJpXCIsXG4gICAgXCJUSC5TQVwiOiBcIlNhdHVuXCIsXG4gICAgXCJUSC5TQlwiOiBcIlNpbmcgQnVyaVwiLFxuICAgIFwiVEguU0lcIjogXCJTaSBTYSBLZXRcIixcbiAgICBcIlRILlNHXCI6IFwiU29uZ2tobGFcIixcbiAgICBcIlRILlNPXCI6IFwiU3VraG90aGFpXCIsXG4gICAgXCJUSC5TSFwiOiBcIlN1cGhhbiBCdXJpXCIsXG4gICAgXCJUSC5TVFwiOiBcIlN1cmF0IFRoYW5pXCIsXG4gICAgXCJUSC5TVVwiOiBcIlN1cmluXCIsXG4gICAgXCJUSC5US1wiOiBcIlRha1wiLFxuICAgIFwiVEguVEdcIjogXCJUcmFuZ1wiLFxuICAgIFwiVEguVFRcIjogXCJUcmF0XCIsXG4gICAgXCJUSC5VUlwiOiBcIlVib24gUmF0Y2hhdGhhbmlcIixcbiAgICBcIlRILlVOXCI6IFwiVWRvbiBUaGFuaVwiLFxuICAgIFwiVEguVVRcIjogXCJVdGhhaSBUaGFuaVwiLFxuICAgIFwiVEguVURcIjogXCJVdHRhcmFkaXRcIixcbiAgICBcIlRILllMXCI6IFwiWWFsYVwiLFxuICAgIFwiVEguWVNcIjogXCJZYXNvdGhvblwiXG4gIH1cbn1cbiJdfQ==
