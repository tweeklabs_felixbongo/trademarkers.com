

( async () => {
  'use strict';

  // Create references to the main form and its submit button.
  const form = document.getElementById('payment-form');
  const submitButton = form.querySelector('button[type=submit]');
  /**
   * Setup Stripe Elements.
   */

  // Create a Stripe client.
  const stripe = Stripe("pk_live_XORR073H0R4crqE5ZWrtuPYi00Fwcl1bJS");

  const total = parseFloat( document.getElementById('total').innerHTML );
  const email = document.getElementById('static_email').value;

  const MAX_POLL_COUNT = 10000;
  let pollCount = 0;

  const alipay = await stripe.createSource({
    type: 'alipay',
    amount: total * 100,
    currency: 'usd',
    redirect: {
      return_url: 'https://dev.trademarkers.com/checkout?method=alipay',
    },
  });

  const wechat = await stripe.createSource({
    type: 'wechat',
    amount: total * 100,
    currency: 'usd',
    statement_descriptor: 'TradeMarkers',
    owner: {
      email: email,
    },
  });

  const qr = () => {

    if ( wechat !== undefined ) {

        const qrCode = new QRCode('wechat-qrcode', {
          text: wechat.source.wechat.qr_code_url,
          width: 128,
          height: 128,
          colorDark: '#424770',
          colorLight: '#f8fbfd',
          correctLevel: QRCode.CorrectLevel.H,
        });
    }
  };

  qr();

  console.log(wechat);

  const pollForSourceStatus = () => {

    stripe.retrieveSource({id: wechat.source.id, client_secret:wechat.source.client_secret }).then(({source}) => {
      if (source.status === 'chargeable') {
        // Make a request to your server to charge the Source.
        // Depending on the Charge status, show your customer the relevant message.

        document.getElementById('wechat-field').value = source.id;

        document.getElementById('wechat-qr').innerHTML = 'Processing payment ...';

        document.getElementById('payment-form').submit();

        console.log(source.id);
        
      } else if (source.status === 'pending' && pollCount < MAX_POLL_COUNT) {
        // Try again in a second, if the Source is still `pending`:
        pollCount += 1;
        setTimeout(pollForSourceStatus, 1000);
        console.log("Pending");
      } else {

        // Depending on the Source status, show your customer the relevant message.
        
        document.getElementById('wechat-field').value = "invalid";

        document.getElementById('wechat-qr').innerHTML = 'Processing payment ...';

        document.getElementById('payment-form').submit();
      }
    });
  };

  pollForSourceStatus();

  const ali_url = document.getElementById('alipay-url').innerHTML = alipay.source.redirect.url;

})();