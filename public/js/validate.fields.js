$ ( function () {

    var objFields = {

        'afghanistan' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : 'e.g. 1001'
        },

        'albania' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'algeria' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'american_samoa' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : true,
            'zip_label' : 'ZIP code',
            'zip_placeholder' : ''
        },

        'andorra' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'angola' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'anguilla' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'antigua_and_barbuda' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'argentina' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'State',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : 'e.g. B1921'
        },

        'armenia' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'aruba' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'austria' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : 'e.g. 3741'
        },

        'australia' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'State',
            'zip' : true,
            'zip_label' : 'Postcode',
            'zip_placeholder' : 'e.g. 2000'
        },

        'azerbaijan' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : 'e.g. K1A 0B1'
        },

        'bahamas' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Island',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : 'e.g. K1A 0B1'
        },

        'bahrain' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'bangladesh' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'barbados' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : 'e.g. K1A 0B1'
        },

        'belarus' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'belgium' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : 'e.g. 4000'
        },

        'belize' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'State',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'Benin' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'bermuda' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'bhutan' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'bolivia' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'bosnia_and_herzegovina' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'botswana' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'brazil' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'State',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : 'e.g. 10025-345'
        },

        'british_virgin_islands' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'brunei_darussalam' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'bulgaria' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'burkina_faso' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'burundi' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'cambodia' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'cameroon' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'canada' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : 'e.g. K1A 0B1'
        },

        'cape_verde' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Island',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'cayman_islands' : {
            'city' : false,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Island',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'central_african_republic' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'cape_verde' : {
            'city' : true,
            'state' : true,
            'state_label' : 'Island',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'chad' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'chile' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'State',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'china' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : 'e.g. 101200'
        },

        'colombia' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'comoros' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'costa_rica' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'croatia' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'Cyprus' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'czech_republic' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'denmark' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : 'e.g. 8660'
        },

        'Djibouti' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'dominica' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : '',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'dominican_republic' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'State',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'east_timor' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'State/Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'ecuador' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'State',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'egypt' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Governate',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'el_salvador' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'equatorial_guinea' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'eritrea' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'estonia' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'ethiopia' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'fiji' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'State',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'finland' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : 'e.g. 00550'
        },

        'france' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'State/Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'gabon' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'gambia' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'georgia' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'germany' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : 'e.g. 60322'
        },

        'ghana' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'gibraltar' : {
            'city' : false,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'greece' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : 'e.g. 151 24'
        },

        'grenada' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : 'e.g. 3911'
        },

        'guatemala' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'guernsey' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : 'e.g. GY1 1AA'
        },

        'guinea' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'guinea-bissau' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'guyana' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'haiti' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'honduras' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'hong_kong' : {
            'city' : true,
            'city_label' : 'District',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Area',
            'zip_placeholder' : ''
        },

        'hungary' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'iceland' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'india' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'State',
            'zip' : true,
            'zip_label' : 'PIN code',
            'zip_placeholder' : 'e.g. 110005'
        },

        'indonesia' : {
            'city' : true,
            'city_label' : 'City/regency',
            'state' : true,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'iran' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'iraq' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'ireland' : {
            'city' : true,
            'city_label' : 'Town/city',
            'state' : true,
            'state_label' : 'County',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'israel' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'italy' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'ivory_coast' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'jamaica' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Parish',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'japan' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Prefecture',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : 'e.g. 142-0062'
        },

        'jersey' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : 'e.g. JE2 2BT'
        },

        'jordan' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'State',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'kenya' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'kosovo' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'State',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'kurdistan_region' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'State',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'kuwait' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'State',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'kyrgyzstan' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'latvia' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'lebanon' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'lesotho' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'liberia' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'libya' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'liechtenstein' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : 'e.g. 9496'
        },

        'lithuania' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'luxembourg' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : 'e.g. 4750'
        },

        'macau' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'macedonia' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'madagascar' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'malawi' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'malaysia' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'maldives' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'mali' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'malta' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'mauritania' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'mauritius' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'Mexico' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'State',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : 'e.g. 03400'
        },

        'moldova' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'monaco' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'mongolia' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'montenegro' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'montserrat' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'morocco' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'mozambique' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'myanmar' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'namibia' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'nepal' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'netherlands' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'new_zealand' : {
            'city' : true,
            'city_label' : 'Town/city',
            'state' : true,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'nicaragua' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Department',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'niger' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'nigeria' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'State',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'norway' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : 'e.g. 0025'
        },

        'oman' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'State',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'pakistan' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'palestine' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'panama' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'papua_new_guinea' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'paraguay' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'peru' : {
            'city' : true,
            'city_label' : 'District',
            'state' : true,
            'state_label' : 'Region',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'philippines' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'poland' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : 'e.g. 00-950'
        },

        'portugal' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : 'e.g. 2725-079'
        },

        'puerto_rico' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'ZIP code',
            'zip_placeholder' : ''
        },

        'qatar' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'republic_of_the_congo' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'romania' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'russian_federation' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'rwanda' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'saint_kitts_and_nevis' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Island',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'saint_lucia' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'saint_vincent_and_the_grenadines' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'samoa' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'san_marino' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'sao_tome_and_principe' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'saudi_arabia' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'senegal' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'serbia' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'seychelles' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Island',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'sierra_leone' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'singapore' : {
            'city' : false,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : 'e.g. 238880'
        },

        'sint_maarten' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'slovakia' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'slovenia' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'solomon_islands' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'south_africa' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'south_korea' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'spain' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'sri_lanka' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'suriname' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Province',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'swaziland' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'sweden' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : 'e.g. 11455'
        },

        'switzerland' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : 'e.g. 2544'
        },

        'taiwan' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : 'e.g. 100'
        },

        'tajikistan' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'tanzania' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'thailand' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'togo' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'tonga' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'trinidad_and_tobago' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'tunisia' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'turkey' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'turkmenistan' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'turks_and_caicos_islands' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'uganda' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'ukraine' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'united_arab_emirates' : {
            'city' : false,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Emirate',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'united_kingdom' : {
            'city' : true,
            'city_label' : 'Town/city',
            'state' : true,
            'state_label' : 'County',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : 'e.g. SE1 0SU'
        },

        'united_states' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'State',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : 'e.g. 98103'
        },

        'uruguay' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'venezuela' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'State',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'vietnam' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'State',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'world_intellectual_property_organization' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'State/Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'yemen' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'zambia' : {
            'city' : true,
            'city_label' : 'City',
            'state' : true,
            'state_label' : 'Province',
            'zip' : true,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'zimbabwe' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        },

        'zanzibar' : {
            'city' : true,
            'city_label' : 'City',
            'state' : false,
            'state_label' : 'Province',
            'zip' : false,
            'zip_label' : 'Postal code',
            'zip_placeholder' : ''
        }

        
    };

    if ( $("#sub_country").length ) {
        var country = $("#sub_country").val().toLowerCase().replace(' ', '_');

        // console.log(country);
        constructForm(objFields[country]);
        $("#sub_country").change(function(){
            
            // validateFields($(this).val());
            country = $(this).val().toLowerCase().replace(' ', '_');
            constructForm(objFields[country]);
        });
    }

    function constructForm(entries){
        if (entries.city) {
            $("#city-block").show();
            $("#city-label").text(entries.city_label);
        } else {
            $("#city-block").hide();
        }

        if (entries.state) {
            $("#state-province-block").show();
            $("#state-label").text(entries.state_label);
        } else {
            $("#state-province-block").hide();
        }

        if (entries.zip) {
            $("#postal-block").show();
            $("#postal-label").text(entries.zip_label);
            $("#zip_code").prop('placeholder', entries.zip_placeholder);
        } else {
            $("#postal-block").hide();
        }
    }
    
});
