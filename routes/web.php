<?php

use Torann\LaravelMetaTags\Facades\MetaTag;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
// use App\Video;
// use App\Landing;
use Spatie\Honeypot\ProtectAgainstSpam;

/* Videos */
$router = app()->make('router');

// GET ALL VIDEO/LANDING PATHS
// $pathsVideo = Video::all();
// $pathsLanding = Landing::all();

// foreach ( $pathsVideo as $pathVideo ) {

//     $router->get( $pathVideo->slug, 'VideoController@videos' );
// }

// foreach ( $pathsLanding as $pathLanding ) {

//     $router->get( $pathLanding->slug, 'LandingController@landings' );
// }

/**
 * MAILGUN WEBHOOKS
 * SEE DOCS. 
 * https://documentation.mailgun.com/en/latest/api-webhooks.html#webhooks
 */
Route::get('/api/mandrill/delivered', 'WebhookController@delivered');
Route::post('/api/mandrill/delivered', 'WebhookController@delivered')->middleware('guest');
Route::get('/api/mandrill/opens', 'WebhookController@opens');
Route::post('/api/mandrill/opens', 'WebhookController@opens')->middleware('guest');


Route::get('/videos', 'VideoController@videolist');

// Route::get('/landings', 'LandingController@landinglist');
Route::get('/article', 'BlogController@blogList');
Route::get('/article/{slug}', 'BlogController@blogSlugShow');
// Route::get('/author/{id}', 'AuthorController@authorProfile');

/* Auth Routes */
Auth::routes(['verify' => true]);

// Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
// Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');
// Route::get('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');

/* Normal Routes */
Route::get('/', 'Trademarker@welcome');

Route::get('/{slug}', 'Trademarker@search_case_number')->where('slug', '(\w)(\w)-(\w)(\w)(\w)(\w)-(\w)');

/* Sitemap */

Route::get('/sitemap.xml', 'SitemapController@sitemap');

Route::get('/sitemaps', 'SitemapController@sitemaps');

Route::get('/sitemaps/{sitemap_name}', 'SitemapController@sitemap_file');

/* Trademark Search */

Route::get('/search', 'TrademarkSearchController@index');
Route::post('/search', 'TrademarkSearchController@index');

/*  Home */
Route::get('/customer/dashboard', 'HomeController@home')->name('dashboard')->middleware('verified');
Route::get('/home', 'HomeController@home')->name('home')->middleware('auth')->middleware('verified');

Route::get('/V1FFFFFFFFFF', 'Trademarker@merge_old_data_trademark_new');

Route::get('/algo', 'Trademarker@algorithm')->name('algorithm');

Route::post('/algo', 'Trademarker@algorithm');

Route::post('/algo1', 'Trademarker@algorithm');

Route::post('/algo2', 'Trademarker@algorithm');

Route::post('/algo3', 'Trademarker@algorithm');

Route::get('/webhooks/endpoint', 'Trademarker@webhooks');
Route::post(
    '/webhooks/endpoint',
    '\App\Http\Controllers\Webhook@handleSources'
);
/* Terms and Conditions */
Route::get('/terms', 'Trademarker@terms');

/* About Us */
Route::get('/about', 'Trademarker@about');

/* Privacy and Policy */
Route::get('/privacy', 'Trademarker@privacy');

/* Cookies Policy Page */
Route::get('/cookies', 'Trademarker@cookies');

/* Service Contract */
Route::get('/service_contract', 'Trademarker@service_contract');

Route::get('/what-is-trademark', 'Trademarker@what_is_trademark');
Route::get('/what-is-priority-claim', 'Trademarker@what_is_priority');
Route::get('/why-register-trademark', 'Trademarker@why_register');
Route::get('/offer', 'Trademarker@offer');

/* Contact Us */
Route::get('/contact', 'Trademarker@contact')->middleware(ProtectAgainstSpam::class);
Route::post('/contact', 'Trademarker@contact')->middleware(ProtectAgainstSpam::class);

/* Quote Us */
Route::get('/quote', 'Trademarker@quote');
Route::post('/quote', 'Trademarker@quote');

/* Classes */
Route::get('/classes', 'Trademarker@class');
Route::get('/class_descriptions', 'Trademarker@class_description');
Route::post('/class_descriptions', 'Trademarker@class_description');

/* Countries */
Route::get('/countries', 'Trademarker@countries');

/* Continent */
Route::get('/region/{continent}', 'Trademarker@region');

/* Prices */
Route::get('/prices', 'Trademarker@pricing');

/* Resources */
Route::get('/resources', 'Trademarker@resources');

/* Services */
Route::get('/services', 'Trademarker@services');

/* paypal */
Route::get('/paypal', 'Trademarker@paypal');

/* Reviews */
Route::get('/customer-reviews', 'ReviewController@index');
Route::post('/customer-reviews', 'ReviewController@index');

/* Case Number */
Route::post('/search/case', 'Trademarker@search_case_number');
Route::get('/search/case', 'Trademarker@search_case_number');

Route::post('/legal/case/add_cart', 'Action@legal_case_add');

Route::post('/legal/case/add_to_cart', 'Action@priority_add_cart')->middleware('auth');

Route::get('/legal/case/{case_number}', 'Action@verify_legal_case')->middleware('auth');

Route::post('/create/account', 'Action@create_account');
Route::get('/customer/create/password', 'Action@set_user_password')->middleware('verified');
Route::post('/customer/create/password', 'Action@set_user_password');

/* Account Information */
Route::get('/customer/profile/{id}', 'HomeController@get_profile')->middleware('auth');
Route::post('/customer/profile/{id}', 'HomeController@get_profile')->middleware('auth');

Route::get('/customer/create/profile/', 'HomeController@create_profile')->middleware('auth');
Route::post('/customer/create/profile/', 'HomeController@create_profile')->middleware('auth');

Route::get('/customer/create/profile/trademark/{id}', 'HomeController@create_profile_trademark')->middleware('auth');
Route::post('/customer/create/profile/trademark/{id}', 'HomeController@create_profile_trademark')->middleware('auth');

Route::get('/profiles', 'HomeController@profile')->name('profile')->middleware('auth');
Route::post('/profiles', 'HomeController@profile')->middleware('auth');

Route::get('/customer/orders/{order_id}', 'HomeController@orders')->middleware('verified');

Route::get('/customer/trademark/{trademark_id}' , 'HomeController@trademark')->name('trademark')->middleware('auth');

Route::get('/customer/trademarks', 'HomeController@trademarks')->middleware('auth');
Route::get('/customer/trademarks/registration', 'HomeController@trademark_registration')->middleware('auth');
Route::get('/customer/trademarks/study', 'HomeController@trademark_study')->middleware('auth');
Route::get('/customer/trademarks/monitoring', 'HomeController@trademark_monitoring')->middleware('auth');
Route::get('/customer/trademarks/others', 'HomeController@trademark_others')->middleware('auth');



/* Order Process Routes*/

/* Country */
Route::get('/trademark-registration-in-{country_name}', 'OrderProcess@country');

// CREATE 301 REDIRECT FOR THIS ROUTE SINCE THIS IS ALREADY READ FROM GOOGLE
// NOTE : THIS IS A PATH FROM THE OLD SYSTEM
Route::get('/country/{country_code}', 'Trademarker@countryCode');

/* Study Step 1 */
Route::get('/study/step1/{abbr}', 'OrderProcess@study_step1');

/* Study Step 2 */
Route::get('/study/step2/{abbr}', 'OrderProcess@study_step2');
Route::post('/study/step2/{abbr}', 'OrderProcess@study_step2');

/* Registration Step 1 */
Route::get('/registration/step1/{abbr}', 'OrderProcess@registration_step1');

/* Registration Step 2 */
Route::get('/registration/step2/{abbr}', 'OrderProcess@registration_step2');
Route::post('/registration/step2/{abbr}', 'OrderProcess@registration_step2');

/* Monitoring Service */
Route::get('/monitoring-service', 'Trademarker@monitoringService');

/* Monitoring */
// Route::get('/monitoring', 'OrderProcess@monitoring_step2');
// Route::post('/monitoring', 'OrderProcess@monitoring_step2');

/* Monitoring Service */
Route::get('/office-service', 'Trademarker@officeService');

Route::get('/service-order-form', 'OrderProcess@serviceOrderForm');
Route::post('/service-order-form', 'OrderProcess@serviceOrderForm');

/* Trademark Contact Information */
Route::get('/trademark_contact/{abbr}', 'OrderProcess@trademark_contact')->name('contact');
Route::post('/trademark_contact/{abbr}', 'OrderProcess@trademark_contact')->middleware('auth');

Route::get('/customer/profile/{id}/{abbr}', 'OrderProcess@validate_contact')->middleware('auth');
Route::post('/customer/profile/{id}/{abbr}', 'OrderProcess@validate_contact')->middleware('auth');

Route::get('/customer/create/profile/{abbr}', 'OrderProcess@create_contact')->middleware('auth');
Route::post('/customer/create/profile/{abbr}', 'OrderProcess@create_contact')->middleware('auth');

/* Order Form*/
Route::get('/order_form/{abbr}', 'OrderProcess@order_form')->name('order_form');
Route::post('/order_form/{abbr}', 'OrderProcess@order_form')->middleware('auth');

/* Cart Page */
Route::get('/cart', 'OrderProcess@cart')->name('cart')->middleware('auth');
Route::get('/cart/coupon', 'OrderProcess@addcoupon');

// AJAX
Route::get('/api/cart', 'CartController@getCart')->name('getCart')->middleware('auth');

/* Checkout Page */
Route::get('/checkout', 'OrderProcess@checkout')->name('checkout')->middleware('auth');
Route::post('/checkout', 'OrderProcess@checkout')->middleware('auth');

// Route::post('/checkout', 'PaypalController@AddActivity')->middleware('auth');
/* After Checkout */
Route::get('/order_received/{order_number}/{qty}/success=true&payment=paid', 'OrderProcess@after_checkout')->name('after_checkout')->middleware('verified');

/* Invoice */
Route::get('/customer/invoice/{invoice}/download=true&success=true', 'HomeController@download_invoice')->middleware('auth');
Route::get('/customer/invoice/{invoice}/download=false&success=true&view=true', 'HomeController@stream_invoice')->middleware('auth');
Route::get('/customer/invoices', 'HomeController@invoices')->middleware('auth');
Route::post('/customer/invoices', 'HomeController@invoices')->middleware('auth');

/**
 * AFFILIATES 
 */
Route::get('/customer/referral', 'ReferralController@index')->middleware('auth');
Route::post('/customer/referral', 'ReferralController@index')->middleware('auth');

Route::get('/customer/referral/withdraw', 'ReferralController@withdraw')->middleware('auth');
Route::post('/customer/referral/withdraw', 'ReferralController@withdraw')->middleware('auth');

/* Comment */
Route::post('/customer/comment/add_comment', 'HomeController@add_comment')->middleware('auth');

/* Attachment */
Route::post('/customer/attachment/add_attachment', 'HomeController@add_attachment')->middleware('auth');

Route::post('/customer/attachment/download', 'HomeController@download_attachment')->middleware('auth');

/* CMS */

Route::get('/Changes-in-Trademark-Regulations-in-Canada', 'CMSController@regulations_canada');
Route::get('/changes-in-trademark-regulations-in-canada', 'CMSController@regulations_canada');

Route::get('/Canada-changes-Trademark-Regulations', 'CMSController@regulations_canada_second');





/**
 * GET api to send email
 * this is to be updated in v2 with proper api group
 */
Route::get('/api/v1/sendCartMail/{cartId}', 'OrderProcess@sendCartMail');
Route::get('/api/v1/showCartMail/{cartId}', 'OrderProcess@showCartMail');
Route::post('/api/v1/sendEmailToMailChimp/', 'MailchimpController@addToList');
Route::get('/api/v1/sendEmailToMailChimp/{email}', 'MailchimpController@addToList2');
Route::get('/api/v1/getState/{ccode}', 'HomeController@getState');
Route::get('/api/v1/getProfile/{id}', 'HomeController@getProfile');
Route::get('/api/v1/generate-code/', 'OrderProcess@generateCode');

Route::get('/api/v1/event-mailer/', 'Trademarker@webApiEventMailer');



/** this route is very generic, possible this will override other path 
 * we add this at the bottom to that other path will be read first
 * 
 */
/* Case Number V2 */
// Route::get('/{caseNumber}', 'Trademarker@directCaseNumber'); 
Route::get( '/{videoSlug}', 'VideoController@videosV2' );

// autologin from abandon cart
Route::get('/abandon/{code}', 'Trademarker@loginVerify');
// user autologin link with hash password and get email
Route::get('/user-login/{pass}', 'Trademarker@loginUser');

// used for pop-up login
Route::post('/user/login', 'Trademarker@popLogin');

Route::get('/user/youtube-subscribe', 'Trademarker@youtubeSubscribe');



