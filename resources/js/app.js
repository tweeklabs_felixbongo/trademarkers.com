
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    el: '#app'
});


$(".navbar-toggler").click(function(){

    let hasToggle = $("#navbarSupportedContent").hasClass('show');
    console.log('hasToggle', hasToggle);
    if ( !hasToggle ) {
        $(".navbar-toggler .fas").removeClass('fa-bars');
        $(".navbar-toggler .fas").addClass('fa-times');
        // alert('asdasd');
        // this.find('.fas').removeClass('fa-bars').addClass('fa-times');
    } else {
        $(".navbar-toggler .fas").removeClass('fa-times');
        $(".navbar-toggler .fas").addClass('fa-bars');
    }

});


$("#tbl-cart-items").on("click", ".updateCart", function(){
    // alert($(this).val());
    loader = '<div style="padding: 40px;text-align: center;">Recalculating...</i></div>';
    $("#tbl-cart-items").html(loader);
    $.ajax({
        /* the route pointing to the post function */
        url: '/api/cart?id='+$(this).val(),
        type: 'GET',
        /* send the csrf-token and the input to the controller */
        // data: {_token: CSRF_TOKEN, message:$(".getinfo").val()},
        dataType: 'html',
        /* remind that 'data' is the response of the AjaxController */
        success: function (data) { 
            // alert('a');
            setTimeout(function(){
                $("#tbl-cart-items").html(data);

                removeMessageAjax();
            }, 1000);
            
        }
    }); 
});

$("#tbl-cart-items").on("click",'.btn-add-coupon', function(){
    // alert($(this).val());
    loader = '<div style="padding: 40px;text-align: center;">Adding Coupon...</i></div>';
    var coupon = $("#coupon-input").val();
    $("#tbl-cart-items").html(loader);
    

    $.ajax({
        /* the route pointing to the post function */
        url: '/cart?action=addCoupon&response=ajax&coupon='+coupon,
        type: 'GET',
        /* send the csrf-token and the input to the controller */
        // data: {_token: CSRF_TOKEN, message:$(".getinfo").val()},
        dataType: 'html',
        /* remind that 'data' is the response of the AjaxController */
        success: function (data) { 
            // alert('a');
            setTimeout(function(){
                $("#tbl-cart-items").html(data);

                removeMessageAjax();
            }, 1000);
            
        }
    }); 

    return false;
});

$("#tbl-cart-items").on("click",'.btn-remove-coupon', function(){
    // console.log($(this).attr('data-coupon'));
    // alert($(this).data('coupon-id'));
    loader = '<div style="padding: 40px;text-align: center;">Removing Coupon...</i></div>';
    // var coupon = $("#coupon-input").val();
    $("#tbl-cart-items").html(loader);
    let action = $(this).data('action-method');

    $.ajax({
        /* the route pointing to the post function */
        url: 'cart?action='+action+'&response=ajax&itemId='+$(this).data('coupon-id') ,
        type: 'GET',
        /* send the csrf-token and the input to the controller */
        // data: {_token: CSRF_TOKEN, message:$(".getinfo").val()},
        dataType: 'html',
        /* remind that 'data' is the response of the AjaxController */
        success: function (data) { 
            // alert('a');
            setTimeout(function(){
                $("#tbl-cart-items").html(data);

                removeMessageAjax();
            }, 1000);

            
            
        }
    }); 

    return false;
});

$("#tbl-cart-items").on("click",'.btn-remove-item', function(){
    // alert($(this).val());
    loader = '<div style="padding: 40px;text-align: center;">Recalculating...</i></div>';
    var item_id = $(this).data('item-id');

    $("#tbl-cart-items").html(loader);
    
    $.ajax({
        /* the route pointing to the post function */
        url: '/cart?id='+item_id+'&delete=true&response=ajax',
        type: 'GET',
        /* send the csrf-token and the input to the controller */
        // data: {_token: CSRF_TOKEN, message:$(".getinfo").val()},
        dataType: 'html',
        /* remind that 'data' is the response of the AjaxController */
        success: function (data) { 
            // alert('a');
            setTimeout(function(){
                $("#tbl-cart-items").html(data);

                removeMessageAjax();
            }, 1000);
            
        }
    }); 

    return false;
});

if ( $("#tbl-cart-items").length ) {
    loader = '<div style="padding: 40px;text-align: center;">Fetching Cart Items...</div>';
    $("#tbl-cart-items").html(loader);
    $.ajax({
        /* the route pointing to the post function */
        url: '/cart?action=addCoupon&response=ajax',
        type: 'GET',
        /* send the csrf-token and the input to the controller */
        // data: {_token: CSRF_TOKEN, message:$(".getinfo").val()},
        dataType: 'html',
        /* remind that 'data' is the response of the AjaxController */
        success: function (data) { 
            // alert('a');
            // setTimeout(function(){
            //     $("#tbl-cart-items").html(data);
            // }, 1000);
            $("#tbl-cart-items").html(data);
            
        }
    }); 
}

// REMOVE MESSAGE FROM AJAX
function removeMessageAjax() {
    if ( $("#ajax-message").length ) {

        setTimeout(function(){
            $("#ajax-message").remove()
        }, 3000);
    }
}

// MODAL POP UP

if ( $('#myModal').length ){
    $('#myModal').modal('show')

    $('#myModal').on('hidden.bs.modal', function (e) {
        $.get( '/api/v1/showCartMail/' + $(this).data('id'), function( data ) { console.log(data); });
        // alert($(this).data('id'));
    })
}

// HALLOWEEN PROMO BANNER POP-UP
if ( $('#pop-up-transparent').length ){
    if ( !getCookie('popup_banner') )  {
        setTimeout(function(){
            $('#pop-up-transparent').modal('show');
            setCookie('popup_banner', 'show', 4);
        }, 6000);
    }

}

// CHAT BANNER POP-UP
// if ( $('#pop-up-transparent-support').length ){
//     if ( !getCookie('popup_banner_support') )  {
//         setTimeout(function(){
//             $('#pop-up-transparent-support').modal('show');
//             setCookie('popup_banner_support', 'show', 1);
//         }, 10000);
//     }

//     $("#contact-support-banner").click(function(){
//         $('#pop-up-transparent-support').modal('toggle');
//     });

// }

// window.addEventListener("beforeunload", function (e) {
//     var confirmationMessage = 'It looks like you have been editing something. '
//                             + 'If you leave before saving, your changes will be lost.';

//     (e || window.event).returnValue = confirmationMessage; //Gecko + IE
//     e.returnValue = confirmationMessage;
//     this.console.log(e.tar);
//     return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
// });

function setCookie(key, value, expiry) {
    var expires = new Date();
    
    expires.setTime(expires.getTime() + (expiry * 60 * 60 * 1000));
    document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
    console.log(expires.toUTCString());
}

function getCookie(key) {
    var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
    return keyValue ? keyValue[2] : null;
}

function eraseCookie(key) {
    var keyValue = getCookie(key);
    setCookie(key, keyValue, '-1');
}

// POPUP YOUTUBE SUBSCRIBE
// setCookie('name', 'me', '1');
$("#popup-window").hide();

if ( !getCookie('yt-subscribe-banner') ) {
    setTimeout(function(){ 
        $("#popup-window").css('display', 'flex');
        $("#popup-window").show();
        $(".step1").show();
        $(".step2").hide();

        setCookie('yt-subscribe-banner', 'true', '12')

    }, 5000);
    
} else {
    $("#popup-window").hide();
}


// EMAIL POP OVER
$("#get-code").on("click",function(){
    let p_email = $("#popover-email").val();
    if ( !p_email ) {
        $(".email_error").show();
    } else if( !validateEmail(p_email) ) {
        $(".email_error").show();
    } else {
        // ajax send email to mailchimp
        $.ajax({
            type: 'GET',
            url: '/api/v1/sendEmailToMailChimp/'+p_email,
            dataType    : 'html',
            error       : function(err) { 
                $(".step1").hide();
                $(".step2").show();
            },
            success     : function(data) {
                // console.log(data);
                // if ( data == '400' ){
                //     $(".email_error").show();
                // } else {
                //     $(".step1").hide();
                //     $(".step2").show();
                    
                // }
                $(".step1").hide();
                $(".step2").show();
            }
        });
        
    }
});

$(".hide-pop-over").on("click", function(){
    $("#popup-window").hide();
})


function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test( $email );
}

if ( $('#paypal-button-container').length ) {

    
    let loader = '<div class="loader-wrap"><div class="loader-content">Processing Payment, Please Wait...</div></div>';
 
    var paypalAmount = $("#paypalAmount").val();
    var paypalDescription = $("#paypalDescription").val();
    
    paypal.Buttons({
        createOrder: function(data, actions) {
          return actions.order.create({
            purchase_units: [{
              amount: {
                value: paypalAmount
              },
              description: paypalDescription
            }]
          });
        },
        onApprove: function(data, actions) {

          // Capture the funds from the transaction
          $("#checkout").hide();
          $("#main").append(loader);

          return actions.order.capture().then(function(details) {

            $("#paypalId").val(data.orderID);
            $("#paypalPayerId").val(data.payerID);
            $("#frmTransaction").submit();

          });
        },

        style: {
            color:  'blue',
            shape:  'pill',
            label:  'pay',
            height: 40,
            layout: 'horizontal'
        },

        onError: function (err) {
            // alert('Sorry Something went wrong..');
            $(".loader-content").text("Sorry, Something wen't wrong, Please try again later");

            setTimeout(function(){
                location.reload();
            }, 1000);
        }
      }).render('#paypal-button-container');
}

$(".toggle-angle").click(function(){
    let icon = $(this).find('.fa');
    
    if ( icon.hasClass('fa-angle-down') ) {
        icon.removeClass('fa-angle-down').addClass('fa-angle-right');
    } else {
        icon.removeClass('fa-angle-right').addClass('fa-angle-down');
    }
});

// SIGNUP DISCOUNT ACTIONS


$("#tbl-cart-items").on("click",'#apply-signup-discount', function(){
    // alert($(this).val());
    loader = '<div style="padding: 40px;text-align: center;">Adding Coupon...</i></div>';
    // var coupon = $("#coupon-input").val();
    $("#tbl-cart-items").html(loader);
    

    $.ajax({
        /* the route pointing to the post function */
        url: '/cart?action=addSignupDiscount&response=ajax',
        type: 'GET',
        /* send the csrf-token and the input to the controller */
        // data: {_token: CSRF_TOKEN, message:$(".getinfo").val()},
        dataType: 'html',
        /* remind that 'data' is the response of the AjaxController */
        success: function (data) { 
            // alert('a');
            setTimeout(function(){
                $("#tbl-cart-items").html(data);

                removeMessageAjax();
            }, 1000);
            
        }
    }); 

    return false;
});

$("#tbl-cart-items").on("click",'#apply-discount', function(){
    // alert($(this).val());
    loader = '<div style="padding: 40px;text-align: center;">Adding Coupon...</i></div>';
    var coupon = $(this).attr('data-coupon-code');

    $("#tbl-cart-items").html(loader);
    

    $.ajax({
        /* the route pointing to the post function */
        url: '/cart?action=addCoupon&response=ajax&routeCoupon=true&coupon='+coupon,
        type: 'GET',
        /* send the csrf-token and the input to the controller */
        // data: {_token: CSRF_TOKEN, message:$(".getinfo").val()},
        dataType: 'html',
        /* remind that 'data' is the response of the AjaxController */
        success: function (data) { 
            // alert('a');
            setTimeout(function(){
                $("#tbl-cart-items").html(data);

                removeMessageAjax();
            }, 1000);
            
        }
    }); 

    return false;
});

$("#tbl-cart-items").on("click",'#cancel-signup-discount', function(){
    // alert($(this).val());
    loader = '<div style="padding: 40px;text-align: center;">Adding Coupon...</i></div>';
    // var coupon = $("#coupon-input").val();
    $("#tbl-cart-items").html(loader);
    

    $.ajax({
        /* the route pointing to the post function */
        url: '/cart?action=cancelSignupDiscount&response=ajax',
        type: 'GET',
        /* send the csrf-token and the input to the controller */
        // data: {_token: CSRF_TOKEN, message:$(".getinfo").val()},
        dataType: 'html',
        /* remind that 'data' is the response of the AjaxController */
        success: function (data) { 
            // alert('a');
            setTimeout(function(){
                $("#tbl-cart-items").html(data);

                removeMessageAjax();
            }, 1000);
            
        }
    }); 

    return false;
});
// $("#per_page_opt").change(function(){
//     alert();
// });



// $(document).on('click','#listClasses .btnRemoveClass',function(e) {
//         alert();
// });

// scrolling
$(window).scroll(function (event) {
    var scroll = $(window).scrollTop();
    // console.log(scroll);
    // Do something
    if (scroll > 290) {
        console.log('show');
        $("#topbar-scroller").addClass('sticky-nav');
        $(".back-to-top").addClass('show');

        $("#navbarSupportedContent").css("top","131px")
        
    } else {
        $("#topbar-scroller").removeClass('sticky-nav');
        $(".back-to-top").removeClass('show');

        $("#navbarSupportedContent").css("top",(179 - scroll ) + "px")
    }
}); 

$( document ).ready(function() {

    let windowWidth = $( window ).width();

    if ( windowWidth <= 768 ){
        $("#topbar #navbarSupportedContent").remove();
    }

    if ( $(".network-box-form") ){
        setTimeout(function(){
            $(".network-box-form").addClass('active-show');
        }, 1500);
        
    }

    $("#back-top").click(function(){
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });

    $("#addClassValue").change(function(){
        let addClassValue = $("#addClassValue").val();
        let varSelected = "class" + addClassValue;

        if (!addClassValue) {
            alert("Please Enter Class");
            return false;
        }

        if (addClassValue > 45 || addClassValue <= 0) {
            alert("Please Enter Class(es) 1 - 45");
            return false;
        }
        
        $("#"+varSelected).trigger("click");
        $("#addClassValue").val('');
        listPopulate();
        // alert(addClassValue);
    });

    $("#changeCountry").change(function(){
        // alert( $(this).val() );
        window.location.replace( $("#order-url").val() + "/" + $(this).val() );
     });

    $(document).on('click','#listClasses .btnRemoveClass',function(e) {
        let value = $(this).data('class-number');
        let classNumber = "class" + value;

        var this_class = $("."+classNumber);
        // console.log(this_class);
        $("#"+classNumber).trigger('click');

        $(".reset").removeClass('hide-class');

        listPopulate();
    });

    $(".reset").click(function(){
        location.reload();
        return false;
    })

    

    if ( $("#listClasses").length ) {
        listPopulate();
    }

    if ( $("#nature_block").length ) {
        let natureValue = $("#nature").val();
        let natureSubValue = $("#sub_nature");
        
        if (natureValue) {
            natureSubValue.val(natureValue);
            showCompanyBlock(natureSubValue.val());
        }

        natureSubValue.on('change',function(){
            showCompanyBlock(natureSubValue.val());
        });
    }

    if ( $(".btn-save-profile").length ) {

        $(".btn-save-profile").click(function(){

            if ( $("#nature").val() === 'Company' && $("#company").val() ) {
                $("#nationality").val('N/A');
            } else {
                $("#company").val('N/A');
            }
            // return true;
        });
    }
    

    if ( $("#sub_country").length ) {
        
        if ( $("#country").val() ) {
            validateFields($("#country").val());
        }
        

        $("#sub_country").change(function(){
            $("#state").html('');
            validateFields($(this).val());

        });
    }

    function validateFields(country)
    {
        if ( country == 'United States'){
            showProfileFields();
            // $("#street-block").hide();
            // $("#street").val('N/A');

        } else if ( country == 'Germany' ) {
            showProfileFields();
            // $("#street-block").hide();
            // $("#street").val('N/A');

            $("#state-province-block").hide();
            $("#state").val('N/A');

        } else {
            showProfileFields();
        }
    }

    function showProfileFields()
    {
        // $("#street-block").show();
        $("#state-province-block").show();

        // $("#street").val( ($("#street").val() == 'N/A' ? '' : $("#street").val()) );
        // $("#state").val( ($("#state").val() == 'N/A' ? '' : $("#state").val()) );
        // alert($("#sub_country").val());
        if ( $("#sub_country").val() ) {
            // call ajax to populate state
            $.ajax({
                type: 'GET',
                url: '/api/v1/getState/'+$("#sub_country").val(),
                dataType    : 'json',
                error       : function(err) { 
                    console.log('error');
                },
                success     : function(data) {
                    var stateOptions = "";
                    var hasData = false;
                    if (data) {
                        $.each( data ,function(i, e) {
                            hasData = true;

                            if ( $("#state").val() != e ) {
                                stateOptions += "<option value='"+e+"'>"+e+"</option>";
                            } else {
                                if (e != null) {
                                    stateOptions += "<option value='"+e+"' selected>"+e+"</option>";
                                }
                                
                            }
                        });
                    } else {
                        stateOptions += "<option value='N/A'>N/A</option>";
                    }

                    if (!hasData) {
                        stateOptions = "<option value='N/A'>N/A</option>";
                        $("#state-province-block").hide();
                    }
          
                    $("#state").html(stateOptions);
                }
            });
        }

        // remove important on postal if us or canada
        if ( $("#sub_country").val() == 'Canada' || $("#sub_country").val() == 'United States') {
            $("#postal-important").show();
            
            $("#zip_code").prop('required',true);
        } else {
            $("#postal-important").hide();
            $("#zip_code").prop('required',false);
        }
        
        // console.log(objFields);
    }

    function showCompanyBlock(value)
    {
        var holder = $("#company").parent().parent().find("b");

        // console.log(holder.text());

        if ( value == 'Individual' ) {

            $("#company").prop('disabled', true);
            $("#company").prop('required', false);
            holder.hide();

            $("#company").val("");

            $("#nature-label").find("h4").text("Client Information");

            $("#fax-block").show();
            $("#position-block").hide();

        } else {
            $("#nature-label").find("h4").text("Contact Person");
            $("#company").prop('disabled', false);
            $("#company").prop('required', true);
            holder.show();

            $("#position-block").show();
            $("#fax-block").hide();
        }
    }



    

    function listPopulate()
    {
        let liHeadDisplay='<tr><td style="background:#dee2e6;text-align:center" colspan="3"> <b>Class Information</b></td></tr><tr><td> <b>Class Number</b></td><td> <b>Description</b></td><td class="text-center"> <b>Action</b></td></tr>';
        let liDisplay='';
        $('input:checkbox.class_chk').each(function () {
            var sThisVal = (this.checked ? $(this).val() : "");
            // console.log(sThisVal);

            if ( sThisVal ) {
                
                console.log(sThisVal);
                let classNumber = sThisVal.replace("class","");

                let description_input = '<input id="class7" type="text" class="form-control" name="description[]" placeholder="Enter Goods/Services on this trademark" data-id="'+classNumber+'">';

                liDisplay += "<tr>" + 
                                "<td>" + classNumber + "</td>" +
                                "<td>" + (class_description ? class_description[classNumber] : description_input )+ "</td>" +
                                "<td class='text-center'> <i data-class-number='" +classNumber+"' class='fa fa-trash btnRemoveClass'></i>" + "</td>" +

                              "</tr>";

                              

                
            }

       });
       $("#listClasses").html(liHeadDisplay);
       $("#listClasses").append(liDisplay ? liDisplay : "<tr><td colspan='3' class='text-center'>No selected class. Please select class below.</td></tr>");

       let hasCampaign = $(".hasCampaign").attr('data-hasCampaign');
       let isPriority = $("#priority").attr('data-isPriority');

       if (hasCampaign) {
            $("#filed").val('yes');
            $("#filed").trigger('change');
       }

       if (isPriority) {
            $("#priority").val('yes');
            $("#priority").trigger('change');

            // select country
            // $("#origin").val($("#origin").attr('data-case-value')).trigger('change');
            // alert($("#date").attr('data-case-value'));
            $("#date").val($("#date").attr('data-case-value'));
            $("#tm").val($("#tm").attr('data-case-value'));
        }
    //    alert(priority);

    //    $(".btnRemoveClass").on("click", function(){
    //         let value = $(this).data('class-number');
    //         let classNumber = "class" + value;

    //         $("."+classNumber).trigger('click');


    //     });
    }

    // if ( class_description ) {
        
    // } 
    // alert();
    if ( $("#payment-buttons").length ) {
        $("#agree-policy").change(function(e){
            // alert( $("#agree-policy:checked").val() );

            if ( $("#agree-policy:checked").val() ) {
                $("#payment-buttons").show();
                $("#disabled-buttons-show").hide();
            } else {
                $("#payment-buttons").hide();
                $("#disabled-buttons-show").show();
            }
        });
    }


    // ADDED DEC 13
	$("#select-profile").change(function(){
		// call ajax to populate state
		$.ajax({
			type: 'GET',
			url: '/api/v1/getProfile/'+$(this).val(),
			dataType    : 'json',
			error       : function(err) { 
				console.log('error');
			},
			success     : function(data) {
				// console.log(data);
				if (data) {
					$("#profileId").val(data.id);
					$("#full_name").val(data.first_name + " " + data.last_name);
					$("#address").val(data.house);
					$("#city").val(data.city);
					$("#state").val(data.state);
					$("#country").val(data.country);
				}
			}
		});
    });

    $("#diff-profile").click(function(){
        
        if ( $("#diff-profile:checked").val() ) {
            $("#select-profile-wrap").show();
        } else {
            $("#select-profile-wrap").hide();
        }
    });

    // script for tab steps
    // $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

    //     var href = $(e.target).attr('href');
    //     var $curr = $(".process-model  a[href='" + href + "']").parent();

    //     $('.process-model li').removeClass();

    //     $curr.addClass("active");
    //     $curr.prevAll().addClass("visited");
    // });
// end  script for tab steps

    $("#yt-register").click(function(){

        step1Trigger();

        return false;

    });

    $("#yt-subscribe").click(function(){

        setCookie('step2','true','1');

        // SHOW MESSAGE
        $('#subscribe-notice-transparent').modal('show');
        setTimeout(function(){
            step2Trigger();
            $("#yt-link")[0].click();
        }, 3000);
        // setTimeout(function(){
        //     step2Trigger();
        //     $("#yt-link")[0].click()
        //     // window.location.replace('https://www.youtube.com/channel/UCFzecLVDoJLH3sX78Cu7cvg?sub_confirmation=1');
        // }, 3000);
        
        // DELAY WITH LOADER 
        // $.ajax({
        //     /* the route pointing to the post function */
        //     url: '/api/v1/generate-code/',
        //     type: 'GET',
        //     /* send the csrf-token and the input to the controller */
        //     // data: {_token: CSRF_TOKEN, message:$(".getinfo").val()},
        //     dataType: 'json',
        //     /* remind that 'data' is the response of the AjaxController */
        //     success: function (data) { 
                
        //         setCookie('stepCode',data.subscribe_promo,'1');
        //         $("#code").text(data.subscribe_promo);
        //         setTimeout(function(){
        //             step2Trigger();
        //             $("#yt-link")[0].click()
        //             // window.location.replace('https://www.youtube.com/channel/UCFzecLVDoJLH3sX78Cu7cvg?sub_confirmation=1');
        //         }, 3000);
                
        //     }
        // }); 

        

    });

    $("#btn-login-popup").click(function(){
        $('#subscribe-notice-transparent-login').modal('show');
    });

    $("#frm-form-login").submit(function(e){
        e.preventDefault();
        $.ajax({
            method: $(this).attr('method'),
            url: $(this).attr('action'),
            data: $(this).serialize(),
            dataType: "json"
        })
        .done(function(data) {
            // alert('suc');
            $('#subscribe-notice-transparent-login').modal('hide');
            $(location).attr("href", '/cart');
        })
        .fail(function(data) {
            console.log(data);
            alert('Unknown Email/Password');
        });

        return false;
    });

    $("#tbl-cart-items").on("click",'#apply-youtube-discount', function(){
        // alert($(this).val());
        loader = '<div style="padding: 40px;text-align: center;">Adding Coupon...</i></div>';
        // var coupon = $("#coupon-input").val();
        $("#tbl-cart-items").html(loader);
        
        $.ajax({
            /* the route pointing to the post function */
            url: '/cart?action=addCouponUser&response=ajax',
            type: 'GET',
            /* send the csrf-token and the input to the controller */
            // data: {_token: CSRF_TOKEN, message:$(".getinfo").val()},
            dataType: 'html',
            /* remind that 'data' is the response of the AjaxController */
            success: function (data) { 
                // alert('a');
                setTimeout(function(){
                    $("#tbl-cart-items").html(data);

                    removeMessageAjax();
                }, 1000);
                
            }
        }); 

        return false;
    });


    



    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
          var c = ca[i];
          while (c.charAt(0) == ' ') {
            c = c.substring(1);
          }
          if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
          }
        }
        return "";
    }

    function step1Trigger(){
        $("#step-1").html('<i class="fa fa-check"></i>');
        $("#step-1-line").removeClass('active');
        $("#step-1-line").addClass('visited');

        $("#step-2-line").removeClass('initial');
        $("#step-2-line").addClass('active');

        $("#step-2").addClass('active');

        $("#yt-register").attr("disabled","disabled");
        
        $("#step-content-2").removeClass('disabled');

        $("#yt-subscribe").prop("disabled", false);
    }

    function step2Trigger(){

        $("#step-2").html('<i class="fa fa-check"></i>');
        $("#step-2-line").removeClass('active');
        $("#step-2-line").addClass('visited');

        // $("#step-3-line").removeClass('initial');
        // $("#step-2-line").addClass('active');

        $("#step-3").addClass('active');

        $("#yt-subscribe").attr("disabled","disabled");
        
        // $("#step-content-2").removeClass('disabled');

        $("#yt-subscribe").prop("disabled", true);
    }

    $(".code-claim-block").hide();

    if (getCookie('step1') == 'true') {

        step1Trigger();

    }

    if (getCookie('step2') == 'true') {
        step1Trigger();
        step2Trigger();
        if (getCookie('stepCode')){
            $("#code").text(getCookie('stepCode'));
        }
        
        $(".code-claim-block").show();
    } else {

        if ( $("#user-subscribe").val() == 'yes' ) {
            step1Trigger();
            step2Trigger();
            $(".code-claim-block").show();
        } 
        
    }

    $(".scroll-container-a a").click(function(){
        // alert(this.attr('target'));
        
        var target = $(this.hash);

        if (target.length) {
            $('html,body').animate({
                scrollTop: target.offset().top - 100
            }, 1000);
            return false;
        }
    });

    $("#quoteType").change(function(){

        window.location = replaceUrlParam(window.location.href, "getQuoteType", this.value);
        // checkFieldShow( $(this).val() );
    });

    if ( $("#quoteType").length ) {
        // alert($("#quoteType").val());
        checkFieldShow( $("#quoteType").val() );
    }

    function checkFieldShow($type) {
        if ( $type == 'oar' 
            || $type == 'afr'
            || $type == 'sou'
            || $type == 'raa'
            || $type == 'tmo'
        ) {
            // show showNeeded
            $('.showNeeded').show();

            // return true;
        } else {
            $('.showNeeded').hide();
            // return false;
        }

        if ($type == 'sou') {
            $(".showRegistration").show();
        } else {
            $(".showRegistration").hide();
        }

        if ($type == 'ita') {
            $(".showCountryDesignate").show();
            $(".showClass").show();
            // $(".showCountry").hide();
        } else {
            $(".showCountryDesignate").hide();
            $(".showClass").hide();
            // $(".showCountry").show();
        }

        if ($type == 'tmo' || $type == 'lopd' || $type == 'lopt') {
            $(".showOppo").show();
        } else {
            $(".showOppo").hide();
        }

        
    }

    function replaceUrlParam(url, paramName, paramValue)
    {
        var pattern = new RegExp('\\b(' + paramName + '=).*?(&|$)')
        if (url.search(pattern) >= 0)
        {
            return url.replace(pattern, '$1' + paramValue + '$2');
        }
        return url + (url.indexOf('?') > 0 ? '&' : '?') + paramName + '=' + paramValue
    }
    
});
