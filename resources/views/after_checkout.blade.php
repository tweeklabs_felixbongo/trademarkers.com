@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">

                	@if ( session('payment_message') )
                        <div style="text-align: center;" class="alert alert-info" role="alert">
                            {{ session('payment_message') }}
                        </div>
                    @endif

                    <table class="table table-bordered-borderless">
                        
                        <thead>
                            <tr>
                                <th> 
                                    Order Number: <strong style="color:red;"> {{ $order->order_number }} </strong>
                                </th>
                                <th> 
                                    Date: <strong>{{ date("F j, Y", strtotime( $order['created_at'] ) ) }}</strong>
                                </th>
                                <th> 
                                    Total: <strong>${{ number_format( $order['total_amount'],2) }}</strong>
                                </th>
                                <th> 
                                    <a href="/customer/orders/{{ $order->order_number }}"> <i class="fa fa-eye"></i> View Order</a>

                                </th>
                                <th> 
                                    <a href="/customer/invoice/{{ $invoice->id }}/download=true&success=true"> <i class="fa fa-download"></i> Download Invoice</a>
                                </th>
                            </tr>
                        </thead>
                    </table>

                	<hr>

                    <p> <b>Order Details</b></p>

                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td style="color:red;"> Item #</td>
                                <td> Country</td>
                                <td> Service</td>
                                <td> Type</td>
                                <td> Classes</td>
                                <td> Amount</td>
                            </tr>
                        </thead>

                        <tbody>
                        	@php
	                            $count = 1;
                                $products = [];
                                $dataLayer = [
                                    'ecommerce' => [
                                        'purchase' => [
                                            'actionField' => [
                                                'id' => $order->order_number,
                                                'revenue' => $order->total_amount
                                            ]
                                        ]
                                    ]
                                ];
                                
	                        @endphp
	                        @foreach( $contents as $content )
                                

                                    @php
                                   
                                        $product = [
                                            'name' => $content->type,
                                            'id' => 'sku-'.$content->type,
                                            'price' => $content->amount,
                                            'category' => $content->service,
                                            'quantity' => '1',
                                            'coupon' => 'N/A'
                                        ];

                                    @endphp
                                    <tr>
                                        <td> <b>{{ str_pad( $count , 3, '0', STR_PAD_LEFT )  }}</b></td>

                                        @if ( $content->country )
                                            <td> <img src="{{ asset('images/' . $content->country->avatar) }}" /> {{ $content->country->name }}</td>
                                        @else
                                            <td> N/A</td>
                                        @endif
                                        
                                        <td> {{ $content['service'] }}</td>
                                        <td> {{ $content['type'] }}</td>
                                        <td> {{ $content['classes'] }}</td>
                                        <td> ${{ number_format($content['amount'],2) }}</td>
                                    </tr>

                                    @if ( $content->coupons ) 

                                        @foreach( $content->coupons as $couponCode )
                                            <tr style="color:#f00;font-style:italic;">
                                                <td colspan="4"></td>
                                                <td>Coupon : {{ $couponCode->code->action_code->case_number }}</td>
                                                <td> - ${{ $content['amount'] * ($couponCode->code->discount / 100) }}</td>
                                                
                                            </tr>
                                            @php
                                                $total -=  $content['amount'] * ($couponCode->code->discount / 100);
                                                $product['coupon'] = $couponCode->code->action_code->case_number;
                                            @endphp
                                        @endforeach
                                        
                                    @endif

                                    @if ( $content->total_discount > 0 && !$content->coupons ) 

                                        <tr style="color:#f00;font-style:italic;">
                                            <td colspan="4"></td>
                                            <td>Discount</td>
                                            <td> - ${{ $content->total_discount }}</td>
                                            
                                        </tr>
                                        @php
                                            $total -=  $content->total_discount;
                                        @endphp
                                        
                                    @endif

                                    @php
                                        $count++;
                                        array_push($products, $product);
                                    @endphp
                                
                            @endforeach

                            @php
                                $dataLayer['ecommerce']['purchase']['products'] = $products;
                                
                            @endphp

	                        <tr>
	                        	<td colspan="4"></td>
	                            <td><b>Overall Total</b></td>
	                            <td style="color: red;">${{ number_format( $total, 2 ) }}</td>
	                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection



@section('gtag-datalayer')

<script>

    dataLayerTemp = <?php echo json_encode( $dataLayer ); ?>;

</script>

@endsection



