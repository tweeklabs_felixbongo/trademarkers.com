@if($cookieConsentConfig['enabled'] && ! $alreadyConsentedWithCookies)

    @include('cookieConsent::dialogContents')

    <script>

        window.laravelCookieConsent = (function () {

            const COOKIE_VALUE = 1;
            const COOKIE_DOMAIN = '{{ config('session.domain') ?? request()->getHost() }}';

            function consentWithCookies() {
                setCookie('{{ $cookieConsentConfig['cookie_name'] }}', COOKIE_VALUE, {{ $cookieConsentConfig['cookie_lifetime'] }});
                hideCookieDialog();
            }

            function cookieExists(name) {
                return (document.cookie.split('; ').indexOf(name + '=' + COOKIE_VALUE) !== -1);
            }

            function hideCookieDialog() {
                const dialogs   = document.getElementsByClassName('js-cookie-consent');
                const container = document.getElementById('cookie-trademark');
                const mobile    = document.getElementById('cookie-trademark-mobile');
                const app       = document.getElementById('app');
                const footer    = document.getElementById('footer');

                for (let i = 0; i < dialogs.length; ++i) {
                    dialogs[i].style.display = 'none';
                }

                if ( mobile ) {

                    mobile.remove();
                }

                if ( container ) {

                    container.remove();
                }
            }

            function setCookie(name, value, expirationInDays) {
                const date = new Date();
                date.setTime(date.getTime() + (expirationInDays * 24 * 60 * 60 * 1000));
                document.cookie = name + '=' + value
                    + ';expires=' + date.toUTCString()
                    + ';domain=' + COOKIE_DOMAIN
                    + ';path=/{{ config('session.secure') ? ';secure' : null }}';
            }

            if (cookieExists('{{ $cookieConsentConfig['cookie_name'] }}')) {
                hideCookieDialog();
            }

            const buttons = document.getElementsByClassName('js-cookie-consent-agree');

            for (let i = 0; i < buttons.length; ++i) {
                buttons[i].addEventListener('click', consentWithCookies);
            }

            return {
                consentWithCookies: consentWithCookies,
                hideCookieDialog: hideCookieDialog
            };
        })();
    </script>

@else
    
    <script>
        window.laravelCookieConsent = (function () {
            const container = document.getElementById('cookie-trademark');
            const mobile    = document.getElementById('cookie-trademark-mobile');
            const app       = document.getElementById('app');
            const footer    = document.getElementById('footer');

            if ( mobile ) {

                mobile.remove();
            }

            if ( container ) {

                container.remove();
            }
        })();
    </script>

@endif
