<div class="js-cookie-consent cookie-consent">

	<p>
	    <span class="cookie-consent__message">
	        
	        This website uses cookies. By using our website, you agree to our <a href="/cookies" target="_blank">Cookie Policy</a>
	    </span>

	    <button class="btn btn-danger js-cookie-consent-agree cookie-consent__agree">
	        Ok, understood.
	    </button>
	</p>

</div>
