@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                
                @if(Session::has('quoteMessage'))
                    <p style="text-align:center" class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('quoteMessage') }}</p>
                @endif
                    @if ( strlen( $message ) > 0 )
                        <div style="text-align: center;" class="alert alert-info" role="alert">
                            {{ $message }}
                        </div>
                    @endif

                    <h2>Request for Quotation</h2>
                    <form method="POST" enctype="multipart/form-data">
                    <input type="hidden" value="submit" name="action">
                    <div class="row">
                        @csrf
                        <br>
                        <div class="form-group col-md-12">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="quoteType">Quote Type</label>
                                </div>
                                <select class="custom-select" id="quoteType" name="quoteType">
                                    <!-- <option value="word" selected>Trademark Study</option>
                                    <option value="logo">Trademark Registration</option> -->
                                    <option value="oar" {{ $type=='oar'? 'selected':'' }}>Office Action Response</option>  
                                    <option value="afr" {{ $type=='afr'? 'selected':'' }}>Appeal a Final Refusal</option>  
                                    <option value="sou" {{ $type=='sou'? 'selected':'' }}>Trademark Statement of Use</option>  
                                    <option value="raa" {{ $type=='raa'? 'selected':'' }}>Revive an Abandoned Application</option>  
                                    <option value="ita" {{ $type=='ita'? 'selected':'' }}>International Trademark Application</option>  
                                    <option value="tr" {{ $type=='tr'? 'selected':'' }}>Trademark Renewal</option>  
                                    <option value="cut" {{ $type=='cut'? 'selected':'' }}>Change or Update Trademark Owner</option>  
                                    <option value="tm" {{ $type=='tm'? 'selected':'' }}>Trademark Monitoring</option>  
                                    <option value="lopd" {{ $type=='lopd'? 'selected':'' }}>Letter of Protest for Domain Owners</option>  
                                    <option value="lopt" {{ $type=='lopt'? 'selected':'' }}>Letter of Protest for Prior Registered TM Holders</option>  
                                    <option value="tmo" {{ $type=='tmo'? 'selected':'' }}>Trademark Opposition</option>  
                                    <option value="nas" {{ $type=='nas'? 'selected':'' }}>Negotiate a Settlement</option>  
                                    <option value="cad" {{ $type=='cad'? 'selected':'' }}>Cease and Desist</option>  
                                    <option value="dsa" {{ $type=='dsa'? 'selected':'' }}>Draft a Settlement Agreement</option>  
                                </select>
                            </div>
                            @if ($errors->has('trademarkType'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('trademarkType') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group col-md-12">
                            @php 
                                $applicantName          = '';
                                $trademarkName          = '';
                                $trademarkNumber        = '';
                                $trademarkClass         = '';
                                $trademarkFilingDate    = '';
                                $opTrademarkName        = '';
                                $opTrademarkNumber      = '';
                                $countryCode = '';

                                $trademarkNameLabel          = 'Trademark Name';
                                $trademarkNumberLabel        = 'Trademark Number';
                                $trademarkFilingDateLabel    = 'Trademark Filling Date';
                                $opTrademarkNameLabel        = 'Opposing Trademark Name';
                                $opTrademarkNumberLabel      = 'Opposing Trademark Number';

                                if ( $type == 'tmo' ) {
                                    $trademarkNameLabel          = 'Opposing Trademark Name';
                                    $trademarkNumberLabel        = 'Opposing Trademark Number';
                                    $trademarkFilingDateLabel    = 'Opposing Trademark Filling Date';
                                    $opTrademarkNameLabel        = 'Defending Trademark Name';
                                    $opTrademarkNumberLabel      = 'Defending Trademark Number';
                                }

                                
                                
                                if ( $formFields['domain'] ) {  
                                    $firstName = $formFields['domain']->first_name ? $formFields['domain']->first_name : '';
                                    $lastName = $formFields['domain']->last_name ? $formFields['domain']->last_name : '';
                                }

                                if ( isset($firstName) || isset($lastName) ) {
                                    $applicantName = $firstName .' '. $lastName;
                                } else {
                                    $applicantName = '';
                                }

                                if ( isset($formFields['event']->caseOwned) ) {
                                    $applicantName = $formFields['event']->caseOwned->tm_holder;
                                    $trademarkName = $formFields['event']->caseOwned->trademark;
                                    $trademarkNumber = $formFields['event']->caseOwned->number;
                                    $trademarkFilingDate = $formFields['event']->caseOwned->tm_filing_date;
                                    $trademarkClass = $formFields['event']->caseOwned->class_number;
                                }

                                if ( isset($formFields['event']->country) ) {
                                    $countryCode = $formFields['event']->country->abbr;
                                }

                                if ( isset($formFields['event']->case) ) {
                                    $opTrademarkName = $formFields['event']->case->trademark;
                                    $opTrademarkNumber = $formFields['event']->case->number;
                                }
                                
                              
                            @endphp
                            <label for="name"><b>Name:</b></label>
                            <input type="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" value="{{ $applicantName ? $applicantName : old('name') }}"  placeholder="Enter name" name="name" require>

                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                        
                        <div class="form-group col-md-6">
                            <label for="email"><b>Email:</b></label>
                            <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ $formFields['email'] ? $formFields['email'] : old('email') }}" id="email" placeholder="Enter email" name="email" require>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col-md-6">
                            <label for="phone"><b>Phone:</b></label>
                            <input type="text"  class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" value="{{ old('phone') }}" id="phone" placeholder="Enter phone" name="phone" require>

                            @if ($errors->has('phone'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group col-md-12">
                            <label for="trademarkName"><b>{{ $trademarkNameLabel }}:</b></label>
                            <input type="text" class="form-control{{ $errors->has('trademarkName') ? ' is-invalid' : '' }}" id="trademarkName" value="{{ $trademarkName ? $trademarkName : old('trademarkName') }}"  placeholder="Enter {{ $trademarkNameLabel }}" name="trademarkName">

                            @if ($errors->has('trademarkName'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('trademarkName') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group showNeeded col-md-12">
                            <label for="trademarkNumber"><b>{{ $trademarkNumberLabel }}:</b></label>
                            <input type="text" class="form-control{{ $errors->has('trademarkNumber') ? ' is-invalid' : '' }}" id="trademarkNumber" value="{{ $trademarkNumber ? $trademarkNumber : old('trademarkNumber') }}"  placeholder="Enter {{ $trademarkNumberLabel }}" name="trademarkNumber">

                            @if ($errors->has('trademarkNumber'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('trademarkNumber') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group showNeeded showClass col-md-12">
                            <label for="trademarkClass"><b>Trademark Class(es):</b></label>
                            <input type="text" class="form-control{{ $errors->has('trademarkClass') ? ' is-invalid' : '' }}" id="trademarkClass" value="{{ $trademarkClass ? $trademarkClass : old('trademarkClass') }}"  placeholder="Enter Trademark Class" name="trademarkClass">
                            @if ($errors->has('trademarkClass'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('trademarkClass') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group showNeeded col-md-12">
                            <label for="trademarkFillingDate"><b>{{ $trademarkFilingDateLabel }}:</b></label>
                            <input type="date" class="form-control{{ $errors->has('trademarkFillingDate') ? ' is-invalid' : '' }}" id="trademarkFillingDate" value="{{ $trademarkFilingDate ? date('Y-m-d', strtotime($trademarkFilingDate)) : old('trademarkFilingDate') }}"  placeholder="Enter {{ $trademarkFilingDateLabel }}" name="trademarkFillingDate">
                            @if ($errors->has('trademarkFillingDate'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('trademarkFillingDate') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group showNeeded showRegistration col-md-12">
                            <label for="trademarkRegistrationDate"><b>Trademark Registration Date:</b></label>
                            <input type="date" class="form-control{{ $errors->has('trademarkRegistrationDate') ? ' is-invalid' : '' }}" id="trademarkRegistrationDate" value="{{ old('trademarkRegistrationDate') }}"  placeholder="Enter Trademark Registration Date" name="trademarkRegistrationDate">
                            @if ($errors->has('trademarkRegistrationDate'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('trademarkRegistrationDate') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group col-md-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="trademarkType">Trademark Type</label>
                                </div>
                                <select class="custom-select" id="trademarkType" name="trademarkType">
                                    <option value="Word-Only" selected> Word-Only</option>
                                    <option value="Design-Only or Stylized Word-Only (Figurative)"> Design-Only or Stylized Word-Only (Figurative)</option>
                                    <option value="Combined Word and Design"> Combined Word and Design </option>  
                                </select>
                            </div>
                            @if ($errors->has('trademarkType'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('trademarkType') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group showCountry col-md-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="trademarkCountry">{{ ($type=='ita' ? 'Country Of Basic Mark' : 'Country') }}</label>
                                </div>
                                <select class="custom-select" id="trademarkCountry" name="trademarkCountry">
                                    <option>Select Country</option>
                                    @foreach( $countries as $iCountry)
                                        <option value="{{$iCountry->name}}" data-tokens="{{$iCountry->abbr}} - {{$iCountry->name}}" {{$countryCode == $iCountry->abbr ? 'selected' : ''}}>{{$iCountry->abbr}} - {{$iCountry->name}}</option>
                                    @endforeach

                                </select>
                            </div>
                            @if ($errors->has('trademarkCountry'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('trademarkCountry') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group showCountryDesignate col-md-12">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="countryDesignate">Countries to Designate</label>
                                </div>
                                <select class="custom-select" id="countryDesignate" name="countryDesignate[]" multiple>
                                    @foreach( $countryMadrid as $madridCountry)
                                        <option value="{{$madridCountry->name}}" data-tokens="{{$madridCountry->abbr}} - {{$madridCountry->name}}">{{$madridCountry->abbr}} - {{$madridCountry->name}}</option>
                                    @endforeach

                                </select>
                            </div>
                            @if ($errors->has('countryDesignate'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('countryDesignate') }}</strong>
                                </span>
                            @endif
                        </div>

                        <hr class="showOppo">

                        <div class="form-group showOppo col-md-12">
                            <label for="opposingTrademarkName"><b>{{ $opTrademarkNameLabel }}:</b></label>
                            <input type="text" class="form-control{{ $errors->has('opposingTrademarkName') ? ' is-invalid' : '' }}" id="opposingTrademarkName" value="{{ $opTrademarkName ? $opTrademarkName : old('opTrademarkName') }}"  placeholder="Enter {{ $opTrademarkNameLabel }}" name="opposingTrademarkName">
                            @if ($errors->has('opposingTrademarkName'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('opposingTrademarkName') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group showOppo col-md-12">
                            <label for="opposingTrademarkNumber"><b>{{ $opTrademarkNumberLabel }}:</b></label>
                            <input type="text" class="form-control{{ $errors->has('opposingTrademarkNumber') ? ' is-invalid' : '' }}" id="opposingTrademarkNumber" value="{{ $opTrademarkNumber ? $opTrademarkNumber : old('opTrademarkNumber') }}"  placeholder="Enter {{ $opTrademarkNumberLabel }}" name="opposingTrademarkNumber">
                            @if ($errors->has('opposingTrademarkNumber'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('opposingTrademarkNumber') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group col-md-12">
                            <label for="additionalDetails"><b>Additional Details</b></label>
                            <textarea class="form-control{{ $errors->has('additionalDetails') ? ' is-invalid' : '' }}" value="{{ old('additionalDetails') }}" rows="7" name="additionalDetails" ></textarea>

                            @if ($errors->has('message'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('additionalDetails') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group col-md-12" >
                            <label for="additionalDetails"><b>Upload supporting documents</b></label>
                            <input type="file" name="attachments[]" class="form-control" multiple>
                        </div>

                        <div class="form-group col-md-12" style="text-align:right;margin-top:30px">
                            <button type="submit" class="btn btn-danger btn-lg">Submit Quote Form</button>
                        </div>

                        
                        @honeypot
                    </form>

                    </div>
                    
                    <br>
                    <br>
                    <hr>

                    <div class="row">
                        <div class="col-md-8">
                            <h6> <b>Trademarkers LLC</b></h6>
                            <h6> 45 Essex Street, Suite 202</h6>
                            <h6> Millburn NJ 07041</h6>
                        </div>

                        <div class="col-md-4" style="float: right;">
                            <h6> <b>Email Us</b></h6>
                            <h6> info@trademarkers.com</h6>
                            <h6> <b>Toll-Free:</b> 1-800-293-3166</h6>
                            <h6> <b>Office:</b> 212-468-5500</h6> 
                            <h6> <b>Fax:</b> 212-504-0888</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
