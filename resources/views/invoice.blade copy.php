<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Invoice</title>

  
        <style type="text/css">
            .clearfix:after {
              content: "";
              display: table;
              clear: both;
            }

            .left{
              float: left;
              font-weight: bold;
              font-size: 13px;
            }

            .right{
              float: right;
              font-size: 13px;
            }

            a {
              color: #5D6975;
              text-decoration: underline;
            }

            body {
              position: relative;
              height: 29.7cm; 
              margin: 0 auto; 
              color: #001028;
              background: #FFFFFF; 
              font-family: 'Calibri', sans-serif;
              font-size: 12px; 
              color: #2c2b26;
              /* font-family: Arial; */
            }

            header {
              padding: 10px 0;
              margin-bottom: 30px;
            }

            #logo {
              /* text-align: center; */
              padding-top: 40px;
              margin-bottom: 10px;
              float: left;
            }

            h1 {
              border-top: 1px solid  #5D6975;
              border-bottom: 1px solid  #5D6975;
              color: #5D6975;
              font-size: 2.4em;
              line-height: 1.4em;
              font-weight: normal;
              text-align: center;
              margin: 0 0 20px 0;
              background: url(../images/invoice/dimension.png);
            }

            #project {
              float: left;
            }

            #project span {
              color: #5D6975;
              text-align: right;
              width: 52px;
              margin-right: 10px;
              display: inline-block;
              font-size: 0.8em;
            }

            #company {
              float: right;
              text-align: right;
            }

            #project div,
            #company div {
              white-space: nowrap;        
            }

            #infotable .info {
              text-align: right;
              padding-left: 4px;
            }

            #maintable {
              width: 100%;
              border-collapse: collapse;
              border-spacing: 0;
              margin-bottom: 20px;
            }

            #maintable tr:nth-child(2n-1) td {
              background: #F5F5F5;
            }

            #maintable th,
            #maintable td {
              text-align: center;
            }

            #maintable th {
              padding: 5px 20px;
              color: #5D6975;
              border-bottom: 1px solid #C1CED9;
              white-space: nowrap;        
              font-weight: normal;
            }

            #maintable .service,
            #maintable .desc {
              text-align: left;
            }

            #maintable td {
              padding: 20px;
              text-align: right;
            }

            #maintable td.service,
            #maintable td.desc {
              vertical-align: top;
            }

            #maintable td.unit,
            #maintable td.qty,
            #maintable td.total {
              font-size: 1.2em;
            }

            #maintable td.grand {
              border-top: 1px solid #5D6975;;
            }

            #notices .notice {
              color: #5D6975;
              font-size: 1.2em;
            }

            footer {
              color: #5D6975;
              width: 100%;
              height: 30px;
              position: absolute;
              bottom: 0;
              border-top: 1px solid #C1CED9;
              padding: 8px 0;
            }

            

            .top-right-ico{
              position: absolute;
              top:-50px;
              right:25%;
            }

            .bill-to-container{
              width:50%;
            }
            .bill-to-label{
              width:25%;
              float: left;
            }
            .bill-to-details{
              width:75%;
              float: right;
            }

            .bill-to-details p{
              font-size: 10px;
              color: #808080;
              line-height: 10px;
            }

            .bill-to{
              font-size : 16px; color: #2c2b26;
              line-height:12px;
            }
            .bill-to-details p.bill-to{
              font-size : 16px; color: #2c2b26;
              line-height:12px;
              margin-bottom:0px;
            }
        </style>
    </head>
    <body>
        <header class="clearfix">

            <img class="top-right-ico" src="images/pdf-icon.png" width="30%">

            <div id="logo">
                <img src="images/trademarkers.png" width="30%">
            </div>

            <!-- <div style="margin-top: 140px;" class="clearfix">
                <div>Trademarkers LLC | 45 Essex Street, Suite 202 | Millburn NJ 07041</div>
            </div> -->
            <div class="clearfix"></div>

            <div  class="clearfix">
              <div class="left" style="line-height: 10px;">

                  <div class="bill-to-container clearfix">
                    <div class="bill-to-label">
                      <p class="bill-to">| Bill To.</p>
                    </div>
                    <div class="bill-to-details">
                      <p class="bill-to">{{ $order['billing_name'] }} </p>
                      <p style="line-height:16px;"> 
                        P. {{ $order['billing_address'] }} <br>
                        E. {{ $order['billing_address'] }} <br>
                        A. {{ $order['billing_country'] . ", " . $order['billing_city'] . ", " . $order['billing_address'] . ", " . $order['billing_state'] . " " . $order['billing_zip'] }}
                      </p>
                    </div>
                  </div>
           
              </div>
              
              <div class="right">
                <div >
                  <p style="font-size:38px;text-transform:uppercase;line-height:12px;font-weight:100;">Invoice</p>
                </div>

                @if ( $order->invoice->status != 'paid' ) 
                  <div>
                    <p><b> Please specify your invoice number in<br>your payment and correspondence. </b></p>

                    <table id="infotable">
                      <tr>
                        <td>Invoice No:</td>
                        <td class="info">{{ $order->invoice->reference }}</td>
                      </tr>
                      <tr>
                        <td>Date:</td>
                        <td class="info">{{ $order->created_at->format('F d, Y') }}</td>
                      </tr>
                      <tr>
                        <td>Due Date:</td>
                        <td class="info">{{ $order->created_at->addDays(30)->format('F d, Y') }}</td>
                      </tr>
                      <tr>
                        <td>Contact:</td>
                        <td class="info">Keziah Sumile</td>
                      </tr>
                      <tr>
                        <td>Phone:</td>
                        <td class="info">212-468-5467</td>
                      </tr>
                      <tr>
                        <td>Email:</td>
                        <td class="info">keziah@trademarkers.com</td>
                      </tr>
                    </table>
                  </div>
                @endif
              </div>
            </div>
        </header>

        <main>
            <table id="maintable" >
                <thead>
                    <tr>
                        <th class="service">SERVICE</th>
                        <th class="desc">DESCRIPTION</th>
                        <th>PRICE</th>
                        <th>QTY</th>
                        <th>TOTAL</th>
                    </tr>
                </thead>

                <tbody>

                    @php
                      $totalCount = 0;
                    @endphp
                    @foreach( $order->trademarks as $trademark )
                        <tr>
                            <td class="service">{{ $trademark->service }}</td>
                            <td class="desc">{{ '[' . $trademark->type . ']' . $trademark->service . ' for ' . $trademark->name }}</td>
                            <td class="unit">${{ number_format( $trademark->amount, 2) }}</td>
                            <td class="qty">1</td>
                            <td class="total">${{ number_format( $trademark->amount, 2) }}</td>
                        </tr>
                        @php
                        $totalCount += $trademark->amount;
                      @endphp
                    @endforeach
          
                    @if ( $totalCount != $order->total_amount ) 
                    <tr>
                        <td colspan="4">TOTAL DISCOUNT</td>
                        <td class="total">-${{ number_format( ($totalCount - $order->total_amount) , 2) }}</td>
                    </tr>
                    @endif
                    <tr>
                        <td colspan="4">SUBTOTAL</td>
                        <td class="total">${{ number_format( $order->total_amount, 2) }}</td>
                    </tr>
                    {{-- <tr>
                        <td colspan="4">TAX 25%</td>
                        <td class="total">$1,300.00</td>
                    </tr> --}}
                    <tr>
                        <td colspan="4" class="grand total">GRAND TOTAL</td>
                        <td class="grand total">${{ number_format( $order->total_amount, 2) }}</td>
                    </tr>
                </tbody>
            </table>

            {{-- <div id="notices">
                <div>NOTICE:</div>
                <div class="notice">A finance charge of 1.5% will be made on unpaid balances after 30 days.</div>
            </div> --}}
        </main>

        <footer>
            <div style="line-height: 5px;">
              <div style="float: left;">
                <p> Trademarkers LLC</p>
                <p> 45 Essex Street, Suite 202</p>
                <p> Millburn NJ 07041</p>
              </div>

              <div style="float: right;">
                <p> Bank Address</p>
                <p> 401 Madison Ave.</p>
                <p> New York, NY 10017</p>
              </div>

              <div style="float: right; margin-right: 180px;">
                <p> JP Morgan Chase</p>
                <p> SWIFT CHASUS33</p>
                <p> ACCOUNT NO: 55964-0730</p>
                <p> ROUTING NO: 267084131</p>
              </div>
            </div>
        </footer>
    </body>
</html>
<!DOCTYPE html>