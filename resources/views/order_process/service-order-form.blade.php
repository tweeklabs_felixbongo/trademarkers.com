@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <!-- <div class="col-md-3">
            <div class="card">
                
                @include('layouts.pricing')

            </div>
        </div> -->

        <div class="col-md-12">
            <div class="card" style="text-align: justify;">
                <div class="card-body">

                    @if ( session('errors') )
                        <div style="text-align: center;" class="alert alert-danger" role="alert">
                            {{session('errors')->first('message')}}
                        </div>
                    @endif

                    <h4>Trademark Service Order Form</h4>
                    
                    <p> In order to proceed with Trademark Service (<strong>{{ $service }}</strong>), please fill out the following form.</p>

                    <hr>

                    <form id="filing_form" method="POST" enctype="multipart/form-data">
                        @csrf
                        
                        <input type="hidden" name="action" value="submit" >
                        <div class="form-group row" id="word">
                            <div class="col-md-12">
                                <div class="form-check">
                                    <label for="word_mark"><b>Trademark</b></label>
                                    <input type="text" name="word_mark" class="form-control{{ $errors->has('word_mark') ? ' is-invalid' : '' }}" id="word_mark" placeholder="Enter Trademark" value="{{ isset( session('legal_case')->trademark ) ? session('legal_case')->trademark : old('word_mark') }}" required autofocus>

                                    @if ($errors->has('word_mark'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('word_mark') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="form-check">
                                    <label for="case_number"><b>Trademark Serial Number</b></label>
                                    <input type="text" name="case_number" class="form-control{{ $errors->has('case_number') ? ' is-invalid' : '' }}" id="case_number" placeholder="Trademark Serial Number" required autofocus>

                                    @if ($errors->has('case_number'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('case_number') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row hidden" style="display:none">
                            <div class="col-md-12">
                                <div class="form-check">
                                    <label for="word_mark"><b>Service</b></label>
                                    <input type="text" name="service" class="form-control" value="{{ isset($service) ? $service : 'Monitoring' }}" required autofocus>

                                    @if ($errors->has('service'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('service') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="form-check">
                                    <label for="word_mark"><b>Country</b></label>
                                    
                                    <select class="form-control" name="country" require>
                                        @foreach ($countrySelect as $price)
                                        <option value="{{$price->country->abbr}}">{{$price->country->name}}</option>
                                        @endforeach
                                        <!-- <option value="CA">Canada</option> -->
                                    </select>

                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12 btn-pull-right">
                                <input class="btn btn-danger" type="submit" name="submit" value="Continue">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
