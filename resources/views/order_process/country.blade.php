@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <!-- <div class="col-md-3">
            <div class="card">
             
                @include('layouts.pricing')
            </div>
        </div> -->

        <div class="col-md-12">
            <div class="card" style="text-align: justify;">
                <div class="card-body">

                    @if ( session('errors') )
                        <div style="text-align: center;" class="alert alert-danger" role="alert">
                            {{session('errors')->first('message')}}
                        </div>
                    @endif
                    
                   <h2> Trademark Registration in {{ $country['name'] }} 
                   
                    @if ( isset( $country['abbr'] ) )
                        <img class="flag-pull-right" src="{{ asset('images/flag/' . strtolower( $country['abbr'] ) . '.png' ) }}" alt="Flag" />
                    @endif 
                    </h2>

                   @if ( isset( $country['eu'] ) )
                        <p class="msg"> In order to obtain trademark protection in <b>{{ $country['name'] }}</b> you can register your trademark in two ways:</p>
                    @endif 

                    @if ( isset( $country['eu'] ) )
                        <p class="msg"> First option is that you request registration in the entire European Union with one single application; this can be done via the Community Trademark agreement which grants trademark protection in the 27 country members of the EU. <a href="/trademark-registration-in-european_union"> Click here.</a></p>
                    @endif

                    @if ( isset( $country['be'] ) )
                        <p class="msg"> Next option is that you can register your trademark in Benelux <a href="/trademark-registration-in-benelux"> Click here.</a></p>
                    @endif

                    @if ( isset( $country['eu'] ) && !isset( $country['be'] ) )
                        <p class="msg"> Next option is that you can register your trademark in {{ $country['name'] }} <a href="/trademark-registration-in-{{ strtolower(str_replace( ' ', '_', $country['name'] )) }}"> Click here.</a></p>
                    @endif

                    @if ( $country['abbr'] == 'EM' )
                        <p class="msg"> The Community Trademark grants protection over the entire European Union with one registration. This Trademark Registration includes protection in: Austria, Bulgaria, Belgium, Czech Republic, Cyprus, Denmark, Estonia, Finland, France, Germany, Greece, Hungary, Ireland, Italy, Latvia, Lithuania, Luxembourg, Malta, The Netherlands, Poland, Portugal, Romania, Slovakia, Slovenia, Spain, Sweden and United Kingdom.</p>
                    @endif
                    
                    <p> The ownership of a trademark is not automatically granted merely by using it. Generally speaking, only by going through the legal process of registration can you ensure rights to your trademark.</p>

                    <p> Registering a trademark is not always a simple matter of submitting an application. Many jurisdictions do not make information readily available to the public and some do not provide reliable data online. Researching similarities to your trademark can be a complex task.</p>

                    <!-- FILTER IF BENELUX -->
                    @if ( !isset( $country['be'] )) 

                    <p> Trademarkers.com will process your trademark registration in {{ $country['name'] }} through the following steps:</p>

                    <div class="panel-group" id="accordion">
                        
                        <div class="panel panel-default">

                        
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="toggle-angle" data-toggle="collapse" data-parent="#accordion" href="#trdRegistration">
                                    <i class="fa fa-angle-right"></i> Step 1: Trademark Comprehensive Study: <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" data-html="true" title="Read More..."></i></a>
                                    <a class="btn-pull-right" href="/study/step1/{{ isset( $country['be'] ) ? 'BX' : $country['abbr'] }}">
                                        <button class="btn btn-danger"> Start Study</button>
                                    </a>
                                </h4>
                            </div>

                            <div id="trdRegistration" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p> Trademark search report with Attorney's analysis about registration probabilities. This report is optional but highly recommended.</p>

                                <p> There are several reasons why you might want to begin the registration process with a study:</p>

                                <ul>
                                    <li> 
                                        If there is a pre-existing claim to your trademark, it is best to know this before you have invested time and resources into an application
                                    </li>

                                    <li> 
                                        Trademark similarities can be ambiguous and are best evaluated by a legal professional
                                    </li>

                                    <li> 
                                        It is possible to accidentally infringe on someone else’s trademark if you are not aware of its existence
                                    </li>

                                    <li> 
                                        A trademark study provides a snapshot of potential brands in your target market
                                    </li>
                                </ul>

                                <p> With representation in almost every major jurisdiction, TradeMarkers has up-to-date information on trademark application activities. Hence, we are able to offer the following services:</p>

                                <ul>
                                    <li> 
                                        Full analysis of existing trademarks, including similarities in appearance, sound, connotation and commercial impression
                                    </li>

                                    <li> 
                                        Consultation on what to do if an existing similarity is found
                                    </li>

                                    <li> 
                                        Enquiry with local trademark offices, as necessary
                                    </li>

                                    <li> 
                                        Detailed reporting on the availability of relevant marks and recommendations for your application
                                    </li>
                                </ul>

                                <p> Start by ordering a search report with Attorney's analysis about registration probabilities. This report is optional but highly recommended.</p>

                            </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="toggle-angle" data-toggle="collapse" data-parent="#accordion" href="#trdStudy">
                                    <i class="fa fa-angle-down"></i> Step 2: Trademark Registration Request: <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" data-html="true" title="Read More..."></i></a>
                                    <a class="btn-pull-right" href="/registration/step1/{{ isset( $country['be'] ) ? 'BX' : $country['abbr'] }}"><button class="btn btn-danger"> Start Registration</button></a>
                                </h4>
                            </div>

                            <div id="trdStudy" class="panel-collapse collapse show">
                                <div class="panel-body">
                                    <p> With a legally registered trademark, you gain several benefits:</p>
                                    <ul>
                                        <li> 
                                            Potential copycats are deterred from infringing on your brand
                                        </li>
                                        <li> 
                                            The ability of other brands to claim that you are infringing on their trademark is severely diminished
                                        </li>
                                    </ul>
                                    <p> Our licensed Trademark Attorneys in {{ $country['name'] }} will handle your trademark request, check all formalities, and then file with the <b>{{ $country['agency_name'] }}</b> (<b>{{ $country['agency_short'] }}</b>). You will receive confirmation of filing and a copy of the filing request. We will contact you immediately if any subsequent office actions require additional information, or if there are any opposition in {{ $country['name'] }} against your application.</p>
                                    <p> After the formal examination at the {{ $country['agency_short'] }} the trademark will be published for opposition to allow third parties to oppose if they believe that the mark infringes on their brands. The opposition period in {{ $country['name'] }} is <b>{{ $country['opposition_period'] }}</b> after publication. You will receive status updates as soon as your mark is published, and any actions the {{ $country['agency_short'] }} will take if oppositions are received.</p>
                                    <p> If no oppositions arise and your trademark is formally approved, you can expect registration within <b>{{ $country['registration_period'] }}</b>.</p>
                                </div>
                            </div>

                        </div>
                        
                        <div class="panel panel-default">
                            
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="toggle-angle" data-toggle="collapse" data-parent="#accordion" href="#trdPricing">
                                    <i class="fa fa-angle-down"></i> Pricing Table <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" data-html="true" title="Read More..."></i></a>
                                </h4>
                            </div>

                            <div id="trdPricing" class="panel-collapse collapse show">
                                <div class="panel-body">

                                    <h5>
                                        @php $abbr = strtoupper( $country['abbr'] ); @endphp

                                        @if ( $abbr == "NL" || $abbr == "LU" )
                                            @php $abbr = "BX"; @endphp
                                        @endif
                                        {{ strtoupper( $abbr )}} Trademark Registration Prices
                                    </h5>

                                    @foreach( $country->prices as $prices )

                                        @if ( $prices['service_type'] == 'Study' )
                                            <p> Step 1 Trademark Comprehensive Study</p>
                                        @elseif( $prices['service_type'] == 'Registration' )
                                            <p> Step 2 Trademark Registration Request</p>
                                        @elseif ( $prices['service_type'] == 'Certificate' )
                                            <p> Step 3 Trademark Certificate Request</p>
                                        @endif

                                        @if ( $prices['service_type'] == 'Study' || 
                                                $prices['service_type'] == 'Registration' ||
                                                $prices['service_type'] == 'Certificate' ) 
                                        <table class="table table-striped">
                                            <tr>
                                                <td> Classes</td>
                                                <td> Word Only</td>
                                                <td> Design Only or Combined Word and Design</td>
                                            </tr>

                                            <tr>
                                                <td> First Class</td>
                                                <td> 
                                                    {{ $prices['initial_cost'] }}
                                                </td>
                                                <td> 
                                                    {{ $prices['logo_initial_cost'] }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td> Each Additional Class</td>
                                                <td> 
                                                    {{ $prices['additional_cost'] }}
                                                </td>
                                                <td> 
                                                    {{ $prices['logo_additional_cost'] }}
                                                </td>
                                            </tr>
                                        </table>
                                        @endif

                                    @endforeach
                                    
                                    <p style="color: red;"> Prices are in U.S Dollars</p>
                                    <p> Note: Prices include all official and professional fees. Prices do not include legal defenses in case of oppositions or objections</p>
                                    <p> Timeframe of the trademark registration process is only an estimate and it may vary considerably if any objections and/or oppositions are presented, or other events occur during the trademark registration process.</p>

                                </div>
                            </div>

                        </div>
                        @endif

                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

@endsection
