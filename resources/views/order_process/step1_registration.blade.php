@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <!-- <div class="col-md-3">
            <div class="card">
                
                @include('layouts.pricing')
            </div>
        </div> -->
        
        <div class="col-md-12">
            <div class="card" style="text-align: justify;">
                <div class="card-body">
                    
                   <h2> Trademark Registration in {{ $country['name'] }}
                   @if ( isset( $country['abbr'] ) )
                        <img class="flag-pull-right" alt="trademarkers" src="{{ asset('images/flag/' . strtolower( $country['abbr'] ) . '.png' ) }}" alt="Flag" />
                    @endif 
                    </h2>
                    
                    <p> Our licensed Trademark Attorneys in <b> {{ $country['name'] }}</b> will handle your trademark request, check all formalities, and then file with the <b> {{ $country['agency_name'] }}</b>. You will receive confirmation of filing and a copy of the filling request. We will contact you immediately if any subsequent office actions require additional information, or if there are any opposition in <b> {{ $country['name'] }}</b> against your application. </p>

                    <p> After the formal examination at the <b> {{ $country['agency_name'] }}</b> the trademark will be published for opposition to allow third parties to oppose if they believe that the mark infringes on their brands. The opposition period in <b> {{ $country['agency_name'] }}</b> is <b> {{ $country['opposition_period'] }}</b> after publication. You will receive status updates as soon as your mark is published, and any actions the will take if oppositions are received. </p>

                    <p> If no oppositions arise and your trademark is formally approved, you can expect registration <b> {{ $country['registration_period'] }}</b>.</p>

                    <p> As <b> {{ $country['name'] }}</b> is a member of the Paris Convention, you can claim the priority of your filing in any other filing in national trademark offices of countries that are part of the Paris Convention.</p>

                    <a class="btn-pull-right" href="/registration/step2/{{ $country['abbr'] }}"><button class="btn btn-danger"> Proceed</button></a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
