@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <!-- <div class="col-md-3">
            <div class="card">
                
                @include('layouts.pricing')
            </div>
        </div> -->

        <div class="col-md-12">
		    <div class="card" style="text-align: justify;">
		        <div class="card-body">
		            
		           <h2> Trademark Study in {{ $country['name'] }}
				   @if ( isset( $country['abbr'] ) )
                        <img class="flag-pull-right" alt="trademarkers" src="{{ asset('images/flag/' . strtolower( $country['abbr'] ) . '.png' ) }}" alt="Flag" />
                    @endif 
                    </h2>
		            
		            <p> Before filing your trademark in {{ $country['name'] }} it is important that you evaluate possible obstacles that may arise during registration process. </p>

		            <p> Our Trademark Comprehensive Study will not only list similar trademarks {graphic/phonetic} that may conflict with yours, but also give you an Attorney's opinion about registration possibilities.</p>

		            <p> <b> This report is optional but highly recommeneded.</b> </p>

		            <a class="btn-pull-right" href="/study/step2/{{ $country['abbr'] }}"><button class="btn btn-danger"> Proceed</button></a>
		        </div>
		    </div>
			
		</div>
    </div>
</div>

@endsection
