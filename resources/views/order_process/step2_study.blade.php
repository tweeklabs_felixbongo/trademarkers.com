@extends('layouts.master')

@push('head')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" /> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" />

@endpush

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <!-- <div class="col-md-3">
            <div class="card">
                
                @include('layouts.pricing')

            </div>
        </div> -->

        <div class="col-md-12">
            <div class="card" style="text-align: justify;">
                <div class="card-body">

                    @if ( session('errors') )
                        <div style="text-align: center;" class="alert alert-danger" role="alert">
                            {{session('errors')->first('message')}}
                        </div>
                    @endif
                    
                    <h3> Trademark Study Order Form in {{ $country['name'] }}
                    @if ( isset( $country['abbr'] ) )
                        <img class="flag-pull-right" src="{{ asset('images/flag/' . strtolower( $country['abbr'] ) . '.png' ) }}" alt="Flag" />
                    @endif 
                    </h3>
                    
                    <p> Please fill out the following form, at the bottom of the form you can find price of the Study. Once your Study is ready you will be able to download it directly from our website, through your user account.</p>

                    <hr>
                    <form method="POST" action="/study/step2/{{ $country['abbr'] }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="form-check">
                                    <label for="type">
                                        <b> Please select the type of trademark you wish to request:</b>
                                        <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-html="true" title="TYPES OF TRADEMARKS<hr><p>Word Mark: one trademark, which contains only verbal elements (words, letters, numbers or any character).
                                        </p>
                                        <p>Logo (design only): one trademark, which contains only design elements (images, graphics, colors).</p>
                                        <p>Combined Mark: one trademark, which contains verbal elements as well as design elements (including stylized text).</p>
                                        "></i>
                                    </label>
                                    
                                    <select class="form-control{{ $errors->has('type') ? ' is-invalid' : '' }}" name="type" id="type" value="{{ old('type') }}">
                                        <option value="word"> Word-Only</option>
                                        <option value="logo"> Design-Only or Stylized Word-Only (Figurative)</option>
                                        <option value="lword"> Combined Word and Design </option>       
                                    </select>

                                    @if ($errors->has('type'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('type') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <hr>

                        <div class="form-group row" id="word">
                            <div class="col-md-12">
                                <div class="form-check">
                                    <label for="word_mark"><b>Trademark</b></label>
                                    <input type="text" name="word_mark" class="form-control{{ $errors->has('word_mark') ? ' is-invalid' : '' }}" id="word_mark" placeholder="Enter word mark" value="{{ old('word_mark') }}" required autofocus>

                                    @if ($errors->has('word_mark'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('word_mark') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row" id="upload" style="display: none;">
                            <div class="col-md-12">
                                <div class="form-check">
                                    <label for="logo_pic"><b>Upload Photo (jpeg, jpg, png)</b></label>
                                    <input type="file" name="logo_pic" class="form-control{{ $errors->has('logo_pic') ? ' is-invalid' : '' }}" id="logo_pic">

                                    @if ($errors->has('logo_pic'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('logo_pic') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <hr>

                        @php 
                            $classes = null;
                            $hasCampaign = false;
                            $isPriority = false;

                            $tmFilingDate='';
                            $trademarknumber='';
                            $trademarkCountryCode='';

                            if ( isset(session('legal_case')->class_number) && session('legal_case')->class_number ){
                                $strclasses = session('legal_case')->class_number;
                                $strclasses = str_replace(array( '[', ']' ), '', $strclasses);
                                $classes = explode(',',$strclasses);
                                
                                if (session('legal_case')->campaign) {
                                    $hasCampaign = true;
                                }

                                if (session('legal_case')->campaign && session('legal_case')->campaign->priority == 'yes' ) {
                                    $isPriority = true;

                                    $tmFilingDate = date('Y-m-d', strtotime(session('legal_case')->tmFilingDate));
                                    $trademarkCountryCode = date('Y-m-d', strtotime(session('legal_case')->country_code));
                                    $trademarknumber = date('Y-m-d', strtotime(session('legal_case')->number));
                                }
                            }

                        @endphp

                        <!-- display class -->

                        <div class="form-group row">
                            <div class="col-md-12">
                                <div style="padding-left:1.25rem;" class="form-check2">
                                <p> 
                                    <b>Select the classes in which you need to register your Trademark:</b>
                                    <span> 
                                        <!-- <a href="/classes" target="_blank" class="btn btn-sm btn-danger" style="color: white;"> 
                                            Search Classes
                                        </a> -->
                                        <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" data-html="true" title="What are classes?<hr><p>When registering a trademark you need to specify the products and services that will be associated with your trademark.

                                        The large majority of countries of the world have adopted the International Classification of Nice. This system groups all products and services into 45 classes - 34 for products, 11 for services - permitting you to specify precise and clear classes covering your trademark.

                                        The protection that is offered to a registered trademark covers only the classes specified at the time of registration, therefore, allowing two identical trademarks to coexist in distinct classes.

                                        We provide a helpful Class Search tool, which allows you to easily determine the classes your products or services are related to; you will find the button that redirects to this application in this page..</p>
                                        "></i>
                                    </span>
                                </p>
                                
                                
                                <div class="new-add-class-container">

                                    <!-- populate selected classes here -->
                                    <table id="listClasses" class="table" style="border-bottom:1px solid #dee2e6; margin-bottom : 30px;">
                                    </table>

                                    @if (!$hasCampaign)
                                    <select id="addClassValue" class="form-control selectpicker"data-live-search="true">
                                    <option value="" disabled selected>Select Trademark Class</option>
                                        @foreach( $trademarkClasses as $trdClass)
                                            <option value="{{$trdClass->id}}" data-tokens="{{$trdClass->id}} - {{$trdClass->name}}">{{$trdClass->id}} - {{$trdClass->name}}</option>
                                        @endforeach
                                    </select>

                                    <!-- <p style="margin-bottom: 10px; margin-top:10px;"><b>Selected Class(es)</b></p> -->
                                    @endif

                                    @if ($hasCampaign)
                                    <a href="#" class="btn btn-danger reset hide-class" >Reset</a>
                                    @endif

                                    <!-- populate selected classes here -->
                                    <!-- <table id="listClasses" class="table" style="border-bottom:1px solid #dee2e6; margin-bottom : 30px;">
                                    </table> -->
                                    
                                </div>
                                <div class="classes_container {{ $classes ? 'hidden' : 'show' }}" style="display:none">
                                    
                                    @for ( $i = 1; $i < 46; $i++ ) 
                                        <div class="form-check form-check-inline">
                                            <table>
                                                <tr>
                                                    <td> 
                                                        @php
                                                            $selectClass = false;
                                                            if ($classes) {
                                                                $selectClass = in_array($i,$classes);
                                                            } 
                                                        @endphp
                                                        <input class="form-check-input class_chk" type="checkbox" id="class{{$i}}" value="class{{$i}}" name="class[]" {{ $selectClass ? 'checked' : '' }}>
                                                        <label class="form-check-label" for="class{{$i}}">{{ $i = $i < 10? '0' . $i : $i }}</label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    @endfor
                                </div>

                                </div>
                            </div>

                        </div>

                        <div class="form-group row">
                            <div class="col-md-12 btn-pull-right">
                                <input class="btn btn-danger" type="submit" name="submit" value="Continue">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
