@extends('layouts.master')


@push('head')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" /> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" />

@endpush



@section('content')


<div class="container">
    <div class="row justify-content-center">
        <!-- <div class="col-md-3">
            <div class="card">
                
                @include('layouts.pricing')

            </div>
        </div> -->

        <div class="col-md-12">
            <div class="card" style="text-align: justify;">
                <div class="card-body">

                    @if ( session('errors') )
                        <div style="text-align: center;" class="alert alert-danger" role="alert">
                            {{session('errors')->first('message')}}
                        </div>
                    @endif

                    <h4> Trademark Request for Registration Order Form in {{ $country['name'] }}</h4>
                    
                    <p> In order to proceed with Trademark Registration in {{ $country['name'] }} please fill out the following form. You'll have to provide information about the Trademark, the Administrative contact and the Owner.</p>

                    <hr>


                    @php 
                        $classes = null;
                        $hasCampaign = false;
                        $isPriority = false;

                        $tmFilingDate='';
                        $trademarknumber='';
                        $trademarkCountryCode='';
                        $trademarkCountryName='';

                        if ( isset(session('legal_case')->class_number) && session('legal_case')->class_number ){
                            $strclasses = session('legal_case')->class_number;
                            $strclasses = str_replace(array( '[', ']' ), '', $strclasses);
                            $classes = explode(',',$strclasses);
                            
                            if (session('legal_case')->campaign) {
                                $hasCampaign = true;
                            }

                            if (session('legal_case')->campaign && session('legal_case')->campaign->priority == 'yes' ) {
                                $isPriority = true;

                                $tmFilingDate = date('Y-m-d', strtotime(session('legal_case')->tm_filing_date));
                                $trademarkCountryCode = session('legal_case')->country_code;
                                $trademarkCountryName = session('legal_case')->country;
                                $trademarknumber = session('legal_case')->number;
                            }
                        }

                        if ( session('email_cron_priority') ) {
                            $isPriority = true;
                        }

                    @endphp

                    
                    <div class="form-group row">
                        <div class="col-md-12">
                            <div class="form-check">
                                <label for="type">
                                    <b> Selected Country </b>
                                </label>
                                <input type="hidden" id="order-url" value='{{url("/registration/step2/")}}'>
                                <select id="changeCountry" class="form-control selectpicker" data-live-search="true">
                                    @foreach( $countries as $iCountry)
                                        <option {{ $iCountry->abbr == $country->abbr ? "selected" : "" }} value="{{$iCountry->abbr}}" data-tokens="{{$iCountry->abbr}} - {{$iCountry->name}}">{{$iCountry->abbr}} - {{$iCountry->name}}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>
                    </div>

                    
                    <form id="filing_form" method="POST" action="/registration/step2/{{ $country['abbr'] }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="form-check">
                                    <label for="type">
                                        <b> Please select the type of trademark you wish to request:</b>
                                        <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-html="true" title="TYPES OF TRADEMARKS<hr><p>Word Mark: one trademark, which contains only verbal elements (words, letters, numbers or any character).
                                        </p>
                                        <p>Logo (design only): one trademark, which contains only design elements (images, graphics, colors).</p>
                                        <p>Combined Mark: one trademark, which contains verbal elements as well as design elements (including stylized text).</p>
                                        "></i>
                                    </label>

                                    @if ($hasCampaign) 
                                        <p style="margin: 0px;border-radius: 3px;background: #e9ecef;padding: 5px 15px;">Word-Only</p>
                                    @endif
                                    
                                    <select class="form-control{{ $errors->has('type') ? ' is-invalid' : '' }} {{ ($hasCampaign ? 'hide-class' : '') }}" name="type" id="type" value="{{ old('type') ? old('type') : 'word' }}" >
                                        <option value="word" selected> Word-Only</option>
                                        <option value="logo"> Design-Only or Stylized Word-Only (Figurative)</option>
                                        <option value="lword"> Combined Word and Design </option>           
                                    </select>

                                    @if ($errors->has('type'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('type') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <hr>

                        <div class="form-group row" id="word">
                            <div class="col-md-12">
                                <div class="form-check">
                                    <label for="word_mark"><b>Trademark</b></label>
                                    @if ($hasCampaign) 
                                        <p style="margin: 0px;border-radius: 3px;background: #e9ecef;padding: 5px 15px;">{{ isset( session('legal_case')->trademark ) ? session('legal_case')->trademark : old('word_mark') }}</p>
                                    @endif
                                    <input type="text" name="word_mark" class="form-control{{ $errors->has('word_mark') ? ' is-invalid' : '' }} {{ ($hasCampaign ? 'hide-class' : '') }}" id="word_mark" placeholder="Enter word mark" value="{{ isset( session('legal_case')->trademark ) ? session('legal_case')->trademark : old('word_mark') }}" required autofocus>

                                    @if ($errors->has('word_mark'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('word_mark') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group row" id="upload" style="display: none;">
                            <div class="col-md-12">
                                <div class="form-check">
                                    <label for="logo_pic"><b>Upload Photo (jpeg, jpg, png)</b></label>
                                    <input type="file" name="logo_pic" class="form-control{{ $errors->has('logo_pic') ? ' is-invalid' : '' }}" id="logo_pic">

                                    @if ($errors->has('logo_pic'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('logo_pic') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-check">
                                    <label for="color_claim"><b>Color Claim</b></label>
                                    <select class="form-control{{ $errors->has('color_claim') ? ' is-invalid' : '' }}" name="color_claim" id="color_claim" {{ ($hasCampaign ? 'disabled' : '') }}>
                                        <option value="no" selected> No</option>
                                        <option value="yes"> Yes</option>
                                    </select>

                                    @if ($errors->has('color_claim'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('color_claim') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <hr>

                        <!-- display class -->

                        <div class="form-group row">
                            <div class="col-md-12">
                                <div style="padding-left:1.25rem;" class="form-check2">
                                <p> 
                                    <b>Select the classes in which you need to register your Trademark:</b>
                                    <span> 
                                        <!-- <a href="/classes" target="_blank" class="btn btn-sm btn-danger" style="color: white;"> 
                                            Search Classes
                                        </a> -->
                                        <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" data-html="true" title="What are classes?<hr><p>When registering a trademark you need to specify the products and services that will be associated with your trademark.

                                        The large majority of countries of the world have adopted the International Classification of Nice. This system groups all products and services into 45 classes - 34 for products, 11 for services - permitting you to specify precise and clear classes covering your trademark.

                                        The protection that is offered to a registered trademark covers only the classes specified at the time of registration, therefore, allowing two identical trademarks to coexist in distinct classes.

                                        We provide a helpful Class Search tool, which allows you to easily determine the classes your products or services are related to; you will find the button that redirects to this application in this page..</p>
                                        "></i>
                                    </span>
                                </p>
                                
                                
                                <div class="new-add-class-container">

                                    <!-- populate selected classes here -->
                                    <table id="listClasses" class="table" style="border-bottom:1px solid #dee2e6; margin-bottom : 30px;">
                                    </table>

                                    @if (!$hasCampaign)
                                    <select id="addClassValue" class="form-control selectpicker"data-live-search="true">
                                    <option value="" disabled selected>Select Trademark Class</option>
                                        @foreach( $trademarkClasses as $trdClass)
                                            <option value="{{$trdClass->id}}" data-tokens="{{$trdClass->id}} - {{$trdClass->name}}">{{$trdClass->id}} - {{$trdClass->name}}</option>
                                        @endforeach
                                    </select>

                                    <!-- <p style="margin-bottom: 10px; margin-top:10px;"><b>Selected Class(es)</b></p> -->
                                    @endif

                                    @if ($hasCampaign)
                                    <a href="#" class="btn btn-danger reset hide-class" >Reset</a>
                                    @endif

                                    
                                    
                                </div>
                                <div class="classes_container {{ $classes ? 'hidden' : 'show' }}" style="display:none !important">
                                    
                                    @for ( $i = 1; $i < 46; $i++ ) 
                                        <div class="form-check form-check-inline">
                                            <table>
                                                <tr>
                                                    <td> 
                                                        @php
                                                            $selectClass = false;
                                                            if ($classes) {
                                                                $selectClass = in_array($i,$classes);
                                                            } 
                                                        @endphp
                                                        <input class="form-check-input class_chk" type="checkbox" id="class{{$i}}" value="class{{$i}}" name="class[]" {{ $selectClass ? 'checked' : '' }}>
                                                        <label class="form-check-label" for="class{{$i}}">{{ $i = $i < 10? '0' . $i : $i }}</label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    @endfor
                                </div>

                                </div>
                            </div>

                        </div>


                        @if ( $country['abbr'] == 'US' )
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="form-check">
                                        <label for="commerce">
                                            <b> Is your trademark being used in commerce in {{ $country['name'] }}?</b>

                                            <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-html="true" title="<i>“Used in commerce”</i> means that the mark is used in the ordinary course of trade. For example, for goods it would be used when it is placed in any manner on the goods, their containers or their displays. For services it is used when the trademark is used or displayed in advertising or during the sales process.">
                                            </i>
                                        </label>
                                        
                                        <select class="form-control{{ $errors->has('commerce') ? ' is-invalid' : '' }}" name="commerce" id="commerce" {{ ($hasCampaign ? 'disabled' : '') }}>
                                            <option value="no" selected> No</option>
                                            <option value="yes"> Yes</option>
                                            <option value="registered in another country"> No, but it is registered in another country</option>         
                                        </select>

                                        @if ($errors->has('commerce'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('commerce') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="form-check hasCampaign" data-hasCampaign="{{$hasCampaign}}">
                                    <label for="filed">
                                        <b> Have you filed your trademark application in any other country within the last 6 months?</b>
                                    </label>
                                    
                                    <select class="form-control{{ $errors->has('filed') ? ' is-invalid' : '' }}" name="filed" id="filed" {{ ($hasCampaign ? 'disabled' : '') }}>
                                        <option value="no" selected> No</option>
                                        <option value="yes"> Yes</option>         
                                    </select>

                                    <div id="claim_priority" style="display: none;">
                                        <span> Then you may want to <b>Claim Priority</b></span>
                                        <div style="text-align: left;" class="alert alert-info" role="alert">
                                            <p>     
                                                Claiming priority is part of the Paris Convention treaty.

                                                It means that if you have filed a trademark within the last 6 months in any of the signatory countries (most countries of the world), you can use the filing date of this first registration as the filing date in the other signatory countries.

                                                This right may be useful to the owner of the trademark, to protect his intellectual property, in cases of legal conflicts with other parties.
                                            </p>
                                        </div>

                                        <label for="priority">
                                            <b> Claim Priority 
                                                <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-html="true" title="<p>In order to claim priority, you need to provide</p>
                                                <p><b>Country</b> in which you first filed your trademark</p>
                                                <p><b>Date</b> in which you first filed your trademark</p>
                                                <p><b>Filing number</b> of your first filed trademark</p>
                                                To claim priority, both applications must be identical">
                                                </i>
                                            </b>
                                        </label>

                                        <select class="form-control{{ $errors->has('priority') ? ' is-invalid' : '' }}" name="priority" id="priority" data-isPriority="{{$isPriority}}" {{ ($hasCampaign ? 'disabled' : '') }}>
                                            <option value="no" {{ ($isPriority ? "" : "selected") }}> No</option>
                                            <option value="yes" {{ ($isPriority ? "selected" : "") }}> Yes</option>         
                                        </select>
                                    </div>

                                    @if ($errors->has('filed'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('filed') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div id="yes_priority" style="display: none;">

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="form-check">
                                        <label for="commerce">
                                            <b> Country of Origin</b></i>
                                        </label>
                                        <select class="form-control{{ $errors->has('origin') ? ' is-invalid' : '' }}" name="origin" id="origin" data-case-value="{{ session('legal_case') && session('legal_case')->country_code ? session('legal_case')->country_code : '' }}" {{ ($hasCampaign ? 'disabled' : '') }}>
                                            
                                            @foreach( $countries as $country )
                                                <option value="{{ $country['abbr'] }}" {{ ( session('legal_case') && session('legal_case')->country_code == $country['abbr'] ? 'selected' : '') }}>
                                                    {{ $country['abbr'] . ' - ' . $country['name'] }}
                                                </option>
                                            @endforeach      
                                        </select>

                                        @if ($errors->has('origin'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('origin') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="form-check">
                                        <label for="commerce">
                                            <b> Date</b></i>
                                        </label>
                                        
                                        <input class="form-control{{ $errors->has('date') ? ' is-invalid' : '' }}" value="{{ session('legal_case') ? date('Y-m-d', strtotime(session('legal_case')->tm_filing_date)) : '' }}" type="date" name="date" id="date" data-case-value="{{ session('legal_case') ? date('Y-m-d', strtotime(session('legal_case')->tm_filing_date)) : '' }}" {{ ($hasCampaign ? 'disabled' : '') }}>

                                        @if ($errors->has('date'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('date') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="form-check">
                                        <label for="commerce">
                                            <b> Trademark Number</b></i>
                                        </label>
                                        
                                        <input class="form-control{{ $errors->has('tm') ? ' is-invalid' : '' }}" type="text" name="tm" id="tm" data-case-value="{{ session('legal_case') ? session('legal_case')->number : '' }}" {{ ($hasCampaign ? 'disabled' : '') }}>

                                        @if ($errors->has('tm'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('tm') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12 btn-pull-right">
                                <input class="btn btn-danger" type="submit" name="submit" value="Continue">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
 

@endsection
