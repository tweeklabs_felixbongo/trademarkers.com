@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <!-- <div class="col-md-3">
            <div class="card">
                
                @include('layouts.pricing')
            </div>
        </div> -->

        <div class="col-md-12">
            <div class="card" style="text-align: justify;">
                <div class="card-body">
                    
                    <h4>{{ session('trademark_order')['service'] }} Order Form in {{ $country['name'] }}</h4>
                    
                    <hr>
                    <p> To finalize your trademark registration<b><i>{{ session('trademark_order')['service'] }}</i></b>, please check and confirm</p>
                    <table class="table table-bordered">
                        <tr>
                            <td>
                                <b>Country</b>
                            </td>
                            <td>
                                <a href="">
                                    <img src="{{ asset('images/' . $country['avatar']) }}" alt="{{ $country['abbr'] }}"> {{ $country['name'] }}
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Service</b>
                            </td>
                            <td>
                                {{ session('trademark_order')['service'] }}
                            </td>
                        </tr>
                        @if ( !session('trademark_order')['service'] == 'Monitoring' ) 
                        <tr>
                            <td>
                                <b>Type</b>
                            </td>
                            <td>
                                {{ session('trademark_order')['type'] }}
                            </td>
                        </tr>
                        @endif
                        <tr>
                            <td>
                                <b>Trademark</b>
                            </td>
                            <td>
                                {{ isset(session('trademark_order')['name']) ? session('trademark_order')['name'] : '' }}
                            </td>
                        </tr>
                        @if ( session('trademark_order')['service'] == 'Monitoring' ) 
                        <tr>
                            <td>
                                <b>Case Number</b>
                            </td>
                            <td>
                                {{ session('trademark_order')['case_number'] }}
                            </td>
                        </tr>
                        @endif
                        @if ( !session('trademark_order')['service'] == 'Monitoring' ) 
                        <tr>
                            <td>
                                <b>Photo</b>
                            </td>
                            <td>

                                @if ( session('trademark_order')['logo'] != "N/A" )
                                    <img src="/uploads/{{ session('trademark_order')['logo'] }}" width="100">

                                    <a href="/uploads/{{ session('trademark_order')['logo'] }}" target="_blank">
                                        <i class="fa fa-eye"></i> View
                                    </a>
                                @else
                                    {{ session('trademark_order')['logo'] }}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Classes</b>
                            </td>
                            <td>
                                {{ session('trademark_order')['classes'] }}
                            </td>
                        </tr>
                        @endif
                        <tr>
                            <td>
                                <b>Total</b>
                            </td>
                            <td>
                                ${{ number_format( session('trademark_order')['amount'], 2 ) }} 
                            </td>
                        </tr>
                    </table>
                    @if ( session('legal_case') ) 
                        <p style="color:#e3342f;text-align:right;font-style:italic">Discount will be deducted in the Cart</p>
                    @endif

                    <form method="POST" action="/order_form/{{ $country['abbr'] }}">
                        @csrf
                        <div class="form-group row" id="word">

                            {{-- <div class="col-md-8">
                                <div class="form-check">
                                    <input type="submit" name="back" value="Back" class="btn btn-danger" />
                                </div>
                            </div> --}}
                            <div class="col-md-12" >
                                <div class="form-check text-right" style="width:100%" >
                                    <input type="submit" name="add_to_cart" value="Add to cart" class="btn btn-danger" />
                                </div>
                            </div>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
