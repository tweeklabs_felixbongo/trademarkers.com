@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <!-- <div class="col-md-3">
            <div class="card">
                
                @include('layouts.pricing')
            </div>
        </div> -->

        @include('profile.information')
    </div>
</div>

@endsection
