@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    
                    <h2> The Trademark Filing Process</h2>

                    <p> Trademark application process can be described in three simple steps.</p>

                    <h4><b> Step 1: Preliminary Study<b></h4>

                    <p> Although optional, it is beneficial to conduct a study of your target market prior to filing a trademark there. This provides information on the probability of successful registration, as well as details on competing brands and potential conflicts. Based on this information, you can make informed decisions for your trademark.</p>

                    <h4><b> Step 2: Application & Registration<b></h4>

                    <p> TradeMarkers manages the entire filing process with trademark authorities. We liaise between all parties, keeping you up-to-date on your application status. If any objections are raised against your trademark or if there are complications, you will be informed as soon as possible for consultation.</p>

                    <h4><b> Step 3: Issuance & Monitoring<b></h4>

                    <p> When your application is complete, we will deliver a certificate of registration along with all pertinent details. At this point, monitoring is recommended for protection against infringement and copycat branding. Our monitoring service regularly checks your target markets for trademark similarities and advises you if any are found.</p>

                    <hr>

                    <h2> Which best describes your situation?</h2>

                    <h4><b> I want to get an idea of where I can use my trademark</b></h4>
                    
                    <p> The first step towards attaining a trademark is to find out if it is available at all in your target regions. Some countries have readily available information on the internet for existing registrations, while others do not even have a website. A trademark study is the best way to get answers and identify opportunities to attain your trademark.</p>

                    <h4><b> I want a trademark registered in a specific country</b></h4>
                    
                    <p> If your brand will be used in a single country and you have no plans to expand elsewhere, the simplest solution may be to attain a trademark in just that country. This tends to be a simpler and less expensive process than registering across multiple jurisdictions.</p>

                    <p> If your target country is part of a treaty region, it may make more sense to register with the relevant regional trademark authority. This allows for registration across multiple countries in a single application, often at only a small difference in cost. Major treaties include the European Union, Benelux, OAPI, and ARIPO. For more information, please see below.</p>

                    <h4><b> I plan to use my trademark in several specific countries</b></h4>

                    <p> If you need trademark protection in several countries, or if you plan to expand your brand later, there are several options:</p>

                    <ul>
                        <li> If your target countries do not share a common treaty region, you can register separately with each respective country’s trademark office</li>
                        <li> If the countries fall under a treaty, you can register with the treaty office</li>
                        <li> You can register under the Paris Convention, a treaty which covers most countries worldwide</li>
                    </ul>

                    <p> If you would like us to put together a quote, please contact us and give details – we will get back to you shortly.</p>

                    <h4><b> I need worldwide trademark protection</b></h4>

                    <p> For brands requiring global registration, the best option can be to file with the office of the Paris Convention. This office covers most countries worldwide. While individual countries reserve the right to accept or reject your application, the administrative process is made much easier using this system.</p>

                    <p> If you require worldwide brand protection, we would like to hear the specifics and consult you on choosing the best option for your situation. Please contact us and outline your trademark needs. One of our representatives will reply shortly.</p>

                    <h2> What are Treaty Regions?</h2>

                    <p> Treaty regions are formed by certain countries which share a common language, geography, or economy. These allow trademarks to be registered across all member countries in a single application. Registration under a treaty is typically more simple than applying in multiple countries individually, and it allows for broader protection of trademarks.</p>

                    <p> Some treaty regions include:</p>

                    <ul>
                        <li> <a href="/country/EM" target="_blank">European Union</a></li>
                        <li> <a href="/country/BX" target="_blank">Benelux – Belgium, Luxembourg, Netherlands</a></li>
                        <li> <a href="/country/OAPI" target="_blank">OAPI – select French-speaking African countries</a></li>
                        <li> <a href="/country/ARIPO" target="_blank">ARIPO – select English-speaking African countries</a></li>
                    </ul>

                    <p> Countries which recognize the Paris Convention are also considered a treaty region. This system, administered by the Paris Convention, allows for a single, streamlined application for multiple countries. Although very convenient, it is not a universal solution for worldwide trademark filing. Your trademark may face oppositions in certain countries, and individual trademark offices ultimately reserve the right to accept or reject applications at their own discretion.</p>

                    <h2> What is Priority Filing?</h2>

                    <p> Priority filing <i>(also known as priority right or right of priority)</i> is the time-limited right of the owner of a trademark to file subsequent applications in other jurisdictions for the same trademark. To claim this right, the subsequent applications must be filed within six months (not twelve months, as in the case of patents) of the submission date of the initial application. Priority filing applies only to countries which are members of the Paris Convention.</p>

                    <p> For example, a company may want to file a trademark in Germany and Canada. An application is made to Germany on January 1st, giving the applicant first priority across Paris Convention until June 30th. As long as the Canadian application is made by June 30th, it claims priority.</p>

                    <p> It is highly recommended that subsequent applications be filed as soon as possible. Certain countries have requirements for claiming priority, and arranging the necessary documentation can take time.</p>

                    <h2> What are Trademark Classes?</h2>

                    <p> Trademarks are classified under categories of goods and services. Known as “Nice Classification”, there are 45 classes <b>(1 to 34 for goods and 35 to 45 for services)</b> which are recognized by nearly every jurisdiction in the world. In most countries, your trademark may fall under multiple classes; however, in a few countries you are limited to one class per trademark.</p>

                    <p> Your trademark will only be protected under those classes which you specify at the time of application. To help you decide which classes best fit your trademark, please see the below dropdown list.</p>

                   	<p> <a href="/classes" target="_blank">Click here to browse trademark class descriptions.</a></p>

                    <p> <a href="/classes" target="_blank">Or feel free to use our very own Nice Classification search tool to find what products fit within which classes.</a></p>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
