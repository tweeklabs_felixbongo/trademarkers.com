@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form method="POST">
                        @csrf

                        <div class="form-group row">

                            <div class="col-md-10">
                                <input id="term" type="text" class="form-control" name="term" autofocus placeholder="Enter trademark term to search ..." value="{{$term}}">
                            </div>

                            <div class="col-md-2">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Search') }}
                                </button>
                            </div>
                        </div>

                       {{--  <div class="form-group row">
                            <label for="fdate" class="col-sm-4 col-form-label text-md-right">Filing Date From</label>

                            <div class="col-md-6">
                                <input id="fdate" type="date" class="form-control" name="fdate" autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="tdate" class="col-sm-4 col-form-label text-md-right">Filing Date To</label>

                            <div class="col-md-6">
                                <input id="tdate" type="date" class="form-control" name="tdate" autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="owner" class="col-sm-4 col-form-label text-md-right">Owner Name</label>

                            <div class="col-md-6">
                                <input id="owner" type="text" class="form-control" name="owner" autofocus placeholder="Owner Name">
                            </div>
                        </div> --}}
                    </form>
                </div>
            </div>
        </div>

        

        <div class="col-md-12" style="center;min-height:300px;">

            @if ($results) 
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th></th>
                        <th> Trademark</th>
                        <th> Status</th>
                        <th> Date</th>
                        <th> Serial #</th>
                        <th> Applicant</th>
                    </tr>
                </thead>

                <tbody>
                    
                    @foreach ( $results['records'] as $result )
                        @php 
                             
                        @endphp
                        @if ($result['_index'] == 'euipo') 
                        <tr> 
                            <td><span class="ico-eu">EUIPO</span></td>
                            <td>{{ $result['_source']['mark_text'] }}</td>
                            <td>
                                <p><b>{{ isset($result['_source']['status']) ? $result['_source']['status'] : "N/A" }}</b></p>
                            </td>
                            <td>
                                {{ date_create_from_format("Ymd",$result['_source']['application_date']) ? date_create_from_format("Ymd",$result['_source']['application_date'])->format('F j, Y') : "" }}
                            </td>
                            <td>
                                {{$result['_source']['application_number']}}    
                            </td>
                            <td>{{ $result['_source']['applicant_name'] }}</td>
                        </tr>
                        @endif

                        @if ($result['_index'] == 'uspto') 
                        <tr> 
                            <td><span class="ico-us">USPTO</span></td>
                            <td>{{ $result['_source']['mark_identification'] }}</td>
                            <td>
                                <p><b>{{ isset($statusList[$result['_source']['status_code']]) ? $statusList[$result['_source']['status_code']] : "Unknown" }}</b></p>
                                @php
                                $link = "http://tsdr.uspto.gov/#caseNumber=".(int)($result['_source']['serial_number'])."&caseSearchType=US_APPLICATION&caseType=DEFAULT&searchType=statusSearch";
                                @endphp
                                <p><a href="{{$link}}" target="_blank"> More Info</a></p>
                            </td>
                            <td>{{ date_create_from_format("Ymd",$result['_source']['filing_date']) ? date_create_from_format("Ymd",$result['_source']['filing_date'])->format('F j, Y') : "" }}</td>
                            
                            <td>
                                {{(int)($result['_source']['serial_number'])}}  
                            </td>
                            

                            <td>{{ $result['_source']['party_name'] }}</td>
                        </tr>
                        @endif
                    @endforeach

                </tbody>
                
            </table>

            <?php
            // config
            $link_limit = 7; // maximum number of links (a little bit inaccurate, but will be ok for now)
            ?>

            @if ($results['meta']['total_page'] > 1)
            <div class="row search-page">
                <div class="col-md-3">
                    <p>{{ $results['meta']['current_page'] }} of {{ $results['meta']['total_page'] }}</p>
                </div>
                <div class="col-md-9">
                    <ul class="pagination">
                        <li class="{{ ($results['meta']['total_page'] == 1) ? ' disabled' : '' }}">
                            <a href="?term={{$term}}&page=1"><<</a>
                        </li>
                        @for ($i = 1; $i <= $results['meta']['total_page']; $i++)
                            <?php
                            $half_total_links = floor($link_limit / 2);
                            $from = $results['meta']['current_page'] - $half_total_links;
                            $to = $results['meta']['current_page'] + $half_total_links;
                            if ($results['meta']['current_page'] < $half_total_links) {
                            $to += $half_total_links - $results['meta']['current_page'];
                            }
                            if ($results['meta']['total_page'] - $results['meta']['current_page'] < $half_total_links) {
                                $from -= $half_total_links - ($results['meta']['total_page'] - $results['meta']['current_page']) - 1;
                            }
                            ?>
                            @if ($from < $i && $i < $to)
                                <li class="{{ ($results['meta']['current_page'] == $i) ? ' active' : '' }}">
                                    <a href="?term={{$term}}&page={{ $i }}">{{ $i }}</a>
                                </li>
                            @endif
                        @endfor
                        <li class="{{ ($results['meta']['current_page'] == $results['meta']['total_page']) ? ' disabled' : '' }}">
                            <a href="?term={{$term}}&page={{$results['meta']['total_page']}}">>></a>
                        </li>
                    </ul>
                </div>
                <!-- <div class="col-md-3">
                    
                    <div class="form-group">
                        
                        <select id="per_page_opt" class="form-control" name="per_page_opt">
                            <option value="10">10</option>
                            <option value="20">20</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        <select>

                    </div>
                </div> -->
            </div>
                
            @endif

            
            @endif


            
        </div>
    </div>
</div>

@endsection
