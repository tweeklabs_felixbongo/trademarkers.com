@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    
                    <h2 class="text-center">USPTO OFFICE ACTION RESPONSE</h2>
                    <br>

                    <div class="row">
                        <div class="col-md-4">
                
                            <div class="text-center office-service">
                                <p class="office-headings bg-blue">Simple
                                    <br>
                                    Office Action Response
                                </p>
                                <p class="price"><sup>$</sup>{{ $simple->initial_cost }}</p>
                                <p class="header-space bg-blue"></p>

                                <p class="sub-heading">Response to all kinds of informality issues, such as:</p>
                                
                                <ul class="blue">
                                    <li>
                                        <span><i class="fa fa-check"></i></span>
                                        <p>Agreement to a disclaimer request</p>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-check"></i></span>
                                        <p>Improper Speciments</p>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-check"></i></span>
                                        <p>Amendment of identification of goods/services</p>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-check"></i></span>
                                        <p>Change/addition of an intentional class (+government fee for any additional classes)</p>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-check"></i></span>
                                        <p>Improper Entity Type</p>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-check"></i></span>
                                        <p>Translation of foreign words</p>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-check"></i></span>
                                        <p>Amendment of filing basis</p>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-check"></i></span>
                                        <p>Claims of ownership of other applications</p>
                                    </li>
                                </ul>
                                <form method="get" action="/service-order-form">
                                    <input type="hidden" name="service" value="Office Status - Simple">
                                    <input type="submit" class="btn btn-blue" value="GET STARTED"></a>
                                </form>
                            
                            </div> 
                            
                        </div>

                        <div class="col-md-4">
                
                            <div class="text-center office-service">
                                <p class="office-headings bg-green">Complex
                                    <br>
                                    Office Action Response
                                </p>
                                <p class="price"><sup>$</sup>{{ $complex->initial_cost }}</p>
                                <p class="header-space bg-green"></p>

                                <p class="sub-heading">Response to substantial refusal or argue against examiner's opinion, such as:</p>
                                
                                <ul class="blue">
                                    <li>
                                        <span><i class="fa fa-check"></i></span>
                                        <p>Refusal on basis of descriptiveness, geographic significance, surname, or other substantive objections</p>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-check"></i></span>
                                        <p>Assertion of acquired distinctiveness with arguments/evidence</p>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-check"></i></span>
                                        <p>Argument against a disclaimer request</p>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-check"></i></span>
                                        <p>Refusal on basis of ornamentation</p>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-check"></i></span>
                                        <p>Deadline missed but no later than 2 months after notice of abandonment issued</p>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-check"></i></span>
                                        <p>Additional Information about specimens</p>
                                    </li>
                                </ul>

                                <form method="get" action="/service-order-form">
                                    <input type="hidden" name="service" value="Office Status - Complex">
                                    <input type="submit" class="btn btn-green" value="GET STARTED"></a>
                                </form>
                            
                            </div> 
                            
                        </div>

                        <div class="col-md-4">
                
                            <div class="text-center office-service">
                                <p class="office-headings bg-violet">Likelihood of Confusion
                                    <br>
                                    Office Action Response
                                </p>
                                <p class="price"><sup>$</sup>{{ $likelihood->initial_cost }}</p>
                                <p class="header-space bg-violet"></p>
                                
                                <ul class="violet">
                                    <li>
                                        <span><i class="fa fa-check"></i></span>
                                        <p>Argument against the refusal based on likelihood of confusion with cited similar trademarks. We will normally draft 7 to 13 pages of argumentation to convinced the USPTO to approve your application.</p>
                                    </li>
                                    
                                </ul>

                                <form method="get" action="/service-order-form">
                                    <input type="hidden" name="service" value="Office Status - Likelihood">
                                    <input type="submit" class="btn btn-violet" value="GET STARTED"></a>
                                </form>
                            
                            </div> 
                            
                        </div>

                    </div>

                </div>
            </div>
        </div>
        
        
        
       

        </div>
    </div>
</div>

@endsection
