@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">

        @include('layouts.customer-nav', $invoice = [1])

        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            
                            <table class="table table-hover table-condensed">
                                
                                <thead>
                                    <tr>
                                        <td> Invoice #</td>
                                        <td> Status</td>
                                        <td> Date</td>
                                        <td> Reference #</td>
                                        <td colspan="2"> <i class="fa fa-cog"></i></td>
                                    </tr>
                                </thead>

                                <tbody>
                                    @if ( count( $user->invoices ) == 0 )
                                        <td colspan="4"> No Invoices yet!</td>
                                    @endif

                                    @foreach( $user->invoices->reverse() as $invoice )
                                        <tr>
                                            <td style="color:red;"> {{ $invoice->reference }}</td>
                                            <td> {{ $invoice['status'] }}</td>
                                            <td> {{ $invoice['created_at']->diffForHumans() }}</td>

                                            @if ( $invoice['status'] == 'pending' )
                                                <td>
                                                    <form method="POST"> 
                                                        <input type="hidden" name="id" value="{{ $invoice['id'] }}">
                                                        <input type="text" name="ref" class="invoice_ref" value="{{ $invoice['payment']->reference_no }}">
                                                    </form>
                                                </td>
                                            @else
                                                <td> {{ $invoice['payment_id'] == 0 ? "" : $invoice['payment']->reference_no}}</td>
                                            @endif

                                            <td>
                                                <a href="/customer/invoice/{{ $invoice->id }}/download=true&success=true" data-toggle="tooltip" title="Download Invoice"> <i class="fa fa-download"></i></a>
                                            </td>
                                            <td>
                                                <a href="/customer/invoice/{{ $invoice->id }}/download=false&success=true&view=true" target="_blank" data-toggle="tooltip" title="View Invoice"> <i class="fa fa-eye"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection