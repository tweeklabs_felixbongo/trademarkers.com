@extends('layouts.master')

@push('head')
<!-- Global site tag (gtag.js) - Google Ads: 851729164 --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-851729164"></script> 
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-851729164'); </script>
@endpush

@section('extra_css')
	<link href="{{ asset('css/store.css') }}" rel="stylesheet">
@endsection

@section('checkout_form')

<main class="py-4 con" style="background-color: white; margin-top: -10px;">
    <div class="container">
        
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        @if ( $error != "" )
                            <div style="text-align: center;" class="alert alert-danger" role="alert">
                                    {{ $error  }}
                            </div>
                        @endif

                        <p> <b>Order Summary</b></p>

                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <td style="color:red;"> Item #</td>
                                    <td> Country</td>
                                    <td> Service</td>
                                    <td> Type</td>
                                    <td> Classes</td>
                                    <td> Amount</td>
                                </tr>
                            </thead>

                            <tbody>
                                @php
                                    $count = 1;
                                    $paypalDescription = "";
                                    $signupDiscount = 0;
                                @endphp
                                @foreach( $contents as $content )
                                    @if ( $content->status == 'active' ) 
                                    @php 
                                        $paypalDescription .= $content->country->name . ' | ' . $content['service'] . ' | ' .  $content['type'] . '<br>';
                                        $signupDiscount += $content->total_discount;
                                    @endphp
                                        <tr>
                                            <td> <b>{{ str_pad( $count , 3, '0', STR_PAD_LEFT )  }}</b></td>

                                            @if ( $content->country )
                                                <td> <img src="{{ asset('images/' . $content->country->avatar) }}" /> {{ $content->country->name }}</td>
                                            @else
                                                <td> N/A</td>
                                            @endif
                                            
                                            <td> {{ $content['service'] }}</td>
                                            <td> {{ $content['type'] }}</td>
                                            <td> {{ $content['classes'] }}</td>
                                            <td> ${{ number_format($content['amount'],2) }}</td>
                                        </tr>

                                        @if ($content->coupons)
                                            @foreach( $content->coupons as $coupons )
                                                <tr style="color:#f00; font-style:italic">
                                                    <td colspan="3"></td>
                                                    <td>Discount</td>
                                                    <td>{{ $coupons->code->action_code->case_number }}</td>
                                                    @if ($coupons->code->max_discount) 
                                                    <td> - ${{ $content->amount * ($content->assigned_discount / 100) }} </td>
                                                    @else 
                                                    <td> - ${{ $content->amount * ($coupons->code->discount / 100) }} </td>
                                                    @endif
                                                </tr>
                                                
                                            @endforeach
                                        @endif

                                        @if ($content->campaign_id)
                                            <tr style="color:#f00; font-style:italic">
                                                <td colspan="3"></td>
                                                <td>Discount</td>
                                                <td>Campaign</td>
                                                <td> - ${{ $content->total_discount }} </td>
                                            </tr>
                                                
                                        @endif
                                            
                                        @php
                                            $count++;
                                        @endphp
                                    @endif
		                        @endforeach

                                @if ( $referral && $referral->apply == 'yes' )
                                    <tr>
                                        <td colspan="4"> 
                                            
                                        </td>
                                        <td><b>Signup Discount</b></td>
                                        <td  style="color: red;">${{ number_format( $signupDiscount, 2 ) }}</td>
                                    </tr>
                                @endif

                                <tr>
                                    <td colspan="4"> 
                                        
                                    </td>
                                    {{-- <td colspan="4">
                                    	<div class="column">Error</div>
                                    </td> --}}
                                    <td><b>Overall Total</b></td>
                                    <td id="total"  style="display: none">{{$total}}</td>
                                    <td  style="color: red;">${{ number_format( $total, 2 ) }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="main" class="loading container" style="margin-top:20px;">

        <div id="checkout" class="row" style="max-width:unset;">
            <div id="payment-request">
                <div id="payment-request-button"></div>
            </div>
            <div class="col-md-8">
            <form id="payment-form" method="POST" action="/checkout" style="margin: 0 -15px;">
                @csrf
                <input type="hidden" name="invoice" id="invoice">
                <input type="hidden" name="wechat" id="wechat-field">
                <input type="hidden" id="static_email" value="{{ auth()->user()->email }}">
                <input type="hidden" id="paypalDescription" value="{{ $paypalDescription }}">
                <input type="hidden" id="profileId" name="profileId" value="{{ $contents[0]->profile->id }}">

                <h4>Complete your payment details below</h4>

                
                    
                    <h2 class="heading">Billing Information</h2>


                    @if ( isset( $contents[0]->profile->first_name ) && isset( $contents[0]->profile->last_name ) )

                        @php $name = $contents[0]->profile->first_name . " " . $contents[0]->profile->last_name; @endphp

                    @else
                        @php $name = ""; @endphp

                    @endif

                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="name" class="col-md-10 col-form-label text-md-right">Choose Different Billing Profile</label>
                                <div class="col-md-2">
                                    <input type="checkbox" class="form-control" value="yes" id="diff-profile">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" id="select-profile-wrap" style="display:none">
                            <div class="form-group row">
                                <label for="name" class="col-md-3 col-form-label text-md-right">Profile</label>
                                <div class="col-md-9">
                                    <select id="select-profile" class="form-control">
                                        @foreach ($profiles as $prof)
                                            <option value="{{ $prof->id }}" {{ ($contents[0]->profile->id == $prof->id ? 'selected' : '') }}>{{ ($prof->nature == 'Company' ? $prof->nature : 'Personal') }} [{{$prof->first_name}}]</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    

                    <div class="form-group row">
                        <label for="name" class="col-md-3 col-form-label text-md-right">Name</label>
                        <div class="col-md-9">
                            <input id="full_name" type="text" class="form-control" name="name" value="{{ $name }}" required autofocus>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="address" class="col-md-3 col-form-label text-md-right">Address</label>
                        <div class="col-md-9">
                            <input id="address" type="text" class="form-control" name="address" value=" {{ isset( $contents[0]->profile->house ) ? $contents[0]->profile->house : ""}}" placeholder="185 Berry Street Suite 550" required autofocus>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="city" class="col-md-3 col-form-label text-md-right">City</label>
                        <div class="col-md-9">
                            <input id="city" type="text" class="form-control" name="city" value=" {{ isset( $contents[0]->profile->city ) ? $contents[0]->profile->city : ""}}" placeholder="San Francisco" required autofocus>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="state" class="col-md-3 col-form-label text-md-right">state</label>
                        <div class="col-md-9">
                            <input id="state" type="text" class="form-control" name="state" value=" {{ isset( $contents[0]->profile->state ) ? $contents[0]->profile->state : ""}}" placeholder="CA" required autofocus>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="state" class="col-md-3 col-form-label text-md-right">ZIP/Postal Code</label>
                        <div class="col-md-9">
                            <input id="postal_code" type="text" class="form-control" name="postal_code" value=" {{ isset( $contents[0]->profile->zip_code ) ? $contents[0]->profile->zip_code : ""}}" required autofocus>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="state" class="col-md-3 col-form-label text-md-right">Country</label>
                        <div class="col-md-9">
                            <select id="country" name="country" class="form-control" required autofocus>
                                    @foreach( $countries as $country )
                                    @if ( $contents[0]->profile->country )
                                        <option value="{{ $country->name }}" {{ $contents[0]->profile->country == $country->name ? 'selected' : '' }} > {{ $country->name }}</option>
                                    @else 
                                        <option value="{{ $country->name }}" {{ $geo->country == $country->name ? 'selected' : '' }} > {{ $country->name }}</option>
                                    @endif
                                        
                                    @endforeach
                            </select>
                        </div>
                    </div>



                    {{-- <p class="tip">Select another country to see different payment options.</p> --}}
                

                
                <button class="paymentbtn" type="submit" id="formbtn" style="display: none;">Pay</button>
            </form>

            </div>
            <div class="col-md-4">

                <section>

                    <h2 class="heading">Payment Method</h2>

                    <nav id="method">
                        <ul>
                            <li>
                                <input class="pm" type="radio" name="payment" id="payment-card" value="card" checked>
                                <label for="payment-card">Card</label>
                            </li>

                            <li>
                                <input class="pm" type="radio" name="payment" id="payment-paypal" value="paypal" >
                                <label for="payment-paypal">Paypal</label>
                            </li>

                            <li>
                                <input class="pm" type="radio" name="payment" id="payment-invoice" value="invoice">
                                <label for="payment-invoice">Invoice</label>
                            </li>
                        </ul>
                    </nav>

                </section>
            
                <div id="payment-buttons"> <!-- WRAP PAYMENT BUTTONS START -->
                <div id="card-payment">
                    <form action="/checkout" method="POST">
                        @csrf
                        <script
                            src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                            data-key="pk_live_XORR073H0R4crqE5ZWrtuPYi00Fwcl1bJS"
                            data-name="TRADEMARKERS LLC"
                            data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                            data-zip-code="true"
                            data-locale="auto">
                        </script>
                    </form>
                </div>

                <div id="paypal-payment" style="display: none;">
                     
                    <div id="paypal-button-container"></div>
                    
                    <form action="/checkout" method="post" id="frmTransaction" name="frmTransaction" id="frmTransaction">
                        
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <input type="hidden" name="orderID" id="paypalId" value="">
                        <input type="hidden" name="payerID" id="paypalPayerId" value="">
                        <input type="hidden" name="paypal" value="paypal">
                        <input type="hidden" id="paypalAmount" name="amount" value="{{$total}}">
                    </form>
                    

                </div>

                <div id="invoice-payment" style="display: none;">
                    <button class="paymentbtn" id="invoice-btn">Pay via Invoice</button>
                </div>


                <div id="card-errors" class="element-errors"></div>
                <div id="iban-errors" class="element-errors"></div>
                </div> <!-- WRAP PAYMENT BUTTONS END-->
                
                <div id="disabled-buttons-show" style="display:none">
                    <p><i>If you do not agree to the <strong>TRADEMARKERS LLC</strong> terms of service and privacy policy you can't proceed with your order</i></p>
                </div>
                
                <!-- DISCLOSURE -->
                <div class="disclosure ">
                    
                    <p class="policy">
                    <input type="checkbox" id="agree-policy" class="agree-policy" name="agree-policy" checked> I agree to the <b>Trademarkers LLC</b> <a href="/terms">Terms Of Service</a> and <a href="/privacy">Privacy Policy</a>
                    </p>
                    <p><img src="/images/credit_card_logos_14.gif" ><br><br><img src="/images/trust.png" ></p>
                    
                </div>

            </div>

            

            
        </div>

        <div id="confirmation">
            <div class="status processing">
                <h1>Completing your order…</h1>
                <p>We’re just waiting for the confirmation from your bank… This might take a moment but feel free to close this page.</p>
                <p>We’ll send your receipt via email shortly.</p>
            </div>
            <div class="status success">
                <h1>Thanks for your order!</h1>
                <p>Woot! You successfully made a payment with Stripe.</p>
                <p class="note">We just sent your receipt to your email address, and your items will be on their way shortly.</p>
            </div>
            <div class="status receiver">
                <h1>Thanks! One last step!</h1>
                <p>Please make a payment using the details below to complete your order.</p>
                <div class="info"></div>
            </div>
            <div class="status error">
                <h1>Oops, payment failed.</h1>
                <p>It looks like your order could not be paid at this time. Please try again or select a different payment option.</p>
                <p class="error-message"></p>
            </div>
        </div>
    </div>
</main>

@endsection

@section('extra_script')
    <!-- paypal script -->
    <script src="https://www.paypal.com/sdk/js?client-id={{$paypalId}}"></script>
	<!-- Library to render QR codes -->
	<script src="{{ asset('js/payments.js') }}" defer></script>
    <script src="{{ asset('js/pay.js') }}"></script>
@endsection