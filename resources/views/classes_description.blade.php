@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">

                  <form method="POST" action="/class_descriptions">
                    @csrf
                      <div class="form-group row">
                          <div class="col-md-12">
                              <input type="text" class="form-control" name="desc" required autofocus placeholder="Search your services/goods">
                          </div>
                      </div>

                      <span style="float: right; color: red;" role="alert">
                          <strong><i> {{ count( $descriptions ) }} results found. </i></strong>
                      </span>
                  </form>

                    <table class="table table-hover" style="text-align: center;">
                        <tbody>
                          @if ( count( $descriptions ) == 0 ) 

                            <tr> <td colspan="3" style="color: red;"> <b>No results found.</b></td></tr>

                          @else

                            @foreach ( $descriptions as $description )
                                <tr>
                                  <td style="color:red;">
                                    <b> Class {{ str_pad( $description->trademarkClass['id'], 2, '0', STR_PAD_LEFT ) }} </b>
                                  </td>
                                  <td style="color:red;">
                                    <b>{{ $description->trademarkClass['id'] * 10000 + $description['id'] }}</b>
                                  </td>
                                  
                                  <td> <b><i>{{ $description['description'] }}</i></b> </td>
                                </tr>
                            @endforeach
                          @endif
                        </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
