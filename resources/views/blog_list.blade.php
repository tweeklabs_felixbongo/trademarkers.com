@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">

    	<div class="col-md-12" style="text-align: center; padding: 10px 0px 40px 0px;">
    		<h2>Articles</h2>
    		<hr>
    	</div>
		@foreach( $blogList as $blog )
            <div class="col-md-4 blog-list" style="position: relative; left: 0; top: 0; margin-bottom: 40px;">
                @if ($blog->featured_img) 
                 <a href="/article/{{ $blog->slug }}">
                    <img src="{{url('/uploads/')}}/{{$blog->featured_img}}" alt="{{$blog->title}}" style="width:100%" />
                </a>
                @endif
                <a href="/article/{{ $blog->slug }}">
                    <h3>{{ $blog->title }}</h3>
                </a>

                @php
                    $description = strip_tags($blog->description);
                @endphp
                <p class="description">
                    {{ \Illuminate\Support\Str::limit($description, 150, $end='...') }}
                </p>

               
                <p>
                    <em> Author : {{$blog->author->name}} </em><br>
                    <em> Published on: {{date('M d, Y', strtotime($blog->created_at))}}</em>
                </p>
            </div>
        @endforeach
       
    </div>
</div>

@endsection

