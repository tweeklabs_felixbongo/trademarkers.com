<div class="box">
    <!-- /.box-header -->
    @if ( $errors->has('action_file') )
	    <div class="alert alert-danger">
	    	<strong>{{ $errors->first('action_file') }}</strong>
	    </div>
	@endif
    <!-- <div class="box-header with-border">

    	<div class="row">
    		<div class="col-md-2">
    			<div class="pull-left">
		    		<a href="#" class="btn btn-sm btn-info" title="Filter">
			    		<i class="fa fa-filter"></i>
			    		<span class="hidden-xs"> &nbsp;&nbsp; Filter</span>
			    	</a>
		    	</div>
    		</div>

    		

    		<div class="col-md-2">
    			<div class="pull-right">
		        	<button  class="btn btn-sm btn-success" data-toggle="modal" data-target="#upload" title="Upload">
		        		<i class="fa fa-upload"></i>
		        		<span class="hidden-xs"> &nbsp;&nbsp; Upload</span>
		        	</button>
		        </div>
    		</div>
    	</div>
    </div> -->

    <div class="row">
		<div class="col-md-12">
			<table class="table table-responsive">
				<tr>
					<th>Brand</th>
					<th>Domains</th>
					<th>Emails</th>
					<th>Actions</th>
				</tr>
				@foreach ($brands as $brand)
				<tr>
					<td>{{ $brand->brand }}</td>
					<td>{{ dd($brand->domains) }}</td>
					<td>emails</td>
					<td>action</td>
				</tr>
				@endforeach
			</table>
		</div>
	</div>

	
</div>