@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-9">
            <div class="row">
                <div class="col-md-12" style="text-align: center;">
                    <p style="font-size: 30px;">{{ $video->title }}</p>
                    <div class="card">
                        <div class="card-body">       
                            {!! $video['embed'] !!}
                        </div>
                    </div>
                </div>

                <div class="col-md-12" style="margin-top: 20px;">
                    <div class="card">
                        <div class="card-body">
                            {!! $video->description !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="row">
                <div class="col-md-11" style=" visibility: hidden; background-color: #eae4e4; height: 90px; margin-bottom: 20px;">
                </div>

                @foreach( $videolist as $list )
                    <div class="col-md-12" style="position: relative; left: 0; top: 0; margin-bottom: 10px;">
                        <a href="/{{ $list->slug }}">

                            <img src="{{ $list['thumbnail'] }}" width="100%" alt="" style="position: relative;">
                            <img src="/images/video.png" width="30%" style="position: absolute; bottom: 60px; left: 33%">
                        </a>

                        <a href="/{{ $list->slug }}"> 
                            {{ strlen( $list->title ) > 26? substr( $list->title, 0, 26) . '...' : $list->title }}
                        </a>
                    </div>
                @endforeach

                <div class="col-md-12" style="text-align: center; margin-top: 20px;">
                    <a href="/videos" class="btn btn-danger"> Show more</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
