@extends('layouts.master')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-12" style="text-align: center;">

			<div class="d-none d-md-none d-lg-block d-xl-block">
				<iframe width="60%" height="380" src="https://www.youtube.com/embed/GKoNHXdItAQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>

			<div class="d-block d-md-none d-lg-none d-xl-none">
				<iframe width="100%" height="200" src="https://www.youtube.com/embed/GKoNHXdItAQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>

			<div class="row" style="margin-top: 20px;">

				<div class="image-container col-sm-12 d-block d-md-none d-lg-none d-xl-none">
					<img class="canada-flag" src="/images/cms/canada-flag-2.jpg" alt="canada" width="100%">

					<div class="image-content" style="width: 93%; text-align: center;">
						<div class="p-image-content-mobile">
							<p> CANADA'S NEW</p>
							<p> TRADEMARK ACT TAKES</p>
							<p> EFFECT JUNE 17. 2019</p>
						</div>

						<div class="second-image-content-mobile">
							<p> HERE'S HOW TO AVOID THE</p>
							<p> PRICE HIKE!</p>
						</div>

						<button class="btn btn-default"><a href="https://tawk.to/chat/5c30b5417a79fc1bddf365e1/default" target="_blank" style="text-decoration: none; color: black;"> Chat With Our Trademark Experts</a> </button>
					</div>
				</div>

				<div class="image-container col-sm-12 d-none d-md-none d-lg-block d-xl-block">
					
					<img class="canada-flag" src="/images/cms/canada-flag-2.jpg" alt="canada" width="100%">

					<div class="image-content" style="text-align: center;">
						<div class="p-image-content">
							<p> CANADA'S NEW</p>
							<p> TRADEMARK ACT TAKES</p>
							<p> EFFECT JUNE 17. 2019</p>
						</div>

						<div class="second-image-content">
							<p> HERE'S HOW TO AVOID THE</p>
							<p> PRICE HIKE!</p>
						</div>

						<button class="btn btn-default"><a href="https://tawk.to/chat/5c30b5417a79fc1bddf365e1/default" target="_blank" style="text-decoration: none; color: black;"> Chat With Our Trademark Experts</a> </button>
					</div>
				</div>
			</div>

			<div class="row" style="text-align: center;">
				<div class="col-md-4">

					<img src="/images/cms/register.png" alt="Register" width="100%">

					<p><a href="/registration/step1/CA"> Multi-Class Trademark Registration</a></p>

					<p> Once the new law is in force, there will be an increase in the registration fees and added costs per additional class. The terms will be reduced by 33%.  NOW is the best time to apply for multi-class registrations to avoid massive increases!</p>

				</div>

				<div class="col-md-4">

					<img src="/images/cms/off.png" alt="50% Off" width="100%">

					<p><a href="/study/step1/CA">50% Off trademark study</a></p>
					<p> 
						Before filing your trademark in Canada have a Trademark Study to identify potential issues.
						Study must be initiated by June 10  to be completed in time.
					</p>
				</div>

				<div class="col-md-4">

					<img src="/images/cms/renew.png" alt="Renew" width="100%">

					<p><a href="/registration/step2/CA"> Trademark Renewal Application</a></p>

					<p>With the new Trademark Act it will be more expensive to renew Canadian registrations regardless of the number of classes.  In addition the registration term is significantly reduced for first time applicants and renewals.</p>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
