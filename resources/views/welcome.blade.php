@extends('layouts.master')

@section('content')

<!-- main block Desktop -->
<div class="row block-welcome d-none d-md-block d-lg-block d-xl-block">

	<div class="bg-darker"></div>
	<div class="col-12 ">
		<div class="network-box">
			<h2>Our network of Experienced Trademark Attorneys</h2>
			<span class="title-separator"></span>
			<p>in 195 Countries and Treaty Regions can provide you with fast, simple, and cost-efficient trademark filling services</p>
		</div>
	</div>

	<div class="col-12 ">
		
		<div class="network-box-form">
			<div id="continent" class="box-form">
				<p> TRADEMARKERS are specialists in handling the registration of your trademarks worldwide.</p>
				<p> Your one stop shop to have your brand registered by experienced attorneys in 195 countries and jurisdictions.</p>
				<p> <b> I would like to register a trademark in :</b></p>
				
				<form id="indexForm" class="form">
					<div class="row inlineFlex">
						<div class="col-sm-10 col-md-9">
							<select name="continent" class="form-control">
								<option value="US"> United States</option>
								@foreach ( $continents as $continent )
									<option value="{{ $continent['abbr'] }}">
										{{ $continent['name'] }}
									</option>
								@endforeach
							</select>
						</div>

						<div class="col-sm-2 col-md-3">
							<button class="btn btn-primary">Go</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- main block Mobile -->
<div class="container  d-block d-md-none d-lg-none d-xl-none">
	<div class="row">
		<div class="col-md-12">

			<div class="row">

		
				<div class="col-12 block-page-title">
					<h2>Our network of Experienced Trademark Attorneys</h2>
					<span class="title-separator"></span>
					<img src="/images/bg_tks_blank.jpg" class="bgImage" />
					<p>in 195 Countries and Treaty Regions can provide you with fast, simple, and cost-efficient trademark filling services</p>
				</div>
				<div class="col-12">
					<div id="continent">
						<p> TRADEMARKERS are specialists in handling the registration of your trademarks worldwide.</p>
						<p> Your one stop shop to have your brand registered by experienced attorneys in 195 countries and jurisdictions.</p>
						<p> <b> I would like to register a trademark in :</b></p>
						
						<form id="indexForm" class="form">
							<div class="row inlineFlex">
								<div class="col-sm-10 col-md-9">
									<select name="continent" class="form-control">
										<option value="US"> United States</option>
										@foreach ( $continents as $continent )
											<option value="{{ $continent['abbr'] }}">
												{{ $continent['name'] }}
											</option>
										@endforeach
									</select>
								</div>

								<div class="col-sm-2 col-md-3">
									<button class="btn btn-primary">Go</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<!--  block about -->
<div class="bg-gray">
	<div class="container">
		<div class="row about-block align-items-center">
			<div class="col-12 col-md-6">
				<h2>Trademark Registration Experience</h2>
				<span class="title-separator"></span>
				<br>
				<p>We have a team of lawyers, with years of experience in Trademark Registrations, which allows us to assure you:</p>
				<ul>
					<li>Timely information – register faster</li>
					<li>Deal with Trademark objections as required</li>
					<li>Transparent status updates – no hidden fees</li>
					<li>Absolute confidentiality through online portal</li>
					<li>Competent professionals – Register Lawyers</li>
				</ul>
			</div>

			<div class="col-12 col-md-6 text-center">
				<img src="/images/img19.jpg" class="img-fluid" />
			</div>

			

		</div>

		<div class="col-12 text-center">
			<a href="/countries" class="btn btn-danger" style="font-size:21px;margin-bottom:30px;">Register Trademark</a>
		</div>

	</div>
</div>



<!--  block about -->
<div class="bg-gray2">
	<div class="container">
		<div class="row about-block align-items-center pull-image-right">
			

			

			<div class="col-12 col-md-6">
				<img class="img-fluid"  src="/images/img20.jpg" />
			</div>

			<div class="col-12 col-md-6">
				<h2>What We Can Do For You</h2>
				<span class="title-separator"></span>
				<br>
				<p>TradeMarkers provides trademark and intellectual property protection for brands used in the United Kingdom and internationally. We work with a wide range of clients, from small businesses establishing a corporate identity to large companies with a portfolio of brands.</p>
				<ul>
					<li> <a href="/countries">Trademark Study</a></li>
					<li> <a href="/countries">Trademark Registration</a></li>
					<li> <a href="/monitoring-service">Trademark Monitoring</a></li>
                    <li> <a href="/office-service">Office Action Response</a></li>
				</ul>
			</div>

			

			


		</div>

		<div class="col-12 text-center">
			<a href="/countries" class="btn btn-danger" style="font-size:21px;margin-bottom:30px;">Register Trademark</a>
		</div>

	</div>
</div>

<!--  block video -->
<div class="bg-white">
	<div class="container">
		<div class="row about-block align-items-center">
			

			

			

			<div class="col-12 col-md-6">
				<h2>What is a Trademark? How Do I Register One?</h2>
				<span class="title-separator"></span>
				<br>
				<div class="embed-responsive embed-responsive-16by9">
					<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/1Ga6zI-GPZM" allowfullscreen></iframe>
				</div>
			</div>

			<div class="col-12 col-md-6">
				<h2>Trademark Classes - All You Need to Know!</h2>
				<span class="title-separator"></span>
				<br>
				<div class="embed-responsive embed-responsive-16by9">
					<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/ZkgvkoGbNls" allowfullscreen></iframe>
				</div>
			</div>

			<div class="col-12 text-center">
			<br>
				<a href="/videos" class="btn btn-danger" style="font-size:21px;">More Videos</a>
			</div>

			

			<!-- <div class="col-12 col-md-6">
				<img class="img-fluid"  src="/images/img20.jpg" />
			</div> -->

			


		</div>
	</div>
</div>

<!--  block about -->
<div class="bg-contact">
	<div class="container">
		<div class="row about-block">
			

			<div class="col-12 col-md-4 ">

				<h2>Talk to an expert</h2>
				<span class="title-separator"></span>
				<br>
				<p>We've got phone and chat support available for you:</p>

				<p style="">
				Monday - Friday : 7am to 7pm (EST)<br>
				Saturday : Closed<br>
				Sunday : Closed</p>

				<table style="margin-bottom: 10px; text-align: right; line-height: 30px; font-weight: bold">
					<tr>
						<td>Toll-Free:</td>
						<td ><b style="color: #1475A9;">1-800-293-3166</b></td>
					</tr>
					<tr>
						<td>Office:</td>
						<td><b style="color: #1475A9;">212-468-5500</b></td>
					</tr>
					<tr>
						<td>Fax:</td>
						<td><b style="color: #1475A9;">212-504-0888</b></td>
					</tr>
				</table>

				<a class="btn btn-success" href="javascript:$zopim.livechat.window.show();">
					<span class="fa fa-comments"></span> Chat Now
				</a>
				
			</div>

			<div class="col-12 col-md-8">
			
				<div class="contact-div">
					<h2>We can do it all. Get in touch for a free consultation.</h2>
					<span class="title-separator"></span>
					<br>
				</div>
				<form method="POST" action="/contact">
					@csrf
					<div class="form-group">
						<label for="name"><b>Name:</b></label>
						<input type="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" value="{{ old('name') }}" placeholder="Enter name" name="name" required>

						@if ($errors->has('name'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('name') }}</strong>
							</span>
						@endif
					</div>
					<div class="form-group">
						<label for="email"><b>Email:</b></label>
						<input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" id="email" placeholder="Enter email" name="email" required>

						@if ($errors->has('email'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
						@endif
					</div>
					<div class="form-group">
						<label for="phone"><b>Phone:</b></label>
						<input type="text"  class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" value="{{ old('phone') }}" id="phone" placeholder="Enter phone" name="phone" required>

						@if ($errors->has('phone'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('phone') }}</strong>
							</span>
						@endif
					</div>
					<div class="form-group">
						<label for="msg"><b>Message:</b></label>
						<textarea class="form-control{{ $errors->has('message') ? ' is-invalid' : '' }}" value="{{ old('message') }}" rows="7" name="message" required></textarea>

						@if ($errors->has('message'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('message') }}</strong>
							</span>
						@endif
					</div>

					<button type="submit" class="btn btn-danger">Submit</button>
				</form>

			</div>
		</div>
	</div>
</div>


@endsection
