@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">

                <div class="card-body">

                    @php $value = $id != ""? "block" : "none"; @endphp

                    @if ( $data )
                        <div style="text-align: center;" class="alert alert-success" role="alert">
                            <p> Case Number: {{ $data->action_code }} </p>
                            <p> Order Number: {{ $data->order_code }}</p>
                            <p> Customer Number: {{ $data->customer_code }}</p>
                        </div>
                    @endif

                    @if ( $flag === false && !$data )
                        <div style="text-align: center;" class="alert alert-danger" role="alert" >
                            <p> Case Number: {{ "Sequential ID is very high!" }} </p>
                        </div>
                    @endif

                    @if ( $id != "" )
                        <div style="text-align: center;" class="alert alert-info" role="alert">
                            <p> {{ $id }}</p>
                        </div>
                    @endif

                    <form method="POST" action="{{ route('algorithm') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="trademark_id" class="col-md-4 col-form-label text-md-right">{{ __('Sequential ID') }}<span class="asterisk"> *</span></label>

                            <div class="col-md-6">
                                <input id="trademark_id" type="text" class="form-control{{ $errors->has('trademark_id') ? ' is-invalid' : '' }}" name="trademark_id" value="{{ old('trademark_id') }}" autofocus>

                                @if ($errors->has('trademark_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('trademark_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Generate') }}
                                </button>
                            </div>
                        </div>
                    </form>

                    <hr>

                    <div class="row">

                        <div class="col-md-12">

                            <form method="POST" action="/algo1">
                                @csrf

                                <div class="form-group row">
                                    <label for="code" class="col-md-4 col-form-label text-md-right">{{ __('Code') }}<span class="asterisk"> *</span></label>

                                    <div class="col-md-6">
                                        <input id="code" type="text" class="form-control{{ $errors->has('code') ? ' is-invalid' : '' }}" name="code" value="{{ old('code') }}" autofocus>

                                        @if ($errors->has('code'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('code') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Get Sequential ID') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
