@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                
                  <!-- FETCH CART ITEMS -->
                  <div id="tbl-cart-items"></div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@php
    $count = 1;
    $itemNotice = null;
@endphp
@foreach( $contents as $content )
    @php
        if ( $content->email_status_show == 'no' && $content->email_status_sent == 'yes'){
            $itemNotice = $content;
        } 
    @endphp
@endforeach
@if ($itemNotice)
<div class="modal" id="myModal" data-id="{{$itemNotice->id}}" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Notification</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {!! $itemNotice->email_content !!}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@endif


@endsection
