@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row justify-content-center" style="font-size: 15px;">

        @if ( !is_array( $trademark ) )
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                         <form method="POST" action="/legal/case/add_cart">
                            @csrf
                            <input type="hidden" id="legal_case_id" name="legal_case_id" value="{{ $trademark->case_number }}">

                            Case number: <b>{{ $trademark->case_number }}</b>

                            @include('layouts.case-register-button', ['trademark' => $trademark])

                        </form>
                    </div>
                </div>
            </div>

  
            @if( $trademark->action_code_type_id == 8 || $trademark->action_code_type_id == 7 )

                    <div class="col-md-12" style="margin-top: 20px;"> 
                        <div class="card">
                            <div class="card-body">
                                @if ($header) 
                                    <div class="headContent">{!! $header !!}</div>
                                @endif
                                <table class="table" style="border-bottom:1px solid #dee2e6; margin-bottom : 30px;">

                                    <tr>
                                        <td class="text-center" style="background:#dee2e6" colspan="2"> <b>Trademark Information</b></td>
                                    </tr>

                                    <tr>
                                        <td> <b>Trademark</b></td>
                                        <td> <i>{{ $trademark->case->trademark }}</i></td>
                                    </tr>

                                    <tr>
                                        <td> <b>Applicant</b></td>
                                        <td> <i>{{ $trademark->case->tm_holder }}</i></td>
                                    </tr>

                                    <tr>
                                        <td class="text-center" style="background:#dee2e6" colspan="2"> <b>Class Information</b></td>
                                    </tr>
                                    <tr>
                                        <td> <b>Class Number</b></td>
                                        <td> <b>Description</b></td>
                                    </tr>
                                    @if ($trademark->case && $trademark->case->class_number)
                                        @php
                                            $classes = explode(",", $trademark->case->class_number[0]);
                                        @endphp
                                        @foreach ($classes as $classNumber)
                                        <tr>
                                            <td> <i>{{ $classNumber }}</i> </td>
                                            <td> <i>{{ $trademark->classes[$classNumber] }}</i> </td>
                                        </tr>
                                        @endforeach

                                        @else 
                                        <tr>
                                            <td colspan="2">No class found!</td>
                                        </tr>
                                    @endif

                                </table>

                                <table class="table" style="border-bottom:1px solid #dee2e6; margin-bottom : 30px;">
                                </table>

                                
                                @if ($footer) 
                                    {!! $footer !!}
                                @else 
                                    <div style="margin: 0px auto; text-align: center; margin-top: 40px;">
                                        <p> Since you have filed NISP in the US on <b>{{ $trademark->case->tm_filing_date->format('F m, Y') }}</b>, in effect, you have until <b>{{ $trademark['filing_exp_date']->format('F m, Y') }}</b> to file an application for the same trademark in any of the listed countries herein, and still claim the same filing date of <b>{{ $trademark->case->tm_filing_date->format('F m, Y') }}</b>.</p>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
            @endif

            @if( $trademark->action_code_type_id == 9 )

                    <div class="col-md-12" style="margin-top: 20px;"> 
                        <div class="card">
                            <div class="card-body">
                                <table style="margin: 0px auto; border-collapse:separate; border-spacing:20px 10px;width:100%">
                                    <tr>
                                        <td style="width:30%;"> <b>Service</b></td>
                                        <td style="width:70%;"> {{ $trademark->trademark[0]->service }} </td>
                                    </tr>

                                    <tr>
                                        <td> <b>Service Type</b></td>
                                        <td> {{ $trademark->trademark[0]->type }}</td>
                                    </tr>

                                    <tr>
                                        <td> <b>Class(es)</b></td>
                                        <td> {{ $trademark->trademark[0]->classes }}</td>
                                    </tr>

                                    <tr>
                                        <td> <b>Class Description(s)</b></td>
                                        <td> {!! $trademark->trademark[0]->classes_description !!}</td>
                                    </tr>

                                    <tr style="padding-top: 30px;">
                                        <td> <b>Service Amount</b></td>
                                        <td> {{ $trademark->trademark[0]->amount }}</td>
                                    </tr>
                                </table>

                                
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12" style="margin-top: 20px;"> 
                        <div class="card">
                            <div class="card-body">
                                
                                <h4>Summary:</h4>
                                <p>{!!  $trademark->actionCart->cart->deatailed_contents !!}</p>
                            </div>
                        </div>
                    </div>
            @endif


            @if ( !Auth::check() )
                @if( $trademark->action_code_type_id == 9 && $trademark->actionCart->status == 'pending')
                <p style="width: 100%;text-align: right;padding: 15px 15px 0px 0px;"> <a class="btn btn-primary" href="/cart?action=addCart&itemId={{$trademark->actionCart->cart_id}}"> Login / Add to cart</a></p>
                @else
                    <!-- <div class="col-md-12" style="margin-top: 30px;">
                        <form method="POST" class="form-inline" action="/create/account" >
                            @csrf
                            <input type="hidden" name="trademark_case" value="{{ $trademark->case_number }}">
                            <div class="form-group mx-sm-3 mb-2">
                                <label for="name" class="sr-only">Name</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Your Name">
                            </div>
                            <div class="form-group mb-2" style="margin-right: 10px;">
                                <label for="staticEmail2" class="sr-only">Email</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Your Email Address">
                            </div>
                            
                            <button type="submit" class="btn btn-primary mb-2">Submit</button>
                        </form>
                    </div> -->
                    
                    <p style="
                        margin: 10px;
                        display: block;
                        width: 100%;
                    "></p>
                    @include('layouts.case-register-button', ['trademark' => $trademark])
                    <br>
                    
                @endif
            @else  

                <!-- @if( $trademark->action_code_type_id == 8 || $trademark->action_code_type_id == 7 )

                    <div class="col-md-12" style="margin-top: 20px;"> 
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group row" style="margin-bottom: 30px;">
                                    <label for="pcountry" class="col-sm-3 col-form-label text-md-right">{{ __('Country') }}</label>

                                    <div class="col-md-6">
                                        
                                        <select id="pselect" name="pcountry" class="form-control">
                                            <option value="-1"> Select Country</option>

                                            @foreach( $countries as $country )
                                                <option value="{{ $country['id'] }}"> {{ $country->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-md-3">
                                        <button id="pbtn" class="btn btn-info" style="display: none;"> Add</button>
                                    </div>
                                </div>

                                @foreach( $countries as $country )
                                    <form class="pFormPriority" method="POST" style="display: none;" id="form{{ $country->id }}">
                                        <input id="pCountryField{{ $country->id }}" type="hidden" name="priorityForm" class="pCountryClass" value="{{ $country->id }}">
                                        <input type="hidden" id="pPriorityCost{{ $country->id }}" value="{{ isset( $country->prioritycost ) ? $country->prioritycost->amount : 0  }}">

                                        @foreach( $country->prices as $price)

                                            @if ( $price->service_type == "Registration" ) 

                                                <input type="hidden" id="pInitialCost{{ $country->id }}" value="{{ $price->initial_cost }}">

                                                <input type="hidden" id="pAdditionalCost{{ $country->id }}" value="{{ $price->additional_cost }}">
                                            @endif
                                        @endforeach
                                        

                                        <div class="form-group row"> 
                                            <label for="{{ $country->name }}" class="col-sm-2 col-form-label text-md-left"><img src="../images/{{ $country->avatar }}" alt="{{ $country->name }}" />&nbsp;&nbsp;{{ $country->name }}</label>

                                            <div class="col-md-5">
                                                <div class="classes_container">
                                                    @for ( $i = 0; $i < count( $trademark['priority_classes'] ); $i++ ) 
                                                        <div class="form-check form-check-inline">
                                                            <table>
                                                                <tr>
                                                                    <td> 
                                                                        <input class="p{{$trademark['priority_classes'][$i]}} form-check-input class_chk_p" type="checkbox" value="{{$trademark['priority_classes'][$i]}}" name="class[]" checked>
                                                                        <label class="form-check-label" for="class{{$trademark['priority_classes'][$i]}}">
                                                                            {{ $trademark['priority_classes'][$i] }}
                                                                        </label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    @endfor
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <input class="form-control pAmountField" id="pPriceField{{ $country->id }}" readonly type="text" value="{{ 100 }}">
                                            </div>

                                            <div class="col-md-2">
                                                <a data-id="{{ $country->id }}" class="paddbtn btn btn-danger"> Add to Cart</a>
                                            </div>

                                            <div class="col-md-1">
                                                <a data-id="{{ $country->id }}" class="btn btn-danger premovebtn"> X</a>
                                            </div>
                                        </div>
                                    </form>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif -->

                @if ($trademark->action_code_type_id == 9 && $trademark->actionCart->status == 'pending') 
                    <p style="width: 100%;text-align: right;padding: 15px 15px 0px 0px;"> <a class="btn btn-primary" href="/cart?action=addCart&itemId={{$trademark->actionCart->cart_id}}"> Add to cart</a></p>
                @else 
                    @include('layouts.case-register-button', ['trademark' => $trademark])
                @endif

            @endif
        @endif
    </div>
</div>

@endsection
