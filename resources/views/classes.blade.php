@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">

                  <form method="POST" action="/class_descriptions">
                    @csrf
                      <div class="form-group row">
                          <div class="col-md-12">
                              <input type="text" class="form-control" name="desc" required autofocus placeholder="Search your services/goods">
                          </div>
                      </div>
                  </form>
                    <table class="table table-hover">
                        <tbody>
                          @foreach ( $classes as $class )
                            <tr>
                              <td> 
                                <a href="/class_descriptions?class={{ $class['id'] }}">
                                  <b>
                                    Class {{ str_pad( $class['id'], 2, '0', STR_PAD_LEFT ) }}
                                  </b>
                                </a>
                              </td>
                              
                              <td> <a href="/class_descriptions?class={{ $class['id'] }}" style="color: black;"><b>{{ $class['name'] }}</b></a> </td>
                            </tr>
                          @endforeach
                        </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
