@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    @if ( strlen( $message ) > 0 )
                        <div style="text-align: center;" class="alert alert-info" role="alert">
                            {{ $message }}
                        </div>
                    @endif

                    <h2>Contact Us</h2>
                    <form method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="name"><b>Name:</b></label>
                            <input type="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" value="{{ old('name') }}" required placeholder="Enter name" name="name">

                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="email"><b>Email:</b></label>
                            <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" id="email" placeholder="Enter email" name="email" required>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="phone"><b>Phone:</b></label>
                            <input type="text"  class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" value="{{ old('phone') }}" id="phone" placeholder="Enter phone" name="phone" >

                            @if ($errors->has('phone'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="msg"><b>Message:</b></label>
                            <textarea class="form-control{{ $errors->has('message') ? ' is-invalid' : '' }}" value="{{ old('message') }}" rows="7" name="message" required></textarea>

                            @if ($errors->has('message'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('message') }}</strong>
                                </span>
                            @endif
                        </div>

                        <button type="submit" class="btn btn-danger">Submit</button>
                        @honeypot
                    </form>
                    <hr>

                    <div class="row">
                        <div class="col-md-8">
                            <h6> <b>Trademarkers LLC</b></h6>
                            <h6> 45 Essex Street, Suite 202</h6>
                            <h6> Millburn NJ 07041</h6>
                        </div>

                        <div class="col-md-4" style="float: right;">
                            <h6> <b>Email Us</b></h6>
                            <h6> info@trademarkers.com</h6>
                            <h6> <b>Toll-Free:</b> 1-800-293-3166</h6>
                            <h6> <b>Office:</b> 212-468-5500</h6> 
                            <h6> <b>Fax:</b> 212-504-0888</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
