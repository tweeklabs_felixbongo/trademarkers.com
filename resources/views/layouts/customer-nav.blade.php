<div class="col-md-3">
    <div class="card">
        <div class="card-header">
            @php
                if ( !isset($user) ) {
                    $user = Auth::user();
                }
            @endphp
            <h5 style="margin-bottom:0;">{{ $user->role->name }} Dashboard</h5>
        </div>
        <div class="card-body">
        
            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
            
              <a class="nav-link {{ isset($dashboard[0]) ? 'active' : '' }}" href="/customer/dashboard" role="tab" >Dashboard</a>
              <a class="nav-link {{ isset($profile[0]) ? 'active' : '' }}" href="/profiles" role="tab">Profiles</a>
              <a class="nav-link {{ isset($trademark[0]) ? 'active' : '' }}" href="/customer/trademarks" role="tab">Trademarks <span class="badge-danger" style="float: right; padding-left: 10px; padding-right: 10px; border-radius: 50%;"> {{ $trademark_count }}</span></a>
              <a class="nav-link {{ isset($order[0]) ? 'active' : '' }}" href="/customer/orders/all" role="tab" >Orders</a>
              <a class="nav-link {{ isset($invoice[0]) ? 'active' : '' }}" href="/customer/invoices" role="tab" >Invoices</a>
              
              @if ($user->role->slug == 'affiliate')
              <a class="nav-link {{ isset($referral[0]) ? 'active' : '' }}" href="/customer/referral" role="tab" >Referral</a>
              @endif

            </div>
        </div>
    </div>
</div>