<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html style="width:100%;font-family:arial, 'helvetica neue', helvetica, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;">
 <head> 
  <meta charset="UTF-8"> 
  <meta content="width=device-width, initial-scale=1" name="viewport"> 
  <meta name="x-apple-disable-message-reformatting"> 
  <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
  <meta content="telephone=no" name="format-detection"> 
  <title>{{ $title }}</title> 

  <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i" rel="stylesheet"> 
   
 <style type="text/css">
	@media only screen and (max-width:600px){
		p,ul li,ol li,a{
			font-size:16px!important;
			line-height:150%!important;
		}

}	@media only screen and (max-width:600px){
		h1{
			font-size:24px!important;
			text-align:center;
			line-height:120%!important;
		}

}	@media only screen and (max-width:600px){
		h2{
			font-size:22px!important;
			text-align:center;
			line-height:120%!important;
		}

}	@media only screen and (max-width:600px){
		h3{
			font-size:18px!important;
			text-align:center;
			line-height:120%!important;
		}

}	@media only screen and (max-width:600px){
		h1 a{
			font-size:24px!important;
		}

}	@media only screen and (max-width:600px){
		h2 a{
			font-size:22px!important;
		}

}	@media only screen and (max-width:600px){
		h3 a{
			font-size:18px!important;
		}

}	@media only screen and (max-width:600px){
		.es-menu td a{
			font-size:16px!important;
		}

}	@media only screen and (max-width:600px){
		.es-header-body p,.es-header-body ul li,.es-header-body ol li,.es-header-body a{
			font-size:16px!important;
		}

}	@media only screen and (max-width:600px){
		.es-footer-body p,.es-footer-body ul li,.es-footer-body ol li,.es-footer-body a{
			font-size:14px!important;
		}

}	@media only screen and (max-width:600px){
		.es-infoblock p,.es-infoblock ul li,.es-infoblock ol li,.es-infoblock a{
			font-size:12px!important;
		}

}	@media only screen and (max-width:600px){
		*[class=gmail-fix]{
			display:none!important;
		}

}	@media only screen and (max-width:600px){
		.es-m-txt-c,.es-m-txt-c h1,.es-m-txt-c h2,.es-m-txt-c h3{
			text-align:center!important;
		}

}	@media only screen and (max-width:600px){
		.es-m-txt-r,.es-m-txt-r h1,.es-m-txt-r h2,.es-m-txt-r h3{
			text-align:right!important;
		}

}	@media only screen and (max-width:600px){
		.es-m-txt-l,.es-m-txt-l h1,.es-m-txt-l h2,.es-m-txt-l h3{
			text-align:left!important;
		}

}	@media only screen and (max-width:600px){
		.es-m-txt-r img,.es-m-txt-c img,.es-m-txt-l img{
			display:inline!important;
		}

}	@media only screen and (max-width:600px){
		.es-button-border{
			display:block!important;
		}

}	@media only screen and (max-width:600px){
		a.es-button{
			font-size:20px!important;
			display:block!important;
			border-left-width:0px!important;
			border-right-width:0px!important;
		}

}	@media only screen and (max-width:600px){
		.es-btn-fw{
			border-width:10px 0px!important;
			text-align:center!important;
		}

}	@media only screen and (max-width:600px){
		.es-adaptive table,.es-btn-fw,.es-btn-fw-brdr,.es-left,.es-right{
			width:100%!important;
		}

}	@media only screen and (max-width:600px){
		.es-content table,.es-header table,.es-footer table,.es-content,.es-footer,.es-header{
			width:100%!important;
			max-width:600px!important;
		}

}	@media only screen and (max-width:600px){
		.es-adapt-td{
			display:block!important;
			width:100%!important;
		}

}	@media only screen and (max-width:600px){
		.adapt-img{
			width:100%!important;
			height:auto!important;
		}

}	@media only screen and (max-width:600px){
		.es-m-p0{
			padding:0px!important;
		}

}	@media only screen and (max-width:600px){
		.es-m-p0r{
			padding-right:0px!important;
		}

}	@media only screen and (max-width:600px){
		.es-m-p0l{
			padding-left:0px!important;
		}

}	@media only screen and (max-width:600px){
		.es-m-p0t{
			padding-top:0px!important;
		}

}	@media only screen and (max-width:600px){
		.es-m-p0b{
			padding-bottom:0!important;
		}

}	@media only screen and (max-width:600px){
		.es-m-p20b{
			padding-bottom:20px!important;
		}

}	@media only screen and (max-width:600px){
		.es-mobile-hidden,.es-hidden{
			display:none!important;
		}

}	@media only screen and (max-width:600px){
		.es-desk-hidden{
			display:table-row!important;
			width:auto!important;
			overflow:visible!important;
			float:none!important;
			max-height:inherit!important;
			line-height:inherit!important;
		}

}	@media only screen and (max-width:600px){
		.es-desk-menu-hidden{
			display:table-cell!important;
		}

}	@media only screen and (max-width:600px){
		table.es-table-not-adapt,.esd-block-html table{
			width:auto!important;
		}

}	@media only screen and (max-width:600px){
		table.es-social{
			display:inline-block!important;
		}

}	@media only screen and (max-width:600px){
		table.es-social td{
			display:inline-block!important;
		}

}		#outlook a{
			padding:0;
		}
		.ExternalClass{
			width:100%;
		}
		.ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{
			line-height:100%;
		}
		.es-button{
			mso-style-priority:100!important;
			text-decoration:none!important;
		}
		a[x-apple-data-detectors]{
			color:inherit!important;
			text-decoration:none!important;
			font-size:inherit!important;
			font-family:inherit!important;
			font-weight:inherit!important;
			line-height:inherit!important;
		}
		.es-desk-hidden{
			display:none;
			float:left;
			overflow:hidden;
			width:0;
			max-height:0;
			line-height:0;
			mso-hide:all;
		}
		.es-button-border:hover a.es-button{
			background:#7dbf44!important;
			border-color:#7dbf44!important;
		}
		.es-button-border:hover{
			background:#7dbf44!important;
			border-color:#7dbf44 #7dbf44 #7dbf44 #7dbf44!important;
		}
		td .es-button-border:hover .es-button-1{
			background:#7dbf44!important;
			border-color:#7dbf44!important;
		}
		td .es-button-border-2:hover{
			background:#7dbf44!important;
		}
		td .es-button-border:hover a.es-button-3{
			background:#1994d6!important;
			border-color:#1994d6!important;
		}
		td .es-button-border-4:hover{
			background:#1994d6!important;
		}
		td .es-button-border:hover a.es-button-5{
			background:#1475a9!important;
			border-color:#1475a9!important;
		}
		td .es-button-border-6:hover{
			background:#1475a9!important;
		}
</style></head> 
 <body style="width:100%;font-family:arial, 'helvetica neue', helvetica, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;"> 
  <div class="es-wrapper-color" style="background-color:#F6F6F6;"> 
   <!--[if gte mso 9]>
			<v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
				<v:fill type="tile" color="#f6f6f6"></v:fill>
			</v:background>
		<![endif]--> 
   <table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top;"> 
     <tr style="border-collapse:collapse;"> 
      <td valign="top" style="padding:0;Margin:0;"> 
       <table cellpadding="0" cellspacing="0" class="es-header" align="center" mc:repeatable style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top;"> 
         <tr style="border-collapse:collapse;"> 
          <td align="center" bgcolor="#ffffff" style="padding:0;Margin:0;background-color:#FFFFFF;"> 
           <table class="es-header-body" width="600" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;"> 
             <tr style="border-collapse:collapse;"> 
              <td align="left" style="padding:0;Margin:0;padding-top:15px;padding-left:20px;padding-right:20px;"> 
               <!--[if mso]><table width="560" cellpadding="0" cellspacing="0"><tr><td width="270" valign="top"><![endif]--> 
               <table class="es-left" cellspacing="0" cellpadding="0" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td class="es-m-p20b" width="270" align="left" style="padding:0;Margin:0;"> 
                   <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" style="padding:0;Margin:0;padding-bottom:5px;font-size:0px;" mc:edit="block_0"><a target="_blank" href="https://www.trademarkers.com/BB-0F2B-R" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:16px;text-decoration:underline;color:#659C35;"><img src="https://fcttwq.stripocdn.email/content/guids/CABINET_369a512c342a1675d6f2209fa716c5a9/images/34311585809437677.jpg" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;" class="adapt-img" width="270"></a></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table> 
               <!--[if mso]></td><td width="20"></td><td width="270" valign="top"><![endif]--> 
               <table class="es-right" cellspacing="0" cellpadding="0" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td width="270" align="left" style="padding:0;Margin:0;"> 
                   <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td style="padding:0;Margin:0;" mc:edit="block_1"> 
                       <table class="es-menu" width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                         <tr class="links" style="border-collapse:collapse;"> 
                          <td style="Margin:0;padding-left:5px;padding-right:5px;padding-top:26px;padding-bottom:10px;border:0;" width="33.33%" valign="top" bgcolor="transparent" align="center"><a target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:16px;text-decoration:none;display:block;color:#1475A9;" href="https://trademarkers.com/EC-0D1A-D">Registration</a></td> 
                          <td style="Margin:0;padding-left:5px;padding-right:5px;padding-top:26px;padding-bottom:10px;border:0;" width="33.33%" valign="top" bgcolor="transparent" align="center"><a target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:16px;text-decoration:none;display:block;color:#1475A9;" href="https://trademarkers.com/EB-0D5C-F">Services</a></td> 
                          <td style="Margin:0;padding-left:5px;padding-right:5px;padding-top:26px;padding-bottom:10px;border:0;" width="33.33%" valign="top" bgcolor="transparent" align="center"><a target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:16px;text-decoration:none;display:block;color:#1475A9;" href="https://trademarkers.com/EB-0D1C-K">About Us</a></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table> 
               <!--[if mso]></td></tr></table><![endif]--></td> 
             </tr> 
           </table></td> 
         </tr> 
       </table> 
      
       @yield('content')

       <!-- footer -->
       <table cellpadding="0" cellspacing="0" class="es-footer" align="center" mc:repeatable style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top;"> 
         <tr style="border-collapse:collapse;"> 
          <td align="center" bgcolor="#ffffff" style="padding:0;Margin:0;background-color:#FFFFFF;"> 
           <table class="es-footer-body" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#333333;" width="600" cellspacing="0" cellpadding="0" bgcolor="#333333" align="center"> 
             <tr style="border-collapse:collapse;"> 
              <td style="Margin:0;padding-bottom:10px;padding-top:20px;padding-left:20px;padding-right:20px;background-color:#FFFFFF;" align="left" bgcolor="#ffffff"> 
               <!--[if mso]><table width="560" cellpadding="0" cellspacing="0"><tr><td width="270" valign="top"><![endif]--> 
               <table class="es-left" cellspacing="0" cellpadding="0" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;width:100%;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td class="es-m-p20b" width="270" align="left" style="padding:0;Margin:0;"> 
                   <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" style="padding:0;Margin:0;" mc:edit="block_22"><h4 style="Margin:0;line-height:120%;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;color:#1475A9;">Contact Us:</h4></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" style="padding:0;Margin:0;padding-top:10px;padding-bottom:15px;" mc:edit="block_23"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;">We're always here for you if you have questions. Just remember we have people who really want to help you out. Our award-winning customer support always has your back.</p></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td style="padding:0;Margin:0;"> 
                       <table class="es-table-not-adapt" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;margin:auto;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td valign="top" align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px;padding-right:10px;" mc:edit="block_24"><img src="https://fcttwq.stripocdn.email/content/guids/CABINET_c6d6983b8f90c1ab10065255fbabfbaf/images/30981556869899567.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;" width="16"></td> 
                          <td align="left" style="padding:0;Margin:0;"> 
                           <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;width:100%;"> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="left" style="padding:0;Margin:0;" mc:edit="block_25"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;">info@trademarkers.com</p></td> 
                             </tr> 
                           </table></td> 
                         </tr> 
                         <tr style="border-collapse:collapse;"> 
                          <td valign="top" align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px;padding-right:10px;" mc:edit="block_26"><img src="https://fcttwq.stripocdn.email/content/guids/CABINET_c6d6983b8f90c1ab10065255fbabfbaf/images/58031556869792224.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;" width="16"></td> 
                          <td align="left" style="padding:0;Margin:0;"> 
                           <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;width:100%;"> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="left" style="padding:0;Margin:0;" mc:edit="block_27"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;"><a target="_blank" href="tel:123456789" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:none;color:#333333;">212 468 5500</a></p></td> 
                             </tr> 
                           </table></td> 
                         </tr> 
                         
                       </table></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" style="padding:0;Margin:0;padding-top:15px;text-align:center" mc:edit="block_30"><span class="es-button-border es-button-border-4" style="border-style:solid;border-color:#659C35;background:#1475A9;border-width:0px;display:inline-block;border-radius:25px;width:auto;"><a href="https://www.trademarkers.com/BB-0F2B-R" class="es-button es-button-3" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#1475A9;border-width:10px 20px 10px 20px;display:inline-block;background:#1475A9;border-radius:25px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;">We're Live!</a></span></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table> 
               <!--[if mso]></td><td width="20"></td><td width="270" valign="top"><![endif]--> 
               
               <!--[if mso]></td></tr></table><![endif]--></td> 
             </tr> 
             <tr style="border-collapse:collapse;"> 
              <td style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px;background-color:#1475A9;" bgcolor="#1475a9" align="left"> 
               <table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td width="560" valign="top" align="center" style="padding:0;Margin:0;"> 
                   <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td style="padding:0;Margin:0;" mc:edit="nblock_32"> 
                       <table class="es-menu" width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                         <tr class="links" style="border-collapse:collapse;"> 
                          <td style="Margin:0;padding-left:5px;padding-right:5px;padding-top:0px;padding-bottom:0px;border:0;" width="25%" valign="top" bgcolor="transparent" align="center"><a target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:none;display:block;color:#FFFFFF;" href="https://trademarkers.com/AA-0K2B-P">Pricing</a></td> 
                          <td style="Margin:0;padding-left:5px;padding-right:5px;padding-top:0px;padding-bottom:0px;border:0;border-left:1px solid #FFFFFF;" width="25%" valign="top" bgcolor="transparent" align="center"><a target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:none;display:block;color:#FFFFFF;" href="https://trademarkers.com/FA-0K6A-T">Resources</a></td> 
                          <td style="Margin:0;padding-left:5px;padding-right:5px;padding-top:0px;padding-bottom:0px;border:0;border-left:1px solid #FFFFFF;" width="25%" valign="top" bgcolor="transparent" align="center"><a target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:none;display:block;color:#FFFFFF;" href="https://trademarkers.com/DA-0H6A-J">Blogs</a></td> 
                          <td style="Margin:0;padding-left:5px;padding-right:5px;padding-top:0px;padding-bottom:0px;border:0;border-left:1px solid #FFFFFF;" width="25%" valign="top" bgcolor="transparent" align="center"><a target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:none;display:block;color:#FFFFFF;" href="https://www.trademarkers.com/FA-0K0B-P">Contact Us</a></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
             <tr style="border-collapse:collapse;"> 
              <td style="Margin:0;padding-bottom:15px;padding-top:20px;padding-left:20px;padding-right:20px;background-color:#1475A9;" bgcolor="#1475a9" align="left"> 
               <table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td width="560" valign="top" align="center" style="padding:0;Margin:0;"> 
                   <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="padding:0;Margin:0;padding-bottom:15px;font-size:0;" mc:edit="block_33"> 
                       <table class="es-table-not-adapt es-social" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td valign="top" align="center" style="padding:0;Margin:0;padding-right:15px;"><a target="_blank" href="https://www.facebook.com/TrademarkersLLC/?modal=admin_todo_tour" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:underline;color:#FFFFFF;"><img title="Facebook" src="https://fcttwq.stripocdn.email/content/assets/img/social-icons/circle-white/facebook-circle-white.png" alt="Fb" width="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></a></td> 
                          <td valign="top" align="center" style="padding:0;Margin:0;padding-right:15px;"><a target="_blank" href="https://twitter.com/trademarkersllc" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:underline;color:#FFFFFF;"><img title="Twitter" src="https://fcttwq.stripocdn.email/content/assets/img/social-icons/circle-white/twitter-circle-white.png" alt="Tw" width="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></a></td> 
                          <td valign="top" align="center" style="padding:0;Margin:0;padding-right:15px;"><a target="_blank" href="https://www.youtube.com/channel/UCFzecLVDoJLH3sX78Cu7cvg" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:underline;color:#FFFFFF;"><img title="Youtube" src="https://fcttwq.stripocdn.email/content/assets/img/social-icons/circle-white/youtube-circle-white.png" alt="Yt" width="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></a></td> 
                          <td valign="top" align="center" style="padding:0;Margin:0;"><a target="_blank" href="https://www.linkedin.com/company/trademarkers/" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:underline;color:#FFFFFF;"><img title="Linkedin" src="https://fcttwq.stripocdn.email/content/assets/img/social-icons/circle-white/linkedin-circle-white.png" alt="In" width="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></a></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="padding:0;Margin:0;" mc:edit="block_34"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:13px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:20px;color:#FFFFFF;">Trademarkers is a leading international brand protection company that specializes in global trademark registration.</p></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="padding:0;Margin:0;padding-bottom:10px;padding-top:15px;font-size:0;" mc:edit="block_35">
                      <img src="https://fcttwq.stripocdn.email/content/guids/CABINET_c6d6983b8f90c1ab10065255fbabfbaf/images/15841556884046468.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;" width="140"></td> 
                      <img src="https://trademarkers.com/api/mailgun/opens?email={{$email->id}}&event={{$event->id}}" style="width:1px;height:1px;">
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
           </table></td> 
         </tr> 
       </table></td> 
     </tr> 
   </table> 
  </div>  
 </body>
</html>