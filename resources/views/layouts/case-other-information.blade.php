@if ( $trademark->campaign && $trademark->campaign->register_country_code && $trademark->case->is_purchase != 'yes' )
    <a class="btn btn-primary" style="float:right" href="/registration/step2/{{$trademark->campaign->register_country_code}}?action=setFields&cCode={{$trademark->campaign->register_country_code}}&case={{$trademark->case->id}}
    ">
                                Register Now!
    </a>
@elseif ($trademark->campaign && $trademark->case->is_purchase != 'no')

    <span style="float: right;font-weight: 600;font-style: italic;">This case was already purchased</span>

@else 

    <span style="float: right;font-weight: 600;font-style: italic;">This case is just a free information</span>

@endif