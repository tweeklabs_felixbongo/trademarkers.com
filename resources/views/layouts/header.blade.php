<div id="top-header" class="">
                <div class="container">
                    <div class="row align-items-center">
                
                        <!-- <div class="col-md-6">
                            <i class="fa fa-headphones"></i>1-800-293-3166
                            <a href="mailto:info@trademarkers.com"><i class="fa fa-envelope"></i>info@trademarkers.com</a>
                        </div> -->
                        <div class="col-6 col-md-8">
                            <div id="contact" >
                                <div id="accordionExample" >
                                    <button class="btn btn-transparent" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Contact Us <span class="fa fa-chevron-circle-down"></span>
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2 d-none d-md-block d-lg-block d-xl-block">
                            <div id="search" class="text-right">
                                <form method="POST" action="/search/case">
                                    @csrf
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="case_number" placeholder="Case #" required>

                                        <button type="submit">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        

                        <div class="col-6 col-md-2">
                            <ul class="navbar-nav ml-auto">
                                <!-- Authentication Links -->
                                @guest
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="/"><i class="fas fa-shopping-cart"></i></a>
                                    </li>
                                @else
                                    <li class="nav-item dropdown">
                                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                            @if ( !isset( Auth::user()->profiles[0]->first_name ) )
                                                Hello
                                            @else
                                                {{ str_limit( Auth::user()->profiles[0]->first_name ? Auth::user()->profiles[0]->first_name : 'Guest', 5 ) }}
                                            @endif
                                            <span class="caret"></span>
                                        </a>

                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                                            <a class="dropdown-item" href="/customer/dashboard">
                                                {{ __('My Account') }}
                                            </a>

                                            <a class="dropdown-item" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                            document.getElementById('logout-form').submit();">
                                                {{ __('Logout') }}
                                            </a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        </div>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link" href="/cart"><i class="fas fa-shopping-cart"></i> <span id="shopping-cart">{{ $cart_contents }}</span></a>
                                    </li>
                                @endguest
                            </ul>
                        </div>

                        

                    </div> 

                </div>
            </div>

            <!-- add banner here -->
           
            @if ($banner) 
            <div id="topbar-banner">
                <div class="container">
                    
                    <div class="row leader-board">
                        <div class="col-12 text-center">
                            <a href="/countries"><img class="img-fluid" alt="{{$banner->module_name}}" src="/uploads/{{$banner->path_lb}}" class="bgImage" /></a>
                            <br>
                        </div>
                    </div>
                    
                    
                </div>
                
            </div>
            @endif
            
            <!-- add banner end -->

            <div id="topbar-scroller">
                <div class="container">
                    
                  
                    
                    <div id="header-nav" class="row align-items-center">
                        <div class="col-md-3 col-lg-3 d-none d-md-none d-lg-block d-xl-block">
                            <a href="/"><img class="logo" alt="trademarkers" src="/images/logo2.png" /></a>
                        </div>

                        <div class="col-12 col-md-12  d-block d-lg-none d-xl-none">
                            <a href="/"><img class="logoMobile" alt="trademarkers" src="/images/logo2.png" /></a>
                        </div>

                        <div class="col-2 col-md-12 col-lg-9" style="padding-top:15px;">
                            @include('layouts.nav')
                        </div>

                        <div class="col-10 d-block d-md-none d-lg-none d-xl-none" style="padding-top:15px;">
                            <div id="search" class="text-right">
                                <form method="POST" action="/search/case">
                                    @csrf
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="case_number" placeholder="Case #" required>

                                        <button type="submit">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>

            <div id="topbar">
                <div class="container">
                    
                  
                    
                    <div id="header-nav" class="row align-items-center">
                        <div class="col-md-3 col-lg-3 d-none d-md-none d-lg-block d-xl-block">
                            <a href="/"><img class="logo" alt="trademarkers" src="/images/logo2.png" /></a>
                        </div>

                        <div class="col-12 col-md-12  d-block d-lg-none d-xl-none">
                            <a href="/"><img class="logoMobile" alt="trademarkers" src="/images/logo2.png" /></a>
                        </div>

                        <div class="col-2 col-md-12 col-lg-9" style="padding-top:15px;">
                            @include('layouts.nav')
                        </div>

                        <div class="col-10 d-block d-md-none d-lg-none d-xl-none" style="padding-top:15px;">
                            <div id="search" class="text-right">
                                <form method="POST" action="/search/case">
                                    @csrf
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="case_number" placeholder="Case #" required>

                                        <button type="submit">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>

            

            <div id="collapseOne" class="collapse hide" aria-labelledby="headingOne" data-parent="#accordionExample" style="background-color: #E9EAEC; position:fixed;width:100%;z-index:1;">
                <div class="card-body">
                    
                    <div class="row">
                        <div class="col-md-4" style="margin-bottom: 20px;">
                            <div class="contact-div">
                                <h2 style="padding-left: 10px;"> Talk to an expert.</h2>
                                <p style="padding-left: 10px;"> We've got phone and chat support available for you:</p>
                                <p style="padding-left: 10px;margin:0;">Monday - Friday : 7am to 7pm (EST)</p>
                                <p style="padding-left: 10px;margin:0;">Saturday : Closed</p>
                                <p style="padding-left: 10px">Sunday : Closed</p>
                            </div>

                            <table style="margin-bottom: 10px; text-align: right; line-height: 30px; font-weight: bold">
                                <tr>
                                    <td>Toll-Free:</td>
                                    <td ><b style="color: #1475A9;">1-800-293-3166</b></td>
                                </tr>
                                <tr>
                                    <td>Office:</td>
                                    <td><b style="color: #1475A9;">212-468-5500</b></td>
                                </tr>
                                <tr>
                                    <td>Fax:</td>
                                    <td><b style="color: #1475A9;">212-504-0888</b></td>
                                </tr>
                            </table>
                            
                            <a class="btn btn-success" href="javascript:$zopim.livechat.window.show();">
                            <span class="fa fa-comments"></span> Chat Now
                            </a>
                            <!-- <a href="https://tawk.to/chat/5c30b5417a79fc1bddf365e1/default" target="_blank" class="btn btn-success"> <span class="fa fa-comments"></span> Chat Now</a> -->
                        </div>

                        @if ( !isset( $contact_hidden ) )
                            <div class="col-md-8">
                                <div class="contact-div">
                                    <h3 style="padding-left: 10px;">Stuck?</h3>
                                    <h5 style="padding-left: 10px;">Bet we can help.</h5>
                                </div>
                                <form method="POST" action="/contact">
                                    @csrf
                                    <div class="form-group">
                                        <label for="name"><b>Name:</b></label>
                                        <input type="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" value="{{ old('name') }}" placeholder="Enter name" name="name" required>

                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="email"><b>Email:</b></label>
                                        <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" id="email" placeholder="Enter email" name="email" required>

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="phone"><b>Phone:</b></label>
                                        <input type="text"  class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" value="{{ old('phone') }}" id="phone" placeholder="Enter phone" name="phone" required>

                                        @if ($errors->has('phone'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="msg"><b>Message:</b></label>
                                        <textarea class="form-control{{ $errors->has('message') ? ' is-invalid' : '' }}" value="{{ old('message') }}" rows="7" name="message" required></textarea>

                                        @if ($errors->has('message'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('message') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <button type="submit" class="btn btn-danger">Submit</button>
                                </form>
                            </div>
                        @endif
                    </div>   
                </div>
            </div>