<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Bing Verification -->
        <meta name="msvalidate.01" content="E556295078A936AD687CD853186714A3" />
        <title>{{ MetaTag::get('title') }}</title>

        {!! MetaTag::tag('description') !!}
        {!! MetaTag::tag('keywords') !!}
        {!! MetaTag::tag('image') !!}
        {!! MetaTag::tag('site_name') !!}
        
        {!! MetaTag::openGraph() !!}
        
        {!! MetaTag::twitterCard() !!}

        {{--Set default share picture after custom section pictures--}}
        {!! MetaTag::tag('image', asset('images/default-logo.png')) !!}

        <meta name="yandex-verification" content="508acfa0f07c8018" />

        <link rel="icon" href="{{ asset('images/favicon.webp')}}"> 
        @if ( Request::getHost() == 'trademarkers.com') 
        <link rel="canonical" href="{{ Request::url() }}">
        @else 
        <meta name="robots" content="noindex">
        @endif

        @yield('gtag-datalayer')

        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-N75TKVW');</script>

        <!-- Scripts -->
        <script src="{{ asset('js/validate.fields.js') }}" defer></script>
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script async src='https://www.google-analytics.com/analytics.js'></script>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

        

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">

        @yield('extra_css')

        <script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/652f0fa2ee96a0510e9aa4e25/cc2034ba18c2f0a056576c6f6.js");
        </script>

        {{-- LinkedIn Insight Tag --}}
        <script type="text/javascript">
            _linkedin_partner_id = "574667";
            window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
            window._linkedin_data_partner_ids.push(_linkedin_partner_id);
        </script>
        <script type="text/javascript">
            (function(){var s = document.getElementsByTagName("script")[0];
            var b = document.createElement("script");
            b.type = "text/javascript";b.async = true;
            b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
            s.parentNode.insertBefore(b, s);})();
        </script>
        {{-- End LinkedIn Insight Tag --}}
        
        

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-15502758-57"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-15502758-57');

            @if ( session('trademark_order') && session('trademark_order')['amount'] ) 
            gtag('event', 'add_to_cart', {
                'send_to': [
                'UA-15502758-57'
                ],
                "items": [
                    {
                    "id": "sku-{{session('trademark_order')['type']}}",
                    "name": "{{session('trademark_order')['type']}}",
                    "list_name": "Trademark Service",
                    "brand": "Trademark",
                    "category": "{{session('trademark_order')['service']}}",
                    "quantity": 1,
                    "price": '{{ number_format( session('trademark_order')['amount'], 2 ) }}'
                    }
                ]
            });
            @endif
                
            if ( typeof dataLayerTemp !== 'undefined' ) {
                dataLayer.push(dataLayerTemp);
            }
            
        </script>

        <!-- Global site tag (gtag.js) - Google Ads: 851729164 -->
        <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=AW-851729164"></script>
        <script>

          window.dataLayer = window.dataLayer || [];

          function gtag(){dataLayer.push(arguments);}

          gtag('js', new Date());

          gtag('config', 'AW-851729164');
        </script> -->

        <!-- Global site tag (gtag.js) - Google Ads: 668416478 -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=AW-668416478"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'AW-668416478');
        </script>

        @if ( session('legal_case') && session('legal_case')->class_number ) 
        <!-- TEST -->
        @php
            $trademark = session('legal_case');
            $classes = explode(",", $trademark->class_number);
        @endphp
        <script>
            var class_description = @json($trademark->classes);
        </script>
        @else 
        <script>
            var class_description = null;
        </script>
        @endif
        

        

        @stack('head')

        <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window,document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '482883292660307'); 
        fbq('track', 'PageView');
        </script>
        <noscript>
        <img height="1" width="1" 
        src="https://www.facebook.com/tr?id=482883292660307&ev=PageView
        &noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->
    </head>
    <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N75TKVW"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->

        {{-- LinkedIn Insight Tag --}}
        <noscript>
        <img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=574667&fmt=gif" />
        </noscript>
        {{-- End LinkedIn Insight Tag --}}
        
        

        <div id="app">

            <!-- HEADER -->
            @include('layouts.header')

            <main class="">
                <!-- GENERAL PAGE TITLES -->
                @if ( MetaTag::get('title') != "Trademark Registration | Trademarkers")
                <h1 style="visibility:hidden;position:absolute">{{ MetaTag::get('title') }}</h1>
                <br>
                @else 
                <h1 style="visibility:hidden;position:absolute">{{ MetaTag::get('title') }}</h1>
                @endif
                
                @yield('content')

                @if( !isset( $_COOKIE['trademarkers_cookie_consent'] ) )
                    <div id="cookie-trademark" class="alert-warning">
                        <div class="js-cookie-consent cookie-consent">
                            <p>
                                <span class="cookie-consent__message">
                                    
                                    This website uses cookies. By using our website, you agree to our <a href="/cookies" target="_blank">Cookie Policy</a>
                                </span>

                                <a href="#" data-id='2' class="cookie-button btn btn-danger js-cookie-consent-agree cookie-consent__agree">
                                    Learn more.
                                </a>

                                <a href="#" class="cookie-button js-cookie-consent-agree cookie-consent__agree" data-id='1' style="margin-left: 30px; color: grey;">
                                    <i class="fa fa-times"></i>
                                </a>
                            </p>

                        </div>
                    </div>
                @endif
            </main>
        </div>

        @yield('checkout_form')

        <!-- <div class="con">
            <div style="border-top: 1px solid #cecece; margin-top: 10px; margin-bottom: -10px;"></div>-->
            @include('layouts.footer')
            
        <!-- </div> -->

        <script src="https://js.stripe.com/v3/"></script>
        <script src="https://cdn.rawgit.com/davidshimjs/qrcodejs/gh-pages/qrcode.min.js"></script>
        <script src="{{ asset('js/jquery.js') }}"></script>
        
        @yield('extra_script')

        <script>

            $ ( function () {

                var run = {

                    'init' : function () {

                        this.tooltip();
                        this.trademark_type();
                        this.trademark_filed();
                        this.trademark_priority();
                        this.add_class();
                        this.natureField();
                        this.subNatureField();
                        this.checkNationality();
                        this.countryField();
                        this.subCountryField();
                        this.showCommentForm();
                        this.showFileForm();
                        this.checkField();
                        this.invoice();
                        this.priorityFiling();
                        this.priorityClass();
                        this.priorityBtn();
                        this.priorityRemoveBtn();
                        this.case_add_to_cart();
                        this.set_cookie();
                    },

                    'tooltip' : function () {

                        $('[data-toggle="tooltip"]').tooltip();

                    },

                    'trademark_type' : function () {

                        $('#type').on( "change", function() {

                            var result = $(this).val();

                            if ( result == "logo" ) {

                                $('#word').css('display','none');
                                $('#upload').css('display','block');
                                $('#logo_pic').prop('required', true);
                                $('#word_mark').prop('required', false);

                            } else if ( result == "word" ) {

                                $('#word').css('display','block');
                                $('#upload').css('display','none');
                                $('#word_mark').prop('required', true);
                                $('#logo_pic').prop('required', false);

                            } else if ( result == "lword" ) {

                                $('#word').css('display','block');
                                $('#upload').css('display','block');
                                $('#word_mark').prop('required', true);
                                $('#logo_pic').prop('required', true);

                            } else {
                                window.location.reload();
                            }

                        } );
                    },

                    'trademark_filed' : function () {
                        $('#filed').on( "change", function () {

                            var result = $(this).val();

                            if ( result == "yes" ) {

                                $('#claim_priority').css('display','block');
                                $('#priority').prop('required', true);
                            } else {
                                $('#claim_priority').css('display','none');
                                $('#priority').prop('required', false);
                            }

                        } );
                    },

                    'trademark_priority' : function () {
                        $('#priority').on( "change", function () {
                            var result = $(this).val();

                            if ( result == "yes" ) {

                                $('#yes_priority').css('display','block');
                                $('#origin').prop('required', true);
                                $('#date').prop('required', true);
                                $('#tm').prop('required', true);

                            } else {

                                $('#yes_priority').css('display','none');   
                                $('#origin').prop('required', false);
                                $('#date').prop('required', false);
                                $('#tm').prop('required', false);
                            }
                        } );
                    },

                    'add_class' : function () {

                        $(".class_chk").on( "click", function () {

                            var val =  $(this).val();

                            var num   = 1;

                            if ( $.isNumeric( val.substr( 5, 2 ) ) ) {
                                
                                num = val.substr( 5, 2 );

                            } else {

                                window.location.reload();
                            }

                            var label = "<label class='col-sm-2 col-form-label' for='class"+ num +"'> Class " + num + "</label>";
                            var input = "<input id='class"+ num +"'type='text' class='form-control' name='description[]' placeholder='Enter Goods/Services on this trademark' data-id='"+num+"'>";
                            var id    = "new" + num;

                            var x = num < 10 ? "0" + num : num;
                            var prepend = "<div class='input-group-prepend'><div class='input-group-text'>"+ x +"</div></div>";
                            var new_elem = "<div class='input-group mb-2' id="+ id +">" + prepend + input + "</div>";

                            if ( $(this).is(":checked") ){

                                if ( $("input[name='description[]']").length == 0 ) {

                                    $("#classes").append( new_elem );

                                } else {

                                    var before    = $("input[name='description[]']")[0];
                                    var trigger_b = false;
                                    var trigger_a = false;
                                    var after     = '';

                                    $("input[name='description[]']").each( function () {

                                        if ( $(this).data('id') < num ) {

                                            after = $(this).parent();

                                            trigger_a = true;
                                            before  = "";
                                        }

                                        if ( $(this).data('id') > num ) {

                                            if ( trigger_b === false ) {

                                                before = $(this).parent();

                                                trigger_b = true;
                                                after  = "";
                                            }
                                           
                                        }

                                    } );

                                    if ( before === "" ) {

                                        $( after ).after( new_elem );
                                    } else {

                                        $( before ).before( new_elem );
                                    }
                                }

                            } else {
                                $("#new" + num ).detach();
                            }
                        } );
                    },

                    'natureField' : function () {

                        $("#nature").on( "focus", function () {

                            $("#nature_block").css('display', 'none');

                            $("#sub_nature_block").css('display', 'block');
                        } )
                    },

                    'subNatureField' : function () {
                        $("#sub_nature").on( "blur", function () {

                            $("#nature").val( $(this).val() );

                            $("#sub_nature_block").css('display', 'none');

                            $("#nature_block").css('display', 'block');
                        } )
                    },

                    'checkNationality' : function () {

                        $("#sub_nature").on( "change", function () {

                            var value = $(this).val();

                            if ( value === "Individual" ) {

                                $("#nationality_block").css('display', 'block');
                                $('#nationality').prop('required', true);
                            } else {
                                $("#nationality_block").css('display', 'none');
                                $('#nationality').prop('required', false);
                            }

                        } );
                    },

                    'countryField' : function () {

                        $("#country").on( "focus", function () {

                            $("#country_block").css('display', 'none');

                            $("#sub_country_block").css('display', 'block');
                        } )
                    },

                    'subCountryField' : function () {
                        $("#sub_country").on( "blur", function () {

                            $("#country").val( $(this).val() );

                            $("#sub_country_block").css('display', 'none');

                            $("#country_block").css('display', 'block');
                        } )
                    },

                    'showCommentForm' : function () {
                        $("#add_comment").on( "click", function () {

                            $("#comment_form").show();
                            $(this).hide();
                        } )
                    },

                    'showFileForm': function () {
                        $("#add_attachment").on("click", function () {

                            $("#file_form").show();
                            $(this).hide();
                        })
                    },

                    'checkField' : function () {

                        var x = @php echo json_encode( session('legal_case') ) @endphp;

                        if ( x !== null ) {

                           var classes = @php echo json_encode( session('legal_case')['numbers'] ) @endphp;
                           
                           if (classes) {
                                for ( var i = 0 ; i < classes.length; i++ ) {

                                    $("#class" + classes[i] ).click();
                                }
                            }
                           
                        }
                    },

                    'invoice' : function () {

                        $(".invoice_ref").on( 'mouseout', function () {

                            $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }
                            });

                            $.post("/customer/invoices", {

                                id: $(this).prev().val(),
                                ref: $(this).val()
                            },

                            function (data, status) {
                                // console.log(data);
                            });

                        } );
                    },

                    'priorityFiling' : function () {

                        $("#pselect").on( 'change', function () {

                            var x = $(this).val();

                            if ( x > 0 ) {

                                $("#pbtn").show();

                            } else {

                                $("#pbtn").hide();
                            }
                        } );
                    },

                    'priorityBtn' : function () {

                        $("#pbtn").on( 'click', function () {

                            var x = $("#pselect").val();
                            var classes = @php echo isset($trademark['priority_classes']) ? count($trademark['priority_classes']) : 0; @endphp

                            $("#pCountryField" + x).parent().show();

                            var a = parseInt($("#pInitialCost" + x).val());
                            var b = parseInt($("#pAdditionalCost" + x).val());
                            var g = parseInt($("#pPriorityCost" + x).val());
                            var c = 0;

                            if ( classes > 0 ) {

                                var c = ((classes - 1) * b) + a + g;
                            }

                            $("#pPriceField" + x).val( "$" +  c);
                            
                        } );
                    },

                    'priorityRemoveBtn' : function () {

                        $(".premovebtn").on( 'click', function () {

                            var x = $(this).data('id');

                            
                            $("#pCountryField" + x).parent().hide();
                        } );
                    },

                    'priorityClass' : function () {

                        $(".class_chk_p").on( 'click', function () {

                            var val     = $(this).val();
                            var checked = 0;

                            if ( $.isNumeric( val ) ) {
                                
                                num = val;

                            } else {

                                window.location.reload();
                            }

                            if ( $(this).is(":checked") ){

                                $(".p" + num).prop('checked', true);

                            } else{

                                $(".p" + num).prop('checked', false);
                            }

                            var current_form = $("#form" + num + " input[name='class[]']");
                            var all_forms    = $(".pFormPriority");

                            current_form.each( function ( index, value ) {

                                if ( $(value).is(":checked") ) {

                                    checked = checked + 1;
                                }
                            } );

                            if ( checked == 0 ) {

                                $(".p" + num).prop('checked', true);

                            } else {

                                all_forms.each( function ( index, value ) {

                                    var x = $(value).children(".pCountryClass").val();

                                    var a = parseInt($("#pInitialCost" + x).val());
                                    var b = parseInt($("#pAdditionalCost" + x).val());
                                    var g = parseInt($("#pPriorityCost" + x).val());
                                    var c = 0;

                                    if ( checked > 0 ) {

                                        var c = ((checked - 1) * b) + a + g;
                                    }

                                    $("#pPriceField" + x).val( "$" +  c);

                                    console.log(checked);
                                } );
                            }   
                        } );
                    },

                    'set_cookie' : function () {

                        $(".cookie-button").on( "click", function () {

                            const COOKIE_VALUE = 1;
                            const COOKIE_DOMAIN = '{{ config('session.domain') ?? request()->getHost() }}';
                            const date = new Date();
                            const COOKIE_NAME = "trademarkers_cookie_consent";

                            date.setTime(date.getTime() + (365 * 20 * 24 * 60 * 60 * 1000));
                            document.cookie = COOKIE_NAME + '=' + COOKIE_VALUE
                                + ';expires=' + date.toUTCString()
                                + ';domain=' + COOKIE_DOMAIN
                                + ';path=/{{ config('session.secure') ? ';secure' : null }}';

                            const val = $(this).data('id');

                            if ( val === 1 ) {

                                $("#cookie-trademark").hide();

                            } else {

                                window.location.href = '/cookies';
                            }
                        } );
                    },

                    'case_add_to_cart' : function () {

                        $(".paddbtn").on( "click", function () {

                            console.log($("#legal_case_id").val());

                            var r = [];
                            var c = $("#legal_case_id").val();
                            var x = $(this).data('id');
                            var y = $("#pCountryField" + x).val();

                            var d = $("#form" + x + " input[name='class[]']");

                            d.each( function ( index, value ) {

                                if ( $(value).is(":checked") ) {

                                    r.push( $(value).val() ); 
                                }
                            } );

                            $(this).text("Added");

                            $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }
                            });

                            $.post("/legal/case/add_to_cart", {

                                case_number: c,
                                country: y,
                                classes: r
                            },

                            function (data, status) {
                                
                                if ( data == 200 ) {

                                    var f = parseInt($("#shopping-cart").text());

                                    $("#shopping-cart").text( f + 1 );
                                }
                            });
                        } );
                    }
                };

                run.init();
            } );

        </script>

        <!-- Start of  Zendesk Widget script -->
        <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=e72f7c51-bfd9-49d3-83d6-3ae8a0f1728f"> </script>
        <!-- End of  Zendesk Widget script -->
        
    </body>
</html>
