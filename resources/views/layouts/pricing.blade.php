<div class="card-body" style="text-align: center;">
                    
    <img src="{{ asset('images/flag/' . strtolower( $country['abbr'] ) . '.png' ) }}" alt="Flag" />

    <hr>

    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">
	  View Pricing
	</button>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h6 class="modal-title" id="exampleModalLabel">
        			@php $abbr = strtoupper( $country['abbr'] ); @endphp

        			@if ( $abbr == "NL" || $abbr == "LU" )
        				@php $abbr = "BX"; @endphp
        			@endif
        			{{ strtoupper( $abbr )}} Trademark Registration Prices
        		</h6>
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          			<span aria-hidden="true">&times;</span>
        		</button>
      		</div>
      		<div class="modal-body">

	        	@foreach( $country->prices as $prices )

	        		@if ( $prices['service_type'] == 'Study' )
	        			<p> Step 1 Trademark Comprehensive Study</p>
	        		@elseif( $prices['service_type'] == 'Registration' )
	        			<p> Step 2 Trademark Registration Request</p>
	        		@else
	        			<p> Step 3 Trademark Certificate Request</p>
	        		@endif

	        		<table class="table table-striped">
	        			<tr>
	        				<td> Classes</td>
	        				<td> Word Only</td>
	        				<td> Design Only or Combined Word and Design</td>
	        			</tr>

	        			<tr>
	        				<td> First Class</td>
	        				<td> 
		        				{{ $prices['initial_cost'] }}
		        			</td>
	        				<td> 
	        					{{ $prices['logo_initial_cost'] }}
	        				</td>
	        			</tr>
	        			<tr>
	        				<td> Each Additional Class</td>
	        				<td> 
		        				{{ $prices['additional_cost'] }}
		        			</td>
	        				<td> 
		        				{{ $prices['logo_additional_cost'] }}
		        			</td>
	        			</tr>
	        		</table>

	        	@endforeach
	        	
	        	<p style="color: red;"> Prices are in U.S Dollars</p>
	        	<p> Note: Prices include all official and professional fees. Prices do not include legal defenses in case of oppositions or objections</p>
	        	<p> Timeframe of the trademark registration process is only an estimate and it may vary considerably if any objections and/or oppositions are presented, or other events occur during the trademark registration process.</p>
	      	</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      		</div>
    	</div>
  	</div>
</div>