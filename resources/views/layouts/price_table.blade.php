<table class="table table-hover">
    <thead>
        <tr style="color: red; font-weight: bold;">
            <td> </td>
            <td colspan="2"> TM Comprehensive Study</td>
            <td colspan="2"> TM Registration Request</td>
            <td colspan="2"> TM Registration Certificate</td>
        </tr>

        <tr>
            <td> Country</td>
            <td> First Class</td>
            <td> Additional Class</td>
            <td> First Class</td>
            <td> Additional Class</td>
            <td> First Class</td>
            <td> Additional Class</td>
        </tr>
    </thead>

    <tbody>
        @foreach( $countries as $country )
            @if( $country['continent_id'] == $region[0] || $region[0] == '999' )
                <tr>
                    <td> 
                        <a href="/trademark-registration-in-{{ strtolower(str_replace( ' ', '_', $country['name'] )) }}">
                            <img src="{{ asset('images/' . $country['avatar']) }}" />
                            {{ $country['name'] }}
                        </a>
                    </td>
                    @foreach( $country->prices as $price )
                        @if ( $price['service_type'] == 'Study' || 
                            $price['service_type'] == 'Registration' ||
                            $price['service_type'] == 'Certificate' ) 

                        <td> {{ $price['initial_cost'] }}</td>
                        <td> {{ $price['additional_cost'] }}</td>
                        @endif
                    @endforeach
                </tr>
            @endif
        @endforeach
    </tbody>
</table>