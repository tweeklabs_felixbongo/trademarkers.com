<div class="footer" style="border-top: 1px solid #dfdfdf; margin-top:20px;padding-top: 20px; background-color: white;">

    <div class="container">

        <div class="row">
        	<div class="col-md-4" style="padding: 25px 0px 30px 10px;">
        		
                <a href="https://www.facebook.com/TrademarkersLLC/" target="_blank"><img src="/social/facebook.png" alt="facebook" width="40" height="40"></a>
                <a href="https://twitter.com/trademarkersllc" target="_blank"><img src="/social/twitter.png" alt="twitter" width="40" height="40"></a>
                <a href="https://www.linkedin.com/company/trademarkers/" target="_blank"><img src="/social/linkedin.png" alt="linkedin" width="40" height="40"></a>
                <a href="http://bit.ly/TrademarkersYoutube" target="_blank"><img src="/social/youtube.png" alt="youtube" width="40" height="40"></a>
        	</div>


        	<div class="col-md-5">
        		<p class="text-muted" style="text-align: center;margin-top: 10px;">
		        	<i class="fas fa-copyright"></i> {{ now()->year }} Trademarkers LLC.
		        </p>
                
                <p class="text-center">
                    <a href="/contact">Contact</a> |
                    <a href="/prices">Pricing</a> |
                    <a href="/privacy">Privacy</a> |
                    <a href="/services">Services</a> |
                    <a href="/about">About</a> |
                    <a href="/terms">Terms</a> |
                </p>
        	</div>

            <div class="col-md-3 text-right" style="padding: 25px 0px 30px 10px;">
                <img src="/images/credit_card_logos_14.gif" width="100" height="20" />
                <img src="/images/trust.png" width="100" />
            </div>
            
        </div>
    </div>
</div>

<!-- back to top -->
<div class="back-to-top">
  <a id="back-top" href="#"><i class="fa fa-arrow-alt-circle-up"></i></a>
</div>
<!-- back to top end-->

<!-- YOUTUBE SUBSCRIBE START -->
<!-- <div id="popup-window" class="overlay">
    <div class="window-body">
        <span class="hide-pop-over close-button"><i class="fa fa-times"></i></span>

        <a href="/customer/youtube-subscribe">
          <h2 class="text-center" > Subscribe to <img src="{{URL::asset('/images/youtube_banner.svg')}}" alt="Youtube Channel"> and get up to 10% DISCOUNT!</h2> 
        </a>
    </div>
</div> -->
<!-- YOUTUBE SUBSCRIBE END --> 

<!-- popup -->
@if ($banner) 
<div class="modal transparent-modal" id="pop-up-transparent" data-id="pop-up-transparent" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Notification</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <a href="/countries"><img class="img-fluid" alt="{{$banner->name}}" src="/uploads/{{$banner->path}}" /></a>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@endif

<!-- CHAT SUPPORT POPUP BANNER -->
<div class="modal transparent-modal" id="pop-up-transparent-support" data-id="pop-up-transparent-support" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Notification</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <a id="contact-support-banner" href="javascript:$zopim.livechat.window.show();"><img class="img-fluid" alt="trademark support" src="{{URL::asset('/images/trademark-get-help.jpg')}}" /></a>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>