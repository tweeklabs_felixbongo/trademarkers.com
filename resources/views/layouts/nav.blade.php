
<nav class="navbar navbar-expand-md navbar-light navbar-laravel" >
    <div class="container">

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="dark-blue-text"><i class="fas fa-bars"></i></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav">
                
                <li class="nav-item"> 
                    <div class="dropdown">
                      <a class="dropdown-toggle" id="dropdownMenuButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">Registration </a>
                      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton1">

                        @foreach( $featured_countries as $country )

                            <a class="dropdown-item" href="/trademark-registration-in-{{ strtolower(str_replace( ' ', '_', $country['name'] )) }}">

                                <img src="{{ asset('images/' . $country['avatar']) }}" />

                                {{ $country['name'] }}
                            </a>

                        @endforeach

                        <hr>

                        <a class="dropdown-item" href="/countries"> All Countries</a>

                        @foreach( $featured_continent as $continent )

                            <a class="dropdown-item" href="/region/{{ $continent['abbr'] }}">

                                {{ $continent['name'] }}
                            </a>

                        @endforeach
                        
                      </div>
                    </div>
                </li>
                <li class="nav-item"> <a href="/prices">Pricing</a></li>
                <li class="nav-item"> 
                    <div class="dropdown">
                      <a class="dropdown-toggle" id="servicedropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">Services </a>
                      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="servicedropdown">
                        
                        <a class="dropdown-item" href="/countries">Trademark Study</a>
                        <a class="dropdown-item" href="/countries">Trademark Registration</a>
                        <a class="dropdown-item" href="/services">Other Services</a>
                        <!-- <a class="dropdown-item" href="/monitoring-service">Trademark Monitoring</a> -->
                        <!-- <a class="dropdown-item" href="/office-service">Office Action Response</a> -->
                        <!-- <a class="dropdown-item" href="/quote">Request for a Quote</a> -->
                      </div>
                    </div>
                </li>
                <li class="nav-item"> 
                    <div class="dropdown">
                      <a class="dropdown-toggle" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">Resources </a>
                      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton2">
                        <a class="dropdown-item" href="/classes">NICE Classes</a>
                        <a class="dropdown-item" href="/resources">Resources</a>
                      </div>
                    </div>
                </li>
                <li class="nav-item"> <a href="/contact">Contact</a></li>
                <li class="nav-item"> 
                    <div class="dropdown">
                      <a class="dropdown-toggle" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">About </a>
                      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton2">
                        <a class="dropdown-item" href="/about">About</a>
                        <a class="dropdown-item" href="/terms">Terms and Conditions</a>
                        <a class="dropdown-item" href="/privacy">Privacy Policy</a>
                        <a class="dropdown-item" href="/service_contract">Service Contract</a>
                        <a class="dropdown-item" href="/cookies">Cookies Policy</a>
                        <a class="dropdown-item" href="https://www.trademarkers.com/blog/">Blog</a>
                      </div>
                    </div>
                </li>
            </ul>

            <!-- Right Side Of Navbar -->
            
        </div>
    </div>
</nav>
