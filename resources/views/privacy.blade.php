@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    
                    <h2> PRIVACY POLICY</h2>

                    <p> Trademarkers is committed to safeguarding your privacy. By using this website, including by providing us with any personally identifiable information, you are accepting and consenting to the practices described in this policy.</p>

                    <ol>
                        <li> <p> COLLECTION AND USE OF PERSONAL INFORMATION.</p>
                            <ol type="A">
                                <li> 
                                    <b> Collection of Personal Information</b>
                                    <p> In order to process the service orders, Trademarkers requires knowing basic information about the “User” of the “Web Site” services and, in case of Companies, it is required to know certain information about the new entity (name, object, capital, company formation date, duration, number of shareholders/members, and number of directors if needed), and some personal information about the shareholders/members and of the directors.</p>
                                    <p> Trademarkers has definite and established policies for information privacy, which represent a clear commitment to the confidentiality needs of the user. These policies are applied to the collection as well as to the delivery of any type of information. Accordingly, the personal information of the user is guarded in safe locations, to which only the personnel of Trademarkers or its duly accredited representatives have access.</p>
                                </li>
                                <li> 
                                    <b> Legally Required Disclosure</b>
                                    <p> We may disclose any information, including Personal Information, we deem necessary, in our sole discretion, to comply with any applicable law, regulation, legal process or governmental request. The information revealed is only that necessary and sufficient to comply with the requirements of government authorities and ruling organizations, and, eventually, with specific instructions emanated from the courts to fulfill the actions requested by the user.</p>
                                </li>
                                <li> 
                                    <b> Third Parties</b>
                                    <p> Except as otherwise disclosed in this Privacy Policy, we will not sell, rent or transfer your Personal Information to third parties without your consent. We may, however, use the information you provide in aggregate form for internal business purposed, such as generating statistics and developing marketing plans.</p>
                                </li>
                                <li> 
                                    <b> Links</b>
                                    <p> Trademarkers may contain links to or from other websites. Please be aware that we are not responsible for the privacy practices of other websites. We encourage you to read the privacy policies of other websites you link to from the Trademarkers website or otherwise visit.</p>
                                </li>
                                <li> 
                                    <b> Business Transfers</b>
                                    <p> As we continue to develop our business, we might sell or otherwise transfer certain of our assets. In such transactions, user information, including Personal Information, is generally one of the transferred business assets, and by providing your Personal Information to us you agree that your data may be transferred to such parties in these circumstances.</p>
                                </li>
                            </ol>
                        </li>

                        <li> <p>CONTACT DATA.</p>
                            <ol type="A">
                                <li> 
                                    <b> Registration</b> 
                                    <p> In order to use this Web site, you must first provide your log in information by completing the registration form (for new users) and create a password. During registration you are required to give contact information such as name and email address).</p>
                                </li>
                                <li> 
                                    <b> Customer Service</b> 
                                    <p> Based upon the personal information you provide us, we will send you a welcoming email to verify your username and password. We will also communicate with you in response to your inquiries, to provide the services you request, and to manage your account. We will communicate with you by email or telephone, in accordance with your wishes.</p>
                                </li>
                                <li> 
                                    <b> Service-related Announcements</b> 
                                    <p> We will send you strictly service-related announcements on rare occasions when it is necessary to do so. For instance, if our service is temporarily suspended for maintenance, we might send you an email.</p>
                                </li>
                                <li> 
                                    <b> Chat System</b> 
                                    <p> When connecting to our live chat, we request that you provide your name and your email, for future purposes of easier communication.</p>
                                </li>
                                <li> 
                                    <b> Passive Collection</b> 
                                    <p> When connecting to our live chat, we request that you provide your name and your email, for future purposes of easier communication.</p>
                                </li>
                            </ol>
                        </li>

                        <li> <p> CHANGES TO THIS PRIVACY POLICY</p> <p>We may revise this Privacy Policy from time to time. If we make changes to our Privacy Policy, we will post the revised policy here. The revised policy will also apply retroactively to all data we have collected through the website.</p></li>

                        <li> <p> COMMENTS</p> <p></p>If you have any questions, comments or concerns about our privacy policy or practices, please contact us by email info@trademarkers.</li>

                    </ol>
                    
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
