<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Invoice</title>

  
        <style type="text/css">
            .clearfix:after {
              content: "";
              display: table;
              clear: both;
            }

            .left{
              float: left;
              font-weight: bold;
              font-size: 13px;
              width : 70%;
            }

            .right{
              float: right;
              font-size: 13px;
              width : 30%;

            }

            a {
              color: #5D6975;
              text-decoration: underline;
            }

            body {
              position: relative;
              height: 29.7cm; 
              margin: 0 auto; 
              color: #001028;
              background: #FFFFFF; 
              font-family: 'Calibri', sans-serif;
              font-size: 12px; 
              color: #2c2b26;
              /* font-family: Arial; */
            }

            header {
              padding: 10px 0;
              margin-bottom: 10px;
            }

            #logo {
              /* text-align: center; */
              padding-top: 40px;
              margin-bottom: 10px;
              float: left;
            }

            h1 {
              border-top: 1px solid  #5D6975;
              border-bottom: 1px solid  #5D6975;
              color: #5D6975;
              font-size: 2.4em;
              line-height: 1.4em;
              font-weight: normal;
              text-align: center;
              margin: 0 0 20px 0;
              background: url(../images/invoice/dimension.png);
            }

            #project {
              float: left;
            }

            #project span {
              color: #5D6975;
              text-align: right;
              width: 52px;
              margin-right: 10px;
              display: inline-block;
              font-size: 0.8em;
            }

            #company {
              float: right;
              text-align: right;
            }

            #project div,
            #company div {
              white-space: nowrap;        
            }

            #infotable .info {
              /* text-align: right; */
              padding-left: 4px;
            }

            #infotable td{
              width : 50%;
              color : #808080;
            }

            #maintable {
              width: 100%;
              border-collapse: collapse;
              border-spacing: 0;
              margin-bottom: 20px;
            }

            #maintable tr td {
              background: #e9f4f9;
            }

            #maintable tr td,
            #maintable tr th {
              border:1px solid #e2ecf6;
            }

            #maintable tr:nth-child(2n-1) td {
              background: #fff;
            }

            #maintable th,
            #maintable td {
              text-align: center;
            }

            #maintable th {
              padding: 5px 20px;
              color: #5D6975;
              border-bottom: 1px solid #C1CED9;
              white-space: nowrap;        
              font-weight: normal;
            }

            #maintable .service,
            #maintable .desc {
              text-align: left;
            }

            #maintable td {
              padding: 20px;
              text-align: right;
            }

            #maintable td.service,
            #maintable td.desc {
              vertical-align: top;
            }

            #maintable td.unit,
            #maintable td.qty,
            #maintable td.total {
              font-size: 1.2em;
            }

            #maintable td.grand {
              border-top: 1px solid #5D6975;
            }

            #maintable thead {
              background: #4486bb;
            }

            #maintable thead th{
              color: #fff;
              padding : 20px;
            }

            #notices .notice {
              color: #5D6975;
              font-size: 1.2em;
            }

            footer {
              color: #808080;
              width: 100%;
              height: 220px;
              position: absolute;
              bottom: 0;
              /* border-top: 1px solid #C1CED9; */
              padding: 8px 0;
            }

            

            .top-right-ico{
              position: absolute;
              top:-50px;
              right:25%;
            }

            .bill-to-container{
              /* width:50%; */
              padding-right: 30px;
            }
            .bill-to-label{
              width:20%;
              float: left;
            }
            .bill-to-details{
              width:80%;
              float: right;
            }

            .bill-to-details p{
              font-size: 10px;
              color: #808080;
              line-height: 10px;
            }

            .bill-to{
              font-size : 16px; color: #2c2b26;
              line-height:12px;
            }
            .bill-to-details p.bill-to{
              font-size : 16px; color: #2c2b26;
              line-height:12px;
              margin-bottom:0px;
            }

            .tbl-bak-details{
              width:100%;
              color: #808080;
            }

            .tbl-bak-details td{
              padding-bottom:10px;
            }

            .payment-total{
              width: 100%;
            }

            .payment-total td{
              vertical-align: top;
            }

            .tbl-total-details{
              width: 100%;
              padding-top:15px;
              border-collapse: collapse;
              border-spacing: 0;
            }

            .tbl-total-details .grand-total td{
              background: #4486bb;
              color : #fff;
              border: 1px solid #e2ecf6;
              padding : 10px;
            }

            .tbl-total-details td{
              font-size: 14px;
              font-weight: 600;
              color: #000;
              padding : 0 10px 10px;
            }

        </style>
    </head>
    <body>
        <header class="clearfix">

            <img class="top-right-ico" src="images/pdf-icon.png" width="30%">

            <div id="logo">
                <img src="images/trademarkers.png" width="40%">
            </div>

            <!-- <div style="margin-top: 140px;" class="clearfix">
                <div>Trademarkers LLC | 45 Essex Street, Suite 202 | Millburn NJ 07041</div>
            </div> -->
            <div class="clearfix"></div>

            <div  class="clearfix">
              <div class="left" style="line-height: 10px;">

                  <div class="bill-to-container clearfix">
                    <div class="bill-to-label">
                      <h2>| Bill To.</h2>
                    </div>
                    <div class="bill-to-details">
                      <h2>{{ $order['billing_name'] }}</h2>
                      <p style="line-height:16px;font-weight:200"> 
                        P. {{ $order['billing_phone_number'] }} <br>
                        E. {{ $order['user']['email'] }} <br>
                        A. {{ $order['billing_country'] . ", " . $order['billing_city'] . ", " . $order['billing_address'] . ", " . $order['billing_state'] . " " . $order['billing_zip'] }}
                      </p>
                    </div>
                  </div>
           
              </div>
              
              <div class="right">
                <div >
                  <p style="font-size:38px;text-transform:uppercase;line-height:12px;font-weight:100;">Invoice</p>
                </div>

                @if ( $order->invoice->status != 'paid' ) 
                  <div>
                    <table id="infotable">
                      <tr>
                        <td>Invoice No</td>
                        <td class="info">: {{ $order->invoice->reference }}</td>
                      </tr>
                      <tr>
                        <td>Date</td>
                        <td class="info">: {{ $order->created_at->format('F d, Y') }}</td>
                      </tr>
                      <tr>
                        <td>Due Date</td>
                        <td class="info">: {{ $order->created_at->addDays(30)->format('F d, Y') }}</td>
                      </tr>

                    </table>
                  </div>
                @endif
              </div>
            </div>
        </header>

        <main>
            <table id="maintable" >
                <thead>
                    <tr>
                        <th class="service">SERVICE</th>
                        <th class="desc">DESCRIPTION</th>
                        <th>PRICE</th>
                        <th>QTY</th>
                        <th>TOTAL</th>
                    </tr>
                </thead>

                <tbody>

                    @php
                      $totalCount = 0;
                    @endphp
                    @foreach( $order->trademarks as $trademark )
                        <tr>
                            <td class="service">{{ $trademark->service }}</td>
                            <td class="desc">{{ '[' . $trademark->type . ']' . $trademark->service . ' for ' . $trademark->name }}</td>
                            <td class="unit">${{ number_format( $trademark->amount, 2) }}</td>
                            <td class="qty">1</td>
                            <td class="total">${{ number_format( $trademark->amount, 2) }}</td>
                        </tr>
                        @php
                        $totalCount += $trademark->amount;
                      @endphp
                    @endforeach
          
                    
                </tbody>
            </table>

            <table class="payment-total">
              <tr>
                <td style="width:50%">
                  <h2 style="margin-bottom:15px;">Payment Method</h2>
                  <table class="tbl-bak-details" style="width:100%">
                    <tr>
                        <td style="width:25%">BANK</td>
                        <td>: JP Morgan Chase</td>
                    </tr>
                    <tr>
                        <td>SWIFT</td>
                        <td>: CHASUS33</td>
                    </tr>
                    <tr>
                        <td>Account No</td>
                        <td>: 55964-0730</td>
                    </tr>
                    <tr>
                        <td>Routing No</td>
                        <td>: 267084131</td>
                    </tr>

                  </table>
                </td>
                <td>
                  <table class="tbl-total-details">
                    @if ( $totalCount != $order->total_amount ) 
                    <tr>
                        <td>DISCOUNT</td>
                        <td class="total">-${{ number_format( ($totalCount - $order->total_amount) , 2) }}</td>
                    </tr>
                    @endif
                    <tr>
                        <td>SUBTOTAL</td>
                        <td class="total">${{ number_format( $order->total_amount, 2) }}</td>
                    </tr>

                    <tr class="grand-total">
                        <td >TOTAL</td>
                        <td class="grand total">${{ number_format( $order->total_amount, 2) }}</td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>

            <h2 style="margin-bottom:15px;padding-top:30px">Notes</h2>
            <p style="color:#808080">Please specify your invoice number in your payment and correspondence</p>

        </main>

        <footer>
            <h3 style="text-align:center;margin-bottom:50px;">THANK YOU FOR YOUR BUSINESS</h3>
            <div style="line-height: 5px;width:100%; text-align:center">
              <div style="float: left;width:33%">
                <img src="images/ico-location.png">
                <p> Trademarkers LLC</p>
                <p> 45 Essex Street, Suite 202</p>
                <p> Millburn NJ 07041</p>
                <p> 212-468-5467</p>
              </div>

              <!-- <div style="float: left;width:33%">
                <img src="images/ico-phone.png">
                <p> Joy Narvasa</p>
                <p> 212-468-5467</p>
                <p> joy@trademarkers.com</p>
              </div> -->

              <div style="float: left;width:33%">
                <img src="images/ico-location.png">
                <p> Bank Address</p>
                <p> 401 Madison Ave.</p>
                <p> New York, NY 10017</p>
              </div>

              
            </div>
        </footer>
    </body>
</html>
<!DOCTYPE html>