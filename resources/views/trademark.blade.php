@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body" style="text-align: center;">

                    @if ( count( $trademark ) == 0 )

                        <h3> No results.</h3>
                    @else
                        <nav style="color: black;">
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-information-tab" data-toggle="tab" href="#nav-information" role="tab" aria-controls="nav-information" aria-selected="true">Trademark Information</a>

                                <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Contact Information</a>

                                <a class="nav-item nav-link" id="nav-comments-tab" data-toggle="tab" href="#nav-comments" role="tab" aria-controls="nav-comments" aria-selected="false">Comments <span class="badge-danger" style=" padding-left: 5px; padding-right: 5px; border-radius: 50%;"><b><i>{{ count($comments) }}</i></b></span></a>

                                <a class="nav-item nav-link" id="nav-attachments-tab" data-toggle="tab" href="#nav-attachments" role="tab" aria-controls="nav-attachments" aria-selected="false">Attachments</a>
                            </div>
                        </nav>

                        @if ( session('errors') )
                            <div style="text-align: center;" class="alert alert-success" role="alert">
                                {{session('errors')->first('message')}}
                            </div>
                        @endif

                        <div class="tab-content" id="nav-tabContent" style="margin-top: 20px;">
                            <div class="tab-pane fade show active" id="nav-information" role="tabpanel" aria-labelledby="nav-information-tab">
                                <table class="table table-hover table-bordered">
                                    <tr>
                                        <td class="left-position"> Service Type</td>
                                        <td> {{ $trademark[0]['service'] }}</td>
                                        <td class="left-position"> Type</td>
                                        <td> {{ $trademark[0]['type'] }}</td>
                                    </tr>

                                    <tr>
                                        <td class="left-position"> Trademark Name</td>
                                        <td> {{ $trademark[0]['name'] }}</td>
                                        <td class="left-position"> Logo</td>
                                        <td> 
                                            @if ( $trademark[0]['logo'] != "N/A" )
                                                <img src="{{ asset('/uploads/'. $trademark[0]['logo']) }}" width="100">

                                                <a href="/uploads/{{ $trademark[0]['logo'] }}" target="_blank">
                                                    <i class="fa fa-eye"></i> View
                                                </a>

                                            @else
                                                {{ $trademark[0]['logo'] }}
                                            @endif
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="left-position"> Status</td>
                                        <td> {{ ucfirst( $trademark[0]['office_status'] ) }}</td>
                                        <td class="left-position"> NICE Classes</td>
                                        <td> {{ $trademark[0]['classes'] }}</td>
                                    </tr>

                                    <tr>
                                        <td class="left-position"> Country</td>
                                        <td> 
                                            <a href="/country/{{ $trademark[0]->country['abbr'] }}">
                                                <img src="{{ asset('/images/' . $trademark[0]->country['avatar'] ) }}" alt="{{ $trademark[0]->country['name'] }}"/>
                                                {{ $trademark[0]->country['name'] }} 
                                            </a>
                                        </td>
                                        <td class="left-position"> Basis</td>
                                        <td> N/A</td>
                                    </tr>

                                </table>

                            </div>

                            <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                                <table class="table table-hover table-bordered">
                                    
                                    <tr>
                                        <td class="left-position"> Holder Name</td>
                                        <td> 
                                            {{ $trademark[0]->profile->last_name . ', ' .$trademark[0]->profile->first_name . ' ' . $trademark[0]->profile->middle_name }}
                                        </td>
                                        <td class="left-position"> Nature</td>
                                        <td style="color: red;"> {{ $trademark[0]->profile->nature }}</td>
                                    </tr>

                                    <tr>
                                        <td class="left-position"> Email Address</td>
                                        <td> {{ $trademark[0]->profile->email }}</td>
                                        <td class="left-position"> Phone Number</td>
                                        <td> {{ $trademark[0]->profile->phone_number }}</td>
                                    </tr>

                                    <tr>
                                        <td class="left-position"> Country</td>
                                        <td> {{ $trademark[0]->profile->country }}</td>
                                        <td class="left-position"> Postal Code</td>
                                        <td> {{ $trademark[0]->profile->zip_code }}</td>
                                    </tr>

                                </table>
                            </div>

                            <div class="tab-pane fade" id="nav-comments" role="tabpanel" aria-labelledby="nav-comments-tab">
                                <div class="row">
                                    <div class="col-md-12">
                                        @foreach( $comments as $comment )
                                            <div class="card" style="text-align: left; padding-top: 10px; margin-top: 10px; background-color: #e8e8e8;">

                                                <div class="card-body">
                                                    <p> {!! $comment->description !!}</p>

                                                    @if ( $comment->user_id == 0 ) 
                                                        <b> by {{ "Representative" }} <i> {{ $comment->created_at->diffForHumans() }}</i></b>
                                                    @else
                                                        <b> by {{ $comment->user->name }} <i> {{ $comment->created_at->diffForHumans() }}</i></b>
                                                    @endif
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>

                                    <div class="col-md-2" style="margin-top: 10px;">
                                        <button id="add_comment" class="btn btn-primary"> Add Comment </button>
                                    </div>
                                </div>

                                <form id="comment_form" method="POST" action="/customer/comment/add_comment" style="margin-top: 20px; display: none;">
                                    @csrf
                                    <input type="hidden" name="trademark_id" value="{{ $trademark[0]->id }}">
                                    <div class="form-group row">
                                        <label for="description" class="col-md-2 col-form-label text-md-right">{{ __('Comment') }}</label>

                                        <div class="col-md-10">

                                            <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" value="{{ old('description') }}"  autofocus rows=5></textarea>

                                            @if ($errors->has('description'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('description') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="col-md-10 offset-md-2" style="margin-top: 10px;">
                                            <button type="submit" class="btn btn-primary">
                                                {{ __('Add Comment') }}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="nav-attachments" role="tabpanel" aria-labelledby="nav-attachments-tab">
                               <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-hover">
                                            @foreach( $attachments as $attachment )
                                                <tr>
                                                    <td>
                                                        <p> {{ $attachment->description }}</p>
                                                    </td>
                                                    <td>
                                                        <form method="POST" action="/customer/attachment/download">
                                                            @csrf
                                                            <input type="hidden" name="trademark_id" value="{{ $trademark[0]->id }}">

                                                            <input type="hidden" name="attachment_id" value="{{ $attachment->id }}">

                                                            <button class="btn btn-warning"> <i class="fa fa-download"></i> Download</button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>

                                    <div class="col-md-2" style="margin-top: 10px;">
                                        <button id="add_attachment" class="btn btn-primary"> Upload File </button>
                                    </div>
                                </div>

                                <form id="file_form" method="POST" action="/customer/attachment/add_attachment" style="margin-top: 20px; display: none;" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="trademark_id" value="{{ $trademark[0]->id }}">
                                    <div class="form-group row">
                                        <label for="attachment" class="col-md-2 col-form-label text-md-right">{{ __('Upload File') }}</label>

                                        <div class="col-md-10">
                                            <input type="file" name="attachment" class="form-control{{ $errors->has('logo_pic') ? ' is-invalid' : '' }}" id="attachment">

                                            @if ($errors->has('attachment'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('attachment') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="col-md-10 offset-md-2" style="margin-top: 10px;">
                                            <button type="submit" class="btn btn-primary">
                                                {{ __('Upload') }}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
