@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">

    	<div class="col-md-12" style="text-align: center; padding: 10px 0px 40px 0px;">
    		<h2> Videos</h2>
    		<hr>
    	</div>
		@foreach( $videolist as $list )
            <div class="col-md-3" style="position: relative; left: 0; top: 0; margin-bottom: 40px;">
                <a href="/{{ $list['slug'] }}">
                    <img src="{{ $list['thumbnail'] }}" width="100%" alt="" style="position: relative;">
                    <img src="/images/video.png" width="30%" style="position: absolute; bottom: 85px; left: 33%">
                </a>

                <a href="/{{ $list['slug'] }}" style="font-weight: bold;"> {{ strlen( $list['title'] ) > 26? substr( $list['title'], 0, 24) . '...' : $list['title'] }}</a>

                <em> Published on: {{ $list['created_at']->diffForHumans() }}</em>
            </div>
        @endforeach
       
    </div>
</div>

@endsection
