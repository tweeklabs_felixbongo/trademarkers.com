@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    
                    <h2 class="text-center"> Grab your own link and get paid with PayPal.Me.</h2>
                    <br>
                    <div class="row" style="max-width:800px;margin:auto;">
                        <div class="col-12 col-sm-6">
                            <p><img src="/images/paypal.jpg" class="img-fluid" /></p>
                        </div>
                        <div class="col-12 col-sm-6">
                            <h4>PayPal.Me is an easy, secure way for you to request payment from your customers.
                            You simply email or text them your business's custom PayPal.Me link and they click to
                            pay you. You'll see the transaction in your PayPal account, usually in seconds. Just
                            like that.</h4>
                            <p class="text-center" style="padding-top:30px;"><a target="_blank" class="btn btn-primary" href="https://www.paypal.me/">Grab Your Link</a></p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        
        
        
       

        </div>
    </div>
</div>

@endsection
