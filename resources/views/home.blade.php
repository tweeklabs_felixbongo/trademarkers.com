@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        @include('layouts.customer-nav', $dashboard = [1])

        <div class="col-md-9">
		    <div class="card">
		        <div class="card-body">
		            <div class="form-group row">
						@php
							$customerStr = ( $user->role->slug ? $user->role->name : 'Customer');
						@endphp
		                <label for="name" class="col-sm-4 col-form-label text-md-right">{{ __($customerStr.' #') }}</label>

		                <div class="col-md-6">
		                    <input id="name" type="text" class="form-control" name="name" value="{{ $user['custom_id'] }}" readonly >
		                </div>
		            </div>

		            <div class="form-group row">
		                <label for="name" class="col-sm-4 col-form-label text-md-right">{{ __('Full Name') }}</label>

		                <div class="col-md-6">
		                    <input id="name" type="text" class="form-control" name="name" value="{{ $user['name'] }}" readonly >
		                </div>
		            </div>

		            <div class="form-group row">
		                <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('Email Address') }}</label>

		                <div class="col-md-6">
		                    <input id="email" type="email" class="form-control" name="email" value="{{ $user['email'] }}" readonly >
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
</div>

@endsection