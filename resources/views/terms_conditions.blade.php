@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body" style="text-align: justify;">
                    
                    <h2> TERMS AND CONDITIONS FOR USE </h2>

                    <h4 style="padding-top:20px;">GENERAL</h4>

                    <p>Trademarkers LLC ("Us" or "We") provide the Trademarkers platform (the
                    "Platform") and website (the "Site"), and various related services
                    accessible through the Internet or other electronic communications methods
                    including, but not limited to, using a web browser to you, the user, as
                    well as downloadable executable code ("Apps"), subject to your compliance
                    with all the terms, conditions, and notices contained or referenced herein
                    (the "Terms"), as well as any other written agreement between us and you.
                    In addition, when using particular services or materials on this site,
                    users shall be subject to any posted rules applicable to such services or
                    materials that may contain terms and conditions in addition to those in
                    these Terms of Use. All such guidelines or rules are hereby incorporated
                    by reference into these Terms of Use.</p>

                    <p>BY USING THIS SITE, YOU AGREE TO BE BOUND BY THESE TERMS OF USE. IF YOU DO
                    NOT WISH TO BE BOUND BY THE THESE TERMS OF USE, PLEASE EXIT THE SITE NOW.
                    YOUR REMEDY FOR DISSATISFACTION WITH THIS SITE, OR ANY PRODUCTS, SERVICES,
                    CONTENT, OR OTHER INFORMATION AVAILABLE ON OR THROUGH THIS SITE, IS TO
                    STOP USING THE SITE AND/OR THOSE PARTICULAR PRODUCTS OR SERVICES. YOUR
                    AGREEMENT WITH US REGARDING COMPLIANCE WITH THESE TERMS OF USE BECOMES
                    EFFECTIVE IMMEDIATELY UPON COMMENCEMENT OF YOUR USE OF THIS SITE.</p>

                    <p>We offer the use of the Site and its content to any citizens of legal age
                    of countries other than Iran and North Korea. We do not grant any
                    permission of use of any content if you are a citizen of Iran or North
                    Korea, or if you are subject to any sanctions passed by the Security
                    Counsel of the United Nations, or if your use of the Site requires the
                    permission of the United States Office for Foreign Asset Control (OFAC).</p>

                    <p>We expressly reserve the right to change these Terms of Use from time to
                    time without notice to you. You acknowledge and agree that it is your
                    responsibility to review this site and these Terms of Use from time to
                    time and to familiarize yourself with any modifications. Your continued
                    use of this site after such modifications will constitute acknowledgement
                    of the modified Terms of Use and agreement to abide and be bound by the
                    modified Terms.</p>

                    <h4 style="padding-top:20px;">NATURE OF THE SERVICES</h4>

                    <p>Our services are restricted to referring your orders to a network of
                    qualified trademark attorneys (the "Trademarkers Network"), who you are
                    entering contracts with by placing orders through the Platform. Those
                    orders are governed by the Terms and Conditions of the legal service
                    provider, and the relevant legal venue and jurisdiction that govern these
                    services.</p>

                    <p>Trademarkers does have authority to collect fees for its services to
                    operate the Platform, and Trademarkers will deduct a service fee from the
                    payment to the attorney, which does not add any cost to you.</p>

                    <h4 style="padding-top:20px;">SUBSCRIPTION SERVICE</h4>

                    <p>We offer the use of the Site to unregistered users as well as registered
                    users, and we may designate any of the content, at our sole discretion, to
                    either free registered users or users that have entered into a
                    subscription contract.</p>
                    <p>We make various services and content available on this site including, but
                    not limited to, Chinese language-learning classes, videos, audio lessons,
                    tests, and other services (the "Content"). You are responsible for
                    providing, at your own expense, all equipment necessary to use the
                    services, including a tablet, smart phone, computer, and Internet access.
                    We make no representations to the quality, accuracy or suitability of the
                    Content and Apps.</p>
                    <p>We reserve the sole right at any point in time to either modify or
                    discontinue the site, including any of the site's features, with or
                    without notice to you. We will not be liable to you or any third party
                    should we exercise such right. Any new features that augment or enhance
                    the then-current services on this site shall also be subject to these
                    Terms.</p>

                    <h4 style="padding-top:20px;">PROVISION OF CONTENT</h4>

                    <p>The Content and Apps are provided for the sole use by you and you only,
                    with no permission to copy, reproduce, license, sublicense or pass on in
                    any form. You agree not to sell, resell, reproduce, duplicate, copy or use
                    for any commercial purposes any portion of this site, or use of or access
                    to this Site or Apps. By singing on with your browser or by registering
                    with a specific identifier and a password (the "Credentials"), or by
                    employing the services of a provider that handles identification of an
                    individual user, you give us the irrevocable permission to store all data
                    about you, your conduct on the site and any information relevant to your
                    use of the site, such as your IP address, and store all transactions and
                    information in a data collection (the "Account") residing on servers
                    operated or contracted by us. You warrant and represent that, once
                    registered, you will not provide your Credentials to any third party, nor
                    will you permit any third party to use Credentials they may have obtained
                    through other ways to use your Account. For your convenience, we may store
                    an identifying token (the "Cookie") on your web browser to identify you
                    when returning to the site for your own convenience, avoiding the need to
                    provide Credentials each time you visit the site.</p>

                    <h4 style="padding-top:20px;">TRUE IDENTITIES</h4>

                    <p>By registering on our site, you warrant and represent that (a) the name
                    you are stating in any fields designated Name or First Name or Last Name
                    is your truthful legal name, as spelled in your government issued identity
                    documents, and (b) your email address and user name does not impersonate
                    or otherwise suggest any identity other than your true own one.</p>

                    <h4 style="padding-top:20px;">CHILDREN</h4>

                    <p>Collecting personal information from children under the age of 18 is
                    prohibited, therefore minor children are not eligible to use the site, and
                    we ask that they do not submit any personal information to us.</p>

                    <h4 style="padding-top:20px;">FAIR USE POLICY</h4>

                    <p>You are provided access to Platform and services under the policy of Fair
                    Use, specifically for your personal use within limitations of access
                    customary for normal consumption of learning content. You warrant and
                    represent that you (a) will not download amounts of content beyond what is
                    typically needed for your own use, which is one lesson per day. If our
                    Site or Apps permit the download of content for offline use, your right is
                    limited to a window of 14 days, half of which is content you already
                    studied for reference and future content you may want to study going
                    forward. You warrant and represent that you will delete any content beyond
                    the limits described above, and you will cooperate with any investigations
                    we may decide to initiate and provide all information about any of your
                    conduct or conduct you have personal knowledge of that may violate these
                    Terms or any laws of any country that the services of the Site are offered
                    in.</p>

                    <h4 style="padding-top:20px;">PAID SUBSCRIPTIONS</h4>

                    <p>If you order a paid subscription on our Site, the services associated with
                    your subscription will be made available for the period subscribed to by
                    you, typically per month, per quarter or per year. All of the time frames
                    described are based on the Gregorian calendar, and any dates relevant to
                    services are dates relating to Eastern Standard Time, with the end of day
                    concluding at midnight New York time. We expressly reserve the right to
                    deny, discontinue, or limit, all or parts of the services in you are
                    delinquent in any payments owed to us. Such limitations do not relieve you
                    from paying the amounts owed until you properly terminate the services in
                    accordance with these Terms.</p>

                    <h4 style="padding-top:20px;">TERMINATION OR RESTRICTION OF YOUR ACCESS</h4>

                    <p>You agree that we may at any time, and at our sole discretion, terminate
                    your membership, account, or other affiliation with our site without prior
                    notice to you for violating any of the above provisions. In addition, you
                    acknowledge that we will cooperate fully with investigations of violations
                    of systems or network security at other sites, including cooperating with
                    law enforcement authorities in investigating suspected criminal
                    violations.</p>

                    <h4 style="padding-top:20px;">CONDUCT ON SITE</h4>

                    <p>We may invite users to participate in open forums and other communications
                    platforms that allow users to share opinions and comments. If you decide
                    to participate by submitting any comments, you are solely responsible for
                    the substance of your communications. By posting information in or
                    otherwise using any communications service, chat room, message board,
                    newsgroup, software library, or other interactive service that may be
                    available to you on or through this site, you agree that you will not
                    upload, share, post, or otherwise distribute or facilitate
                    distribution of any content - including text, communications, software,
                    images, sounds, data, or other information - that:</p>

                    <ol>
                    <li type="a">is unlawful, threatening, abusive, harassing, defamatory, libelous,
                    deceptive, fraudulent, invasive of another's privacy, tortuous, contains
                    explicit or graphic descriptions or accounts of sexual acts (including but
                    not limited to sexual language of a violent or threatening nature directed
                    at another individual or group of individuals), or otherwise violates our
                    rules or policies,</li>

                    <li type="a">victimizes, harasses, degrades, or intimidates an individual or group
                    of individuals on the basis of religion, gender, sexual orientation, race,
                    ethnicity, age, or disability,</li>

                    <li type="a">infringes on any patent, trademark, trade secret, copyright, the
                    common personality rights, the right of publicity, or another proprietary
                    right of any third party,</li>

                    <li type="a">constitutes unauthorized or unsolicited advertising, junk or bulk
                    email (also known as "spamming"), chain letters, any other form of
                    unauthorized solicitation, or any form of lottery or gambling,</li>

                    <li type="a">contains malicious software, viruses or any other computer code,
                    files, or
                    programs that are designed or intended to disrupt, damage, or limit the
                    functioning of any software, hardware, or telecommunications equipment or
                    to damage or obtain unauthorized access to any data or other information
                    of any third party, or</li>

                    <li type="a">impersonates any person or entity, including any of our employees or
                    representatives.</li>
                    </ol>

                    <p>We neither endorse nor assume any liability for the contents of any
                    material uploaded or submitted by third party users of the site. We
                    generally do not pre-screen, monitor, or edit the content posted by users
                    of communications services, chat rooms, message boards, newsgroups,
                    software libraries, or other interactive services that may be available on
                    or through this site. However, we and our agents have the right at their
                    sole discretion to remove any content that, in our judgment, does not
                    comply with these Terms of Use and any other rules of user conduct for our
                    site, or is otherwise harmful, objectionable, or inaccurate. We are not
                    responsible for any failure or delay in removing such content. You hereby
                    consent to such removal and waive any claim against us arising out of such
                    removal of content.</p>

                    <h4 style="padding-top:20px;">INTELLECTUAL PROPERTY INFORMATION</h4>

                    <p>Any information, data, communications, software, photos, video, graphics,
                    music, sounds, and other material and services that can be viewed on the
                    Site or Apps is the intellectual property owned by Trademarkers LLC, and
                    protected by copyrights, trademarks, service marks, patents or other
                    proprietary rights and laws. You are only permitted to use the content as
                    expressly authorized by us or the specific content provider. You may not
                    copy, reproduce, modify, republish, upload, post, transmit, or distribute
                    any documents or information from this site in any form or by any means
                    without prior written permission from us. You are solely responsible for
                    obtaining permission before reusing any copyrighted material that is
                    available on this site. Any unauthorized use of the materials appearing on
                    this site may violate copyright, trademark and other applicable laws and
                    could result in criminal or civil penalties.</p>

                    <p></p>

                    <h4 style="padding-top:20px;">PROCESS TO REPORT VIOLATIONS</h4>

                    <p>If you believe that any of the content of the Site or any related
                    platforms under the control of Trademarkers violates any rights that you
                    believe you have, you may provide notice in writing to our corporate
                    offices located at 246 West Broadway, New York NY 10013, (a) identifying
                    in sufficient detail the copyrighted work that you believe has been
                    infringed upon or other information sufficient to specify the
                    copyrighted work being infringed, (b) identify the material that you claim
                    is infringing the copyrighted work listed as photo or "screen shot",
                    provide information reasonably sufficient to permit us to contact you by
                    both a legal postal address of service (no P.O. box) as well as an address
                    for electronic communications (email), (c) include the following
                    statement: "I have a good faith belief that
                    use of the copyrighted materials described above as allegedly infringing
                    is not authorized by the copyright owner, its agent, or the law" and (d)
                    include a written statement and your signature below "I swear, under
                    penalty of perjury, that the information in the notification is accurate
                    and that I am the copyright owner or am authorized to act on behalf of the
                    owner of an exclusive right that is allegedly infringed.", for
                    adjudication as provided in the Digital Millennium Copyright Act.</p>

                    <p></p>

                    <h4 style="padding-top:20px;">DISCLAIMER OF WARRANTIES</h4>

                    <p>ALL MATERIALS AND SERVICES ON THIS SITE ARE PROVIDED ON AN "AS IS" AND "AS
                    AVAILABLE" BASIS WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
                    INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
                    OR FITNESS FOR A PARTICULAR PURPOSE, OR THE WARRANTY OF NON-INFRINGEMENT.
                    WITHOUT LIMITING THE FOREGOING, WE MAKE NO WARRANTY THAT (A) THE SERVICES
                    AND MATERIALS WILL MEET YOUR
                    REQUIREMENTS, (B) THE SERVICES AND MATERIALS WILL BE UNINTERRUPTED,
                    TIMELY, SECURE, OR ERROR-FREE, (C) THE RESULTS THAT MAY BE OBTAINED FROM
                    THE USE OF THE SERVICES OR MATERIALS WILL BE EFFECTIVE, ACCURATE OR
                    RELIABLE, OR (D) THE QUALITY OF ANY PRODUCTS, SERVICES, OR INFORMATION
                    PURCHASED OR OBTAINED BY YOU FROM THE SITE FROM US OR OUR AFFILIATES WILL
                    MEET YOUR EXPECTATIONS OR BE FREE FROM MISTAKES, ERRORS OR DEFECTS.</p>

                    <p>THIS SITE COULD INCLUDE TECHNICAL OR OTHER MISTAKES, INACCURACIES OR
                    TYPOGRAPHICAL ERRORS. WE MAY MAKE CHANGES TO THE MATERIALS AND SERVICES AT
                    THIS SITE, INCLUDING THE PRICES AND DESCRIPTIONS OF ANY PRODUCTS LISTED
                    HEREIN, AT ANY TIME WITHOUT NOTICE. THE MATERIALS OR SERVICES AT THIS SITE
                    MAY BE OUT OF DATE, AND WE MAKE NO COMMITMENT TO UPDATE SUCH MATERIALS OR
                    SERVICES.</p>

                    <p>THE USE OF THE SERVICES OR THE DOWNLOADING OR OTHER ACQUISITION OF ANY
                    MATERIALS THROUGH THIS SITE IS DONE AT YOUR OWN DISCRETION AND RISK AND
                    WITH YOUR AGREEMENT THAT YOU WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE TO
                    YOUR COMPUTER SYSTEM OR LOSS OF DATA THAT RESULTS FROM SUCH ACTIVITIES.</p>
                    <p>Through your use of the site, you may have the opportunities to engage in
                    commercial transactions with other users and vendors. You acknowledge that
                    all transactions relating to any merchandise or services offered by any
                    party, including, but not limited to the purchase terms, payment terms,
                    warranties, guarantees, maintenance and delivery terms relating to such
                    transactions, are agreed to solely between the seller or purchaser of such
                    merchandize and services and you. WE MAKE NO WARRANTY REGARDING ANY
                    TRANSACTIONS EXECUTED THROUGH, OR IN CONNECTION WITH THIS SITE, AND YOU
                    UNDERSTAND AND AGREE THAT SUCH TRANSACTIONS ARE CONDUCTED ENTIRELY AT YOUR
                    OWN RISK. ANY WARRANTY THAT IS PROVIDED IN CONNECTION WITH ANY PRODUCTS,
                    SERVICES,
                    MATERIALS, OR INFORMATION AVAILABLE ON OR THROUGH THIS SITE FROM A THIRD
                    PARTY IS PROVIDED SOLELY BY SUCH THIRD PARTY, AND NOT BY US OR ANY OTHER
                    OF OUR AFFILIATES.</p>
                    <p>Content available through this site often represents the opinions and
                    judgments of an information provider, site user, or other person or entity
                    not connected with us. We do not endorse, nor are we responsible for the
                    accuracy or reliability of, any opinion, advice, or statement made by
                    anyone other than an authorized Trademarkers spokesperson speaking in his
                    or her official capacity.</p>
                    <p>You understand and agree that temporary interruptions of the services
                    available through this site may occur as normal events. You further
                    understand and agree that we have no control over third party networks you
                    may access in the course of the use of this site, and therefore, delays
                    and disruption of other network transmissions are completely beyond our
                    control.</p>
                    <p>You understand and agree that the services available on this site are
                    provided "as is" and that we assume no responsibility for the timeliness,
                    deletion, mis-delivery or failure to store any user communications or
                    personalization settings.</p>

                    <h4 style="padding-top:20px;">LIMITATION OF LIABILITY</h4>

                    <p>IN NO EVENT SHALL WE OR OUR AFFILIATES BE LIABLE TO YOU OR ANY THIRD
                    PARTY FOR ANY SPECIAL, PUNITIVE, INCIDENTAL, INDIRECT OR CONSEQUENTIAL
                    DAMAGES OF ANY KIND, OR ANY DAMAGES WHATSOEVER, INCLUDING, WITHOUT
                    LIMITATION, THOSE RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
                    OR NOT WE HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, AND ON
                    ANY THEORY OF LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE
                    OF THIS SITE OR OF ANY WEB SITE REFERENCED OR LINKED TO FROM THIS
                    SITE.</p>

                    <p>FURTHER, WE SHALL NOT BE LIABLE IN ANY WAY FOR THIRD PARTY GOODS AND
                    SERVICES OFFERED THROUGH THIS SITE OR FOR ASSISTANCE IN CONDUCTING
                    COMMERCIAL TRANSACTIONS THROUGH THIS SITE, INCLUDING WITHOUT
                    LIMITATION THE PROCESSING OF ORDERS.</p>
                    <p>SOME JURISDICTIONS PROHIBIT THE EXCLUSION OR LIMITATION OF LIABILITY
                    FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, SO THE ABOVE LIMITATIONS MAY
                    NOT APPLY TO YOU.</p>
                    <p>SOME STATES OR JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF CERTAIN
                    WARRANTIES, SO SOME OF THE ABOVE LIMITATIONS MAY NOT APPLY TO YOU.</p>


                    <h4 style="padding-top:20px;">INDEMNIFICATION</h4>

                    <p>You agree to defend, indemnify, and hold us and our affiliates harmless
                    from all liabilities, claims, and expenses, including attorney's fees,
                    that arise from your use or misuse of this site. We reserve the right, at
                    our own expense, to assume the
                    exclusive defense and control of any matter otherwise subject to
                    indemnification by you, in which event you will cooperate with us in
                    asserting any available defenses.</p>
                    <p></p>
                    <h4 style="padding-top:20px;">SECURITY AND PASSWORD</h4>

                    <p>You are solely responsible for maintaining the confidentiality of your
                    password and account and for any and all statements made and acts or
                    omissions that occur through the use of your password and account.
                    Therefore, you must take steps to ensure that others do not gain access to
                    your password and account. Our personnel will never ask you for your
                    password. You may not transfer or share your account with anyone, and we
                    reserve the right to immediately terminate your account if you do transfer
                    or share your account.</p>
                    <p></p>

                    <h4 style="padding-top:20px;">E-MAIL, MESSAGING, BLOGGING, AND CHAT SERVICES</h4>

                    <p>We may make email, messaging, blogging, or chat services (collectively,
                    "Communications") available to users of our site, either directly or
                    through a third-party provider. We make available separate supplemental
                    agreements characterizing the relationship between you and us that, except
                    where expressly noted or contradictory, includes these Terms.</p>
                    <p>We will not inspect or disclose the contents of private Communications
                    except with the consent of the sender or the recipient, or in the
                    narrowly-defined situations provided under the Electronic Communications
                    Privacy Act, or as other required by law or by court or governmental
                    order. Further information is available in our Privacy Policy.</p>
                    <p>We may employ automated monitoring devices or techniques to protect our
                    users from mass unsolicited communications (also known as "Spam") and/or
                    other types of electronic communications that we deem inconsistent with
                    our business purposes. However, such devices or techniques are not
                    perfect, and we will not be responsible for any legitimate communication
                    that is blocked, or for any unsolicited communication that is not blocked.</p>

                    <h4 style="padding-top:20px;">INTERNATIONAL USE</h4>

                    <p>Although this site may be accessible worldwide, we make no representation
                    that materials on this site are appropriate or available for use in
                    locations outside the United States, and accessing them from territories
                    where their contents are illegal is prohibited. Those who choose to access
                    this site from other locations do so on their own initiative and are
                    responsible for compliance with local laws. Any offer for any product,
                    service, and/or information made in connection with this site is void
                    where prohibited.</p>
                    <p></p>


                    <h4 style="padding-top:20px;">TERMINATION OF USE</h4>

                    <p>You agree that we may, in our sole discretion, terminate or suspend your
                    access to all or part of the site with or without notice and for any
                    reason, including, without limitation, breach of these Terms of Use. Any
                    suspected fraudulent, abusive or illegal activity may be grounds for
                    terminating your relationship and may be referred to appropriate law
                    enforcement authorities.</p>
                    <p>Upon termination or suspension, regardless of the reasons thereof, your
                    right to use the services available on this site immediately ceases, and
                    you acknowledge and agree that we may immediately deactivate or delete
                    your account and all related information and files in your account and/or
                    bar any further access to such files or
                    this site. We shall not be liable to you or any third party for any claims
                    or damages arising out of any termination or suspension or any other
                    actions taken by us in connection with such termination or suspension.</p>

                    <h4 style="padding-top:20px;">GOVERNING LAW</h4>

                    <p>This site (excluding any linked sites) is controlled by us from our
                    Principal offices in New York, NY, United States of America. It can be
                    accessed from all 50 states, as well as from other countries around the
                    world. As each of these places has laws that may differ from those of
                    Delaware, by accessing this site both of us agree that the statutes and
                    laws of the State of New York, without regard to the conflicts of laws
                    principles thereof and the United Nations Convention on the
                    International Sales of Goods, will apply to all matters relating to the
                    use of this site and the purchase of products and services available
                    through this site. Each of us agrees and hereby submits to the exclusive
                    personal jurisdiction and venue any court of competent jurisdiction within
                    the State of Delaware with respect to such matters.</p>
                    <p></p>

                    <h4 style="padding-top:20px;">ENTIRE AGREEMENT</h4>

                    <p>These Terms constitute the entire agreement and understanding between us
                    concerning the subject matter of this agreement and supersedes all prior
                    agreements and understandings of the parties with respect to that subject
                    matter. These Terms may not be altered, supplemented, or amended by the
                    use of any other document(s). Any attempt to alter, supplement or amend
                    this document or to enter an order for products or services which are
                    subject to additional or altered terms and conditions shall be null and
                    void, unless otherwise agreed to in a written agreement signed by you and
                    us. To the extent that anything in or associated with this site is in
                    conflict or inconsistent with these Terms, these Terms shall take
                    precedence.</p>
                    <p></p>

                    <h4 style="padding-top:20px;">MISCELLANEOUS</h4>

                    <p>If any part of these Terms of Use is held invalid or unenforceable, that
                    portion shall be construed in a manner consistent with applicable law to
                    reflect, as nearly as possible, the original intentions of the parties,
                    and the remaining portions shall remain in full force and effect.</p>
                    <p>Any failure by us to enforce or exercise any provision of these Terms of
                    Use or related rights shall not constitute a waiver of that right or
                    provision.</p>

                    <h4 style="padding-top:20px;">CONTACT INFORMATION</h4>

                    <p><strong>Trademarkers LLC</strong><br>
                    45 Essex Street, Suite 202<br>
                    Millburn NJ 07041</p>

                    <p>Toll-Free: 1-800-293-3166<br>
                    Office: 212-468-5500<br>
                    Fax: 212-504-0888<br>
                    legal@trademarkers.com</p>




                    <!-- <p> This agreement controls and regulates the conditions for use of this website, Trademarkers.com (the “Site”).</p>

                    <p> This agreement was last modified on Aug, 2014. Trademarkers.com reserves the right to update it terms and conditions periodically. Use of this site will act as express consent that the user has read and understood the latest terms of this agreement. You agree that the Site will not be liable to you or any third-party for any modification or discontinuance of the Site.</p>

                    <p> These Terms constitute the entire agreement between you and the Site and govern your use of the Site, superseding any prior agreements between you and the Site. You also may be subject to additional terms and conditions that may apply when you use affiliate services, third-party content or third-party software, or visit another site linked to by this Site. The section titles in these Terms are for convenience only and have no legal or contractual effect.</p>

                    <p> ALL USERS SHOULD READ AND REVIEW THIS AGREEMENT BEFORE USING THIS WEBSITE. ANY PARTICIPATION OR USE OF THIS SITE WILL CONSTITUTE EXPRESS KNOWLEDGE AND ACCEPTANCE OF THIS AGREEMENT. IF YOU DO NOT AGREE TO ABIDE BY THIS AGREEMENT, PLEASE DO NOT USE THIS SITE.</p>

                    <h3> I. DEFINITIONS</h3>

                    <p> Trademarkers.com is a service provider bound by the following acceptable terms of conduct. Your right to use the site Trademarkers.com is governed by these terms and conditions (“Terms”) please carefully read these BEFORE using this site. In using this site you expressly agree that you will comply with these terms.</p>

                    <p> This site and all of its content are the sole property of Trademarkers.com and as such, are fully protected by international trademark, patent and copyright law, as well as all other intellectual proprietary laws.</p>

                    <p> These Terms represent the entire agreement between the User and Company and supersede any and all other agreements or terms verbal or written.</p>

                    <p> You agree to comply with all applicable laws, statutes, regulations, and ordinances concerning your use of the Site.</p>

                    <p> For all the intents and purposes of this "Agreement", the following definitions will apply:</p>

                    <p> “Attorney” is the lawyer or law firm in charge of the services requested by the “User” within the “Territory” and acting in the ordinary course of its business.</p>

                    <p> "User" is the “Titleholder” or his/her/its representative, identified as such by registration through the "Website". It is responsible for delivering all the information in each of the “Website” Service Application Forms. It is understood when the “User” is a representative that it is expressly authorized by the “Titleholder” to deliver this information to the “Company”. All communications will be exclusively held between the "Company" and the "User".</p>

                    <p> “Agreement” is the THE TERMS AND CONDITIONS OF USE and the <a href="/privacy">Privacy Policy</a>.</p>

                    <p> “Company” is Trademarkers.com, the corporation that provides the services of promoting and rendering of trademark registration, domain name registration, company formation and registration, and other related services.</p>

                    <p> “Contracting Parties” are the “User” and the “Company.” The “Company” is responsible for all services requested by the “User”.</p>

                    <p> “Charge” is the amount of money in the specified currency for each case, that the “User” must pay to the “Company” in order to receive the <a href="/service_contract">contracted service</a>.</p>

                    <p> “Power of Attorney” is a written authorization issued according to the corresponding legal requirements of each "Territory" that the “User” delivers to the “Company” and the “Attorney” in order for them to perform all the necessary actions required to apply for a Trademark or to Create a Company (Incorporation) on behalf of the "Titleholder."</p>

                    <p> “Domain Registry” is the organization in charge of administering top-level Domain Names. These top-level domain names are classified as generic domain names (gTLDs or Generic Top Level Domain, like .com, .org, .net, etc.) and domain names that identify a country code (ccTLDs or country code Top Level Domain Names, like com.ar, .es, .com.br, .cl, etc.).</p>

                    <p> “Service Request“ is the form that the “User” must fill out through the “Website” with the data therein required by the “Company” and by the “Attorney” to render a specified service.</p>

                    <p> “Specific Contract“ is the agreement whereby the “Company” pledges to perform the services requested by the “User” through the "Website" for a specific "Service Request". The ”User” in turn commits to deliver all the information therein required together with the payment of the “Charge” for the requested service.</p>

                    <p> “Titleholder” is the owner of the Trademark, Domain Name, or Company that wishes to register and that identifies him-/her-/itself as such in the Registration Request for Trademark, Domain Name, Company Formation Service or related services. The “Titleholder” may be a natural or legal person and act by him-/her-/itself or through his/her/its representative.</p>

                    <p> "Trademarks Office" is the Governmental Authority of the country where the “Attorney” requests and files the trademark registration.</p>

                    <p> “Companies Register” or "Registrar of Companies" is the Governmental Authority of the country/state/province where the “Attorney” requests and files the company creation.</p>

                    <p> “Website” is the internet site www.TRADEMARKERS.com, in which services related to the Registration of Trademarks, Domain Names and Company Formation Services are offered.</p>

                    <p> “Territory”, refers to a country jurisdiction or other territorial organizations in which the "Attorney" will exclusively render the services requested by the "User".</p>

                    <h2> II. GENERAL RULES</h2>

                    <ol>
                        <li> <p>The trademarks and logos used in the “Website” are protected by the standing rules and regulations on Industrial and Intellectual Property and may not be used in any way whatsoever by third parties without the authorization of their owner.</p></li>

                        <li> <p> All materials on the Site, including without limitation, logos, images, text, illustrations, audio and video files are protected by copyrights, trademarks, service marks, or other proprietary rights which are either owned by or licensed to the Site or owned by other parties who have posted on the Site. Materials from the Site and from any other web site owned, operated, controlled, or licensed by the Site may not be copied, reproduced, republished, uploaded, posted, transmitted, or distributed in any way.</p><p>Except as expressly provided herein, you are not granted any rights or license to patents, copyrights, trade secrets, rights of publicity or trademarks with respect to any of the Content, and TRADEMARKERS reserves all rights not expressly granted hereunder. TRADEMARKERS expressly disclaims all responsibility and liability for uses by you of any Content obtained on or in connection with the Site.</p></li>

                        <li> <p> TRADEMARKERS is only liable for rendering the services for which it has been contracted for through the “Website” which includes by definition telephone and fax orders.</p></li>

                        <li> <p> It is understood that the “User,” by virtue of accessing the “Website,” or using its services, expressly waives any indemnification, claim or right that it may have against the “Company” and its representatives.</p></li>

                        <li> <p> Under no circumstance will the “Company,” its “Attorneys” and/or employees be responsible for any damages that the “User” may incur while using the “Website” or any link to the site, except when expressly provided to the contrary. The extent of this liability clause is applicable to damages of any nature, including, but not limited to, the loss of data and programs, losses in results, losses or interruptions of businesses, and third party claims.</p></li>

                        <p> THE “COMPANY” SERVICES ARE PROVIDED ON AN “AS IS”, “AS AVAILABLE” BASIS WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT. THE “COMPANY” EXPRESSLY DISCLAIMS ANY REPRESENTATION OR WARRANTY THAT THE "COMPANY" SERVICES WILL BE ERROR-FREE, TIMELY, SECURE OR UNINTERRUPTED. NO ORAL ADVICE OR WRITTEN INFORMATION GIVEN BY THE "COMPANY" ITS EMPLOYEES, LICENSORS OR AGENTS WILL CREATE A WARRANTY; NOR MAY THE “USER” RELY ON ANY SUCH INFORMATION OR ADVICE.</p>

                        <p> SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF CERTAIN WARRANTIES, SO SOME OF THE ABOVE EXCLUSIONS MAY NOT APPLY TO THE “USER”.</p>

                        <p> UNDER NO CIRCUMSTANCES, INCLUDING NEGLIGENCE, WILL THE "COMPANY" OR ITS AFFILIATES BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL OR CONSEQUENTIAL DAMAGES THAT RESULT FROM THE USE OF OR INABILITY TO USE THE "COMPANY" SERVICES, INCLUDING BUT NOT LIMITED TO RELIANCE ON ANY INFORMATION OBTAINED ON THE "COMPANY" SERVICES; OR THAT RESULT FROM MISTAKES, OMISSIONS, INTERRUPTIONS, DELETION OF FILES OR E-MAIL, LOSS OF OR DAMAGE TO DATA, ERRORS, DEFECTS, VIRUSES, DELAYS IN OPERATION, OR TRANSMISSION, OR ANY FAILURE OF PERFORMANCE, WHETHER OR NOT LIMITED TO ACTS OF GOD, COMMUNICATION FAILURE, THEFT, DESTRUCTION OR UNAUTHORIZED ACCESS TO "COMPANY" RECORDS, PROGRAMS OR SERVICES. THE SUBSCRIBER HEREBY ACKNOWLEDGES THAT THIS PROVISION WILL APPLY WHETHER OR NOT THE "COMPANY" IS GIVEN NOTICE OF THE POSSIBILITY OF SUCH DAMAGES AND THAT THIS PROVISION WILL APPLY TO ALL CONTENT, MERCHANDISE OR SERVICES AVAILABLE.</p>

                        <p> IF THE “USER” ARE DISSATISFIED WITH THE SITE, OR ANY PORTION THEREOF, OR DO NOT AGREE WITH THESE TERMS, THE “USER”’s ONLY RECOURSE AND EXCLUSIVE REMEDY SHALL BE TO STOP USING THE SITE.
                        </p>

                        <p> As consideration for the services requested, the “User” agrees to pay the “Company,” applicable service(s) fees. All fees payable hereunder are non-refundable.</p>

                        <p> Requested services with pending documentation or information from the client, have a refund deadline of 90 days. After this period only official expenses of the Trademark Office shall be returned.</p>

                        <p> The information, services and products available to the “User” on this Site may contain errors and are subject to periods of interruption. While the Site does its best to maintain the information, services and products it offers on the Site, it cannot be held responsible for any errors, defects, lost profits or other consequential damages arising from the use of the Site.</p>

                        <li> <p> In spite of the efforts made by the “Company” the information delivered through the “Website” may contain typographical or data errors which will be rectified immediately upon detection. The “User” takes upon all responsibilities and risks for the use of the “Website” and for the information obtained therein.</p></li>

                        <li> <p> The “Company” and the “Attorney” take upon no responsibility in the event that a Trademark, Patent, Domain Name or Company Formation is not granted. The “User” accepts that any objection, opposition, or rejection, or any other situation that prevents the further processing of the registration request, is not the responsibility of the “Company” its directors, employees, and/or "Attorneys" and hereby waives any indemnification.</p></li>

                        <li> <p> All the actions taken by the “Company” and the “Attorney” will be performed according to the information delivered by the “User.” To such effect, the “Company” and the “Attorney” will not accept any responsibility if such information infringes any third party rights or contains errors and/or omissions. The “User” is absolutely responsible for the data delivered in any "Service Request" available on the “Website,” whether recommended or not by the “Company.”</p></li>

                        <li> <p> The “Company” neither represents nor controls other web sites that can be accessed through the “Website”, and consequently, has no responsibility for the content, use, products, and services that may be available in those other web sites.</p></li>

                        <li> <p> The surveillance service and the search service for Trademarks and Domain Names may contain errors and/or omissions derived from the information available in the data banks, whether proprietary or not, of the “Trademarks Offices” or the “Registration Administration” which may themselves contain errors and/or omissions. The “User” releases the “Company” and its “Attorneys” of any such responsibility, waiving any claim for indemnification due to error, incomplete information, or in respect of recommendations and propositions made by the "Company" according to this information.</p></li>

                        <li> <p> Under no circumstance the “Company” and the “Attorneys” will be responsible for any damage, loss of data or their inputs incurred by the “User” as a consequence of its connection with the “Website” or due to the interruption of a communication, or malfunctioning of the “Company” servers.</p></li>

                        <li> <p> The parties agree that the “Company” may change this "Website" "Agreement" without notification to the “User,” in order to improve the service or due to changes in the rules and regulations of the country in which the application is being filed. It is understood that the unmodified Terms and Conditions remain in full force and must be complied with by the parties.</p></li>

                        <li> <p> The “Company” will provide a password to the “User,” or a personal access keyword, to access the web pages of “My Account.” The “User” will be responsible for the custody and nondisclosure of this password as well as of its use. Certain services offered on or through this Site require the “User” to first open an Account. The “User” is responsible for maintaining the confidentiality of the “User”’s information, including their password, and for all activity that occurs under their account. The “User” agree to notify TRADEMARKERS immediately of any unauthorized use of their account or password, or any other breach of security. The “User” may be held liable for losses incurred by TRADEMARKERS or any other user of the Site due to someone else using their password or customer account. The “User” may not use anyone else's password or customer account at any time. The “User” may not attempt to gain unauthorized access to the Site. Should the “User” attempt to do so, assist others in making such attempts, or distributing instructions, software or tools for that purpose, then their customer My Account will be terminated. The “User” agrees to provide us with accurate, current and complete information about the “User” and their billing information as prompted by the registration process. The “User” may not use any automatic device, program, algorithm or methodology, or any similar or equivalent manual process, to access, acquire, copy, probe, test or monitor any portion of the Site or any content, or in any way reproduce or circumvent the navigational structure or presentation of the Site or any content, to obtain or attempt to obtain any materials, documents or information through any means not purposely made available through the Site. The “User” agrees that the “User” will not take any action that imposes an unreasonable or disproportionately large load on the infrastructure of the Site or any of the systems or networks comprising or connected to the Site.</p>

                            <p>The “User” also agree that TRADEMARKERS may, in its sole discretion and without prior notice to the “User”, terminate their access to the Site and their Account for any reason, including without limitation: (1) attempts to gain unauthorized access to the Site or assistance to others' attempting to do so, (2) overcoming software security features limiting use of or protecting any Content, (3) discontinuance or material modification of the Site or any service offered on or through the Site, (4) violations of this Terms of Use, (5) failure to pay for purchases, (6) suspected or actual copyright infringement, (7) unexpected operational difficulties, or (8) requests by law enforcement or other government agencies. The “User” agrees that TRADEMARKERS will not be liable to the “User” or to any third party for termination of their access to the Site.</p>
                        </li>

                        <li> <p> The delays and errors resulting from force majeure will not be considered a breach of the “Company” or the "Attorney" services, including but not limited to, problems arising from elements of nature, fires, acts of war, national security attacks, coups d’état, or interruptions in the “Website” derived from major technical problems as determined by the “Company.”</p></li>

                        <li> <p> Upon submittal of the Service Request, the non-“Titleholder” “User” certifies that it is authorized by the “Titleholder” “User” to request the services and bind it according to the terms of this Contract.</p></li>

                        <li> <p> The “Company” will not return the “Charge” paid, in case of errors and/or omissions made by the “User,” including, but not limited to, a breach in the terms of this “Agreement”, delivery of incorrect information when submitting any “Service Request” and/or the incorrect modification of necessary information for the processing thereof.</p></li>

                        <li> <p> The “User” is responsible to contract through the “Website”, within the deadlines established by the “Company”, the legal services in case of objections, oppositions, and refusals in the processing of the “Service Request”. The “User” must be aware that the deadlines are binding, meaning that if the required action is not complied with within the prescribed term, the right for a cause of action is relinquished, what normally leads to the loss of the registration request and/or other rights.</p></li>

                        <li> <p> The “User” accepts and declares that, if it does not contract certain services within the term required by the “Company,” it will be assumed that it relinquishes its interest in pursuing the processing of the respective “Service Request” terminating the “Specific Contract,” without any refund of money whatsoever by the “Company.”</p></li>

                        <li> <p> The “Charges” for the different services offered on the “Website” are those in force at the time of payment by the “User.”</p><p>TRADEMARKERS RESERVES THE RIGHT, AT ANY TIME, TO CHANGE ITS PRICES AND BILLING METHODS FOR PRODUCTS OR SERVICES SOLD, EFFECTIVE IMMEDIATELY UPON POSTING ON THE SITE OR BY E-MAIL DELIVERY TO THE “USER”.</p></li>

                        <li> <p> The “User” declares that by virtue of filling out the form of any “Service Request” it guarantees to the “Company” that all the information supplied in them is true and based on the principles of mercantile bona fide and further guarantees that it knows this “Agreement” in its entirety as well as the remaining contracts associated with it.</p></li>

                        <li> <p> The “User,” by means of this “Agreement,” declares that it is entirely responsible for any claim, procedure, damage, injury, loss, or cost that may arise from, or that is related to, its deeds.</p></li>

                        <p> The “company” is not responsible for the publication and/or the content of the web site. It is not responsible either for complaints, law suits, trials, damages, costs or expenses related to the violation of third person rights claimed by them inside or outside a tribunal. The “User” declares that it is fully responsible for any complaint, procedure, damage, slander.</p>

                        <p> The “User” agrees to indemnify and hold harmless the Indemnified Parties from any claim or demand, including reasonable attorneys' fees, made by any third party due to or arising out of your use of the Site, the violation of these Terms by the “User”, or the infringement by the “User”, or other users of the Site using their computer, of any intellectual property or other right of any person or entity. The Site reserves the right, at its own expense, to assume the exclusive defense and control of any matter that is subject to indemnification by the "User"</p>

                        <p> The requested and registered domain name will be the “User’s” exclusive responsibility; whose registration should not contradict the current standards, morally or ethically. In case the “User” does not comply with the former the User must respond before the authorities, releasing the “Company” from any responsibility.</p>

                        <p> The “Company” will not be responsible for the content of the Domain Name Registration that has been requested by the “User”. The “User” is exclusively responsible for the content of its Domain name which must not be contrary to the law, accepted moral codes and norms of good behavior. In case of infringement, the “User” shall respond before the correspondent authorities, liberating the “Company” from any responsibility.
                        </p>

                        <li> <p> The “Company” and the "Attorney" are obliged to render the requested service after it has received from the “User”: (i) the information required in the respective “Service Request” contained on the “Website”, (ii) payment of the respective charge, and (iii) additional information, in such cases where this is necessary.</p></li>

                        <li> <p> The “User” must provide and cooperate, during the processing of the “Service Request” in an integral, complete and opportune manner, delivering precise and trustworthy information, and responding to the requirements for information requested by the “Company” within the terms indicated.</p></li>

                        <li> <p> The Trademark Registry grants the “Titleholder” the right to use the Trademark in the way it is conferred and for the products, services, and industrial or commercial establishments included in such rights as set and earned by the local laws and rules.</p></li>

                        <li> <p> The Company Formation Service grants the “Titleholder” the right to use the Company in the way it is conferred and for the purpose included in such rights as set and earned by the local laws and rules.</p></li>

                        <li> <p> The Site has created a Privacy Policy setting forth how information collected about the “User” is collected, used and stored. The “Users” use of the Site constitutes acknowledgment and agreement with our privacy policy. The “User” further acknowledge and agree that The Site may use your personal information in the manner described in our Privacy Policy.</p></li>

                        <li> <p> The “Company” may terminate this “Agreement,” the “Contract” and the authorization to the “User” to use this “Website” at the moment it ceases to comply with its obligations with the “Company.”</p><p> It will be a cause for termination of this "Agreement": (i) the delivery of any inaccurate information by the "User"; (ii) the "User’s" refusal to update or deliver the requested information; and (iii) the "User’s" failure to provide instructions within the terms required by the "Company" to continue processing the contracted services.</p></li>

                        <li> <p> All communications between the parties will be via e-mail or fax and directed to the "Contracting Parties" addresses.</p></li>

                        <li> <p> These Terms, the use of the site and TRADEMARKERS's collection and use of customer information shall be governed by, construed and enforced in accordance with the laws of the Country that the services are ordered for without regard to its choice of law provisions. Any action the “User” or any third party may bring to enforce these Terms in the United States, shall be brought only in either the state or Federal courts located in Miami, Florida, (Which court (or mediation)do you want to use?) and the “User” expressly consent to the jurisdiction of said courts.</p><p>The “User” also agrees that regardless of any statute or law to the contrary, any claim or cause of action arising out of or related to use of the Site or the Terms must be filed within one (1) year after such claim or cause of action arose or be forever barred.</p></li>

                        <li> <p> The “Company” reserves the right, without expression of cause, to reject any request for service.</p></li>

                        <li> <p> All new Services requests ordered directly to the managers without using this web site will be subject to these Terms of Use.</p></li>

                        <li> <p> The “User” acknowledges that they have read, understand and agree to be bound by TRADEMARKERS’s Anti-Spam Policy available here.</p><p>The “User” agrees that TRADEMARKERS may immediately terminate any Account which it believes, in its sole and absolute discretion, is transmitting or is otherwise connected with any spam or other unsolicited bulk email. In addition, if actual damages cannot be reasonably calculated then the “User” agrees to pay TRADEMARKERS liquidated damages in the amount of $1.00 for each piece of spam or unsolicited bulk email transmitted from or otherwise connected with their Account.</p></li>

                        <li> <p> The Site has the right to terminate your access for any reason if we believe you have violated these Terms in any manner. You agree not to hold the Site liable for such termination, and further agree not to attempt to use the Site after termination.</p></li>

                        <li> <p> You agree that, except as otherwise provided in this Terms of Use, there shall be no third party beneficiaries to these Terms.</p></li>

                        <li> <p> If you believe that your work has been copied in a way that constitutes copyright infringement, or your intellectual property rights have been otherwise violated, please provide the following information to the Site’s Intellectual Property Agent:</p></li>

                        <p> a. A physical or electronic signature of a person authorized to act on behalf of the owner of an exclusive right that is allegedly infringed;</p>

                        <p> b. Identification of Intellectual Property Rights claimed to have been infringed, or, if multiple Intellectual Properties covered by a single notification, a representative list of such infringement;</p>

                        <p> c. Identification of the material that is claimed to be infringing or to be the subject of infringing activity and that is to be removed or access to which is to be disabled, and information reasonably sufficient to permit TRADEMARKERS to locate the material on the Site;</p>

                        <p> d. Your name, address, telephone number and e-mail address and Information reasonably sufficient to permit TRADEMARKERS to contact the complaining party such as an address, telephone number, and if available, an electronic mail address at which the complaining party may be contacted;</p>

                        <p> e. A signed statement by you that you have a good faith belief that the disputed use is not authorized by the Intellectual Property Rights owner, its agent, or the law; and</p>

                        <p> f. A statement by you, made under penalty of perjury, that the information provided in your notice is accurate and that you are the copyright or intellectual property owner or authorized to act on the copyright or intellectual property owner’s behalf.</p>

                        <p> The Site may, under appropriate circumstances and at our own discretion, disable and/or terminate the accounts of users who may be repeat infringers.</p>

                        <li> <p> Any translation of this Policy is done for local requirements and in the event of a dispute between the English and any non-English versions, the English version of this License Agreement shall govern.</p></li>

                        <li> <p> Please report any violations of these Terms to:admin@trademarkers.com.</p></li>

                        <li> <p> You acknowledge and agree that if any provision of these Terms shall be unlawful, void, or for any reason unenforceable, then that provision shall be deemed severable from these Terms and shall not affect the validity and enforceability of any remaining provisions. Furthermore, if any provision of these Terms is found by a court of competent jurisdiction to be invalid, the parties nevertheless agree that the court should endeavor to give effect to the parties' intentions as reflected in the provision. The Site's failure to exercise or enforce any right or provision of these Terms shall not constitute a waiver of such right or provision unless acknowledged and agreed to by the Site.</p></li>

                        <li> <p> You acknowledge, consent and agree that the Site may access, preserve and disclose your account information and Content you upload, post, or otherwise make available on the Site if required to do so by law or in a good faith belief that such access, preservation or disclosure is reasonably necessary to: (i) comply with legal process; (ii) comply with legal requirements imposed by Federal, State or Local law or authorities (iii) enforce these Terms; (iv) respond to claims that any Content violates the rights of third parties; (iv) respond to your requests for customer service; or (vi) protect the rights, property or personal safety of the Site, its users and the public.</p></li>

                        <li> <p> The Site has no control over and is not responsible for the content of or claims made on websites that may be linked to or from the Site, whether or not they may be affiliated with the Site. Trademarkers.com is not responsible for the privacy practices or the content of any other websites to which the Site links or to which link to the Site.</p></li>

                    </ol> -->
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
