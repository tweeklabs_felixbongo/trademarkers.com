@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h5> Register your Trademark - before it's too late!</h5>
                </div>

                @foreach( $continents as $continent )
                    @if ( $continent['name'] != 'United States' ) 
                        <div class="card-body">
                            <h4> {{ $continent['name'] }} </h4>
                            <hr>
                            <div class="row">
                                @foreach( $continent->countries as $country )
                                
                                    @if ( $continent['name'] == $country->continent['name'] )
                                        <div class="col-md-4">
                                            <a href="/trademark-registration-in-{{ strtolower(str_replace( ' ', '_', $country['name'] )) }}" style="color: #000;">
                                                <p>
                                                    <img src="images/{{ $country['avatar'] }}" /> 
                                                    {{ $country['name'] }} 
                                                </p>
                                            </a>
                                        </div>
                                    @endif
                                
                                
                                @endforeach
                            </div>
                        </div>
                    @endif
                @endforeach
                
            </div>
        </div>
    </div>
</div>

@endsection
