@extends('layouts.master')

@push('head')
<!-- Global site tag (gtag.js) - Google Ads: 851729164 --> 
<script src="https://apis.google.com/js/platform.js"></script>

@endpush

@section('content')
<div class="youtube-subscribe-container">
    <h1 class="text-center" > Subscribe to <img src="{{URL::asset('/images/youtube_banner.svg')}}" alt="Youtube Channel"></h1>
    <h2 class="text-center" style="margin-bottom:40px;">And get an exlusive <b>PROMOCODE DISCOUNT!!!</b></h2>
    <div class="container">
        
        @php 
        $user = null;
            if(Auth::check()) {
                $user = Auth::user();
                
            }
        @endphp
        
        <!-- ROW BLOCK FOR SUBSCRIBE START -->
        <div class="row">
            @if ( !$user || ($user->youtube_subscribe == 'no' && $user->youtube_promo_claim == 'no') )
            <div class="col-md-4 text-center con-steps">
                <span id="step-1-line" class="step-line"></span>
                <span id="step-1" class="step active">
                    1
                </span>

                <div id="step-content-1" class="content-step">
                    <!-- <a href="#" id="yt-register" class="btn btn-register"> Register </a> -->
                    <p class="label-steps">Register here</p>
                    <input id="yt-register" type="button" value="Register" class="btn btn-register">
                </div>
                
            </div>

            <div class="col-md-4 text-center con-steps">
                 <span id="step-2-line" class="step-line initial"></span>
                 <span id="step-2" class="step step-2">
                    2
                </span>

                <div id="step-content-2" class="content-step disabled">
                    <p class="label-steps">Subscribe to our channel</p>
                    <input id="yt-subscribe" type="button" value="Subscribe" class="btn btn-register" disabled>
                </div>

                
            </div>
            

            <div class="col-md-4 text-center con-steps">
                <span id="step-3" class="step step-3">
                    3
                </span>

                <div id="step-content-3" class="content-step">
                    <p class="label-steps"></p>
                    <div class="code-claim-block">
                        @if(Auth::check())
                            <p>Proceed to cart to claim your Discount!</p>
                            <strong id="code"></strong>
                            <a class="btn btn-register" href="/cart">Proceed</a>
                        @else
                            <p>Login to claim your Discount!</p>
                            <strong id="code"></strong>
                            <span id="btn-login-popup" class="btn btn-register">Login</span>
                        @endif
                    </div>
                </div>
            </div>
            @endif

            @if ( $user && $user->youtube_subscribe == 'yes' && $user->youtube_promo_claim == 'no' )
                <div class="col-md-4 text-center con-steps" style="margin:auto;">
                    <span id="step-3" class="step step-3 visited">
                        <i class="fa fa-check"></i>
                    </span>

                    <div id="step-content-3" class="content-step">
                        <p class="label-steps"></p>
                        <div class="code-claim-block-visited">
                            @if(Auth::check())
                                <p>Proceed to cart to claim your Discount!</p>
                                <strong id="code"></strong>
                                <a class="btn btn-register" href="/cart">Proceed</a>
                            @else
                                <p>Login to claim your Discount!</p>
                                <strong id="code"></strong>
                                <span id="btn-login-popup" class="btn btn-register">Login</span>
                            @endif
                        </div>
                    </div>
                </div>
            @endif

            @if ( $user && $user->youtube_subscribe == 'yes' && $user->youtube_promo_claim == 'yes' )
                <div class="col-md-12 text-center con-steps">
                    <h2>Promo Code Claimed!</h2>
                </div>
            @endif

            


        </div>
        
        <!-- ROW BLOCK FOR SUBSCRIBE END -->

        <!-- MESSAGE MODAL AFTER CLICK ON SUBSCRIBE -->
        <div class="modal transparent-modal" id="subscribe-notice-transparent" data-id="subscribe-notice-transparent" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Notification</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" style="text-align:center">
                        <h2>Thank You!</h2>
                        <h4>You will be redirected to our channel.<br>After subscibing to our channel<br>press back on your browser to claim your <br>DISCOUNT CODE</h4>
                        <a id="yt-link" href="https://www.youtube.com/channel/UCFzecLVDoJLH3sX78Cu7cvg?sub_confirmation=1">Youtube Channel</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- MESSAGE MODAL AFTER CLICK ON SUBSCRIBE -- END -->

        <!-- Login -->
        <div class="modal " id="subscribe-notice-transparent-login" data-id="subscribe-notice-transparent" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Customer Login</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" >
                    
                    <form id="frm-form-login" method="POST" action="/user/login">
                        @csrf

                        
                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ session('case_email_address') ?  session('case_email_address') : old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Login -- END -->


    </div>

</div>

@endsection
