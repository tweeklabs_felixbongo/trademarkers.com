<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
	<head>

		<meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
		<style>
			body{
				font: small/1.5 Arial,Helvetica,sans-serif;
			}
			p {
				margin-bottom: 15px;
				line-height: 20px;
				text-align: justify;
			}
		</style>
    </head>
    <body>
	<p>Hi <strong>{{ $user->name }}</strong>,</p>
	<p></p>
	<p>Congratulations on taking the important first step in starting your Trademark Monitoring. We're glad you've chosen us to help you get started.</p>

	<p>You can count on our vast experience in serving our customers to help get your Trademark protected legally.</p>

	<p><strong>ROLE OF TRADEMARK MONITORING</strong></p>

	<p>Trademark Monitoring Service offered by Trademarkers.com  is an activity that protects your mark from the potential infringements. Trademark Monitoring is basically a process of monitoring your trademark and looking out for all the similar marks that are applied. This monitoring enables you to initiate an action against infringement. This is carried out to make sure that there is no similar mark or potentially harmful mark registered.</p>  

	<p>One of our Trademark Executive will get in touch with your shortly.</p> 
	
	<p>In the meantime, if you have any questions, you may call us at  1-800-293-3166  or visit our website at <a href="https://trademarkers.com/">www.trademarkers.com</a>. We're available to provide assistance.</p> 

		<br>
	<p>Best regards,</p>
	
	<p>Trademarkers, LLC</p>
    </body>
</html>