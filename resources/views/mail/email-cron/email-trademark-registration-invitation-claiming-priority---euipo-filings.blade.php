@extends('layouts.event-mail-master', ['event' => $event, 'email' => $email, 'title' => 'Trademark Registration Invitation Claiming Priority - US Filings' ])

@section('content')

<!-- body -->

       <table cellpadding="0" cellspacing="0" class="es-content" align="center" mc:repeatable style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;"> 
         <tr style="border-collapse:collapse;"> 
          <td align="center" bgcolor="#ffffff" style="padding:0;Margin:0;background-color:#FFFFFF;"> 
           <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" width="600" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;"> 
             <tr style="border-collapse:collapse;"> 
              <td align="left" style="padding:0;Margin:0;padding-left:20px;padding-right:20px;"> 
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td width="560" align="center" valign="top" style="padding:0;Margin:0;"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="padding:0;Margin:0;padding-top:15px;" mc:edit="block_5"><h2 style="Margin:0;padding-bottom:20px;line-height:31px;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:26px;font-style:normal;font-weight:bold;color:#1475A9;">Expand Your Trademark Protection By CLAIMING PRIORITY Now!</h2></td> 
                     </tr> 

                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="Margin:0;padding-top:5px;padding-bottom:5px;padding-left:20px;padding-right:20px;font-size:0;" bgcolor="transparent" mc:edit="block_7"> 
                       <table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td style="padding:0;Margin:0px 0px 0px 0px;border-bottom:1px solid #CCCCCC;background:none;height:1px;width:100%;margin:0px;"></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
           </table></td> 
         </tr> 
       </table> 
       <table cellpadding="0" cellspacing="0" class="es-content" align="center" mc:repeatable style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;"> 
         <tr style="border-collapse:collapse;"> 
          <td align="center" bgcolor="#ffffff" style="padding:0;Margin:0;background-color:#FFFFFF;"> 
           <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" width="600" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;"> 
             <tr style="border-collapse:collapse;"> 
              <td align="left" style="padding:0;Margin:0;padding-left:20px;padding-right:20px;"> 
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td width="560" align="center" valign="top" style="padding:0;Margin:0;"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" style="padding:0;Margin:0;" mc:edit="block_8"><h2 style="Margin:0;line-height:31px;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:26px;font-style:normal;font-weight:bold;color:#0B5394;"><br>Dear Sirs,</h2></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
             <tr style="border-collapse:collapse;"> 
              <td align="left" style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px;"> 
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td width="560" align="center" valign="top" style="padding:0;Margin:0;"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" style="padding:0;Margin:0;" mc:edit="block_9"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;text-align:justify;">
                      
                      Our records show that you have successfully filed your trademark in the European Union Intellectual Property Office (EUIPO) on {{ date('M d, Y', strtotime( $event->caseOwned->tm_filing_date )) }} with trademark no. {{ $event->caseOwned->number }}. <br><br>Congratulations as this trademark application gets you one step closer to getting GLOBAL BRAND PROTECTION.<br></p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;"><br></p></td> 
                     
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td style="padding:0;Margin:0;" mc:edit="block_10"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;text-align:justify;">
                      
                      Securing the protection of your trademark "{{ $event->caseOwned->trademark }}" plays an important role in today’s competitive online marketplace. However, it is important to note that your trademark application only protects you in the member states of the European Union and does NOT prevent others from registering the same trademark in other countries.<br><br></p></td> 
                     
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td style="padding:0;Margin:0;" mc:edit="block_11"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;text-align:justify;">
                      
                      Expanding your trademark protection through “Rights of Priority” is a way to GO. This time-limited right originates from the Paris Convention for the Protection of Industrial Property. Sounds interesting, isn’t it? 
                      <b style="text-decoration:underline">This trademark strategy helps your organization expand and leverage your business in any of its 177 countries by filing the identical trademark before {{ date('M d, Y', strtotime( $event->caseOwned->getTmExpiredFillingDate6M() )) }} and effectively claim its priority right to get the same filing date of {{ date('M d, Y', strtotime( $event->caseOwned->tm_filing_date )) }}.</b><br><br></p></td> 
                     
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" style="padding:0;Margin:0;" mc:edit="block_12"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;">
                      
                      Remember that in most countries, the first filer gets priority over prior users of the trademarks in commerce, even if at the time of filing, the first filer has not used the mark in commerce themselves. To help you decide whether a foreign trademark application is needed, here are the questions worth answering now:</p></td> 
                     
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" style="padding:0;Margin:0;" mc:edit="block_13"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;"><br></p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;">
                      
                      1. Do you have plans for future expansion abroad?<br>
                      2. In what countries are you effectively offering your products or services.<br>
                      3. In what countries are you planning to offer your products or services in the future?<br>
                      4. Do you have business operations (e.g. offices, distributors, partnerships or manufacturers) overseas?</p></td> 
                     
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" style="padding:0;Margin:0;" mc:edit="block_14"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;"><br></p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;">Remember, time is of the essence. Act fast. Protect your trademark Globally. We exist because we care for everyone’s businesses. Your success in brand protection is our story!</p></td> 
                     </tr> 
                     
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" style="padding:0;Margin:0;" mc:edit="block_15">
                      	<span class="es-button-border es-button-border-4" style="border-style:solid;border-color:#659C35;background:#0B5394;border-width:0px;display:block;border-radius:25px;width:auto;margin-top:20px;"><a href="https://trademarkers.com/registration/step2/US?action=setFields&case={{$event->caseOwned->id}}&priority=true" class="es-button es-button-3" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#0B5394;border-width:10px 20px 10px 20px;display:block;background:#0B5394;border-radius:25px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;border-left-width:0px;border-right-width:0px;">CLAIM THE PRIORITY RIGHT OF MY TRADEMARK NOW!</a></span>
                      </td> 
                     </tr> 
                     
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" style="padding:0;Margin:0;" mc:edit="block_15"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;"><br></p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;text-align:center;"><strong>What is a Priority Claim? How Do Priority Claims Work? We hope we can answer these and many more questions via this video! Watch as one of our licensed attorneys explains these concepts and more!</strong></p></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
             <tr style="border-collapse:collapse;"> 
              <td align="left" style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px;"> 
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td width="560" align="center" valign="top" style="padding:0;Margin:0;"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="padding:0;Margin:0;" mc:edit="block_16"><a target="_blank" href="https://www.youtube.com/watch?v=EMsLW1B0Dfc" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:none;color:#659C35;"><img class="adapt-img" src="https://fcttwq.stripocdn.email/content/guids/videoImgGuid/images/38451588753538179.png" alt="What is a Priority Claim? - TradeMarkers®" width="560" title="What is a Priority Claim? - TradeMarkers®" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></a></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
             <tr style="border-collapse:collapse;"> 
              <td align="left" bgcolor="#ffffff" style="padding:0;Margin:0;padding-left:20px;padding-right:20px;background-color:#FFFFFF;"> 
               <!--[if mso]><table width="560" cellpadding="0" cellspacing="0"><tr><td width="270" valign="top"><![endif]--> 
               <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td width="270" class="es-m-p20b" align="left" style="padding:0;Margin:0;"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="Margin:0;padding-top:10px;padding-bottom:10px;padding-left:10px;padding-right:10px;" mc:edit="block_17"><span class="es-button-border es-button-border-4" style="border-style:solid;border-color:#659C35;background:#0B5394;border-width:0px;display:block;border-radius:25px;width:auto;"><a href="https://trademarkers.com/registration/step2/US?action=setFields&case={{$event->caseOwned->id}}" class="es-button es-button-3" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#0B5394;border-width:10px 20px 10px 20px;display:block;background:#0B5394;border-radius:25px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;border-left-width:0px;border-right-width:0px;">Trademark Application</a></span></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="padding:10px;Margin:0;" mc:edit="block_18"><span class="es-button-border es-button-border-4" style="border-style:solid;border-color:#659C35;background:#0B5394;border-width:0px;display:block;border-radius:25px;width:auto;"><a href="https://trademarkers.com/{{ $dataUrl['oar'] }}" class="es-button es-button-3" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#0B5394;border-width:10px 20px 10px 20px;display:block;background:#0B5394;border-radius:25px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;border-left-width:0px;border-right-width:0px;">Office Action Response</a></span></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="padding:10px;Margin:0;" mc:edit="block_19"><span class="es-button-border es-button-border-4" style="border-style:solid;border-color:#659C35;background:#0B5394;border-width:0px;display:block;border-radius:25px;width:auto;"><a href="https://trademarkers.com/{{ $dataUrl['afr'] }}" class="es-button es-button-3" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#0B5394;border-width:10px 20px 10px 20px;display:block;background:#0B5394;border-radius:25px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;border-left-width:0px;border-right-width:0px;">Trademark Renewal</a></span></td> 
                     </tr> 
                     
                   </table></td> 
                 </tr> 
               </table> 
               <!--[if mso]></td><td width="20"></td><td width="270" valign="top"><![endif]--> 
               <table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td width="270" align="left" style="padding:0;Margin:0;"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="padding:10px;Margin:0;" mc:edit="block_21"><span class="es-button-border es-button-border-4" style="border-style:solid;border-color:#659C35;background:#0B5394;border-width:0px;display:block;border-radius:25px;width:auto;"><a href="https://trademarkers.com/DB-0H0A-P" class="es-button es-button-3" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#0B5394;border-width:10px 20px 10px 20px;display:block;background:#0B5394;border-radius:25px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;border-left-width:0px;border-right-width:0px;">Trademark Study</a></span></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" bgcolor="transparent" style="padding:10px;Margin:0;" mc:edit="block_22"><span class="es-button-border es-button-border-4" style="border-style:solid;border-color:#659C35;background:#0B5394;border-width:0px;display:block;border-radius:25px;width:auto;"><a href="https://trademarkers.com/{{ $dataUrl['ita'] }}" class="es-button es-button-3" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#0B5394;border-width:10px 20px 10px 20px;display:block;background:#0B5394;border-radius:25px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;border-left-width:0px;border-right-width:0px;">International Application</a></span></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="padding:10px;Margin:0;" mc:edit="block_23"><span class="es-button-border es-button-border-4" style="border-style:solid;border-color:#659C35;background:#0B5394;border-width:0px;display:block;border-radius:25px;width:auto;"><a href="https://trademarkers.com/CA-0B1B-M" class="es-button es-button-3" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#0B5394;border-width:10px 20px 10px 20px;display:block;background:#0B5394;border-radius:25px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;border-left-width:0px;border-right-width:0px;">Trademark Monitoring</a></span></td> 
                     </tr> 
                     
                   </table></td> 
                 </tr> 
               </table> 
               <!--[if mso]></td></tr></table><![endif]--></td> 
             </tr> 
             
           </table></td> 
         </tr> 
       </table> 
      
@endsection