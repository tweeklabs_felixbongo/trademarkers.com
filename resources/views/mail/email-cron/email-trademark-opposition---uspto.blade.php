@extends('layouts.event-mail-master', ['event' => $event, 'email' => $email, 'title' => 'Trademark Opposition' ])

@section('content')

<!-- body -->

<table cellpadding="0" cellspacing="0" class="es-content" align="center" mc:repeatable style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;"> 
         <tr style="border-collapse:collapse;"> 
          <td align="center" bgcolor="#ffffff" style="padding:0;Margin:0;background-color:#FFFFFF;"> 
           <table bgcolor="#EFEFEF" class="es-content-body" align="center" cellpadding="0" cellspacing="0" width="600" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#EFEFEF;"> 
             <tr style="border-collapse:collapse;"> 
              <td align="left" bgcolor="#ffffff" style="padding:0;Margin:0;padding-left:20px;padding-right:20px;background-color:#FFFFFF;"> 
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td width="560" align="center" valign="top" style="padding:0;Margin:0;"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="padding:0;Margin:0;padding-top:20px;padding-bottom:20px;font-size:0px;" mc:edit="block_2"><a target="_blank" href="https://trademarkers.com/EC-0D1A-D" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:16px;text-decoration:underline;color:#2CB543;"><img class="adapt-img" src="https://fcttwq.stripocdn.email/content/guids/CABINET_99dbaabaa8fe07212f124009a7047115/images/6681587714325838.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;" width="560"></a></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="padding:0;Margin:0;padding-top:5px;" mc:edit="block_3"><h1 style="Margin:0;line-height:29px;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:24px;font-style:normal;font-weight:bold;color:#1475A9;text-align:left;"><br>Dear Sirs,</h1></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" style="padding:0;Margin:0;padding-top:10px;" mc:edit="block_4"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;">We want to bring to your immediate attention the newly filed trademark which may create confusion to your earlier trademark rights.<br><br>Don't allow this application at the United States Patent and Trademark Office (USPTO) be granted registration as this will severely affect the reputation of your trademark in the marketplace!</p></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" style="padding:0;Margin:0;padding-top:10px;" mc:edit="block_5"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;"><strong>Trademark Name: {{ $event->case->trademark }}<br>Trademark Number: {{ $event->case->number }}<br>Application Date: {{ date('M d, Y', strtotime( $event->case->tm_filing_date )) }}<br>Deadline to Oppose: {{ date('M d, Y', strtotime( $event->case->getTmExpiredFillingDate() )) }}</strong><br></p></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" style="padding:0;Margin:0;padding-top:10px;" mc:edit="block_6"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;">Do you know that you can stop this application by filing an opposition?<br><br>An opposition is a proceeding in which one party is seeking to prevent registration of another party's trademark. Under the law, if a party believes that he will be damaged by the registration of a mark, he can file an opposition.<br><br>This is a complex legal procedure that requires the attention of experienced trademark attorneys who know the technicalities of trademark registration procedure.<br><br>We analyze the specific facts and evidence of each case and formulate a plan to best achieve our client’s goals, whether by settlement or through official litigation procedures.</p></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                     <td align="" style="padding:30px 0 15px;Margin:0;font-weight:bold;font-size:20px;color:#1475a9" mc:edit="block_7"><span class="" style="">FAQ'S About Trademark Opposition</span></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
           </table></td> 
         </tr> 
       </table> 
       <table cellpadding="0" cellspacing="0" class="es-content" align="center" mc:repeatable style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;"> 
         <tr style="border-collapse:collapse;"> 
          <td align="center" bgcolor="#ffffff" style="padding:0;Margin:0;background-color:#FFFFFF;"> 
           <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" width="600" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;"> 
             <tr style="border-collapse:collapse;"> 
              <td align="left" style="padding:0;Margin:0;padding-left:20px;padding-right:20px;"> 
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td width="560" align="center" valign="top" style="padding:0;Margin:0;"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" style="padding:0;Margin:0;" mc:edit="block_8"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;"><strong>1. How long does the process take?</strong></p> 
                       <ul> 
                        <li style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;Margin-bottom:15px;color:#333333;">An opposition proceeding before the Registrar can take as long as two to four years or even longer.</li> 
                       </ul><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;"><strong>2. Who can oppose a trademark application?</strong></p> 
                       <ul> 
                        <li style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;Margin-bottom:15px;color:#333333;">If a person objects to the registration of a trademark that has been applied for, they can start an opposition as long as it properly raises at least one ground of opposition set out in sufficient detail to enable the other party (the applicant) to reply to it. An opponent can be one or more individuals or entities.</li> 
                       </ul><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;"><strong>3. Can I oppose any trademark that I come across?</strong></p> 
                       <ul> 
                        <li style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;Margin-bottom:15px;color:#333333;">No. You can only oppose a trademark that has been applied for registration in the United States but that has not yet been registered. An opposition can take place only once an application has been advertised in the Trademark Official Gazette on United States Patent and Trademark Office (USPTO) website. An opposition must be filed within thirty days after the date of publication, or within an extension of time granted by the Board for filing an opposition.</li> 
                       </ul><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;"><strong>4.&nbsp;What kind of assistance can the Trademarks Opposition Board provide?</strong></p> 
                       <ul> 
                        <li style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;Margin-bottom:15px;color:#333333;">The Board cannot provide legal or substantive advice with respect to your case, nor can the Board comment on the merits of your case. The Board however, can provide general information with respect to opposition procedure.</li> 
                       </ul><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;"><strong>4. Why hire Trademarkers?<br></strong><br>Our Trademark Professionals are adept in the handling of domestic and international trademark oppositions and related trademark administrative proceedings. Our understanding of the specific procedures in each venue puts us in an indestructible position to help protect and preserve your intellectual property rights.</p></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
             <tr style="border-collapse:collapse;"> 
              <td align="left" style="padding:0;Margin:0;padding-left:20px;padding-right:20px;"> 
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td width="560" align="center" valign="top" style="padding:0;Margin:0;"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                     <td align="center" style="padding:30px 0;Margin:0;" mc:edit="block_9"><span class="es-button-border es-button-border-4" style="border-style:solid;border-color:#659C35;background:#1475A9;border-width:0px;display:block;border-radius:25px;width:auto;"><a href="https://trademarkers.com/EB-0D5C-F" class="es-button es-button-3" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#1475A9;border-width:10px 20px 10px 20px;display:block;background:#1475A9;border-radius:25px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;border-left-width:0px;border-right-width:0px;">Show Options</a></span></td>
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="padding:0;Margin:0;padding-bottom:5px;padding-left:20px;padding-right:20px;font-size:0;" mc:edit="block_10"> 
                       <table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td style="padding:0;Margin:0px 0px 0px 0px;border-bottom:1px solid #CCCCCC;background:none;height:1px;width:100%;margin:0px;"></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" style="padding:0;Margin:0;" mc:edit="block_11"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;text-align:center;">Watch as one of our licensed attorneys explains the&nbsp;concepts of Trademark Registration&nbsp;and many more! </p></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
             <tr style="border-collapse:collapse;"> 
              <td align="left" style="padding:0;Margin:0;padding-top:5px;padding-left:20px;padding-right:20px;"> 
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td width="560" align="center" valign="top" style="padding:0;Margin:0;"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="padding:0;Margin:0;" mc:edit="block_12"><a target="_blank" href="https://www.youtube.com/watch?v=XiiI_l0CfF8&t=2s" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:16px;text-decoration:underline;color:#2CB543;"><img class="adapt-img" src="https://fcttwq.stripocdn.email/content/guids/videoImgGuid/images/57401586847573034.png" alt="Why Should You Register a Trademark? - TradeMarkers®" width="560" title="Why Should You Register a Trademark? - TradeMarkers®" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></a></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="padding:0;Margin:0;padding-left:20px;padding-right:20px;font-size:0;" mc:edit="block_13"> 
                       <table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td style="padding:0;Margin:0px 0px 0px 0px;border-bottom:1px solid #CCCCCC;background:none;height:1px;width:100%;margin:0px;"></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" style="padding:0;Margin:0;" mc:edit="block_14"><h1 style="Margin:0;line-height:24px;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:20px;font-style:normal;font-weight:bold;color:#1475A9;text-align:center;"><br>OUR SERVICES AVAILABLE</h1><br></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
             <tr style="border-collapse:collapse;"> 
              <td align="left" style="padding:0;Margin:0;padding-left:20px;padding-right:20px;"> 
               <!--[if mso]><table width="560" cellpadding="0" cellspacing="0"><tr><td width="270" valign="top"><![endif]--> 
               <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td width="270" class="es-m-p20b" align="left" style="padding:0;Margin:0;"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="padding:10px;Margin:0;" mc:edit="block_15"><span class="es-button-border es-button-border-4" style="border-style:solid;border-color:#659C35;background:#1475A9;border-width:0px;display:block;border-radius:25px;width:auto;"><a href="https://trademarkers.com/EC-0D1A-D" class="es-button es-button-3" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#1475A9;border-width:10px 20px 10px 20px;display:block;background:#1475A9;border-radius:25px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;border-left-width:0px;border-right-width:0px;">Trademark Registration</a></span></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table> 
               <!--[if mso]></td><td width="20"></td><td width="270" valign="top"><![endif]--> 
               <table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td width="270" align="left" style="padding:0;Margin:0;"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="padding:10px;Margin:0;" mc:edit="block_16"><span class="es-button-border es-button-border-4" style="border-style:solid;border-color:#659C35;background:#1475A9;border-width:0px;display:block;border-radius:25px;width:auto;"><a href="https://trademarkers.com/{{ $dataUrl['afr'] }}" class="es-button es-button-3" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#1475A9;border-width:10px 20px 10px 20px;display:block;background:#1475A9;border-radius:25px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;border-left-width:0px;border-right-width:0px;">Appeal for Final Refusal</a></span></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table> 
               <!--[if mso]></td></tr></table><![endif]--></td> 
             </tr> 
             <tr style="border-collapse:collapse;"> 
              <td align="left" style="padding:0;Margin:0;padding-left:20px;padding-right:20px;"> 
               <!--[if mso]><table width="560" cellpadding="0" cellspacing="0"><tr><td width="270" valign="top"><![endif]--> 
               <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td width="270" class="es-m-p20b" align="left" style="padding:0;Margin:0;"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="padding:10px;Margin:0;" mc:edit="block_17"><span class="es-button-border es-button-border-4" style="border-style:solid;border-color:#659C35;background:#1475A9;border-width:0px;display:block;border-radius:25px;width:auto;"><a href="https://trademarkers.com/CA-0B1B-M" class="es-button es-button-3" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#1475A9;border-width:10px 20px 10px 20px;display:block;background:#1475A9;border-radius:25px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;border-left-width:0px;border-right-width:0px;">Trademark Monitoring</a></span></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="padding:10px;Margin:0;" mc:edit="block_18"><span class="es-button-border es-button-border-4" style="border-style:solid;border-color:#659C35;background:#1475A9;border-width:0px;display:block;border-radius:25px;width:auto;"><a href="https://trademarkers.com/{{ $dataUrl['ita'] }}" class="es-button es-button-3" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#1475A9;border-width:10px 20px 10px 20px;display:block;background:#1475A9;border-radius:25px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;border-left-width:0px;border-right-width:0px;">International Application</a></span></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table> 
               <!--[if mso]></td><td width="20"></td><td width="270" valign="top"><![endif]--> 
               <table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td width="270" align="left" style="padding:0;Margin:0;"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="padding:10px;Margin:0;" mc:edit="block_19"><span class="es-button-border es-button-border-4" style="border-style:solid;border-color:#659C35;background:#1475A9;border-width:0px;display:block;border-radius:25px;width:auto;"><a href="https://trademarkers.com/{{ $dataUrl['afr'] }}" class="es-button es-button-3" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#1475A9;border-width:10px 20px 10px 20px;display:block;background:#1475A9;border-radius:25px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;border-left-width:0px;border-right-width:0px;">Trademark Renewal</a></span></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="padding:10px;Margin:0;" mc:edit="block_20"><span class="es-button-border es-button-border-4" style="border-style:solid;border-color:#659C35;background:#1475A9;border-width:0px;display:block;border-radius:25px;width:auto;"><a href="https://trademarkers.com/{{ $dataUrl['oar'] }}" class="es-button es-button-3" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#1475A9;border-width:10px 20px 10px 20px;display:block;background:#1475A9;border-radius:25px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;border-left-width:0px;border-right-width:0px;">Office Action Response</a></span></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table> 
               <!--[if mso]></td></tr></table><![endif]--></td> 
             </tr> 
           </table></td> 
         </tr> 
       </table> 
       

@endsection