@extends('layouts.event-mail-master', ['event' => $event, 'email' => $email, 'title' => 'Trademark Opposition' ])

@section('content')

<!-- body -->
        
    <table cellpadding="0" cellspacing="0" class="es-content" align="center" mc:repeatable style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;">
      <tr style="border-collapse:collapse;">
        <td align="center" bgcolor="#ffffff" style="padding:0;margin:0;background-color:#FFFFFF;">
          <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" width="600" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;">
            <tr style="border-collapse:collapse;">
              <td align="left" style="padding:0;margin:0;background-position:center top;">
                <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                  <tr style="border-collapse:collapse;">
                    <td width="600" align="center" valign="top" style="padding:0;margin:0;">
                      <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                        <tr style="border-collapse:collapse;">
                          <td align="center" style="padding:0;margin:0;font-size:0px;" mc:edit="block_4"><a target="_blank" href="https://trademarkers.com/BB-0F2B-R" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:none;color:#659C35;"><img class="adapt-img" src="https://fcttwq.stripocdn.email/content/guids/CABINET_f668200b601d8999550434f90402a561/images/3131590134335439.jpeg" alt="" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;" width="280"></a>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <table cellpadding="0" cellspacing="0" class="es-content" align="center" mc:repeatable style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;">
    <tr style="border-collapse:collapse;">
      <td align="center" bgcolor="#ffffff" style="padding:0;margin:0;background-color:#FFFFFF;">
        <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" width="600" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;">
          <tr style="border-collapse:collapse;">
            <td align="left" style="padding:0;margin:0;padding-left:20px;padding-right:20px;">
              <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                <tr style="border-collapse:collapse;">
                  <td width="560" align="center" valign="top" style="padding:0;margin:0;">
                    <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                      <tr style="border-collapse:collapse;">
                        <td align="center" style="padding:0;margin:0;" mc:edit="block_5">
                          <h2 style="margin:0;line-height:31px;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:26px;font-style:normal;font-weight:bold;color:#0B5394;">OFFICE ACTION OFFICIAL LETTER</h2>
                        </td>
                      </tr>
                      <tr style="border-collapse:collapse;">
                        <td align="center" style="padding:0;margin:0;padding-top:10px;" mc:edit="block_6">
                          <p style="margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:12px;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;line-height:20px;color:#333333;"><strong>Don't give up on your trademark. Our Response Packages will get it back on the right track.</strong>
                        </p>
                      </td>
                    </tr>
                    <tr style="border-collapse:collapse;">
                      <td align="center" style="margin:0;padding-top:5px;padding-bottom:5px;padding-left:20px;padding-right:20px;font-size:0;" mc:edit="block_7">
                        <table border="0" width="100%" cellpadding="0" cellspacing="0" role="presentation" style="height:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                          <tr style="border-collapse:collapse;">
                            <td style="padding:0;margin:0px;border-bottom:1px solid #CCCCCC;background:none;height:1px;width:100%;"></td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table cellpadding="0" cellspacing="0" class="es-content" align="center" mc:repeatable style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;">
  <tr style="border-collapse:collapse;">
    <td align="center" bgcolor="#ffffff" style="padding:0;margin:0;background-color:#FFFFFF;">
      <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" width="600" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;">
        <tr style="border-collapse:collapse;">
          <td align="left" style="padding:0;margin:0;padding-left:20px;padding-right:20px;">
            <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
              <tr style="border-collapse:collapse;">
                <td width="560" align="center" valign="top" style="padding:0;margin:0;">
                  <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                    <tr style="border-collapse:collapse;">
                      <td align="left" style="padding:0;margin:0;" mc:edit="block_8">
                        <h2 style="margin:0;line-height:31px;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:26px;font-style:normal;font-weight:bold;color:#0B5394;">Dear {{ ucfirst($event->caseOwned->tm_holder) }},</h2>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr style="border-collapse:collapse;">
          <td align="left" style="padding:0;margin:0;padding-top:5px;padding-left:20px;padding-right:20px;">
            <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
              <tr style="border-collapse:collapse;">
                <td width="560" align="center" valign="top" style="padding:0;margin:0;">
                  <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                    <tr style="border-collapse:collapse;">
                      <td align="left" style="padding:0;margin:0;" mc:edit="block_9">
                        <p style="margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:15px;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;line-height:23px;color:#222222;">
                          <br style="font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;">
                        </p>
                        <p style="margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:15px;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;line-height:23px;color:#222222;">We understand it was frustrating to learn that the U.S. Patent and Trademark Office (USPTO) refused your application to register the trademark <strong>{{ $event->caseOwned->trademark }}</strong>.</p>
                        <div style="font-size:15px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;">
                          <br>
                        </div>
                        <p style="margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:15px;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;line-height:23px;color:#333333;">Don’t be discouraged. Many trademarks achieve U.S. registration after facing an initial refusal. The Office Action letter you received probably seems to be written in confusing legal language and to use lots of trademark jargon.<br>
                          <br>
                        </p>
                        <p style="margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:15px;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;line-height:23px;color:#333333;">But our experienced trademark lawyers see refusals with this language all the time, and they may be able to help you overcome this obstacle to registering your mark.<br>
                          <br>
                        </p>
                        <p style="margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:15px;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;line-height:23px;color:#333333;">The Office Action is just the USPTO’s initial reaction to the data entered on your application. It explains in detail why the USPTO Examiner decided not to automatically approve your proposed trademark for registration. But it’s not the final word, and it gives you an opportunity to present arguments as to why your mark should be registered.<br>
                          <br>
                        </p>
                        <p style="margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:15px;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;line-height:23px;color:#333333;">TradeMarkers LLC has a team of lawyers with years of experience in trademark registrations. Our lawyers can evaluate your Office Action and let you know whether they think they can overcome it. If the outlook for success seems positive and you agree to move forward, they will research the issues the Examiner raised and prepare customized legal arguments aimed at convincing the PTO to register your mark.</p>
                      </td>
                    </tr>
                    <tr style="border-collapse:collapse;">
                      <td align="center" style="padding:10px;margin:0;" mc:edit="block_10"><span class="es-button-border es-button-border-6" style="border-style:solid;border-color:#659C35;background:#1475A9;border-width:0px;display:block;border-radius:25px;width:auto;"><a href="https://trademarkers.com/{{ $dataUrl['oar'] }}" class="es-button es-button-5" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:16px;color:#FFFFFF;border-style:solid;border-color:#1475A9;border-width:10px 20px 10px 20px;display:block;background:#1475A9;border-radius:25px;font-weight:normal;font-style:normal;line-height:19px;width:auto;text-align:center;border-left-width:20px;border-right-width:20px;">EVALUATE MY US TRADEMARK OFFICE ACTION FOR FREE!</a></span>
                    </td>
                  </tr>
                  <tr style="border-collapse:collapse;">
                    <td align="left" style="padding:0;margin:0;" mc:edit="block_11">
                      <p style="margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:15px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:23px;color:#333333;">Although you could try to respond without an attorney, our trademark attorneys know how to conduct legal research into the specific issues raised in your Office Action and where to find prior USPTO decisions and court cases that support registration of your mark.<br>
                        <br>In addition, our experienced trademark lawyers will present legal arguments similar to responses that succeeded in the past. Our team already has the registered account necessary to file your response on the USPTO website -- and years of experience navigating the complicated electronic filing system.<br>
                        <br>As stated in the Office Action, you have six months from the date it was issued to respond, however we advise you not to wait to get help. Analyzing the reasons for the refusal takes time, as does conducting the legal research, preparing the response, and filing it electronically.<br>
                        <br>Plus, the sooner your response is filed, the sooner your application can start moving again if the USPTO agrees to approve it. Remember, time is of the essence. Act fast. Our TradeMarkers team is standing by to help your mark achieve registration.<br>
                      </p>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr style="border-collapse:collapse;">
        <td align="left" style="padding:0;margin:0;padding-left:20px;padding-right:20px;">
          <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
            <tr style="border-collapse:collapse;">
              <td width="560" align="center" valign="top" style="padding:0;margin:0;">
                <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;border-top:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC;background-position:center top;" role="presentation">
                  <tr style="border-collapse:collapse;">
                    <td align="center" style="padding:0;margin:0;" mc:edit="block_12">
                      <p style="margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;font-weight:600">
                        <br>Sometimes your trademark would receive an initial refusal. So what should you do next? How do you respond to such refusal? Would you need an attorney in order to resolve such an issue? Find out more on this video.<br>
                        <br>
                      </p>
                    </td>
                  </tr>
                  <tr style="border-collapse:collapse;">
                    <td align="center" style="padding:0;margin:0;" mc:edit="block_13"><a target="_blank" href="https://www.youtube.com/watch?v=NIw8C5HoehM&amp;t=92s" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:none;color:#659C35;"><img class="adapt-img" src="https://fcttwq.stripocdn.email/content/guids/videoImgGuid/images/50391585202105725.png" alt="Best Ways of Responding to a Trademark Refusal | TradeMarkers®" width="560" title="Best Ways of Responding to a Trademark Refusal | TradeMarkers®" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></a>
                  </td>
                </tr>
                <tr style="border-collapse:collapse;">
                  <td align="left" bgcolor="#ffffff" style="padding:0;margin:0;" mc:edit="block_14">
                    <h3 style="margin:0;line-height:26px;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:22px;font-style:normal;font-weight:normal;color:#0B5394;text-align:center;">
                      <br><strong>OTHER SERVICES</strong>
                    </h3>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr style="border-collapse:collapse;">
      <td align="left" bgcolor="#ffffff" style="padding:0;margin:0;padding-left:20px;padding-right:20px;background-color:#FFFFFF;">
        <!--[if mso]>
        <table width="560" cellpadding="0" cellspacing="0">
          <tr>
            <td width="270" valign="top">
              <![endif]-->
        <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;">
          <tr style="border-collapse:collapse;">
            <td width="270" class="es-m-p20b" align="left" style="padding:0;margin:0;">
              <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                <tr style="border-collapse:collapse;">
                  <td align="center" style="margin:0;padding-top:10px;padding-bottom:10px;padding-left:10px;padding-right:10px;" mc:edit="block_15"><span class="es-button-border es-button-border-4" style="border-style:solid;border-color:#659C35;background:#0B5394;border-width:0px;display:block;border-radius:25px;width:auto;"><a href="https://trademarkers.com/registration/step2/US?action=setFields&case={{$event->caseOwned->id}}" class="es-button es-button-3" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#0B5394;border-width:10px 20px 10px 20px;display:block;background:#0B5394;border-radius:25px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;border-left-width:0px;border-right-width:0px;">Trademark Application</a></span>
                </td>
              </tr>
              <tr style="border-collapse:collapse;">
                <td align="center" style="padding:10px;margin:0;" mc:edit="block_16"><span class="es-button-border es-button-border-4" style="border-style:solid;border-color:#659C35;background:#0B5394;border-width:0px;display:block;border-radius:25px;width:auto;"><a href="https://trademarkers.com/{{ $dataUrl['oar'] }}" class="es-button es-button-3" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#0B5394;border-width:10px 20px 10px 20px;display:block;background:#0B5394;border-radius:25px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;border-left-width:0px;border-right-width:0px;">Office Action Response</a></span>
              </td>
            </tr>
            <tr style="border-collapse:collapse;">
              <td align="center" style="padding:10px;margin:0;" mc:edit="block_17"><span class="es-button-border es-button-border-4" style="border-style:solid;border-color:#659C35;background:#0B5394;border-width:0px;display:block;border-radius:25px;width:auto;"><a href="https://trademarkers.com/{{ $dataUrl['afr'] }}" class="es-button es-button-3" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#0B5394;border-width:10px 20px 10px 20px;display:block;background:#0B5394;border-radius:25px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;border-left-width:0px;border-right-width:0px;">Trademark Renewal</a></span>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <!--[if mso]>
</td>
<td width="20">
</td>
<td width="270" valign="top">
  <![endif]-->
  <table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right;">
    <tr style="border-collapse:collapse;">
      <td width="270" align="left" style="padding:0;margin:0;">
        <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
          <tr style="border-collapse:collapse;">
            <td align="center" style="padding:10px;margin:0;" mc:edit="block_18"><span class="es-button-border es-button-border-4" style="border-style:solid;border-color:#659C35;background:#0B5394;border-width:0px;display:block;border-radius:25px;width:auto;"><a href="https://trademarkers.com/DB-0H0A-P" class="es-button es-button-3" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#0B5394;border-width:10px 20px 10px 20px;display:block;background:#0B5394;border-radius:25px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;border-left-width:0px;border-right-width:0px;">Trademark Study</a></span>
          </td>
        </tr>
        <tr style="border-collapse:collapse;">
          <td align="center" style="padding:10px;margin:0;" mc:edit="block_19"><span class="es-button-border es-button-border-4" style="border-style:solid;border-color:#659C35;background:#0B5394;border-width:0px;display:block;border-radius:25px;width:auto;"><a href="https://trademarkers.com/{{ $dataUrl['ita'] }}" class="es-button es-button-3" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#0B5394;border-width:10px 20px 10px 20px;display:block;background:#0B5394;border-radius:25px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;border-left-width:0px;border-right-width:0px;">International Application</a></span>
        </td>
      </tr>
      <tr style="border-collapse:collapse;">
        <td align="center" style="padding:10px;margin:0;" mc:edit="block_20"><span class="es-button-border es-button-border-4" style="border-style:solid;border-color:#659C35;background:#0B5394;border-width:0px;display:block;border-radius:25px;width:auto;"><a href="https://trademarkers.com/CA-0B1B-M" class="es-button es-button-3" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#0B5394;border-width:10px 20px 10px 20px;display:block;background:#0B5394;border-radius:25px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;border-left-width:0px;border-right-width:0px;">Trademark Monitoring</a></span>
      </td>
    </tr>
  </table>
</td>
</tr>
</table>
       

@endsection