<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
	<head>

		<meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
		<style>
			body{
				font: small/1.5 Arial,Helvetica,sans-serif;
			}
			p {
				margin-bottom: 15px;
				line-height: 20px;
				text-align: justify;
			}
		</style>
    </head>
    <body>
	<p>Hi Admin,</p>
	<p></p>
	<p></p>

	@php
	$trdType = '';
		switch($data['quoteType']){
			case 'oar' : $trdType = 'Office Action Response'; break;
			case 'afr' : $trdType = 'Appeal a Final Refusal'; break;
			case 'sou' : $trdType = 'Trademark Statement of Use'; break;
			case 'raa' : $trdType = 'Revive an Abandoned Application'; break;
			case 'ita' : $trdType = 'International Trademark Application'; break;
			case 'tr' : $trdType = 'Trademark Renewal'; break;
			case 'cut' : $trdType = 'Change or Update Trademark Owner'; break;
			case 'tm' : $trdType = 'Trademark Monitoring'; break;
			case 'lop' : $trdType = 'Letter of Protest'; break;
			case 'eyt' : $trdType = 'Enforce Your Trademark';break;
			case 'nas' : $trdType = 'Negotiate a Settlement'; break;
			case 'dsa' : $trdType = 'Draft a Settlement Agreement'; break;
			case 'tmo' : $trdType = 'Trademark Opposition'; break;
			case 'cad' : $trdType = 'Cease and Desist'; break;
		}	
	$countryDesignate = '';

	if ($data['countryDesignate'] && count($data['countryDesignate']) > 0 ) {
		$countryDesignate = implode(',' , $data['countryDesignate']);
	}
	
	@endphp

	<p>Below is the quote request details</p>
	<br>
	<p>Name : {{ $data['name'] }}</p>
	<p>Email : {{ $data['email'] }}</p>
	<p>phone : {{ $data['phone'] }}</p>

	<p>Request Type : {{ $trdType }}</p>

	@if ( $trdType != 'tmo' )

		@if ($data['trademarkName']) <p>Trademark Name : {{ $data['trademarkName'] }}</p> @endif
		@if ($data['trademarkNumber']) <p>Trademark Number : {{ $data['trademarkNumber'] }}</p> @endif
		@if ($data['trademarkNumber']) <p>Trademark Class(es) : {{ $data['trademarkClass'] }}</p> @endif
		@if ($data['trademarkFillingDate']) <p>Filling Date : {{ $data['trademarkFillingDate'] }}</p> @endif
		@if ($data['trademarkType']) <p>Trademark Type : {{ $data['trademarkType'] }}</p> @endif
		@if ($data['trademarkCountry']) <p>Country to File : {{ $data['trademarkCountry'] }}</p> @endif
		@if ($countryDesignate) <p>Countries to Designate : {{ $countryDesignate }}</p> @endif
		@if ($data['additionalDetails']) <p>Additional Details : {{ $data['additionalDetails'] }}</p> @endif

		@if ($data['additionalDetails']) <p>Opposing Trademark Name : {{ $data['opposingTrademarkName'] }}</p> @endif
		@if ($data['additionalDetails']) <p>Opposing Trademark Number : {{ $data['opposingTrademarkNumber'] }}</p> @endif
		@if ($data['additionalDetails']) <p>Opposing Trademark Office : {{ $data['opposingTrademarkOffice'] }}</p> @endif
	@else 

		@if ($data['trademarkName']) <p>Opposing Trademark Name : {{ $data['trademarkName'] }}</p> @endif
		@if ($data['trademarkNumber']) <p>Opposing Trademark Number : {{ $data['trademarkNumber'] }}</p> @endif
		@if ($data['trademarkNumber']) <p>Opposing Trademark Class(es) : {{ $data['trademarkClass'] }}</p> @endif
		@if ($data['trademarkFillingDate']) <p>Opposing Filling Date : {{ $data['trademarkFillingDate'] }}</p> @endif
		@if ($data['trademarkType']) <p>Opposing Trademark Type : {{ $data['trademarkType'] }}</p> @endif
		@if ($data['trademarkCountry']) <p>Country to File : {{ $data['trademarkCountry'] }}</p> @endif
		@if ($countryDesignate) <p>Countries to Designate : {{ $countryDesignate }}</p> @endif
		@if ($data['additionalDetails']) <p>Additional Details : {{ $data['additionalDetails'] }}</p> @endif

		@if ($data['additionalDetails']) <p>Defending Trademark Name : {{ $data['opposingTrademarkName'] }}</p> @endif
		@if ($data['additionalDetails']) <p>Defending Trademark Number : {{ $data['opposingTrademarkNumber'] }}</p> @endif
		@if ($data['additionalDetails']) <p>Defending Trademark Office : {{ $data['opposingTrademarkOffice'] }}</p> @endif

	@endif

	<br>
	<p>Best regards,</p>
	
	<p>Trademarkers, LLC</p>
    </body>
</html>