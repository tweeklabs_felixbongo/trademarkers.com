<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
	<head>

		<meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
		<style>
			body{
				font: small/1.5 Arial,Helvetica,sans-serif;
			}
			p {
				margin-bottom: 15px;
				line-height: 20px;
				text-align: justify;
			}
		</style>
    </head>
    <body>
	<p>Hi Admin,</p>
	<p></p>
	<p>Payment Request: </p>
	
	<p>Name: {{ $user->name }}</p>
	<p>Payment Method: {{ $request->get('payment_method') }}</p>
	<p>Amount: {{ $request->get('req_amount') }}</p>
	<p>Information: {{ $request->get('information') }}</p>

	<br>
	<p>Best regards,</p>
	
	<p>Trademarkers, LLC</p>
    </body>
</html>