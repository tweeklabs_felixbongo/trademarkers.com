<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html style="width:100%;font-family:arial, 'helvetica neue', helvetica, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;">
 <head> 
  <meta charset="UTF-8"> 
  <meta content="width=device-width, initial-scale=1" name="viewport"> 
  <meta name="x-apple-disable-message-reformatting"> 
  <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
  <meta content="telephone=no" name="format-detection"> 
  <title>1 day old Abandoned Cart Template</title> 
  <!--[if (mso 16)]>
    <style type="text/css">
    a {text-decoration: none;}
    </style>
    <![endif]--> 
  <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]--> 
   
 <style type="text/css">
	@media only screen and (max-width:600px){
		p,ul li,ol li,a{
			font-size:14px!important;
			line-height:150%!important;
		}

}	@media only screen and (max-width:600px){
		h1{
			font-size:30px!important;
			text-align:center;
			line-height:120%!important;
		}

}	@media only screen and (max-width:600px){
		h2{
			font-size:22px!important;
			text-align:center;
			line-height:120%!important;
		}

}	@media only screen and (max-width:600px){
		h3{
			font-size:20px!important;
			text-align:center;
			line-height:120%!important;
		}

}	@media only screen and (max-width:600px){
		h1 a{
			font-size:30px!important;
		}

}	@media only screen and (max-width:600px){
		h2 a{
			font-size:22px!important;
		}

}	@media only screen and (max-width:600px){
		h3 a{
			font-size:20px!important;
		}

}	@media only screen and (max-width:600px){
		.es-menu td a{
			font-size:16px!important;
		}

}	@media only screen and (max-width:600px){
		.es-header-body p,.es-header-body ul li,.es-header-body ol li,.es-header-body a{
			font-size:16px!important;
		}

}	@media only screen and (max-width:600px){
		.es-footer-body p,.es-footer-body ul li,.es-footer-body ol li,.es-footer-body a{
			font-size:14px!important;
		}

}	@media only screen and (max-width:600px){
		.es-infoblock p,.es-infoblock ul li,.es-infoblock ol li,.es-infoblock a{
			font-size:12px!important;
		}

}	@media only screen and (max-width:600px){
		*[class=gmail-fix]{
			display:none!important;
		}

}	@media only screen and (max-width:600px){
		.es-m-txt-c,.es-m-txt-c h1,.es-m-txt-c h2,.es-m-txt-c h3{
			text-align:center!important;
		}

}	@media only screen and (max-width:600px){
		.es-m-txt-r,.es-m-txt-r h1,.es-m-txt-r h2,.es-m-txt-r h3{
			text-align:right!important;
		}

}	@media only screen and (max-width:600px){
		.es-m-txt-l,.es-m-txt-l h1,.es-m-txt-l h2,.es-m-txt-l h3{
			text-align:left!important;
		}

}	@media only screen and (max-width:600px){
		.es-m-txt-r img,.es-m-txt-c img,.es-m-txt-l img{
			display:inline!important;
		}

}	@media only screen and (max-width:600px){
		.es-button-border{
			display:block!important;
		}

}	@media only screen and (max-width:600px){
		a.es-button{
			font-size:20px!important;
			display:block!important;
			border-left-width:0px!important;
			border-right-width:0px!important;
		}

}	@media only screen and (max-width:600px){
		.es-btn-fw{
			border-width:10px 0px!important;
			text-align:center!important;
		}

}	@media only screen and (max-width:600px){
		.es-adaptive table,.es-btn-fw,.es-btn-fw-brdr,.es-left,.es-right{
			width:100%!important;
		}

}	@media only screen and (max-width:600px){
		.es-content table,.es-header table,.es-footer table,.es-content,.es-footer,.es-header{
			width:100%!important;
			max-width:600px!important;
		}

}	@media only screen and (max-width:600px){
		.es-adapt-td{
			display:block!important;
			width:100%!important;
		}

}	@media only screen and (max-width:600px){
		.adapt-img{
			width:100%!important;
			height:auto!important;
		}

}	@media only screen and (max-width:600px){
		.es-m-p0{
			padding:0px!important;
		}

}	@media only screen and (max-width:600px){
		.es-m-p0r{
			padding-right:0px!important;
		}

}	@media only screen and (max-width:600px){
		.es-m-p0l{
			padding-left:0px!important;
		}

}	@media only screen and (max-width:600px){
		.es-m-p0t{
			padding-top:0px!important;
		}

}	@media only screen and (max-width:600px){
		.es-m-p0b{
			padding-bottom:0!important;
		}

}	@media only screen and (max-width:600px){
		.es-m-p20b{
			padding-bottom:20px!important;
		}

}	@media only screen and (max-width:600px){
		.es-mobile-hidden,.es-hidden{
			display:none!important;
		}

}	@media only screen and (max-width:600px){
		.es-desk-hidden{
			display:table-row!important;
			width:auto!important;
			overflow:visible!important;
			float:none!important;
			max-height:inherit!important;
			line-height:inherit!important;
		}

}	@media only screen and (max-width:600px){
		.es-desk-menu-hidden{
			display:table-cell!important;
		}

}	@media only screen and (max-width:600px){
		table.es-table-not-adapt,.esd-block-html table{
			width:auto!important;
		}

}	@media only screen and (max-width:600px){
		table.es-social{
			display:inline-block!important;
		}

}	@media only screen and (max-width:600px){
		table.es-social td{
			display:inline-block!important;
		}

}		#outlook a{
			padding:0;
		}
		.ExternalClass{
			width:100%;
		}
		.ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{
			line-height:100%;
		}
		.es-button{
			mso-style-priority:100!important;
			text-decoration:none!important;
		}
		a[x-apple-data-detectors]{
			color:inherit!important;
			text-decoration:none!important;
			font-size:inherit!important;
			font-family:inherit!important;
			font-weight:inherit!important;
			line-height:inherit!important;
		}
		.es-desk-hidden{
			display:none;
			float:left;
			overflow:hidden;
			width:0;
			max-height:0;
			line-height:0;
			mso-hide:all;
		}
		.es-button-border:hover a.es-button{
			background:#7dbf44!important;
			border-color:#7dbf44!important;
		}
		.es-button-border:hover{
			background:#7dbf44!important;
			border-color:#7dbf44 #7dbf44 #7dbf44 #7dbf44!important;
		}
		td .es-button-border:hover a.es-button-1{
			background:#7dbf44!important;
			border-color:#7dbf44!important;
		}
		td .es-button-border-2:hover{
			background:#7dbf44!important;
		}
		td .es-button-border:hover a.es-button-3{
			background:#0e6ec2!important;
			border-color:#0e6ec2!important;
		}
		td .es-button-border-4:hover{
			background:#0e6ec2!important;
		}
</style></head> 
 <body style="width:100%;font-family:arial, 'helvetica neue', helvetica, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;"> 
  <div class="es-wrapper-color" style="background-color:#F6F6F6;"> 
   <!--[if gte mso 9]>
			<v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
				<v:fill type="tile" color="#f6f6f6"></v:fill>
			</v:background>
		<![endif]--> 
   <table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top;"> 
     <tr style="border-collapse:collapse;"> 
      <td valign="top" style="padding:0;Margin:0;"> 
       <table cellpadding="0" cellspacing="0" class="es-header" align="center" mc:repeatable style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top;"> 
         <tr style="border-collapse:collapse;"> 
          <td align="center" style="padding:0;Margin:0;"> 
           <table class="es-header-body" width="600" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;"> 
             <tr style="border-collapse:collapse;"> 
              <td align="left" style="Margin:0;padding-bottom:10px;padding-top:20px;padding-left:20px;padding-right:20px;"> 
               <!--[if mso]><table width="560" cellpadding="0" cellspacing="0"><tr><td width="270" valign="top"><![endif]--> 
               <table class="es-left" cellspacing="0" cellpadding="0" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td class="es-m-p20b" width="270" align="left" style="padding:0;Margin:0;"> 
                   <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" style="padding:0;Margin:0;padding-bottom:5px;font-size:0px;" mc:edit="block_0"><img src="https://fcttwq.stripocdn.email/content/guids/CABINET_f9d556123d9e4fb32e622ac29d3c1de0/images/18231583481137214.jpg" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;" class="adapt-img" width="265"></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table> 
               <!--[if mso]></td><td width="20"></td><td width="270" valign="top"><![endif]--> 
               <table class="es-right" cellspacing="0" cellpadding="0" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td width="270" align="left" style="padding:0;Margin:0;"> 
                   <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td mc:edit="block_1" style="padding:0;Margin:0;"> 
                       <table class="es-menu" width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                         <tr class="links" style="border-collapse:collapse;"> 
                          <td style="Margin:0;padding-left:5px;padding-right:5px;padding-top:25px;padding-bottom:10px;border:0;" width="33.33%" valign="top" bgcolor="transparent" align="center"><a target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:16px;text-decoration:none;display:block;color:#0B5394;" href="https://trademarkers.com/AA-0K2C-P">Registration</a></td> 
                          <td style="Margin:0;padding-left:5px;padding-right:5px;padding-top:25px;padding-bottom:10px;border:0;" width="33.33%" valign="top" bgcolor="transparent" align="center"><a target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:16px;text-decoration:none;display:block;color:#0B5394;" href="https://trademarkers.com/EB-0D5C-F">Services</a></td> 
                          <td style="Margin:0;padding-left:5px;padding-right:5px;padding-top:25px;padding-bottom:10px;border:0;" width="33.33%" valign="top" bgcolor="transparent" align="center"><a target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:16px;text-decoration:none;display:block;color:#0B5394;" href="https://trademarkers.com/EB-0D1C-K">About</a></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table> 
               <!--[if mso]></td></tr></table><![endif]--></td> 
             </tr> 
           </table></td> 
         </tr> 
       </table> 
       <table cellpadding="0" cellspacing="0" class="es-content" align="center" mc:repeatable style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;"> 
         <tr style="border-collapse:collapse;"> 
          <td align="center" style="padding:0;Margin:0;"> 
           <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" width="600" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;"> 
             <tr style="border-collapse:collapse;"> 
              <td align="left" style="padding:0;Margin:0;background-position:center top;"> 
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td width="600" align="center" valign="top" style="padding:0;Margin:0;"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="padding:0;Margin:0;position:relative;" mc:edit="block_2"><img class="adapt-img" src="https://fcttwq.stripocdn.email/content/guids/bannerImgGuid/images/47771583743357234.png" alt title width="100%" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
           </table></td> 
         </tr> 
       </table> 
       <table cellpadding="0" cellspacing="0" class="es-content" align="center" mc:repeatable style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;"> 
         <tr style="border-collapse:collapse;"> 
          <td align="center" style="padding:0;Margin:0;"> 
           <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" width="600" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;"> 
             <tr style="border-collapse:collapse;"> 
              <td align="left" style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px;background-position:center top;"> 
               <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td width="560" align="center" valign="top" style="padding:0;Margin:0;"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" mc:edit="block_3" style="padding:0;Margin:0;"><h2 style="Margin:0;line-height:31px;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:26px;font-style:normal;font-weight:bold;color:#0B5394;text-align:left;">Hello {{ $user->profiles[0]->first_name }},</h2></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" mc:edit="block_4" style="padding:0;Margin:0;padding-top:10px;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;">Just following up as you didn’t reply to my previous emails. Your pending items on your cart are valuable. Trademarks play an essential role in protecting consumers and in promoting global economic growth.<br><br><strong>You might want to take a look again why are trademarks important?</strong></p></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" mc:edit="block_5" style="padding:0;Margin:0;"> 
                       <ul> 
                        <li style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;Margin-bottom:15px;color:#333333;">Trademarks play an essential role in protecting consumers and in promoting global economic growth.</li> 
                        <li style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;Margin-bottom:15px;color:#333333;">Trademarks enable consumers to make quick, confident and safe purchasing decisions.</li> 
                        <li style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;Margin-bottom:15px;color:#333333;">Trademarks promote freedom of choice.</li> 
                        <li style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;Margin-bottom:15px;color:#333333;">Trademarks and related intellectual property encourage vibrant competition for the benefit of consumers, workers, brand owners and society at large.</li> 
                       </ul></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" mc:edit="block_6" style="padding:0;Margin:0;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;">One of the most important things to remember is that Trademark Registration is on a first-come, first-serve sort of affair. If you wait or delay registering a trademark and someone else comes along and registers it, you’re going to be out of the corner because that company or person will now own that trademark. Trademark is one of the important assets you or your company can have.</p></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" mc:edit="block_7" style="Margin:0;padding-left:10px;padding-right:10px;padding-top:20px;padding-bottom:20px;"><span class="es-button-border es-button-border-4" style="border-style:solid;border-color:#659C35;background:#0B5394;border-width:0px;display:block;border-radius:0px;width:auto;"><a href="https://trademarkers.com/abandon/{{$user->token}}" class="es-button es-button-3" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#0B5394;border-width:10px 0px;display:block;background:#0B5394;border-radius:0px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;">Don’t delay, Don’t wait…Protect and Innovate Instead!</a></span></td> 
                     </tr> 
                     <!-- <tr style="border-collapse:collapse;"> 
                      <td align="center" mc:edit="block_8" style="padding:0;Margin:0;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;">Act now before its too late. Click on the button below to grab your exclusive discount. Hurry, this is a limited time offer!</p></td> 
                     </tr>  -->
                     <!-- <tr style="border-collapse:collapse;"> 
                      <td align="center" mc:edit="block_9" style="padding:10px;Margin:0;"><span class="es-button-border es-button-border-4" style="border-style:solid;border-color:#659C35;background:#0B5394;border-width:0px;display:inline-block;border-radius:25px;width:auto;"><a href="" class="es-button es-button-3" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#0B5394;border-width:10px 20px 10px 20px;display:inline-block;background:#0B5394;border-radius:25px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;">Claim Your Discount Now!</a></span></td> 
                     </tr>  -->
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" mc:edit="block_10" style="padding:0;Margin:0;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;">Need further assistance? Call us at&nbsp;212-468-5500 or dial&nbsp;1-800-293-3166&nbsp;Toll-Free. You may also talk to one of our trademark consultants live at www.trademarkers.com.<br><br>Best Regards,<br><br><strong>Trademarkers Team</strong></p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;"><br></p></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" mc:edit="block_11" style="padding:0;Margin:0;"><a target="_blank" href="https://www.youtube.com/watch?v=XiiI_l0CfF8" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:none;color:#659C35;"><img class="adapt-img" src="https://fcttwq.stripocdn.email/content/guids/videoImgGuid/images/25271583743310085.png" alt="Why Should You Register a Trademark? - TradeMarkers®" width="560" title="Why Should You Register a Trademark? - TradeMarkers®" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></a></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
             <tr style="border-collapse:collapse;"> 
              <td align="left" style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px;"> 
               <!--[if mso]><table width="560" cellpadding="0" cellspacing="0"><tr><td width="270" valign="top"><![endif]--> 
               <table cellpadding="0" cellspacing="0" class="es-left" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td width="270" class="es-m-p20b" align="left" style="padding:0;Margin:0;"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" mc:edit="block_12" style="padding:10px;Margin:0;"><span class="es-button-border es-button-border-4" style="border-style:solid;border-color:#659C35;background:#0B5394;border-width:0px;display:block;border-radius:25px;width:auto;"><a href="https://trademarkers.com/abandon/{{$user->token}}" class="es-button es-button-3" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#0B5394;border-width:10px 20px 10px 20px;display:block;background:#0B5394;border-radius:25px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;border-left-width:0px;border-right-width:0px;">Log In to Your Account</a></span></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table> 
               <!--[if mso]></td><td width="20"></td><td width="270" valign="top"><![endif]--> 
               <table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td width="270" align="left" style="padding:0;Margin:0;"> 
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" mc:edit="block_13" style="padding:10px;Margin:0;"><span class="es-button-border es-button-border-4" style="border-style:solid;border-color:#659C35;background:#0B5394;border-width:0px;display:block;border-radius:25px;width:auto;"><a href="https://trademarkers.com/EB-0D5C-F" class="es-button es-button-3" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#0B5394;border-width:10px 20px 10px 20px;display:block;background:#0B5394;border-radius:25px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;border-left-width:0px;border-right-width:0px;">Explore Our Services</a></span></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table> 
               <!--[if mso]></td></tr></table><![endif]--></td> 
             </tr> 
           </table></td> 
         </tr> 
       </table> 
       <table cellpadding="0" cellspacing="0" class="es-content" align="center" mc:repeatable style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;"> 
         <tr style="border-collapse:collapse;"> 
          <td align="center" style="padding:0;Margin:0;"> 
           <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" width="600" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;"> 
             <tr style="border-collapse:collapse;"> 
              <td align="left" style="Margin:0;padding-left:20px;padding-right:20px;padding-top:30px;padding-bottom:30px;background-position:left top;"> 
               <!--[if mso]><table width="560" cellpadding="0" cellspacing="0"><tr><td width="270" valign="top"><![endif]--> 
               <table class="es-left" cellspacing="0" cellpadding="0" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td class="es-m-p20b" width="270" align="left" style="padding:0;Margin:0;"> 
                   <table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-position:center center;" role="presentation"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" mc:edit="block_14" style="padding:0;Margin:0;"><h4 style="Margin:0;line-height:120%;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;color:#0B5394;">Contact Us:</h4></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" mc:edit="block_15" style="padding:0;Margin:0;padding-top:10px;padding-bottom:15px;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;">We're always here for you. If you want to talk through your options, remember we have people who really want to help you out. Our award-winning, support always has your back.</p></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td style="padding:0;Margin:0;"> 
                       <table class="es-table-not-adapt" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td valign="top" align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px;padding-right:10px;font-size:0;" mc:edit="block_16"><img src="https://fcttwq.stripocdn.email/content/guids/CABINET_45fbd8c6c971a605c8e5debe242aebf1/images/30981556869899567.png" alt width="16" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></td> 
                          <td align="left" style="padding:0;Margin:0;"> 
                           <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="left" mc:edit="block_17" style="padding:0;Margin:0;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;"><a target="_blank" href="mailto:help@mail.com" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:none;color:#333333;">info@trademarkers.com</a></p></td> 
                             </tr> 
                           </table></td> 
                         </tr> 
                         <tr style="border-collapse:collapse;"> 
                          <td valign="top" align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px;padding-right:10px;font-size:0;" mc:edit="block_18"><img src="https://fcttwq.stripocdn.email/content/guids/CABINET_45fbd8c6c971a605c8e5debe242aebf1/images/58031556869792224.png" alt width="16" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></td> 
                          <td align="left" style="padding:0;Margin:0;"> 
                           <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="left" mc:edit="block_19" style="padding:0;Margin:0;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;"><a target="_blank" href="tel:+14155555553" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:none;color:#333333;">+1 212 468 5500</a></p></td> 
                             </tr> 
                           </table></td> 
                         </tr> 
                         <tr style="border-collapse:collapse;"> 
                          <td valign="top" align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px;padding-right:10px;font-size:0;" mc:edit="block_20"><img src="https://fcttwq.stripocdn.email/content/guids/CABINET_45fbd8c6c971a605c8e5debe242aebf1/images/78111556870146007.png" alt width="16" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></td> 
                          <td align="left" style="padding:0;Margin:0;"> 
                           <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="left" mc:edit="block_21" style="padding:0;Margin:0;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;">Millburn, New Jersey</p></td> 
                             </tr> 
                           </table></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" mc:edit="block_22" style="padding:0;Margin:0;padding-top:15px;"><span class="es-button-border es-button-border-4" style="border-style:solid;border-color:#659C35;background:#0B5394;border-width:0px;display:inline-block;border-radius:25px;width:auto;"><a href="https://trademarkers.com/FA-0K0B-P" class="es-button es-button-3" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#0B5394;border-width:10px 20px 10px 20px;display:inline-block;background:#0B5394;border-radius:25px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;">We're Live</a></span></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table> 
               <!--[if mso]></td><td width="20"></td><td width="270" valign="top"><![endif]--> 
               <table class="es-right" cellspacing="0" cellpadding="0" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td width="270" align="left" style="padding:0;Margin:0;"> 
                   <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="padding:0;Margin:0;font-size:0;" mc:edit="block_23"><img class="adapt-img" src="https://fcttwq.stripocdn.email/content/guids/CABINET_45fbd8c6c971a605c8e5debe242aebf1/images/52821556874243897.jpg" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;" width="270"></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table> 
               <!--[if mso]></td></tr></table><![endif]--></td> 
             </tr> 
           </table></td> 
         </tr> 
       </table> 
       <table cellpadding="0" cellspacing="0" class="es-footer" align="center" mc:repeatable style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top;"> 
         <tr style="border-collapse:collapse;"> 
          <td align="center" style="padding:0;Margin:0;"> 
           <table class="es-footer-body" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#333333;" width="600" cellspacing="0" cellpadding="0" bgcolor="#333333" align="center"> 
             <tr style="border-collapse:collapse;"> 
              <td style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px;background-color:#0B5394;" bgcolor="#0b5394" align="left"> 
               <table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td width="560" valign="top" align="center" style="padding:0;Margin:0;"> 
                   <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td mc:edit="block_24" style="padding:0;Margin:0;"> 
                       <table class="es-menu" width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                         <tr class="links" style="border-collapse:collapse;"> 
                          <td style="Margin:0;padding-left:5px;padding-right:5px;padding-top:0px;padding-bottom:0px;border:0;" width="33.33%" valign="top" bgcolor="transparent" align="center"><a target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:none;display:block;color:#FFFFFF;" href="https://trademarkers.com/AA-0K2B-P">Pricing</a></td> 
                          <td style="Margin:0;padding-left:5px;padding-right:5px;padding-top:0px;padding-bottom:0px;border:0;border-left:1px solid #FFFFFF;" width="33.33%" valign="top" bgcolor="transparent" align="center"><a target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:none;display:block;color:#FFFFFF;" href="https://trademarkers.com/FA-0K6A-T">Resources</a></td> 
                          <td style="Margin:0;padding-left:5px;padding-right:5px;padding-top:0px;padding-bottom:0px;border:0;border-left:1px solid #FFFFFF;" width="33.33%" valign="top" bgcolor="transparent" align="center"><a target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:none;display:block;color:#FFFFFF;" href="https://trademarkers.com/FA-0K0B-P">Contact Us</a></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
             <tr style="border-collapse:collapse;"> 
              <td style="Margin:0;padding-bottom:15px;padding-top:20px;padding-left:20px;padding-right:20px;background-color:#0B5394;" bgcolor="#0b5394" align="left"> 
               <table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td width="560" valign="top" align="center" style="padding:0;Margin:0;"> 
                   <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="padding:0;Margin:0;padding-bottom:15px;font-size:0;" mc:edit="block_25"> 
                       <table class="es-table-not-adapt es-social" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td valign="top" align="center" style="padding:0;Margin:0;padding-right:15px;"><a target="_blank" href="https://www.facebook.com/TrademarkersLLC" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:none;color:#FFFFFF;"><img title="Facebook" src="https://fcttwq.stripocdn.email/content/assets/img/social-icons/circle-white/facebook-circle-white.png" alt="Fb" width="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></a></td> 
                          <td valign="top" align="center" style="padding:0;Margin:0;padding-right:15px;"><a target="_blank" href="https://twitter.com/trademarkersllc" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:none;color:#FFFFFF;"><img title="Twitter" src="https://fcttwq.stripocdn.email/content/assets/img/social-icons/circle-white/twitter-circle-white.png" alt="Tw" width="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></a></td> 
                          <td valign="top" align="center" style="padding:0;Margin:0;padding-right:15px;"><a target="_blank" href="https://www.youtube.com/channel/UCFzecLVDoJLH3sX78Cu7cvg" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:none;color:#FFFFFF;"><img title="Youtube" src="https://fcttwq.stripocdn.email/content/assets/img/social-icons/circle-white/youtube-circle-white.png" alt="Yt" width="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></a></td> 
                          <td valign="top" align="center" style="padding:0;Margin:0;"><a target="_blank" href="https://www.linkedin.com/company/trademarkers/" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:none;color:#FFFFFF;"><img title="Linkedin" src="https://fcttwq.stripocdn.email/content/assets/img/social-icons/circle-white/linkedin-circle-white.png" alt="In" width="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></a></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                     <!-- <tr style="border-collapse:collapse;"> 
                      <td align="center" mc:edit="block_26" style="padding:0;Margin:0;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:13px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:20px;color:#FFFFFF;">You are receiving this email because you opted in at our website www.trademarkers.com</p></td> 
                     </tr>  -->
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="padding:0;Margin:0;padding-bottom:10px;padding-top:15px;font-size:0;" mc:edit="block_27"><img src="https://fcttwq.stripocdn.email/content/guids/CABINET_c6d6983b8f90c1ab10065255fbabfbaf/images/15841556884046468.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;" width="140"></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
           </table></td> 
         </tr> 
       </table></td> 
     </tr> 
   </table> 
  </div>  
 </body>
</html>