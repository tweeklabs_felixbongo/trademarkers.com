<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
	<head>

		<meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
		<style>
			body{
				font: small/1.5 Arial,Helvetica,sans-serif;
			}
			p {
				margin-bottom: 15px;
				line-height: 20px;
				text-align: justify;
			}
		</style>
    </head>
    <body>

        
<!-- BODY START -->

	<p>Dear Admin,</p>
	<br>
	<p>A customer tried to make a purchase. Check below details</p>

	<p><strong>Name :</strong> {{$user->name}}</p>
	<p><strong>Email :</strong> {{$user->email}}</p>
	<p><strong>System Message : </strong> {{ $sysMessage->getMessage() }}</p>
	

	<br><br>
	<p style="text-align:center"><i>This is an automated email</i></p>

    </body>
</html>