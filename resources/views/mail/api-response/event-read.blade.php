<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
	<head>

		<meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
		<style>
			body{
				font: small/1.5 Ariail,Helvetica,sans-serif;
			}
			p {
				margin-bottom: 15px;
				line-height: 20px;
				text-align: justify;
			}
		</style>
    </head>
    <body>
	<p>Hi <strong>Admin</strong>,</p>
	<p></p>
	
	<p>Email : {{ $email }}</p>
	<p>Event Name : {{ $event->name }}</p>
	<p>Event Type : {{ $event->type->type }}</p>
	<p>Admin event link : https://trademarkers.com/admin/manage/events/{{$event->id}}/edit</p>

		<br>
	<p>Best regards,</p>
	
	<p>Trademarkers, LLC</p>
    </body>
</html>