<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html style="width:100%;font-family:arial, 'helvetica neue', helvetica, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;">
 <head> 
  <meta charset="UTF-8"> 
  <meta content="width=device-width, initial-scale=1" name="viewport"> 
  <meta name="x-apple-disable-message-reformatting"> 
  <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
  <meta content="telephone=no" name="format-detection"> 
  <title>After 4 hours Abandoned Cart Template</title> 
  <!--[if (mso 16)]>
    <style type="text/css">
    a {text-decoration: none;}
    </style>
    <![endif]--> 
  <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]--> 
  <!--[if !mso]><!-- --> 
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i" rel="stylesheet"> 
  <!--<![endif]--> 
   
 <style type="text/css">
	@media only screen and (max-width:600px){
		p,ul li,ol li,a{
			font-size:14px!important;
			line-height:150%!important;
		}

}	@media only screen and (max-width:600px){
		h1{
			font-size:25px!important;
			text-align:center;
			line-height:120%!important;
		}

}	@media only screen and (max-width:600px){
		h2{
			font-size:22px!important;
			text-align:center;
			line-height:120%!important;
		}

}	@media only screen and (max-width:600px){
		h3{
			font-size:16px!important;
			text-align:center;
			line-height:120%!important;
		}

}	@media only screen and (max-width:600px){
		h1 a{
			font-size:25px!important;
		}

}	@media only screen and (max-width:600px){
		h2 a{
			font-size:22px!important;
		}

}	@media only screen and (max-width:600px){
		h3 a{
			font-size:16px!important;
		}

}	@media only screen and (max-width:600px){
		.es-menu td a{
			font-size:16px!important;
		}

}	@media only screen and (max-width:600px){
		.es-header-body p,.es-header-body ul li,.es-header-body ol li,.es-header-body a{
			font-size:16px!important;
		}

}	@media only screen and (max-width:600px){
		.es-footer-body p,.es-footer-body ul li,.es-footer-body ol li,.es-footer-body a{
			font-size:14px!important;
		}

}	@media only screen and (max-width:600px){
		.es-infoblock p,.es-infoblock ul li,.es-infoblock ol li,.es-infoblock a{
			font-size:12px!important;
		}

}	@media only screen and (max-width:600px){
		*[class=gmail-fix]{
			display:none!important;
		}

}	@media only screen and (max-width:600px){
		.es-m-txt-c,.es-m-txt-c h1,.es-m-txt-c h2,.es-m-txt-c h3{
			text-align:center!important;
		}

}	@media only screen and (max-width:600px){
		.es-m-txt-r,.es-m-txt-r h1,.es-m-txt-r h2,.es-m-txt-r h3{
			text-align:right!important;
		}

}	@media only screen and (max-width:600px){
		.es-m-txt-l,.es-m-txt-l h1,.es-m-txt-l h2,.es-m-txt-l h3{
			text-align:left!important;
		}

}	@media only screen and (max-width:600px){
		.es-m-txt-r img,.es-m-txt-c img,.es-m-txt-l img{
			display:inline!important;
		}

}	@media only screen and (max-width:600px){
		.es-button-border{
			display:block!important;
		}

}	@media only screen and (max-width:600px){
		a.es-button{
			font-size:20px!important;
			display:block!important;
			border-left-width:0px!important;
			border-right-width:0px!important;
		}

}	@media only screen and (max-width:600px){
		.es-btn-fw{
			border-width:10px 0px!important;
			text-align:center!important;
		}

}	@media only screen and (max-width:600px){
		.es-adaptive table,.es-btn-fw,.es-btn-fw-brdr,.es-left,.es-right{
			width:100%!important;
		}

}	@media only screen and (max-width:600px){
		.es-content table,.es-header table,.es-footer table,.es-content,.es-footer,.es-header{
			width:100%!important;
			max-width:600px!important;
		}

}	@media only screen and (max-width:600px){
		.es-adapt-td{
			display:block!important;
			width:100%!important;
		}

}	@media only screen and (max-width:600px){
		.adapt-img{
			width:100%!important;
			height:auto!important;
		}

}	@media only screen and (max-width:600px){
		.es-m-p0{
			padding:0px!important;
		}

}	@media only screen and (max-width:600px){
		.es-m-p0r{
			padding-right:0px!important;
		}

}	@media only screen and (max-width:600px){
		.es-m-p0l{
			padding-left:0px!important;
		}

}	@media only screen and (max-width:600px){
		.es-m-p0t{
			padding-top:0px!important;
		}

}	@media only screen and (max-width:600px){
		.es-m-p0b{
			padding-bottom:0!important;
		}

}	@media only screen and (max-width:600px){
		.es-m-p20b{
			padding-bottom:20px!important;
		}

}	@media only screen and (max-width:600px){
		.es-mobile-hidden,.es-hidden{
			display:none!important;
		}

}	@media only screen and (max-width:600px){
		.es-desk-hidden{
			display:table-row!important;
			width:auto!important;
			overflow:visible!important;
			float:none!important;
			max-height:inherit!important;
			line-height:inherit!important;
		}

}	@media only screen and (max-width:600px){
		.es-desk-menu-hidden{
			display:table-cell!important;
		}

}	@media only screen and (max-width:600px){
		table.es-table-not-adapt,.esd-block-html table{
			width:auto!important;
		}

}	@media only screen and (max-width:600px){
		table.es-social{
			display:inline-block!important;
		}

}	@media only screen and (max-width:600px){
		table.es-social td{
			display:inline-block!important;
		}

}		#outlook a{
			padding:0;
		}
		.ExternalClass{
			width:100%;
		}
		.ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{
			line-height:100%;
		}
		.es-button{
			mso-style-priority:100!important;
			text-decoration:none!important;
		}
		a[x-apple-data-detectors]{
			color:inherit!important;
			text-decoration:none!important;
			font-size:inherit!important;
			font-family:inherit!important;
			font-weight:inherit!important;
			line-height:inherit!important;
		}
		.es-desk-hidden{
			display:none;
			float:left;
			overflow:hidden;
			width:0;
			max-height:0;
			line-height:0;
			mso-hide:all;
		}
		.es-button-border:hover a.es-button{
			background:#7dbf44!important;
			border-color:#7dbf44!important;
		}
		.es-button-border:hover{
			background:#7dbf44!important;
		}
		td .es-button-border:hover a.es-button-1{
			background:#7dbf44!important;
			border-color:#7dbf44!important;
		}
		td .es-button-border-2:hover{
			background:#7dbf44!important;
		}
		td .es-button-border:hover a.es-button-3{
			background:#0e6ec2!important;
			border-color:#0e6ec2!important;
		}
		td .es-button-border-4:hover{
			background:#0e6ec2!important;
		}
</style></head> 
 <body style="width:100%;font-family:arial, 'helvetica neue', helvetica, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;"> 
  <div class="es-wrapper-color" style="background-color:#F6F6F6;"> 
   <!--[if gte mso 9]>
			<v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
				<v:fill type="tile" color="#f6f6f6"></v:fill>
			</v:background>
		<![endif]--> 
   <table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top;"> 
     <tr style="border-collapse:collapse;"> 
      <td valign="top" style="padding:0;Margin:0;"> 
       <table cellpadding="0" cellspacing="0" class="es-header" align="center" mc:repeatable style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top;"> 
         <tr style="border-collapse:collapse;"> 
          <td align="center" style="padding:0;Margin:0;"> 
           <table class="es-header-body" width="600" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;"> 
             <tr style="border-collapse:collapse;"> 
              <td align="left" style="Margin:0;padding-bottom:10px;padding-top:20px;padding-left:20px;padding-right:20px;"> 
               <!--[if mso]><table width="560" cellpadding="0" cellspacing="0"><tr><td width="270" valign="top"><![endif]--> 
               <table class="es-left" cellspacing="0" cellpadding="0" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td class="es-m-p20b" width="270" align="left" style="padding:0;Margin:0;"> 
                   <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" style="padding:0;Margin:0;padding-bottom:5px;font-size:0px;" mc:edit="block_0"><img src="https://fcttwq.stripocdn.email/content/guids/CABINET_86f5fa72be96a457c614fce36af2e0d3/images/71321583484725966.jpg" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;" class="adapt-img" width="270"></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table> 
               <!--[if mso]></td><td width="20"></td><td width="270" valign="top"><![endif]--> 
               <table class="es-right" cellspacing="0" cellpadding="0" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td width="270" align="left" style="padding:0;Margin:0;"> 
                   <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td mc:edit="block_1" style="padding:0;Margin:0;"> 
                       <table class="es-menu" width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                         <tr class="links" style="border-collapse:collapse;"> 
                          <td style="Margin:0;padding-left:5px;padding-right:5px;padding-top:28px;padding-bottom:10px;border:0;" width="33.33%" valign="top" bgcolor="transparent" align="center"><a target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:16px;text-decoration:none;display:block;color:#0B5394;" href="https://trademarkers.com/AA-0K2C-P">Registration</a></td> 
                          <td style="Margin:0;padding-left:5px;padding-right:5px;padding-top:28px;padding-bottom:10px;border:0;" width="33.33%" valign="top" bgcolor="transparent" align="center"><a target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:16px;text-decoration:none;display:block;color:#0B5394;" href="https://trademarkers.com/EB-0D5C-F">Services</a></td> 
                          <td style="Margin:0;padding-left:5px;padding-right:5px;padding-top:28px;padding-bottom:10px;border:0;" width="33.33%" valign="top" bgcolor="transparent" align="center"><a target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:16px;text-decoration:none;display:block;color:#0B5394;" href="https://trademarkers.com/EB-0D1C-K">About</a></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table> 
               <!--[if mso]></td></tr></table><![endif]--></td> 
             </tr> 
           </table></td> 
         </tr> 
       </table> 
       <table class="es-content" cellspacing="0" cellpadding="0" align="center" mc:repeatable style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;"> 
         <tr style="border-collapse:collapse;"> 
          <td align="center" style="padding:0;Margin:0;"> 
           <table class="es-content-body" width="600" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;"> 
             <tr style="border-collapse:collapse;"> 
              <td style="padding:0;Margin:0;background-position:center center;" align="left"> 
               <table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td width="600" valign="top" align="center" style="padding:0;Margin:0;"> 
                   <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td style="padding:0;Margin:0;position:relative;" align="center" mc:edit="block_2"><img class="adapt-img" src="https://fcttwq.stripocdn.email/content/guids/bannerImgGuid/images/54031583910114357.png" alt title width="100%" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
           </table></td> 
         </tr> 
       </table> 
       <table class="es-content" cellspacing="0" cellpadding="0" align="center" mc:repeatable style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;"> 
         <tr style="border-collapse:collapse;"> 
          <td align="center" style="padding:0;Margin:0;"> 
           <table class="es-content-body" width="600" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;"> 
             <tr style="border-collapse:collapse;"> 
              <td align="left" style="Margin:0;padding-bottom:15px;padding-top:20px;padding-left:20px;padding-right:20px;"> 
               <table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td width="560" valign="top" align="center" style="padding:0;Margin:0;"> 
                   <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" mc:edit="block_3" style="padding:0;Margin:0;"><h2 style="Margin:0;line-height:31px;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:26px;font-style:normal;font-weight:bold;color:#0B5394;text-align:left;">
                        Hello {{ $user->profiles[0]->first_name }},</h2></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" mc:edit="block_4" style="padding:0;Margin:0;padding-top:10px;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;">
                        Thank you again for considering&nbsp;Trademarkers. Customer like you is the lifeblood of our business. We exist because we care.<br><br></p>
                        <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;">
                        We noticed that you are just <strong><a href="https://trademarkers.com/abandon/{{$user->token}}" target="_blank">a few steps away </a></strong>from completing your Trademark Registration order.<br><br>
                        You can count on our fast and efficient process. Once you complete our order form, we'll handle the next steps, so that you can move on to other important things. We're confident you'll love our dedicated and prompt service. Plus, you get lifetime customer support to help you along the way.<br><br>
                        Lastly, don't forget to take advantage of your&nbsp;exclusive discount.&nbsp;Just <strong><a href="https://trademarkers.com/abandon/{{$user->token}}" target="_blank">sign back in</a></strong>&nbsp;and contact our customer service to get your discount code.<br><br>If you have any questions, call us at +1 212-468-5500 or 1-800-293-3166 Toll-Free.<br><br>You may also talk to one of our trademark consultants live at&nbsp;<strong><a href="http://trademarkers.com">www.trademarkers.com</a>.</strong><br><br>We're here to help.<br><br>Best regards,<br><br><strong>Trademarkers Team </strong></p></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" mc:edit="block_5" style="padding:10px;Margin:0;"><span class="es-button-border es-button-border-4" style="border-style:solid;border-color:#2CB543;background:#0B5394;border-width:0px;display:block;border-radius:25px;width:auto;">
                        <a href="https://trademarkers.com/abandon/{{$user->token}}" class="es-button es-button-3" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#0B5394;border-width:10px 20px 10px 20px;display:block;background:#0B5394;border-radius:25px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;border-left-width:0px;border-right-width:0px;">
                          Log In to Your Account</a></span></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" mc:edit="block_6" style="padding:10px;Margin:0;"><span class="es-button-border es-button-border-4" style="border-style:solid;border-color:#2CB543;background:#0B5394;border-width:0px;display:block;border-radius:25px;width:auto;">
                        <a href="https://trademarkers.com/abandon/{{$user->token}}" class="es-button es-button-3" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#0B5394;border-width:10px 20px 10px 20px;display:block;background:#0B5394;border-radius:25px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;border-left-width:0px;border-right-width:0px;">
                          Check Out Your Oder</a></span></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" mc:edit="block_7" style="padding:10px;Margin:0;"><span class="es-button-border es-button-border-4" style="border-style:solid;border-color:#2CB543;background:#0B5394;border-width:0px;display:block;border-radius:25px;width:auto;">
                        <a href="https://trademarkers.com/EB-0D5C-F" class="es-button es-button-3" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#0B5394;border-width:10px 20px 10px 20px;display:block;background:#0B5394;border-radius:25px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;border-left-width:0px;border-right-width:0px;">
                          Explore Our Services</a></span></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="Margin:0;padding-top:5px;padding-bottom:5px;padding-left:20px;padding-right:20px;font-size:0;" mc:edit="block_8"> 
                       <table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td style="padding:0;Margin:0px 0px 0px 0px;border-bottom:1px solid #CCCCCC;background:none;height:1px;width:100%;margin:0px;"></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" mc:edit="block_9" style="padding:0;Margin:0;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;">Subscribe to our YouTube Channel to get valuable insights about the mechanics of applying for trademark registrations, defending your marks, opposing other conflicting marks, dealing with office actions and many other intellectual property issues.</p></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="Margin:0;padding-top:5px;padding-bottom:5px;padding-left:20px;padding-right:20px;font-size:0;" mc:edit="block_10"> 
                       <table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td style="padding:0;Margin:0px 0px 0px 0px;border-bottom:1px solid #CCCCCC;background:none;height:1px;width:100%;margin:0px;"></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" mc:edit="block_11" style="padding:0;Margin:0;"><a target="_blank" href="https://www.youtube.com/watch?v=XiiI_l0CfF8" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:underline;color:#2CB543;"><img class="adapt-img" src="https://fcttwq.stripocdn.email/content/guids/videoImgGuid/images/83041583485377505.png" alt="Why Should You Register a Trademark? - TradeMarkers®" width="560" title="Why Should You Register a Trademark? - TradeMarkers®" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></a></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
           </table></td> 
         </tr> 
       </table> 
       <table class="es-content" cellspacing="0" cellpadding="0" align="center" mc:repeatable style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;"> 
         <tr style="border-collapse:collapse;"> 
          <td align="center" style="padding:0;Margin:0;"> 
           <table class="es-content-body" width="600" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;"> 
             <tr style="border-collapse:collapse;"> 
              <td style="Margin:0;padding-left:20px;padding-right:20px;padding-top:30px;padding-bottom:30px;background-position:left top;" align="left"> 
               <!--[if mso]><table width="560" cellpadding="0" cellspacing="0"><tr><td width="270" valign="top"><![endif]--> 
               <table class="es-left" cellspacing="0" cellpadding="0" align="left" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td class="es-m-p20b" width="270" align="left" style="padding:0;Margin:0;"> 
                   <table style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-position:center center;" width="100%" cellspacing="0" cellpadding="0" role="presentation"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" mc:edit="block_12" style="padding:0;Margin:0;"><h4 style="Margin:0;line-height:120%;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;color:#0B5394;">Contact Us:</h4></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" mc:edit="block_13" style="padding:0;Margin:0;padding-top:10px;padding-bottom:15px;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;">We're always here for you. If you want to talk through your options, remember we have people who really want to help you out. Our award-winning, support always has your back.</p></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td style="padding:0;Margin:0;"> 
                       <table class="es-table-not-adapt" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td valign="top" align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px;padding-right:10px;font-size:0;" mc:edit="block_14"><img src="https://fcttwq.stripocdn.email/content/guids/CABINET_c6d6983b8f90c1ab10065255fbabfbaf/images/30981556869899567.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;" width="16"></td> 
                          <td align="left" style="padding:0;Margin:0;"> 
                           <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="left" mc:edit="block_15" style="padding:0;Margin:0;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;"><a target="_blank" href="mailto:your@mail.com" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:none;color:#333333;"> info@trademarkers.com</a></p></td> 
                             </tr> 
                           </table></td> 
                         </tr> 
                         <tr style="border-collapse:collapse;"> 
                          <td valign="top" align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px;padding-right:10px;font-size:0;" mc:edit="block_16"><img src="https://fcttwq.stripocdn.email/content/guids/CABINET_c6d6983b8f90c1ab10065255fbabfbaf/images/58031556869792224.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;" width="16"></td> 
                          <td align="left" style="padding:0;Margin:0;"> 
                           <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="left" mc:edit="block_17" style="padding:0;Margin:0;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;"><a target="_blank" href="tel:123456789" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:none;color:#333333;">+1 212 468 5500</a></p></td> 
                             </tr> 
                           </table></td> 
                         </tr> 
                         <tr style="border-collapse:collapse;"> 
                          <td valign="top" align="left" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px;padding-right:10px;font-size:0;" mc:edit="block_18"><img src="https://fcttwq.stripocdn.email/content/guids/CABINET_c6d6983b8f90c1ab10065255fbabfbaf/images/78111556870146007.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;" width="16"></td> 
                          <td align="left" style="padding:0;Margin:0;"> 
                           <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="left" mc:edit="block_19" style="padding:0;Margin:0;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;">Millburn, New Jersey </p></td> 
                             </tr> 
                           </table></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="left" mc:edit="block_20" style="padding:0;Margin:0;padding-top:15px;"><span class="es-button-border es-button-border-4" style="border-style:solid;border-color:#2CB543;background:#0B5394;border-width:0px;display:inline-block;border-radius:25px;width:auto;"><a href="https://trademarkers.com/FA-0K0B-P" class="es-button es-button-3" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#0B5394;border-width:10px 20px 10px 20px;display:inline-block;background:#0B5394;border-radius:25px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;">We're Live</a></span></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table> 
               <!--[if mso]></td><td width="20"></td><td width="270" valign="top"><![endif]--> 
               <table class="es-right" cellspacing="0" cellpadding="0" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td width="270" align="left" style="padding:0;Margin:0;"> 
                   <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="padding:0;Margin:0;font-size:0;" mc:edit="block_21"><img class="adapt-img" src="https://fcttwq.stripocdn.email/content/guids/CABINET_c6d6983b8f90c1ab10065255fbabfbaf/images/52821556874243897.jpg" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;" width="270"></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table> 
               <!--[if mso]></td></tr></table><![endif]--></td> 
             </tr> 
           </table></td> 
         </tr> 
       </table> 
       <table cellpadding="0" cellspacing="0" class="es-footer" align="center" mc:repeatable style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top;"> 
         <tr style="border-collapse:collapse;"> 
          <td align="center" style="padding:0;Margin:0;"> 
           <table class="es-footer-body" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#333333;" width="600" cellspacing="0" cellpadding="0" bgcolor="#333333" align="center"> 
             <tr style="border-collapse:collapse;"> 
              <td style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px;background-color:#0B5394;" bgcolor="#0b5394" align="left"> 
               <table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td width="560" valign="top" align="center" style="padding:0;Margin:0;"> 
                   <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td mc:edit="block_22" style="padding:0;Margin:0;"> 
                       <table class="es-menu" width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                         <tr class="links" style="border-collapse:collapse;"> 
                          <td style="Margin:0;padding-left:5px;padding-right:5px;padding-top:0px;padding-bottom:0px;border:0;" width="33.33%" valign="top" bgcolor="transparent" align="center"><a target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:none;display:block;color:#FFFFFF;" href="https://trademarkers.com/AA-0K2B-P">Pricing</a></td> 
                          <td style="Margin:0;padding-left:5px;padding-right:5px;padding-top:0px;padding-bottom:0px;border:0;border-left:1px solid #FFFFFF;" width="33.33%" valign="top" bgcolor="transparent" align="center"><a target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:none;display:block;color:#FFFFFF;" href="https://trademarkers.com/FA-0K6A-T">Resources</a></td> 
                          <td style="Margin:0;padding-left:5px;padding-right:5px;padding-top:0px;padding-bottom:0px;border:0;border-left:1px solid #FFFFFF;" width="33.33%" valign="top" bgcolor="transparent" align="center"><a target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:none;display:block;color:#FFFFFF;" href="https://trademarkers.com/FA-0K0B-P">Contact Us</a></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
             <tr style="border-collapse:collapse;"> 
              <td style="Margin:0;padding-bottom:15px;padding-top:20px;padding-left:20px;padding-right:20px;background-color:#0B5394;" bgcolor="#0b5394" align="left"> 
               <table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td width="560" valign="top" align="center" style="padding:0;Margin:0;"> 
                   <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="padding:0;Margin:0;padding-bottom:15px;font-size:0;" mc:edit="block_23"> 
                       <table class="es-table-not-adapt es-social" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td valign="top" align="center" style="padding:0;Margin:0;padding-right:15px;"><a target="_blank" href="https://www.facebook.com/TrademarkersLLC/" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:underline;color:#FFFFFF;"><img title="Facebook" src="https://fcttwq.stripocdn.email/content/assets/img/social-icons/circle-white/facebook-circle-white.png" alt="Fb" width="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></a></td> 
                          <td valign="top" align="center" style="padding:0;Margin:0;padding-right:15px;"><a target="_blank" href="https://twitter.com/trademarkersllc" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:underline;color:#FFFFFF;"><img title="Twitter" src="https://fcttwq.stripocdn.email/content/assets/img/social-icons/circle-white/twitter-circle-white.png" alt="Tw" width="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></a></td> 
                          <td valign="top" align="center" style="padding:0;Margin:0;padding-right:15px;"><a target="_blank" href="https://www.youtube.com/channel/UCFzecLVDoJLH3sX78Cu7cvg" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:underline;color:#FFFFFF;"><img title="Youtube" src="https://fcttwq.stripocdn.email/content/assets/img/social-icons/circle-white/youtube-circle-white.png" alt="Yt" width="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></a></td> 
                          <td valign="top" align="center" style="padding:0;Margin:0;"><a target="_blank" href="https://www.linkedin.com/company/trademarkers/" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:14px;text-decoration:underline;color:#FFFFFF;"><img title="Linkedin" src="https://fcttwq.stripocdn.email/content/assets/img/social-icons/circle-white/linkedin-circle-white.png" alt="In" width="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;"></a></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                     <!-- <tr style="border-collapse:collapse;"> 
                      <td align="center" mc:edit="block_24" style="padding:0;Margin:0;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:13px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:20px;color:#FFFFFF;">You are receiving this email because you opted in at our website www.trademarkers.com </p></td> 
                     </tr>  -->
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="padding:0;Margin:0;padding-bottom:10px;padding-top:15px;font-size:0;" mc:edit="block_25"><img src="https://fcttwq.stripocdn.email/content/guids/CABINET_c6d6983b8f90c1ab10065255fbabfbaf/images/15841556884046468.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;" width="140"></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
           </table></td> 
         </tr> 
       </table></td> 
     </tr> 
   </table> 
  </div>  
 </body>
</html>