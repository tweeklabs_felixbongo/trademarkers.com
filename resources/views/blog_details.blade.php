@extends('layouts.master')

@push('head')
<!-- Global site tag (gtag.js) - Google Ads: 851729164 --> 

<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v5.0&appId=1349763241764094&autoLogAppEvents=1"></script>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
@endpush

@section('content')

<div class="container">
    <div class="row justify-content-center">

    	<div class="col-md-12" style="text-align: left;">
    		<h2 class="text-center" style="font-weight:bold">{{ $blog->title }}</h2>
            <hr>
    	</div>

        <div class="col-md-12">
            @if ( $blog->featured_img )
                <img src="{{url('/uploads/')}}/{{$blog->featured_img}}" alt="{{$blog->title}}" style="width:100%" />
            @endif
            <div class="system-content">
                {!! $blog->description !!}
            </div>

            <p class="author">
                <em> Author : {{$blog->author->name}} </em><br>
                <em> Published on: {{date('M d, Y', strtotime($blog->created_at))}}</em>
            </p>

            <div id="fb-root"></div>

            @comments([
                'model' => $blog
            ])

            <!-- SHARE BUTTONS CUSTOM -->
            <div class="share-buttons">
                <div class="fb-share-button" data-href="{{url('/uploads/')}}/{{$blog->slug}}" data-layout="button" data-size="large"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Share</a></div>
                <a class="linkedin-btn" 
                    href="https://www.linkedin.com/shareArticle?mini=true&url={{url('/article/')}}/{{$blog->slug}}&title={{$blog->title}}&summary=trademarkers.com&source=Trademarkers LLC" 
                    onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" >
                    <img src="{{URL::asset('/images/linkedin.png')}}" />
                </a>
                <a 
                    href="https://twitter.com/intent/tweet?url={{url('/article/')}}/{{$blog->slug}}&text={{$blog->title}}" 
                    data-href="{{url('/uploads/')}}/{{$blog->slug}}" 
                    class="twitter-hashtag-button" 
                    data-size="large" 
                    data-show-count="false">Tweet
                </a>

            </div><!-- END SHARE BUTTONS CUSTOM -->
            

    

        </div>
       
    </div>
</div>
@endsection

