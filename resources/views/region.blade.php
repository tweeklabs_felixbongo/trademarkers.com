@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h5> Register your Trademark in {{ $result[0]['name'] }}</h5>
                    <hr>
                    <p> Every day, disputes about brand names, domain names and company names are litigated worldwide, and companies worldwide lose the right to use a brand, or even their Internet domain names because they failed to protect it through trademark registration. TRADEMARKERS.COM is a worldwide network of attorneys that can make sure your brand enjoys trademark protection - in the markets you need to own!</p>

                    <p>What many do not know is that there is no such thing as a "worldwide trademark". All trademarks jurisdictions cover only the territory they are valid in. So you might live in false confidence that your brand is protected worldwide.</p>
                </div>

                <div class="card-body">
                    <div class="row">
                        @foreach( $result[0]->countries as $country )
                            <div class="col-md-4">
                                <a href="/trademark-registration-in-{{ strtolower(str_replace( ' ', '_', $country['name'] )) }}" style="color: #000;">
                                    <p>
                                        <img src="/images/{{ $country['avatar'] }}" /> 
                                        {{ $country['name'] }} 
                                    </p>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
