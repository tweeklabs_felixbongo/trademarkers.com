@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        @include('layouts.customer-nav', $profile = [1])

        @include('profile.information');
    </div>
</div>

@endsection