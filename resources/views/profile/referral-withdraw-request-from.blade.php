@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        @include('layouts.customer-nav', $referral = [1])

        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <h4> Trademarkers Withdraw Referral Points Form <span class="coins"><i class="fa fa-coins"></i> {{ isset($actionCode) ? $actionCode->total_points : '0' }}</span></h4>
                    <br>
                    <form method="post" action="">
                    @csrf

                        <div class="form-group row">
                            <div class="col-md-12">
                                <label for="type">
                                    <b> Please select Payment Method</b>
                                </label>
                                <select class="form-control{{ $errors->has('type') ? ' is-invalid' : '' }}" name="payment_method" value="" requi>
                                    <option value="paypal" selected>Paypal</option>
                                    <option value="bank">US Bank Account</option>
                                    <option value="check">USD Check</option>           
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <label for="type">
                                    <b> Name</b>
                                </label>
                                <input type="text" class="form-control" name="name" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <label for="type">
                                    <b>Contact Email</b>
                                </label>
                                <input type="email" class="form-control" name="email" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <label for="type">
                                    <b> Request Amount</b>
                                </label>
                                <input type="number" class="form-control" name="req_amount" max="{{$actionCode->total_points}}" value="{{ $actionCode->total_points }}" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <label for="type">
                                    <b> Please enter needed information for the selected payment method</b>
                                    <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-html="true" title="
                                        <p class='text-left'><b>Paypal :</b> Email Address assosiated with paypal account</p>
                                        <p class='text-left'><b>US Bank Account :</b> Account number, Account name, Bank name, Bank address, Account address</p>
                                        <p class='text-left'><b>USD Check :</b> Mailing Address</p>
                                        "></i>
                                </label>
                                <textarea class="form-control" rows="6" name="information" required>
                                </textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12 text-right">
                                <input type="submit" class="btn btn-danger" value="Request Payment">
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection