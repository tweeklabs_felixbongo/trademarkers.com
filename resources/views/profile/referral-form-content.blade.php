<div class="col-md-9">
    <div class="card">
        <div class="card-body">
            <h4> Trademarkers Referral <span class="coins"><i class="fa fa-coins"></i> {{ isset($actionCode) ? $actionCode->total_points : '0' }}</span></h4>
            <br>
            @if ( session('requestMessage') )
                <div style="text-align: center;" class="alert alert-info" role="alert">
                    {{ session('requestMessage') }}
                </div>
            @endif

            @if ($actionCode)
                <table class="table">
                    <tr>
                        <th></th>
                        <th>Description</th>
                        <th>Points</th>
                        <th>Date</th>
                    </tr>
                    @if ( $actionCode->invites )
                        
                        @foreach ($actionCode->transactions as $transaction)
                            <tr class="{{ $transaction->status }}">
                                <td>
                                    @if ( $transaction->status == 'credit' ) 
                                        <i class="fa fa-money-bill-wave"></i>
                                    @elseif( $transaction->status == 'processing' ) 
                                        <i class="fa fa-money-check-alt"></i>
                                   @else
                                        <i class="fa fa-star"></i>
                                    @endif
                                </td>
                                <td>{{ $transaction->description }}</td>
                                <td>{{ ($transaction->status == 'credit' ? '+' : '-') }} {{ $transaction->amount }}</td>
                                <td>{{ date('M d, Y', strtotime($transaction->created_at)) }}</td>
                            </tr>
                        @endforeach 
                    @endif
                </table>

                @if ($actionCode->total_points > 0)
                <p class="text-right">
                    <a class="btn btn-primary" href="/customer/referral/withdraw">Withdraw Points</a>
                    </p>
                @endif
            @endif 
            @if ( $actionCode ) 
            <p class="referral-link"><b>Referral Link :</b> <i>{{ url('/') . '/' . $actionCode->action->case_number }}</i></p>

            @else 
                <form method="post" action="">
                @csrf
                    <input type="hidden" name="action" value="generate">
                    <input type="submit" class="btn btn-danger" value="Create Referral link"> 
                </form>
            @endif
        </div>
    </div>
</div>