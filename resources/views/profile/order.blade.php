@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">

        @include('layouts.customer-nav', $order = [1])

        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">

                            @if ( count( $trademarks ) == 0 )
                                <table class="table table-hover table-condensed">
                                    
                                    <thead>
                                        <tr>
                                            <td> Order #</td>
                                            <td> Order Date</td>
                                            <td> Total Items</td>
                                            <td> <i class="fa fa-cog"></i></td>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach( $orders->reverse() as $order )
                                            <tr>
                                                <td style="color:red;"> {{ $order['order_number'] }}</td>
                                                <td> {{ $order['created_at']->diffForHumans() }}</td>
                                                <td> {{ $order['total_items'] }}</td>
                                                {{-- <td> 
                                                    ${{ number_format( $order['total_amount'], 2) }}
                                                </td> --}}
                                                <td>
                                                    <a href="/customer/orders/{{ $order['order_number'] }}">
                                                        <i class="fa fa-eye"></i> View
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            @else
                                <table class="table table-striped">
                                    
                                    <thead>
                                        <tr>
                                            <td> Trademark ID</td>
                                            <td> Service</td>
                                            <td> Country</td>
                                            <td> Amount</td>
                                            <td> <i class="fa fa-cog"></i></td>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach( $trademarks as $trademark )
                                            <tr>
                                                <td> {{ 14107360 + $trademark['id'] }}</td>
                                                <td> {{ $trademark['service'] }}</td>
                                                <td> 
                                                    <img src="{{ asset('images/' . $trademark->country['avatar']) }}" /> 
                                                    {{ $trademark->country['name'] }}
                                                </td>
                                                <td> 
                                                    ${{ number_format( $trademark['amount'], 2) }}
                                                </td>
                                                <td>
                                                    <a href="/customer/trademark/{{ $trademark['id'] }}">
                                                        <i class="fa fa-eye"></i> View
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection