<div class="col-md-9">
    <div class="card">
        <div class="card-body">
            @if ( session('check_profile') )
                <div class="alert alert-success" role="alert">
                    {{ session('check_profile') }}
                </div>
            @endif
            <form method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-10">
                        <div class="form-group row">
                            <span style="padding-left: 15px;"> <b>Please select a profile: </b></span>
                            <div class="col-md-12">
                                <select class="form-control" name="q">
                                    <option selected> Select </option>
                                    @foreach( $profiles as $profile )
                                        <option value="{{ $profile->id }}"> 
                                            @if ( $profile->nature == 'Company' )
                                                Company Profile [{{ $profile->first_name }}] [{{ $profile->company }}]
                                            @else
                                                Personal Profile [{{ $profile->first_name }} {{ $profile->last_name }}] [{{ $profile->email }}]
                                            @endif
                                            
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group row">
                            <span style="padding-left: 15px; visibility: hidden;" >s</span>
                            <div class="col-md-12">
                                <button type="submit" name="view" class="btn btn-danger">
                                    {{ isset( $country ) ? __('Proceed') : __('View') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <hr>

            <div class="col-md-2">
                <div class="form-group row">
                    <div class="col-md-12">
                        <button onclick="window.location.href='{{ isset( $country ) ? '/customer/create/profile/' . $country->abbr : '/customer/create/profile' }}'" name="create" class="btn btn-info">
                            {{ __('Add New Profile') }}
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>