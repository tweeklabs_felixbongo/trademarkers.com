@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        @include('layouts.customer-nav', $referral = [1])

        @include('profile.referral-form-content');
    </div>
</div>

@endsection