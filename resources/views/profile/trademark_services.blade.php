@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">

        @include('layouts.customer-nav', $trademark = [1])

        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">

                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <td> Trademark ID</td>
                                        <td> Service</td>
                                        <td> Country</td>
                                        <td> Amount</td>
                                        <td> <i class="fa fa-cog"></i></td>
                                    </tr>
                                </thead>

                                <tbody>

                                    @if ( count( $trademarks ) == 0 )
                                        <tr><td colspan="5">No results</td></tr>
                                    @endif

                                    @foreach( $trademarks as $trademark )
                                        <tr>
                                            <td> {{ 14107360 + $trademark->id }}</td>
                                            <td> {{ $trademark['service'] }}</td>
                                            <td> 
                                                <img src="{{ asset('images/' . $trademark->country['avatar']) }}" /> 
                                                {{ $trademark->country['name'] }}
                                            </td>
                                            <td> 
                                                ${{ number_format( $trademark['amount'], 2) }}
                                            </td>
                                            <td>
                                                <a href="/customer/trademark/{{ $trademark['id'] }}">
                                                    <i class="fa fa-eye"></i> View
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection