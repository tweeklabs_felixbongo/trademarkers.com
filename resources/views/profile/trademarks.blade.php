@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">

        @include('layouts.customer-nav', $trademark = [1])

        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <div class="row" style="text-align: center;">
                        <div class="col-md-3">
                            <div class="icon-wrapper">
                                <i class="i fa fa-envelope fa-5x icon-grey"></i>
                                <span class="badge-registration">{{ $data['registration'] }}</span>
                                <a href="/customer/trademarks/registration"><p> <b> Trademark Registration</b></p></a>
                            </div>
                        </div>  
                        <div class="col-md-3">
                            <div class="icon-wrapper">
                                <i class="fa fa-book-open fa-5x icon-grey"></i>
                                <span class="badge-study">{{ $data['study'] }}</span>
                                <a href="/customer/trademarks/study"><p> <b> Trademark Study</b></p></a>
                            </div>
                        </div> 
                        <div class="col-md-3">
                            <div class="icon-wrapper">
                                <i class="fa fa-calendar-check fa-5x icon-grey"></i>
                                <span class="badge-monitoring">{{ $data['monitoring'] }}</span>
                                <a href="/customer/trademarks/monitoring"><p> <b> Trademark Monitoring</b></p></a>
                            </div>
                        </div>  
                        <div class="col-md-3">
                            <div class="icon-wrapper">
                                <i class="fa fa-envelope-open-text fa-5x icon-grey"></i>
                                <span class="badge-others">{{ $data['others'] }}</span>
                                <a href="/customer/trademarks/others"><p> <b> Other Services</b></p></a>
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection