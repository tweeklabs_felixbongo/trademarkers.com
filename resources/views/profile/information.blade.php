<div class="col-md-9">
    <div class="card">
        <div class="card-body">
            <h4> Trademark Applicant Information</h4>
            <p><b> Please check and indicate <i>N/A</i> if not applicable.</b></p>
            @if ( session('profile') )
                <div class="alert alert-success" role="alert">
                    {{ session('profile') }}
                </div>
            @endif

            @php
              $case = session('legal_case');
        
            @endphp
            <form id="frm-profile" method="POST">
                @csrf
                <div class="row">


                    <div id="nature_block" class="col-md-6">
                        <div class="form-group row">
                            <span style="padding-left: 15px;"> Nature </span>
                            <div class="col-md-12">
                                <input id="nature" type="text" class="form-control{{ $errors->has('nature') ? ' is-invalid' : '' }}" name="nature" value="{{ isset( $prof->nature ) ? $prof->nature : (old('nature') ? old('nature') : 'Individual') }}" >

                                
                                @if ($errors->has('nature'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nature') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div id="sub_nature_block" class="col-md-6" style="display: none;">
                        <div class="form-group row">
                            <span style="padding-left: 15px;"> Nature </span>
                            <div class="col-md-12">
                                <select id="sub_nature" name="sub_nature" class="form-control">
                                    <option value="Individual"> Individual</option>
                                    <option value="Company"> Company</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div id="company_block" class="col-md-6">
                        <div class="form-group row">
                            <span style="padding-left: 15px;"> Company Name </span>
                            <div class="col-md-12">
                                <input id="company" type="text" class="form-control{{ $errors->has('company') ? ' is-invalid' : '' }}" name="company" value="{{ isset( $prof->company ) ? $prof->company : old('company') }}"  >

                                @if ($errors->has('company'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('company') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <!-- START WRAP NAME INFO -->
                    <div id="wrap-name-info" class="col-md-12"><div class="row">
                            
                    <!-- NATURE INFORMATION HERE -->
                    <div id="nature-label" class="col-md-12">
                        <div class="form-group row">
                            <div class="col-md-12">
                                <h4>Address Information</h4>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group row">
                            <span style="padding-left: 15px;"> First Name </span>
                            <div class="col-md-12">
                                <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ isset( $prof->first_name ) ? $prof->first_name : old('first_name') }}" >

                                @if ($errors->has('first_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group row">
                            <span style="padding-left: 15px;"> Last Name </span>
                            <div class="col-md-12">
                                <input id="name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ isset( $prof->last_name ) ? $prof->last_name : old('last_name') }}" >

                                @if ($errors->has('last_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group row">
                            <span style="padding-left: 15px;">Email Address <b class="asterisk">*</b></span>
                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ (isset( $prof->email ) && strpos($prof->email,'@temporary.email') == false) ? $prof->email : old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group row">
                            <span style="padding-left: 15px;"> Phone Number</span>
                            <div class="col-md-12">
                                <input id="phone" type="number" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ isset( $prof->phone_number ) ? $prof->phone_number :old('phone') }}" >

                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div id="fax-block" style="display:none" class="col-md-3">
                        <div class="form-group row">
                            <span style="padding-left: 15px;"> Fax</span>
                            <div class="col-md-12">
                                <input id="fax" type="text" class="form-control{{ $errors->has('fax') ? ' is-invalid' : '' }}" name="fax" value="{{ isset( $prof->fax ) ? $prof->fax : old('fax') }}" >

                                @if ($errors->has('fax'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('fax') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div id="position-block" style="display:none" class="col-md-3">
                        <div class="form-group row">
                            <span style="padding-left: 15px;"> Position</span>
                            <div class="col-md-12">
                                <input id="position" type="text" class="form-control{{ $errors->has('fax') ? ' is-invalid' : '' }}" name="position" value="{{ isset( $prof->position ) ? $prof->position : old('position') }}" >

                                @if ($errors->has('fax'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('fax') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    </div></div><!-- END WRAP NAME INFO -->

                    

                    

                    

               
                    
                    <!-- ADDRESS INFORMATION HERE -->
                    <div class="col-md-12">
                        <div class="form-group row">
                            <div class="col-md-12">
                                <h4>Address Information</h4>
                            </div>
                        </div>
                    </div>

                    <div id="country_block" class="col-md-12">
                        <div class="form-group row">
                            <span style="padding-left: 15px;"> Country </span>
                            <div class="col-md-12">
                                <input id="country" type="text" class="form-control{{ $errors->has('country') ? ' is-invalid' : '' }}" name="country" value="{{ isset( $prof->country ) ? $prof->country : old('country') }}"  >

                                @if ($errors->has('country'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    

                    <div id="sub_country_block" class="col-md-12" style="display: none;">
                        <div class="form-group row">
                            <span style="padding-left: 15px;"> Country </span>
                            <div class="col-md-12">
                                <select id="sub_country" name="sub_country" class="form-control">
                                    @if ( isset($prof) )
                                        <option>-</option>
                                    @endif

                                    @foreach( $countries as $country )
                                        <option value="{{ $country->name }}" {{ ( isset($prof) && $country->name == $prof->country ? 'selected' : '') }}> {{ $country->name }}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>
                    </div>

                    

                    <div class="col-md-12">
                        <div class="form-group row">
                            <span style="padding-left: 15px;"> Address </span>
                            <div class="col-md-12">
                                <input id="house" type="text" class="form-control{{ $errors->has('house') ? ' is-invalid' : '' }}" name="house" value="{{ isset( $prof->house ) ? $prof->house : old('house') }}">
                                
                                <input type="text" style="margin-top:10px" class="form-control{{ $errors->has('street') ? ' is-invalid' : '' }}" name="street" value="{{ isset( $prof->street ) ? $prof->street : old('street') }}">

                                @if ($errors->has('house'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('house') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div id="city-block" class="col-md-2">
                        <div class="form-group row">
                            <span style="padding-left: 15px;"> <label id="city-label">City</label> </span>
                            <div class="col-md-12">
                                <input id="city" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{ isset( $prof->city ) ? $prof->city : old('city') }}">

                                @if ($errors->has('city'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div id="state-province-block" class="col-md-5">
                        <div class="form-group row">
                            <span style="padding-left: 15px;"> <label id="state-label">State/Province</label> </span>
                            <div class="col-md-12">
                                <select name="state" id="state" class="form-control">
                                    @if ( isset($prof) && $prof->state)
                                        <option value="{{$prof->state}}">{{$prof->state}}</option>
                                    @endif
                                </select>
                                @if ($errors->has('state'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('state') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div id="postal-block" class="col-md-5">
                        <div class="form-group row">
                            <span style="padding-left: 15px;"> <label id="postal-label">Postal Code</label></span>
                            <div class="col-md-12">
                                <input id="zip_code" type="text" class="form-control{{ $errors->has('zip_code') ? ' is-invalid' : '' }}" name="zip_code" value="{{ isset( $prof->zip_code ) ? $prof->zip_code : old('zip_code') }}" >

                                @if ($errors->has('zip_code'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('zip_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    

                    <div class="col-md-6" style="visibility: hidden;">
                        <div class="form-group row">
                            <span style="padding-left: 15px;"> Fax</span>
                            <div class="col-md-12">
                                <input id="fax" type="text" class="form-control" name="origin" value="{{ isset( $prof->fax ) ? $prof->fax : "" }}" >
                            </div>
                        </div>
                    </div>
                </form>
                
                <div class="form-group row mb-0">
                    <div class="col-md-8 offset-md-2 btn-pull-right">
                        <button type="submit" class="btn btn-danger btn-save-profile">
                            {{ isset( $country ) ?  __('Proceed') :  __('Save') }}
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>