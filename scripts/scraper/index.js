let scraper = require('./scraper');
let config  = require('./config.json');
let params  = require('./params.json');

let run = async () => {
    Promise
        .resolve()
        .then(() => {
            scraper.getTableData(config.domSelectors, params.startDate, params.endDate);
        });
};

run();
