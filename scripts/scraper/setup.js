const puppeteer = require('puppeteer');

const getBrowser = (() => {
    let browser;

    return async () => {
        browser = await puppeteer.launch({
            headless: false,
            dumpio:   true,
            slowMo:   250
        });

        return browser;
    }
})();

exports.getBrowser = getBrowser;
