let setup  = require('./setup');
let config = require('./config.json');

module.exports = {
    init:         async () => {
        const browser = await setup.getBrowser();
        const page    = await browser.newPage();
        await page.setExtraHTTPHeaders({
            'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8'
        });

        return page;
    },
    applyFilter:  async (page, domSelector, startDate, endDate) => {
        console.log('Applying filters...');
        await page.goto(config.url);
        await page.waitFor(domSelector.datesTab);
        await page.click(domSelector.datesTab);
        await page.click(domSelector.registrationField);
        await page.keyboard.type(startDate + ' TO ' + endDate);
        await page.$eval(domSelector.searchButton, el => el.click());
        await page.waitForSelector(domSelector.table, {timeout: 120000, visible: true});
    },
    getTableData: async (domSelector, startDate, endDate) => {
        let page = await module.exports.init();
        await module.exports.applyFilter(page, domSelector, startDate, endDate);

        // Change result to 100 per page
        console.log('Changing result per page to 100...');
        await page.$eval(domSelector.displayPerPageList, el => el.click());
        await page.waitForSelector(domSelector.table, {timeout: 120000, visible: true});
        let totalResultText = await page.evaluate((el) => {
            return document.querySelectorAll(el)[0].textContent;
        }, domSelector.totalResultText);
        console.log(totalResultText);
        let totalData = totalResultText.split('/')[1].replace(/\s/g, '').replace(',', '').trim();
        console.log(totalData);
        let totalPage = (Math.ceil(totalData / 100) * 100) / 100; // Round off to the nearest hundreds
        console.log(totalPage);

        // Loop through all pages
        let result = [];
        for (let currentPage = 1; currentPage <= totalPage; currentPage++) {
            let pageResult = await module.exports.parsePage(page, domSelector);
            result.push(pageResult);

            if (totalPage > 1) {
                await page.click(domSelector.nextPageButton);
                await page.waitForSelector(domSelector.table, {timeout: 120000, visible: true});
            }
        }

        return result;
    },
    parsePage:    async (page, domSelector) => {
        let tableRowSelector = domSelector.tableRow;
        let totalRows        = await page.evaluate((el) => {
            return document.querySelectorAll(el).length;
        }, tableRowSelector);
        console.log(totalRows);

        // Loop through all rows for current page
        let result = [];
        for (let currentRow = 0; currentRow < totalRows; currentRow++) {
            let data = await page.evaluate(({el, currentRow}) => {
                let tableRow  = document.getElementById(currentRow);
                let tableCell = Array.from(tableRow.getElementsByTagName('td'));

                return tableCell.map(td => td.innerHTML);
            }, {tableRowSelector, currentRow});
            console.log(data);
            result.push(data);
        }

        return result;
    }
}
