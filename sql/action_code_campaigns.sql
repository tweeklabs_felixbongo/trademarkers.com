-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 22, 2019 at 08:02 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tm`
--

-- --------------------------------------------------------

--
-- Table structure for table `action_code_campaigns`
--

CREATE TABLE `action_code_campaigns` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `count` mediumint(9) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `action_code_campaigns`
--

INSERT INTO `action_code_campaigns` (`id`, `name`, `file_name`, `count`, `created_at`, `updated_at`) VALUES
(1, 'Brexit-2019-02-23.csv', '88d2b5a7baab74ff4e573b73fb2d48c5.csv', 78754, '2019-04-22 09:33:02', '2019-04-22 09:33:02'),
(2, 'uspto-2018-04-22.csv', '3588860246e6ed74c25c3c672cdd5550.csv', 3000, '2019-04-22 09:44:52', '2019-04-22 09:44:52'),
(3, 'eu-2018-10.csv', '90bb2ae8d1d84b59c3cce19ccde7400a.csv', 500, '2019-04-22 09:46:04', '2019-04-22 09:46:04'),
(4, 'eu-2018-11.csv', 'fff031421a71573ac98a9fce6896f6df.csv', 1002, '2019-04-22 09:46:16', '2019-04-22 09:46:16'),
(5, 'eu-2018-12.csv', '2320981147dfbbebdb793733734f4b14.csv', 1002, '2019-04-22 09:47:18', '2019-04-22 09:47:18'),
(6, 'eu-2019-01.csv', 'acc1360915dfc628b7f3752ce536847e.csv', 1001, '2019-04-22 09:48:06', '2019-04-22 09:48:06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `action_code_campaigns`
--
ALTER TABLE `action_code_campaigns`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `action_code_campaigns`
--
ALTER TABLE `action_code_campaigns`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
