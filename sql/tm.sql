-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 18, 2018 at 10:26 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tm`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_config`
--

CREATE TABLE `admin_config` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_menu`
--

CREATE TABLE `admin_menu` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uri` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permission` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_menu`
--

INSERT INTO `admin_menu` (`id`, `parent_id`, `order`, `title`, `icon`, `uri`, `permission`, `created_at`, `updated_at`) VALUES
(1, 0, 1, 'Index', 'fa-bar-chart', '/', NULL, NULL, NULL),
(2, 0, 2, 'Admin', 'fa-tasks', '', NULL, NULL, NULL),
(3, 2, 3, 'Users', 'fa-users', 'auth/users', NULL, NULL, NULL),
(4, 2, 4, 'Roles', 'fa-user', 'auth/roles', NULL, NULL, NULL),
(5, 2, 5, 'Permission', 'fa-ban', 'auth/permissions', NULL, NULL, NULL),
(6, 2, 6, 'Menu', 'fa-bars', 'auth/menu', NULL, NULL, NULL),
(7, 2, 7, 'Operation log', 'fa-history', 'auth/logs', NULL, NULL, NULL),
(8, 0, 0, 'Class Description', 'fa-bars', 'manage/description', '*', '2018-12-16 19:21:55', '2018-12-16 19:21:55'),
(9, 0, 0, 'Orders', 'fa-bars', 'manage/orders', '*', '2018-12-16 19:23:45', '2018-12-16 19:23:45'),
(10, 0, 0, 'Trademarks', 'fa-bars', 'manage/trademarks', '*', '2018-12-16 19:25:52', '2018-12-16 19:25:52');

-- --------------------------------------------------------

--
-- Table structure for table `admin_operation_log`
--

CREATE TABLE `admin_operation_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `input` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_operation_log`
--

INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-12-16 19:19:41', '2018-12-16 19:19:41'),
(2, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:19:50', '2018-12-16 19:19:50'),
(3, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"Class Description\",\"icon\":\"fa-bars\",\"uri\":\"manage\\/description\",\"roles\":[\"1\",null],\"permission\":\"*\",\"_token\":\"q8hH1XK7q2ckFXs2flDwJwLB9vGvHnyc11grdcb8\"}', '2018-12-16 19:21:55', '2018-12-16 19:21:55'),
(4, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-12-16 19:21:55', '2018-12-16 19:21:55'),
(5, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-12-16 19:21:56', '2018-12-16 19:21:56'),
(6, 1, 'admin/manage/description', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:21:58', '2018-12-16 19:21:58'),
(7, 1, 'admin/manage/description/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:22:09', '2018-12-16 19:22:09'),
(8, 1, 'admin/api/class', 'GET', '127.0.0.1', '{\"q\":\"h\"}', '2018-12-16 19:22:14', '2018-12-16 19:22:14'),
(9, 1, 'admin/api/class', 'GET', '127.0.0.1', '{\"q\":\"h\"}', '2018-12-16 19:22:15', '2018-12-16 19:22:15'),
(10, 1, 'admin/api/class', 'GET', '127.0.0.1', '{\"q\":\"h\",\"page\":\"2\"}', '2018-12-16 19:22:21', '2018-12-16 19:22:21'),
(11, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:23:08', '2018-12-16 19:23:08'),
(12, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"Orders\",\"icon\":\"fa-bars\",\"uri\":\"manage\\/orders\",\"roles\":[\"1\",null],\"permission\":\"*\",\"_token\":\"q8hH1XK7q2ckFXs2flDwJwLB9vGvHnyc11grdcb8\"}', '2018-12-16 19:23:45', '2018-12-16 19:23:45'),
(13, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-12-16 19:23:45', '2018-12-16 19:23:45'),
(14, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-12-16 19:23:52', '2018-12-16 19:23:52'),
(15, 1, 'admin/manage/orders', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:23:54', '2018-12-16 19:23:54'),
(16, 1, 'admin/manage/orders/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:24:06', '2018-12-16 19:24:06'),
(17, 1, 'admin/manage/orders', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:24:16', '2018-12-16 19:24:16'),
(18, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:25:39', '2018-12-16 19:25:39'),
(19, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"Trademarks\",\"icon\":\"fa-bars\",\"uri\":\"manage\\/trademarks\",\"roles\":[\"1\",null],\"permission\":\"*\",\"_token\":\"q8hH1XK7q2ckFXs2flDwJwLB9vGvHnyc11grdcb8\"}', '2018-12-16 19:25:52', '2018-12-16 19:25:52'),
(20, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-12-16 19:25:52', '2018-12-16 19:25:52'),
(21, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2018-12-16 19:25:53', '2018-12-16 19:25:53'),
(22, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:25:55', '2018-12-16 19:25:55'),
(23, 1, 'admin/manage/trademarks/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:27:05', '2018-12-16 19:27:05'),
(24, 1, 'admin/manage/trademarks/1/edit', 'GET', '127.0.0.1', '[]', '2018-12-16 19:27:46', '2018-12-16 19:27:46'),
(25, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:27:49', '2018-12-16 19:27:49'),
(26, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '[]', '2018-12-16 19:28:50', '2018-12-16 19:28:50'),
(27, 1, 'admin/manage/trademarks/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:28:53', '2018-12-16 19:28:53'),
(28, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:28:56', '2018-12-16 19:28:56'),
(29, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '[]', '2018-12-16 19:29:26', '2018-12-16 19:29:26'),
(30, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '[]', '2018-12-16 19:29:35', '2018-12-16 19:29:35'),
(31, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '[]', '2018-12-16 19:29:48', '2018-12-16 19:29:48'),
(32, 1, 'admin/manage/trademarks/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:29:56', '2018-12-16 19:29:56'),
(33, 1, 'admin/manage/trademarks/2/edit', 'GET', '127.0.0.1', '[]', '2018-12-16 19:30:40', '2018-12-16 19:30:40'),
(34, 1, 'admin/manage/trademarks/2/edit', 'GET', '127.0.0.1', '[]', '2018-12-16 19:30:48', '2018-12-16 19:30:48'),
(35, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:30:50', '2018-12-16 19:30:50'),
(36, 1, 'admin/manage/trademarks/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:30:52', '2018-12-16 19:30:52'),
(37, 1, 'admin/manage/trademarks/1/edit', 'GET', '127.0.0.1', '[]', '2018-12-16 19:31:17', '2018-12-16 19:31:17'),
(38, 1, 'admin/manage/trademarks/1/edit', 'GET', '127.0.0.1', '[]', '2018-12-16 19:31:30', '2018-12-16 19:31:30'),
(39, 1, 'admin/manage/trademarks/1/edit', 'GET', '127.0.0.1', '[]', '2018-12-16 19:34:33', '2018-12-16 19:34:33'),
(40, 1, 'admin/manage/trademarks/1', 'PUT', '127.0.0.1', '{\"case_number\":\"k13-5S5Us\",\"service\":\"Registration\",\"type\":\"Trademark\",\"name\":\"Test\",\"logo\":\"N\\/A\",\"summary\":\"Test\",\"number\":\"0\",\"filing_number\":\"0\",\"registration_number\":\"0\",\"international_registration_number\":\"0\",\"filing_date\":\"2018-12-17 03:34:33\",\"registration_date\":\"2018-12-17 03:34:33\",\"expiry_date\":\"2018-12-17 03:34:33\",\"classes\":\"1-43-\",\"office\":null,\"office_status\":null,\"status_description\":null,\"brand_name\":null,\"purpose\":null,\"value\":null,\"category\":null,\"hvt\":\"off\",\"revocation\":\"off\",\"priority_date\":\"2018-12-17 03:34:33\",\"priority_country\":null,\"priority_number\":\"0\",\"recently_filed\":\"no\",\"commerce\":\"no\",\"handler_name\":\"Roland\",\"atty_name\":null,\"_token\":\"q8hH1XK7q2ckFXs2flDwJwLB9vGvHnyc11grdcb8\",\"_method\":\"PUT\"}', '2018-12-16 19:35:03', '2018-12-16 19:35:03'),
(41, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '[]', '2018-12-16 19:35:03', '2018-12-16 19:35:03'),
(42, 1, 'admin/manage/trademarks/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:35:07', '2018-12-16 19:35:07'),
(43, 1, 'admin/manage/trademarks/1', 'PUT', '127.0.0.1', '{\"case_number\":\"k13-5S5U\",\"service\":\"Registration\",\"type\":\"Trademark\",\"name\":\"Test\",\"logo\":\"N\\/A\",\"summary\":\"Test\",\"number\":\"0\",\"filing_number\":\"0\",\"registration_number\":\"0\",\"international_registration_number\":\"0\",\"filing_date\":\"2018-12-17 03:34:33\",\"registration_date\":\"2018-12-17 03:34:33\",\"expiry_date\":\"2018-12-17 03:34:33\",\"classes\":\"1-43-\",\"office\":null,\"office_status\":null,\"status_description\":null,\"brand_name\":null,\"purpose\":null,\"value\":null,\"category\":null,\"hvt\":\"off\",\"revocation\":\"off\",\"priority_date\":\"2018-12-17 03:34:33\",\"priority_country\":null,\"priority_number\":\"0\",\"recently_filed\":\"no\",\"commerce\":\"no\",\"handler_name\":\"Roland\",\"atty_name\":null,\"_token\":\"q8hH1XK7q2ckFXs2flDwJwLB9vGvHnyc11grdcb8\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/manage\\/trademarks\"}', '2018-12-16 19:35:25', '2018-12-16 19:35:25'),
(44, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '[]', '2018-12-16 19:35:25', '2018-12-16 19:35:25'),
(45, 1, 'admin/manage/trademarks/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:35:37', '2018-12-16 19:35:37'),
(46, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:35:46', '2018-12-16 19:35:46'),
(47, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '[]', '2018-12-16 19:37:09', '2018-12-16 19:37:09'),
(48, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '{\"Case_number\":\"n13\",\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:37:15', '2018-12-16 19:37:15'),
(49, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '{\"Case_number\":\"n13\"}', '2018-12-16 19:40:07', '2018-12-16 19:40:07'),
(50, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '{\"Case_number\":\"5S\",\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:40:15', '2018-12-16 19:40:15'),
(51, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '{\"Case_number\":\"5S\"}', '2018-12-16 19:40:40', '2018-12-16 19:40:40'),
(52, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '[]', '2018-12-16 19:40:49', '2018-12-16 19:40:49'),
(53, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '{\"case_number\":\"gk\",\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:40:57', '2018-12-16 19:40:57'),
(54, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:41:11', '2018-12-16 19:41:11'),
(55, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '[]', '2018-12-16 19:41:11', '2018-12-16 19:41:11'),
(56, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '{\"case_number\":\"Gk\",\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:41:17', '2018-12-16 19:41:17'),
(57, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"case_number\":\"13\"}', '2018-12-16 19:41:24', '2018-12-16 19:41:24'),
(58, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '{\"case_number\":\"Gk\",\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:41:37', '2018-12-16 19:41:37'),
(59, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '{\"case_number\":\"Gk\"}', '2018-12-16 19:41:54', '2018-12-16 19:41:54'),
(60, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:41:59', '2018-12-16 19:41:59'),
(61, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"case_number\":null,\"name\":\"Th\"}', '2018-12-16 19:42:08', '2018-12-16 19:42:08'),
(62, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '{\"case_number\":null,\"name\":\"Th\"}', '2018-12-16 19:42:19', '2018-12-16 19:42:19'),
(63, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:42:25', '2018-12-16 19:42:25'),
(64, 1, 'admin/manage/trademarks/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:42:52', '2018-12-16 19:42:52'),
(65, 1, 'admin/manage/trademarks/1/edit', 'GET', '127.0.0.1', '[]', '2018-12-16 19:43:38', '2018-12-16 19:43:38'),
(66, 1, 'admin/manage/trademarks/1/edit', 'GET', '127.0.0.1', '[]', '2018-12-16 19:43:41', '2018-12-16 19:43:41'),
(67, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:43:45', '2018-12-16 19:43:45'),
(68, 1, 'admin/manage/trademarks/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:43:47', '2018-12-16 19:43:47'),
(69, 1, 'admin/manage/trademarks/1/edit', 'GET', '127.0.0.1', '[]', '2018-12-16 19:44:46', '2018-12-16 19:44:46'),
(70, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:45:54', '2018-12-16 19:45:54'),
(71, 1, 'admin/manage/trademarks/2', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:45:56', '2018-12-16 19:45:56'),
(72, 1, 'admin/manage/trademarks/2', 'GET', '127.0.0.1', '[]', '2018-12-16 19:53:11', '2018-12-16 19:53:11'),
(73, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:53:13', '2018-12-16 19:53:13'),
(74, 1, 'admin/manage/trademarks/1', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:53:20', '2018-12-16 19:53:20'),
(75, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:53:45', '2018-12-16 19:53:45'),
(76, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '[]', '2018-12-16 19:55:36', '2018-12-16 19:55:36'),
(77, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '[]', '2018-12-16 19:57:22', '2018-12-16 19:57:22'),
(78, 1, 'admin/manage/trademarks/1', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-16 19:57:29', '2018-12-16 19:57:29'),
(79, 1, 'admin/manage/trademarks/1', 'GET', '127.0.0.1', '[]', '2018-12-16 20:03:06', '2018-12-16 20:03:06'),
(80, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-16 20:03:34', '2018-12-16 20:03:34'),
(81, 1, 'admin/manage/trademarks/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-16 20:03:36', '2018-12-16 20:03:36'),
(82, 1, 'admin/manage/trademarks', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2018-12-16 20:03:44', '2018-12-16 20:03:44');

-- --------------------------------------------------------

--
-- Table structure for table `admin_permissions`
--

CREATE TABLE `admin_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `http_method` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `http_path` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_permissions`
--

INSERT INTO `admin_permissions` (`id`, `name`, `slug`, `http_method`, `http_path`, `created_at`, `updated_at`) VALUES
(1, 'All permission', '*', '', '*', NULL, NULL),
(2, 'Dashboard', 'dashboard', 'GET', '/', NULL, NULL),
(3, 'Login', 'auth.login', '', '/auth/login\r\n/auth/logout', NULL, NULL),
(4, 'User setting', 'auth.setting', 'GET,PUT', '/auth/setting', NULL, NULL),
(5, 'Auth management', 'auth.management', '', '/auth/roles\r\n/auth/permissions\r\n/auth/menu\r\n/auth/logs', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_roles`
--

CREATE TABLE `admin_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_roles`
--

INSERT INTO `admin_roles` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'administrator', '2018-12-16 19:19:37', '2018-12-16 19:19:37');

-- --------------------------------------------------------

--
-- Table structure for table `admin_role_menu`
--

CREATE TABLE `admin_role_menu` (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_role_menu`
--

INSERT INTO `admin_role_menu` (`role_id`, `menu_id`, `created_at`, `updated_at`) VALUES
(1, 2, NULL, NULL),
(1, 8, NULL, NULL),
(1, 9, NULL, NULL),
(1, 10, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_role_permissions`
--

CREATE TABLE `admin_role_permissions` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_role_permissions`
--

INSERT INTO `admin_role_permissions` (`role_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_role_users`
--

CREATE TABLE `admin_role_users` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_role_users`
--

INSERT INTO `admin_role_users` (`role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_users`
--

CREATE TABLE `admin_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_users`
--

INSERT INTO `admin_users` (`id`, `username`, `password`, `name`, `avatar`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2y$10$UO.YGeDqRFmpF0CVFUGPMuX.N9Lh4BmCiNQkkvMkd/qeWl02P/NcS', 'Administrator', NULL, NULL, '2018-12-16 19:19:37', '2018-12-16 19:19:37');

-- --------------------------------------------------------

--
-- Table structure for table `admin_user_permissions`
--

CREATE TABLE `admin_user_permissions` (
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `class_descriptions`
--

CREATE TABLE `class_descriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `trademark_class_id` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `class_descriptions`
--

INSERT INTO `class_descriptions` (`id`, `trademark_class_id`, `description`, `created_at`, `updated_at`) VALUES
(1, 1, 'combusting preparations [chemical additives to motor fuel]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(2, 1, 'adhesives for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(3, 1, 'salt for preserving, other than for foodstuffs', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(4, 1, 'auxiliary fluids for use with abrasives', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(5, 1, 'vulcanisation accelerators', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(6, 1, 'anti-frothing solutions for batteries', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(7, 1, 'acetates [chemicals]*', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(8, 1, 'acetate of cellulose, unprocessed', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(9, 1, 'bacteriological preparations for acetification', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(10, 1, 'acetic anhydride', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(11, 1, 'acetone', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(12, 1, 'acetylene', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(13, 1, 'acetylene tetrachloride', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(14, 1, 'acids*', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(15, 1, 'chemical condensation preparations', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(16, 1, 'acid proof chemical compositions', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(17, 1, 'finishing preparations for use in the manufacture of steel', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(18, 1, 'actinium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(19, 1, 'additives, chemical, to drilling muds', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(20, 1, 'chemical additives to drilling muds', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(21, 1, 'additives, chemical, to motor fuel', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(22, 1, 'chemical additives to motor fuel', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(23, 1, 'detergent additives to petrol [gasoline]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(24, 1, 'detergent additives to gasoline', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(25, 1, 'detergent additives to gasoline [petrol]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(26, 1, 'adhesive preparations for surgical bandages', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(27, 1, 'water-softening preparations', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(28, 1, 'gum tragacanth for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(29, 1, 'activated carbon', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(30, 1, 'activated charcoal', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(31, 1, 'propellant gases for aerosols', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(32, 1, 'reducing agents for use in photography', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(33, 1, 'adhesives for billposting', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(34, 1, 'agar-agar', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(35, 1, 'agglutinants for concrete', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(36, 1, 'agricultural chemicals, except fungicides, herbicides, insecticides and parasiticides', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(37, 1, 'compositions for repairing inner tubes of tires [tyres]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(38, 1, 'albumin [animal or vegetable, raw material]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(39, 1, 'iodised albumen', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(40, 1, 'malt albumen', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(41, 1, 'albumenized paper', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(42, 1, 'alkalies', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(43, 1, 'caustic alkali', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(44, 1, 'alkaline-earth metals', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(45, 1, 'alcohol*', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(46, 1, 'ethyl alcohol', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(47, 1, 'aldehydes*', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(48, 1, 'seaweeds [fertilizers]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(49, 1, 'chemical substances for preserving foodstuffs', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(50, 1, 'chemical preparations for facilitating the alloying of metalss', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(51, 1, 'alumina', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(52, 1, 'aluminium alum', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(53, 1, 'aluminium hydrate', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(54, 1, 'aluminium silicate', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(55, 1, 'aluminium chloride', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(56, 1, 'aluminium iodide', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(57, 1, 'alum', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(58, 1, 'soil conditioning preparations', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(59, 1, 'americium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(60, 1, 'starch for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(61, 1, 'starch-liquifying chemicals [ungluing agents]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(62, 1, 'sal ammoniac', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(63, 1, 'spirits of salt', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(64, 1, 'ammoniacal salts', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(65, 1, 'ungluing agents [chemical preparations for liquifying starch]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(66, 1, 'ammonia*', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(67, 1, 'ammonium aldehyde', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(68, 1, 'ammonia alum', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(69, 1, 'amyl acetate', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(70, 1, 'amyl alcohol', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(71, 1, 'anhydrous ammonial', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(72, 1, 'anhydrides', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(73, 1, 'animal charcoal', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(74, 1, 'animal albumen [raw material]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(75, 1, 'anthranilic acid', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(76, 1, 'anti-knock substances for internal combustion engines', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(77, 1, 'antifreeze', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(78, 1, 'anti-incrustants', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(79, 1, 'antimony', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(80, 1, 'antimony oxide', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(81, 1, 'antimony sulfide', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(82, 1, 'size for use in the textile industrye', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(83, 1, 'gum arabic for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(84, 1, 'glutinous tree-grafting preparations', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(85, 1, 'tree cavity fillers [forestry]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(86, 1, 'silver salt solutions for silvering', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(87, 1, 'argon', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(88, 1, 'lead arsenate', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(89, 1, 'arsenic', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(90, 1, 'arsenious acid', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(91, 1, 'astatine', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(92, 1, 'fuel for atomic piles', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(93, 1, 'engine-decarbonising chemicals', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(94, 1, 'chemical preparations for decarbonising engines', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(95, 1, 'self-toning paper [photography]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(96, 1, 'textile-brightening chemicals', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(97, 1, 'nitrogen', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(98, 1, 'nitrous oxide', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(99, 1, 'nitrogenous fertilizers', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(100, 1, 'nitric acid', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(101, 1, 'oenological bactericides [chemical preparations for use in wine making]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(102, 1, 'fixing baths [photography]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(103, 1, 'galvanizing baths', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(104, 1, 'soda ash', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(105, 1, 'barium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(106, 1, 'baryta', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(107, 1, 'baryta paper', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(108, 1, 'barium compounds', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(109, 1, 'substances for preventing runs in stockings', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(110, 1, 'bases [chemical preparations]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(111, 1, 'chemicals, except pigments, for the manufacture of enamel', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(112, 1, 'bauxite', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(113, 1, 'bentonite', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(114, 1, 'benzene-based acids', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(115, 1, 'benzene derivatives', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(116, 1, 'benzoic acid', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(117, 1, 'benzoic sulfimide', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(118, 1, 'saccharin', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(119, 1, 'berkelium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(120, 1, 'concrete-aeration chemicals', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(121, 1, 'concrete preservatives, except paints and oils', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(122, 1, 'bichloride of tin', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(123, 1, 'bichromate of potassium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(124, 1, 'bichromate of soda', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(125, 1, 'beer-clarifying and preserving agents', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(126, 1, 'biochemical catalysts', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(127, 1, 'potassium dioxalate', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(128, 1, 'manganese dioxide', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(129, 1, 'bismuth', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(130, 1, 'basic gallate of bismuth', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(131, 1, 'moistening [wetting] preparations for use in bleaching', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(132, 1, 'wetting preparations for use in bleaching', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(133, 1, 'wax-bleaching chemicals', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(134, 1, 'tan-wood', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(135, 1, 'wood alcohol', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(136, 1, 'wood pulp', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(137, 1, 'wood vinegar [pyroligneous acid]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(138, 1, 'pyroligneous acid [wood vinegar]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(139, 1, 'borax', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(140, 1, 'boric acid for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(141, 1, 'drilling muds', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(142, 1, 'brazing preparations', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(143, 1, 'condensation-preventing chemicals', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(144, 1, 'catechu', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(145, 1, 'kainite', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(146, 1, 'calcium cyanamide [fertilizer]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(147, 1, 'californium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(148, 1, 'plasticizers', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(149, 1, 'rubber preservatives', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(150, 1, 'carbonates', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(151, 1, 'magnesium carbonate', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(152, 1, 'carbon', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(153, 1, 'carbon disulfide', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(154, 1, 'carbonic acid', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(155, 1, 'carbide', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(156, 1, 'calcium carbide', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(157, 1, 'cassiopium [lutetium]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(158, 1, 'lutetium [cassiopium]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(159, 1, 'catalysts', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(160, 1, 'cellulose', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(161, 1, 'paper pulp', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(162, 1, 'viscose', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(163, 1, 'cement [metallurgy]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(164, 1, 'fermium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(165, 1, 'ceramic glazings', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(166, 1, 'cerium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(167, 1, 'salts from rare earth metals', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(168, 1, 'caesium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(169, 1, 'ketones', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(170, 1, 'animal carbon preparations', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(171, 1, 'carbon for filters', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(172, 1, 'bone charcoal', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(173, 1, 'blood charcoal', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(174, 1, 'coal saving preparations', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(175, 1, 'cement for footwear', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(176, 1, 'lime acetate', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(177, 1, 'lime carbonate', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(178, 1, 'lime chloride', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(179, 1, 'chimney cleaners, chemical', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(180, 1, 'leather-renovating chemicals', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(181, 1, 'industrial chemicals', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(182, 1, 'chemical preparations for scientific purposes, other than for medical or veterinary use', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(183, 1, 'chemical reagents, other than for medical or veterinary purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(184, 1, 'moderating materials for nuclear reactors', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(185, 1, 'fissionable chemical elements', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(186, 1, 'chemical substances for analyses in laboratories, other than for medical or veterinary purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(187, 1, 'chlorates', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(188, 1, 'chlorine', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(189, 1, 'hydrochlorates', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(190, 1, 'hydrochloric acid', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(191, 1, 'cholic acid', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(192, 1, 'chromates', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(193, 1, 'chrome alum', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(194, 1, 'chromium oxide', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(195, 1, 'chrome salts', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(196, 1, 'chromic acid', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(197, 1, 'mastic for leather', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(198, 1, 'mastic for tires [tyres]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(199, 1, 'cement for pneumatic tires [tyres]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(200, 1, 'mastic for tires', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(201, 1, 'cement for mending broken articles', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(202, 1, 'cement-waterproofing chemicals, except paints', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(203, 1, 'cement preservatives, except paints and oils', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(204, 1, 'fluids for hydraulic circuits', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(205, 1, 'liquids for hydraulic circuits', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(206, 1, 'grafting wax for trees', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(207, 1, 'citric acid for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(208, 1, 'must-fining preparations', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(209, 1, 'size for finishing and priming', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(210, 1, 'adhesives for paper hanging', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(211, 1, 'adhesives for wallpaper', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(212, 1, 'wine finings', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(213, 1, 'collodion*', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(214, 1, 'salts for coloring [colouring] metal', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(215, 1, 'salts for coloring metal', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(216, 1, 'bate for dressing skins', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(217, 1, 'dressing, except oils, for skins', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(218, 1, 'flower preservatives', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(219, 1, 'preservatives for use in the pharmaceutical industry', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(220, 1, 'chemical preparations for use in photography', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(221, 1, 'sensitized cloth for photography', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(222, 1, 'photosensitive plates', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(223, 1, 'corrosive preparations', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(224, 1, 'currying preparations for leather', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(225, 1, 'currying preparations for skins', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(226, 1, 'cream of tartar for chemical purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(227, 1, 'crotonic aldehyde', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(228, 1, 'cryogenic preparations', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(229, 1, 'leather gluess', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(230, 1, 'leather-dressing chemicals', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(231, 1, 'leather-impregnating chemicals', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(232, 1, 'blue vitriol', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(233, 1, 'copper sulfate [blue vitriol]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(234, 1, 'curium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(235, 1, 'solutions for cyanotyping', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(236, 1, 'cyanides [prussiates]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(237, 1, 'prussiates', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(238, 1, 'ferrocyanides', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(239, 1, 'cymene', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(240, 1, 'degreasing preparations for use in manufacturing processes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(241, 1, 'grease-removing preparations for use in manufacturing processes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(242, 1, 'separating and unsticking [ungluing] preparations', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(243, 1, 'oil-separating chemicals', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(244, 1, 'gum solvents', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(245, 1, 'degumming preparations', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(246, 1, 'defoliants', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(247, 1, 'mould-release preparations', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(248, 1, 'polish removing substances', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(249, 1, 'dehydrating preparations for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(250, 1, 'disincrustants', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(251, 1, 'detergents for use in manufacturing processes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(252, 1, 'dextrin [size]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(253, 1, 'diagnostic preparations, other than for medical or veterinary purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(254, 1, 'diastase for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(255, 1, 'diazo paper', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(256, 1, 'renovating preparations for phonograph records', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(257, 1, 'distilled water', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(258, 1, 'dolomite for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(259, 1, 'metal hardening preparations', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(260, 1, 'dysprosium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(261, 1, 'acidulated water for recharging batteries', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(262, 1, 'acidulated water for recharging accumulators', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(263, 1, 'glycerine for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(264, 1, 'heavy water', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(265, 1, 'clarification preparations', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(266, 1, 'purification preparations', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(267, 1, 'ion exchangers [chemicals]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(268, 1, 'flashlight preparations', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(269, 1, 'fuel-saving preparations', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(270, 1, 'reagent paper, other than for medical or veterinary purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(271, 1, 'antistatic preparations, other than for household purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(272, 1, 'salts for galvanic cells', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(273, 1, 'opacifiers for enamel', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(274, 1, 'opacifiers for glass', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(275, 1, 'enamel-staining chemicals', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(276, 1, 'photographic emulsions', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(277, 1, 'emulsifiers', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(278, 1, 'sensitized photographic plates', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(279, 1, 'sizing preparations', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(280, 1, 'fertilizers', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(281, 1, 'enzyme preparations for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(282, 1, 'enzymes for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(283, 1, 'epoxy resins, unprocessed', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(284, 1, 'gas purifying preparations', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(285, 1, 'erbium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(286, 1, 'spirits of vinegar [dilute acetic acid]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(287, 1, 'test paper, chemical', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(288, 1, 'esters*', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(289, 1, 'ethane', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(290, 1, 'ethers*', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(291, 1, 'ethyl ether', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(292, 1, 'glycol ether', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(293, 1, 'methyl ether', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(294, 1, 'sulfuric ether', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(295, 1, 'stain-preventing chemicals for use on fabrics', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(296, 1, 'europium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(297, 1, 'fire extinguishing compositions', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(298, 1, 'flour for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(299, 1, 'iron salts', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(300, 1, 'ferments for chemical purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(301, 1, 'ferrotype plates [photography]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(302, 1, 'fertilizing preparations', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(303, 1, 'fireproofing preparations', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(304, 1, 'compositions for threading', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(305, 1, 'filtering preparations for the beverages industry', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(306, 1, 'fissionable material for nuclear energy', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(307, 1, 'fixing solutions [photography]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(308, 1, 'flowers of sulfur for chemical purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(309, 1, 'limestone hardening substances', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(310, 1, 'fluorine', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(311, 1, 'fluorspar compounds', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(312, 1, 'hydrofluoric acid', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(313, 1, 'graphite for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(314, 1, 'foundry binding substances', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(315, 1, 'foundry molding [moulding] preparations', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(316, 1, 'additives, chemical, to insecticides', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(317, 1, 'additives, chemical, to fungicides', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(318, 1, 'formic acid', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(319, 1, 'formic aldehyde for chemical purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(320, 1, 'fulling preparations for use in the textile industry', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(321, 1, 'fulling preparations', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(322, 1, 'francium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(323, 1, 'brake fluid', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(324, 1, 'lamp black for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(325, 1, 'chemical preparations for smoking meat', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(326, 1, 'gadolinium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(327, 1, 'gallnuts', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(328, 1, 'gallic acid for the manufacture of ink', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(329, 1, 'gallium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(330, 1, 'photographic paper', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(331, 1, 'gallotannic acid', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(332, 1, 'galvanizing preparations', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(333, 1, 'gambier', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(334, 1, 'protective gases for welding', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(335, 1, 'solidified gases for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(336, 1, 'gelatine for photographic purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(337, 1, 'gelatine for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(338, 1, 'rock salt', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(339, 1, 'getters [chemically active substances]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(340, 1, 'dry ice [carbon dioxide]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(341, 1, 'birdlime', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(342, 1, 'glucosides', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(343, 1, 'glycerides', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(344, 1, 'glycol', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(345, 1, 'fat-bleaching chemicals', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(346, 1, 'fatty acids', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(347, 1, 'grafting mastic for trees', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(348, 1, 'guano', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(349, 1, 'balm of gurjun [gurjon, gurjan] for making varnish', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(350, 1, 'helium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(351, 1, 'holmium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(352, 1, 'hormones for hastening the ripening of fruit', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(353, 1, 'horticultural chemicals, except fungicides, herbicides, insecticides and parasiticides', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(354, 1, 'oils for the preservation of food', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(355, 1, 'oils for preparing leather in the course of manufacture', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(356, 1, 'oils for tanning leather', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(357, 1, 'petroleum dispersants', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(358, 1, 'oil dispersants', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(359, 1, 'oil-bleaching chemicals', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(360, 1, 'oil-purifying chemicals', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(361, 1, 'humus', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(362, 1, 'hydrates', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(363, 1, 'carbonic hydrates', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(364, 1, 'hydrazine', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(365, 1, 'hydrogen', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(366, 1, 'hypochlorite of soda', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(367, 1, 'hyposulfites', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(368, 1, 'textile-waterproofing chemicals', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(369, 1, 'textile-impregnating chemicals', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(370, 1, 'leather-waterproofing chemicals', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(371, 1, 'iodine for chemical purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(372, 1, 'iodised salts', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(373, 1, 'iodic acid', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(374, 1, 'iodine for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(375, 1, 'isotopes for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(376, 1, 'kaolin', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(377, 1, 'china clay', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(378, 1, 'china slip', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(379, 1, 'kieselgur', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(380, 1, 'krypton', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(381, 1, 'lactic acid', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(382, 1, 'milk ferments for chemical purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(383, 1, 'lanthanum', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(384, 1, 'preparations for preventing the tarnishing of lenses', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(385, 1, 'lithia [lithium oxide]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(386, 1, 'lithium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(387, 1, 'masonry preservatives, except paints and oils', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(388, 1, 'brickwork preservatives, except paints and oils', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(389, 1, 'magnesite', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(390, 1, 'magnesium chloride', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(391, 1, 'manganate', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(392, 1, 'mangrove bark for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(393, 1, 'glass-frosting chemicals', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(394, 1, 'mercury', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(395, 1, 'mercury salts', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(396, 1, 'mercuric oxide', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(397, 1, 'metalloids', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(398, 1, 'salts of precious metals for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(399, 1, 'metal annealing preparations', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(400, 1, 'methane', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(401, 1, 'chemical preparations to prevent mildew', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(402, 1, 'mineral acids', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(403, 1, 'salts for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(404, 1, 'moistening [wetting] preparations for use in dyeing', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(405, 1, 'wetting preparations for use in dyeing', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(406, 1, 'naphthalene', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(407, 1, 'neodymium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(408, 1, 'neon', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(409, 1, 'neptunium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(410, 1, 'toxic gas neutralizers', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(411, 1, 'chemical preparations for protection against wheat blight [smut]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(412, 1, 'uranium nitrate', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(413, 1, 'sensitized plates for offset printing', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(414, 1, 'oleic acid', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(415, 1, 'olivine [silicate mineral]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(416, 1, 'gold salts', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(417, 1, 'sorrel salt', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(418, 1, 'oxalates', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(419, 1, 'oxalic acid', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(420, 1, 'oxygen', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(421, 1, 'hydrogen peroxide for industrial purpose', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(422, 1, 'palladium chloride', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(423, 1, 'nitrate paper', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(424, 1, 'photometric paper', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(425, 1, 'sensitized paper', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(426, 1, 'litmus paper', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(427, 1, 'pectin [photography]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(428, 1, 'perborate of soda', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(429, 1, 'percarbonates', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(430, 1, 'perchlorates', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(431, 1, 'persulfates', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(432, 1, 'persulfuric acid', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(433, 1, 'phenol for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(434, 1, 'phosphates [fertilizers]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(435, 1, 'slag [fertilizers]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(436, 1, 'phosphatides', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(437, 1, 'phosphorus', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(438, 1, 'superphosphates [fertilizers]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(439, 1, 'blueprint paper', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(440, 1, 'phosphoric acid', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(441, 1, 'blueprint cloth', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(442, 1, 'photographic developers', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(443, 1, 'photographic sensitizers', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(444, 1, 'picric acid', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(445, 1, 'plastics, unprocessed', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(446, 1, 'plastisols', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(447, 1, 'lead acetate', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(448, 1, 'lead oxide', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(449, 1, 'plutonium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(450, 1, 'polonium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(451, 1, 'potato flour for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(452, 1, 'peat pots for horticulture', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(453, 1, 'potash', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(454, 1, 'potassium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(455, 1, 'potash water', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(456, 1, 'praseodymium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(457, 1, 'promethium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(458, 1, 'protactinium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(459, 1, 'protein [raw material]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(460, 1, 'pyrogallic acid', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(461, 1, 'quebracho for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(462, 1, 'synthetic resins, unprocessed', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(463, 1, 'artificial resins, unprocessed', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(464, 1, 'radioactive elements for scientific purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(465, 1, 'radon', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(466, 1, 'radium for scientific purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(467, 1, 'refrigerants', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(468, 1, 'by-products of the processing of cereals for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(469, 1, 'acrylic resins, unprocessed', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(470, 1, 'rhenium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(471, 1, 'X-ray films, sensitized but not exposed', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(472, 1, 'compositions for repairing tires [tyres]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(473, 1, 'tire repairing compositions', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(474, 1, 'rubidium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(475, 1, 'foundry sand', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(476, 1, 'salicylic acid', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(477, 1, 'saltpeter', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(478, 1, 'samarium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(479, 1, 'sauce for preparing tobacco', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(480, 1, 'soap [metallic] for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(481, 1, 'scandium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(482, 1, 'salts [chemical preparations]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(483, 1, 'salt, raw', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(484, 1, 'chromic salts', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(485, 1, 'salts [fertilizers]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(486, 1, 'selenium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(487, 1, 'seed preserving substances', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(488, 1, 'silicates', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(489, 1, 'silicon', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(490, 1, 'silicones', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(491, 1, 'sodium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(492, 1, 'sulfides', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(493, 1, 'soldering chemicals', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(494, 1, 'welding chemicals', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(495, 1, 'calcined soda', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(496, 1, 'caustics for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(497, 1, 'caustic soda for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(498, 1, 'sodium salts [chemical compounds]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(499, 1, 'sulfur', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(500, 1, 'bismuth subnitrate for chemical purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(501, 1, 'barytes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(502, 1, 'spinel [oxide mineral]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(503, 1, 'stearic acid', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(504, 1, 'strontium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(505, 1, 'soot for industrial or agricultural purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(506, 1, 'liquids for removing sulfates from batteries', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(507, 1, 'sulfonic acids', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(508, 1, 'sulfurous acid', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(509, 1, 'sulfuric acid', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(510, 1, 'sumac for use in tanning', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(511, 1, 'chemicals for use in forestry, except fungicides, herbicides, insecticides and parasiticides', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(512, 1, 'talc [magnesium silicate]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(513, 1, 'tan', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(514, 1, 'tannin', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(515, 1, 'tanning substances', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(516, 1, 'calcium salts', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(517, 1, 'tannic acid', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(518, 1, 'tapioca flour for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(519, 1, 'tartar, other than for pharmaceutical purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(520, 1, 'tartaric acid', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(521, 1, 'technetium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(522, 1, 'tellurium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(523, 1, 'tensio-active agents', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(524, 1, 'surface-active chemical agents', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(525, 1, 'terbium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(526, 1, 'glass-staining chemicals', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(527, 1, 'preparations for preventing the tarnishing of glass', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(528, 1, 'anti-tarnishing chemicals for windows', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(529, 1, 'soil for growing', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(530, 1, 'fuller\'s earth for use in the textile industry', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(531, 1, 'rare earths', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(532, 1, 'loam', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(533, 1, 'carbon tetrachloride', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(534, 1, 'tetrachlorides', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(535, 1, 'moistening [wetting] preparations for use in the textile industry', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(536, 1, 'wetting preparations for use in the textile industry', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(537, 1, 'thallium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(538, 1, 'thiocarbanilide', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(539, 1, 'thulium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(540, 1, 'thorium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(541, 1, 'titanite', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(542, 1, 'toluol', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(543, 1, 'toluene', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(544, 1, 'peat [fertilizer]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(545, 1, 'preservatives for tiles, except paints and oils', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(546, 1, 'tungstic acid', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(547, 1, 'uranium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(548, 1, 'uranium oxide', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(549, 1, 'water glass [soluble glass]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(550, 1, 'meat tenderizers for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(551, 1, 'vine disease preventing chemicals', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(552, 1, 'vinic alcohol', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(553, 1, 'toning salts [photography]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(554, 1, 'vulcanising preparations', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(555, 1, 'witherite', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(556, 1, 'xenon', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(557, 1, 'ytterbium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(558, 1, 'yttrium', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(559, 1, 'chlorides', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(560, 1, 'sulfates', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(561, 1, 'zirconia', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(562, 1, 'preparations for stimulating cooking for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(563, 1, 'ammonia [volatile alkali] for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(564, 1, 'volatile alkali [ammonia] for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(565, 1, 'alkaline iodides for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(566, 1, 'alkaline metals', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(567, 1, 'salts of alkaline metals', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(568, 1, 'alkaloids*', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(569, 1, 'alginates for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(570, 1, 'aluminium acetate*', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(571, 1, 'starch paste [adhesive], other than for stationery or household purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(572, 1, 'ammonium salts', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(573, 1, 'animal carbon', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(574, 1, 'silver nitrate', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(575, 1, 'color- [colour-] brightening chemicals for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(576, 1, 'anti-sprouting preparations for vegetables', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(577, 1, 'nitrates', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(578, 1, 'adhesives for wall tiles', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(579, 1, 'barium sulfate', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(580, 1, 'chemicals for the manufacture of paints', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(581, 1, 'methyl benzol', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(582, 1, 'methyl benzene', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(583, 1, 'bicarbonate of soda for chemical purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(584, 1, 'biological preparations, other than for medical or veterinary purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(585, 1, 'decolorants for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(586, 1, 'bleaching preparations [decolorants] for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(587, 1, 'sensitized films, unexposed', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(588, 1, 'preparations of the distillation of wood alcohol', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(589, 1, 'brazing fluxes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(590, 1, 'soldering fluxes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(591, 1, 'bromine for chemical purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(592, 1, 'carbolineum for the protection of plants', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(593, 1, 'flocculants', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(594, 1, 'lecithin [raw material]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(595, 1, 'substrates for soil-free growing [agriculture]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(596, 1, 'cellulose esters for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(597, 1, 'casein for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(598, 1, 'cellulose derivatives [chemicals]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(599, 1, 'cellulose ethers for industrial purposes]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(600, 1, 'bacterial preparations, other than for medical and veterinary use', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(601, 1, 'bacteriological preparations, other than for medical and veterinary use', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(602, 1, 'cultures of microorganisms, other than for medical and veterinary use', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(603, 1, 'carbon black for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(604, 1, 'cinematographic film, sensitized but not exposed', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(605, 1, 'cobalt oxide for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(606, 1, 'glue for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(607, 1, 'oils for currying leather', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(608, 1, 'creosote for chemical purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(609, 1, 'compositions for the manufacture of phonograph records', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(610, 1, 'preparations for the separation of greases', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(611, 1, 'dispersions of plastics', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(612, 1, 'solvents for varnishes', '2018-12-13 09:33:20', '2018-12-13 09:33:20');
INSERT INTO `class_descriptions` (`id`, `trademark_class_id`, `description`, `created_at`, `updated_at`) VALUES
(613, 1, 'artificial sweeteners [chemical preparations]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(614, 1, 'water-purifying chemical', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(615, 1, 'emollients for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(616, 1, 'filtering materials [unprocessed plastics]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(617, 1, 'filtering materials [chemical preparations]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(618, 1, 'filtering materials [mineral substances]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(619, 1, 'filtering materials [vegetable substances]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(620, 1, 'glucose for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(621, 1, 'gluten [glue], other than for stationery or household purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(622, 1, 'gums [adhesives] for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(623, 1, 'damp-proofing chemicals, except paints, for masonry', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(624, 1, 'isinglass, other than for stationery, household or alimentary purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(625, 1, 'beer preserving agents', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(626, 1, 'synthetic materials for absorbing oil', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(627, 1, 'ceramic materials in particulate form, for use as filtering media', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(628, 1, 'compost', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(629, 1, 'compositions for the manufacture of technical ceramics', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(630, 1, 'diatomaceous earth', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(631, 1, 'mordants for metals', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(632, 1, 'plant growth regulating preparations', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(633, 1, 'descaling preparations, other than for household purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(634, 1, 'seawater for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(635, 1, 'preparations of trace elements for plants', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(636, 1, 'camphor, for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(637, 1, 'chemical intensifiers for paper', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(638, 1, 'chemical intensifiers for rubber', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(639, 1, 'humus top dressing', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(640, 1, 'magnetic fluid for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(641, 1, 'power steering fluid', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(642, 1, 'transmission fluid', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(643, 1, 'anti-boil preparations for engine coolants', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(644, 1, 'ceramic compositions for sintering [granules and powders]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(645, 1, 'coolants for vehicle engines', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(646, 1, 'radiator flushing chemicals', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(647, 1, 'automobile body fillers', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(648, 1, 'car body fillers', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(649, 1, 'electrophoresis gels', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(650, 1, 'glaziers\' putty', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(651, 1, 'expanded clay for hydroponic plant growing [substrate]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(652, 1, 'wallpaper removing preparations', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(653, 1, 'chemical additives for oils', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(654, 1, 'oil cement [putty]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(655, 1, 'genes of seeds for agricultural production', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(656, 1, 'stem cells, other than for medical or veterinary purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(657, 1, 'biological tissue cultures, other than for medical or veterinary purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(658, 1, 'fish meal fertilizers', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(659, 1, 'enzyme preparations for the food industry', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(660, 1, 'enzymes for the food industry', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(661, 1, 'glucose for the food industry', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(662, 1, 'lecithin for the food industry', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(663, 1, 'lecithin for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(664, 1, 'pectin for the food industry', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(665, 1, 'pectin for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(666, 1, 'cream of tartar for the food industry', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(667, 1, 'cream of tartar for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(668, 1, 'alginates for the food industry', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(669, 1, 'gluten for the food industry', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(670, 1, 'gluten for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(671, 1, 'lactose for the food industry', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(672, 1, 'lactose for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(673, 1, 'lactose [raw material]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(674, 1, 'milk ferments for the food industry', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(675, 1, 'milk ferments for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(676, 1, 'casein for the food industry', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(677, 1, 'sal ammoniac spirits', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(678, 1, 'chemicals for the manufacture of pigments', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(679, 1, 'preparations of microorganisms, other than for medical and veterinary use', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(680, 1, 'potting soil', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(681, 1, 'metal tempering preparations', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(682, 1, 'glutamic acid for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(683, 1, 'tea extracts for use in the manufacture of pharmaceuticals', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(684, 1, 'tea extracts for the food industry', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(685, 1, 'organic digestate [fertilizer]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(686, 1, 'tea extracts for use in the manufacture of cosmetics', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(687, 1, 'collagen for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(688, 1, 'silicon carbide [raw material]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(689, 1, 'adjuvants, other than for medical or veterinary purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(690, 1, 'vitamins for use in the manufacture of food supplements', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(691, 1, 'vitamins for the food industry', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(692, 1, 'antioxidants for use in manufacture', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(693, 1, 'antioxidants for use in the manufacture of pharmaceuticals', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(694, 1, 'antioxidants for use in the manufacture of food supplements', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(695, 1, 'proteins for use in manufacture', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(696, 1, 'proteins for use in the manufacture of food supplements', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(697, 1, 'proteins for the food industry', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(698, 1, 'ammonium nitrate', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(699, 1, 'vitamins for use in the manufacture of pharmaceuticals', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(700, 1, 'vitamins for use in the manufacture of cosmetics', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(701, 1, 'flavonoids for industrial purposes [phenolic compounds]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(702, 1, 'thymol for industrial purposes', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(703, 1, 'topsoil', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(704, 1, 'transmission oil', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(705, 1, 'polymer resins, unprocessed', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(706, 1, 'chemical coatings for ophthalmic lenses', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(707, 1, 'calomel [mercurous chloride]', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(708, 1, 'animal manure', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(709, 1, 'xylol', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(710, 1, 'xylene', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(711, 1, 'benzene', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(712, 1, 'benzol', '2018-12-13 09:33:20', '2018-12-13 09:33:20');

-- --------------------------------------------------------

--
-- Table structure for table `contact_informations`
--

CREATE TABLE `contact_informations` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nature` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `province` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `street` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `house` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zip_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact_informations`
--

INSERT INTO `contact_informations` (`id`, `user_id`, `name`, `nature`, `country`, `contact`, `email`, `city`, `province`, `street`, `house`, `zip_code`, `phone_number`, `fax`, `created_at`, `updated_at`) VALUES
(1, 1, 'Bonn', 'individual', 'AF', 'Jenny', 'jenny@gmail.com', 'cebu', 'cebu', 'Cebu', 'cebu', '123', '123', NULL, '2018-12-13 09:35:31', '2018-12-13 09:35:31'),
(2, 1, 'Era', 'company', 'AW', 'Jun', 'jun@gmail.com', 'cebu', 'cebu', 'Cebu', 'cebu', '12345', '123', NULL, '2018-12-13 09:35:31', '2018-12-13 09:35:31');

-- --------------------------------------------------------

--
-- Table structure for table `continents`
--

CREATE TABLE `continents` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `continents`
--

INSERT INTO `continents` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Middle East', '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(2, 'Asia', '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(3, 'North America', '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(4, 'Ocenia', '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(5, 'Africa', '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(6, 'Europe', '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(7, 'South America', '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(8, 'Central America', '2018-12-13 09:33:19', '2018-12-13 09:33:19');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `continent_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abbr` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_feature` tinyint(1) NOT NULL,
  `agency_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `agency_short` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `opposition_period` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `registration_period` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `madrid_protocol` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `continent_id`, `name`, `abbr`, `avatar`, `is_feature`, `agency_name`, `agency_short`, `opposition_period`, `registration_period`, `madrid_protocol`, `created_at`, `updated_at`) VALUES
(1, 2, 'Bangladesh', 'BD', 'countries/bd.png', 0, 'Department of Patents Designs and Trademarks Ministry of Industries', 'DPDT', '2 months', '15 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(2, 2, 'Bermuda', 'BM', 'countries/bm.png', 0, 'Intellectual Property Office of Bermuda', 'IPO', '2 months', '12 to 18 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(3, 2, 'Bhutan', 'BT', 'countries/bt.png', 0, 'Industrial Property Division  Ministry of Economic Affairs', 'IPD', '3 months', 'within 9 to 12 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(4, 2, 'Brunei Darussalam', 'BN', 'countries/bn.png', 0, ' Brunei Darussalam Intellectual Property Office Ministry of Energy, Manpower and Industry', ' BruIPO', '3 months', 'within 1-2 years', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(5, 2, 'Cambodia', 'KH', 'countries/kh.png', 0, 'Department of Industrial Property, Ministry of Industry and Handicraft', 'DIP', '3 months', '9 months to 1 year', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(6, 2, 'China', 'CN', 'countries/cn.png', 1, 'National Intellectual Property Administration, PRC', ' NCAC', '3 months', '15 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(7, 2, 'Hong Kong', 'HK', 'countries/hk.png', 1, 'Intellectual Property Department', 'IPD', '3 months', '9 to 12 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(8, 2, 'India', 'IN', 'countries/in.png', 0, 'Controller General of Patents Designs & Trade Marks', 'Controller General of Patents Designs & Trade Marks', '4 months', '12 to 14 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(9, 2, 'Indonesia', 'IN', 'countries/id.png', 0, 'Directorate General of Intellectual Property Rights Ministry of Law and Human Rights', 'DGIP', '3 months', '1 year and 6 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(10, 2, 'Iran', 'IR', 'countries/ir.png', 0, 'Industrial Property Office', 'Industrial Property Office', '1 month', '8 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(11, 2, 'Japan', 'JP', 'countries/jp.png', 0, 'International Affairs Division General Affairs Department Japan Patent Office', 'JPO', '2 months', '7-8 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(12, 2, 'South Korea', 'KR', 'countries/kr.png', 0, 'Korean Intellectual Property Office', 'KIPO', '2 months', '9-12 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(13, 2, 'Kyrgyzstan', 'KG', 'countries/kg.png', 0, 'Kyrgyzpatent', 'Kyrgyzpatent', '?', '6-14 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(14, 2, 'Macau', 'MO', 'countries/mo.png', 0, 'Macau Economic Services - Intellectual Property Department', 'Macau Economic Services - Intellectual Property Department', '2 months', '6-8 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(15, 2, 'Malaysia', 'MY', 'countries/my.png', 0, 'Intellectual Property Corporation of Malaysia', 'MyIPO', '2 months', '9 to 12 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(16, 2, 'Maldives', 'MV', 'countries/mv.png', 0, 'Intellectual Property Unit Ministry of Economic Development', 'Intellectual Property Unit Ministry of Economic Development', '?', '3 to 4 weeks', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(17, 2, 'Mongolia', 'MN', 'countries/mn.png', 0, 'Intellectual Property Office Implementing Agency of the Government of Mongolia', 'IPOM', '1 year', '6-9 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(18, 2, 'Myanmar', 'MM', 'countries/mm.png', 0, 'Department of Technical and Vocational Education Ministry of Science and Technology', 'Department of Technical and Vocational Education Ministry of Science and Technology', '?', '3 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(19, 2, 'Nepal', 'NP', 'countries/mm.png', 0, 'Nepal Copyright Registrar\'s Office Ministry of Culture, Tourism and Civil Aviation', 'Department of Technical and Vocational Education Ministry of Science and Technology', '?', '3 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(20, 2, 'Pakistan', 'PK', 'countries/pk.png', 0, 'Intellectual Property Organization of Pakistan (IPO-Pakistan)', 'IPO-Pakistan', '2 months', '9 to 12 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(21, 2, 'Philippines', 'PH', 'countries/ph.png', 1, 'Intellectual Property Office of the Philippines', 'IPOPHL', '30 days', '3 to 6 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(22, 2, 'Singapore', 'SG', 'countries/sg.png', 1, 'Intellectual Property Office of Singapore', 'IPOS', '2 to 4 months', '6 to 12 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(23, 2, 'Sri Lanka', 'LK', 'countries/lk.png', 0, 'National Intellectual Property Office of Sri Lanka', 'National Intellectual Property Office of Sri Lanka', '3 months', '8 years', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(24, 2, 'Taiwan', 'TW', 'countries/tw.png', 1, 'Taiwan Intellectual Property Office', 'Taiwan Intellectual Property Office', '3 months', '10 to 12 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(25, 2, 'Tajikistan', 'TJ', 'countries/tj.png', 0, 'Agency of Copyright and Related Rights, Ministry of Culture', 'Agency of Copyright and Related Rights, Ministry of Culture', 'no opposition period', '16 to 25 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(26, 2, 'Thailand', 'TH', 'countries/th.png', 0, 'Agency of Copyright and Related Rights', 'Agency of Copyright and Related Rights', '2 months', '12 to 18 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(27, 2, 'East Timor', 'TL', 'countries/tl.png', 0, 'Intellectual Property Office of East Timor', 'IPO', '1 month', '8 to 12 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(28, 2, 'Turkmenistan', 'TM', 'countries/tm.png', 0, 'State Service on Intellectual Property of Ministry of Finance and Economy of Turkmenistan', 'State Service on Intellectual Property of Ministry of Finance and Economy of Turkmenistan', '?', '26 to 29 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(29, 2, 'Vietnam', 'VN', 'countries/vn.png', 0, 'Intellectual Property Office of Viet Nam', 'Intellectual Property Office of Viet Nam', '12 months', '12 to 15 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(30, 1, 'Afghanistan', 'AF', 'countries/af.png', 0, 'Afghanistan Central Business Registry and Intellectual Property ', 'Afghanistan Central Business Registry and Intellectual Property ', '1 month', '5 to 6 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(31, 1, 'Bahrain', 'BH', 'countries/bh.png', 0, 'Industrial Property Directorate', 'Industrial Property Directorate', '2 months', '5 to 7 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(32, 1, 'Iraq', 'IQ', 'countries/iq.png', 0, 'Industrial Property Department ', 'Industrial Property Department ', '3 months', '1.5 to 2 years', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(33, 1, 'Israel', 'IL', 'countries/il.png', 0, 'Israel Intellectual Property Office', 'Israel Intellectual Property Office', '3 months', '17 to 21 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(34, 1, 'Jordan', 'JO', 'countries/jo.png', 0, 'Industrial Property Protection Directorate', 'Industrial Property Protection Directorate', '3 months', '9 to 11 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(35, 1, 'Kuwait', 'KW', 'countries/kw.png', 0, 'Ministry of Trade and Industry Trademarks and Patents Department', 'Ministry of Trade and Industry Trademarks and Patents Department', '2 months', '8 to 10 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(36, 1, 'Lebanon', 'LB', 'countries/lb.png', 0, 'Office of Intellectual Property of Lebanon', 'Office of Intellectual Property of Lebanon', 'no opposition period', '4 to 6 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(37, 1, 'Oman', 'OM', 'countries/om.png', 0, 'Intellectual Property Department Ministry of Commerce and Industry', 'Intellectual Property Department Ministry of Commerce and Industry', '2 months', '10 to 12  months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(38, 1, 'Palestine', 'PS', 'countries/ps.png', 0, 'Palestine Intellectual Property Office', 'IPO', '?', '?', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(39, 1, 'Qatar', 'QA', 'countries/qa.png', 0, 'Trademarks, Industrial Designs, Geographical Indication, Commercial Registration and Licensing', 'Intellectual Property Department', '4 months', '8 to 12 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(40, 1, 'Saudi Arabia', 'SA', 'countries/sa.png', 0, 'General Administration of Trademarks Ministry of Commerce and Investment', 'General Administration of Trademarks Ministry of Commerce and Investment', '2 months', '3 to 5 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(41, 1, 'Turkey', 'TR', 'countries/tr.png', 0, 'Turkish Patent and Trademark Office', 'Turkish Patent and Trademark Office', '2 months', '4 to 10 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(42, 1, 'United Arab Emirates', 'AE', 'countries/ae.png', 0, 'Trademark Department of UAE', 'Trademark Department of UAE', '1 month', '10 to 12 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(43, 1, 'Yemen', 'YE', 'countries/ye.png', 0, 'Ministry of Industry and Trade, General Department for Intellectual Property Protection', 'Ministry of Industry and Trade, General Department for Intellectual Property Protection', '3 months', '7 to 9 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(44, 1, 'Kurdistan Region', 'krd', 'countries/krd.png', 0, 'Intellectual Property Office', 'IPO', '1 month', '8 to 12 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(45, 6, 'Albania', 'AL', 'countries/al.png', 0, 'General Directorate of Industrial Property', 'GDIP ', '3 months', '6 to 9 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(46, 6, 'Andorra', 'AD', 'countries/ad.png', 0, 'Trademarks Office of the Principality of Andorra', 'Trademarks Office of the Principality of Andorra', '?', '3 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(47, 6, 'Armenia', 'AM', 'countries/am.png', 0, 'Intellectual Property Agency of the Republic of Armenia, Ministry of Economic           Development and Investments', 'Intellectual Property Agency of the Republic of Armenia, Ministry of Economic           Development and Investments', '2 months', '10 to 12 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(48, 6, 'Austria', 'AT', 'countries/at.png', 0, 'Intellectual Property Agency of the Republic of Armenia, Ministry of Economic           Development and Investments', 'OPA', '2 to 3 months', '3 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(49, 6, 'Azerbaijan', 'AZ', 'countries/az.png', 0, 'Intellectual Property Agency of the Republic of Azerbaijan', 'IPOA', '2 months', '26 to 29 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(50, 6, 'Belarus', 'BY', 'countries/by.png', 0, 'National Center of Intellectual Property', 'NCIP', '?', '20 to 24 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(51, 6, 'Belgium', 'BE', 'countries/be.png', 0, '', '', '?', '', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(52, 6, 'Benelux', 'BX', 'countries/bx.png', 0, 'Benelux Office for Intellectual Property', 'BOIP', '2 months', '3 to 4 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(53, 6, 'Bosnia and Herzegovina', 'BA', 'countries/ba.png', 0, 'Institute for Intellectual Property of Bosnia and Herzegovina', 'IIP-BIH', '3 months', '1 to 2 years', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(54, 6, 'Bulgaria', 'BG', 'countries/bg.png', 0, 'Patent Office of the Republic of Bulgaria', 'BPO', '3 months', '6 to 9 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(55, 6, 'Croatia', 'HR', 'countries/hr.png', 0, 'State Intellectual Property Office of the Republic of Croatia', 'DZIV', '3 months', '9 to 12 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(56, 6, 'Cyprus', 'CY', 'countries/cy.png', 0, 'Department of Registrar of Companies and Official Receiver', 'DRCOR', '2 months', '6 to 9 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(57, 6, 'Czech Republic', 'CZ', 'countries/cz.png', 0, 'Intellectual Property Office', 'IPOCZ', '2 months', '6 to 9 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(58, 6, 'Denmark', 'DK', 'countries/dk.png', 0, 'Danish Patent and Trademark Office', 'DKPTO', '2 months', '3 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(59, 6, 'Estonia', 'EE', 'countries/ee.png', 0, 'The Estonian Patent Office', 'EPA', '2 months', '11 to 13 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(60, 6, 'European Union', 'EM', 'countries/em.png', 1, 'European Union Intellectual Property Office', 'EUIPO', '3 months', '7 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(61, 6, 'Finland', 'FI', 'countries/fi.png', 0, 'Finnish Patent and Registration Office', 'PRH', '2 months', '4 to 5 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(62, 6, 'France', 'FR', 'countries/fr.png', 0, 'National Institute of Industrial Property', 'I.N.P.I', '2 months', '4 to 6 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(63, 6, 'Georgia', 'GE', 'countries/ge.png', 0, 'National Intellectual Property Center (Sakpatenti)', 'NIPCG', '3 months', '14 to 17 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(64, 6, 'Germany', 'DE', 'countries/de.png', 0, 'German Patent and Trade Mark Office', 'DPMA', '3 months', '6-9 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(65, 6, 'Gibraltar', 'GI', 'countries/gi.png', 0, 'Intellectual Property Office', 'IPO', '?', '?', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(66, 6, 'Greece', 'GR', 'countries/gr.png', 0, 'Industrial Property Organization', 'GGE', '3 months', '6 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(67, 6, 'Guernsey', 'GG', 'countries/gg.png', 0, 'Intellectual Property Office of Guernsey', 'IPO', '?', '?', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(68, 6, 'Hungary', 'HU', 'countries/hu.png', 0, 'Hungarian Intellectual Property Office', 'HIPO', '3 months', '7 to 9 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(69, 6, 'Iceland', 'IS', 'countries/is.png', 0, 'Icelandic Patent Office', 'ELS-IPO', '2 months', '2 to 4months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(70, 6, 'Ireland', 'IE', 'countries/ie.png', 0, 'Patents Office - Department of Jobs, Enterprise & Innovation', 'IEIPO', '3 months', '9 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(71, 6, 'Italy', 'IT', 'countries/it.png', 0, 'Italian Patent and Trademark Office', 'UIBM', '3 months', '12 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(72, 6, 'Jersey', 'JE', 'countries/je.png', 0, 'Intellectual Property Office', 'IPO', '1 month', '8 to 12 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(73, 6, 'Kosovo', 'KS', 'countries/ks.png', 0, 'Intellectual Property Office', 'IPO', '3 months', '12 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(74, 6, 'Latvia', 'LV', 'countries/lv.png', 0, 'Patent Office of the Republic of Latvia', 'LRPV', '3 months', '6 to 8 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(75, 6, 'Liechtenstein', 'LI', 'countries/li.png', 0, 'Amt für Volkswirtschaft', 'AVW', 'no opposition period', '3 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(76, 6, 'Lithuania', 'LT', 'countries/lt.png', 0, 'State Patent Bureau of the Republic of Lithuania', 'VPB', '3 months', '5 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(77, 6, 'Luxembourg', 'LU', 'countries/lu.png', 0, 'Intellectual Property Office', 'IPO', '1 month', '8 to 12 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(78, 6, 'Macedonia', 'MK', 'countries/mk.png', 0, 'State Office of Industrial Property', 'SOIP', '3 months', '6 to 12 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(79, 6, 'Malta', 'MT', 'countries/mt.png', 0, 'Industrial Property Registrations Directorate', 'CD-IPRD', '9 months', '9 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(80, 6, 'Moldova', 'MD', 'countries/md.png', 0, 'State Agency on Intellectual Property', 'AGEPI', '3 months', '12 to 14 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(81, 6, 'Monaco', 'MC', 'countries/mc.png', 0, 'Intellectual Property Division', 'MIPRO', 'no opposition period', '3 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(82, 6, 'Netherlands', 'NL', 'countries/nl.png', 0, 'Intellectual Property Office', 'IPO', '1 month', '8 to 12 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(83, 6, 'Norway', 'NO', 'countries/no.png', 0, 'Norwegian Industrial Property Office', 'NIPO', '3 months', '3 to 6 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(84, 6, 'Poland', 'PL', 'countries/pl.png', 0, 'Patent Office of the Republic of Poland', 'PPO', '3 months', '6 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(85, 6, 'Portugal', 'PT', 'countries/pt.png', 0, 'Portuguese Institute of Industrial Property', 'INPIPT', '2 months', '4 to 6 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(86, 6, 'Romania', 'RO', 'countries/ro.png', 0, 'State Office for Inventions and Trademarks', 'OSIM', '2 months', '6 to 12 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(87, 6, 'Russian Federation', 'RU', 'countries/ru.png', 0, 'Federal Service for Intellectual Property', 'ROSPATENT', '5 years', '12 to 14 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(88, 6, 'San Marino', 'SM', 'countries/sm.png', 0, 'Department of External Affairs', 'USBM', '3 months', '12 to 18 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(89, 6, 'Serbia', 'RS', 'countries/rs.png', 0, 'Intellectual Property Office of the Republic of Serbia', 'IPORS', '3 months', '12 to 18 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(90, 6, 'Montenegro', 'ME', 'countries/me.png', 0, 'Intellectual Property Office of Montenegro', 'IPOM', '3 months', '12 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(91, 6, 'Sint Maarten', 'SX', 'countries/sx.png', 0, 'Bureau for Intellectual Property Sint Maarten', 'IPO', '1 month', '8 to 12 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(92, 6, 'Slovakia', 'SK', 'countries/sk.png', 0, 'Industrial Property Office of the Slovak Republic', 'SKIPO', '3 months', '4 to 5 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(93, 6, 'Slovenia', 'SI', 'countries/si.png', 0, 'Slovenian Intellectual Property Office', 'SIPO', '3 months', '5 to 12 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(94, 6, 'Spain', 'ES', 'countries/es.png', 0, 'Spanish Patent and Trademark Office O.A.', 'OEPM', '2 months', '6 to 12 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(95, 6, 'Sweden', 'SE', 'countries/se.png', 0, 'Swedish Patent and Registration Office (SPRO)', 'PRV', '3 months', '3 to 6 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(96, 6, 'Switzerland', 'CH', 'countries/ch.png', 0, 'Swiss Federal Institute of Intellectual Property', 'IGE-IPI', '3 months', '3 to 6 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(97, 6, 'Ukraine', 'UA', 'countries/ua.png', 0, 'Ministry of Economic Development and Trade of Ukraine', 'UIPV', '12 to 15 months', '12 to 15 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(98, 6, 'United Kingdom', 'GB', 'countries/gb.png', 0, 'United Kingdom Intellectual Property Office', 'UKIPO', '2 months', '4 to 6 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(99, 3, 'Canada', 'CA', 'countries/ca.png', 1, 'Canadian Intellectual Property Office', 'CIPO', '2 months', '14 to 20 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(100, 3, 'Mexico', 'MX', 'countries/mx.png', 0, 'Mexican Institute of Industrial Property', 'IMPI', '10 business days', '8 to 10 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(101, 3, 'United States of America', 'US', 'countries/us.png', 1, 'United States Patent and Trademark Office', 'USPTO', ' 30 days', '9 to 16 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(102, 7, 'Argentina', 'AR', 'countries/ar.png', 0, 'National Institute of Industrial Property', 'INPI', ' 30 days', '12 to 18 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(103, 7, 'Bolivia', 'BO', 'countries/bo.png', 0, 'National Intellectual Property Service, Ministry of Productive Development and Plural Economy', 'NIPS', ' 30 days', '12 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(104, 7, 'Brazil', 'BR', 'countries/br.png', 0, 'National Institute of Industrial Property', 'INPI', ' 60 days', '24 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(105, 7, 'Chile', 'CL', 'countries/cl.png', 0, 'National Institute of Industrial Property', 'INAPI', ' 30 days', '4 to 12 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(106, 7, 'Colombia', 'CO', 'countries/co.png', 0, 'Superintendence of Industry and Commerce', 'SIC', ' 30 days', '6 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(107, 7, 'Ecuador', 'EC', 'countries/ec.png', 0, 'Instituto Ecuatoriano de Propiedad Industrial Intelectual', 'IEPI', ' 30 days', '6 to 8 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(108, 7, 'Guyana', 'GY', 'countries/gy.png', 0, 'Deeds and Commercial Registries', 'IPO', ' 30 days', '12 to 18 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(109, 7, 'Paraguay', 'PY', 'countries/py.png', 0, 'National Directorate of Intellectual Property', 'NDIP', ' 60 days', '8 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(110, 7, 'Peru', 'PE', 'countries/pe.png', 0, 'National Institute for the Defense of Competition and Protection of Intellectual Property', 'INDECOPI', ' 30 days', '5 to 6 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(111, 7, 'Suriname', 'SR', 'countries/sr.png', 0, 'Intellectual Property Office of Suriname', 'IPO', '6 months', '3 years', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(112, 7, 'Uruguay', 'UY', 'countries/uy.png', 0, 'National Directorate of Industrial Property', 'MIEM-DNPI', '30 days', '14 to 18 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(113, 7, 'Venezuela', 'VE', 'countries/ve.png', 0, 'Autonomous Service of Intellectual Property', 'SAPI', '30 days', '12 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(114, 8, 'Antigua and Barbuda', 'AG', 'countries/ag.png', 0, 'Antigua and Barbuda Intellectual Property & Commerce Office', 'ABIPCO', '3 months', '3 to 4 years', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(115, 8, 'Aruba', 'AW', 'countries/ag.png', 0, 'Bureau of Intellectual Property in Aruba', 'IPO', '2 months', '6 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(116, 8, 'Bahamas', 'BS', 'countries/bs.png', 0, 'Registrar General\'s Department', 'IPO', '30 days', '4 years', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(117, 8, 'Barbados', 'BB', 'countries/bb.png', 0, 'Corporate Affairs and Intellectual Property Office', 'CAIPO', '?', '?', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(118, 8, 'Belize', 'BZ', 'countries/bz.png', 0, 'Belize Intellectual Property Office', 'BELIPO', '3 months', '8 to 12 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(119, 8, 'Cayman Islands', 'KY', 'countries/ky.png', 0, 'Intellectual Property Office', 'IPO', '?', '?', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(120, 8, 'Costa Rica', 'CR', 'countries/cr.png', 0, 'Register of Industrial Property', 'RNPCR', '2 months', '5 to 6 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(121, 8, 'Dominica', 'DM', 'countries/dm.png', 0, 'Companies and Intellectual Property Office', 'RNPCR', '2 months', '6 to 12 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(122, 8, 'Dominican Republic', 'DO', 'countries/do.png', 0, 'National Office of Industrial Property', 'NOIP', '45 days', '30 days', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(123, 8, 'El Salvador', 'SV', 'countries/sv.png', 0, 'National Center of Registries', 'CNR', '2 months', '7 to 9 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(124, 8, 'Grenada', 'GD', 'countries/gd.png', 0, 'Corporate Affairs and Intellectual Property Office', 'CAIPO', '1 month', '1 year', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(125, 8, 'Guatemala', 'GT', 'countries/gt.png', 0, 'Registry of Intellectual Property', 'IPO', '2 months', '2 to 3 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(126, 8, 'Haiti', 'HT', 'countries/ht.png', 0, 'Haitian Copyright Office', 'BHDA', '2 months', '8 to 12 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(127, 8, 'Honduras', 'HN', 'countries/hn.png', 0, 'Directorate General of Intellectual Property', 'DIGEPIH', '1 month', '3 to 4 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(128, 8, 'Jamaica', 'JM', 'countries/jm.png', 0, 'Jamaica Intellectual Property Office', 'JIPO', '2 months', '12 to 18 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(129, 8, 'Montserrat', 'MS', 'countries/ms.png', 0, 'Intellectual Property Office', 'IPO', '?', '?', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(130, 8, 'Nicaragua', 'NI', 'countries/ni.png', 0, 'Intellectual Property Registry', 'RPI', '2 months', '10 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(131, 8, 'Panama', 'PA', 'countries/pa.png', 0, 'Directorate General of the Industrial Property Registry', 'DIGERPI', '2 months', '3 to 6 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(132, 8, 'Puerto Rico', 'PR', 'countries/pr.png', 0, 'The Department of Economic development and Commerce', 'IPO', '1 month', '2 years', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(133, 8, 'Saint Kitts and Nevis', 'KN', 'countries/kn.png', 0, 'Intellectual Property Office', 'IPO', '?', '?', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(134, 8, 'Saint Lucia', 'LC', 'countries/lc.png', 0, 'Intellectual Property Office', 'IPO', '?', '?', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(135, 8, 'Saint Vincent and the Grenadines', 'VC', 'countries/vc.png', 0, 'Commerce and Intellectual Property Office', 'IPO', '3 months', '12 to 18 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(136, 8, 'Trinidad and Tobago', 'TT', 'countries/tt.png', 0, 'Intellectual Property Office', 'IPO', '3 months', '8 months to 2 years', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(137, 8, 'Turks and Caicos Islands', 'TC', 'countries/tt.png', 0, 'Intellectual Property Office', 'IPO', '1 month', ' 4 to 6 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(138, 8, 'British Virgin Islands', 'VG', 'countries/vg.png', 0, 'Intellectual Property Office', 'IPO', '3 months', '8 to 12 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(139, 4, 'American Samoa', 'AS', 'countries/as.png', 0, 'Intellectual Property Office', 'IPO', '?', '?', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(140, 4, 'Australia', 'AU', 'countries/au.png', 0, 'IP Australia Department of Industry', 'IPO', '2 months', '7 to 24 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(141, 4, 'Fiji', 'FJ', 'countries/fj.png', 0, 'Office of the Attorney-General', 'IPO', '3 months', '12 to 18 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(142, 4, 'New Zealand', 'NZ', 'countries/nz.png', 0, 'New Zealand Intellectual Property Office', 'IPONZ', '3 months', '6 to 12 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(143, 4, 'Samoa', 'WS', 'countries/ws.png', 0, 'Registries of Companies and Intellectual Property Division', 'RCIP', '3 months', '1 to 2 years', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(144, 4, 'Solomon Islands', 'SB', 'countries/sb.png', 0, 'Registrar General\'s Office', 'RGO', 'no opposition period', '2 to 3 years', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(145, 4, 'Tonga', 'TO', 'countries/to.png', 0, 'Registry & Intellectual Property Office', 'RIPO', '3 months', '1 to 2 years', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(146, 5, 'Algeria', 'DZ', 'countries/dz.png', 0, 'Algerian National Institute of Industrial Property', 'INAPI', 'no opposition period', '2 to 15 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(147, 5, 'Angola', 'AO', 'countries/ao.png', 0, 'Angolan Institute of Industrial Property', 'AIIP', '2 months', '2 to 4 years', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(148, 5, 'Anguilla', 'AI', 'countries/ai.png', 0, 'Intellectual Property Office', 'IPO', '?', '?', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(149, 5, 'Benin', 'BJ', 'countries/bj.png', 0, 'Beninese Copyright Office', 'BUBEDRA', '?', '?', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(150, 5, 'Botswana', 'BW', 'countries/bw.png', 0, 'Companies and Intellectual Property Authority', 'CIPA', '?', '?', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(151, 5, 'Burkina Faso', 'BF', 'countries/bf.png', 0, 'General Directorate of Industrial Property', 'DGPI', '?', '?', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(152, 5, 'Burundi', 'BF', 'countries/bf.png', 0, 'General Directorate of Industrial Property', 'DGPI', '?', '?', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(153, 5, 'Cameroon', 'CM', 'countries/cm.png', 0, 'Directorate of Technological Development and Industrial Property', '?', '?', '?', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(154, 5, 'Cape Verde', 'CV', 'countries/cv.png', 0, 'Industrial Property Institute', ' IPICV', '3 months', '6 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(155, 5, 'Central African Republic', 'CF', 'countries/cf.png', 0, 'National Industrial Property Service', 'SNL', '?', '?', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(156, 5, 'Chad', 'TD', 'countries/td.png', 0, 'Intellectual Property Office', 'IPO', '?', '?', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(157, 5, 'Comoros', 'KM', 'countries/km.png', 0, 'Intellectual Property Office', 'IPO', '?', '?', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(158, 5, 'Republic of the Congo', 'CG', 'countries/cg.png', 0, 'Congolese Copyright Office', 'BCDA', '?', '?', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(159, 5, 'Ivory Coast', 'CI', 'countries/ci.png', 0, 'Intellectual Property Office', 'IPO', '?', '?', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(160, 5, 'Djibouti', 'DJ', 'countries/dj.png', 0, 'Office of Industrial Property and Commerce', 'OIPC', '?', '?', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(161, 2, 'Egypt', 'EG', 'countries/eg.png', 0, 'Trademarks and Industrial Designs Office', 'TIDO', '2 months', '18 to 24 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(162, 5, 'Equatorial Guinea', 'GQ', 'countries/gq.png', 0, 'Council of Scientific and Tecnological Research', 'IPO', '?', '?', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(163, 5, 'Eritrea', 'ER', 'countries/er.png', 0, 'Domestic Trade and Intellectual Property', 'IPO', '?', '?', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(164, 5, 'Ethiopia', 'ET', 'countries/et.png', 0, 'Ethiopian Intellectual Property Office', 'EIPO', '2 months', '5 to 7 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(165, 5, 'Gabon', 'GA', 'countries/ga.png', 0, 'Intellectual Property Office', 'IPO', '?', '?', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(166, 5, 'Gambia', 'GM', 'countries/gm.png', 0, 'National Centre for Arts and Culture', 'IPO', '?', '?', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(167, 5, 'Ghana', 'GH', 'countries/gh.png', 0, 'Registrar General\'s Department Ministry of Justice', 'IPO', '2 months', '1 year', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(168, 5, 'Guinea', 'GN', 'countries/gn.png', 0, 'National Service of Industrial Property', 'NSIP', '?', '?', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(169, 5, 'Guinea-Bissau', 'GW', 'countries/gw.png', 0, 'General Directorate of Industrial Property', 'GDIP', '?', '?', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(170, 5, 'Kenya', 'KE', 'countries/ke.png', 0, 'Kenya Industrial Property Institute', 'KIPI', '2 months', '15 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(171, 5, 'Lesotho', 'LS', 'countries/ls.png', 0, 'Ministry of Law and Constitutional Affairs', '?', '3 months', '24 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(172, 5, 'Liberia', 'LR', 'countries/lr.png', 0, 'Liberia Intellectual Property Office', 'LIPO', '?', '?', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(173, 5, 'Libya', 'LY', 'countries/ly.png', 0, 'National Authority for Scientific Research', 'NASR', '3 months', '3 years', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(174, 5, 'Madagascar', 'MG', 'countries/mg.png', 0, 'Malagasy Industrial Property Office', 'MIPO', 'no opposition period', '10 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(175, 5, 'Malawi', 'MW', 'countries/mg.png', 0, 'Intellectual Property Office', 'IPO', '?', '?', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(176, 5, 'Mali', 'ML', 'countries/ml.png', 0, 'Malian Centre for the Promotion of Industrial Property', 'CEMAPI', '?', '?', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(177, 5, 'Mauritania', 'MR', 'countries/mr.png', 0, 'Directorate of Industry', '?', '?', '?', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(178, 5, 'Mauritius', 'MU', 'countries/mu.png', 0, 'Industrial Property Office', 'IPO', '2 months', '1 year', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(179, 5, 'Morocco', 'MA', 'countries/ma.png', 0, 'Moroccan Industrial and Commercial Property Office', 'MICPO', '2 months', '4 to 6 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(180, 5, 'Mozambique', 'MZ', 'countries/mz.png', 0, 'Industrial Property Institute', 'IPI', '30 days', '6 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(181, 5, 'Namibia', 'NA', 'countries/na.png', 0, 'Business and Intellectual Property Authority', 'BIPA', '2 months', '3 years', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(182, 5, 'Niger', 'NE', 'countries/ne.png', 0, 'Intellectual Property Office', 'IPO', '?', '?', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(183, 5, 'Nigeria', 'NG', 'countries/ng.png', 0, 'Commercial Law Department of Trademarks, Patents and Designs', '?', '2 months', '2 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(184, 5, 'Papua New Guinea', 'PG', 'countries/pg.png', 0, 'Intellectual Property Office of Papua New Guinea', 'IPOPNG', '?', '?', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(185, 5, 'Rwanda', 'RW', 'countries/rw.png', 0, 'Rwanda Development Board', 'RDB', '2 months', '3 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(186, 5, 'Sao Tome and Principe', 'ST', 'countries/rw.png', 0, 'Industrial Property National Service', 'SENAPI', '3 months', '3 to 12 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(187, 5, 'Senegal', 'SN', 'countries/sn.png', 0, 'Senegalese Agency of Industrial Property and Innovation Technologique', 'ASPIT', '?', '?', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(188, 5, 'Seychelles', 'SC', 'countries/sc.png', 0, 'Department of Legal Affairs - President\'s Office', '?', '2 months', '1 to 2 years', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(189, 5, 'Sierra Leone', 'SL', 'countries/sl.png', 0, 'National Registry for industrial property and copyright', '?', '?', '?', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(190, 5, 'South Africa', 'ZA', 'countries/za.png', 0, 'Companies and Intellectual Property Commission', 'CIPC', '3 months', '2 years', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(191, 5, 'Swaziland', 'SZ', 'countries/sz.png', 0, 'Ministry of Commerce Industry and Trade', '?', '8 months', '9 to 12 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(192, 5, 'Tanzania', 'TZ', 'countries/tz.png', 0, 'Business Registrations and Licensing Agency', 'BRELA', '2 months', '6 months', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(193, 5, 'Togo', 'TG', 'countries/tg.png', 0, 'National Institute for Industrial Property and Technology', 'INPIT', '?', '?', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(194, 5, 'Tunisia', 'TN', 'countries/tn.png', 0, 'National Institute for Standardization and Industrial Property', 'NISIP', '2 months', '12 to 14 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(195, 5, 'Uganda', 'UG', 'countries/ug.png', 0, 'Uganda Registration Services Bureau', 'URSB', '2 months', '3 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(196, 5, 'Zambia', 'ZM', 'countries/zm.png', 0, 'Patents and Companies Registration Agency', 'PCRA', '?', '?', 1, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(197, 5, 'Zimbabwe', 'ZW', 'countries/zw.png', 0, 'Zimbabwe Intellectual Property Office', 'ZIPO', '2 months', '8 to 16 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(198, 5, 'ARIPO Treaty', 'ARIPO', 'countries/aripo.png', 0, 'African Regional Intellectual Property Organization', 'ARIPO', '3 months', '12 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(199, 5, 'OAPI Treaty', 'OAPI', 'countries/oapi.png', 0, 'African Intellectual Property Organization', 'OAPI', '6 months', '9 to 12 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(200, 5, 'Zanzibar', 'EAZ', 'countries/eaz.png', 0, 'Intellectual Property Office', 'IPO', '1 month', '8 to 12 months', 0, '2018-12-13 09:33:19', '2018-12-13 09:33:19');

-- --------------------------------------------------------

--
-- Table structure for table `formats`
--

CREATE TABLE `formats` (
  `id` int(10) UNSIGNED NOT NULL,
  `country_id` int(11) NOT NULL,
  `input_type` tinyint(1) NOT NULL,
  `poa_required` tinyint(1) NOT NULL,
  `trade_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `word_label` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `commerce_use` tinyint(1) NOT NULL,
  `has_article` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `formats`
--

INSERT INTO `formats` (`id`, `country_id`, `input_type`, `poa_required`, `trade_type`, `word_label`, `commerce_use`, `has_article`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 0, 'w', 'Word Mark', 0, 0, '2018-12-13 09:33:20', '2018-12-13 09:33:20');

-- --------------------------------------------------------

--
-- Table structure for table `inquiries`
--

CREATE TABLE `inquiries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `laravel_exceptions`
--

CREATE TABLE `laravel_exceptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `line` int(11) NOT NULL,
  `trace` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `query` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `cookies` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `headers` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(908, '2014_10_12_000000_create_users_table', 1),
(909, '2014_10_12_100000_create_password_resets_table', 1),
(910, '2016_01_04_173148_create_admin_tables', 1),
(911, '2017_07_17_040159_create_config_table', 1),
(912, '2017_07_17_040159_create_exceptions_table', 1),
(913, '2018_11_03_152859_create_trademark_classes_table', 1),
(914, '2018_11_03_152950_create_class_descriptions_table', 1),
(915, '2018_11_06_165929_create_countries_table', 1),
(916, '2018_11_06_171122_create_prices_table', 1),
(917, '2018_11_07_095041_create_formats_table', 1),
(918, '2018_11_09_064758_create_continents_table', 1),
(919, '2018_11_17_172713_create_inquiries_table', 1),
(920, '2018_11_28_151803_create_sessions_table', 1),
(921, '2018_11_29_144206_create_trademarks_table', 1),
(922, '2018_11_29_154711_create_contact_informations_table', 1),
(923, '2018_12_10_151926_create_orders_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_items` int(11) NOT NULL,
  `total_amount` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_number`, `total_items`, `total_amount`, `created_at`, `updated_at`) VALUES
(1, 'TM-1544693730', 2, 2355, '2018-12-13 09:35:30', '2018-12-13 09:35:30');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `prices`
--

CREATE TABLE `prices` (
  `id` int(10) UNSIGNED NOT NULL,
  `country_id` int(11) NOT NULL,
  `service_type` enum('Study','Registration','Certificate') COLLATE utf8mb4_unicode_ci NOT NULL,
  `initial_cost` double(8,2) NOT NULL,
  `additional_cost` double(8,2) NOT NULL,
  `logo_initial_cost` double(8,2) NOT NULL,
  `logo_additional_cost` double(8,2) NOT NULL,
  `tax` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `prices`
--

INSERT INTO `prices` (`id`, `country_id`, `service_type`, `initial_cost`, `additional_cost`, `logo_initial_cost`, `logo_additional_cost`, `tax`, `created_at`, `updated_at`) VALUES
(1, 99, 'Study', 87.00, 87.00, 170.00, 170.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(2, 99, 'Registration', 685.00, 0.00, 685.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(3, 99, 'Certificate', 432.00, 0.00, 432.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(4, 100, 'Study', 53.00, 53.00, 68.00, 68.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(5, 100, 'Registration', 543.00, 543.00, 543.00, 543.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(6, 100, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(7, 101, 'Study', 81.00, 81.00, 144.00, 144.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(8, 101, 'Registration', 427.00, 400.00, 427.00, 400.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(9, 101, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(10, 114, 'Study', 175.00, 116.00, 175.00, 116.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(11, 114, 'Registration', 966.00, 419.00, 966.00, 419.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(12, 114, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(13, 115, 'Study', 243.00, 116.00, 243.00, 116.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(14, 115, 'Registration', 1086.00, 138.00, 1086.00, 138.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(15, 115, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(16, 116, 'Study', 121.00, 121.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(17, 116, 'Registration', 1150.00, 745.00, 1150.00, 745.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(18, 116, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(19, 117, 'Study', 175.00, 116.00, 175.00, 116.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(20, 117, 'Registration', 1500.00, 1380.00, 1500.00, 1380.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(21, 117, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(22, 118, 'Study', 116.00, 97.00, 116.00, 97.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(23, 118, 'Registration', 667.00, 368.00, 667.00, 368.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(24, 118, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(25, 119, 'Study', 146.00, 87.00, 146.00, 87.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(26, 119, 'Registration', 1113.00, 212.00, 1113.00, 212.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(27, 119, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(28, 120, 'Study', 78.00, 58.00, 78.00, 58.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(29, 120, 'Registration', 455.00, 442.00, 465.00, 442.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(30, 120, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(31, 121, 'Study', 121.00, 121.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(32, 121, 'Registration', 1168.00, 543.00, 1168.00, 543.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(33, 121, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(34, 122, 'Study', 82.00, 82.00, 82.00, 82.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(35, 122, 'Registration', 432.00, 377.00, 432.00, 377.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(36, 122, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(37, 123, 'Study', 78.00, 78.00, 78.00, 78.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(38, 123, 'Registration', 727.00, 460.00, 727.00, 460.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(39, 123, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(40, 124, 'Study', 146.00, 146.00, 146.00, 146.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(41, 124, 'Registration', 1932.00, 1072.00, 1932.00, 1072.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(42, 124, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(43, 125, 'Study', 78.00, 78.00, 116.00, 116.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(44, 125, 'Registration', 810.00, 515.00, 810.00, 515.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(45, 125, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(46, 126, 'Study', 121.00, 121.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(47, 126, 'Registration', 626.00, 437.00, 662.00, 437.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(48, 126, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(49, 127, 'Study', 53.00, 53.00, 131.00, 131.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(50, 127, 'Registration', 566.00, 566.00, 566.00, 566.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(51, 127, 'Certificate', 247.00, 247.00, 247.00, 247.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(52, 128, 'Study', 121.00, 121.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(53, 128, 'Registration', 948.00, 212.00, 948.00, 212.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(54, 128, 'Certificate', 194.00, 0.00, 194.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(55, 129, 'Study', 175.00, 116.00, 175.00, 116.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(56, 129, 'Registration', 791.00, 345.00, 791.00, 345.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(57, 129, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(58, 130, 'Study', 58.00, 58.00, 68.00, 68.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(59, 130, 'Registration', 432.00, 313.00, 497.00, 340.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(60, 130, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(61, 131, 'Study', 87.00, 78.00, 175.00, 155.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(62, 131, 'Registration', 432.00, 386.00, 432.00, 386.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(63, 131, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(64, 132, 'Study', 102.00, 82.00, 102.00, 82.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(65, 132, 'Registration', 593.00, 465.00, 593.00, 465.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(66, 132, 'Certificate', 102.00, 102.00, 102.00, 102.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(67, 133, 'Study', 121.00, 121.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(68, 133, 'Registration', 1086.00, 414.00, 1086.00, 414.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(69, 133, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(70, 134, 'Study', 116.00, 78.00, 116.00, 78.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(71, 134, 'Registration', 869.00, 828.00, 869.00, 828.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(72, 134, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(73, 135, 'Study', 175.00, 78.00, 175.00, 78.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(74, 135, 'Registration', 957.00, 322.00, 957.00, 322.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(75, 135, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(76, 136, 'Study', 175.00, 116.00, 175.00, 116.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(77, 136, 'Registration', 1012.00, 120.00, 1012.00, 120.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(78, 136, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(79, 137, 'Study', 121.00, 78.00, 121.00, 78.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(80, 137, 'Registration', 1693.00, 980.00, 1693.00, 980.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(81, 137, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(82, 138, 'Study', 121.00, 121.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(83, 138, 'Registration', 1053.00, 538.00, 1053.00, 538.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(84, 138, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(85, 45, 'Study', 78.00, 78.00, 78.00, 78.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(86, 45, 'Registration', 488.00, 83.00, 488.00, 83.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(87, 45, 'Certificate', 272.00, 0.00, 272.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(88, 46, 'Study', 124.00, 76.00, 178.00, 118.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(89, 46, 'Registration', 926.00, 138.00, 926.00, 138.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(90, 46, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(91, 47, 'Study', 175.00, 97.00, 291.00, 136.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(92, 47, 'Registration', 580.00, 115.00, 580.00, 115.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(93, 47, 'Certificate', 417.00, 0.00, 417.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(94, 48, 'Study', 78.00, 78.00, 136.00, 136.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(95, 48, 'Registration', 1794.00, 156.00, 1794.00, 156.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(96, 48, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(97, 49, 'Study', 121.00, 121.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(98, 49, 'Registration', 699.00, 92.00, 699.00, 92.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(99, 49, 'Certificate', 500.00, 0.00, 500.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(100, 50, 'Study', 121.00, 121.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(101, 50, 'Registration', 1012.00, 216.00, 1012.00, 216.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(102, 50, 'Certificate', 558.00, 0.00, 558.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(103, 51, 'Study', 97.00, 97.00, 173.00, 173.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(104, 51, 'Registration', 545.00, 144.00, 545.00, 144.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(105, 51, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(106, 52, 'Study', 97.00, 97.00, 173.00, 173.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(107, 52, 'Registration', 545.00, 144.00, 545.00, 144.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(108, 52, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(109, 53, 'Study', 116.00, 78.00, 378.00, 49.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(110, 53, 'Registration', 690.00, 110.00, 690.00, 110.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(111, 53, 'Certificate', 1019.00, 116.00, 1019.00, 116.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(112, 54, 'Study', 76.00, 76.00, 151.00, 151.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(113, 54, 'Registration', 514.00, 66.00, 529.00, 66.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(114, 54, 'Certificate', 596.00, 0.00, 596.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(115, 55, 'Study', 78.00, 78.00, 155.00, 155.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(116, 55, 'Registration', 672.00, 41.00, 672.00, 41.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(117, 55, 'Certificate', 698.00, 78.00, 698.00, 78.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(118, 56, 'Study', 97.00, 76.00, 97.00, 76.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(119, 56, 'Registration', 812.00, 812.00, 812.00, 812.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(120, 56, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(121, 57, 'Study', 78.00, 78.00, 155.00, 155.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(122, 57, 'Registration', 754.00, 124.00, 754.00, 124.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(123, 57, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(124, 58, 'Study', 68.00, 68.00, 116.00, 116.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(125, 58, 'Registration', 580.00, 115.00, 580.00, 115.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(126, 58, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(127, 59, 'Study', 65.00, 65.00, 130.00, 130.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(128, 59, 'Registration', 638.00, 123.00, 638.00, 123.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(129, 59, 'Certificate', 411.00, 0.00, 411.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(130, 60, 'Study', 131.00, 131.00, 335.00, 335.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(131, 60, 'Registration', 1337.00, 185.00, 1337.00, 185.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(132, 60, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(133, 61, 'Study', 78.00, 78.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(134, 61, 'Registration', 1072.00, 276.00, 1072.00, 276.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(135, 61, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(136, 62, 'Study', 113.00, 76.00, 162.00, 124.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(137, 62, 'Registration', 663.00, 123.00, 663.00, 123.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(138, 62, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(139, 63, 'Study', 87.00, 58.00, 87.00, 58.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(140, 63, 'Registration', 690.00, 166.00, 690.00, 166.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(141, 63, 'Certificate', 611.00, 0.00, 611.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(142, 64, 'Study', 118.00, 118.00, 243.00, 245.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(143, 64, 'Registration', 616.00, 144.00, 616.00, 144.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(144, 64, 'Certificate', 130.00, 0.00, 130.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(145, 65, 'Study', 146.00, 95.00, 146.00, 95.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(146, 65, 'Registration', 465.00, 0.00, 465.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(147, 65, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(148, 66, 'Study', 76.00, 76.00, 76.00, 76.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(149, 66, 'Registration', 638.00, 169.00, 638.00, 169.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(150, 66, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(151, 67, 'Study', 99.00, 99.00, 217.00, 217.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(152, 67, 'Registration', 842.00, 59.00, 842.00, 59.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(153, 67, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(154, 68, 'Study', 59.00, 59.00, 97.00, 97.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(155, 68, 'Registration', 1013.00, 375.00, 1013.00, 376.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(156, 68, 'Certificate', 346.00, 0.00, 346.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(157, 69, 'Study', 78.00, 58.00, 78.00, 58.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(158, 69, 'Registration', 1311.00, 152.00, 1311.00, 152.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(159, 69, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(160, 70, 'Study', 124.00, 124.00, 151.00, 151.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(161, 70, 'Registration', 616.00, 211.00, 616.00, 211.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(162, 70, 'Certificate', 672.00, 0.00, 672.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(163, 71, 'Study', 70.00, 70.00, 70.00, 70.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(164, 71, 'Registration', 709.00, 92.00, 709.00, 92.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(165, 71, 'Certificate', 130.00, 0.00, 130.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(166, 72, 'Study', 99.00, 99.00, 99.00, 99.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(167, 72, 'Registration', 1202.00, 182.00, 1202.00, 182.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(168, 72, 'Certificate', -1.00, -1.00, -1.00, -1.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(169, 73, 'Study', 131.00, 116.00, 131.00, 116.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(170, 73, 'Registration', 534.00, 78.00, 534.00, 78.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(171, 73, 'Certificate', 42.00, 0.00, 42.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(172, 74, 'Study', 124.00, 124.00, 151.00, 151.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(173, 74, 'Registration', 360.00, 98.00, 360.00, 98.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(174, 74, 'Certificate', 363.00, 0.00, 363.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(175, 75, 'Study', 78.00, 78.00, 78.00, 78.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(176, 75, 'Registration', 1702.00, 147.00, 1702.00, 147.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(177, 75, 'Certificate', 359.00, 0.00, 359.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(178, 76, 'Study', 86.00, 86.00, 151.00, 151.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(179, 76, 'Registration', 426.00, 92.00, 426.00, 92.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(180, 76, 'Certificate', 449.00, 0.00, 449.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(181, 77, 'Study', 97.00, 97.00, 173.00, 173.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(182, 77, 'Registration', 545.00, 144.00, 545.00, 144.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(183, 77, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(184, 78, 'Study', 121.00, 49.00, 194.00, 78.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(185, 78, 'Registration', 368.00, 46.00, 368.00, 46.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(186, 78, 'Certificate', 388.00, 49.00, 388.00, 49.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(187, 79, 'Study', 76.00, 76.00, 124.00, 124.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(188, 79, 'Registration', 658.00, 658.00, 658.00, 658.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(189, 79, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(190, 80, 'Study', 76.00, 76.00, 157.00, 157.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(191, 80, 'Registration', 915.00, 87.00, 915.00, 87.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(192, 80, 'Certificate', 471.00, 0.00, 471.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(193, 81, 'Study', 124.00, 124.00, 124.00, 124.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(194, 81, 'Registration', 616.00, 98.00, 616.00, 98.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(195, 81, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(196, 82, 'Study', 97.00, 97.00, 173.00, 173.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(197, 82, 'Registration', 545.00, 144.00, 545.00, 144.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(198, 82, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(199, 83, 'Study', 113.00, 113.00, 113.00, 113.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(200, 83, 'Registration', 957.00, 220.00, 957.00, 220.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(201, 83, 'Certificate', 344.00, 0.00, 344.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(202, 84, 'Study', 78.00, 78.00, 155.00, 155.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(203, 84, 'Registration', 796.00, 87.00, 796.00, 87.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(204, 84, 'Certificate', 543.00, 388.00, 543.00, 388.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(205, 85, 'Study', 76.00, 76.00, 118.00, 118.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(206, 85, 'Registration', 442.00, 159.00, 442.00, 159.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(207, 85, 'Certificate', 189.00, 0.00, 189.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(208, 86, 'Study', 86.00, 70.00, 135.00, 97.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(209, 86, 'Registration', 684.00, 354.00, 879.00, 354.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(210, 86, 'Certificate', 178.00, 0.00, 178.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(211, 87, 'Study', 121.00, 121.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(212, 87, 'Registration', 833.00, 133.00, 833.00, 133.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(213, 87, 'Certificate', 606.00, 0.00, 606.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(214, 88, 'Study', 70.00, 70.00, 70.00, 70.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(215, 88, 'Registration', 694.00, 205.00, 694.00, 205.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(216, 88, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(217, 89, 'Study', 78.00, 63.00, 116.00, 97.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(218, 89, 'Registration', 616.00, 129.00, 616.00, 129.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(219, 89, 'Certificate', 776.00, 116.00, 776.00, 116.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(220, 90, 'Study', 76.00, 76.00, 76.00, 76.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(221, 90, 'Registration', 565.00, 76.00, 565.00, 76.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(222, 90, 'Certificate', 786.00, 59.00, 786.00, 59.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(223, 91, 'Study', 116.00, 78.00, 116.00, 78.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(224, 91, 'Registration', 708.00, 64.00, 708.00, 64.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(225, 91, 'Certificate', -1.00, -1.00, -1.00, -1.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(226, 92, 'Study', 76.00, 76.00, 157.00, 157.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(227, 92, 'Registration', 828.00, 201.00, 828.00, 201.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(228, 92, 'Certificate', 141.00, 0.00, 141.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(229, 93, 'Study', 76.00, 76.00, 157.00, 157.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(230, 93, 'Registration', 627.00, 102.00, 627.00, 102.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(231, 93, 'Certificate', 727.00, 130.00, 727.00, 130.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(232, 94, 'Study', 53.00, 53.00, 216.00, 216.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(233, 94, 'Registration', 350.00, 211.00, 350.00, 211.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(234, 94, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(235, 95, 'Study', 78.00, 78.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(236, 95, 'Registration', 598.00, 120.00, 598.00, 120.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(237, 95, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(238, 96, 'Study', 87.00, 87.00, 87.00, 87.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(239, 96, 'Registration', 764.00, 138.00, 764.00, 138.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(240, 96, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(241, 97, 'Study', 121.00, 121.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(242, 97, 'Registration', 506.00, 258.00, 506.00, 258.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(243, 97, 'Certificate', 514.00, 150.00, 514.00, 150.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(244, 98, 'Study', 68.00, 68.00, 136.00, 105.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(245, 98, 'Registration', 459.00, 129.00, 459.00, 129.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(246, 98, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(247, 1, 'Study', 146.00, 146.00, 146.00, 146.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(248, 1, 'Registration', 451.00, 414.00, 451.00, 414.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(249, 1, 'Certificate', 466.00, 437.00, 466.00, 437.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(250, 2, 'Study', 121.00, 121.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(251, 2, 'Registration', 1408.00, 1380.00, 1408.00, 1380.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(252, 2, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(253, 3, 'Study', 116.00, 78.00, 116.00, 78.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(254, 3, 'Registration', 428.00, 331.00, 428.00, 331.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(255, 3, 'Certificate', 349.00, 267.00, 349.00, 267.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(256, 4, 'Study', 121.00, 121.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(257, 4, 'Registration', 1150.00, 644.00, 1150.00, 644.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(258, 4, 'Certificate', 301.00, 243.00, 301.00, 243.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(259, 5, 'Study', 121.00, 121.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(260, 5, 'Registration', 552.00, 331.00, 552.00, 331.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(261, 5, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(262, 6, 'Study', 73.00, 73.00, 146.00, 146.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(263, 6, 'Registration', 386.00, 386.00, 386.00, 386.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(264, 6, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(265, 7, 'Study', 68.00, 68.00, 194.00, 155.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(266, 7, 'Registration', 589.00, 313.00, 589.00, 313.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(267, 7, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(268, 8, 'Study', 97.00, 78.00, 136.00, 116.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(269, 8, 'Registration', 359.00, 359.00, 359.00, 359.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(270, 8, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(271, 9, 'Study', 97.00, 97.00, 136.00, 136.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(272, 9, 'Registration', 515.00, 460.00, 515.00, 460.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(273, 9, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(274, 10, 'Study', 279.00, 279.00, 279.00, 279.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(275, 10, 'Registration', 801.00, 383.00, 801.00, 383.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(276, 10, 'Certificate', 495.00, 147.00, 495.00, 147.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(277, 11, 'Study', 76.00, 76.00, 151.00, 151.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(278, 11, 'Registration', 593.00, 414.00, 593.00, 414.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(279, 11, 'Certificate', 530.00, 423.00, 1060.00, 852.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(280, 12, 'Study', 121.00, 87.00, 194.00, 155.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(281, 12, 'Registration', 423.00, 423.00, 423.00, 423.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(282, 12, 'Certificate', 388.00, 388.00, 388.00, 388.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(283, 13, 'Study', 121.00, 121.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(284, 13, 'Registration', 810.00, 299.00, 810.00, 299.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(285, 13, 'Certificate', 631.00, 0.00, 631.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(286, 14, 'Study', 78.00, 78.00, 155.00, 155.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(287, 14, 'Registration', 474.00, 474.00, 474.00, 474.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(288, 14, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(289, 15, 'Study', 68.00, 68.00, 68.00, 68.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(290, 15, 'Registration', 396.00, 391.00, 396.00, 391.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(291, 15, 'Certificate', 485.00, 466.00, 485.00, 466.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(292, 16, 'Study', 116.00, 116.00, 116.00, 116.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(293, 16, 'Registration', 1012.00, 147.00, 1012.00, 147.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(294, 16, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(295, 17, 'Study', 175.00, 78.00, 175.00, 78.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(296, 17, 'Registration', 501.00, 184.00, 501.00, 184.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(297, 17, 'Certificate', 432.00, 97.00, 432.00, 97.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(298, 18, 'Study', 97.00, 97.00, 97.00, 97.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(299, 18, 'Registration', 460.00, 110.00, 460.00, 110.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(300, 18, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(301, 19, 'Study', 78.00, 78.00, 78.00, 78.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(302, 19, 'Registration', 460.00, 419.00, 460.00, 419.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(303, 19, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(304, 20, 'Study', 121.00, 121.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(305, 20, 'Registration', 304.00, 285.00, 304.00, 285.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(306, 20, 'Certificate', 340.00, 320.00, 340.00, 320.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(307, 21, 'Study', 97.00, 78.00, 160.00, 97.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(308, 21, 'Registration', 672.00, 478.00, 672.00, 478.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(309, 21, 'Certificate', 296.00, 0.00, 296.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(310, 22, 'Study', 87.00, 87.00, 87.00, 87.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(311, 22, 'Registration', 497.00, 396.00, 497.00, 396.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(312, 22, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(313, 23, 'Study', 146.00, 97.00, 97.00, 97.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(314, 23, 'Registration', 460.00, 460.00, 460.00, 460.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(315, 23, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(316, 24, 'Study', 121.00, 121.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(317, 24, 'Registration', 451.00, 327.00, 451.00, 327.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(318, 24, 'Certificate', 223.00, 223.00, 223.00, 223.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(319, 25, 'Study', 78.00, 78.00, 78.00, 78.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(320, 25, 'Registration', 653.00, 276.00, 653.00, 276.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(321, 25, 'Certificate', 825.00, 0.00, 825.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(322, 26, 'Study', 121.00, 121.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(323, 26, 'Registration', 432.00, 368.00, 432.00, 368.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(324, 26, 'Certificate', 184.00, 175.00, 184.00, 175.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(325, 27, 'Study', -1.00, -1.00, -1.00, -1.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(326, 27, 'Registration', 1130.00, 6.00, 1130.00, 6.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(327, 27, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(328, 28, 'Study', 78.00, 78.00, 78.00, 78.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(329, 28, 'Registration', 690.00, 221.00, 690.00, 221.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(330, 28, 'Certificate', 626.00, 626.00, 626.00, 626.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(331, 29, 'Study', 78.00, 78.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(332, 29, 'Registration', 359.00, 299.00, 359.00, 299.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(333, 29, 'Certificate', 175.00, 116.00, 175.00, 116.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(334, 30, 'Study', 243.00, 102.00, 243.00, 102.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(335, 30, 'Registration', 685.00, 313.00, 685.00, 313.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(336, 30, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(337, 31, 'Study', 121.00, 121.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(338, 31, 'Registration', 764.00, 653.00, 764.00, 653.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(339, 31, 'Certificate', 1639.00, 1504.00, 1639.00, 1504.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(340, 32, 'Study', 121.00, 121.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(341, 32, 'Registration', 1224.00, 120.00, 1224.00, 120.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(342, 32, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(343, 33, 'Study', 121.00, 121.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(344, 33, 'Registration', 920.00, 644.00, 920.00, 644.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(345, 33, 'Certificate', 369.00, 257.00, 369.00, 257.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(346, 34, 'Study', 121.00, 121.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(347, 34, 'Registration', 635.00, 566.00, 635.00, 566.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(348, 34, 'Certificate', 640.00, 601.00, 640.00, 601.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(349, 35, 'Study', 121.00, 121.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(350, 35, 'Registration', 1564.00, 1527.00, 1564.00, 1527.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(351, 35, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(352, 36, 'Study', 121.00, 78.00, 121.00, 78.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(353, 36, 'Registration', 1012.00, 294.00, 1012.00, 294.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(354, 36, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(355, 37, 'Study', 78.00, 78.00, 78.00, 78.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(356, 37, 'Registration', 644.00, 644.00, 644.00, 644.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(357, 37, 'Certificate', 621.00, 582.00, 621.00, 582.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(358, 38, 'Study', 272.00, 243.00, 272.00, 243.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(359, 38, 'Registration', 865.00, 828.00, 865.00, 828.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(360, 38, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(361, 39, 'Study', 121.00, 121.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(362, 39, 'Registration', 1288.00, 1288.00, 1288.00, 1288.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(363, 39, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(364, 40, 'Study', 121.00, 121.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(365, 40, 'Registration', 1518.00, 1440.00, 1518.00, 1440.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(366, 40, 'Certificate', 1668.00, 1668.00, 1668.00, 1668.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(367, 41, 'Study', 78.00, 78.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(368, 41, 'Registration', 515.00, 230.00, 515.00, 230.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(369, 41, 'Certificate', 543.00, 0.00, 543.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(370, 42, 'Study', 121.00, 121.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(371, 42, 'Registration', 1739.00, 1412.00, 1739.00, 1412.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(372, 42, 'Certificate', 3220.00, 3220.00, 3220.00, 3220.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(373, 43, 'Study', 121.00, 121.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(374, 43, 'Registration', 1150.00, 920.00, 1150.00, 920.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(375, 43, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(376, 44, 'Study', 175.00, 175.00, 175.00, 175.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(377, 44, 'Registration', 368.00, 41.00, 368.00, 41.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(378, 44, 'Certificate', 437.00, 44.00, 437.00, 44.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(379, 102, 'Study', 78.00, 68.00, 78.00, 68.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(380, 102, 'Registration', 506.00, 465.00, 506.00, 465.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(381, 102, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(382, 103, 'Study', 87.00, 68.00, 97.00, 78.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(383, 103, 'Registration', 708.00, 626.00, 708.00, 626.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(384, 103, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(385, 104, 'Study', 78.00, 78.00, 155.00, 116.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(386, 104, 'Registration', 543.00, 543.00, 543.00, 543.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(387, 104, 'Certificate', 669.00, 669.00, 669.00, 669.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(388, 105, 'Study', 49.00, 49.00, 49.00, 49.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(389, 105, 'Registration', 414.00, 322.00, 414.00, 322.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(390, 105, 'Certificate', 243.00, 243.00, 243.00, 243.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(391, 106, 'Study', 57.00, 57.00, 115.00, 113.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(392, 106, 'Registration', 626.00, 488.00, 626.00, 488.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(393, 106, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(394, 107, 'Study', 63.00, 63.00, 63.00, 63.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(395, 107, 'Registration', 524.00, 524.00, 524.00, 524.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(396, 107, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(397, 108, 'Study', 194.00, 194.00, 194.00, 194.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(398, 108, 'Registration', 598.00, 598.00, 598.00, 598.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(399, 108, 'Certificate', 243.00, 243.00, 243.00, 243.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(400, 109, 'Study', 49.00, 44.00, 49.00, 44.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(401, 109, 'Registration', 359.00, 304.00, 363.00, 308.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(402, 109, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(403, 110, 'Study', 63.00, 58.00, 121.00, 112.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(404, 110, 'Registration', 511.00, 474.00, 547.00, 497.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(405, 110, 'Certificate', 131.00, 131.00, 131.00, 131.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(406, 111, 'Study', 272.00, 272.00, 272.00, 272.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(407, 111, 'Registration', 736.00, 644.00, 736.00, 644.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(408, 111, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(409, 112, 'Study', 107.00, 78.00, 107.00, 78.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(410, 112, 'Registration', 644.00, 566.00, 842.00, 593.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(411, 112, 'Certificate', 121.00, 0.00, 121.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(412, 113, 'Study', 97.00, 97.00, 97.00, 97.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(413, 113, 'Registration', 727.00, 727.00, 727.00, 727.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(414, 113, 'Certificate', 2192.00, 2192.00, 2192.00, 2192.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(415, 139, 'Study', 466.00, 466.00, 466.00, 466.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(416, 139, 'Registration', 1233.00, 1233.00, 1233.00, 1233.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(417, 139, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(418, 140, 'Study', 107.00, 53.00, 136.00, 68.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(419, 140, 'Registration', 475.00, 475.00, 475.00, 475.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(420, 140, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(421, 141, 'Study', 175.00, 116.00, 175.00, 116.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(422, 141, 'Registration', 1412.00, 681.00, 1412.00, 681.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(423, 141, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(424, 142, 'Study', 116.00, 116.00, 155.00, 155.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(425, 142, 'Registration', 566.00, 271.00, 566.00, 271.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(426, 142, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(427, 143, 'Study', 175.00, 116.00, 175.00, 116.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(428, 143, 'Registration', 1334.00, 819.00, 1334.00, 819.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(429, 143, 'Certificate', 679.00, 0.00, 679.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(430, 144, 'Study', 108.00, 108.00, 108.00, 108.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(431, 144, 'Registration', -1.00, -1.00, -1.00, -1.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(432, 144, 'Certificate', -1.00, -1.00, -1.00, -1.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(433, 145, 'Study', 175.00, 116.00, 175.00, 175.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(434, 145, 'Registration', 920.00, 497.00, 920.00, 497.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(435, 145, 'Certificate', -1.00, -1.00, -1.00, -1.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(436, 146, 'Study', 262.00, 58.00, 262.00, 58.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(437, 146, 'Registration', 598.00, 184.00, 598.00, 184.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(438, 146, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(439, 147, 'Study', 124.00, 124.00, 124.00, 124.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(440, 147, 'Registration', 828.00, 719.00, 828.00, 719.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(441, 147, 'Certificate', 411.00, 357.00, 411.00, 357.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(442, 148, 'Study', 175.00, 116.00, 175.00, 116.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(443, 148, 'Registration', 874.00, 350.00, 874.00, 350.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(444, 148, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(445, 149, 'Study', 406.00, 103.00, 406.00, 103.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(446, 149, 'Registration', 1126.00, 257.00, 1126.00, 257.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(447, 149, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(448, 150, 'Study', 243.00, 68.00, 243.00, 68.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(449, 150, 'Registration', 1288.00, 152.00, 1288.00, 152.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(450, 150, 'Certificate', 1358.00, 601.00, 1358.00, 601.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(451, 151, 'Study', 406.00, 103.00, 406.00, 103.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(452, 151, 'Registration', 1126.00, 257.00, 1126.00, 257.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(453, 151, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(454, 152, 'Study', 121.00, 121.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(455, 152, 'Registration', 1320.00, 92.00, 1320.00, 92.00, 0.00, '2018-12-13 09:33:19', '2018-12-13 09:33:19'),
(456, 152, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(457, 153, 'Study', 406.00, 103.00, 406.00, 103.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(458, 153, 'Registration', 1126.00, 257.00, 1126.00, 257.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(459, 153, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(460, 154, 'Study', 121.00, 121.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(461, 154, 'Registration', 736.00, 432.00, 736.00, 432.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(462, 154, 'Certificate', 514.00, 514.00, 514.00, 514.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(463, 155, 'Study', 406.00, 103.00, 406.00, 103.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(464, 155, 'Registration', 1126.00, 257.00, 1126.00, 257.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(465, 155, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(466, 156, 'Study', 406.00, 103.00, 406.00, 103.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(467, 156, 'Registration', 1126.00, 257.00, 1126.00, 257.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(468, 156, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(469, 157, 'Study', 406.00, 103.00, 406.00, 103.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(470, 157, 'Registration', 1126.00, 257.00, 1126.00, 257.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(471, 157, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(472, 158, 'Study', 406.00, 103.00, 406.00, 103.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(473, 158, 'Registration', 1126.00, 257.00, 1126.00, 257.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(474, 158, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(475, 159, 'Study', 406.00, 103.00, 406.00, 103.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(476, 159, 'Registration', 1126.00, 257.00, 1126.00, 257.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(477, 159, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20');
INSERT INTO `prices` (`id`, `country_id`, `service_type`, `initial_cost`, `additional_cost`, `logo_initial_cost`, `logo_additional_cost`, `tax`, `created_at`, `updated_at`) VALUES
(478, 160, 'Study', 291.00, 204.00, 291.00, 204.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(479, 160, 'Registration', 1987.00, 644.00, 1987.00, 644.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(480, 160, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(481, 161, 'Study', 107.00, 73.00, 107.00, 73.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(482, 161, 'Registration', 304.00, 258.00, 304.00, 258.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(483, 161, 'Certificate', 126.00, 92.00, 126.00, 92.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(484, 162, 'Study', 406.00, 103.00, 406.00, 103.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(485, 162, 'Registration', 1126.00, 257.00, 1126.00, 257.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(486, 162, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(487, 163, 'Study', -1.00, -1.00, -1.00, -1.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(488, 163, 'Registration', 1182.00, 598.00, 1182.00, 598.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(489, 163, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(490, 164, 'Study', 223.00, 223.00, 223.00, 223.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(491, 164, 'Registration', 1771.00, 506.00, 2553.00, 506.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(492, 164, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(493, 165, 'Study', 406.00, 103.00, 406.00, 103.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(494, 165, 'Registration', 1126.00, 257.00, 1126.00, 257.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(495, 165, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(496, 166, 'Study', 175.00, 116.00, 175.00, 116.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(497, 166, 'Registration', 1012.00, 1012.00, 1012.00, 1012.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(498, 166, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(499, 167, 'Study', 175.00, 116.00, 175.00, 116.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(500, 167, 'Registration', 782.00, 644.00, 782.00, 644.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(501, 167, 'Certificate', 388.00, 369.00, 388.00, 369.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(502, 168, 'Study', 406.00, 103.00, 406.00, 103.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(503, 168, 'Registration', 1126.00, 257.00, 1126.00, 257.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(504, 168, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(505, 169, 'Study', 406.00, 103.00, 406.00, 103.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(506, 169, 'Registration', 1126.00, 257.00, 1126.00, 257.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(507, 169, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(508, 170, 'Study', 121.00, 121.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(509, 170, 'Registration', 883.00, 534.00, 883.00, 534.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(510, 170, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(511, 171, 'Study', 116.00, 116.00, 116.00, 116.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(512, 171, 'Registration', -1.00, -1.00, -1.00, -1.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(513, 171, 'Certificate', -1.00, -1.00, -1.00, -1.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(514, 172, 'Study', 78.00, 78.00, 116.00, 116.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(515, 172, 'Registration', 1895.00, 1868.00, 1895.00, 1868.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(516, 172, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(517, 173, 'Study', 121.00, 121.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(518, 173, 'Registration', 920.00, 566.00, 920.00, 566.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(519, 173, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(520, 174, 'Study', 108.00, 108.00, 108.00, 108.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(521, 174, 'Registration', 791.00, 87.00, 791.00, 87.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(522, 174, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(523, 175, 'Study', 243.00, 243.00, 243.00, 243.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(524, 175, 'Registration', 1996.00, 1886.00, 1996.00, 1886.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(525, 175, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(526, 176, 'Study', 406.00, 103.00, 406.00, 103.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(527, 176, 'Registration', 1126.00, 257.00, 1126.00, 257.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(528, 176, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(529, 177, 'Study', 406.00, 103.00, 406.00, 103.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(530, 177, 'Registration', 1126.00, 257.00, 1126.00, 257.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(531, 177, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(532, 178, 'Study', 78.00, 78.00, 78.00, 78.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(533, 178, 'Registration', 653.00, 138.00, 653.00, 138.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(534, 178, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(535, 179, 'Study', 65.00, 65.00, 65.00, 65.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(536, 179, 'Registration', 689.00, 46.00, 689.00, 46.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(537, 179, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(538, 180, 'Study', 121.00, 78.00, 121.00, 78.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(539, 180, 'Registration', 736.00, 644.00, 736.00, 644.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(540, 180, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(541, 181, 'Study', 72.00, 72.00, 72.00, 72.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(542, 181, 'Registration', 519.00, 476.00, 1039.00, 950.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(543, 181, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(544, 182, 'Study', 97.00, 97.00, 97.00, 97.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(545, 182, 'Registration', 745.00, 653.00, 745.00, 653.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(546, 182, 'Certificate', 378.00, 340.00, 378.00, 340.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(547, 183, 'Study', 97.00, 97.00, 97.00, 97.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(548, 183, 'Registration', 745.00, 653.00, 745.00, 653.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(549, 183, 'Certificate', 378.00, 340.00, 378.00, 340.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(550, 184, 'Study', 78.00, 78.00, 78.00, 78.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(551, 184, 'Registration', 1780.00, 658.00, 1780.00, 658.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(552, 184, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(553, 185, 'Study', 121.00, 121.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(554, 185, 'Registration', 984.00, 359.00, 984.00, 359.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(555, 185, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(556, 186, 'Study', 243.00, 68.00, 243.00, 68.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(557, 186, 'Registration', 1288.00, 152.00, 1288.00, 152.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(558, 186, 'Certificate', 1358.00, 601.00, 1358.00, 601.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(559, 187, 'Study', 406.00, 103.00, 406.00, 103.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(560, 187, 'Registration', 1126.00, 257.00, 1126.00, 257.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(561, 187, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(562, 188, 'Study', 121.00, 121.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(563, 188, 'Registration', 754.00, 727.00, 754.00, 727.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(564, 188, 'Certificate', 388.00, 0.00, 388.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(565, 189, 'Study', 116.00, 78.00, 116.00, 78.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(566, 189, 'Registration', 856.00, 534.00, 856.00, 534.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(567, 189, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(568, 190, 'Study', 184.00, 184.00, 184.00, 184.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(569, 190, 'Registration', 635.00, 635.00, 635.00, 635.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(570, 190, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(571, 191, 'Study', 243.00, 68.00, 243.00, 68.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(572, 191, 'Registration', 1288.00, 152.00, 1288.00, 152.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(573, 191, 'Certificate', 1358.00, 601.00, 1358.00, 601.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(574, 192, 'Study', 121.00, 121.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(575, 192, 'Registration', 842.00, 685.00, 842.00, 685.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(576, 192, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(577, 193, 'Study', 175.00, 116.00, 175.00, 175.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(578, 193, 'Registration', 920.00, 497.00, 920.00, 685.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(579, 193, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(580, 194, 'Study', 78.00, 78.00, 78.00, 78.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(581, 194, 'Registration', 598.00, 129.00, 598.00, 129.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(582, 194, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(583, 195, 'Study', 121.00, 121.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(584, 195, 'Registration', 902.00, 805.00, 902.00, 805.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(585, 195, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(586, 196, 'Study', 121.00, 121.00, 121.00, 121.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(587, 196, 'Registration', 1040.00, 920.00, 1040.00, 920.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(588, 196, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(589, 197, 'Study', 78.00, 58.00, 78.00, 58.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(590, 197, 'Registration', 819.00, 331.00, 819.00, 331.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(591, 197, 'Certificate', 184.00, 126.00, 184.00, 126.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(592, 198, 'Study', 243.00, 68.00, 243.00, 68.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(593, 198, 'Registration', 1288.00, 152.00, 1288.00, 152.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(594, 198, 'Certificate', 1358.00, 601.00, 1358.00, 601.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(595, 199, 'Study', 406.00, 103.00, 406.00, 103.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(596, 199, 'Registration', 1126.00, 257.00, 1126.00, 257.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(597, 199, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(598, 200, 'Study', 116.00, 78.00, 116.00, 78.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(599, 200, 'Registration', 741.00, 400.00, 471.00, 400.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(600, 200, 'Certificate', 0.00, 0.00, 0.00, 0.00, 0.00, '2018-12-13 09:33:20', '2018-12-13 09:33:20');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `trademarks`
--

CREATE TABLE `trademarks` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `case_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `contact_information_id` int(11) NOT NULL,
  `service` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `summary` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` int(11) DEFAULT NULL,
  `filing_number` int(11) DEFAULT NULL,
  `registration_number` int(11) DEFAULT NULL,
  `international_registration_number` int(11) DEFAULT NULL,
  `filing_date` datetime DEFAULT NULL,
  `registration_date` datetime DEFAULT NULL,
  `expiry_date` datetime DEFAULT NULL,
  `classes` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `office` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `office_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_description` text COLLATE utf8mb4_unicode_ci,
  `brand_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purpose` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hvt` tinyint(1) DEFAULT NULL,
  `revocation` tinyint(1) DEFAULT NULL,
  `priority_date` datetime DEFAULT NULL,
  `priority_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priority_number` int(11) DEFAULT NULL,
  `recently_filed` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `commerce` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double NOT NULL,
  `handler_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `atty_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `trademarks`
--

INSERT INTO `trademarks` (`id`, `user_id`, `case_number`, `country_id`, `order_id`, `contact_information_id`, `service`, `type`, `name`, `logo`, `summary`, `number`, `filing_number`, `registration_number`, `international_registration_number`, `filing_date`, `registration_date`, `expiry_date`, `classes`, `office`, `office_status`, `status_description`, `brand_name`, `purpose`, `value`, `category`, `hvt`, `revocation`, `priority_date`, `priority_country`, `priority_number`, `recently_filed`, `commerce`, `amount`, `handler_name`, `atty_name`, `created_at`, `updated_at`) VALUES
(1, 1, 'k13-5S5U', 1, 1, 1, 'Registration', 'Trademark', 'Test', 'N/A', 'Test', 0, 0, 0, 0, '2018-12-17 03:34:33', '2018-12-17 03:34:33', '2018-12-17 03:34:33', '1-43-', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2018-12-17 03:34:33', NULL, 0, 'no', 'no', 827, 'Roland', NULL, '2018-12-13 09:35:31', '2018-12-16 19:35:25'),
(2, 1, 'N13-gK1H', 1, 1, 2, 'Registration', 'Logo Only', 'Another', '48387744_705968783178188_8364142217014542336_n.png', 'Test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7-24-39-41-', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', 'no', 1528, 'Jenny', '', '2018-12-13 09:35:31', '2018-12-13 09:35:31');

-- --------------------------------------------------------

--
-- Table structure for table `trademark_classes`
--

CREATE TABLE `trademark_classes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `trademark_classes`
--

INSERT INTO `trademark_classes` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Chemicals', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(2, 'Paints, Coatings & Pigments', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(3, 'Cleaning Products, Bleaching & Abrasives, Cosmetics', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(4, 'Fuels, Industrial Oils and Greases, Illuminates', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(5, 'Pharmaceutical, Veterinary Products, Dietetic', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(6, 'Metals, metal castings, Locks, Safes, Hardware', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(7, 'Machines and Machine Tools, Parts', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(8, 'Hand Tools and implements, Cutlery', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(9, 'Computers, Software, Electronic instruments, & Scientific appliances', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(10, 'Medical, Dental Instruments and Apparatus', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(11, 'Appliances, Lighting, Heating, Sanitary Installations', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(12, 'Vehicles', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(13, 'Firearms, Explosives and Projectiles', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(14, 'Precious Metal ware, Jewellery', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(15, 'Musical Instruments and supplies', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(16, 'Paper, Items made of Paper, Stationary items', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(17, 'Rubber, Asbestos, Plastic Items', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(18, 'Leather and Substitute Goods', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(19, 'Construction Materials (building - non metallic)', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(20, 'Furniture, Mirrors', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(21, 'Crockery, Containers, Utensils, Brushes, Cleaning Implements', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(22, 'Cordage, Ropes, Nets, Awnings, Sacks, Padding', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(23, 'Yarns, Threads', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(24, 'Fabrics, Blankets, Covers, Textile', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(25, 'Clothing, Footwear and Headgeare', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(26, 'Sewing Notions, Fancy Goods, Lace and Embroidery', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(27, 'Carpets, Linoleum, Wall and Floor Coverings (non textile)', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(28, 'Games, Toys, Sports Equipment', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(29, 'Foods - Dairy, Meat, Fish, Processed & Preserved Foods', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(30, 'Foods - Spices, Bakery Goods, Ice, Confectionery', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(31, 'Fresh Fruit & Vegetables, Live Animals', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(32, 'Beer, Ales, Soft Drinks, Carbonated Waters', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(33, 'Wines, Spirits, Liqueurs', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(34, 'Tobacco, Smokers Requisites & Matches', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(35, 'Advertising, Business Consulting', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(36, 'Insurance, Financial', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(37, 'Construction, Repair, Cleaning', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(38, 'Communications', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(39, 'Transport, Utilities, Storage & Warehousing', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(40, 'Materials Treatment, Working', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(41, 'Education, Amusement, Entertainment, Reproduction', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(42, 'Scientific and technological services and research and design relating thereto', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(43, 'Services for providing food and drink; temporary accommodations', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(44, 'Medical services; veterinary services; hygienic and beauty care for human beings or animals', '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(45, 'Personal and social services rendered by others to meet the needs of individuals', '2018-12-13 09:33:20', '2018-12-13 09:33:20');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middle_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ipaddress` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `middle_name`, `last_name`, `name`, `email`, `email_verified_at`, `password`, `ipaddress`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Test', '', 'Test', 'Test Test', 'test1@gmail.com', NULL, '$2y$10$QfpN3T/G2dnqwQ6acF.fbOcjTHEg1OM1ISiCmCT8G3InlgUIhNIPO', '127.0.0.1', NULL, '2018-12-13 09:33:20', '2018-12-13 09:33:20'),
(2, 'Test', '', 'Test', 'Test Test', 'test2@gmail.com', NULL, '$2y$10$OxoMazSa2Sak2DU9u2jVJOoHFfAht0K9mg84Z8BdwNnzMhwE8smGK', '127.0.0.1', NULL, '2018-12-13 09:33:21', '2018-12-13 09:33:21'),
(3, 'Test', '', 'Test', 'Test Test', 'test3@gmail.com', NULL, '$2y$10$wBVri2/YER226UIDeo/ej.d5/5mMsFKDhQtVsbxoGa5AznltRxeVe', '127.0.0.1', NULL, '2018-12-13 09:33:21', '2018-12-13 09:33:21'),
(4, 'Test', '', 'Test', 'Test Test', 'test4@gmail.com', NULL, '$2y$10$0zndt8rTmwhGr2AFXGwN7uEvj7njlmswlV0VJHfCtybBezqvFFa3q', '127.0.0.1', NULL, '2018-12-13 09:33:21', '2018-12-13 09:33:21'),
(5, 'Test', '', 'Test', 'Test Test', 'test5@gmail.com', NULL, '$2y$10$NOo6or9knbYljjPhroi0yeG2kMek8Iuf84f2uLvTjKN.KWiO34YJ.', '127.0.0.1', NULL, '2018-12-13 09:33:21', '2018-12-13 09:33:21'),
(6, 'Test', '', 'Test', 'Test Test', 'test6@gmail.com', NULL, '$2y$10$Q4zcOgp8d1ows2q6Q/kZzOG.Qu.ugGtMI7K90iR.nFOK/A/gr7klK', '127.0.0.1', NULL, '2018-12-13 09:33:21', '2018-12-13 09:33:21'),
(7, 'Test', '', 'Test', 'Test Test', 'test7@gmail.com', NULL, '$2y$10$cR5zYZ/MwkJnN.0a8Q2zuOeBPSqeMDaQSDUbdP7//06CkdwcYkXwG', '127.0.0.1', NULL, '2018-12-13 09:33:21', '2018-12-13 09:33:21'),
(8, 'Test', '', 'Test', 'Test Test', 'test8@gmail.com', NULL, '$2y$10$FvMvDadd1AHwQzZzv9Xa9.nI7GQiK2wh1YyqbcZJbasGOWV3UvRVa', '127.0.0.1', NULL, '2018-12-13 09:33:21', '2018-12-13 09:33:21'),
(9, 'Test', '', 'Test', 'Test Test', 'test9@gmail.com', NULL, '$2y$10$JtMzVIEkjMnIhMvvYXdm7.BjynmEs7nY2mP.xhINzVs0/WLhvLMVq', '127.0.0.1', NULL, '2018-12-13 09:33:21', '2018-12-13 09:33:21'),
(10, 'Test', '', 'Test', 'Test Test', 'test10@gmail.com', NULL, '$2y$10$Y0coRnKpuAGDi7BQE9ENdeZ.dv.6nigVVHYN6u96N4Ygxo4B1T/Lq', '127.0.0.1', NULL, '2018-12-13 09:33:21', '2018-12-13 09:33:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_config`
--
ALTER TABLE `admin_config`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_config_name_unique` (`name`);

--
-- Indexes for table `admin_menu`
--
ALTER TABLE `admin_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_operation_log`
--
ALTER TABLE `admin_operation_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_operation_log_user_id_index` (`user_id`);

--
-- Indexes for table `admin_permissions`
--
ALTER TABLE `admin_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_permissions_name_unique` (`name`);

--
-- Indexes for table `admin_roles`
--
ALTER TABLE `admin_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_roles_name_unique` (`name`);

--
-- Indexes for table `admin_role_menu`
--
ALTER TABLE `admin_role_menu`
  ADD KEY `admin_role_menu_role_id_menu_id_index` (`role_id`,`menu_id`);

--
-- Indexes for table `admin_role_permissions`
--
ALTER TABLE `admin_role_permissions`
  ADD KEY `admin_role_permissions_role_id_permission_id_index` (`role_id`,`permission_id`);

--
-- Indexes for table `admin_role_users`
--
ALTER TABLE `admin_role_users`
  ADD KEY `admin_role_users_role_id_user_id_index` (`role_id`,`user_id`);

--
-- Indexes for table `admin_users`
--
ALTER TABLE `admin_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_users_username_unique` (`username`);

--
-- Indexes for table `admin_user_permissions`
--
ALTER TABLE `admin_user_permissions`
  ADD KEY `admin_user_permissions_user_id_permission_id_index` (`user_id`,`permission_id`);

--
-- Indexes for table `class_descriptions`
--
ALTER TABLE `class_descriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_informations`
--
ALTER TABLE `contact_informations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `continents`
--
ALTER TABLE `continents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `formats`
--
ALTER TABLE `formats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inquiries`
--
ALTER TABLE `inquiries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laravel_exceptions`
--
ALTER TABLE `laravel_exceptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `prices`
--
ALTER TABLE `prices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `trademarks`
--
ALTER TABLE `trademarks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trademark_classes`
--
ALTER TABLE `trademark_classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_config`
--
ALTER TABLE `admin_config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admin_menu`
--
ALTER TABLE `admin_menu`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `admin_operation_log`
--
ALTER TABLE `admin_operation_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT for table `admin_permissions`
--
ALTER TABLE `admin_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `admin_roles`
--
ALTER TABLE `admin_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admin_users`
--
ALTER TABLE `admin_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `class_descriptions`
--
ALTER TABLE `class_descriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=713;

--
-- AUTO_INCREMENT for table `contact_informations`
--
ALTER TABLE `contact_informations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `continents`
--
ALTER TABLE `continents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=201;

--
-- AUTO_INCREMENT for table `formats`
--
ALTER TABLE `formats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `inquiries`
--
ALTER TABLE `inquiries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `laravel_exceptions`
--
ALTER TABLE `laravel_exceptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=924;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `prices`
--
ALTER TABLE `prices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=601;

--
-- AUTO_INCREMENT for table `trademarks`
--
ALTER TABLE `trademarks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `trademark_classes`
--
ALTER TABLE `trademark_classes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
