<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    public function country()
    {
    	return $this->belongsTo( Country::class );
    }

    public static function getPrice( $price )
    {
    	if ( $price == 0 ) {

        	$price = 'Free';
        } else if ( $price == -1 ) {

        	$price = 'TBA';
        } else {

            $price = '$' . number_format( $price, 2);
        }

        return $price;
    }

    /**
     * Loop each price then format
     *
     * @param Price price
     * @return Price price
     */
    public static function format_price( $prices )
    {
        foreach ( $prices as $price ) {
            $price['initial_cost']         = Price::getPrice( $price['initial_cost'] );
            $price['additional_cost']      = Price::getPrice( $price['additional_cost'] );
            $price['logo_initial_cost']    = Price::getPrice( $price['logo_initial_cost'] );
            $price['logo_additional_cost'] = Price::getPrice( $price['logo_additional_cost'] );
        }

        return $prices;
    }

}
