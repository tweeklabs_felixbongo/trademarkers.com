<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Country;
use App\Price;

class ElasticSearch extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'trade_mark_type';
}
