<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    //

    // GET BANNER
    public static function getBanner()
    {
  
        $banner = Banner::whereDate( 'expiry_date', '>=' , now() )
                            ->where('status','active')->first();

        return $banner;
    }
}
