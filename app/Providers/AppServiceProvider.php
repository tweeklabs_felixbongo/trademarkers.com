<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Encore\Admin\Config\Config;
use \App\Trademark;
use Encore\Admin\Admin;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        if (class_exists(Config::class)) {
            Config::load();
        }

        view()->composer('layouts.header', function ( $view ) {

            $view->with('featured_countries', \App\Country::featured() );
            $view->with('featured_continent', \App\Continent::getContinents() );
            $view->with('cart_contents', \App\Cart::count_user_cart() );
            $view->with('banner', \App\Banner::getBanner() );
            
        } );

        view()->composer('layouts.footer', function ( $view ) {

            $view->with('banner', \App\Banner::getBanner() );
            
        } );

        view()->composer('layouts.customer-nav', function ( $view ) {
            
            $user = auth()->user();
            $trademarks = Trademark::where( 
                [ 'user_id' => $user->id ] )->whereNotNull('office_status')->get();
           
            $view->with('trademark_count', count( $trademarks ) );
        } );

        Admin::booting(function () {
            Admin::js('vendor/laravel-admin-ext/action/datatables.js');
            Admin::js('vendor/laravel-admin-ext/action/action.js');
            Admin::css('vendor/laravel-admin-ext/action/datatables.min.css');
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
