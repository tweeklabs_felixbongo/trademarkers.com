<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Brand extends Model
{
    protected $fillable = [
        'user_id',
        'assigned_user_id',
        'brand',
        'notes',
        'purpose',
        'category',
        'value'
    ];

    // public function user()
    // {
    // 	return $this->belongsTo( User::class );
    // }

    public function user()
    {
    	return $this->hasOne( AdminUser::class , 'id' , 'user_id');
    }

    public function assignedUser()
    {
    	return $this->belongsTo( User::class, 'assigned_user', 'id' );
    }

    public function country() {

        return $this->hasOne( Country::class );
    }

    public function domains()
    {
        return $this->hasMany( BrandDomain::class );
    }

    public function events()
    {
        return $this->hasMany( Event::class );
    }

    // public function brand()
    // {
    //     return $this->hasMany( BrandDomain::class );
    // }

  

}
