<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cohensive\Embed\Facades\Embed;

class ActionCodeReferralTransaction extends Model
{
    protected $fillable = [
        'referrals_id',
        'ref_invite_id',
        'amount',
        'description',
        'payment_method',
        'info_name',
        'info_email',
        'information',
        'status'
        ];
    
}
