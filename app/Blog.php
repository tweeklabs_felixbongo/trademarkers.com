<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cohensive\Embed\Facades\Embed;
use Laravelista\Comments\Commentable;

class Blog extends Model
{
    use Commentable;
    
    public function author()
    {
        return $this->hasOne( AdminUser::class, 'id', 'author_id' );
    }
}
