<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Attachment extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'trademark_id', 'name', 'description', 'file_size', 'file_type'
    ];

    public function trademark()
    {
    	return $this->belongsTo( Trademark::class );
    }

    public function user()
    {
    	return $this->belongsTo( User::class );
    }

    public static function get_user_attachment( $attachment_id, $trademark_id )
    {
        $trademark = Trademark::find( $trademark_id );

        if ( !$trademark ) {
            
            return false;
        }

        if ( $trademark->user_id != auth()->user()->id ) {

            return false;
        }

        $attachment = Attachment::where( [ 'id' => $attachment_id, 'trademark_id' => $trademark_id  ] )->get();

        if ( count( $attachment ) < 1 ) {

            return false;
        }

        return $attachment;
    }

    public static function upload_attachment( $request )
    {
    	$filename = Attachment::move_file( $request->attachment );

    	$attachment = Attachment::create( [
    		'user_id'      => auth()->user()->id,
    		'trademark_id' => $request->trademark_id,
    		'name'         => $filename,
    		'description'  => $request->attachment->getClientOriginalName(),
    		'file_size'    => '',
    		'file_type'    => '',
    	] );

    	return $attachment;
    }

    public static function move_file( $file )
    {
        $filename = md5( time() ) . '.' . $file->getClientOriginalExtension();

        $file->move( public_path('uploads'), $filename );

        return $filename;
    }

    public static function retrieve_attachments( $trademark_id )
    {
    	$attachments = Attachment::where( ['trademark_id' => $trademark_id ] )->get();

    	return $attachments;
    }

    public static function download_attachment( $filename )
    {
        $storage = Storage::disk('admin');

        return $storage->download( $filename, $filename );
    }
}
