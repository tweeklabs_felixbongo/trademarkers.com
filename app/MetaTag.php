<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MetaTag extends Model
{
    protected $fillable = [
        'uri',
        'title',
        'description',
        'keywords'
    ];

    public function getMeta($slug)
    {
        return $this->where('uri', $slug)->first();
    }
}
