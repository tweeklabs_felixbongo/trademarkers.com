<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class EmailTracker extends Model
{

    protected $fillable = [
        'email_id',
        'event_id',
        'domain_id',
        'status'
    ];

    public function tracker()
    {
    	return $this->hasOne( BrandDomain::class );
    }

    public function event()
    {
    	return $this->belongsTo( Event::class );
    }


    public function scopeGetPerStatus($query, $status)
    {
        return  $query->where('status', $status);
    }

    // public function scopeGetNotPerStatus($query, $status)
    // {
    //     return  $query->where('status', '<>', $status);
    // }

}
