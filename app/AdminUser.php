<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cohensive\Embed\Facades\Embed;
use Encore\Admin\Facades\Admin;

class AdminUser extends Model
{
    protected $table = 'admin_users';

    public function author()
    {
        return $this->hasOne( Admin::class, 'id', 'author_id' );
    }
}
