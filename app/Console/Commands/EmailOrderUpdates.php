<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use App\Services\CronNotifyService;

class EmailOrderUpdates extends Command
{
    public $service;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orderemail:send {--self=} {--date=}';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Notifications';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        \App\Services\CronNotifyService $service
    )
    { 
        parent::__construct();
        $this->service = $service;
        // $this->service->notifyOfficeStatus();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // echo "this";
        $this->service->notifyOfficeStatus();
        return 'sent';
    }

    
}
