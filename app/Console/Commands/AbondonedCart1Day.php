<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use App\Services\CronNotifyService;

class AbondonedCart1Day extends Command
{
    public $service;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'abondonedcart1day:send {--self=} {--date=}';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'abondoned cart 1 day old notification';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        \App\Services\CronNotifyService $service
    )
    { 
        parent::__construct();
        $this->service = $service;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // echo "this";
        $this->service->notifyAbondonedCart1Day();
        return 'sent';
    }

    
}
