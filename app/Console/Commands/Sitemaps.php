<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\SitemapGenerator;
use Spatie\Sitemap\SitemapIndex;
use Spatie\Sitemap\Tags\Url;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class Sitemaps extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:generate {--self=} {--date=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate daily basis sitemap';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $date = $this->option('date');
        $self = $this->option('self');

        if ( $self ) {

            $this->self_generate();
        }

        else if ( $date ) {

            if ( preg_match("/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/", $date) ) {

                $date1 = Carbon::createFromFormat('Y-m-d', $date);

                $date2 = $date1->month . "/" . $date1->day . "/" . substr( $date1->year, 2 );

                $date = $this->generate_sitemap( $date1, $date2 );

            } else {

                $date = "Invalid date format";
            }

        } else {

            $base_date = Carbon::create( 2017, 8, 15 );

            $now_date = Carbon::now();

            $interval = 604;

            $x = $now_date->diffInDays( $base_date );

            $now_date->subDays( $x + ( $x - $interval ) );

            $date = $now_date->month . "/" . $now_date->day . "/" . substr( $now_date->year, 2 );

            $date = $this->generate_sitemap( $now_date, $date );
        }

        $this->generate_sitemap_index();

        $this->info( $date );
    }

    public function generate_sitemap( $date1, $date2 )
    {
        ini_set('memory_limit', '512M');

        $database  = DB::connection('sitemap');

        $pattern   = '/[^\p{L}\p{N}]/u';
        $pattern1  = '/-+/';

        $q_date    = $date1->format('Y-m-d');

        $file_name = $q_date .  "-details" . ".xml";

        $sitemap   = Sitemap::create();

        $result    = $database->table('details')->where('reg_date', 'like', '%' . $q_date . '%')->get();

        // $result    = $database->table('details')->whereBetween('reg_date',['2017-08-15', '2017-12-30'])->get();

        if ( count( $result ) > 0 ) {

            foreach ( $result as $res ) {

                $new_name  = $this->escape_characters( $res->name );
                $new_orga  = $this->escape_characters( $res->applicant_orga );

                if ( strlen( $new_name ) == 0 || strlen( $new_orga ) == 0 ) {

                    continue;
                }
                
                if ( strlen( $res->number ) != 9 ) {

                    $res->number = "0" . $res->number; 
                }

                $url_name  = "https://trademarkers.com/EUIPO/" . $res->number . "/";

                $url_name .= preg_replace( "/(.)\\1+/", "$1", "trademark-" . $new_name  . "-" );
                
                $url_name .= preg_replace( "/(.)\\1+/", "$1", "granted-to-" . $new_orga );

                $url_name    = rtrim( $url_name,'-' );

                $length = $this->review_url( $url_name );

                if ( $length < 20000 ) {

                    continue;
                }

                $sitemap->add( $url_name );
            }

            $sitemap->writeToFile( base_path("sitemaps/" . $file_name) );

            return "Successfully generated sitemap on " . $date1->format('Y-m-d');
        }

        return "There is no data available on this date " . $date1->format('Y-m-d');
    }

    public function generate_sitemap_index()
    {
        $files        = Storage::disk('sitemap')->files();
        $sitemapindex = SitemapIndex::create();

        foreach ( $files as $file ) {
            
            $x = explode('.xml', $file);

            if ( count( $x ) > 1 ) {

                if ( $file == "sitemap-index.xml" || $file == "sitemapindex.xml" ) {

                    continue;
                }

                $sitemapindex->add( 'https://trademarkers.com/sitemaps/' . $file  );
            }
        }

        $sitemapindex->writeToFile( base_path("sitemaps/") . "sitemap-index.xml"  );
    }

    public function old_generate_sitemap_index()
    {
        $files        = Storage::disk('old_sitemap')->files();
        $sitemapindex = SitemapIndex::create();

        foreach ( $files as $file ) {
            
            $x = explode('.xml', $file);

            if ( count( $x ) > 1 ) {

                if ( $file == "sitemap-index.xml" || $file == "sitemapindex.xml" ) {

                    continue;
                }

                $sitemapindex->add( 'https://trademarkers.com/sitemaps/' . $file  );
            }
        }

        $sitemapindex->writeToFile( base_path("sitemaps/") . "sitemapindex.xml"  );
    }

    public function self_generate()
    {
        SitemapGenerator::create('https://trademarkers.com')->writeToFile( base_path("sitemaps/") . "index.xml" );
    }

    public function escape_characters( $string )
    {
        $clean    = preg_replace('/[^\w\p{L}\p{N}]/u', '-' , $string);
        $initial  = preg_replace("/(.)\\1+/", "$1", $clean);
        $final    = rtrim( $initial,'-' );

        return $final;
    }

    public function review_url( $url )
    {
        $ch = curl_init(); 

        curl_setopt( $ch, CURLOPT_URL, $url); 

        curl_setopt($ch, CURLOPT_HEADER, TRUE);

        curl_setopt($ch, CURLOPT_NOBODY, TRUE);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 

        curl_exec($ch); 

        $length = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD); 

        curl_close($ch);

        return $length;
    }
}
