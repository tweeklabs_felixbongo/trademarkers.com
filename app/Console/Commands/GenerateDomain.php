<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use App\Services\CronGenerateDomainService;

class GenerateDomain extends Command 
{
    public $service;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generateDomain:create {--self=} {--date=}';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create domains';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        \App\Services\CronGenerateDomainService $service
    )
    { 
        parent::__construct();
        $this->service = $service;
        // $this->service->notifyOfficeStatus();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // echo "this";
        $this->service->generateDomain();
        return 'generate domain';
    }

    
}
