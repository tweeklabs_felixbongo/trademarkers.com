<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Sitemaps::class,
        Commands\EmailOrderUpdates::class,
        Commands\AbondonedCart4Days::class,
        Commands\AbondonedCart1Day::class,
        Commands\EmailLayout::class,
        Commands\EmailGenerate::class,
        Commands\GenerateDomain::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('sitemap:generate')->daily();
        $schedule->command('orderemail:send')->dailyAt('6:10');
        // THIS WAS CHANGE TO 1 DAY OLD
        $schedule->command('abondonedcart4hours:send')->hourly();
        $schedule->command('abondonedcart1day:send')->dailyAt('6:25');
        $schedule->command('abondonedcart4days:send')->dailyAt('6:35');
        $schedule->command('abondonedcart1month:send')->monthlyOn(5, '5:50');

        // CHECKING OF EMAILS FROM EMAIL LAYOUT SEND 1 EMAIL EACH FROM A DOMAIN
        $schedule->command('emaillayout:send')->everyFifteenMinutes();
        $schedule->command('emailgenerate:send')->hourly();
        $schedule->command('generateDomain:create')->hourly();

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
