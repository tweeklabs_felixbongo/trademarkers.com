<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class EventType extends Model
{


    public function scopeGetShowEvent($query)
    {
        return  $query->where('event_show', 'yes');
    }
  

}
