<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class BrandLead extends Model
{
    protected $fillable = [
        'brand_id',
        'lead_id'
    ];

    public function domains()
    {
        return $this->hasOne( Brand::class );
    }

    public function case()
    {
        return $this->hasOne( ActionCodeCase::class , 'id', 'lead_id' );
    }

    public function emails()
    {
        return $this->hasMany(DomainEventEmail::class);
    }

    // mutators
    public function getBrandIdAttribute($value)
    {
        return explode(',', $value);
    }

    public function setBrandIdAttribute($value)
    {
        $this->attributes['brand_id'] = implode(',', $value);
    }

    // SCOPE
    // public function scopeGetLead($query, $brandId)
    // {
    //     dd($this->brand_id);
    //     // return  $query->where('email_flag', 'no');
    // }


}
