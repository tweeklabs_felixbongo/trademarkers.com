<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class DomainEventEmail extends Model
{

    protected $fillable = [
        'email',
        'notes',
        'brand_domain_id',
        'action_code_case_id',
        'events_id',
    ];

    public function brand()
    {
    	return $this->belongsTo( BrandDomain::class );
    }

    public function case()
    {
    	return $this->belongsTo( ActionCodeCase::class, 'id', 'brand_lead_id' );
    }

    // public function object()
    // {
    //     return $this->morphTo();
    // }

    public function tracker()
    {
    	return $this->hasOne( EmailTracker::class , 'event_id', 'id' );
    }

    public function scopeGetPerStatus($query, $status)
    {
        return  $query->where('status', $status);
    }

    public function scopeHasTracker($query) 
    {
        return  $query->whereNull('tracker');
    }

    // public function scopeGetNotPerStatus($query, $status)
    // {
    //     return  $query->where('status', '<>', $status);
    // }

}
