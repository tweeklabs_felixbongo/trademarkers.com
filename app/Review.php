<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $fillable = [
        'user_id',
        'rating',
        'comment'
    ];

    public function addReview( $data )
    {
        return Review::create([
            'user_id' => $data['user_id'],
            'rating' => $data['rating'],
            'comment' => $data['comment']
        ]);
    }

    public function user()
    {
        return $this->belongsTo( User::class );
    }

}
