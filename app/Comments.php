<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'trademark_id', 'description'
    ];

    protected $table = 'comment_admins';

    public function user()
    {
        return $this->belongsTo( User::class );
    }

    public function trademark()
    {
    	return $this->belongsTo( Trademark::class );
    }

    public static function create_comment( $request )
    {
        $comment = Comment::create( [
            'trademark_id' => $request->trademark_id,
            'description'  => $request->description,
            'user_id'      => auth()->user()->id
        ] );

        return $comment;
    }

    public static function retrieve_comments( $trademark_id )
    {
        $comments = Comment::where( ['trademark_id' => $trademark_id ] )->orderBy('id', 'desc')->get();

        return $comments;
    }
}
