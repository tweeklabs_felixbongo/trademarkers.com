<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Barryvdh\PDF;

class Invoice extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'name', 'path', 'order_id', 'user_id', 'status', 'reference', 'payment_id', 'billing', 'created_at'
    ];

    public function order()
    {
    	return $this->belongsTo( Order::class );
    }

    public function user()
    {
    	return $this->belongsTo( User::class );
    }

    public function payment()
    {
        return $this->belongsTo( Payment::class );
    }

    public static function get_user_invoice( $invoice_id )
    {

        if ( auth()->user()->id == 2 ) {

            $invoice = Invoice::where( [ 'id' => $invoice_id] )->get();

        } else {

            $invoice = Invoice::where( [ 'id' => $invoice_id, 'user_id' => auth()->user()->id ] )->get();
        }
        
        return $invoice;
    }

    public static function create_invoice( $order, $status, $data )
    {
        $invoice_name   = 'Invoice of Order #' . $order->order_number;
        $filename       = storage_path('invoices/') . md5( uniqid() . time() ) . '.pdf';
        $invoice_number = uniqid();

        if ( $data ) {

            $data = json_encode( $data );
        }
        
        for ( $i = 0;  $i < 5 ;  $i++ ) {

            $invoice_number = Invoice::generate_invoice_number();
            $result = Invoice::check_invoice_number( $invoice_number );

            if ( $result ) {
                break;
            }
        }

        $invoice = Invoice::create( [
            'reference'=> $invoice_number,
            'name'     => $invoice_name,
            'path'     => $filename,
            'order_id' => $order->id,
            'user_id'  => $order->user->id,
            'payment_id' => $order->payment->id,
            'billing'    => $data,
            'status'     => $status,
            'created_at' => $order->created_at
        ] );

        return $invoice;
    }

    public static function validate_billing_info( $order )
    {
        $order['billing_name']    = "";
        $order['billing_address'] = "";
        $order['billing_state']   = "";
        $order['billing_zip']     = "";
        $order['billing_city']    = "";
        $order['billing_country'] = "";

        if ( $order->invoice->billing ) {

            $info = json_decode( $order->invoice->billing );

            $order['billing_name']    = strtoupper( $info->name );
            $order['billing_address'] = strtoupper( $info->address );
            $order['billing_state']   = strtoupper( $info->state );
            $order['billing_zip']     = strtoupper( $info->postal_code );
            $order['billing_city']    = strtoupper( $info->city );
            $order['billing_country'] = strtoupper( $info->country );
            $order['billing_phone_number'] = strtoupper( $info->phone_number );
        } 

        else {

            if ( $order->trademarks[0]->profile ) {
                $order['billing_name']    = strtoupper( $order->trademarks[0]->profile->first_name . ' ' . $order->trademarks[0]->profile->last_name );
                $order['billing_address'] = strtoupper( $order->trademarks[0]->profile->house );
                $order['billing_state']   = strtoupper( $order->trademarks[0]->profile->state );
                $order['billing_zip']     = strtoupper( $order->trademarks[0]->profile->zip_code );
                $order['billing_city']    = strtoupper( $order->trademarks[0]->profile->city );
                $order['billing_country'] = strtoupper( $order->trademarks[0]->profile->country );
                $order['billing_phone_number'] = strtoupper( $order->trademarks[0]->profile->phone_number );
            }
        }

        return $order;
    }

    public static function create_invoice_old( $order, $status, $data )
    {
        $invoice_name   = 'Invoice of Order #' . $order->order_number;
        $filename       = storage_path('invoices/') . md5( uniqid() . time() ) . '.pdf';
        $invoice_number = uniqid();

        if ( $data ) {

            $data = json_encode( $data );
        }
        
        for ( $i = 0;  $i < 5 ;  $i++ ) {

            $invoice_number = Invoice::generate_invoice_number();
            $result = Invoice::check_invoice_number( $invoice_number );

            if ( $result ) {
                break;
            }
        }

        $invoice = Invoice::create( [
            'reference'=> $invoice_number,
            'name'     => $invoice_name,
            'path'     => $filename,
            'order_id' => $order->id,
            'user_id'  => $order->user->id,
            'payment_id' => $order->payment->id,
            'billing'    => $data,
            'status'     => $status
        ] );

        return $invoice;
    }

    public static function generate_invoice_pdf( $order, $filename )
    {
        try {

            $pdf = app('dompdf.wrapper')->loadView( 'invoice', ['order' => $order ] )->save( $filename );

        } catch ( Exception $e ) {

            return false;
        }

        return $pdf;
    }

    public static function download_invoice_pdf( $invoice_id, $type )
    {
        $invoice = Invoice::get_user_invoice( $invoice_id );
        $order   = Order::findOrFail($invoice[0]->order_id);

        $order = Invoice::validate_billing_info( $order );

        $pdf = app('dompdf.wrapper')->loadView( 'invoice', ['order' => $order, 'status' => $invoice[0]->status] );

        if ( $type == 'stream' ) {

            return $pdf->stream( $invoice[0]->name . '.pdf');
        }

        if ( $type == 'download' ) {

            return $pdf->download( $invoice[0]->name . '.pdf');
        }

        return $pdf->stream( $invoice[0]->name . '.pdf');
    }

    public static function generate_invoice_number()
    {
        // Array of uppercase alphabets from A-Z
        $alphabets = [
            'B','D','U','S','Y',
            'F','H','I','J','W',
            'L','A','P','Q','R',
            'V','C','G','E','O',
            'X','Z','K','T','M',
            'N',
        ];

        // Array of numbers from zero to nine
        $numbers = [
            '0','3','6','9','4',
            '8','7','1','5','2'
        ];

        $alphanumeric = array_merge( $alphabets, $numbers );

        $first  = $alphanumeric[ rand(0, 35) ];
        $second = $alphanumeric[ rand(0, 35) ];
        $third  = $alphanumeric[ rand(0, 35) ];
        $fourth = $numbers[ rand(1, 9) ];
        $fifth  = $numbers[ rand(0, 9) ];
        $sixth  = $numbers[ rand(0, 9) ];
        $seventh= $numbers[ rand(0, 9) ];
        $eight  = $numbers[ rand(0, 9) ];
        $ninth  = $numbers[ rand(0, 9) ];

        return $first . $second . $third . "-" . $fourth . $fifth . $sixth . $seventh . $eight . $ninth;
    }

    public static function check_invoice_number( $invoice_number )
    {
        $result = Invoice::where('reference', $invoice_number)->get();

        if ( count( $result->toArray() ) == 0 ) {

            return false;
        }

        return true;
    }
}
