<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class CartCoupon extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'promo_code_id',
        'trademark_id',
        'user_id',
        'cart_id',
        'order_id',
    ];

    public function code()
    {
        return $this->hasOne( ActionCodePromo::class, 'id','promo_code_id' );
    }

    public static function addOrderId($orderId)
    {
        $this->order_id = $orderId;
        $this->save();
    }

}
