<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \App\Price;
use \App\CountryDiscount;

class Country extends Model
{
    public function continent() {

    	return $this->belongsTo( Continent::class );
    }

    public function trademarks() {

        return $this->hasMany( Trademark::class );
    }

    public function prioritycost() {

        return $this->hasOne( PriorityCost::class );
    }

    public function promos()
    {
        return $this->hasMany( ActionCodePromo::class );
    }

    public static function featured() {

    	$countries = Country::where('is_feature', '=' , 1)
    					->orderByRaw('name desc')
    					->get();

    	return $countries;
    }

    public function prices() {
    	
    	return $this->hasMany( Price::class );
    }

    public function discount() {
    	
    	return $this->hasOne( CountryDiscount::class );
    }

    public static function getCountries() {

        return Country::where('is_treaty', 0)->orderByRaw('name asc')->get();
    }

    public static function getParisConvetionCountries() {

        return Country::where('is_treaty', 0)->orderByRaw('name asc')->get();
    }

    public static function getAllCountries() {

        return Country::orderByRaw('name asc')->get();
    }

    public static function countries_pricing() {

        $countries = Country::getAllCountries();

        foreach ( $countries as $country ) {

            $country->prices = Price::format_price( $country->prices );
        }

        return $countries;
    }

    /**
     * Validate if a given country code is valid
     *
     * @param country code
     * @return Country country
     */
    public static function validate_country_code( $country_code )
    {
        // Select country code in country table
        $country  = Country::where( 'abbr', '=', $country_code )->get();

        // Check if the return value is empty, then redirect to the home page
        if ( count( $country ) == 0 ) {
            $country['redirect'] = true;
            return $country;
        }

        // Assign the country
        $country = $country[0];

        $country->prices = Price::format_price( $country->prices );

        $euipo = array(
            0 => 'BE', // Belgium
            1 => 'BG', // Bulgaria
            2 => 'DK', // Denmark
            4 => 'DE', // Germany
            5 => 'EE', // Estonia
            6 => 'FI', // Finland
            7 => 'FR', // France
            8 => 'GR', // Greece
            9 => 'GB', // Great Britain
            10 => 'IE', // Ireland
            11 => 'IT', // Italy
            12 => 'LV', // Latvia
            13 => 'LT', // Lithuania
            14 => 'LU', // Luxembourg
            15 => 'MT', // Malta
            16 => 'NL', // Netherlands
            17 => 'AT', // Austria
            18 => 'PT', // Portugal
            19 => 'RO', // Romania
            20 => 'SE', // Sweden
            21 => 'SK', // Slovakia
            22 => 'SI', // Slovenia
            23 => 'ES', // Spain
            24 => 'CZ', // Czech Republic
            25 => 'HU', // Hungary
            26 => 'CY', // Cyprus
            27 => 'HR', // Croatia
        );

        $benelux = [
            'BE', // Belgium
            'LU', // Luxembourg
            'NL', // Netherlands
        ];

        $eu = array_search( $country['abbr'] , $euipo);
        $be = array_search( $country['abbr'] , $benelux);

        if ( $eu !== false ) {

            $country['eu'] = true;
        }

        if ( $be !== false ) {

            $country['be'] = true;
        }

        return $country;
    }

    /**
     * Validate if a given country name is valid
     *
     * @param country name
     * @return Country country
     */
    public static function validate_country_name( $country_name )
    {
        // Select country code in country table
        $country  = Country::where( 'name', 'like', $country_name )->get();

        // Check if the return value is empty, then redirect to the home page
        if ( count( $country ) == 0 ) {
            $country['redirect'] = true;
            return $country;
        }

        // Assign the country
        $country = $country[0];

        $country->prices = Price::format_price( $country->prices );

        $euipo = array(
            0 => 'BE', // Belgium
            1 => 'BG', // Bulgaria
            2 => 'DK', // Denmark
            4 => 'DE', // Germany
            5 => 'EE', // Estonia
            6 => 'FI', // Finland
            7 => 'FR', // France
            8 => 'GR', // Greece
            9 => 'GB', // Great Britain
            10 => 'IE', // Ireland
            11 => 'IT', // Italy
            12 => 'LV', // Latvia
            13 => 'LT', // Lithuania
            14 => 'LU', // Luxembourg
            15 => 'MT', // Malta
            16 => 'NL', // Netherlands
            17 => 'AT', // Austria
            18 => 'PT', // Portugal
            19 => 'RO', // Romania
            20 => 'SE', // Sweden
            21 => 'SK', // Slovakia
            22 => 'SI', // Slovenia
            23 => 'ES', // Spain
            24 => 'CZ', // Czech Republic
            25 => 'HU', // Hungary
            26 => 'CY', // Cyprus
            27 => 'HR', // Croatia
        );

        $benelux = [
            'BE', // Belgium
            'LU', // Luxembourg
            'NL', // Netherlands
        ];

        $eu = array_search( $country['abbr'] , $euipo);
        $be = array_search( $country['abbr'] , $benelux);

        if ( $eu !== false ) {

            $country['eu'] = true;
        }

        if ( $be !== false ) {

            $country['be'] = true;
        }

        return $country;
    }

    // FETCH COUNTRIES MEMBER TO THE MADRID AGREEMENT
    public static function fetchCountryMadrid() {
        return Country::where( 'madrid_protocol', '=', '1' )->get();
    }
}
