<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Icomment extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'admin_user_id', 'trademark_id', 'description'
    ];

    public function trademark()
    {
    	return $this->belongsTo( Trademark::class );
    }
}
