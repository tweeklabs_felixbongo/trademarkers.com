<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrademarkClass extends Model
{
    public function classDescriptions()
    {
    	return $this->hasMany(ClassDescription::class);
    }

    public static function get_class_name( $class_id )
    {
    	$class = TrademarkClass::findOrFail($class_id);

    	return $class;
    }
}
