<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Continent extends Model
{
    public function countries () {

    	return $this->hasMany( Country::class )->orderBy('name');
    }

    public static function getContinents() {

    	return Continent::orderByRaw('name')->get();
    }

    public static function get_region( $region )
    {
    	$continent = Continent::where('abbr', 'like',  $region )->get();

    	return $continent;
    }
}
