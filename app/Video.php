<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cohensive\Embed\Facades\Embed;

class Video extends Model
{
    public static function parse_video( $video )
    {
    	$embed = Embed::make( $video->link )->parseUrl();

    	if (!$embed)
            return '';

        $embed->setAttribute(['width' => '100%', 'height' => 350]);

    	return $embed->getHtml();
    }

    public static function get_thumbnail( $video )
    {
    	$embed = Embed::make( $video->link )->parseUrl();

    	if (!$embed)
            return '';

        return $embed->getProvider()->info->imageRoot . "hqdefault.jpg";
    }

    public static function parse_video_side( $video )
    {
    	$embed = Embed::make( $video->link )->parseUrl();

    	if (!$embed)
            return '';

        $embed->setAttribute(['width' => '100%', 'height' => 120]);

    	return $embed->getHtml();
    }
}
