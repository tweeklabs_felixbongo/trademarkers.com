<?php

namespace App;

use App\TrademarkClass;
use App\Trademark;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\ActionCodeCart;



class ActionCode extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'case_number', 'action_code_type_id', 'action_code_campaign_id'
    ];

    public function case()
    {
        return $this->hasOne( ActionCodeCase::class );
    }

    public function aRoute()
    {
        return $this->hasOne( ActionRoute::class );
    }

    public function campaign()
    {
        return $this->hasOne( ActionCodeCampaign::class, 'id','action_code_campaign_id' );
    }

    public function promo()
    {
        return $this->hasOne( ActionCodePromo::class );
    }

    public static function get_case_number( $case_number )
    {
        $trademark = ActionCode::where( 'case_number', $case_number )->limit(1)->get();

        if ( count($trademark->toArray()) == 0 ) {

            return false;
        }

        return $trademark;
    }

    /**
     * CURRENT ACTION TYPES
     * 1 ?
     * 2 ?
     * 7 DEFAULT TO CASE
     * 8 SOMETHING LIKE CASE
     * 9 ?
     * 10 ?
     * 11 NEW CREATE CASE WITH SENDING EMAIL CAMPAIGN - ONGOING DEV
     * 12 ROUTING TO PAGES
     */

    public static function validate_action_code( $case_number )
    {
        $result = ActionCode::get_case_number( $case_number );

        if ( $result === false ) {

            return array();
        }

        $result = $result[0];

        if ( $result->action_code_type_id == 2 ) {

            return $result;

        } else if ( $result->action_code_type_id < 7 ) {

            return array();

        } else if ( $result->action_code_type_id == 9 ) {
            // dd($result->case_number);
            $case_number = $result->case_number;
            $trademark = Trademark::where( 'case_number', $case_number )->limit(1)->get();
            $actionCart = ActionCodeCart::with('cart')->where( 'action_code_id', $result->id )->first();
            
            // dd($trademark);
            $result['trademark'] = $trademark;
            $result['actionCart'] = $actionCart;
            // dd($actionCart);
            return $result;

        } else if ( $result->action_code_type_id == 10 ) {
            
            return $result;

        } else {

            $result->case->address = str_replace("\n", "<br>", $result->case->address);

            $case_number = ActionCode::get_class_numbers( $result->case );

            $result->case->class_description = ActionCode::get_class_descriptions( $result->case );

            $result->case->tm_filing_date = Carbon::parse($result->case->tm_filing_date);

            $result['filing_exp_date'] = Carbon::parse($result->case->tm_filing_date)->addMonths(6);

            $result['priority_classes'] = ActionCode::get_class_numbers( $result->case );

            $result->case->class_number = $case_number;
        }

        return $result;
    }

    public static function get_class_descriptions( $trademark )
    {
        $descriptions = $trademark->class_description;
        $classes      = $trademark->class_number;
        $data         = [];

        $d_step1 = str_replace("sssss", "<br>", $descriptions);

        return $d_step1;
    }

    public static function get_class_numbers( $trademark )
    {
        $classes  = $trademark->class_number;
        $step1    = explode(";", $trademark->class_number);
        $data     = []; 

        for ( $i = 0; $i < count( $step1 ); $i++ ) {

            $x = (int) $step1[$i];

            if ( $x != 0 ) {

                array_push( $data , $step1[$i]);
            }
            
        }

        return $data;
    }
    
    public static function get_trademark( $case_number )
    {
    	$trademark = ActionCode::where( 'case_number', $case_number )->limit(1)->get();

        $country  = Country::where('abbr','GB' )->limit(1)->get();

        $price = $country[0]->prices;

        $reg_price = $price[1];

    	if ( count( $trademark ) < 1 ) {

    		return $trademark;
    	}

    	$trademark = $trademark[0];

    	$trademark->euipo_id = $trademark->euipo_id;

    	$class_numbers = ActionCode::explode_class_number($trademark->class_number);

        $address = explode( '\\\n' , $trademark->address );

        $trademark['company'] = isset( $address[0] ) ? $address[0] : '';
        $trademark['street']  = isset( $address[1] ) ? $address[1] : '';
        $trademark['address'] = str_replace( "\\n", " ",  stripslashes( $trademark->address ) );

    	$trademark['numbers'] = $class_numbers;
    	$description = [];

        $trademark['price'] = ( count( $trademark['numbers'] ) - 1 ) *  $reg_price->additional_cost + $reg_price->initial_cost;

    	foreach ( $trademark['numbers'] as $number ) {

    		$class = TrademarkClass::get_class_name( (int) $number );

    		$description[$class->id] = $class->name;
    	}

    	ksort($description);

    	$trademark['classes'] = $description;

    	return $trademark;
    }

    public static function explode_class_number( $class_number )
    {
    	$first_step  = explode('[', $class_number);
    	$second_step = explode(']', $first_step[1]);
    	$third_step  = explode(',', $second_step[0]);

    	return $third_step;
    }
}
