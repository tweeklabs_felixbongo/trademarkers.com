<?php
namespace App\Services;

use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Mail;

class WebhookService 
{

    public $rpoHook;

    public function __construct(
        \App\Webhook $rpoHook
    ) {
        $this->rpoHook = $rpoHook;

        foreach ( $this->rpoHook->get() as $hook ) {
            $arrayHook = json_decode($hook->response);
            
            $emailEvents = json_decode($arrayHook->mandrill_events);

            // TO DO: cron to update email tracking records
        }
    }

    



    
}