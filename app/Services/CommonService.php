<?php
namespace App\Services;

use Illuminate\Support\Facades\Route;
use Torann\LaravelMetaTags\Facades\MetaTag;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Mail;

use App\Repositories\Utils;

class CommonService 
{

    public $rpoCase;
    public $rpoCampaign;
    public $rpoActionCode;
    public $rpoActionRoute;
    public $rpoEvent;
    public $rpoBrandDomain;

    public function __construct(
        \App\ActionCodeCase $rpoCase,
        \App\ActionCodeCampaign $rpoCampaign,
        \App\ActionRoute $rpoActionRoute,
        \App\Event $rpoEvent,
        \App\BrandDomain $rpoBrandDomain,
        \App\ActionCode $rpoActionCode
    ) {
        $this->rpoCase = $rpoCase;
        $this->rpoCampaign = $rpoCampaign;
        $this->rpoActionCode = $rpoActionCode;
        $this->rpoActionRoute = $rpoActionRoute;
        $this->rpoEvent = $rpoEvent;
        $this->rpoBrandDomain = $rpoBrandDomain;

        // test
        // $this->removeCasePerCampaign();
 
    }

    public function getCase($id)
    {
        return $this->rpoCase->find($id);
    }

    /**
     *  PARAMETERS SHOULG BE COMPLETE URL INCLUDING PARAMETERS
     *  
     */
    public function createActionRoute($url)
    {
        if (!$url)
            return;
           
        // Generate action code for routing
        $last_action_code = $this->rpoActionCode->max('id');

        $last_action_code++;
        $actionC = Utils::case_gen( $last_action_code );

        $code = $this->rpoActionCode->create( [
            'case_number'             => $actionC,
            'action_code_type_id'     => 12,
            'action_code_campaign_id' => 0,
        ] );

        $this->rpoActionRoute->create([
            'url' => $url,
            'action_code_id' => $code->id,
            'notes' => 'System Generated Email Layout'
        ]);

        return $code->case_number;
    }

    public function getEvent($id)
    {
        return $this->rpoEvent->find($id);
    }

    public function getDomain($id)
    {
        return $this->rpoBrandDomain->find($id);
    }

    public function removeCasePerCampaign()
    {
        $campaigns = ['35','36','37','38'];

        foreach ($campaigns as $campaign){
            $repoCampaign = $this->rpoCampaign->find($campaign);

            if ($repoCampaign) {
                $cases = $this->rpoCase->where('action_code_campaign_id', $repoCampaign->id)->get();

            // dd($cases);
                foreach ( $cases as $case ) {
                    // remove case
                    $action = $this->rpoActionCode->find($case->action_code_id);
                    if ($action) {
                        $action->delete();

                        $case->delete();
                    }
                    
                }

                $repoCampaign->delete();
            }
            
        }

    }


    
}