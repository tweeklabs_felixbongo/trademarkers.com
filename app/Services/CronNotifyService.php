<?php
namespace App\Services;

use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Mail;
use App\ActionCodeCase;
use App\Repositories\Utils;

// use Carbon\Carbon;



class CronNotifyService 
{

    public $rpoTrademark;
    public $rpoCart;
    public $rpoUser;
    public $rpoCronLast;
    public $rpoAction;
    public $rpoPromo;
    public $rpoBrand;
    public $rpoDomain;
    public $rpoEvent;
    public $rpoEmailTracker;
    public $rpoDomainEmail;
    public $loginUserService;
    public $commonService;
    public $eventService;

    public $mailTimer = 0;

    public function __construct(
        \App\Trademark $rpoTrademark,
        \App\Cart $rpoCart,
        \App\User $rpoUser,
        \App\CronLastCrawl $rpoCronLast,
        \App\ActionCodePromo $rpoPromo,
        \App\Brand $rpoBrand,
        \App\BrandLead $rpoBrandLead,
        \App\ActionCode $rpoAction,
        \App\BrandDomain $rpoDomain,
        \App\Event $rpoEvent,
        \App\EmailTracker $rpoEmailTracker,
        \App\DomainEventEmail $rpoDomainEmail,
        \App\Services\LoginUserService $loginUserService,
        \App\Services\CommonService $commonService,
        \App\Services\CronEventService $eventService
        
    ) {
        $this->rpoTrademark = $rpoTrademark;
        $this->rpoCart = $rpoCart;
        $this->rpoUser = $rpoUser;
        $this->rpoAction = $rpoAction;
        $this->rpoPromo = $rpoPromo;
        $this->rpoBrand = $rpoBrand;
        $this->rpoBrandLead = $rpoBrandLead;
        $this->rpoEmailTracker = $rpoEmailTracker;
        $this->rpoDomain = $rpoDomain;
        $this->rpoEvent = $rpoEvent;
        $this->rpoDomainEmail = $rpoDomainEmail;
        $this->loginUserService = $loginUserService;
        $this->commonService = $commonService;
        $this->eventService = $eventService;
        $this->rpoCronLast = $rpoCronLast;

        // $this->sendEmailLayout();
    }

    public function notifyOfficeStatus()
    {
        $internalContent = $this->getTableHeadersOfficeStatus();
        $cond_interval = [];
        // $trademarks = [];
        $intervals = [14];
        $status = [
            'Under Examinations',
            'Published',
            'Under Opposition',
            'Under Cancellation',
        ];

        foreach($intervals as $interval) {
            $cond_interval[] = "MOD(DATEDIFF(NOW(), filing_date), {$interval}) = 0";
        }

        $strStatus = "'" . implode ( "', '", $status ) . "'";
        $cond_interval[] = "office_status IN (".$strStatus.")";

        $cond_interval = join(" AND ", $cond_interval);

        $trademarks = $this->rpoTrademark->whereRaw($cond_interval)->get();
        // dd($trademarks);
        foreach ($trademarks as $trademark) {

            $internalContent .= $this->getTableContentOfficeStatus($trademark);
        }

        $internalContent .= $this->getTableFooterOfficeStatus();

        // send internal email
        if ( $trademarks ) {
            $internalSubject = "Email Automations (Office Status)";
            Mail::send('mail.notifications-officestatus-internal', ['internalSubject' => $internalSubject, 'internalContent' => $internalContent ], function ($m) use ($internalSubject) {
                $m->from('notifications@trademarkers.com', 'TradeMarkers LLC');

                $m->to(['info@trademarkers.com'], 'Trademark LLC')->subject($internalSubject);
            });
        }
        
        
    }

    public function notifyAbondonedCart4Hours()
    {
        // echo "The time is " . date("d-m-Y h:i:sa");
        // dd(0);
        $status = [
            'active',
            'inactive'
        ];

        $mytime = Carbon::now();

        // dd($mytime);
        $subTime = $mytime->subHour(3);
        $subTimeStr = $subTime->toDateTimeString();
 
        $maxTime = $mytime->subHours(1);
        $maxTimeStr = $maxTime->toDateTimeString();

        // echo $maxTimeStr . "\n" . $subTimeStr . "\n";
        // dd($mytime);
        
        $strStatus = "'" . implode ( "', '", $status ) . "'";
        $cond_interval[] = "status IN (".$strStatus.")";

        $cond_interval[] = "updated_at BETWEEN '{$maxTimeStr}' AND '{$subTimeStr}'";

        $cond_interval = join(" AND ", $cond_interval);
        // dd($cond_interval);
        $carts = $this->rpoCart->whereRaw($cond_interval)
        ->get()
        ->pluck('user_id');
        // dd($carts);
        // SEND EMAILS TO CUSTOMERS
        $users = $this->rpoUser->find($carts);
        // dd($carts);
        foreach ($users as $usr) {

            $user = $this->loginUserService->generateUserLinkToken($usr);

            $subject = "Looks like you forgot something, complete your order now!";

            // send mail
            Mail::send('mail.notifications-abandoned-cart4hour', ['user' => $user], function ($m) use ($subject, $user) {
                $m->from('notifications@trademarkers.com', 'TradeMarkers LLC');

                $m->to($user->email, $user->name)->subject($subject);
            });

        }

        return $this->rpoUser->find($carts);
    }

    public function notifyAbondonedCart1Day()
    {
        $status = [
            'active',
            'inactive'
        ];
        
        $strStatus = "'" . implode ( "', '", $status ) . "'";
        $cond_interval[] = "status IN (".$strStatus.")";
        $cond_interval[] = "DATE(updated_at) = ( CURDATE() - INTERVAL 1 DAY )";

        $cond_interval = join(" AND ", $cond_interval);

        $carts = $this->rpoCart->whereRaw($cond_interval)
        ->get()
        ->pluck('user_id');
        
        // SEND EMAILS TO CUSTOMERS
        $users = $this->rpoUser->find($carts);
        foreach ($users as $usr) {

            $user = $this->loginUserService->generateUserLinkToken($usr);

            $subject = "Heading out without checking out?";

            // send mail
            Mail::send('mail.notifications-abandoned-cart1day', ['user' => $user ], function ($m) use ($subject, $user) {
                $m->from('notifications@trademarkers.com', 'TradeMarkers LLC');

                $m->to($user->email, $user->name)->subject($subject);
            });

        }

        return $this->rpoUser->find($carts);
    }

    public function notifyAbondonedCart4Days()
    {
        $status = [
            'active',
            'inactive'
        ];
        
        $strStatus = "'" . implode ( "', '", $status ) . "'";
        $cond_interval[] = "status IN (".$strStatus.")";

        $cond_interval[] = "DATE(updated_at) = ( CURDATE() - INTERVAL 3 DAY )";

        $cond_interval = join(" AND ", $cond_interval);
        // dd($cond_interval);
        $carts = $this->rpoCart->whereRaw($cond_interval)
        ->get()
        ->pluck('user_id');
        
        // SEND EMAILS TO CUSTOMERS
        $users = $this->rpoUser->find($carts);
        foreach ($users as $usr) {
            
            $user = $this->loginUserService->generateUserLinkToken($usr);

            // CREATE ACTION FOR PROMO
            $last_action_code = $this->rpoAction->max('id');
            $action_code = Utils::case_gen( $last_action_code );

            $code = $this->rpoAction->create( [
                'case_number'             => $action_code,
                'action_code_type_id'     => 2,
                'action_code_campaign_id' => 0,
            ] );
            
            // CREATE PROMO CODE FOR USER
            $promo = $this->rpoPromo->create([
                'action_code_id'             => $code->id,
                'user_id'             => $user->id,
                'discount'             => '5.0',
                'expiry_date'             => $code->updated_at->addDays(2),
                'limit'             => '1',
                'status'             => 'on-going'
            ]);

            // dd($promo);

            $subject = "Empty your cart with 10% Off - Trademarkers LLC";

            // send mail
            Mail::send('mail.notifications-abandoned-cart4day', ['promo' => $promo, 'user' => $user ], function ($m) use ($subject, $user) {
                $m->from('notifications@trademarkers.com', 'TradeMarkers LLC');

                $m->to($user->email, $user->name)->subject($subject);
            });

        }

        return $this->rpoUser->find($carts);
    }

    public function notifyAbondonedCart1Month()
    {
        // echo "The time is " . date("d-m-Y h:i:sa");
        // dd(0);
        $status = [
            'active',
            'inactive'
        ];

        $mytime = Carbon::now();

        $subTime = $mytime->subMonths(2);
        $subTimeStr = $subTime->toDateTimeString();
 
        $maxTime = $mytime->subMonth();
        $maxTimeStr = $maxTime->toDateTimeString();
        
        $strStatus = "'" . implode ( "', '", $status ) . "'";
        $cond_interval[] = "status IN (".$strStatus.")";

        $cond_interval[] = "updated_at BETWEEN '{$maxTimeStr}' AND '{$subTimeStr}'";

        $cond_interval = join(" AND ", $cond_interval);
        $carts = $this->rpoCart->whereRaw($cond_interval)
        ->get()
        ->pluck('user_id');
   
        // SEND EMAILS TO CUSTOMERS
        $users = $this->rpoUser->find($carts);

        foreach ($users as $user) {

            $subject = "You left something behind";

            if ( $user->profiles ) {
                // send mail
                Mail::send('mail.notifications-abandoned-cart1month', ['user' => $user ], function ($m) use ($subject, $user) {
                    $m->from('notifications@trademarkers.com', 'TradeMarkers LLC');

                    $m->to($user->email, $user->name)->subject($subject);
                });
            }
            

        }

        return $this->rpoUser->find($carts);
    }

    // HTML 
    public function getTableHeadersOfficeStatus()
    {
        return "<table class='table table-responsive'>
                <tr>
                    <th>ID</th>
                    <th>Trademark</th>
                    <th>Service</th>
                    <th>Country</th>
                    <th>Filling Date</th>
                    <th>Office Status</th>
                    <th>Owner Name</th>
                    <th>Owner Email</th>
                    <th>Link</th>
                </tr>
        ";
    }

    public function getTableContentOfficeStatus($trademark)
    {
        return "<tr>
                    <td>{$trademark->id}</td>
                    <td>{$trademark->name}</td>
                    <td>{$trademark->service}</td>
                    <td>{$trademark->country->abbr}</td>
                    <td>{$trademark->filing_date}</td>
                    <td>{$trademark->office_status}</td>
                    <td>{$trademark->user->name}</td>
                    <td>{$trademark->user->email}</td>
                    <td><a href='". url('/admin/manage/trademarks/'.$trademark->id) ."' target='_blank'>Visit</a></td>
                </tr>
        ";
    }

    public function getTableFooterOfficeStatus()
    {
        return "</table>";
    }

    public function generateUserLinkToken($user)
    {
        $delta = 86400 * 7;

        if ($user->tstamp){
            if ( $_SERVER["REQUEST_TIME"] - $user->tstime > $delta ) {
                $token = sha1(uniqid($user->email, true));
                $user->token = $token;
                $user->tstamp = $_SERVER["REQUEST_TIME"];
                $user->save();
            }
            
        } else {
            // no link generate one
            $token = sha1(uniqid($user->email, true));
            $user->token = $token;
            $user->tstamp = $_SERVER["REQUEST_TIME"];
            $user->save();
        }

        return $user;
        
    }

    // SEND EMAIL MAIL LAYOUT
    public function testEmail()
    {
        $subject = "test email";

        $userEmail = "febongo111222@gmail.com";
        $userName = "Felix Bongo";

        $res = Mail::send('mail.test-email', ['userName' => $userName], function ($m) use ($subject, $userName, $userEmail) {
            $m->from('notifications@trademarkers.com', 'TradeMarkers LLC');
            // $m->track_opens(true);
            $m->to($userEmail, $userName)->subject($subject);
        });
        
        // dd($res);
    
    }

    // EMAIL LAYOUT FUNCTION SERVICE
    public function sendEmailLayout()
    {
        return false;
        // get last crawled 
        // this is to minimized execution time
        $crawl = $this->rpoCronLast->where('name','event-sender')->orderby('id','desc')->first();
        
        $perPage = 150;
        $lastPageCrawl = $crawl ? $crawl->last_page + 1 : 1;

        

        $events = $this->rpoEvent->sendAvailablePublished()->paginate( $perPage , ['*'], 'page', $lastPageCrawl);
        // $events = $this->rpoEvent->sendAvailablePublished()->get();
        
        // dd($events);
        // EVENTS EXCEPT OPPOSISTION AND PROTEST EVENT
        if ( $events ) {
            $email_sent = false;
            foreach ( $events as $event ) {

            //    dd($event);
                
                if ($event->type && $event->type->slug) 
                switch($event->type->slug) {
                    // proper slug letter-of-protest-for-domain-owners
                    // temporary disabled
                    case 'letter-of-protest-for-domain-owners-disabled' :
                        $domains = $event->brand->domains;
                        
                        if ( $domains && $event->case ) 
                        foreach ( $domains as $domain ) {

                            // GENERATE EMAILS HERE
                            $this->eventService->generateRandomEmail($domain);
                            
                            if ( $email = $this->fetchEmailNoTracker($domain->emails, $event) ) {

                                $this->sendEventEmail($domain, $event, $domain->brand->brand, $email);
                                $email_sent = true;
                                $domain->last_crawl = Carbon::now();
                                $domain->save();
                            break;
                            } 
    
                        }
                    break;

                    case 'trademark-registration-invitation-claiming-priority---euipo-filings':
                    case 'trademark-registration-invitation-claiming-priority---us-filings' :
                    case 'letter-of-protest-for-prior-registered-tm-holders' :
                    case 'trademark-opposition---uspto' :
                    case 'trademark-opposition---euipo' :
                    case 'office-action-response' :
                        
                        $caseOwned = ActionCodeCase::find($event->case_owned_id);
        
                        if ( $caseOwned ) {
                                $emails = $caseOwned->emails;

                                if ( $email = $this->fetchEmailNoTracker($emails, $event) ) {
        
                                    $this->sendEventEmail(null, $event, $caseOwned->tm_holder, $email);
                                    $email_sent = true;
                                }
                        }
                    break;


                    default :

                    break;
                }


            } // end foreach event

            // update last crawl
            $lastPageCrawl = $events->lastPage() > $lastPageCrawl ? $lastPageCrawl : 0;

            $this->rpoCronLast->create([
                'name' => 'event-sender',
                'last_page' => $lastPageCrawl
            ]);
            
            
        } // end if event

        

            
    }

    public function sendEventEmail($domain, $event, $userName, $email)
    {
        

        try {

        // $email = $this->fetchEmailNoTracker($domain->emails);
        if ( $event->type->slug == "letter-of-protest-for-domain-owners" ) {
            $subject = $event->type->email_subject ? $domain->brand->brand .": " . $event->type->email_subject : $event->type->type;
        } elseif ( $event->type->slug == "letter-of-protest-for-prior-registered-tm-holders" ) {
            $subject = $event->type->email_subject ? $event->caseOwned->trademark .": " . $event->type->email_subject : $event->type->type;
        } elseif ( $event->type->slug == "trademark-opposition---uspto" || $event->type->slug == "trademark-opposition---euipo" ) {
            $subject = $event->type->email_subject ? $event->type->email_subject . ' ' . $event->caseOwned->trademark : $event->type->type;
        } elseif ( $event->type->slug == "trademark-registration-invitation-claiming-priority---us-filings" || $event->type->slug == "trademark-registration-invitation-claiming-priority---euipo-filings" ) {
            $subject = $event->type->email_subject ? $event->type->email_subject . ' ' . strtoupper($event->caseOwned->trademark) : $event->type->type;
        } elseif ( $event->type->slug == "office-action-response" ) {
            $subject = $event->type->email_subject ? $event->type->email_subject . "{$event->caseOwned->trademark}" : $event->type->type;
        } else {
            $subject = $event->type->email_subject ? $event->type->email_subject : $event->type->type;
        }
        $userEmail = strtolower(trim($email->email));
        // $userEmail = "felix@trademarkers.com";
        // dd($email->email);

        // generate action code with event
        $lopdUrl = "quote?getQuoteType=lopd&event=". $event->id."&mail=".$userEmail."&domain=".($domain ? $domain->id : '');
        $loptUrl = "quote?getQuoteType=lopt&event=". $event->id."&mail=".$userEmail."&domain=".($domain ? $domain->id : '');
        $dsaUrl = "quote?getQuoteType=dsa&event=". $event->id."&mail=".$userEmail."&domain=".($domain ? $domain->id : '');
        $oarUrl = "quote?getQuoteType=oar&event=". $event->id."&mail=".$userEmail."&domain=".($domain ? $domain->id : '');
        $itaUrl = "quote?getQuoteType=ita&event=". $event->id."&mail=".$userEmail."&domain=".($domain ? $domain->id : '');
        $trUrl = "quote?getQuoteType=tr&event=". $event->id."&mail=".$userEmail."&domain=".($domain ? $domain->id : '');
        $afrUrl = "quote?getQuoteType=afr&event=". $event->id."&mail=".$userEmail."&domain=".($domain ? $domain->id : '');
        $nasUrl = "quote?getQuoteType=nas&event=". $event->id."&mail=".$userEmail."&domain=".($domain ? $domain->id : '');
        $tmoUrl = "quote?getQuoteType=tmo&event=". $event->id."&mail=".$userEmail."&domain=".($domain ? $domain->id : '');
        $cadUrl = "quote?getQuoteType=cad&event=". $event->id."&mail=".$userEmail."&domain=".($domain ? $domain->id : '');
        
        $dataUrl = [
            'lopd'  => $this->commonService->createActionRoute($lopdUrl),
            'lopt'  => $this->commonService->createActionRoute($loptUrl),
            'dsa'   => $this->commonService->createActionRoute($dsaUrl),
            'oar'   => $this->commonService->createActionRoute($oarUrl),
            'ita'   => $this->commonService->createActionRoute($itaUrl),
            'tr'    => $this->commonService->createActionRoute($trUrl),
            'afr'   => $this->commonService->createActionRoute($afrUrl),
            'nas'   => $this->commonService->createActionRoute($nasUrl),
            'tmo'   => $this->commonService->createActionRoute($tmoUrl),
            'cad'   => $this->commonService->createActionRoute($cadUrl),
        ];

        $from = $this->getFromEmail($email->email);
        
        if ( filter_var( $userEmail, FILTER_VALIDATE_EMAIL ) )
        if ( $this->rpoEmailTracker->get()->count() % 100 == 99 ) {
         
            // every 100 email bcc mg
            $res = Mail::send('mail.email-cron.email-'.$event->type->slug, 
                [   
                    'userName' => $userName, 
                    'domain' => $domain, 
                    'event' => $event, 
                    'userEmail' => $userEmail,
                    'dataUrl' => $dataUrl,
                    'email' => $email,
                ], 
                function ($m) use 
                (
                    $subject, 
                    $userName, 
                    $userEmail,
                    $from
                ) {
                    $m->from($from, 'TradeMarkers LLC');
                    $m->to($userEmail, $userName)
                    ->bcc(["ivy@trademarkers.com","mgleiss@bigfoot.com"])
                    ->subject($subject);
                });

        } else {
        
            $res = Mail::send('mail.email-cron.email-'.$event->type->slug, 
                [   
                    'userName' => $userName, 
                    'domain' => $domain, 
                    'event' => $event, 
                    'userEmail' => $userEmail,
                    'dataUrl' => $dataUrl,
                    'email' => $email,
                ], 
                function ($m) use 
                (
                    $subject, 
                    $userName, 
                    $userEmail,
                    $from
                ) {
                    $m->from($from, 'TradeMarkers LLC');
                    $m->to($userEmail, $userName)
                    ->bcc(["ivy@trademarkers.com","mgleiss@bigfoot.com"])
                    ->subject($subject);
                });
        }



        $this->rpoEmailTracker->create([
            'event_id' => $event->id,
            'domain_id' => ($domain ? $domain->id : null),
            'email_id' => $email->id,
            'status' => 'sent',
        ]);

        $event->last_crawl = Carbon::now();
        $event->save();

        
        } catch (Exception $e) {
            
        }

    }

    public function fetchEmailNoTracker($emails, $event)
    {
        foreach ($emails as $email){

            if ( !$this->rpoEmailTracker->where([
                    ['event_id',$event->id],
                    ['email_id',$email->id],
                ])->first() ) {
                return $email;
            }

            // return null;
        }

        return null;
    }

    public function getFromEmail($email)
    {
        $tld = substr($email, strrpos($email, ".")+1);
        $tldUsed = "";

        switch (strtolower($tld)) {
            case 'de': 
                $tldUsed = "notification@trademarkers.de";
            break;
            default : 
                $tldUsed = "notification@trademarkers.net";
            break;
        }

        return $tldUsed;
    }

}