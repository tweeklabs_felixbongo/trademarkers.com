<?php
namespace App\Services;

use Illuminate\Support\Facades\Route;
use Torann\LaravelMetaTags\Facades\MetaTag;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Mail;

class CampaignService 
{

    public $rpoCampaign;

    public function __construct(
        \App\ActionCodeCampaign $rpoCampaign
    ) {
        $this->rpoCampaign = $rpoCampaign;
        
    }

    public function getCampaign($id)
    {
        return $this->rpoCampaign->find($id);
    }


    
}