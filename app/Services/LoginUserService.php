<?php
namespace App\Services;

use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Mail;
use App\Repositories\Utils;

// use Carbon\Carbon;



class LoginUserService 
{

    public $rpoUser;

    public function __construct(
        \App\User $rpoUser
        
    ) {
        $this->rpoUser = $rpoUser;

    }

    public function generateUserLinkToken($user)
    {
        // 1month expiry
        $delta = $this->getDelta();
// echo $user->token.'\n\n';
        if (!$user->tstamp){
            // no link generate one
            $token = sha1(uniqid($user->email, true));
            $user->token = $token;
            $user->tstamp = $_SERVER["REQUEST_TIME"];
            $user->save();
        }

        return $user;
        
    }

    public function getDelta()
    {
        // 1 month expiry
        return 86400 * 30;;
    }

    public function createHashEmail($email)
    {
        return Hash::make($email);
    }

    // used for autologin with creds
    public function getUserExist($pass, $email)
    {

        $decodePass = urldecode($pass);
        $decodePass = str_replace('-','/',$decodePass);
        return $this->rpoUser->where([
            ['email',$email],
            ['password',$decodePass]
        ])->first();
        
    }
}