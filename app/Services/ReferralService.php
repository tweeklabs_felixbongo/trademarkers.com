<?php
namespace App\Services;

use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Mail;

class ReferralService 
{

    // public $rpoCoupon;
    // public $rpoCouponUser;
    // public $rpoCart;
    // public $actionCode;
    // public $cart;
    // public $cartCoupons;
    public $actionCodeReferral;
    public $actionCodeReferralInvite;

    public function __construct(
        // \App\ActionCodePromo $coupon,
        // \App\Cart $cart,
        // \App\CartCoupon $cartCoupons,
        // \App\CouponUser $rpoCouponUser,
        // \App\ActionCode $actionCode,
        \App\ActionCodeReferral $actionCodeReferral,
        \App\ActionCodeReferralInvite $actionCodeReferralInvite
    ) {
        // $this->rpoCoupon = $coupon;
        // $this->rpoCouponUser = $rpoCouponUser;
        // $this->actionCode = $actionCode;
        // $this->cart = $cart;
        // $this->cartCoupons = $cartCoupons;
        $this->actionCodeReferral = $actionCodeReferral;
        $this->actionCodeReferralInvite = $actionCodeReferralInvite;
    }

    public function getReferralInvite()
    {
        $user =  Auth::user();

        $referralInvite = $this->actionCodeReferralInvite->where('invited_user_id', $user->id)->first();

        return $referralInvite;
    }

    public function getReferral()
    {
        $user =  Auth::user();

        $referralInvite = $this->actionCodeReferralInvite->where('invited_user_id', $user->id)->first();

        if ($referralInvite) {
            $referral = $this->actionCodeReferral->with('invites', 'transactions', 'action')->find($referralInvite->referrals_id);
        }

        return $referral;
    }

    public function getReferrer()
    {
        $user =  Auth::user();

        // $referralInvite = $this->actionCodeReferralInvite->where('invited_user_id', $user->id)->first();

        // if ($referralInvite) {
        //     $referral = $this->actionCodeReferral->with('invites', 'transactions', 'action')->find($referralInvite->referrals_id);
        // }

        return $this->actionCodeReferral->with('invites', 'invites.user', 'invites.transactions', 'action')->where('user_id',$user->id)->first();
    }

    public function sendMailNotificationRequest($request, $user)
    {
        $subject = "Affiliate Payment Request";
        $res = Mail::send('mail.payment-request', ['user' => $user, 'request' => $request], function ($m) use ($user, $subject) {
            $m->from('info@trademarkers.com', 'TradeMarkers LLC');
            $m->to($user->email, $user->name)->subject($subject);
        });
    }



    
}