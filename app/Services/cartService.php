<?php
namespace App\Services;

use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Mail;

class CartService 
{

    public $rpoCoupon;
    public $rpoCouponUser;
    public $rpoCase;
    public $rpoCart;
    public $actionCode;
    public $cart;
    public $cartCoupons;
    public $countryDiscount;
    public $actionCodeReferralInvite;

    public function __construct(
        \App\ActionCodePromo $coupon,
        \App\ActionCodeCase $rpoCase,
        \App\Cart $cart,
        \App\CartCoupon $cartCoupons,
        \App\CouponUser $rpoCouponUser,
        \App\ActionCode $actionCode,
        \App\CountryDiscount $countryDiscount,
        \App\ActionCodeReferralInvite $actionCodeReferralInvite
    ) {
        $this->rpoCase = $rpoCase;
        $this->rpoCoupon = $coupon;
        $this->rpoCouponUser = $rpoCouponUser;
        $this->actionCode = $actionCode;
        $this->countryDiscount = $countryDiscount;
        $this->cart = $cart;
        $this->cartCoupons = $cartCoupons;
        $this->actionCodeReferralInvite = $actionCodeReferralInvite;
    }

    // CHECK CURRENT CART IF THERE IS COUPON
    public function hasCoupon()
    {
        foreach ( $this->cart->get_user_cart_items() as $item) {
            if ( count($item->coupons) > 0 ) {
                return true;
            }
        }

        return false;
    }

    // GET COUPON AMOUNT
    public function getCoupon($code)
    {
        // GET COUPON AND RETURN ARRAY
        // $trademark = ActionCode::get_case_number( $code )->with('promo');
        $actionCodeArr = $this->actionCode->get_case_number( $code );
        // dd($actionCodeArr[0]->case_number);
        if ($actionCodeArr)
        return $this->rpoCoupon::where('action_code_id',$actionCodeArr[0]->id)->first();
    }

    public function getCouponUser($uid, $cid)
    {
        // GET COUPON AND RETURN ARRAY
        return $this->cartCoupons::selectRaw('count(*) AS cnt')->where([
            ['user_id','=',$uid],
            ['promo_code_id','=',$cid],
            ['order_id','<>',null],
        ])
        ->groupBy('order_id')
        ->get();
        // $count = 0;
        // foreach ($couponCnt as $cnt) {
        //     $count += 
        // }
    }

    // COMPUTE TOTAL AMOUNT WITH COUPON
    public function computedAmount($items)
    {

        $total = 0;
        $itemCoupon = '';
        $discount = 0;
        // get total
        foreach($items as $item){

            
            if ( $item->status == 'active' ){
                $total += $item->amount - $item->total_discount;
                $itemCoupon = $item->discount;
            }
        }

        $coupon = $this->getCoupon($itemCoupon);

        if (!$coupon) {
            return null;
        }
        // echo $total;
        // dd($coupon->discount);

        // create data array and return 
        $percent = $this->getPercentDiscount($total, $coupon);

        // dd($percent);
        $percent /= 100;

        $discount = $total - ($total * $percent);

        $data = [
            'totalDiscount' => $total - $discount,
            'totalAmount' => $total,
            'promoCode' => $itemCoupon,
            'discountId' => $coupon->id
        ];
        return $data;


    }

    public function getPercentDiscount($total, $coupon)
    {
        // $coupon = $this->getCoupon($itemCoupon);

        $discount = 0;
        if ( $total >= $coupon->supplementary_amount_point) {
            $discount = $coupon->discount + $coupon->supplementary_discount;
        } else {
            $discount = $coupon->discount;
        }

        return $discount;

    }

    public function validateCoupon($coupon)
    {
        if (!$coupon) return false; 
        
        if ( $coupon->status == 'done' || $coupon->status == 'expired') {
            return false;
        }

        if ( $coupon->expiry_date ) {
            // echo 'date validate';
            $current = Carbon::now();
            // echo $current;

            if ($coupon->expiry_date < $current) {
                return false;
            }
        }


        // LIMIT ON USAGE TO BE CONTINUE
        if ( $coupon->limit && $coupon->limit > 0) {
            $res = $this->getCouponUser(auth()->user()->id, $coupon->id);
            // dd($res[0]->cnt);
            if ( count($res) >= $coupon->limit) {
                return false;
            }
        }

        // check user
        // dd( $coupon->user_id);
        if ( $coupon->user_id && $coupon->user_id != auth()->user()->id) {
            return false;
        }
        // check country
        // if ( $coupon->country_id && $coupon->country_id != auth()->user()->id) {
        //     return false;
        // }

        // dd($coupon);

        return true;
    }

    public function getCouponErrorMessage($coupon)
    {
        if (!$coupon) return 'Coupon does not exist'; 
        
        if ( $coupon->status == 'done' || $coupon->status == 'expired') {
            return 'Coupon is no longer active';
        }

        if ( $coupon->expiry_date ) {
            // echo 'date validate';
            $current = Carbon::now();
            // echo $current;

            if ($coupon->expiry_date < $current) {
                return 'Coupon expired';
            }
        }


        // LIMIT ON USAGE TO BE CONTINUE
        if ( $coupon->limit && $coupon->limit > 0) {
            $res = $this->getCouponUser(auth()->user()->id, $coupon->id);
            // dd($res[0]->cnt);
            if ( count($res) >= $coupon->limit) {
                return 'Coupon usage limit';
            }
        }

        // check user
        // dd( $coupon->user_id);
        if ( $coupon->user_id && $coupon->user_id != auth()->user()->id) {
            return 'Coupon is not meant for this user';
        }
        // check country
        // if ( $coupon->country_id && $coupon->country_id != auth()->user()->id) {
        //     return false;
        // }

        // dd($coupon);

        return '';
    }

    public function validatePromoCountry($items, $coupon)
    { 
        foreach($items as $item){
            if ($item->country_id != $coupon->country_id) {
                return false;
            }
        }
        return true;
    }

    public function validatePromoTrade($item, $coupon)
    {
        // if () {

        // }
    }

    public function getCart()
    {
        $contents = $this->cart->get_user_cart_items();
        // dd($contents->country);
        foreach ($contents as $content) {
            // dd($content->country);
        }
        return $contents->country;
    }

    /**
     * TO HTML CART TABLE
     */
    public function cartToHtml($message = null)
    {
        $toHtml = '';
        $contents = $this->cart->get_user_cart_items();
        // $dataCoupon = $this->computedAmount($contents);
        $dataCoupon = null;
        $total = 0;
        
        $user = auth()->user();
        $referral = $this->actionCodeReferralInvite
                    ->where('invited_user_id', $user->id)
                    ->whereNull('order_id')
                    ->first();

        if ( $contents->count() > 0) {
            // dd($referral);

            if ( session('routeCode') ) {
                $actionPromo = $this->rpoCoupon->find(session('routeCode')->related_action_id);
                $toHtml .= '<p>We have prepared a discount coupon just for you! <span id="apply-discount" data-coupon-code="'.$actionPromo->action_code->case_number.'" class="btn btn-primary">Apply Coupon</span></p>';
            }

            if ( $referral && $referral->apply == 'no' ) {
                $toHtml .= '<p>Apply 10% Signup Discount : <span id="apply-signup-discount" class="btn btn-primary">Apply</span></p>';
            }
    
            if ( $referral && $referral->apply == 'yes' ) {
                $toHtml .= '<p>Signup Discount Applied: <span id="cancel-signup-discount" class="btn btn-danger">Cancel</span></p>';
            }

            if ( $user->youtube_subscribe == 'no' && $user->role->id != 3) {
                $toHtml .= '<p>Subscribe to our youtube channel and get a discount up to 10% <a class="btn btn-primary" href="/user/youtube-subscribe">Youtube channel</a></p>';
            }

            if ( $user->youtube_subscribe == 'yes' && $user->youtube_promo_claim == 'no' && $user->role->id != 3) {
                $toHtml .= '<p>Apply Youtube Subscribe Discount : <span id="apply-youtube-discount" class="btn btn-primary">Apply</span></p>';
            }
    

            $toHtml .= '<table class="table table-hover table-bordered">

                        <thead>
                            <tr>
                                <td style="width:34px;"></td>
                                <td style="color:red;"> Item #</td>
                                <td> Country</td>
                                <td> Service</td>
                                <td> Type</td>
                                <td> Classes</td>
                                <td> Amount</td>
                                <td> <i class="fa fa-cog"></i></td>
                            </tr>
                        </thead>';
            if ($message) {
                $toHtml .= '<tr id="ajax-message"><td colspan="8"><div style="text-align: center;" class="alert alert-'.$message['type'].'" role="alert">
                                '.$message['message'].'
                                </div></td></tr>';
            }

            $count = 1;
            $hasCoupon = false;
            $totalDiscount = 0;
            foreach ($contents as $content) {
                
                $cartItem = $this->cart->with(['coupons','coupons.code', 'coupons.code.action_code'])->find($content['id']);

                $toHtml .= '<tr>
                                <td><input class="updateCart" type="checkbox" value="'.$content['id'].'" name="itemSelected" '.($content['status'] == 'active' ? "checked" : "").' /></td>
                                <td> <b>'.str_pad( $count++ , 3, '0', STR_PAD_LEFT ).'</b></td>';
                
                if ( $content->country ) {
                    $toHtml .= '<td> <img src="'.asset('images/' . $content->country->avatar).'" /> '.$content->country->name.'</td>';
                }       
                else {
                    $toHtml .=  '<td> N/A</td>';
                }
                                
                            
                $toHtml .=  '   <td>'.$content['service'].'</td>
                                <td>'.$content['type'].'</td>
                                <td>'.$content['classes'].'</td>
                                <td>$'.number_format($content['amount'],2).'</td>
                                <td> <a href="cart?id='.$content['id'].'&delete=true&response=ajax" data-item-id="'.$content['id'].'" class="btn-remove-item"><i class="fa fa-trash"></i></a></td>
                            </tr>';
                
                // Display coupons per item
                if ( $cartItem->coupons ) {
                    foreach( $cartItem->coupons as $couponCode ) {
                        // dd($couponCode->code->action_code->case_number);
                        if ( $couponCode->code ) {

                            $hasCoupon = true;
                            $displayCode = $couponCode->code->action_code->case_number;
                            $displayCodeId = $couponCode->promo_code_id;
                            $actionAjax = 'removeCoupon';

                            if ( $couponCode->code && $couponCode->code->action_code->case_number == $user->subscribe_promo ) {
                                $actionAjax = 'removeCouponUser';
                                
                            }
                            

                            $toHtml .=  '<tr style="color:#f00;font-style:italic;">
                                    <td colspan="4"></td>
                                    <td colspan="2">Coupon : '.$displayCode.'</td>
                                    <td> - $'.number_format($cartItem->total_discount, 2).'</td>
                                    <td><a data-coupon-id="'.$displayCodeId.'" data-action-method="'.$actionAjax.'" href="cart?action='.$actionAjax.'&itemId=1&response=ajax" class="btn-remove-coupon"><i class="fa fa-trash"></i></a></td>
                                </tr>';
                        }
                    }
                    
                }

                if ( $cartItem->campaign_id ) {
                    $hasCoupon = true;

                    $toHtml .=  '<tr style="color:#f00;font-style:italic;">
                                    <td colspan="4"></td>
                                    <td colspan="2">Campaign Discount : </td>
                                    <td> - $'.number_format($cartItem->total_discount, 2).'</td>
                                    <td></td>
                                </tr>';
                }
                
                if ( auth()->user()->role_id == 3 ) {
                    $hasCoupon = true;

                    $toHtml .=  '<tr style="color:#f00;font-style:italic;">
                                    <td colspan="4"></td>
                                    <td colspan="2">Partner Discount : </td>
                                    <td> - $'.number_format($cartItem->total_discount, 2).'</td>
                                    <td></td>
                                </tr>';
                }
                
                if ($content['status'] == 'active') {
                    $discountItem = $content['total_discount'] ? $content['total_discount'] : 0;
                    $total += ($content['amount'] - $content['total_discount']);
                    $totalDiscount += $content->total_discount;
                }
            } // end foreach

            // $totalAmount = 0;
            // if ( $total > 0 && $total > $dataCoupon['totalDiscount'] ){
            //     $totalAmount = $total - $dataCoupon['totalDiscount'];
            // }

            if ( $referral && $referral->apply == 'yes' ) {
                $toHtml .= '<tr>
                                <td colspan="4"></td>
                                <td colspan="2" class="text-center" style="color:#e3342f;"><i>Signup discount applied</i></td>
                                <td>$'.number_format($totalDiscount, 2).'</td>
                                <td></td>
                            </tr>';
            }


            $toHtml .=  '<tr>';
                
            if ( count( $contents ) != 0 ) {

                $couponhtml = '';

                if (!$hasCoupon) {
                    if ( !$referral || $referral->apply == 'no' ) {
                        $couponhtml = '<form method="get" action="/cart" id="frm-add-coupon">
                                        <div class="form-row align-items-center">
                                            <div class="col-sm-8">
                                                <input type="text" name="coupon" id="coupon-input" placeholder="Enter a coupon" class="form-control">
                                            </div>   
                                            <div class="col-sm-4">
                                                <button type="button" class="btn btn-primary btn-add-coupon">Add Coupon</button>
                                            </div>
                                        </div>
                                        <input type="hidden" name="action" value="addCoupon">
                                    </form>';
                    }
                }

                
                $toHtml .= '    <td colspan="3"><a href="/checkout" class="btn btn-danger">Proceed to checkout</td>
                                <td colspan="2"> 
                                    '.$couponhtml.'
                                </td>';
            } else {
                $toHtml .= '    <td colspan="3"></td>
                                <td colspan="2"></td>';
            }
            
            $toHtml .= '        <td style="padding-top: 22px;">Total</td>
                                <td style="color: red; padding-top: 22px;">$'.number_format( $total, 2 ).'</td>
                                <td></td>
                            </tr>';
        } else {
            $toHtml .= '<table id="tbl-cart-items" class="table table-hover table-bordered">

                        <thead>
                            <tr>
                                <td style="width:34px;"></td>
                                <td style="color:red;"> Item #</td>
                                <td> Country</td>
                                <td> Service</td>
                                <td> Type</td>
                                <td> Classes</td>
                                <td> Amount</td>
                                <td> <i class="fa fa-cog"></i></td>
                            </tr>
                        </thead>';
            $toHtml .= '<tbody><tr><td colspan="8" class="text-center"><i>Cart Empty</i></td></tr></tbody>';
        } // end if contents
        
        $toHtml .= '</table>';

        

        return $toHtml;
    }

    public function applyCoupon($rpoCoupon, $cart_id)
    {
        // dd($rpoCoupon);
        $item = $this->cart->find($cart_id);
        // dd($item);
        // $cartItem = $this->cart->find($cart_id);
        
        if ( $item->campaign_id ) {
            return;
        }


        $find = $this->cartCoupons->where([
            ['user_id', auth()->user()->id],
            ['cart_id', $cart_id],
            ['promo_code_id', $rpoCoupon->id],
        ])->first();

        if ( !$find ) {
            
            // fetch percentage

            $this->cartCoupons::create( [
                'promo_code_id'    => $rpoCoupon->id,
                'cart_id'      => $cart_id,
                'user_id'    => auth()->user()->id,
                
            ] );
        }

        $this->reCalculateItem();
        // }
        
        
    }

    public function reCalculateItem()
    {   
        $user =  Auth::user();
        $referralInvite = $this->actionCodeReferralInvite->where('invited_user_id', $user->id)->first();

        if ($referralInvite && $referralInvite->apply == 'yes') {
            return false;
        }

        $cartItems = $this->cart->getUserCartActiveItems();
        $cartTotal = $this->cart->get_cart_total();

        $randomDiscount = 0;

        if ( auth()->user()->role_id != 3 ) 
        foreach ($cartItems as $cartItem) {
            $totalDiscount = 0;
            // $randomDiscount = false;
            // dd($cartItem);
            if ($cartItem->coupons) {
                
                foreach( $cartItem->coupons as $itemCode ) {
   
                    if ( $itemCode->code ) {

                        if ( $itemCode->code->supplementary_amount_point && $itemCode->code->supplementary_amount_point <= $cartTotal ) {
                            $totalDiscount += ($itemCode->code->discount + $itemCode->code->supplementary_discount);


                        } elseif( $itemCode->code->max_discount > 0 ) {
                            
                            // dd(0);

                            if ($cartItem->assigned_discount <= 0) {
                                
                                // fetch country discount
                                $countryDiscountItem = $this->countryDiscount->where('country_id',$cartItem->country_id)->first();
                                
                                if ($countryDiscountItem) {
                                    $randomDiscount = ($countryDiscountItem->total_discount <= $itemCode->code->max_discount ? $countryDiscountItem->total_discount : $itemCode->code->max_discount);
                                }

                                $totalDiscount += $randomDiscount;
                                $cartItem->assigned_discount = $totalDiscount;
                                
                            } else {
                                $totalDiscount += $cartItem->assigned_discount;
                            }
                            
                        } else {
                            // dd($itemCode->code->max_discount);
                            // dd('else');
                            $totalDiscount += $itemCode->code->discount;
                        }


                    }
                }
            }

            if ($cartItem->campaign_id) { 
                $totalDiscount += $cartItem->total_discount;
            }
            
            if ( !$cartItem->campaign_id ) { 
                $cartItem->total_discount = $cartItem->amount * ($totalDiscount / 100);
            }
            $cartItem->save();
        }

    }

    public function removeCoupon($itemId)
    {
        // $itemCoupon = $this->cartCoupons->where([
        //     ['user_id', auth()->user()->id],
        //     ['promo_code_id', $itemId],
        // ])->first();

        // $cartId = $itemCoupon->cart_id;
        
        $find = $this->cartCoupons->where([
            ['user_id', auth()->user()->id],
            ['promo_code_id', $itemId],
        ])->delete();
        // dd($find);
        $this->reCalculateItem();
        // dd($find);
    }

    public function removeItemCoupon($cartId)
    {
        $find = $this->cartCoupons->where([
            ['user_id', auth()->user()->id],
            ['cart_id', $cartId],
        ])->delete();
    }

    public function updateCartCoupons($order, $cartItems)
    {
        // dd($cartItems);
        foreach($cartItems as $cartItem) {
            $cartCoupon = $this->cartCoupons->where([
                ['user_id', auth()->user()->id],
                ['cart_id', $cartItem->id],
            ])->first();
            if ($cartCoupon) {
                $cartCoupon->order_id = $order->id;
                $cartCoupon->save();
            }
            
        }
    }

    public function updateCaseCampaign( $cartItems ){
        foreach($cartItems as $cartItem) {
            // check if cart item has case id if so, update case mark as ordered
            if ( $cartItem->case_id ) {
                $case = $this->rpoCase->find($cartItem->case_id);

                if ($case) {
                    $case->is_purchase = 'yes';
                    $case->save();
                }
            }
        }
    }

    public function sendNewOrder($order, $contents)
    { 
        $user =  $order->user;
        $hasDiscount = false;
        $hasMonitoring = false;

        // dd($order->payment);
        foreach ($contents as $item) {
            if ( $item->coupons && count($item->coupons) > 0 ){
                $hasDiscount = true;
            }

            if ( $item->service == 'Monitoring' ){
                $hasMonitoring = true;
            }
        }

        $subject = "TradeMarkers LLC " . $order->order_number . " | TradeMarkers.net";
        // dd($user);
        // client mail
        $res = Mail::send('mail.new-order-template', ['user' => $user, 'order' => $order, 'contents' => $contents, 'hasDiscount' => $hasDiscount], function ($m) use ($user, $subject) {
            $m->from('info@trademarkers.net', 'TradeMarkers LLC');

            $m->to($user->email, $user->name)->subject($subject);
        });

        // SEND SEPARATE EMAIL IF HAS MONITORING
        if ( $hasMonitoring ) {
            $subject = "Lets protect your trademark from infringement!";
            $res = Mail::send('mail.new-order-monitoring', ['user' => $user], function ($m) use ($user, $subject) {
                $m->from('info@trademarkers.net', 'TradeMarkers LLC');
                $m->to($user->email, $user->name)->subject($subject);
            });
        }
        

        // info mail
        $subjectInfo = "New Order to Trademarkers";
        $res = Mail::send('mail.new-order-template', ['user' => $user, 'order' => $order, 'contents' => $contents, 'hasDiscount' => $hasDiscount], function ($m) use ($user, $subjectInfo) {
            $m->from('notifications@trademarkers.net', 'TradeMarkers LLC');

            $m->to(['info@trademarkers.net'], 'info')->subject($subjectInfo);
        });

        // dd($res);
        return $res;
    }

    // GET CAMPAIGN DISCOUNT
    public function getcampaignDiscount($campaignId)
    {
        $campaign = $this->rpoCoupon->where('campaign_id',$campaignId)->first();
        // dd($campaign);
        return $campaign;
    }

    public function applyReferralInvite($apply = 'no')
    {
        $user =  Auth::user();

        $referralInvite = $this->actionCodeReferralInvite->where('invited_user_id', $user->id)->first();

        $referralInvite->apply = $apply;
        $referralInvite->save();
        
        return $referralInvite;
    }



    public function cleanUpItems()
    {
        $items = $this->cart->get_user_cart_items();

        foreach ($items as $item) {

            $totalDiscount = 0;
            // dd($item->coupons);
            if ($item->coupons) {
                foreach( $item->coupons as $itemCode ) {
                    // dd($itemCode->code->expiry_date);
                    if ( $itemCode->code ) {

                        if ( $itemCode->code->expiry_date && $itemCode->code->expiry_date < now() ) {
                            $itemCode->delete();
                        }

                        if ( !in_array($item->service, $itemCode->code->promo_type) ) {
                            $itemCode->delete();
                        }
                       

                    }
                }
            }

            $item->save();
        }

        $this->reCalculateItem();
    }

    public function sendAdminNotification($coupon, $rpoCoupon)
    {
        $user = auth()->user();
        // auth()->user()
        // dd($user);

        $message = $this->getCouponErrorMessage($rpoCoupon);

        $subjectInfo = "Invalid Adding of coupon code[".$coupon."]";
        $res = Mail::send('mail.notification-invalid-coupon', ['user' => $user, 'coupon' => $coupon, 'sysMessage' => $message], function ($m) use ($user, $subjectInfo) {
            $m->from('notifications@trademarkers.net', 'TradeMarkers LLC');

            $m->to(['info@trademarkers.com'], 'info')->subject($subjectInfo);
        });


    }

    public function sendAdminNotificationPaymentError($request, $ex)
    {
        $user = auth()->user();

        // $message = $this->getCouponErrorMessage($rpoCoupon);

        $subjectInfo = $user->name . " Tried to make payments";
        $res = Mail::send('mail.notification-error-payment', ['user' => $user, 'request' => $request, 'sysMessage' => $ex], function ($m) use ($user, $subjectInfo) {
            $m->from('notifications@trademarkers.net', 'TradeMarkers LLC');

            $m->to(['info@trademarkers.com'], 'info')->subject($subjectInfo);
        });


    }

    
}