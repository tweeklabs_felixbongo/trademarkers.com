<?php
namespace App\Services;

use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Mail;
use App\Repositories\Utils;


class CronEventService 
{

    public $rpoEvent;
    public $rpoDomainEmail;
    public $selectedEvent;

    public function __construct(
        \App\Event $rpoEvent,
        \App\DomainEventEmail $rpoDomainEmail
        
    ) {
        $this->rpoEvent = $rpoEvent;
        $this->rpoDomainEmail = $rpoDomainEmail;

        $this->selectedEvent = $this->getEvents();

        // $this->populateDomainEmail();
    }

    public function getEvents()
    {
        return $this->rpoEvent->whereNotNull('brand_id')->get();
        
    }

    public function populateDomainEmail()
    {

        foreach ( $this->selectedEvent as $event ) {
            // dd($event->brand->domains);
            if ($event->brand && $event->brand->domains) 
            foreach ($event->brand->domains as $domain) {
                $this->generateRandomEmail($domain);
            }
        }
    }


    public function generateRandomEmail($domain)
    { 

        $emails = $this->generateEmails($domain);
        
        foreach ($emails as $email) {

            if (!$this->emailExist($email) && $email){
                $data = [
                    "brand_domain_id"  => $domain->id,
                    "email"  => $email,
                    "notes"     => "System Generated"
                ];
                $this->rpoDomainEmail->create($data);
            }
        }
        
    }

    public function generateEmails($domain)
    {
        // dd(0);
        $firstName = $this->clean($domain->first_name);
        $lastName = $this->clean($domain->last_name);
     
        if ( $domain->admin_email ) 
        $data['owner'] = $domain->admin_email;

        if ( $domain->other_emails ) {
            $other_emails = explode(",", $domain->other_emails);

            

            foreach ($other_emails as $key => $omails) {

                // check here if this is a valid email
                if ( filter_var(  trim($omails), FILTER_VALIDATE_EMAIL ) ) {
                    $data['other'. $key ] = trim($omails);
                }

                
            }
        } 
        $data['owner'] = $domain->admin_email;

        if ( $firstName || $lastName ) {
            $data['name1'] = $firstName . '@' .$domain->domain;
            if ( $lastName ) {
                $data['name2'] = $firstName . '.' . $lastName . '@' .$domain->domain;
                $data['name3'] = substr($firstName,0,1) . '.' . $lastName . '@' .$domain->domain;
                $data['name4'] = $firstName . $lastName . '@' .$domain->domain;
            }
        }
        
        if ( $domain && $domain->domain ) {
            $data['legal'] = 'legal@' .$domain->domain;
            $data['webmaster'] = 'webmaster@' .$domain->domain;
            $data['info'] = 'info@' .$domain->domain;
        }
        

        return $data;
    }

    function clean($string) {

        $string = str_replace(' ', '', $string); 
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);

        return strtolower($string);
    }

    public function emailExist($email)
    {
        return $this->rpoDomainEmail->where('email',$email)->first();
    }


}