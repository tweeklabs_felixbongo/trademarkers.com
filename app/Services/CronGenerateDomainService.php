<?php
namespace App\Services;

use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Mail;
use App\Repositories\Utils;
use App\BrandLead;


class CronGenerateDomainService  
{

    public $rpoEvent;
    public $rpoLeads;
    public $rpoDomainEmail;
    public $rpoBrand;
    public $rpoDomain;
    public $rpoCampaign;
    public $eventService;

    public function __construct(
        \App\Event $rpoEvent,
        \App\DomainEventEmail $rpoDomainEmail,
        \App\Brand $rpoBrand,
        \App\BrandDomain $rpoDomain,
        \App\ActionCodeCase $rpoLeads,
        \App\Services\CronEventService $eventService,
        \App\ActionCodeCampaign $rpoCampaign
        
    ) {
        $this->rpoEvent = $rpoEvent;
        $this->rpoDomainEmail = $rpoDomainEmail;
        $this->rpoLeads = $rpoLeads;
        $this->rpoBrand = $rpoBrand;
        $this->rpoDomain = $rpoDomain;
        $this->eventService = $eventService;
        $this->rpoCampaign = $rpoCampaign;


        // $this->populateDomainEmail();
    }

    

    public function generateDomain()
    {
        $campaign = $this->rpoCampaign->where('cron','yes')->first();

        if ( $campaign ) {

        
            // $campaignId = '68';

            $leads = $this->rpoLeads->where('action_code_campaign_id', $campaign->id )->get();

            foreach ( $leads as $lead ) {
                if ( strlen($lead->trademark) <= 20 ) 
                try {

                    if ( !$lead->brand ) {

                        $brand = $this->rpoBrand->firstOrCreate(['brand' => $lead->trademark]);

                        if ($brand) {
                            BrandLead::create([
                                'brand_id' => [$brand->id],
                                'lead_id' => $lead->id
                            ]);
                        }

                        $lead = $this->rpoLeads->find($lead->id);

                    }

                    if ( $lead->brand ) {

                        // dd( $lead->brand->id );
                        $brand = $this->rpoBrand->find($lead->brand->id);
                       
                        // dd($brand->domains);
                        if ( isset($brand->domains) ) { 
                            if ( $domain = $this->transformMarkDomain($lead->trademark) ) { 
                                
                                $event = $this->rpoEvent
                                        ->where([
                                            ["brand_id" , $lead->brand->brand_id[0]],
                                            ["event_type_id" , "17"],
                                            ["action_code_case_id" , $lead->id]
                                        ])
                                        ->first();

                                if ( !$event ) {
                                    // CREATE DOMAIN
                                    // dd($lead->brand, $brand);
                                    // dd($lead->brand->brand_id[0]);
                                    if ( !$this->hasDomain($domain) ) {
                                        $dom = $this->rpoDomain->create([
                                            'brand_id' => $lead->brand->brand_id[0],
                                            'domain' => $domain,
                
                                            'notes' => 'System Generated'
                                        ]);

                                        // generate email address on this domain created
                                        $this->eventService->generateRandomEmail($dom);
                                    }
                                    

                                    

                                    // CREATE EVENT
                                    $this->rpoEvent->create([
                                        'event_type_id' => '17',
                                        'brand_id' => $lead->brand->brand_id[0],
                                        'action_code_case_id' => $lead->id,
                                        'action' => 'published',
                                        'notes' => 'System Generated'

                                    ]);
                                    
                                } else {
                                    echo "\n event exist --- " . $event->id;
                                }
                                
                            }
                        } 

                    
                        
                    }

                } catch (Exception $e) {
                
                }
            } // end foreach

            $campaign->cron = 'done';
            $campaign->save();
        
        }
        
    }

    public function transformMarkDomain($mark)
    {
        $markArr = explode(' ', $mark);

        if ( $mark != 'NULL' ) {
            
            if ( $mark && count($markArr) <= 2 ) {
                $domain = $this->clean($mark);

                if ( $domain ) {
                    return $domain . '.com';
                } else {
                    return null;
                }
            } else {
                return null;
            }

        } 

        return null;

    }

    public function clean($string) {

        $string = str_replace(' ', '', $string); 
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);

        return strtolower($string);
    }

    public function hasDomain($name)
    {
        $domain = $this->rpoDomain->where('domain', $name)->first();


        return $domain ? true : false;
    }


}