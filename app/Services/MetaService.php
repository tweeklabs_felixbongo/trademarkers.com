<?php
namespace App\Services;

use Illuminate\Support\Facades\Route;
use Torann\LaravelMetaTags\Facades\MetaTag;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Mail;

class MetaService 
{

    public $rpoMeta;
    public $rpoArticle;
    public $path;

    public function __construct(
        \App\MetaTag $rpoMeta,
        \App\Blog $rpoArticle
    ) {
        $this->rpoMeta = $rpoMeta;
        $this->rpoArticle = $rpoArticle;
        if ( Route::getFacadeRoot() && $this->path = Route::getFacadeRoot()->current()) {
            $this->path = Route::getFacadeRoot()->current()->uri();
            $this->path = $this->path == '/' ? $this->path : '/' . $this->path;
        }
        
    }

    public function getMetaTag()
    {
        $currentPath= Route::getFacadeRoot()->current()->uri();

        $meta = $this->rpoMeta->getMeta($this->path);
        // dd($this->path);
        if ($meta) {
            MetaTag::set('title',$meta->title);
            MetaTag::set('description',$meta->description);
            MetaTag::set('keywords',$meta->keywords);
        }
        
        return $meta;
    }

    // GET MEWTA BY SLUG FROM ARTICLES
    // SINCE CURRENT URI WILL RETURN ROUTE NAME
    public function getMetaTagArticleSlug($slug)
    {
        $currentPath = '/blog/'.$slug;

        $meta = $this->rpoMeta->getMeta($currentPath);
        // dd($this->path);
        if ($meta) {
            MetaTag::set('title',$meta->title);
            MetaTag::set('description',$meta->description);
            MetaTag::set('keywords',$meta->keywords);

            $blog = $this->rpoArticle->where('slug', $slug)->first();

            if ($blog && $blog->featured_img) {
                MetaTag::set('image',url('/uploads/').'/'.$blog->featured_img);
            }
        }
        
        return $meta;
    }


    
}