<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'user_id', 'reference_no', 'pay_type', 'gateway', 'card_type', 'card_ref', 'currency', 'amount', 'order_id', 'created_at'
    ];

	public function user()
    {
        return $this->belongsTo( User::class );
    }

    public function order()
    {
        return $this->belongsTo( Order::class );
    }

    public function invoice()
    {
        return $this->hasOne( Invoice::class );
    }

    public static function today_total_payment()
    {
        $result = Payment::whereRaw('date(created_at) = curdate()')->sum('amount');

        return $result;
    }
}
