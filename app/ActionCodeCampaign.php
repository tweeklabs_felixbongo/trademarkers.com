<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActionCodeCampaign extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'count', 'file_name'
    ];

    public function cases()
    {
        return $this->hasMany( ActionCodeCases::class );
    }

    public static function check_duplicate_campaign( $filename )
    {
    	$campaign = ActionCodeCampaign::where('name', 'like', "%" . $filename . "%")->get();

    	if ( count( $campaign->toArray() ) == 0 ) {

    		return false;
    	}

    	return true;
    }

}
