<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ActionCodeCase extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'action_code_id', 'action_code_campaign_id', 'number', 'trademark', 'tm_filing_date',
        'tm_holder', 'application_reference', 'mark_current_status_code', 'mark_current_status_date',
        'ref_processed_date', 'registration_date', 'address', 'city', 'state', 'city_of_registration',
        'class_number', 'class_description', 'representatives', 'email', 'fax_no', 'telephone_no',
        'website', 'zip', 'country', 'discount', 'discount_expiry_date', 'expiry_date', 'country_code', 'is_purchase',
        'jurisdiction', 'is_priority', 'notes'
    ];

    // protected $appends = ['tm_expired_filling_date'];

    public function getTmExpiredFillingDate()
    {
        return Carbon::parse($this->tm_filing_date)->addMonths(1);
    }

    public function getTmExpiredFillingDateEUIPO()
    {
        return Carbon::parse($this->mark_current_status_date)->addMonths(3);
    }

    public function getTmExpiredFillingDate6M()
    {
        return Carbon::parse($this->tm_filing_date)->addMonths(6);
    }

    // public function setTmExpiredFillingDate()
    // {
    //     $this->tm_expired_filling_date = Carbon::parse($this->tm_filing_date)->addMonths(1);
    // }

    public function campaign()
    {
        return $this->belongsTo( ActionCodeCampaign::class, 'action_code_campaign_id' );
    }

    public function action_code()
    {
        return $this->belongsTo( ActionCode::class, 'action_code_id' );
    }

    // public function event()
    // {
    //     return $this->belongsTo( Event::class, 'action_code_case_id' );
    // }

    public function brand()
    {
        return $this->hasOne( BrandLead::class , 'lead_id');
    }
    
    public function emails()
    {
        return $this->hasMany(DomainEventEmail::class);
    }

    public function domain()
    {
        return $this->hasMany(BrandDomain::class);
    }

    
}
