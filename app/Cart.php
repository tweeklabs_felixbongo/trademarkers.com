<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Cart extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'country_id', 'name', 'profile_id', 'service', 'type', 'logo', 'classes',
        'amount', 'recently_filed', 'commerce', 'priority_number', 'priority_date',
        'priority_country', 'claim_priority', 'classes_description', 'relative_trademark_id', 'campaign_id', 
        'case_number', 'total_discount', 'case_id', 'color_claim', 'assigned_discount'
    ];

    public function user()
    {
    	return $this->belongsTo( User::class );
    }

    public function profile()
    {
        return $this->belongsTo( Profile::class );
    }

    public function country()
    {
        return $this->belongsTo( Country::class );
    }

    public function coupons()
    {
        return $this->hasMany( CartCoupon::class, 'cart_id' );
    }

    public static function get_user_cart_items()
    {
    	$items = Cart::with(['coupons','coupons.code', 'coupons.code.action_code'])->where('user_id', auth()->user()->id)
            ->whereIn('status', ['active', 'inactive'])
            ->get();

    	return $items;
    }

    public static function getUserCartActiveItems()
    {
    	$items = Cart::with(['coupons','coupons.code', 'coupons.code.action_code'])->where([
            ['user_id', auth()->user()->id],
            ['status', 'active'],
        ])->get();

    	return $items;
    }

    public static function clear_cart()
    {
    	Cart::where('user_id', auth()->user()->id)->delete();
    }

    public static function get_cart_total()
    {
        $total =  Cart::where([
            ['user_id', auth()->user()->id],
            ['status', 'active'],
        ])->sum('amount');

        return $total;
    }

    public static function get_cart_totalDiscount()
    {

        $totalDiscount =  Cart::where([
            ['user_id', auth()->user()->id],
            ['status', 'active'],
        ])->sum('total_discount');

        return $totalDiscount;
    }

    public static function count_user_cart()
    {
        if ( Auth::check() ) {

            $result = Cart::get_user_cart_items();

            return count( $result->toArray() );
        }
        
        return 0;
    }

    public static function delete_item_cart( $cart_id )
    {
        $item = Cart::find( $cart_id );
        $data = [];

        if ( $item ) {

            if ( $item->user_id == auth()->user()->id ) {

                $success = Cart::destroy( $cart_id );

                if ( $success ) {

                    $data['type']    = "info";
                    $data['message'] = "The item is successfully removed!";

                    return $data;
                }
            }

        }

        $data['type']    = "danger";
        $data['message'] = "Something went wrong, please try again!";
        
        return $data;
    }

    public static function add_cart( $data )
    {
        $abbr     = $data['chosen_country']['abbr'];
        $country  = Country::validate_country_code( $abbr );
        $origin   = null;

        if ( isset( $data['origin'] ) ) {
            $origin = Country::validate_country_code( $data['origin'] );
            $origin  = $origin->name;
        }
        // dd(auth()->user()->profiles);
        $cart = Cart::create( [
            'user_id'                => auth()->user()->id,
            'country_id'             => $country['id'],
            'profile_id'             => isset($data['profile_id']) ? $data['profile_id'] : auth()->user()->profiles[0]->id,
            'service'                => $data['service'],
            'type'                   => $data['type'],
            'name'                   => $data['name'],
            'logo'                   => $data['logo'],
            'classes'                => $data['classes'],
            'amount'                 => $data['amount'],
            'recently_filed'         => isset( $data['recently_filed'] ) ? $data['recently_filed'] : null,
            'commerce'               => isset( $data['commerce'] ) ? $data['commerce'] : null,
            'priority_number'        => isset( $data['tm'] ) ? $data['tm'] : null,
            'priority_date'          => isset( $data['date'] ) ? $data['date'] : null,
            'priority_country'       => $origin,
            'claim_priority'         => isset( $data['claim_priority'] ) ? $data['claim_priority'] : null,
            'classes_description'    => isset( $data['classes_description'] ) ? $data['classes_description'] : "",
            'relative_trademark_id'  => isset( $data['relative_trademark_id'] ) ? $data['relative_trademark_id'] : null,
            'campaign_id'            => isset($data['campaign_id']) ? $data['campaign_id'] : null,
            'case_number'            => isset( $data['case_number'] ) ? $data['case_number'] : null,
            'total_discount'         => isset( $data['total_discount'] ) ? $data['total_discount'] : '0',
            'case_id'                => isset( $data['case_id'] ) ? $data['case_id'] : null,
            'color_claim'            => isset( $data['color_claim'] ) ? $data['color_claim'] : null,
        ] );

        return $cart;
    }

    public function getActionCode()
    {
        $ActionCart = ActionCodeCart::where( 'cart_id', $this->id )->first();
                // dd($ActionCart->action_code->case_number);
        return isset($ActionCart->action_code->case_number) ? $ActionCart->action_code->case_number : 'N/A';
    }
    /**
     * ************* REVISIONS STARTS HERE *************
     */
    
    //  public function getCart()
    //  {
         
    //  }
    public function updateToggle(){
        if ($this->status == 'active'){
            $this->status = 'inactive';
            $this->save();            
        } else if ( $this->status == 'pending' ) { 
            $this->status = 'active';
            $this->save(); 
        }else {
            $this->status = 'active';
            $this->save(); 
        }
    }

    public static function updateCartItemComplete($orderId)
    {
        $activeItems =  Cart::where([
            ['user_id', auth()->user()->id],
            ['status', 'active'],
        ])->get();

        foreach ( $activeItems as $item ) {
            $item->status = 'completed';
            $item->order_id = $orderId;
            $item->save();
        }
        // return $total;
    }
}
