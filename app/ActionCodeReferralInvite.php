<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cohensive\Embed\Facades\Embed;

class ActionCodeReferralInvite extends Model
{
    protected $fillable = [
        'referrals_id',
        'invited_user_id',
        'apply'
        ];

    public function user()
    {
        return $this->hasOne( User::class, 'id','invited_user_id');
    }

    public function transactions()
    {
        return $this->hasOne( ActionCodeReferralTransaction::class, 'ref_invite_id', 'id');
    }
}
