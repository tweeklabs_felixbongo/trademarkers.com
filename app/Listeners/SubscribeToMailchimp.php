<?php

namespace App\Listeners;

use App\Events\UserRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use \DrewM\MailChimp\MailChimp;

class SubscribeToMailchimp
{
    private $mailchimp;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
      $this->mailchimp = new MailChimp('3fac7d9f3b477c56ab494d1006a2a3f9-us16');
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistered  $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
      $list_id = '0a6282286c';
      $res = $this->mailchimp->post("lists/$list_id/members", [
				'email_address' => $event->user->email,
				'status'        => 'subscribed',
      ]);
      
      return $res;
    }
}
