<?php

namespace App\Listeners;

use App\Events\UserRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class SendVerificationEmail
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
  
    }

    /**
     * Handle the event.
     *
     * @param  VerifyEmail  $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
      // $list_id = '0a6282286c';
      // $res = $this->mailchimp->post("lists/$list_id/members", [
			// 	'email_address' => $event->user->email,
			// 	'status'        => 'subscribed',
      // ]);

      $internalSubject = "Please Verify Email Address";
      $res = Mail::send('mail.verify-email', ['user' => $event->user], function ($m) use ($internalSubject, $event) {
          $m->from('notifications@trademarkers.com', 'TradeMarkers LLC');

          $m->to($event->user->email, $event->user->name)->subject($internalSubject);
      });
      
      return $res;
    }
}
