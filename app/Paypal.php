<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Paypal extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'paypal_order_id', 
        'paypal_payer_id', 
        'order_id', 
        'user_id', 
        'amount', 
        'message_status'
    ];

    protected $table = 'paypal';



}
