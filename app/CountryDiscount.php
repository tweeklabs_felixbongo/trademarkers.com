<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class CountryDiscount extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'country_referral_discounts';
    
    public function country() {

    	return $this->belongsTo( Country::class );
    }

    
}
