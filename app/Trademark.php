<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Country;
use App\Price;

class Trademark extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'case_number', 'country_id', 'order_id', 'profile_id', 'euipo_id',
        'service', 'type', 'name', 'logo', 'summary', 'filing_date', 'filing_number',
        'app_reference', 'registration_date' , 'expiry_date', 'classes',
        'classes_description', 'office', 'office_status', 'status_description',
        'brand_name', 'purpose', 'value', 'category', 'hvt', 'revocation',
        'priority_date', 'priority_country', 'priority_number', 'recently_filed',
        'claim_priority', 'commerce', 'rep_name',
        'rep_email', 'rep_phone', 'rep_fax', 'number', 'registration_number',
        'international_registration_number', 'amount', 'discount', 'atty_id','rep_id',
        'logo_description','relative_trademark_id', 'created_at','office_deadline_date', 'paralegal_id','paralegal_email','color_claim'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
         'discount', 'atty_name'
    ];
    
    public function user()
    {
        return $this->belongsTo( User::class );
    }

    public function order()
    {
        return $this->belongsTo( Order::class );
    }

    public function country()
    {
        return $this->belongsTo( Country::class );
    }

    public function profile()
    {
        return $this->belongsTo( Profile::class );
    }

    public function attachments()
    {
        return $this->hasMany( Attachment::class );
    }

    public function comments()
    {
        return $this->hasMany( Comment::class );
    }

    public function icomments()
    {
        return $this->hasMany( Icomment::class );
    }

    public static function get_user_trademark( $trademark_id )
    {
        $trademark = Trademark::where( [ 'user_id' => auth()->user()->id, 'id' => $trademark_id ] )->get();

        if ( count( $trademark ) < 1 ) {

            return false;
        }

        return $trademark;
    }

    /**
     * Validate the trademark info
     *
     * @param array of request
     * @return Array of trademark info, type of service
     */
    public static function validate_trademark_info( $request, $type )
    {  
        session()->forget('trademark_order');

        $trademark_info = [];
        // dd($request['service']);
        // INSERT FOR MONITORING
        // dd($request->all());
        if ( $request['action'] ) {

            $trademark_info['service']        = $request['service'];
            $trademark_info['recently_filed'] = null;
            $trademark_info['commerce']       = null;
            $trademark_info['type']      = "N/A";
            $trademark_info['name']      = $request['word_mark'];
            $trademark_info['logo']      = "N/A";
            // $trademark_info['amount']      = "150";
            $trademark_info['case_number']      = $request['case_number'];
            // $request['amount'] = '150';
            
        } else {

            // ORIGINAL FLOW
            if ( $type ) {

                $trademark_info['service']        = "Trademark Study";
                $trademark_info['recently_filed'] = null;
                $trademark_info['commerce']       = null;
    
            } else {
    
                $trademark_info['service']        = "Trademark Registration";
                $trademark_info['recently_filed'] = $request['filed'];
                $trademark_info['commerce']       = $request['commerce'];
            }
    
            if ( $request['type'] == "word" ) {
    
                $trademark_info['type']      = "Word-Only";
                $trademark_info['name']      = $request['word_mark'];
                $trademark_info['logo']      = "N/A";
    
            } else if ( $request['type'] == "logo" ) {
    
                $trademark_info['type']      = "Design-Only or Stylized Word-Only(Figurative)";
                $trademark_info['name']      = "N/A";
                $trademark_info['logo']      = Trademark::move_image( $request->logo_pic );
    
            } else if ( $request['type'] == "lword" ) {
    
                $trademark_info['type']      = "Combined Word and Design";
                $trademark_info['logo']      = Trademark::move_image( $request->logo_pic );
                $trademark_info['name']      = $request['word_mark'];
            }

        }
        

        $trademark_info['chosen_country'] = $request['chosen_country'];
        $trademark_info['classes']        = $request['classes'];
        $trademark_info['amount']         = $request['amount'];
        $trademark_info['classes_description']   = $request['descriptions'];
        $trademark_info['claim_priority'] = isset( $request['priority'] ) ? $request['priority'] : null;
        $trademark_info['origin']         = isset( $request['origin'] ) ? $request['origin'] : null;
        $trademark_info['date']           = isset( $request['date'] ) ? $request['date'] : null;
        $trademark_info['tm']             = isset( $request['tm'] ) ? $request['tm'] : null;
        $trademark_info['color_claim']    = isset( $request['color_claim'] ) ? $request['color_claim'] : 'no';
        // dd($trademark_info);
        session( ['trademark_order' => $trademark_info ] );
    }

    public static function move_image( $image )
    {
        $imagename = md5( time() ) . '.' . $image->getClientOriginalExtension();

        $image->move( public_path('uploads'), $imagename );

        return $imagename;
    }

    /**
     * Validate the trademark contact info
     *
     * @param array of profile
     * @return Array of trademark and trademark contact
     */
    public static function validate_trademark_contact_info( $profile )
    {

        $trademark = session('trademark_order');

        $arr = array(
            'profile_id' => $profile['id'],
            'company'    => $profile['company']
        );

        session()->forget('trademark_order');

        $info = array_merge( $trademark, $arr );

        session( ['trademark_order' => $info ] );
    } 

    /**
     * Get a trademark handler for a specific country code
     *
     * @param country code
     * @return handler
     */
    public static function get_handler( $country_code )
    {
        // US Handlers
        $us_handlers = [ 'Rexis', 'Luigi', 'Roland' ];

        // All countries except US, handlers
        $rest_of_the_world_handlers = [ 'Bonn', 'Jenny', 'Jerome', 'Jun', 'Era' ];

        // Check if the country code is US or not
        if ( $country_code == 'US' ) {
            // Randomize US handlers array
            $handler  = array_rand( $us_handlers, 1 );

            // Assigned US handler
            $handler  = $us_handlers[$handler];

        } else {
            // Randomize rest of the world handlers array
            $handler  = array_rand( $rest_of_the_world_handlers, 1 );

            // Assigned rest of the world handler
            $handler  = $rest_of_the_world_handlers[$handler];
        }

        // TO DO

        // Check if the random handler is available

        return $handler;
    }

    public static function calculate_class_prices( $data, $service_type )
    {
        $price               = [];
        $data['classes']     = '';
        $data['descriptions'] = '';
        $classes             = count( $data['class'] );
        // dd($data->all());

        $country = Country::where( 'abbr', '=', $data['chosen_country']['abbr'] )->get();

        foreach ( $country[0]->prices as $result ) {
            
            if ( $service_type ) {

                if ( $result['service_type'] == 'Study' ) {
                    $price = $result;
                    break;
                }

            } else {

                if ( $result['service_type'] == 'Registration' ) {
                    $price = $result;
                    break;
                }
            }
        }
        // dd($classes);
        for ( $i = 0; $i < $classes; $i++ ) {

            $x = explode( 'class', $data['class'][$i] );
            // dd($data['class']);
            if ( $i + 1 == $classes ) {

                $data['classes'] .= $x[1];

            } else {

                $data['classes'] .= $x[1] . '-';
            }
            // dd($data['descriptions']);
            $data['descriptions'] .= $x[1] . "{" . (isset($data['description'][$i]) ? $data['description'][$i] : '') . "}";
        }

        if ( $data['type'] == 'word' ) {

            if ( $classes > 0 ) {

                $data['amount'] = (($classes - 1) * $price['additional_cost']) +  $price['initial_cost'];
            } else {
                return redirect('/');
            }
            
        } else {

            if ( $classes > 0 ) {

                $data['amount'] = (($classes - 1) * $price['logo_additional_cost']) +  $price['logo_initial_cost'];
            }else {
                return redirect('/');
            }
        }
        
        return $data;
    }

    public static function get_service_prices( $data )
    {
        $price               = [];
        $data['classes']     = '';
        $data['descriptions'] = '';
        // $classes             = count( $data['class'] );
        
        $price = Price::where([
            ['country_id', '=', $data['chosen_country']['id']],
            ['service_type', $data['service']]
            ])->first();
        // dd($price);
        // dd($country[0]->prices);
        $data['amount'] = $price ? $price->initial_cost : 0;
        return $data;
    }

    public static function get_service_countries( $service )
    {
        // $price               = [];
        // $data['classes']     = '';
        // $data['descriptions'] = '';
        // $classes             = count( $data['class'] );
        
        $priceCountries = Price::with('country')->where('service_type', $service)->get();
        // dd($price);
        // dd($country[0]->prices);
        // $data['amount'] = $price ? $price->initial_cost : 0;
        // dd($priceCountries);
        // foreach($priceCountries as $price) {
        //     dd($price->country);
        // }
        return $priceCountries;
    }

    /**
     * Create a new trademark instance after validation.
     *
     * @param  array  $data
     * @return \App\Trademark
     */
    public static function store( $data, $order_number, $status )
    {

        if ( $status == "paid" ) {

            $status = "Pending";

        } else {

            $status = "UnPaid";
        }
        
        return Trademark::create([
            'user_id'                           => auth()->user()->id,
            'profile_id'                        => isset( $data['profile_id'] ) ? $data['profile_id'] : 0,
            'order_id'                          => $order_number,
            'service'                           => isset( $data['service'] ) ? $data['service'] : null,
            'type'                              => isset( $data['type'] ) ? $data['type'] : null,
            'name'                              => isset( $data['name'] ) ? $data['name'] : null,
            'logo'                              => isset( $data['logo'] ) ? $data['logo'] : null,
            'classes'                           => isset( $data['classes'] ) ? $data['classes'] : null,
            'country_id'                        => isset( $data['country_id'] ) ? $data['country_id'] : null,
            'amount'                            => isset( $data['amount'] ) ? $data['amount'] : null,
            'recently_filed'                    => isset( $data['recently_filed'] ) ? $data['recently_filed'] : null,
            'commerce'                          => isset( $data['commerce'] ) ? $data['commerce'] : null,
            'office'                            => $data->country ? $data->country->name : "" ,
            'office_status'                     => $status,
            'priority_number'                   => isset( $data['priority_number'] ) ? $data['priority_number'] : null,
            'priority_date'                     => isset( $data['priority_date'] ) ? $data['priority_date'] : null,
            'priority_country'                  => isset( $data['priority_country'] ) ? $data['priority_country'] : null,
            'claim_priority'                    => isset( $data['claim_priority'] ) ? $data['claim_priority'] : null,
            'classes_description'               => isset( $data['classes_description'] ) ? $data['classes_description'] : null,
            'relative_trademark_id'             => isset( $data['relative_trademark_id'] ) ? $data['relative_trademark_id'] : null,
            'case_number'                       => isset( $data['case_number'] ) ? $data['case_number'] : null,
            'color_claim'                       => isset( $data['color_claim'] ) ? $data['color_claim'] : 'no'
        ]);
    }
}
