<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActionCodeCart extends Model
{
    protected $fillable = [
        'action_code_id', 'trademark_id', 'user_id', 'cart_id','status'
    ];

    public function action_code()
    {
        return $this->belongsTo( ActionCode::class, 'action_code_id' );
    }

    public function user()
    {
        return $this->belongsTo( User::class, 'user_id' );
    }

    public function trademark()
    {
        return $this->belongsTo( Trademark::class, 'trademark_id' );
    }

    public function cart()
    {
        return $this->belongsTo( Cart::class, 'cart_id' );
    }


    // function

    public function updateToggle(){
        if ($this->status == 'pending'){
            $this->status = 'added';
            $this->save();            
        } else {
            $this->status = 'pending';
            $this->save(); 
        }
    }

    
}
