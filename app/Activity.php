<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Activity extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'description', 'module', 'ip_address', 'browser', 'os'
    ];

    public function user()
    {
    	return $this->belongsTo( User::class );
    }

    public static function add( $description, $module )
    {
        $id = auth()->check() ? auth()->user()->id : -1;

        Activity::create( [
            'user_id'     => $id,
            'description' => $description,
            'module'      => $module,
            'ip_address'  => isset($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['HTTP_CLIENT_IP'] : isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'],
            'browser'     => '',
            'os'          => ''
        ] );
    }

    public static function today_user_activity()
    {
        $result = Activity::whereRaw('date(created_at) = curdate()')->count();

        return $result;
    }
}
