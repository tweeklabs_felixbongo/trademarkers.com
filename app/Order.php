<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dompdf\Dompdf;

class Order extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'order_number', 'total_amount', 'total_items', 'user_id', 'created_at'
    ];

    public function trademarks()
    {
    	return $this->hasMany( Trademark::class );
    }

    public function payment()
    {
        return $this->hasOne( Payment::class );
    }

    public function user()
    {
        return $this->belongsTo( User::class );
    }

    public function invoice()
    {
        return $this->hasOne( Invoice::class );
    }

    public function items()
    {
        return $this->hasMany( Cart::class );
    }

    public static function today_orders()
    {
        $result = Order::whereRaw('date(created_at) = curdate()')->count();

        return $result;
    }
}
