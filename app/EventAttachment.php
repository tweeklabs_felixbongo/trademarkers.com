<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class EventAttachment extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 
        'event_id', 
        'name', 
        'description', 
        'file_size', 
        'file_type'
    ];

    public function event()
    {
    	return $this->belongsTo( Event::class );
    }

    public function user()
    {
    	return $this->belongsTo( User::class );
    }

    public static function get_user_attachment( $attachment_id, $event_id )
    {
        $event = Event::find( $event_id );

        if ( !$event ) {
            
            return false;
        }

        if ( $event->user_id != auth()->user()->id ) {

            return false;
        }

        $attachment = EventAttachment::where( [ 'id' => $attachment_id, 'event_id' => $event_id  ] )->get();

        if ( count( $attachment ) < 1 ) {

            return false;
        }

        return $attachment;
    }

    public static function upload_attachment( $request )
    {
    	$filename = EventAttachment::move_file( $request->attachment );

    	$attachment = Attachment::create( [
    		'user_id'      => auth()->user()->id,
    		'event_id' => $request->event_id,
    		'name'         => $filename,
    		'description'  => $request->attachment->getClientOriginalName(),
    		'file_size'    => '',
    		'file_type'    => '',
    	] );

    	return $attachment;
    }

    public static function move_file( $file )
    {
        $filename = md5( time() ) . '.' . $file->getClientOriginalExtension();

        $file->move( public_path('uploads'), $filename );

        return $filename;
    }

    public static function retrieve_attachments( $event_id )
    {
    	$attachments = EventAttachment::where( ['event_id' => $event_id ] )->get();

    	return $attachments;
    }

    public static function download_attachment( $filename )
    {
        $storage = Storage::disk('admin');

        return $storage->download( $filename, $filename );
    }
}
