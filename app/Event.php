<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Event extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'event_type_id',
        'country_id',
        'case_owned_id',
        'user_id',
        'trademark',
        'tm_holder',
        'registration_date',
        'tm_filing_date',
        'address',
        'city',
        'state',
        'class_number',
        'class_description',
        'email',
        'fax_no',
        'website',
        'notes',
        'brand_id',
        'action_code_case_id',
        'action'
    ];

    public function user()
    {
    	return $this->hasOne( AdminUser::class , 'id' , 'user_id');
    }

    

    public function country() {

        return $this->hasOne( Country::class, 'id', 'country_id' );
    }

    public function type()
    {
        return $this->hasOne( EventType::class , 'id', 'event_type_id');
    }

    public function case()
    {
        return $this->hasOne( ActionCodeCase::class , 'id','action_code_case_id');
    }

    public function caseOwned()
    {
        return $this->hasOne( ActionCodeCase::class , 'id','case_owned_id');
    }

    public function attachments()
    {
        return $this->hasMany( EventAttachment::class );
    }

    public function brand()
    {
        return $this->belongsTo( Brand::class );
    }

    // SCOPE FILTER
    public function scopeGetPerType($query, $type)
    {
        return  $query->where('event_type_id', $type);
    }

    public function scopeGetNotPerType($query, $type)
    {
        return  $query->where('event_type_id', '<>', $type);
    }

    public function scopeIsEmailFlag($query)
    {
        return  $query->where('email_flag', 'no');
    }

    public function scopeSendAvailable($query)
    {
        return  $query->where("action","published")
                        ->where( function ($query) {
                            $query->where("last_crawl", "<", Carbon::now()->subDays(3))
                            ->orWhereNull("last_crawl");
                        });
                        
    }

    public function scopeSendAvailablePublished($query)
    {
        return  $query->where("action","published")
                        ->where("event_type_id","<>","19")
                        ->where( function ($query) {
                            $query->where("last_crawl", "<", Carbon::now()->subDays(3))
                            ->orWhereNull("last_crawl");
                        });
                        
    }

    // public function scopeGetPublished($query)
    // {
    //     return  $query->where("action","published");
    // }

  

}
