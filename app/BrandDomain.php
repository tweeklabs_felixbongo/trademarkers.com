<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class BrandDomain extends Model
{

    protected $fillable = [
        'brand_id',
        'domain',
        'notes',
        'first_name',
        'last_name',
        'last_name',
        'admin_email',
        'postal_address',
        'last_crawl',
        'registrar',
        'registrant_name',
        'registrant_organization',
        'registrant_street',
        'registrant_city',
        'registrant_state',
        'registrant_postcode',
        'registrant_country_id',
        'registrant_phone',
        'registrant_fax',
        'website_usage',
        'for_sale',
        'selling_price',
        'registrant_background',
        'active_website',
        'active_description',
        'domain_ext',
        'other_emails'
    ];

    public function brand()
    {
    	return $this->belongsTo( Brand::class );
    }

    public function user()
    {
    	return $this->hasOne( AdminUser::class , 'id' , 'user_id');
    }

    public function researcher()
    {
    	return $this->hasOne( AdminUser::class , 'id' , 'researcher_id');
    }

    // public function emails()
    // {
    //     return $this->morphMany('App\DomainEventEmail', 'object');
    // }

    public function emails()
    {
        return $this->hasMany(DomainEventEmail::class);
    }

    public function scopeSendAvailable($query)
    {
        return  $query->where("last_crawl", "<", Carbon::now()->subDays(3))
                        ->orWhereNull("last_crawl");
    }

}
