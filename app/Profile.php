<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'first_name', 'middle_name', 'last_name', 'nature', 'country', 'city', 'province',
        'street', 'house', 'zip_code', 'phone_number', 'fax', 'state', 'company', 'country_origin', 'email','nationality','address'
    ];

    public function user()
    {
        return $this->belongsTo( User::class );
    }

    public function trademarks()
    {
        return $this->hasMany( Trademark::class );
    }

    public static function get_profile_by_id( $id )
    {
        $profile = Profile::where( [ 'id' => $id, 'user_id' => auth()->user()->id ] )->first();

        return $profile;
    }

    public static function save_profile( $request, $profile )
    {
        // dd($request);
        $profile->user_id     = auth()->id();
        $profile->first_name  = ucfirst( $request['first_name'] );
        $profile->middle_name = isset( $request['middle_name'] ) ? ucfirst( $request['middle_name'] ) : "";
        $profile->last_name   = ucfirst( $request['last_name'] );
        $profile->nature      = $request['nature'];
        $profile->nationality = isset( $request['nationality'] ) ? $request['nationality'] : "";
        $profile->company     = isset($request['company']) ? $request['company'] : "";
        $profile->country     = $request['country'];
        $profile->email       = $request['email'];
        $profile->city        = $request['city'];
        $profile->state       = isset($request['state']) ? $request['state'] : "";
        $profile->street      = isset($request['street']) ? $request['street'] : "";
        $profile->house       = $request['house'];
        $profile->zip_code    = isset($request['zip_code']) ? $request['zip_code'] : "";
        $profile->phone_number= $request['phone'];
        $profile->fax         = $request['fax'];
        $profile->position     = isset($request['position']) ? $request['position'] : "";

        $profile->save();

        return $profile;
    }

    public static function validation( $request )
    {
        $validatedData = $request->validate([
            'first_name'    => 'required|string|max:255',
            'middle_name'   => 'nullable|string|max:255',
            'last_name'     => 'required|string|max:255',
            'nature'        => 'required',
            'country'       => 'required',
            'email'         => 'required|string|email|max:255',
            'phone'         => 'nullable',
            'house'         => 'required|string|max:255',
            'city'          => 'required|string|max:255',
            'state'         => 'required|string|max:255',
            'fax'           => 'nullable'
        ]);

        return $validatedData;
    }
}
