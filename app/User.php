<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\VerifyEmail;
use Laravelista\Comments\Commenter;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, Commenter;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'ipaddress', 'created_at', 'has_default_password', 'token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function trademarks()
    {
        return $this->hasMany( Trademark::class );
    }

    public function orders()
    {
        return $this->hasMany( Order::class );
    }

    public function profiles()
    {
        return $this->hasMany( Profile::class );
    }

    public function carts()
    {
        return $this->hasMany( Cart::class );
    }

    public function invoices()
    {
        return $this->hasMany( Invoice::class );
    }

    public function comments()
    {
        return $this->hasMany( Comment::class );
    }

    public function activities()
    {
        return $this->hasMany( Activity::class );
    }

    public function promos()
    {
        return $this->hasMany( ActionCodePromo::class );
    }

    public function role()
    {
        return $this->belongsTo( UserRole::class , 'role_id');
    }

    public static function today_new_users()
    {
        $result = User::whereRaw('date(created_at) = curdate()')->count();

        return $result;
    }

    public static function weekly_new_users()
    {
        $result = User::where('created_at', Carbon::now()->weekOfYear)->count();

        return $result;
    }

    public static function find_email_address( $email_address )
    {
        $user = User::where('email', $email_address )->get();

        if ( count( $user->toArray() ) == 0 ) {

            return false;
        }

        return $user;
    }

    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail); // my notification
    }

    // public function getTokenAttribute()
    // {
    //     return sha1(uniqid($this->email, true));
    // }

    // public function getTstampAttribute()
    // {
    //     return $_SERVER["REQUEST_TIME"];
    // }
}
