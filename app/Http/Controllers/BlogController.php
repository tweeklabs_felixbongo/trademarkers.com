<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use App\Author;
use App\Activity;


class BlogController extends Controller
{
    
    public $metaService;

    public function __construct(
        \App\Services\MetaService $metaService) {

        $this->metaService = $metaService;
        // dd(0);
        $this->metaService->getMetaTag();
        // dd($test);
    }

    public function blogList()
    {
        $hide = true;

        $blogList = Blog::orderByRaw('created_at desc')->get();

        // foreach ( $blogList as $key ) {
            
        //     $key['thumbnail'] = Landing::get_thumbnail( $key );
        // }

        return view('blog_list', compact('blogList', 'hide'));
    }

    public function blogSlugShow($slug)
    {
        $hide = true;

        $blog = Blog::where('slug', $slug)->first();

        // GET META TAGS
        $this->metaService->getMetaTagArticleSlug($slug);

        return view('blog_details', compact('blog', 'hide'));
    }
}
