<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\ActionCode;
use App\User;
use App\Profile;
use App\Country;
use App\Cart;
use App\Activity;
use Carbon\Carbon;

class Action extends Controller
{
    public function create_account( Request $request )
    {
    	if ( count( request()->all() ) != 0 ) {

            $validatedData = $request->validate( [
                'email' => 'required|email',
                'name'  => 'required|string',
            ] );

            if ( !$request->trademark_case ) {

            	return redirect()->back();
            }

            if ( $request->email ) {

            	if ( filter_var( $request->email, FILTER_VALIDATE_EMAIL ) ) {

            		$result = User::find_email_address( $request->email );

            		session(['case_number' => $request->trademark_case]);

	            	if ( $result === false ) {

                        $trademark = ActionCode::validate_action_code( $request->trademark_case );

	            		$user = User::create( [
				            'name'        => $request->name,
				            'email'       => $request->email,
				            'ipaddress'   => \Request::ip(),
				            'password'    => bcrypt( '123456!' ),
				        ] );

                        $user->sendEmailVerificationNotification();

                        Profile::create( [
                            'user_id'     => $user->id,
                            'first_name'  => $request->name,
                            'middle_name' => "N/A",
                            'last_name'   => "N/A",
                            'nature'      => "Company",
                            'nationality' => "",
                            'company'     => $trademark->case->tm_holder,
                            'country'     => $trademark->case->country,
                            'email'       => $request->email,
                            'city'        => $trademark->case->city,
                            'state'       => $trademark->case->state,
                            'street'      => $trademark->case->address,
                            'house'       => "",
                            'zip_code'    => $trademark->case->zip
                        ] );

				        Auth::login( $user );

				        return redirect('/customer/create/password');

	            	} else {

	            		session(['case_email_address' => $request->email]);

	            		return redirect('/legal/case/' . session('case_number') );
	            	}

            	} else {

            		return redirect()->back();
            	}
            }

            return redirect('/');

        }
    }

    public function priority_add_cart( Request $request )
    {
        if ( count( $request->all() ) != 0 ) {

            if ( !$request->case_number ) {

                echo "Error case number";

                exit;
            }

            if ( !$request->country ) {

                echo "Error country";

                exit;
            }

            if ( !$request->classes ) {

                echo "Error classes";

                exit;
            }

            if ( count( $request->classes ) < 1 ) {

                echo "Class should have atleast one.";

                exit;
            }

            $trademark = ActionCode::validate_action_code( $request->case_number );

            $country = Country::find( $request->country );

            if ( is_array( $trademark ) || !$country ) {

                echo "Something went wrong, please refresh the page!";

                exit;
            }

            $price_initial = 0;
            $price_additional = 0;
            $cost   = isset( $country->prioritycost ) ? $country->prioritycost->amount : 0;
            $amount = 0;
            $count  = count( $request->classes );
            $class  = "";

            foreach ( $country->prices as $price) {
                
                if ( $price->service_type == "Registration" ) {

                    $price_initial = $price->initial_cost;
                    $price_additional = $price->additional_cost;
                }
            }

            $amount = (($count - 1) * $price_additional ) + $price_initial + $cost;

            for ( $i = 0; $i < $count ; $i++ ) { 

                $class .= $request->classes[$i];

                $class .= ( $i + 1 == $count ) ? "" : "-";
            }

            $cart = Cart::create( [
                'user_id'                => auth()->user()->id,
                'country_id'             => $country->id,
                'profile_id'             => auth()->user()->profiles[0]->id,
                'service'                => "Trademark Registration [Priority]",
                'type'                   => "Word-Only",
                'name'                   => $trademark->case->trademark,
                'logo'                   => "",
                'classes'                => $class,
                'amount'                 => $amount,
                'classes_description'    => $trademark->case->class_description
            ] );

            if ( $cart ) {

                Activity::add( 'added an item to the cart [Trademark Registration [Priority] of ' . $trademark->case->trademark, 'Case' );
                echo 200;

                exit;
            }
        }
    }

    public function legal_case_add( Request $request )
    {
        if ( $case = $request->legal_case_id  ) {

            $trademark = ActionCode::get_trademark( $case );

            if ( count( $trademark->toArray() ) > 0 ) {
                
                session( ['legal_case' => $trademark ] );

                return redirect('/registration/step2/GB');
            }
        }

        return redirect()->back();
    }

    public function verify_legal_case( $case_number )
    { 
        $trademark = ActionCode::validate_action_code( $case_number );
        $countries = Country::getParisConvetionCountries();

        Activity::add( 'searched for a case number ' . $case_number, 'Case' );

        if ( is_array( $trademark ) ) {

            return redirect('/');
        }
        
        if ( $trademark->action_code_type_id == 2  ) {

            if ( $trademark->promo ) {

                $country = Country::find( $trademark->promo->country_id );

                return redirect('/trademark-registration-in-' . strtolower( str_replace( '_', ' ', $country->name ) ));
            }

            return redirect('/');
        }

        if ( $trademark->action_code_type_id == 7 ) { 
            
            session( ['legal_case' => $trademark ] );
            // session( ['campaign_id' => $trademark ] );
            session( ['redirectUrl' => '/customer/profile/' ] );
            session( ['redirectUrlAbbr' => '/GB' ] );
            return redirect('/registration/step2/GB');
        }

        if ( $trademark->action_code_type_id == 8) {

            $header = null;
            $footer = null;

            if ($trademark && $trademark->campaign->header_content) 
            $header = $this->assignKeys($trademark->campaign->header_content,$trademark);

            if ($trademark && $trademark->campaign->footer_content) 
            $footer = $this->assignKeys($trademark->campaign->footer_content,$trademark);

            return view('case', compact( 'trademark', 'countries', 'header', 'footer' ) );
        }

        if ( $trademark->action_code_type_id == 9 ) {

            $user = Auth::user();
            // dd($user);
            if ( $trademark->actionCart && $trademark->actionCart->status == 'added') {
                return redirect('/');
            }
            if ($user && $user->id != $trademark->actionCart->user_id){
                return redirect('/');
            }
            return view('case', compact( 'trademark', 'countries' ) );
        }

        if ( $trademark->action_code_type_id == 10 ) {

            session( ['referral' => $trademark ] );
            return redirect('/register');
            // $user = Auth::user();
            // dd($user);
            // if ( $trademark->actionCart && $trademark->actionCart->status == 'added') {
            //     return redirect('/');
            // }
            // if ($user && $user->id != $trademark->actionCart->user_id){
            //     return redirect('/');
            // }
            // return view('case', compact( 'trademark', 'countries' ) );
        }
    }

    // FUNCTION TO REPLACE ALL KEY TO ACTUAL VALUES FROM HEADER AND FOOTER CONTENTS SET IN CAMPAIGN
    public function assignKeys($string,$trademark)
    {
        $cleanString = $string;

        $keys = [
            'trademark' => '__trademarkName__',
            'tm_filing_date' => '__filingDate__',
            'tm_filing_date_6' => '__filingDatePlus6__',
            'country' => '__country__'
        ];

        foreach ($keys as $key=>$value){
            // dd($value);
            if ( $key == 'tm_filing_date_6') {
                // $fillingDatePlus6 = $trademark->case->tm_filing_date;
                // dd($trademark->case->tm_filing_date->addMonths(6)->toDateTimeString());
                $date6 = $trademark->case->tm_filing_date->addMonths(6)->toDateTimeString();
                $date62 = Carbon::parse($date6)->format('F m, Y');
                $cleanString = str_replace($value,$date62,$cleanString);

            } elseif ( $key == 'tm_filing_date' ) {
                $date2 = Carbon::parse($trademark->case->{$key})->format('F m, Y');
                $cleanString = str_replace($value,$date2,$cleanString);
            } else {
                $cleanString = str_replace($value,$trademark->case->{$key},$cleanString);
            }
        }

        return $cleanString;
    }

    public function get_legal_case( $case_number )
    {
    	$trademark = ActionCode::get_trademark( $case_number );

    	if ( count( $trademark->toArray() ) > 0 ) {

    		session( ['legal_case' => $trademark ] );

            return redirect('/registration/step2/GB');
    	}

    	return redirect('/');

    }

    public function set_user_password( Request $request )
    {
    	if ( count( request()->all() ) != 0 ) {

            $validatedData = $request->validate( [
                'password' => 'required|string|min:6|confirmed',
            ] );

            $user = Auth::user();

            $user->password = bcrypt( $request->password );

            $user->save();

            if ( session('case_number') ) {
                return redirect('/legal/case/' . session('case_number') );
            } else {
                if ( $user->has_default_password == 'yes' ) {

                    $user->has_default_password = 'no';
                    $user->save();

                    $message = session('payment_message');
                    $order = session('auto_order');
                    session()->forget('auto_order');

                    // add validator to check where the user last visited
                    if ($order) {
                        return redirect()->route('after_checkout', ['order_number' => $order['order_number'], 'qty' => $order['total_items'] ] )->withErrors(array('message' => $message));
                    } else {
                        return redirect('/cart');
                    }
                } else {
                    return redirect('/customer/dashboard' );
                }
                // return redirect('/home' );
            }
            
        } else {
            $user = Auth::user();
            if ( $user->has_default_password == 'no' ) {
                return redirect('/customer/dashboard' );
            }
        }

        return view('legal.set-password');
    }

}
