<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\ActionCode;
use App\ActionCodeReferralTransaction;
use App\Repositories\Utils;

class ReferralController extends Controller
{
  
    public $rpoReferral;
    public $rpoReferralTransaction;
    public $rpoReferralInvites;
    public $referralService;
    
    public function __construct(
        \App\ActionCodeReferral $rpoReferral,
        \App\ActionCodeReferralInvite $rpoReferralInvites,
        \App\ActionCodeReferralTransaction $rpoReferralTransaction,
        \App\Services\ReferralService $referralService
    ) {
        $this->rpoReferral = $rpoReferral;
        $this->rpoReferralInvites = $rpoReferralInvites;
        $this->rpoReferralTransaction = $rpoReferralTransaction;
        $this->referralService = $referralService;
    }

    public function index(Request $request)
    {
        

        $user = Auth::user();

        if ( $user->role->slug != 'affiliate' ) {
            return redirect('/customer/dashboard');
        }
            
        $findRef = $this->referralService->getReferrer();
        
        if ($request->get('action') && !$findRef) {
            // dd('action');
            // GENERATE ACTION CODE AND STORE IT IN EXTRA DB TABLE
            $last_action_code = ActionCode::max('id');
            $last_action_code += 1;

            

            $action_code = Utils::case_gen( $last_action_code );

            $code = ActionCode::create( [
                'case_number'             => $action_code,
                'action_code_type_id'     => 10,
                'action_code_campaign_id' => 0,
            ] );
            
            

            // if ($validatedData) {

            // }

            // dd($code);
            $actionCode = $this->rpoReferral->create([
                'user_id' => $user->id,
                'action_code_id' => $code->id
            ]);

            $findRef = $this->rpoReferral->with('action')->where('user_id',$user->id)->first();

        }
        // dd($findRef->invites);
        return view('profile.referral', ['actionCode' => $findRef] );
    }

    public function withdraw(Request $request)
    {
  
        $user = Auth::user();

        if ( $user->role->slug != 'affiliate' ) {
            return redirect('/customer/dashboard');
        }
            
        $findRef = $this->referralService->getReferrer();

        if (count($request->all()) > 0){

            if ($findRef->total_points >= $request->get('req_amount')) {

                ActionCodeReferralTransaction::create( [
                    'referrals_id'      => $findRef->id,
                    'amount'            => $request->get('req_amount'),
                    'info_name'         => $request->get('name'),
                    'info_email'        => $request->get('email'),
                    'information'       => $request->get('information'),
                    'payment_method'    => $request->get('payment_method'),
                    'description'       => 'Request Payment',
                    'status'            => 'processing'
                ] );

                $findRef->total_points -= $request->get('req_amount');
                $findRef->save();

                session()->flash('requestMessage', 'Payment Request Submitted!'); 

                $this->referralService->sendMailNotificationRequest($request, $user);

            } else {
                session()->flash('requestMessage', 'Failed to make a request payment'); 
            }

            return redirect('/customer/referral');
            // return view('profile.referral', ['actionCode' => $findRef] );
            
        } 
    
        return view('profile.referral-withdraw-request-from', ['actionCode' => $findRef] );
    }

}
