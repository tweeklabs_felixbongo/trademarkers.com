<?php

namespace App\Http\Controllers;

use App\Activity;
use Illuminate\Http\Request;

class CMSController extends Controller
{
    public function regulations_canada()
    {
    	Activity::add( 'visited canada regulations ' , 'Other' );

    	return view('cms.canada');
    }

    public function regulations_canada_second()
    {
    	Activity::add( 'visited canada regulations (second layout) ' , 'Other' );

    	return view('cms.canada_second');
    }
}
