<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\Utils;
// use App\Cart;

// new class
use App\Services\CartService;

class CartController extends Controller
{

    public $cartService;
    public $rpoCart;

    // constructor
    public function __construct(
        \App\Services\CartService $cartService,
        \App\Cart $cart
    ) {
        $this->cartService = $cartService;
        $this->rpoCart = $cart;
    }

    public function getCart()
    {
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $item = null;
        if ( $id ) {
            $item = $this->rpoCart->find($id);
            // dd($item);
            if ( $item ){
                $item->updateToggle();
            }
        }

        // $action = isset($_GET['id']) ? $_GET['id'] : null;
        // if ( $action ) {
        //     switch($action) {
        //         case 'addCoupon' :
        //             $this->cartService->
        //         break;
        //     }
        // }
        // dd($id);
        $this->cartService->reCalculateItem();
        return $this->cartService->cartToHtml();
    }

    
}
