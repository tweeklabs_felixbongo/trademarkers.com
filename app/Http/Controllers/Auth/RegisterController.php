<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Profile;
use App\ActionCodeReferral;
use App\ActionCodeReferralInvite;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Torann\LaravelMetaTags\Facades\MetaTag;
use App\Repositories\MandrillMailer;
use App\Events\UserRegistered;
use App\Events\VerifyEmail;
use Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/customer/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');

        MetaTag::set('title', 'Registration');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'email'      => 'required|string|email|max:255|unique:users',
            'password'   => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create( array $data )
    {
        $user = User::create( [
            'name'        => $data['last_name'] . ',' . $data['first_name'] . ' ' . $data['middle_name'],
            'email'       => $data['email'],
            'ipaddress'   => \Request::ip(),
            'password'    => Hash::make($data['password']),
        ] );

        

        if ( session('legal_case') ) {

            $case = session('legal_case');

            Profile::create( [
                'user_id'     => $user->id,
                'first_name'  => $data['first_name'],
                'middle_name' => $data['middle_name'],
                'last_name'   => $data['last_name'],
                'email'       => $data['email'],
                'phone'       => $case->telephone_no,
                'country'     => $case->country,
                'city'        => $case->city,
                'state'       => $case->state,
                'house'       => $case->address,
                'zip_code'    => $case->zip,
                'fax'         => $case->fax_no,
            ] );

        } else {
            Profile::create( [
                'user_id'     => $user->id,
                'first_name'  => $data['first_name'],
                'middle_name' => $data['middle_name'],
                'last_name'   => $data['last_name'],
                'email'       => $data['email'],
            ] );
        }

        // event( new UserRegistered( $user ) );
        // event( new VerifyEmail( $user ) );

        // MandrillMailer::new_user_mail( $data['first_name'], $data['email'] );

        $name = explode(",",$user->name);
        $fname = '';

        if ( isset($name[1] ) ){
            $fname = $name[1];
        } else {
            $fname = $name[0];
        }



        $subject = "New Account Created - TradeMarkers.com";
        $res = Mail::send('mail.new-client-account', ['name' => $fname], function ($m) use ($user, $subject) {
            $m->from('info@trademarkers.com', 'TradeMarkers LLC');
            $m->to($user->email, $user->name)->subject($subject);
        });
        // check if from referral link
        if ( session('referral') ) {
            // dd(session('referral'));
            // get referrer
            $referral = ActionCodeReferral::where('action_code_id', session('referral')->id)->first();
            // add user to referral invites
            // dd($referral);
            ActionCodeReferralInvite::create( [
                'referrals_id' => $referral->id,
                'invited_user_id' => $user->id
            ] );

            session()->forget('referral');
        }
        

        event( new UserRegistered( $user ) );

        // MandrillMailer::new_user_mail( $data['first_name'], $data['email'] );

        return $user;
    }
}
