<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Auth\Events\Verified;
use Illuminate\Support\Facades\Auth;

class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    use VerifiesEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }

    protected function redirectTo()
    {
        // return '/customer/create/password';
        $user = Auth::user();
        // dd($user);
        if ( $user && $user->has_default_password == 'yes' ) {
            // $current = User::find($user->id);
            // $current->has_default_password = 'no';
            // $current->save();
            return '/customer/create/password';
        } else {
            return '/customer/dashboard';
        }
    }

    public function verify(Request $request)
    {
        $user = User::find($request->id);

        if ($user->has_default_password == 'yes') {
            Auth::loginUsingId($user->id);
            // return redirect($this->redirectPath());
        }

        if ($request->route('id') != $user->getKey()) {
            throw new AuthorizationException;
        }

        if ($user->markEmailAsVerified()) {
            event(new Verified($user));
        }

        
        return redirect($this->redirectPath())->with('verified', true);
    }
}
