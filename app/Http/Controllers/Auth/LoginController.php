<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use App\Cart;
use App\Country;

use Torann\LaravelMetaTags\Facades\MetaTag;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/customer/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');

        MetaTag::set('title', 'Trademarkers Login');
        MetaTag::set('description', 'Trademarkers | Login');
    }

    public function login( Request $request )
    {
        $this->validateLogin( $request );

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ( $this->hasTooManyLoginAttempts( $request ) ) {
            $this->fireLockoutEvent( $request );

            return $this->sendLockoutResponse( $request );
        }

        $user = User::where('email', $request['email'])->get();

        if ( count( $user ) > 0 ) {

            $salt = substr( $user[0]['password'], 0, 9 );

            $hash = $salt . md5( $request['password'] . $salt );

            if ( $user[0]['password'] === $hash ) {

                $user[0]['password'] = bcrypt( $request['password'] );
                $user[0]->save();
            }

        }

        if ( $this->attemptLogin( $request ) ) {
            return $this->sendLoginResponse( $request );
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts( $request );

        return $this->sendFailedLoginResponse( $request );
    }

    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        $user = User::with('role')->where('email', $request['email'])->first();
        // dd($user->role);
        if ( session('url') ) {

            $url = session('url')['intended'];
            $key = 'response=ajax';
         

            session()->forget('url');

            if ( strpos( $url, 'admin') !== false ) {
                
                return redirect('/customer/dashboard');

            } else if ( session('redirectUrl') ) {

                $redUrl = session('redirectUrl') . $user->id . session('redirectUrlAbbr');
                session()->forget('redirectUrl');
                session()->forget('redirectUrlAbbr');
                return redirect( $redUrl );

            } else if ( session('monitorLink') ) {

                $redUrl = session('monitorLink');
                session()->forget('monitorLink');
                return redirect( $redUrl );

            } else if ( strpos( $url, 'legal') !== false ) {

                return redirect('/legal/case/' . session('case_number') );

            } else if ( strpos($url, $key) !== false ) {
                return redirect( $url );
            }else {

                return redirect( $url );
            }
        } else if ( session('redirectUrl') ) {
            
            $redUrlNew = session('redirectUrl') . session('redirectUrlAbbr');
            session()->forget('redirectUrl');
            session()->forget('redirectUrlAbbr');
            return redirect( $redUrlNew );
        }
        
        return redirect($this->redirectPath());
    }

    public function showLoginForm()
    {
        $customer = null;
        if ( session('legal_case') && session('legal_case')->email ){
            $customer = User::where("email",session('legal_case')->email )->first();
        } elseif ( session('customer') ) {
            $customer = User::where("email",session('customer')->email )->first();
            // session()->forget('customer');
        }

        return view('auth.login', compact('customer'));
    }
}
