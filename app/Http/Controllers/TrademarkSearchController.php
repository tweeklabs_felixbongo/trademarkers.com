<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PHPDots\ElasticSearchTailor\Elastic;
use App\ElasticSearch;

class TrademarkSearchController extends Controller
{
    private $status;

    public function __construct()
    {
         $this->status = [
            '000' => 'UNKNOWN',
            '400' => 'IR CANCELLED; APPLICATION PENDING TRANSFORMATION',
            '401' => 'IR CANCELLED - NO TRANSFORMATION FILED',
            '402' => 'IR CANCELLED--CASE ABANDONED/CANCELLED',
            '403' => 'IR CANCELLED; REGISTRATION PENDING TRANSFORMATION',
            '600' => 'ABANDONED - INCOMPLETE RESPONSE',
            '601' => 'ABANDONED - EXPRESS',
            '602' => 'ABANDONED-FAILURE TO RESPOND OR LATE RESPONSE',
            '603' => 'ABANDONED - AFTER EX PARTE APPEAL',
            '604' => 'ABANDONED - AFTER INTER-PARTES DECISION',
            '605' => 'ABANDONED - AFTER PUBLICATION',
            '606' => 'ABANDONED - NO STATEMENT OF USE FILED',
            '607' => 'ABANDONED - DEFECTIVE STATEMENT OF USE',
            '608' => 'ABANDONED - AFTER PETITION DECISION',
            '609' => 'ABANDONED - DEFECTIVE DIVIDED APPLICATION',
            '612' => 'PETITION TO REVIVE-RECEIVED',
            '614' => 'ABANDONED PETITION TO REVIVE-DENIED',
            '616' => 'REVIVED - AWAITING FURTHER ACTION',
            '618' => 'ABANDONED FILE - BACKFILE',
            '620' => 'BACKFILE APPLICATION ADDED TO DATA BASE - STATUS NOT RECORDED',
            '622' => 'MISASSIGNED SERIAL NUMBER',
            '624' => 'REGISTERED - BACKFILE',
            '625' => 'REGISTRATION ADDED TO THE DATA BASE-STATUS UNCLEAR',
            '626' => 'REGISTERED - BACKFILE CANCELLED OR EXPIRED',
            '630' => 'NEW APPLICATION - RECORD INITIALIZED NOT ASSIGNED TO EXAMINER',
            '631' => 'NEW APPLICATION - DIVIDED - INITIAL PROCESSING',
            '632' => 'INFORMAL APPLICATION',
            '638' => 'NEW APPLICATION - ASSIGNED TO EXAMINER',
            '640' => 'NON-FINAL ACTION COUNTED - NOT MAILED',
            '641' => 'NON-FINAL ACTION - MAILED',
            '642' => 'INVENTORIED AS REJECTED',
            '643' => 'PREVIOUS ACTION/ALLOWANCE COUNT WITHDRAWN',
            '644' => 'FINAL REFUSAL COUNTED - NOT MAILED',
            '645' => 'FINAL REFUSAL - MAILED',
            '646' => 'EXAMINERS AMENDMENT COUNTED - NOT MAILED',
            '647' => 'EXAMINERS AMENDMENT - MAILED',
            '648' => 'ACTION CONTINUING FINAL COUNTED - NOT MAILED',
            '649' => 'ACTION CONTINUING FINAL - MAILED',
            '650' => 'SUSPENSION INQUIRY COUNTED - NOT MAILED',
            '651' => 'SUSPENSION INQUIRY - MAILED',
            '652' => 'SUSPENSION LETTER COUNTED - NOT MAILED',
            '653' => 'SUSPENSION LETTER - MAILED',
            '654' => 'REPORT COMPLETED SUSPENSION CHECK - CASE STILL SUSPENDED',
            '655' => 'EXAMINER\'S AMENDMENT/PRIORITY ACTION COUNTED NOT MAILED',
            '656' => 'EXAMINER\'S AMENDMENT/PRIORITY ACTION MAILED',
            '657' => 'PRIORITY ACTION COUNTED NOT MAILED',
            '658' => 'PRIORITY ACTION MAILED',
            '659' => 'SUBSEQUENT FINAL REFUSAL COUNTED NOT MAILED',
            '660' => 'SUBSEQUENT FINAL MAILED',
            '661' => 'RESPONSE AFTER NON-FINAL ACTION - ENTERED',
            '663' => 'RESPONSE AFTER FINAL REJECTION - ENTERED',
            '664' => 'INVENTORIED AS AMENDED',
            '665' => 'NOTICE OF UNRESPONSIVE AMENDMENT - COUNTED',
            '666' => 'NOTICE OF UNRESPONSIVE AMENDMENT - MAILED',
            '667' => 'REFUSAL WITHDRAWL LETTER - COUNTED',
            '668' => 'REFUSAL WITHDRAWL LETTER - MAILED',
            '672' => 'REINSTATED - AWAITING FURTHER ACTION',
            '673' => 'PETITION GRANTED- AWAITING FURTHER ACTION',
            '680' => 'APPROVED FOR PUBLICATION',
            '681' => 'PUBLICATION/ISSUE REVIEW COMPLETE',
            '682' => 'ON HOLD - ADDITIONAL PUB REVIEW REQUIRED',
            '686' => 'PUBLISHED FOR OPPOSITION',
            '688' => 'NOTICE OF ALLOWANCE - ISSUED',
            '689' => 'NOTICE OF ALLOWANCE - WITHDRAWN',
            '690' => 'NOTICE OF ALLOWANCE - CANCELLED',
            '692' => 'WITHDRAWN BEFORE PUBLICATION',
            '693' => 'WITHDRAWN FROM ISSUE-JURISDICTION RESTORED',
            '694' => 'WITHDRAWN BEFORE ISSUE',
            '700' => 'REGISTERED',
            '701' => 'SECTION 8-ACCEPTED',
            '702' => 'SECTION 8 & 15-ACCEPTED AND ACKNOWLEDGED',
            '703' => 'SECTION 15-ACKNOWLEDGED',
            '704' => 'PARTIAL SECTION 8 ACCEPTED',
            '705' => 'PARTIAL SECTION 8 & 15 ACCEPTED AND ACKNOWLEDGED',
            '710' => 'CANCELLED - SECTION 8',
            '711' => 'CANCELLED - SECTION 7',
            '712' => 'CANCELLED BY COURT ORDER (SECTION 37)',
            '713' => 'CANCELLED - SECTION 18',
            '714' => 'CANCELLED - SECTION 24',
            '715' => 'CANCELLED - RESTORED TO PENDENCY',
            '716' => 'INADVERTENTLY ISSUED REGISTRATION NUMBER CANCELLED',
            '717' => 'REGISTERED - AWAITING DIVISIONAL FEE',
            '718' => 'REQUEST FOR FIRST EXTENSION - FILED',
            '719' => 'REQUEST FOR SECOND EXTENSION - FILED',
            '720' => 'REQUEST FOR THIRD EXTENSION - FILED',
            '721' => 'REQUEST FOR FOURTH EXTENSION - FILED',
            '722' => 'REQUEST FOR FIFTH EXTENSION - FILED',
            '724' => 'EXTENSION REQUEST REFUSAL - COUNTED',
            '725' => 'EXTENSION REQUEST REFUSAL - MAILED',
            '730' => 'FIRST EXTENSION - GRANTED',
            '731' => 'SECOND EXTENSION - GRANTED',
            '732' => 'THIRD EXTENSION - GRANTED',
            '733' => 'FOURTH EXTENSION - GRANTED',
            '734' => 'FIFTH EXTENSION - GRANTED',
            '740' => 'POST REGISTRATION PAPER FILED - ASSIGNED TO PARA-LEGAL',
            '744' => 'STATEMENT OF USE - FILED',
            '745' => 'STATEMENT OF USE - INFORMAL-LETTER MAILED',
            '746' => 'STATEMENT OF USE - INFORMAL-RESPONSE ENTERED',
            '748' => 'STATEMENT OF USE - TO EXAMINER',
            '752' => 'SU - EXAMINER STATEMENT COUNTED - NOT MAILED',
            '753' => 'SU - EXAMINER STATEMENT - MAILED',
            '756' => 'EXAMINER STATEMENT COUNTED - NOT MAILED',
            '757' => 'EXAMINER STATEMENT - MAILED',
            '760' => 'EX PARTE APPEAL PENDING',
            '762' => 'EX PARTE APPEAL TERMINATED',
            '763' => 'EX PARTE APPEAL-REFUSAL AFFIRMED',
            '764' => 'EX PARTE APPEAL DISMISSED AS MOOT',
            '765' => 'CONCURRENT USE PROCEEDING TERMINATED-GRANTED',
            '766' => 'CONCURRENT USE PROCEEDING TERMINATED-DENIED',
            '771' => 'CONCURRENT USE PROCEEDING PENDING',
            '772' => 'INTERFERENCE PROCEEDING PENDING',
            '773' => 'EXTENSION OF TIME TO OPPOSE PROCESS - TERMINATED',
            '774' => 'OPPOSITION PENDING',
            '775' => 'OPPOSITION DISMISSED',
            '777' => 'OPPOSITION TERMINATED-SEE TTAB RECORDS',
            '778' => 'CANCELLATION DISMISSED',
            '779' => 'OPPOSITION SUSTAINED',
            '780' => 'CANCELLATION TERMINATED - SEE TTAB RECORDS',
            '790' => 'CANCELLATION PENDING',
            '794' => 'JURISDICTION RESTORED TO EXAMINING ATTORNEY',
            '800' => 'REGISTERED AND RENEWED',
            '801' => 'OPPOSITION PAPERS FILED',
            '802' => 'REQUEST FOR EXTENSION OF TIME TO FILE OPPOSITION',
            '803' => 'AMENDMENT AFTER PUBLICATION',
            '804' => 'APPEAL RECEIVED AT TTAB',
            '806' => 'SU - NON-FINAL ACTION COUNTED - NOT MAILED',
            '807' => 'SU - NON-FINAL ACTION - MAILED',
            '808' => 'SU - FINAL REFUSAL COUNTED - NOT MAILED',
            '809' => 'SU - FINAL REFUSAL - MAILED',
            '810' => 'SU - EXAMINER\'S AMENDMENT COUNTED - NOT MAILED',
            '811' => 'SU - EXAMINER\'S AMENDMENT - MAILED',
            '812' => 'SU - ACTION CONTINUING FINAL COUNTED - NOT MAILED',
            '813' => 'SU - ACTION CONTINUING FINAL - MAILED',
            '814' => 'SU - RESPONSE AFTER NON-FINAL ACTION - ENTERED',
            '815' => 'SU - RESPONSE AFTER FINAL REJECTION - ENTERED',
            '816' => 'SU - NOTICE OF UNRESPONSIVE AMENDMENT - COUNTED',
            '817' => 'SU - NOTICE OF UNRESPONSIVE AMENDMENT - MAILED',
            '818' => 'SU - STATEMENT OF USE ACCEPTED - APPROVED FOR REGISTRATION',
            '819' => 'SU - REGISTRATION REVIEW COMPLETE',
            '820' => 'SU-EXAMINER\'S AMENDMENT/PRIORITY ACTION COUNTED NOT MAILED',
            '821' => 'SU - EXAMINER\'S AMENDMENT/PRIORITY ACTION MAILED',
            '822' => 'SU - PRIORITY ACTION COUNTED NOT MAILED',
            '823' => 'SU - PRIORITY ACTION MAILED',
            '824' => 'SU - SUBSEQUENT FINAL REFUSAL WRITTEN',
            '825' => 'SU - SUBSEQUENT FINAL MAILED',
            '900' => 'EXPIRED',
            '969' => 'NON REGISTRATION DATA',
            '970' => 'RECORD CREATED DUE TO ASSIGNMENT REQUEST',
            '973' => 'PENDING PETITION/COURT DECISION',
        ];
    }

    public function index( Request $request )
    {
    	$elastic = new Elastic();

    	// $index   = "uspto";
    	// $type    = "case_file";
    	$results = [];
    	// $tdate   = "00-00-0000";
    	// $fdate   = "00-00-0000";
    	$term   = "";
    	// $owner   = "";
    	// $fuzzy   = 0;
        // $temp    = [];
        $page    = 1;
        // dd($request->page);
    	if ( count( $request->all() ) > 0 ) {

    		// if ( $request->tdate ) {

    		// 	$tdate = $request->tdate;
    		// }

    		// if ( $request->fdate ) {

    		// 	$tdate = $request->fdate;
    		// }

    		// if ( $request->owner ) {

    		// 	$owner = $request->owner;
    		// }

    		if ( $request->term ) {

    			$term = $request->term;
    			$fuzzy = 1;
            }
            
            if ( $request->page ) {

    			$page = $request->page;
    			// $fuzzy = 1;
            }
            
            if ( $request->term ) {
                $results = $elastic->searchAll($term , $page, 10 );

                // MANUAL SEARCH ON DB
                // $trdSearch = ElasticSearch::get()->simplePaginate(15);
                // dd($trdSearch);
            }

            
            // dd($elastic->search('euipo','milk', 1, 10, 1, 1));
    	}
        // if ($results) {
        //     foreach ( $results['records'] as $result ) {
        //         // dd($result);
        //         if ( $result['_index'] == 'euipo' ) {

        //             $number = $result['_source']['application_number'];
        //             $link = "http://tsdr.uspto.gov/#caseNumber=".$number."&caseType=SERIAL_NO&searchType=statusSearch";
        //             // $status = $result['_source']['status_code'];

        //             $result['link'] = $link;
        //             $result['serial'] = $number;

        //             // if ( isset( $this->status[$status] ) ) {

        //             //     $result['status'] = $this->status[$status];
        //             // }

        //             array_push( $temp['items'], $result);
        //         } else {
        //             $number = str_replace( '.00', '', $result['_source']['serial_number']);
        //             $link = "http://tsdr.uspto.gov/#caseNumber=".$number."&caseType=SERIAL_NO&searchType=statusSearch";
        //             $status = $result['_source']['status_code'];

        //             $result['link'] = $link;
        //             $result['serial'] = $number;

        //             if ( isset( $this->status[$status] ) ) {

        //                 $result['status'] = $this->status[$status];
        //             }

        //             array_push( $temp['items'], $result);
        //         }
                
        //     }
        //     // dd($results['meta']);
        //     $temp['meta'] = $results['meta'];
        // }

        $statusList = $this->status;

    	return view('search.index', compact( 'results', 'term', 'statusList' ));
    }
}