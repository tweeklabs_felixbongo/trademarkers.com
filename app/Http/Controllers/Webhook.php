<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Laravel\Cashier\Http\Controllers\WebhookController as CashierController;

class Webhook extends CashierController
{
    public function handleSources()
    {
        // You can find your endpoint's secret in your webhook settings
		$endpoint_secret = 'whsec_wRplNjEcAyypumpPMWozE4ukMbEh7Jk6';

		$payload = @file_get_contents('php://input');
		$sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
		$event = null;

		try {
		    $event = \Stripe\Webhook::constructEvent(
		        $payload, $sig_header, $endpoint_secret
		    );
		} catch(\UnexpectedValueException $e) {
		    // Invalid payload
		    http_response_code(400); // PHP 5.4 or greater
		    exit();
		} catch(\Stripe\Error\SignatureVerification $e) {
		    // Invalid signature
		    http_response_code(400); // PHP 5.4 or greater
		    exit();
		}

		// Do something with $event

		dd($event);

		http_response_code(200); // PHP 5.4 or greater
    }
}
