<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Torann\LaravelMetaTags\Facades\MetaTag;
use Illuminate\Support\Facades\Auth;
use App\Repositories\MandrillMailer;
use App\Repositories\MandrillMailerSend;
use App\Repositories\Utils;
use App\TrademarkClass;
use App\ClassDescription;
use App\Continent;
use App\Country;
use App\User;
use App\Profile;
use App\Payment;
use App\Order;
use App\Trademark;
use Carbon\Carbon;
use App\Invoice;
use App\Video;
use App\Activity;
use App\ActionCode;
use App\Price;
use App\ActionCodeCase;
use Dompdf\Dompdf;
use Barryvdh\PDF;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Spatie\Sitemap\Sitemap;
use Mail;



// use App\Services\MetaService;


// new class
use \DrewM\MailChimp\MailChimp;

class Trademarker extends Controller
{
    protected $pdf;
    private $mailchimp;

    public $metaService;
    public $commonService;
    public $loginUserService;
    public $cronNotifyService;
    
    public function __construct(
        \App\Services\MetaService $metaService,
        \App\Services\CommonService $commonService,
        \App\Services\LoginUserService $loginUserService,
        \App\Services\CronNotifyService $cronNotifyService
        // \App\Services\OfflineService $offlineService
    ) {
        // $this->notyService = $notyService;
        // $this->offlineService = $offlineService;
        $this->cronNotifyService = $cronNotifyService;
        
        // $this->offlineService->isOffline();


        $this->loginUserService = $loginUserService;
        $this->commonService = $commonService;


        $this->metaService = $metaService;
        // dd(0);
        $this->metaService->getMetaTag();
        $this->pdf = new Dompdf;
        $this->mailchimp = new MailChimp('3fac7d9f3b477c56ab494d1006a2a3f9-us16');

        // dd(0);


    }

    public function test( $slug )
    {
        dd( $slug );
    }

    public function merge_old_data_trademark_new()
    { 
        $users = file_get_contents( public_path() . '/json/users.json' );

        $converted_users = json_decode( $users, true );

        $profiles = file_get_contents( public_path() . '/json/profiles.json' );

        $converted_profiles = json_decode( $profiles, true );

        $orders = file_get_contents( public_path() . '/json/orders.json' );

        $converted_orders = json_decode( $orders, true );

        foreach ( $converted_users['users'] as $user) {

            foreach ( $converted_orders['orders'] as $order ) {

                if ( $user['_id']['$oid'] == $order['user_id'] ) {

                    $result = User::find_email_address( $user['email'] );

                    $str = $user['name'];

                    $s = explode(' ', $str);

                    $udate = isset( $user['signup_date'] ) ? Carbon::parse($user['signup_date']['$date'])->format('Y-m-d H:i:s') : Carbon::now()->format('Y-m-d H:i:s');

                    if ( $result === false ) {

                        $u = User::create( [
                            'email'       => $user['email'],
                            'name'        => $user['name'],
                            'password'    => $user['password_md5'],
                            'ipaddress'   => $user['signup_ip'],
                            'created_at'  => $udate,
                            'updated_at'  => Carbon::now()->format('Y-m-d H:i:s')
                        ] );

                    } else {

                        $u = $result[0];

                        $uu = User::find( $u->id );

                        if ( isset( $user['signup_date'] ) ) {

                            $uu->created_at = Carbon::parse($user['signup_date']['$date'])->format('Y-m-d H:i:s');
                        }

                        $uu->save();
                    }

                    if ( !isset($order['details']['items'][0]['profile_id']) ) {

                        $p = Profile::create( [

                            'user_id'      => $u->id,
                            'first_name'   => $s[0],
                            'last_name'    => isset( $s[1] ) ? $s[1] : "" ,
                            'nature'       => isset( $user['nature'] ) ? $user['nature'] == "C"? "Company" : "Individual" : null,
                            'company'      => isset( $user['company'] ) ? $user['company'] : null,
                            'country'      => isset( $user['country'] ) ? $user['country'] : null,
                            'city'         => isset( $user['city'] ) ? $user['city'] : null,
                            'state'        => isset( $user['state'] ) ? $user['state'] : null,
                            'street'       => isset( $user['street'] ) ? $user['street'] : null,
                            'house'        => isset( $user['address'] ) ? $user['address'] : null,
                            'zip_code'     => isset( $user['zip'] ) ? $user['zip'] : null,
                            'phone_number' => isset( $user['phone'] ) ? $user['phone'] : null,
                            'email'        => isset( $user['email'] ) ? $user['email'] : null,
                            'fax'          => isset( $user['fax'] ) ? $user['fax'] : null,

                        ] );

                    } else {

                         foreach ( $converted_profiles['profiles'] as $profile ) {

                            if ( $profile['_id']['$oid'] == $order['details']['items'][0]['profile_id'] ) {

                                $p = Profile::create( [

                                    'user_id'      => $u->id,
                                    'first_name'   => $s[0],
                                    'last_name'    => isset( $s[1] ) ? $s[1] : "" ,
                                    'nature'       => isset( $profile['nature'] ) ? $profile['nature'] == "C"? "Company" : "Individual" : null,
                                    'company'      => isset( $user['company'] ) ? $user['company'] : null,
                                    'country'      => isset( $profile['country'] ) ? $profile['country'] : null,
                                    'city'         => isset( $profile['city'] ) ? $profile['city'] : null,
                                    'state'        => isset( $profile['state'] ) ? $profile['state'] : null,
                                    'street'       => isset( $profile['street'] ) ? $profile['street'] : null,
                                    'house'        => isset( $profile['address'] ) ? $profile['address'] : null,
                                    'zip_code'     => isset( $profile['zip'] ) ? $profile['zip'] : null,
                                    'phone_number' => isset( $profile['phone'] ) ? $profile['phone'] : null,
                                    'email'        => isset( $profile['email'] ) ? $profile['email'] : null,
                                    'fax'          => isset( $profile['fax'] ) ? $profile['fax'] : null,
                                ] );

                                break;
                            }
                        }
                    }

                    $odate = isset( $order['txdate'] ) ? Carbon::parse($order['txdate']['$date'])->format('Y-m-d H:i:s') : Carbon::now()->format('Y-m-d H:i:s');

                    $pdate = isset( $order['payment']['txdate'] ) ? Carbon::parse($order['payment']['txdate']['$date'])->format('Y-m-d H:i:s') : Carbon::now()->format('Y-m-d H:i:s');

                    $o = Order::create( [

                        'order_number' => $order['ref'],
                        'total_items'  => count($order['details']['items']),
                        'total_amount' =>$order['amount'],
                        'user_id'      => $u->id,
                        'created_at'   => $odate,
                        'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
                    ] );

                    $pm = Payment::create( [
                        'user_id'      => $u->id,
                        'order_id'     => $o->id,
                        'reference_no' => $order['payment']['gw_reference'],
                        'pay_type'     => $order['payment']['pay_type'],
                        'gateway'      => $order['payment']['gateway'],
                        'card_type'    => $order['payment']['card_type'],
                        'card_ref'     => $order['payment']['card_ref'],
                        'currency'     => $order['payment']['currency'],
                        'amount'       => $order['payment']['amount'],
                        'created_at'   => $pdate,
                        'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
                    ] );

                    foreach ( $order['details']['items'] as $trademark ) {
                            
                        if ( $trademark['service'] == "registration" ) {

                            $trademark['service'] = "Trademark Registration";

                        } else if ( $trademark['service'] == "study" ) {

                            $trademark['service'] = "Trademark Study";

                        } else  {

                            $trademark['service'] = "Other Services";
                        }  

                        if ( $trademark['type'] == "w" || $trademark['type'] == "tm" ) {

                            $trademark['type'] = "Word Only";

                        } else if ( $trademark['type'] == "l" ) {

                            $trademark['type'] = "Design Only";

                        } else if ( $trademark['type'] == "tml" || $trademark['type'] == "wl" ) {

                            $trademark['type'] = "Combined Word and Design";

                        } else {

                            $trademark['type'] = "Other";
                        }

                        $class_name = "";
                        $class_description = "";
                        $summary = "";
                        $country_id = 0;

                        if ( isset( $trademark['country'] ) ) {

                            if ( $trademark['country'] == "EU" ) {

                                $trademark['country'] = "EM";
                            }

                            $country = Country::where('abbr', $trademark['country'])->get();

                            if ( count( $country->toArray() ) != 0 ) {

                                $country_id = $country[0]->id;

                            } 
                        }

                        if ( isset( $trademark['study_summary'] ) ) {

                            $summary = $trademark['study_summary'];
                        }

                        if ( isset( $trademark['notes'] ) ) {

                            $summary = $trademark['notes'];
                        }

                        if ( isset($trademark['classes']) ) {

                            foreach ( $trademark['classes'] as $class ) {
                                
                                $class_name .= $class['name'] . "-";

                                $class_description .= $class['name'] . "{" . $class['details'] . "}";
                            }
                        }
                         
                        $tm = Trademark::create( [
                            'user_id'     => $u->id,
                            'order_id'    => $o->id,
                            'profile_id'  => $p->id,
                            'country_id'  => $country_id,
                            'type'        => $trademark['type'],
                            'service'     => isset( $trademark['service'] )? $trademark['service'] : "Test",
                            'classes_description' => $class_description,
                            'classes'     => $class_name,
                            'summary'     => $summary,
                            'name'    => isset( $trademark['wordmark'] ) ? $trademark['wordmark'] : "Test",
                            'logo'        => isset( $trademark['logo_url'] ) ? $trademark['logo_url'] : "",
                            'amount'      => $trademark['amount'],
                            'created_at'  => $odate,
                            'update_at'   => Carbon::now()->format('Y-m-d H:i:s')
                        ] );

                    }

                    Invoice::create_invoice( $o, "paid", null );
                }
            }
        }
    }

    public function algorithm( Request $request )
    {
        $data = null;
        $flag = true;
        $id   = "";
        $case = "";

        if ( $request['code'] ) {

            $validatedData = $request->validate([
                'code'      => 'required|min:6',
            ]);

            if ( strlen( $request['code'] ) == 9 ) {

                // $id = Utils::resolve_codes( strtoupper( $request['code'] ), 1 );
                $id = Utils::resolve_case( strtoupper( $request['code'] ) );
            }

            if ( strlen( $request['code'] ) == 7 ) {

                $id = Utils::resolve_codes( strtoupper( $request['code'] ), 2 );
            }

            if ( strlen( $request['code'] ) == 6 ) {

                $id = Utils::resolve_codes( strtoupper( $request['code'] ), 3 );
            }

            if ( strlen( $request['code'] ) > 9 ) {

                $id = "Invalid code format!";
            }
            
        }

        if ( $request['trademark_id'] ) {

            $validatedData = $request->validate([
                'trademark_id'      => 'required|numeric',
            ]);

            $flag = false;

            $data = Utils::search_codes( $request['trademark_id'] );

            if ( $data ) {
                $data->action_code = Utils::case_gen( $request['trademark_id'] );
            }
        }

        return view('algorithm', compact( 'data', 'id', 'flag', 'case' ) );
    }

    public function class()
    {
    	$classes = TrademarkClass::all();

    	return view('classes', compact( 'classes' ) );
    }

    public function contact( Request $request )
    {
        $message = "";
        $contact_hidden = true;

        if ( count( request()->all() ) > 0 ) {

            $validatedData = request()->validate([
                'name'      => 'required|string|max:50',
                'email'     => 'required|string|email',
                'message'   => 'required|string|max:255'
            ]);

            $city = geoip( \Request::ip() );

            $information = array(
                'name'      => request()->name,
                'email'     => request()->email,
                'message'   => request()->message,
                'city'      => !isset( $city->iso_code )? 'US' : $city->iso_code,
                'phone'     => request()->phone ? request()->phone : '',
                'ipaddress' => \Request::ip()
            );

            $badWords = array(
                        "sex",
                        "porn",
                        "penis",
                        "vagina",
                        "pornography",
                        "nude",
                        "naked"
                    );

            $matches = array();
            $matchFound = preg_match_all(
                            "/\b(" . implode($badWords,"|") . ")\b/i", 
                            $information['message'], 
                            $matches
                        );

            if ($matchFound) {

                $message = "Sorry, Found offensive word(s)!";
                Activity::add( 'Send email from ' . $city->iso_code, 'Contact Form using adult words' );
            } else {
                try {
                    $subject = "Contact Us Email | " . $information['name'];
                    $res = Mail::send('mail.contact-email', ['information' => $information], function ($m) use ($subject, $information) {
                        $m->from('notifications@trademarkers.com', 'TradeMarkers LLC');
                    //     // $m->track_opens(true);
                        $m->to('info@trademarkers.com', 'TradeMarkers LLC')->subject($subject);
                    });

                    // MandrillMailer::contact_us_mail( $information );
                    Activity::add( 'Send email from ' . $city->iso_code, 'Contact Form' );
                    $message = "Your message is successfully submitted!";

                    // add email in contact list
                    // $list_id = '97b08a0041';
                    // $res = $this->mailchimp->post("lists/$list_id/members", [
                    //             'email_address' => request()->email,
                    //             'status'        => 'subscribed',
                    // ]);

                } catch ( Exception $e ) {
    
                    $message = "Something went wrong, please try again!";
                }
            }
            


            
                
        }

        return view('contact', compact('message', 'contact_hidden'));
    }

    public function quote( Request $request )
    {
        $message = "";
        $quote_hidden = true;
        $countries = Country::getParisConvetionCountries();
        $countryMadrid = Country::fetchCountryMadrid();

        $type = $request->get('getQuoteType');
        $domain = $request->get('domain');
        $mail = $request->get('mail');
        $event = $request->get('event');
        $action = $request->get('action');
        // echo $type;
        // dd(count( $request->all() ));
        
        if ( $action ) {

            // dd($type);
            $validatedData = request()->validate([
                'name'              => 'required|string|max:50',
                'email'             => 'required|string|email',
                'trademarkName'     => 'required|string|max:255',
                'trademarkType'     => 'required|string|max:255',
            ]);
                

            try {

                $send = MandrillMailerSend::sendQuoteRequest( $request );
                Activity::add( 'Send Quote Request', 'Quote' );
                $message = "Your Request is successfully submitted!";
                session()->flash('quoteMessage', 'Your Request is successfully submitted!, We will get back to you within 24 hours!');
                session()->flash('alert-class', 'alert-success'); 

                return redirect('/quote');

            } catch ( Exception $e ) {

                $message = "Something went wrong, please try again!";
            }
     
        }

        // check if it has event
        // gather needed possible data
        // $formFields;
        $formFields['email'] = '';
        $formFields['event'] = '';
        $formFields['domain'] = '';
        if ( $mail ) {
            $formFields['email'] = $mail;
        }    

        if ( $event ) {
            $formFields['event'] = $this->commonService->getEvent($event);
        }

        if ( $domain ) {
            $formFields['domain'] = $this->commonService->getDomain($domain);
        }


        return view('quote', compact('message', 'quote_hidden','countries','type', 'countryMadrid', 'formFields'));
    }




    public function class_description( Request $request )
    {
        $descriptions = [];

        if ( $id = $request['class'] ) {

            $descriptions = TrademarkClass::where('id', $id )->get();

            Activity::add( 'searched for a class name ' . $descriptions[0]->name, 'Classes' );

            $descriptions = $descriptions[0]->classDescriptions;
        }

        if ( $desc = $request['desc'] ) {

            $descriptions = ClassDescription::searchDescription( $desc );

            Activity::add( 'searched for a class description ' . $desc, 'Classes' );
        }

        return view('classes_description', compact( 'descriptions' ));
    }

    public function countries()
    {
        $continents = Continent::getContinents();

		return view('countries', compact( 'continents' ) );
    }

    public function countryCode($code)
    {
        $country = Country::where('abbr', $code)->first();

        if ($country) {
            return redirect('/trademark-registration-in-'.$country->name);
        }

		return redirect('/');
    }

    public function region ( $region )
    {
        $result = Continent::get_region( $region );

        if ( count( $result ) == 0 ) {

            return redirect('/');
        }

        Activity::add( 'visited region ' . $result[0]['name'], 'Region' );

        return view('region', compact( 'result' ) );
    }

    public function welcome()
    {
        $continents = Continent::all();

        // dd($continents[0]);
        // $meta = $this->metaService->getMetaTag('home');
        // dd($meta->title);
        // MetaTag::set('title', 'Trademarkers | Experienced Trademark Attorneys');
        // MetaTag::set('keywords',    'search trademark, 
        //                             what is a trademark, 
        //                             trademark definition, 
        //                             industrial property, 
        //                             about trademarks, 
        //                             trademark database, 
        //                             trademark search, 
        //                             international trademark search, 
        //                             international trademark, 
        //                             trademark database, 
        //                             Trademarkers,
        //                             brand protection company,
        //                             global trademark and domain name registration,
        //                             domain name registration,
        //                             global trademark,
        //                             trademark,
        //                             trademarks,
        //                             trademarker,
        //                             trademarkers,
        //                             US trademarks');
        // MetaTag::set('description', 'TRADEMARKERS are specialists in handling the registration of your trademarks worldwide. Your one stop shop to have your brand registered by experienced');

        if ( $con = request('continent') ) {

            if ( $con == 'US' ) {

                return redirect('/trademark-registration-in-united_states' );
            }

            return redirect('/region/' . $con );
        }

    	return view('welcome', compact( 'continents' ) );
    }

    public function pricing()
    {
        $countries = Country::countries_pricing();

        return view('prices', compact( 'countries' ) );
    }

    public function search_case_number( $code = null, Request $request )
    {
        $trademark = [];
        $countries = Country::getParisConvetionCountries();
        $header = null;
        $footer = null;


        if ( count( request()->all() ) <= 0 && $code == null ) {
            return redirect('/');
        }

        Activity::add( 'searched for a case number ' . $request['case_number'], 'Case' );

        if ( $code ){
            $request['case_number'] = $code;
        }

        if ( $request['case_number'] ) {

            $case_no = trim( $request['case_number'] );

            // REPLACE O TO 0
            $case_no = str_replace("o","0",$case_no);

            // INJECT FOR ROUTING ACTION CODE
            $aCode = ActionCode::where( "case_number", $case_no )->first();

            if ( $aCode && $aCode->action_code_type_id == 12 ) {
                // redirect to url
                // dd($aCode->aRoute);
                session( ['routeCode' => $aCode->aRoute ] );
                return redirect('/'.$aCode->aRoute->url);
            }

            // END INJECT

            $trademark = ActionCode::validate_action_code($case_no);

            // dd($trademark->case);

            // if (!$trademark) {
            //     $trademark = $this->searchIfEmpty($case_no);
            // }

            if (!$trademark->case) {
                return redirect('/');
            }

            // dd($trademark);
            if ($trademark && $trademark->campaign->header_content) 
            $header = $this->assignKeys($trademark->campaign->header_content,$trademark);

            if ($trademark && $trademark->campaign->footer_content) 
            $footer = $this->assignKeys($trademark->campaign->footer_content,$trademark);

            $classes = TrademarkClass::all();
            if ( $trademark && $classes) 
            $trademark->classes = $classes->pluck('name', 'id');
            
            // dd($trademark->classes[1]);
        }

        

        return view('case', compact( 'trademark', 'countries', 'header', 'footer' ) );
    }

    // public function searchIfEmpty($case_no, $find = 'o')
    // {
    //     // $find = '0';

    //     $find_ = $find == 0 ? 'o' : '0';

    //     $pos = strpos($case_no, $find);

    //     $trd = null;
    //     // dd($pos);
    //     if ( $pos ) {
    //         $findThis = str_replace($find,$find_,$case_no);
    //         $trd = ActionCode::validate_action_code($findThis);
    //         // dd($findThis);
    //         if ( $trd ) {
    //             return $trd;
    //         } else {
    //             $trd = $this->searchIfEmpty($case_no, $find_);
    //         }
    //     } else {
    //         $trd = $this->searchIfEmpty($case_no, $find_);
    //     }

    //     return $trd;

    // }

    // FUNCTION TO REPLACE ALL KEY TO ACTUAL VALUES FROM HEADER AND FOOTER CONTENTS SET IN CAMPAIGN
    public function assignKeys($string,$trademark)
    {
        $cleanString = $string;
        
        $case = ActionCodeCase::find($trademark->case->id);
        // dd($case);
        $keys = [
            'trademark' => '__trademarkName__',
            'tm_filing_date' => '__filingDate__',
            'tm_filing_date_6' => '__filingDatePlus6__',
            'country' => '__country__'
        ];

        // foreach ($keys as $key=>$value){
        //     // dd($value);
        //     if ( $key == 'tm_filing_date_6') {
        //         // $fillingDatePlus6 = $trademark->case->tm_filing_date;
        //         // dd($trademark->case->tm_filing_date->addMonths(6)->toDateTimeString());
        //         // $date6 = $case->tm_filing_date->addMonths(6)->toDateTimeString();
        //         // $date62 = Carbon::parse($case->tm_filing_date)->addMonths(6)->format('F m, Y');

        //         $date = Carbon::createFromFormat('Y-m-d H:i:s', $case->tm_filing_date, 'America/New_York');
        //         $date->setTimezone('UTC');
        //         // dd($trademark->case->tm_filing_date->format('F j, Y'));
        //         // $date = date("F j, Y", $trademark->case->tm_filing_date);
        //         $cleanString = str_replace($value,$trademark->case->tm_filing_date->addMonths(6)->format('F j, Y'),$cleanString);

        //     } elseif ( $key == 'tm_filing_date' ) {
        //         $date2 = Carbon::createFromFormat('Y-m-d H:i:s', $case->{$key}, 'America/New_York')->format('F m, Y');
                
        //         $cleanString = str_replace($value,$trademark->case->tm_filing_date->format('F j, Y'),$cleanString);
        //     } else {
        //         $cleanString = str_replace($value,$case->{$key},$cleanString);
        //     }
        // }

        foreach ($keys as $key=>$value){
            // dd($value);
            if ( $key == 'tm_filing_date_6') {
    
                $date = Carbon::createFromFormat('Y-m-d H:i:s', $case->tm_filing_date);
                $cleanString = str_replace($value,$date->addMonths(6)->format('F j, Y'),$cleanString);

            } elseif ( $key == 'tm_filing_date' ) {

                $cleanString = str_replace($value,$trademark->case->tm_filing_date->format('F j, Y'),$cleanString);
            } else {
                $cleanString = str_replace($value,$case->{$key},$cleanString);
            }
        }

        return $cleanString;
    }

    public function directCaseNumber( $caseNumber )
    {

        // dd($request);
        $trademark = [];
        $countries = Country::getParisConvetionCountries();

        $header = null;
        $footer = null;

        Activity::add( 'Direct path search, case number ' . $caseNumber, 'Case' );

        if ( $caseNumber ) {
            $case_no = trim( $caseNumber );

            $trademark = ActionCode::validate_action_code($case_no);

            if ( !$trademark ) {
                abort(404);
            }

            if ($trademark && $trademark->campaign->header_content) 
            $header = $this->assignKeys($trademark->campaign->header_content,$trademark);

            if ($trademark && $trademark->campaign->footer_content) 
            $footer = $this->assignKeys($trademark->campaign->footer_content,$trademark);
        } 
// dd(0);
        return view('case', compact( 'trademark', 'countries', 'header', 'footer' ) );
    }

    public function resources()
    {
        return view('resources');
    }

    public function what_is_trademark()
    {
        return view('what-trademark');
    }

    public function what_is_priority()
    {
        return view('what-priority');
    }

    public function why_register()
    {
        return view('why-register');
    }

    public function offer()
    {
        return view('offer');
    }

    public function about()
    {
        // MetaTag::set('title', 'About');
        // MetaTag::set('description', 'About Page');

        // MetaTag::set('title', 'About Trademarkers | Experienced Trademark Attorneys');
        // MetaTag::set('keywords',    'search trademark, 
        //                             SCT, what is a trademark, 
        //                             trademark definition, 
        //                             industrial property, 
        //                             about trademarks, 
        //                             trademark database, 
        //                             trademark search, 
        //                             international trademark search, 
        //                             international trademark, 
        //                             trademark database, 
        //                             Trademarkers,
        //                             leading international brand protection company,
        //                             global trademark and domain name registration,
        //                             domain name registration,
        //                             global trademark,
        //                             US trademarks');
        // MetaTag::set('description', 'Our clients range from individuals who wish to protect one idea in one country, to intellectual property attorneys with multiple clients, to large businesses trademarking their brands throughout entire industries globally');


        return view('about');
    }

    public function privacy()
    {
        // MetaTag::set('title', 'Privacy Policy');
        // MetaTag::set('description', 'Privacy Policy Page');

        return view('privacy');
    }

    public function cookies()
    {
        // MetaTag::set('title', 'Cookies Policy');
        // MetaTag::set('description', 'Cookies Policy Page');

        return view('cookie');
    }

    public function service_contract()
    {
        // MetaTag::set('title', 'Service Contract');
        // MetaTag::set('description', 'Service Contract Page');

        return view('service_contract');
    }

    public function terms()
    {
        // MetaTag::set('title', 'Terms and conditions');
        // MetaTag::set('description', 'Terms and conditions Page');

        return view('terms_conditions');
    }

    public function services()
    {
        // MetaTag::set('title', 'Trademarkers Services');
        // MetaTag::set('description', 'TradeMarkers provides trademark and intellectual property protection for brands used in the United Kingdom and internationally. We work with a wide range of clients, from small businesses establishing a corporate identity to large companies with a portfolio of brands.');
        // MetaTag::set('keywords', 'Trademark Study, Trademark Registration, Trademark Monitoring, ownership of a trademark');

        return view('service');
    }

    public function paypal()
    {
        return view('paypal');
    }

    public function monitoringService()
    {
        return view('monitoring_service');
    }

    public function officeService()
    {

        return redirect('/');

        $simple = Price::where('service_type','Office Status - Simple')->first();
        $complex = Price::where('service_type','Office Status - Complex')->first();
        $likelihood = Price::where('service_type','Office Status - Likelihood')->first();


        return view('office_service' ,compact( 'simple', 'complex', 'likelihood' ));
    }

    public function webhooks( Request $request )
    {
        // \Stripe\Stripe::setApiKey("sk_test_kLLO3TEoXNLWdBFKbXzmKzGi");

        // // You can find your endpoint's secret in your webhook settings
        // $endpoint_secret = 'whsec_jkljKaC6kDS1jubywgm2yvwKcacC3wtH';

        // // Do something with $event_json

        // $input = @file_get_contents('php://input');
        // $event_json = json_decode($input);

        // http_response_code(200);

        dd( $request );

        return view('webhooks', compact('event_json') );
    }

    // ABONDONED CART AUTOLOGIN
    public function loginVerify( Request $request, $code = null)
    {
        
        // modify to always login using token
        $user = User::where('token',$code)->first();

        if ( !$user->token ) {
            $user->token = $this->loginUserService->createHashEmail($user->email);
            $user->save();
        }

        if ($user) {
            Auth::loginUsingId($user->id);
        }
        return redirect('/cart');


        // if ($code){

        //     $user = User::where('token',$code)->first();
  
        //     $delta = $this->loginUserService->getDelta();


        //     if ( $user && $_SERVER["REQUEST_TIME"] - $user->tstamp < $delta ) {
        //         // auto login customer with cart active -- ABANDONED
        //         Auth::loginUsingId($user->id);
  
        //     } 
        // }
        // return redirect('/cart');
    } 

    // auto login user
    public function loginUser($pass, Request $request)
    {
// dd(0);
        if ($request->get('email')) {

            // check if match 
            $user = $this->loginUserService->getUserExist($pass,$request->get('email'));

            if ($user) {
                Auth::loginUsingId($user->id);
            }

        } 

        return redirect('/cart');
    }

    // YOUTUBE SUBSCRIBE
    public function youtubeSubscribe()
    {
        // $user = Auth::user();
        // dd(0);
        Activity::add( 'Visited for youtube subscribe' , 'PromoCode' );

        return view('youtube-subscribe');
    }

    public function popLogin(Request $request)
    {
        // $this->validateLogin( $request );

        $user = null;

        if (Auth::attempt(['email' => $request['email'], 'password' => $request['password']]))
        {
            $user = User::where('email', $request['email'])->first();
        }

        return $user;
    }

    public function webApiEventMailer()
    {
        // echo 'this';
        $this->cronNotifyService->sendEmailLayout();

        return redirect('/');
    }

    
}
