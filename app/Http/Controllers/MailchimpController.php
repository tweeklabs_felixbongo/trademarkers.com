<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use \DrewM\MailChimp\MailChimp;

// new class
use App\Services\CartService;

class MailchimpController extends Controller
{

    public $mailchimp;

    // constructor
    public function __construct() 
    {
        $this->mailchimp = new MailChimp('3fac7d9f3b477c56ab494d1006a2a3f9-us16');
    }

    public function addToList( Request $request)
    {
        return $request;
    }

    public function addToList2( $email )
    {
        $list_id = '0a6282286c';
        $res = $this->mailchimp->post("lists/$list_id/members", [
				'email_address' => $email,
                'status'        => 'subscribed'
        ]);

        // $subscriber_hash = MailChimp::subscriberHash($email);

        // $result = $MailChimp->patch("lists/$list_id/members/$subscriber_hash", [
		// 		'merge_fields' => ['FNAME'=>'Davy', 'LNAME'=>'Jones'],
		// 		'interests'    => ['2s3a384h' => true],
		// 	]);
      
        return $res;
    }

    
}
