<?php
namespace App\Http\Controllers;

// use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use Mail;

use App\Webhook;

class WebhookController extends Controller
{
  
    public $rpoEmails;
    public $rpoTracker;
    
    public function __construct(
        \App\DomainEventEmail $rpoEmails,
        \App\EmailTracker $rpoTracker
    ) {
        $this->rpoEmails = $rpoEmails;
        $this->rpoTracker = $rpoTracker;
    }


    public function delivered(Request $request)
    {
        // echo 'delivered';

        // var_dump($request->all());
        Webhook::create([
            "response" => json_encode($request->all())
        ]);
        return 'true';
    }

    public function opens(Request $request)
    {
        if ($request->get('email')){
            $email = $this->rpoEmails->find($request->get('email'));
            if ($email) {
                $email->status = 'read';
                $email->save();
            }

            if ( $request->get('event') ) {
                $track = $this->rpoTracker->where([
                    'email_id' => $email->id,
                    'event_id' => $request->get('event'),
                ])->first();
    
                if ( $track && $track->status != "read" ) {
          
                    $track->status = "read";
                    $track->save();

                    $subject = "Event email Read | " . $email->email;
                    $res = Mail::send('mail.api-response.event-read', 
                        [   
                            'email' => $email->email, 
                            'event' => $track->event
                        ], 
                        function ($m) use 
                        (
                            $subject
                        ) {
                            $m->from('notification@trademarkers.co.uk', 'TradeMarkers LLC');
                            $m->to('info@trademarkers.com', 'Trademarkers')
                            ->cc('felix@trademarkers.com')
                            ->subject($subject);
                        });
                    
                }
            }

        } 

        return redirect('/');
        
    }
}
