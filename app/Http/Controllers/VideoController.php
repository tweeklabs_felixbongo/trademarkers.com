<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;
use App\Activity;

class VideoController extends Controller
{
    public function videos( Request $request )
    {
        $path = $request->path();
        $hide = true;

        $video = Video::where('slug', $path)->get();

        if ( count( $video->toArray() ) == 0 ) {

            return redirect('/');
        }

        $video = $video[0];
        $video['embed'] = Video::parse_video( $video );

        Activity::add( 'watched the video "' . $video['title'] . '"', 'Videos' );

        $id = $video->id;

        $max = Video::max('id');
        $min = $max - 3;
        $video_append = [];
        $videolist    = [];

        if ( $id == $max ) {

            $id = 0;
        }

        $results = Video::where('id', '>', $id)->orderByRaw('id asc')->limit(3)->get();

        foreach ( $results  as $key ) {

            $key['thumbnail'] = Video::get_thumbnail( $key );

            array_push( $videolist, $key );
        }

        if ( $id == ($max - 2) ) {

            $video_append = Video::find(1);

            $video_append['thumbnail'] = Video::get_thumbnail( $video_append );

            array_push( $videolist , $video_append);
        }

        if ( $id == ($max - 1) ) {

            $video_append = Video::where('id', '>', 0)->orderByRaw('id asc')->limit(2)->get();

            foreach ( $video_append  as $key ) {

                $key['thumbnail'] = Video::get_thumbnail( $key );

                array_push( $videolist, $key );
            }
        }

        return view('videos', compact('video', 'videolist', 'hide'));
    }

    public function videosV2( $path )
    {
        // $path = $request->path();
        $hide = true;

        $video = Video::where('slug', $path)->get();

        if ( count( $video->toArray() ) == 0 ) {

            return redirect('/');
        }

        $video = $video[0];
        $video['embed'] = Video::parse_video( $video );

        Activity::add( 'watched the video "' . $video['title'] . '"', 'Videos' );

        $id = $video->id;

        $max = Video::max('id');
        $min = $max - 3;
        $video_append = [];
        $videolist    = [];

        if ( $id == $max ) {

            $id = 0;
        }

        $results = Video::where('id', '>', $id)->orderByRaw('id asc')->limit(3)->get();

        foreach ( $results  as $key ) {

            $key['thumbnail'] = Video::get_thumbnail( $key );

            array_push( $videolist, $key );
        }

        if ( $id == ($max - 2) ) {

            $video_append = Video::find(1);

            $video_append['thumbnail'] = Video::get_thumbnail( $video_append );

            array_push( $videolist , $video_append);
        }

        if ( $id == ($max - 1) ) {

            $video_append = Video::where('id', '>', 0)->orderByRaw('id asc')->limit(2)->get();

            foreach ( $video_append  as $key ) {

                $key['thumbnail'] = Video::get_thumbnail( $key );

                array_push( $videolist, $key );
            }
        }

        return view('videos', compact('video', 'videolist', 'hide'));
    }

    public function videolist()
    {
        $hide = true;

        $videolist = Video::orderByRaw('created_at desc')->get();

        foreach ( $videolist as $key ) {
            
            $key['thumbnail'] = Video::get_thumbnail( $key );
        }

        return view('video_list', compact('videolist', 'hide'));
    }
}
