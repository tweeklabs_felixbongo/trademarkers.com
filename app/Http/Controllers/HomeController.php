<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\MandrillMailer;
use App\Repositories\Utils;
use MetaTag;
use App\Trademark;
use App\Order;
use App\Profile;
use App\Country;
use App\Comment;
use App\Invoice;
use App\Attachment;
use App\Payment;
use App\Activity;
use CountryState;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        
        MetaTag::set('title', 'Customer Dashboard');
        MetaTag::set('description', 'Customer');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        $user = Auth::user();

        if ( session('url') ) {

            $url = session('url')['intended'];

            session()->forget('url');

            if ( strpos( $url, 'admin') !== false) {
                
                return redirect('/customers/area/dashboard');

            } else{

                return redirect($url);
            }
        }

        $user['custom_id'] = Utils::get_customer_number( $user['id'] );

        return view('home', compact( 'user' ) );
    }

    public function profile()
    {
        $profiles = Auth::user()->profiles;

        if ( request('q') ) {

            return redirect('/customer/profile/' . request('q') );
        }

        return view('profile.select_profile', compact( 'profiles' ) );
    }

    public function get_profile( Request $request, $id )
    {
        $prof      = Profile::get_profile_by_id( $id );
        $countries = Country::getCountries();

        if ( !$prof ) {
            return redirect()->route('profile');
        }

        if ( count( request()->all() ) > 0 ) {

            $validatedData = Profile::validation( $request );

            Profile::save_profile( request()->all(), $prof );

            session()->flash('profile', 'Successfully updated!');
        }

        return view('profile.profile', compact('prof', 'countries') );
    }

    public function create_profile( Request $request )
    {   
        $countries = Country::getCountries();

        if ( count( request()->all() ) > 0 ) {

            $validatedData = Profile::validation( $request );

            try {
                // dd(request()->all());
                $profile = Profile::save_profile( request()->all(), new Profile );

                session()->flash('profile', 'Successfully created!');

                return redirect('/customer/profile/' . $profile->id );

            } catch( Exception $e ) {

                return redirect()->route('profile');
            }
        }

        return view('profile.profile', compact( 'countries' ) );
    }

    public function create_profile_trademark( Request $request, $trademark_id )
    {   
        $countries = Country::getCountries();

        if ( count( request()->all() ) > 0 ) {

            $validatedData = Profile::validation( $request );

            try {

                $profile = Profile::save_profile( request()->all(), new Profile );

                $trademark = Trademark::find( $trademark_id );

                if ( auth()->user()->id != $trademark->user->id ) {

                    return redirect('/home');
                }

                $trademark->profile_id = $profile->id;

                $trademark->save();

                session()->flash('profile', 'Profile successfully created!');

                return redirect('/customer/trademark/' . $trademark_id );

            } catch( Exception $e ) {

                return redirect()->route('profile');
            }
        }

        return view('profile.profile', compact( 'countries' ) );
    }

    public function orders( $order_id )
    {
        $user = Auth::user();
        $trademarks = [];

        if ( $order_id != "all" ) {

            $order = Order::where( 'order_number', $order_id )->get();

            if ( count( $order ) > 0 ) {
                $trademarks = Trademark::where( 
                    [ 'order_id' => $order[0]['id'], 'user_id' => $user->id ] )->whereNotNull('office_status')->get();
            }   
        }

        $orders = $user->orders;

        return view('profile.order', compact( 'orders', 'trademarks' ) );
    }

    public function trademark( $trademark_id )
    {
        $user = Auth::user();

        $trademark = $user->trademarks;
        $comments = [];
        $attachments = [];

        if ( $trademark_id != "all" ) {

            $trademark = Trademark::get_user_trademark( $trademark_id );

            if ( $trademark === false ) {

                return redirect()->route('dashboard');
            }

            if ( !$trademark[0]->profile ) {

                return redirect('/customer/create/profile/trademark/' . $trademark_id );
            }

            $comments     = Comment::retrieve_comments( $trademark_id );
            $attachments  = Attachment::retrieve_attachments( $trademark_id );
        }

        return view('trademark', compact( 'trademark', 'comments', 'attachments' ) );
    }

    public function invoices( Request $request )
    {
        $user = Auth::user();

        if ( $request->ref && $request->id ) {

            $invoice = Invoice::find( $request->id );

            if ( $invoice ) {

                if ( $invoice->user_id == auth()->user()->id ) {

                    $payment = Payment::find( $invoice->payment->id );

                    $payment->reference_no = $request->ref;

                    $payment->save();
                }

            }
            
            exit;
        }

        return view('invoices', compact('user') );
    }

    public function download_invoice( $invoice_id )
    {
        return Invoice::download_invoice_pdf( $invoice_id, 'download' );
    }

    public function stream_invoice( $invoice_id )
    {
        return Invoice::download_invoice_pdf( $invoice_id, 'stream' );
    }

    public function add_comment( Request $request )
    {   
        $comment = [];

        if ( count( $request->all() ) > 0 ) {

            $validatedData = $request->validate( [
                'description'      => 'required',
            ] );

            if ( !isset( $request->trademark_id ) ) {

                return redirect()->back()->withErrors(array('message' => 'Comment not successfully submitted!'));
            }

            $trademark = Trademark::get_user_trademark( $request->trademark_id );

            if ( $trademark === false ) {

                return redirect()->back()->withErrors(array('message' => 'Comment not successfully submitted!'));
            }

            $comment = Comment::create_comment( $request );

            MandrillMailer::trademark_new_comment( $trademark[0], Auth::user(), false, false );

            Activity::add( 'commented on the trademark "' . $trademark[0]->name . '"' , 'Trademark' );

            return redirect('customer/trademark/' . $request->trademark_id )->withErrors(array('message' => 'Comment successfully submitted!'));
        }
    }

    public function add_attachment( Request $request )
    {   
        if ( count( $request->all() ) > 0 ) {

            $validatedData = $request->validate( [
                'attachment' => 'required|max:500000',
            ] );

            if ( !isset( $request->trademark_id ) ) {

                return redirect()->back()->withErrors(array('message' => 'Permission denied!'))
                        ->withInput();
            }

            $trademark = Trademark::get_user_trademark( $request->trademark_id );

            if ( $trademark === false ) {

                return redirect()->back()->withErrors(array('message' => 'Permission denied!'))
                        ->withInput();
            }

            $attachment = Attachment::upload_attachment( $request );

            Activity::add( 'attached a file on the trademark "' . $trademark[0]->name . '"' , 'Trademark' );

            return redirect('customer/trademark/' . $request->trademark_id )->withErrors(array('message' => 'File Successfully Uploaded'))
                        ->withInput();
        }
    }

    public function download_attachment( Request $request )
    {  
        if ( count( $request->all() ) < 0 ) {

            return redirect()->back();
        }

        $attachment_id = $request->attachment_id;
        $trademark_id  = $request->trademark_id;

        $attachment = Attachment::get_user_attachment( $attachment_id, $trademark_id );

        if ( $attachment === false ) {

            return redirect()->back();
        }

        Activity::add( 'downloaded a file ' . $attachment[0]->name , 'Trademark' );

        return Attachment::download_attachment( $attachment[0]->name );
    }

    public function trademarks()
    {
        $user = Auth::user();
        $data['registration']= 0;
        $data['study']       = 0;
        $data['monitoring']  = 0;
        $data['others']      = 0;

        $trademarks = Trademark::where( 
            [ 'user_id' => $user->id ] )->whereNotNull('office_status')->get();

        foreach ( $trademarks as $trademark ) {
            
            if ( $trademark->service == "Trademark Registration" ) {

                $data['registration']++;
            }

            else if ( $trademark->service == "Trademark Study" ) {

                $data['study']++;
            }

            else if ( $trademark->service == "Trademark Monitoring" ) {

                $data['registration']++;

            } else {
                $data['others']++;
            }
        }

        return view('profile.trademarks', compact( 'data' ) );
    }

    public function trademark_registration()
    {
        $user = Auth::user();

        $trademarks = Trademark::where( 
            ['service'  => "Trademark Registration",
             'user_id'  => $user->id] 
        )
        ->whereNotNull('office_status')
        ->get();

        return view('profile.trademark_services', compact( 'trademarks' ) );
    }

    public function trademark_study()
    {
        $user = Auth::user();

        $trademarks = Trademark::where( 
            ['service'  => "Trademark Study",
             'user_id'  => $user->id] 
        )
        ->whereNotNull('office_status')
        ->get();

        return view('profile.trademark_services', compact( 'trademarks' ) );
    }

    public function trademark_monitoring()
    {
        $user = Auth::user();

        $trademarks = Trademark::where( 
            ['service'  => "Trademark Monitoring",
             'user_id'  => $user->id] 
        )
        ->whereNotNull('office_status')
        ->get();

        return view('profile.trademark_services', compact( 'trademarks' ) );
    }

    public function trademark_others()
    {
        $user = Auth::user();

        $trademarks = Trademark::where( 
            ['service'  => "Other Services",
             'user_id'  => $user->id] 
        )
        ->whereNotNull('office_status')
        ->get();
        
        return view('profile.trademark_services', compact( 'trademarks' ) );
    }

    // Youtube Subscribe Page
    public function youtubeSubscribe()
    {
        $user = Auth::user();

        Activity::add( $user->name . ' Visited for youtube subscribe' , 'PromoCode' );

        return view('youtube-subscribe', compact( 'user' ) );
    }


    /** 
     * DIRTY SOLUTION
     * ADDED FUNCTION FOR CALLING STATES
    */
    public function getState($ccode)
    {
        $country = Country::where('name',$ccode)->first();

        if (!$country) {
            return false;
        }

        return CountryState::getStates(strtoupper($country->abbr));
    }

    public function getProfile($id)
    {
        return Profile::find($id);
    }
}
