<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SitemapController extends Controller
{
    public function sitemap()
    {

    	return redirect('/sitemaps/sitemapindex.xml');
    }

    public function sitemaps()
    {

    	header('Content-type: text/xml');

		die(readfile( dirname( app_path() ) . "/sitemaps/sitemap-index.xml" ) );
    }

    public function sitemap_file( $sitemap_name )
    {
    	try {

			header('Content-type: text/xml');

			die(readfile( dirname( app_path() ) . "/sitemaps/" . $sitemap_name));

		} catch (Exception $e) {

			try {

				header('Content-type: text/xml');

				die(readfile( dirname( app_path() ) . "/sitemaps/old/" . $sitemap_name));

			} catch (Exception $e) {

				abort(404);
			}
		}
    }
}
