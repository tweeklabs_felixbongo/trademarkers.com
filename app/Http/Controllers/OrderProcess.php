<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\MandrillMailer;
use App\Repositories\MandrillMailerSend;
use App\Repositories\Utils;
use App\Country;
use App\Trademark;
use App\Order;
use App\Payment;
use App\Profile;
use App\Invoice;
use App\Cart;
use App\ActionCodeCart;
use App\Activity;
use App\ActionCode;
use App\CouponUser;
use App\Paypal;
use App\ActionCodeReferralTransaction;
use App\TrademarkClass;
use App\ActionCodeCase;
use App\User;
use App\ActionCodePromo;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;
use Torann\LaravelMetaTags\Facades\MetaTag;
use Illuminate\Support\Facades\Hash;
use App\Events\UserRegistered;

use App\Notifications\VerifyEmail;

// new class
use App\Services\CartService;
// use App\Services\MetaService;

class OrderProcess extends Controller
{

    // use VerifyEmail;

    public $cartService;
    public $metaService;
    public $referralService;
    // public $metaService;

    // constructor
    public function __construct(
        \App\Services\CartService $cartService,
        \App\Services\MetaService $metaService,
        \App\Services\ReferralService $referralService
        // \Sunnydevbox\Recoveryhub\Repositories\User\UserRepository $userRepository
    ) {
        $this->cartService = $cartService;

        $this->metaService = $metaService;
        $this->referralService = $referralService;
        $this->metaService->getMetaTag();
    }

    public function country( $country_name )
    {
        $name = str_replace( '_', ' ', $country_name );

        $country = Country::validate_country_name( $country_name );

        if ( isset( $country['redirect'] ) ) {
            return redirect('/');
        }

        if ( session('legal_case') ) {

            session()->forget('legal_case');
        }

        Activity::add( 'visited country ' . ucfirst( $country_name ) , 'Country' );


        MetaTag::set('title', 'Trademark Registration in ' . ucfirst($country_name) );
        MetaTag::set('description', 'Registering a trademark in '.ucfirst($country_name) .' is not always a simple matter of submitting an application. Many jurisdictions do not make information readily available to the public and some do not provide reliable data online');
        MetaTag::set('keywords',    'Trademark Registration '.ucfirst($country_name) .',
                                    Registering a trademark in '.ucfirst($country_name) .',
                                    how to register a trademark in '.ucfirst($country_name) .',
                                    similarities to your trademark in' .ucfirst($country_name) . '
        ');

        return view('order_process.country', compact( 'country') );
    }

    public function study_step1( $abbr )
    {
        $country = Country::validate_country_code( $abbr );

        foreach ( $country->prices as $price ) {

            if ( $price->service_type == "Study" ) {

                if ( $price->initial_cost == "TBA" || $price->initial_cost == -1 ) {

                    return redirect()->back()->withErrors(array('message' => 'Trademark Study is not available for now, please contact our support for further information!'));
                }
            }
        }

        $country_name = $country['name'];

        Activity::add( 'started the first step of study in country ' . ucfirst($country_name) , 'Country' );

        MetaTag::set('title', 'Trademark Study in ' . ucfirst($country_name) );
        MetaTag::set('description', "Our Trademark Comprehensive Study in ".ucfirst($country_name) ." will not only list similar trademarks {graphic/phonetic} that may conflict with yours, but also give you an Attorney's opinion about registration possibilities");
        MetaTag::set('keywords',    'Trademark Study '.ucfirst($country_name) .',
                                    Study a trademark in '.ucfirst($country_name) .',
                                    how to study a trademark in '.ucfirst($country_name) .'
        ');
        
        return view('order_process.step1_study', compact( 'country' ) );
    }

    public function study_step2( Request $request, $abbr  )
    {   
        $country   = Country::validate_country_code( $abbr );
        $country_name = $country['name'];
        // set meta
        MetaTag::set('title', 'Trademark Study Form in ' . ucfirst($country_name) );
        MetaTag::set('description', "Please fill out the following form, at the bottom of the form you can find price of the Study in ".ucfirst($country_name).". Once your Study in ".ucfirst($country_name)." is ready you will be able to download it directly from our website, through your user account.");
        MetaTag::set('keywords',    'Trademark Study Form in '.ucfirst($country_name) .'
        ');

        if ( count( request()->all() ) != 0 ) {

            $validatedData = $request->validate([
                'type'      => 'required',
                'word_mark' => 'nullable',
                'logo_pic'  => 'nullable|image|mimes:jpeg,png,jpg',
                'summary'   => 'nullable',
            ]);

            $request['chosen_country'] = $country;

            if ( !$request['class'] ) {

                return redirect('registration/step2/' . $abbr)
                        ->withErrors(array('message' => 'Please select atleast one class.'))
                        ->withInput();
            }

            Trademark::calculate_class_prices( $request, true );
            Trademark::validate_trademark_info( $request, true );

            Activity::add( 'started the second step of study in country ' . $country['name'] , 'Country' );

            return redirect()->route('contact', ['abbr' => $abbr] );
        }

        // FETCH ALL CLASSES
        $trademarkClasses = TrademarkClass::all();

        return view('order_process.step2_study', compact( 'country', 'trademarkClasses' ) );
    }

    public function registration_step1( $abbr )
    {
        $country = Country::validate_country_code( $abbr );

        foreach ( $country->prices as $price ) {

            if ( $price->service_type == "Registration" ) {

                if ( $price->initial_cost == "TBA" || $price->initial_cost == -1 ) {

                    return redirect()->back()->withErrors(array('message' => 'Trademark Registration is not available for now, please contact our support for further information!'));
                }
            }
        }

        Activity::add( 'started the first step of registration in country ' . $country['name'] , 'Country' );

        $country_name = $country['name'];

        MetaTag::set('title', 'Trademark Registration in ' . ucfirst($country_name) .' Details');
        MetaTag::set('description', "Our licensed Trademark Attorneys in ".ucfirst($country_name)." will handle your trademark request, check all formalities, and then file with the ".ucfirst($country_name)." Patent and Trademark Office. You will receive confirmation of filing and a copy of the filling request.");
        MetaTag::set('keywords',    'Trademark Registration '.ucfirst($country_name) .',
                                    Register a trademark in '.ucfirst($country_name) .',
                                    '.ucfirst($country_name).' Patent and Trademark Office,
                                    how to Registration a trademark in '.ucfirst($country_name) .'
        ');

        return view('order_process.step1_registration', compact( 'country' ) );
    }

    public function registration_step2( Request $request, $abbr )
    { 
        
        $country = Country::validate_country_code( $abbr );
        $countries = Country::get();

        // dd($countries);

        $country_name = $country['name'];
        // set meta
        MetaTag::set('title', 'Trademark Registration Form in ' . ucfirst($country_name) );
        MetaTag::set('description', "In order to proceed with Trademark Registration in ".ucfirst($country_name)." please fill out the following form. You'll have to provide information about the Trademark, the Administrative contact and the Owner");
        MetaTag::set('keywords',    'Trademark registration Form in '.ucfirst($country_name) .'
        ');

        if ( count( request()->all() ) != 0 && $request->get('action') != 'setFields' ) {
            // dd($request->all());
            $validatedData = $request->validate([
                'type'      => 'required',
                'word_mark' => 'nullable',
                'logo_pic'  => 'nullable|image|mimes:jpeg,png,jpg',
                'summary'   => 'nullable',
                'commerce'  => 'sometimes|required',
                'filed'     => 'sometimes|required',
                'priority'  => 'sometimes|required',
            ]);

            $request['chosen_country'] = $country;

            if ( !$request['class'] ) {

                return redirect('registration/step2/' . $abbr)
                        ->withErrors(array('message' => 'Please select atleast one class.'))
                        ->withInput();
            }
            // dd($request->get('class'));
            // $classReq = explode(',',$request->get('class'));
            // $request->class = $classReq;
            Trademark::calculate_class_prices( $request, false );
            Trademark::validate_trademark_info( $request, false );

            Activity::add( 'started the second step of registration in country ' . $country['name'] , 'Country' );
            
            // CHECK IF CAMPAIGN HAS EMAIL AND ITS EMAIL EXIST AS CUSTOMER USER
            // dd(session('legal_case')->email);
            // if ( !Auth::check() ) {
            if ( session('legal_case') && session('legal_case')->email && !Auth::check() ) {
                // autologin if user exist

                $customer = User::where('email',session('legal_case')->email)->first();
        
                // Auth::loginUsingId($customer->id);
                return redirect()->route('order_form', ['abbr' => $abbr] );

            } 

            if ( session('legal_case') && !Auth::check() ) {
                return redirect()->route('contact', ['abbr' => $abbr] );
            }

            // CHECK CURRENT USER IF HAS ONLY 1 PROFILE PRE SELECT PROFILE
            if ( Auth::check() && count(Auth::user()->profiles) == 1 ){
                // dd(Auth::user()->profiles);
                return redirect('/customer/profile/' . Auth::user()->profiles[0]->id . '/' . $abbr);
            } 

            return redirect()->route('contact', ['abbr' => $abbr] );
            
        }

        // FETCH ALL CLASSES
        $trademarkClasses = TrademarkClass::all();

        // $case = null;
        if ( $request->get('action') == 'setFields' ){
            // session()->forget('legal_case');
            // if ( !session('legal_case') ) {
            //     echo 'redirect';
            // }

            $case = ActionCodeCase::find($request->get('case'));
            $abbrCode = $request->get('cCode') ? $request->get('cCode') : 'GB';

            

            if ($case->is_purchase == 'yes') {
                session()->forget('legal_case');
                return redirect('/registration/step2/'.$abbrCode);
            }

            if ( !$case->country_code && $case->country) {
                // get country and assign abbr
                $countrySingle = Country::where('name', $case->country)->first();

                if ( $countrySingle ) {
                    // dd($countrySingle);
                    $case->country_code = $countrySingle->abbr;
                    $case->save();
                }
            }

            // $classes = TrademarkClass::all();
            if ( $trademarkClasses ) 
            $case->classes = $trademarkClasses->pluck('name', 'id');

            $isPriority = $request->get('priority') ? true : false;

            // dd($case->campaign);
            session( ['legal_case' => $case ] );
            session( ['email_cron_priority' => $isPriority ] );
            session( ['redirectUrl' => '/customer/profile/' ] );
            session( ['redirectUrlAbbr' => '/'.$abbrCode ] );

        }

        
        // dd($classes);
        return view('order_process.step2_registration', compact( 'country', 'countries', 'trademarkClasses') );
    }


    public function serviceOrderForm( Request $request )
    { 
        // session()->forget('errors');
        $abbr = 'US';
        $country = Country::validate_country_code( $abbr );
        $countries = Country::getCountries();
        $service = ($request->get('service') ? $request->get('service') : 'Monitoring' );

        if ( $request->get('action') ) {
            $abbr = $request->get('country');

            $country = Country::validate_country_code( $request->get('country') );


            $request['chosen_country'] = $country;


            Trademark::get_service_prices( $request );
            Trademark::validate_trademark_info( $request, false );
            // dd($request->get('amount'));
            Activity::add( 'started the Monitoring in country ' . $country['name'] , 'Country' );

            // session( ['redirectUrl' => '/contact' ] );
            session( ['monitorLink' => '/trademark_contact/'. $abbr ] );
            return redirect()->route('contact', ['abbr' => $abbr] );
        }

        $countrySelect = null;
        if ( $service ) {
            $countrySelect = Trademark::get_service_countries($service);
        }

        

        // $countrySelect = Trademark::get_service_countries();


        return view('order_process.service-order-form', compact( 'country','countries', 'service', 'countrySelect' ) );
    }

    public function trademark_contact( $abbr )
    {

        $createGuest = false;

        if ( !session('trademark_order') ) {
            return redirect('/');
        }

        if ( !Auth::check() ) {
            // CREATE USER AND PROFILE FROM CASE
            // ASSIGN TEMPORARY EMAIL FOR USER TO BE CHANGE AFTER CHECKOUT
            $t=time();
            $tempEmail = $t . "@temporary.email";
            if ( session('legal_case') ) {
                $case = session('legal_case');
                
            } else { 
                $case = new ActionCodeCase;
                $case->tm_holder = $t;
                $case->email = $tempEmail;

                $createGuest = true;
            }
            // dd($case);
            
            
            $user = User::create( [
                'name'                  => $case->tm_holder,
                'email'                 => $case->email ? $case->email :  $tempEmail,
                'ipaddress'             => \Request::ip(),
                'password'              => Hash::make('temporary123'),
                'has_default_password'  => 'yes',
            ] );
            

            Profile::create( [
                'user_id'     => $user->id,
                'first_name'  => '',
                'middle_name' => '',
                'last_name'   => '',
                'email'       => $case->email,
                'phone'       => $case->telephone_no,
                'country'     => $case->country,
                'city'        => $case->city,
                'state'       => $case->state,
                'house'       => $case->address,
                'zip_code'    => $case->zip,
                'fax'         => $case->fax_no,
            ] );

            // auto login customer with default password
            Auth::loginUsingId($user->id);

            // session( ['auto_login' => true ] );
        }

        // $profiles  = Auth::user()->profiles;

        if (!Auth::check() && !session('legal_case') ) {
            session( ['redirectUrl' => '/trademark_contact' ] );
            session( ['redirectUrlAbbr' => '/'.$abbr ] );
            return redirect()->route('login');
        }

        if (Auth::user()->profiles) {
            $profiles  = Auth::user()->profiles;
        }
        

        if ( session('legal_case') ) {

            if ( count( $profiles ) == 0 ) {

                return redirect('/customer/create/profile/' . $abbr);
            }

            $profile = $profiles[0];

            return redirect('/customer/profile/' . $profile->id . '/' . $abbr);
        }

        if ( $createGuest ) {
            $profile = $profiles[0];

            return redirect('/customer/profile/' . $profile->id . '/' . $abbr);
        }


        $countries = Country::getCountries();
        $country   = Country::validate_country_code( $abbr );

        if ( request('q') ) {

            return redirect('/customer/profile/' . request('q') . '/' . $abbr);
        }


        
        return view('order_process.trademark_contact_form', compact( 'country', 'countries', 'profiles' ) );
    }

    public function validate_contact( Request $request, $id, $abbr )
    {
        $prof      = Profile::get_profile_by_id( $id );
        $country   = Country::validate_country_code( $abbr );
        $countries = Country::getCountries();

        if ( !session('trademark_order') ) {
            return redirect('/');
        }

        if ( !$prof ) {
            return redirect()->route('contact', ['abbr' => $abbr]);
        }

        if ( count( request()->all() ) > 0 ) {

            // $validatedData = Profile::validation( $request );

            $prof = Profile::save_profile( $request->all(), $prof );

            $currentUser = Auth::user();
            $userEmailBreak = explode("@", $currentUser->email);
            
            if ( $userEmailBreak[1] == "temporary.email" ) {
                $user = User::find($currentUser->id);

                $existEmail = User::where('email', $request->get('email'))->first();
                // dd($user);
                // dd($existEmail);
                if ($existEmail) { 
                    Auth::logout();
                    session( ['customer' => $existEmail ] );
                    return redirect()->route('order_form', ['abbr' => $abbr] );
                } else {
                    // dd($user);
                    if ( $request->get('email') ) {
                        $user->email = $this->getUserEmail($request->get('email'),1);
                        $user->name = $request->get('last_name') . ', ' . $request->get('first_name');
                        $user->save();

                        // TRIGGER EMAIL NEW CUSTOMER AND EMAIL VARIFICATION
                        // MandrillMailerSend::sendVerificationEmail($user);
                        $user->notify(new VerifyEmail($user));
                    }
                    

                    
                }
                // $email

                

                // event( new UserRegistered( $user ) );
            }

            Trademark::validate_trademark_contact_info( $prof->toArray() );

            return redirect()->route('order_form', ['abbr' => $abbr] );
        }

        return view('order_process.trademark_info', compact( 'prof', 'country', 'countries' ));
    }

    // recursive function to fetch unique email
    public function getUserEmail($email,$count)
    {
        $existEmail = User::where('email', $email )->first();

        if ($existEmail) {

            $userEmailBreak = explode("@", $existEmail->email);
            return $this->getUserEmail($userEmailBreak[0] . $count . '@' . $userEmailBreak[1] , ++$count);

        } else {
            return $email;
        }
    }

    public function create_contact( Request $request, $abbr )
    {
        $country   = Country::validate_country_code( $abbr );
        $countries = Country::getCountries();

        if ( count( request()->all() ) > 0 ) {

            $validatedData = Profile::validation( $request );

            $profile = Profile::save_profile( $request->all(), new Profile );

            Trademark::validate_trademark_contact_info( $profile->toArray() );

            Activity::add( 'created new profile', 'Country' );

            return redirect()->route('order_form', ['abbr' => $abbr] );
        }

        return view('order_process.trademark_info', compact( 'country', 'countries' ));
    }

    public function order_form( Request $request, $abbr )
    { 
        
        $country = Country::validate_country_code( $abbr );
        // dd(session('legal_case')->action_code_campaign_id);
        // dd(session('legal_case'));
        if ( !session('trademark_order') ) {
            return redirect('/');
        }

        if ( !Auth::check() ) {

            session('order_url', [ 'url' => '/order_form/' . $abbr ]);
            session()->flash('status', true);

        }
        
        if ( count( request()->all() ) != 0 ) {
            // dd(0);
            if ( request('back') ) {
                
                return redirect()->back();
            }
            
            $data = session('trademark_order');
            
            if ( auth()->user()->role_id == 3 ) {
                // PARTNER USER
                // dd($data);
                $data['total_discount'] = $data['amount'] * 0.05;
            } else {

                // NORMAL USER 
                if ( session('legal_case') !== null && session('legal_case')->action_code_campaign_id ) {

                    $data['campaign_id'] = session('legal_case')->action_code_campaign_id;
    
                    $data['case_id'] = session('legal_case')->id;

                    if ( session('legal_case') ) {
                        session()->forget('legal_case');
                        session()->forget('email_cron_priority');
                        
                    }

                    // GET DISCOUNT IF IT HAS CAMPAIGN WITH CAMPAIGN DISCOUNT
                    $campaignPromo = $this->cartService->getcampaignDiscount($data['campaign_id']);
        
                    if ( $this->cartService->validateCoupon($campaignPromo) ) {

                        $data['total_discount'] = $data['amount'] * ($campaignPromo->discount / 100);
                    }
                }
            }

      
            
            Cart::add_cart( $data );

            Activity::add( 'added an item to the cart [' . $data['service'] . '] for ' . $data['name'], 'Order' );
            return redirect()->route('cart');
        }

        Activity::add( 'proceeded to the order form in country ' . $country['name'] , 'Order' );
        
        return view('order_process.order_form', compact( 'country' ) );
    }

    public function cart( Request $request)
    { 
        $user = Auth::user();
        $response = isset($_GET['response']) ? $_GET['response'] : '';

        $coupon = isset($_GET['coupon']) ? $_GET['coupon'] : '';
        $action = isset($_GET['action']) ? $_GET['action'] : '';

        if (isset($_COOKIE['step2']) && $_COOKIE['step2'] == 'true' && $user->youtube_subscribe == 'no' && $user->youtube_promo_claim == 'no'){
            // generate promocode
            $ytCode = $this->generateCode();
            // $user->youtube_subscribe = 'yes';
            // $user->subscribe_promo = $ytCode;
            // $user->save();
        }

        $data = [];

        if ( $id = request('id') ) {
            $this->cartService->removeItemCoupon($id);
            $data = Cart::delete_item_cart( $id );
            $this->cartService->reCalculateItem();
        }

        // cleanup cart items
        $this->cartService->cleanUpItems();
        // fetch cart items 
        $contents = Cart::get_user_cart_items();


        // dd($contents);
        $total    = Cart::get_cart_total();

        Activity::add( 'visited his cart ', 'Cart' );
        // echo $action;
        $couponMessage=null;

        if ( $action ) { 
            switch($action) {
                    case 'addCouponUser' :
                        $coupon = $user->subscribe_promo;
                        $user->youtube_promo_claim = 'yes';
                        $user->save();
                    case 'addCoupon' :
                    // ADDED COUPON ON EACH CART ITEM
                    if ( $coupon ) : 

                        // CHECK IF THERE ARE EXISTING COUPONS
                        if ($this->cartService->hasCoupon()) {
                            $couponMessage = [
                                'type' => 'info',
                                'message' => 'Sorry, you can only apply 1 coupon at a time'
                            ];
                            break;
                        }
                        
                        // USED FOR DISPLAYING A BUTTON FROM ROUTING DISCOUNT
                        if (isset($_GET['routeCoupon'])) {
                            session()->forget('routeCode');
                        }

                        // REPLACE O TO 0
                        $coupon = str_replace("o","0",$coupon);

                        $rpoCoupon = $this->cartService->getCoupon($coupon);
                        if ( $this->cartService->validateCoupon($rpoCoupon) ) :
                            
                            $hasNotApplicable = false;
                            $hasApplicable = false;

                            foreach($contents as $item){

                                if ( count($rpoCoupon->country_id) == 1 || in_array($item->country_id, $rpoCoupon->country_id) ) {

                                    if ( !in_array($item->service, $rpoCoupon->promo_type) ) {
                                        $hasNotApplicable = true;
                                        
                                    } else { 
                                        $hasApplicable = true;
                                        $this->cartService->applyCoupon($rpoCoupon, $item->id);
                                    }
                                    
                                } else {
                                    $hasNotApplicable = true;
                                }
                                
                            }
                            // dd($hasApplicable);
                            if ( $hasNotApplicable && !$hasApplicable ) {
                                $couponMessage = [
                                    'type' => 'info',
                                    'message' => 'Cart Item not applicable for this PROMOCODE!'
                                ];
                            } elseif( $hasNotApplicable && $hasApplicable ) {
                                $couponMessage = [
                                    'type' => 'info',
                                    'message' => 'Coupon successfully added, But some item is not applicable for this PROMOCODE'
                                ];
                            } else {
                                $couponMessage = [
                                    'type' => 'success',
                                    'message' => 'Coupon successfully added!'
                                ];
                            }
                            

                            // add entry user / coupon
                            CouponUser::create( [
                                'user_id'      => auth()->user()->id,
                                'coupon_id'    => $rpoCoupon->id,
                            ] );
                        else :
                            Activity::add( auth()->user()->name.' tried to add coupon code('.$coupon.')', 'Cart' );
                            $this->cartService->sendAdminNotification($coupon, $rpoCoupon);
                            $couponMessage = [
                                'type' => 'danger',
                                'message' => 'Sorry, No coupon found!'
                            ];
                        endif;
                        
                        Activity::add( 'Added coupon code('.$coupon.')', 'Cart' );

                        
                    endif;

                break;

                case 'removeCouponUser' :
                    $user->youtube_promo_claim = 'no';
                    $user->save();

                // break;
                case 'removeCoupon' :
                    $delCoupon='';
                   
                    $itemId = isset($_GET['itemId']) ? $_GET['itemId'] : '';
                    $this->cartService->removeCoupon($itemId);

                        $couponMessage = [
                            'type' => 'success',
                            'message' => 'Coupon successfully removed!'
                        ];
                        Activity::add( 'remove coupon code('.$coupon.')', 'Cart' );
        
                break;

                case 'addCart' :
                    $itemId = isset($_GET['itemId']) ? $_GET['itemId'] : '';
                    $item = Cart::find($itemId);
                    if ( $item ) {
                        $item->updateToggle();
                        $actionCart = ActionCodeCart::where('cart_id', $item->id)->first();
                        $actionCart->updateToggle();
                        $this->cartService->reCalculateItem();
                    }
                    return redirect('cart');
                break;

                case 'addSignupDiscount' :

                    $user = auth()->user();
                    
                    $referral = $this->cartService->applyReferralInvite('yes');

                    // remove other discount
                    foreach ( $contents as $content ) {

                        if ( $content->country->discount ) {

                            foreach ($content->coupons as $coupon) {
                                $this->cartService->removeCoupon($coupon->promo_code_id);
                            }

                            // add discount 10%
                            $content->total_discount =  $content->amount * ($content->country->discount->customer_discount / 100);
                            $content->save();
                        }
                    }



                break;

                case 'cancelSignupDiscount' :

                    $user = auth()->user();
                    $referral = $this->cartService->applyReferralInvite('no');

                    $this->cartService->reCalculateItem();

                break;

                default :
                    return redirect('cart');
                break;

            }
        }
        // $dataCoupon = $this->cartService->computedAmount($contents);
        
        if ($response == 'ajax') {

            if($request->ajax()){
                return $this->cartService->cartToHtml($couponMessage);
            }
            // return $this->cartService->cartToHtml($couponMessage);
            return redirect('cart');
        }

        return view('cart', compact( 'user','contents', 'total', 'data', 'couponMessage' ) );
    }

    

    public function addCoupon(Request $request)
    {
        $coupon = $_GET['coupon'];
        $data = [];
        $contents     = Cart::get_user_cart_items();
        $total    = Cart::get_cart_total();
        foreach($contents as $item){
            // dd($item);
            if ( isset($coupon) ) {

            }
            $item->discount = $coupon;
            $item->save();
        }


        // dd($total);
        if ( $id = request('id') ) {

            $data = Cart::delete_item_cart( $id );
        }

        $contents = Cart::get_user_cart_items();
        $total    = Cart::get_cart_total();

        Activity::add( 'visited his cart ', 'Cart' );

        return view('cart', compact( 'contents', 'total', 'data' ) );
    }

    public function checkout1( Request $request ) 
    {
        $order_number = Utils::get_order_number( Order::max('id') + 1 );
        $contents     = Cart::get_user_cart_items();
        $countries    = Country::getCountries();

        if ( count( $contents ) == 0 ) {

            return redirect('/');
        }

        $total        = Cart::get_cart_total();
        $error        = "";
        $message      = "";
        $success      = false;
        $invoice_status = "pending";
        $error = "Oops! Something went wrong, please try again.";

        if ( count( $request->all() ) > 0 ) {
            
            if ( $request->card_checkout ) {

                Payment::create( [
                    'user_id'      => auth()->user()->id,
                    'order_id'     => "9999999",
                    'reference_no' => "Test",
                    'pay_type'     => "Card",
                    'gateway'      => 'Stripe',
                    'card_type'    => json_encode( $request->all() ),
                    'card_ref'     => "",
                    'currency'     => "usd",
                    'amount'       => $total
                ] );
            }
        }

        Activity::add( 'failed to pay due to incorrect details', 'Payment' );

        return view('checkout', compact( 'error', 'contents', 'total', 'countries' ) );
    }

    public function checkout( Request $request )
    {
        $order_number = Utils::get_order_number( Order::max('id') + 1 );

        $profiles = Profile::where('user_id', auth()->user()->id)->get();

        // cleanup cart items
        $this->cartService->cleanUpItems();

        $contents     = Cart::getUserCartActiveItems();
        $countries    = Country::getCountries();

        // Get referral
        $referral = $this->referralService->getReferralInvite();

        $geo = geoip( \Request::ip() );
        // dd($city);
        $paypalId = '';

        if ( config('app.env') == 'staging' ) {
            $paypalId = config('app.paypal_client_id_dev');
        } else {
            $paypalId = config('app.paypal_client_id_live');
        }

        $dataCoupon = $this->cartService->computedAmount($contents);

        if ( count( $contents ) == 0 ) {

            return redirect('/');
        }

        $total               = Cart::get_cart_total();
        $totalDiscount       = Cart::get_cart_totalDiscount();
        // dd($totalDiscount);
        $total -= $totalDiscount;

        $error        = "";
        $message      = "";
        $success      = false;
        $invoice_status = "pending";
        $payment_method = "invoice";

        //  total deducted with discount
        if ($dataCoupon) {
            $total -= $dataCoupon['totalDiscount'];
        }
        // dd($request->paypal);
        Stripe::setApiKey( 'sk_live_22xwrVdLHpDFZzJ0goISCycg003nAJraSf' );

        Activity::add( 'proceeded to checkout ', 'Checkout' );

        if ( count( $request->all() ) > 0 ) {
            
            try {

                if ( $request->wechat ) {
                    $payment_method = "WeChat";
                    try {

                        $charge = \Stripe\Charge::create([
                            "amount" => $total * 100,
                            "currency" => "usd",
                            "source" => $request->wechat,
                        ]);

                        if ( $charge->paid ) {

                            $success = true;

                            $order = Order::create( [
                                'order_number' => $order_number,
                                'total_items'  => count( $contents ),
                                'total_amount' => $total,
                                'user_id'      => auth()->user()->id

                            ] );

                            Payment::create( [
                                'user_id'      => auth()->user()->id,
                                'order_id'     => $order->id,
                                'reference_no' => $charge->id,
                                'pay_type'     => "Wechat Pay",
                                'gateway'      => 'Stripe',
                                'card_type'    => "",
                                'card_ref'     => "",
                                'currency'     => "usd",
                                'amount'       => $total

                            ] );

                            $invoice_status = "paid";
                            
                            $message = "Payment successfull!";
                        }

                    } catch ( \Exception $ex ) {
                        $error =  $ex->getMessage();
                    }

                }

                else if ( $request->method ) {

                    if ( $request->method == "alipay" ) {
                        $payment_method = "alipay";
                        try {

                            $charge = \Stripe\Charge::create([
                                "amount" => $total * 100,
                                "currency" => "usd",
                                "source" => $request->source,
                            ]);

                            if ( $charge->paid ) {

                                $success = true;

                                $order = Order::create( [
                                    'order_number' => $order_number,
                                    'total_items'  => count( $contents ),
                                    'total_amount' => $total,
                                    'user_id'      => auth()->user()->id

                                ] );

                                Payment::create( [
                                    'user_id'      => auth()->user()->id,
                                    'order_id'     => $order->id,
                                    'reference_no' => $charge->id,
                                    'pay_type'     => "Alipay",
                                    'gateway'      => 'Stripe',
                                    'card_type'    => "",
                                    'card_ref'     => "",
                                    'currency'     => "usd",
                                    'amount'       => $total

                                ] );

                                $invoice_status = "paid";

                                $message = "Payment successfull!";
                            }

                        } catch ( \Exception $ex ) {
                            $error =  $ex->getMessage();
                        }
                    }
                }

                else if ( $request->stripeToken ) {
                    $payment_method = "Stripe";
                    $customer = Customer::create( array(
                        'email'   => auth()->user()->email,
                        'source'  => $request->stripeToken
                    ) );

                    $charge = Charge::create(array(
                        'customer'    => $customer->id,
                        'amount'      => $total * 100,
                        'currency'    => 'usd',
                        'description' => 'Trademarkers_' . $order_number
                    ) );

                    if ( $charge->paid ) {

                        $success = true;

                        $order = Order::create( [
                            'order_number' => $order_number,
                            'total_items'  => count( $contents ),
                            'total_amount' => $total,
                            'user_id'      => auth()->user()->id

                        ] );

                        Payment::create( [
                            'user_id'      => auth()->user()->id,
                            'order_id'     => $order->id,
                            'reference_no' => $charge->source->id,
                            'pay_type'     => $charge->source->object,
                            'gateway'      => 'Stripe',
                            'card_type'    => $charge->source->brand,
                            'card_ref'     => $charge->source->last4,
                            'currency'     => $charge->currency,
                            'amount'       => $total

                        ] );

                        $invoice_status = "paid";

                        Activity::add( 'successfully paid via '. $charge->source->brand . ' ' . $charge->source->object .' to the order number ' . $order_number, 'Payment' );

                        $message = "Payment successfull!";
                    }
                    
                } else if ( $request->paypal ) {
                    
                    $payment_method = "paypal";

                    // $customer = Customer::create( array(
                    //     'email'   => auth()->user()->email,
                    //     'source'  => "paypal"
                    // ) );

                    $order = Order::create( [
                        'order_number' => $order_number,
                        'total_items'  => count( $contents ),
                        'total_amount' => $total,
                        'user_id'      => auth()->user()->id

                    ] );

                    $paypal = Paypal::create(array(
                        'user_id'    => auth()->user()->id,
                        'paypal_order_id'      => $request->orderID,
                        'paypal_payer_id'      => $request->payerID,
                        'order_id'    => $order->id,
                        'amount'    => $request->amount,
                        'message_status' => 'Paypal Ref. No ' . $request->orderID
                    ) );

                    // if ( $charge->paid ) {

                    $success = true;

                    

                    Payment::create( [
                        'user_id'      => auth()->user()->id,
                        'order_id'     => $order->id,
                        'reference_no' => $request->orderID,
                        'pay_type'     => 'paypal',
                        'gateway'      => 'paypal',
                        'card_type'    => '',
                        'card_ref'     => '',
                        'currency'     => 'usd',
                        'amount'       => $total

                    ] );

                    $invoice_status = "paid";

                    Activity::add( 'successfully paid via paypal to the order number ' . $order_number, 'Payment' );

                    $message = "Payment successfull!";
                    // }
                    
                } else if ( $request->invoice ) {
                    $payment_method = "Invoice";
                    $validatedData = $request->validate([
                        'name'         => 'required',
                        'address'      => 'required',
                        'state'        => 'required',
                        'city'         => 'required',
                        'postal_code'  => 'required'
                    ]);
                        // dd($request->all());
                    if ( $request->invoice ) {

                        $order = Order::create( [
                            'order_number' => $order_number,
                            'total_items'  => count( $contents ),
                            'total_amount' => $total,
                            'user_id'      => auth()->user()->id

                        ] );

                        Payment::create( [
                            'user_id'      => auth()->user()->id,
                            'order_id'     => $order->id,
                            'reference_no' => "",
                            'pay_type'     => 'Bill Payment',
                            'gateway'      => 'TradeMarkers',
                            'card_type'    => "",
                            'card_ref'     => "",
                            'currency'     => "usd",
                            'amount'       => $total
                        ] );

                        $invoice_status = "pending";

                        $message = "Please check your invoice details.";

                        $success = true;

                        Activity::add( 'wanted to pay via invoice to the order number ' . $order_number, 'Payment' );

                    } else {

                        $error = "Something went wrong, please try again!";
                    }
                    
                }

                if ( $success ) {

                    foreach ( $contents as $content ) {

                        Trademark::store( $content, $order['id'], $invoice_status );
                    }

                    if ( $order->trademarks[0]->profile ) {

                        $data = null;

                        if ( $request->payment == "invoice" ) {

                            $data = array(
                                'name'        => $request->name,
                                'address'     => $request->address,
                                'state'       => $request->state,
                                'postal_code' => $request->postal_code,
                                'city'        => $request->city,
                                'country'     => $request->country
                            );
                        }

                        $invoice = Invoice::create_invoice( $order, $invoice_status, $data );
                    }

                    // dd($order);
                    $this->cartService->updateCartCoupons($order, $contents);

                    // update case if its linked to cart item
                    $this->cartService->updateCaseCampaign($contents);

                    // update profile used
                    // if ( $request->profileId ) {
                    //     $profile = Profile::find($request->profileId);

                    //     if ( $profile ) {
                    //         $profile->house = $request->address;
                    //         $profile->city = $request->city;
                    //         $profile->state = $request->state;
                    //         $profile->zip_code = $request->postal_code;
                    //         $profile->save();

                    //         // update cart item to which profile used upon checkout
                    //         foreach($contents as $cartItem) {
                    //             $cartItem->profile_id = $request->profileId;
                    //             $cartItem->save();
                    //         }
                    //     }
                    // }
                    

                    // MandrillMailer::new_order_mail( $order, $contents->toArray(), true, $invoice_status );
                    // MandrillMailer::new_order_mail( $order, $contents->toArray(), false, $invoice_status, $payment_method );
                    
                    $this->cartService->sendNewOrder($order, $contents);

                    session( ['payment_message' => $message ] );

                    if ( Auth::user()->has_default_password == 'yes' ) {
                        session( ['auto_order' => $order ] );
                        $user = User::find(Auth::user()->id);

                        if ( $user ) {
                            $user->sendEmailVerificationNotification();
                        }
                        // User::sendEmailVerificationNotification();

                        event( new UserRegistered( $user ) );
                        // return redirect('/customer/create/password');
                    }
                    Cart::updateCartItemComplete($order->id);

                    return redirect()->route('after_checkout', ['order_number' => $order['order_number'], 'qty' => $order['total_items'] ] )->withErrors(array('message' => $message));

                } else {

                    $error = "Something went wrong, please try again!";

                    Activity::add( 'failed to pay due to incorrect details', 'Payment' );
                }

            } catch(\Exception $ex) {
                // dd($request->all());
                $this->cartService->sendAdminNotificationPaymentError($request, $ex);
                $error =  $ex->getMessage();

                Activity::add( 'failed to pay due to ' . $error, 'Payment' );
            }
        }
        // $referral = $this->referralService->getReferral();


        return view('checkout', compact( 'error', 'contents', 'total', 'countries', 'dataCoupon', 'paypalId', 'profiles', 'geo', 'referral' ) );
    }

    public function after_checkout( $order_number )
    {
        $order    = Order::where('order_number', $order_number )->first();
        $contents = $order->items;
        // dd($order->items);
        $total    = $contents->sum('amount');
        $invoice  = $order->invoice;

        $dataCoupon = $this->cartService->computedAmount($contents);

        if ( !$order ) {

            return redirect('/');
        }

        // update user if using discount from subscribe
        $user = Auth::user();
        if ($user->youtube_promo_claim == 'no' && $user->youtube_subscribe == 'yes') {
            foreach ($contents as $content ) {
                if ( isset($content->coupons[0]) ){
                    if ($content->coupons[0]->code->action_code->case_number == $user->subscribe_promo) {
                        $usr = User::find($user->id);
                        $usr->youtube_promo_claim = 'yes';
                        $usr->save();
                    }
                }
            }
        }

        // comment clear to be replace with status on cart items
        // Cart::clear_cart();
        // Cart::updateCartItemComplete();

        // add points to referrer if any
        $invite = $this->referralService->getReferralInvite();
        // if ( $invite && $invite->apply == 'yes' && isset($invoice[0]) && $invoice[0]->status == 'paid' ) {
        if ( $invite && $invite->apply == 'yes' ) {

            $referral = $this->referralService->getReferral();
        
            if ( $referral ) {
                
                $discountTotal = 0;
                foreach($contents as $content){
                    if ($content->country->discount)
                    $discountTotal += $content->amount * ($content->country->discount->referrer_discount / 100);
                }

                // update and add point to referrer
                $invite->order_id = $invoice->order_id;
                $invite->save();

                $referral->total_points += $discountTotal;
                $referral->save();

                $user = Auth::user();

                ActionCodeReferralTransaction::create( [
                    'referrals_id'      => $referral->id,
                    'ref_invite_id'      => $invite->id,
                    'amount'            => $discountTotal,
                    'description'       => 'Received Referral Points ' . $discountTotal . ' From '. $user->name
                ] );

            }

        }
        

        // Cart::updateCartItemComplete($order->id);
        

        return view('after_checkout', compact('order','contents','total','invoice', 'dataCoupon') );
    }

    public function sendCartMail($cartId)
    {

        $cart = Cart::find($cartId);

        if ( $cart->email_status_sent == 'yes' ) {
            return [
                'type' => 'error',
                'message' => 'Email Already Sent'
            ];
        } else {

            
            // $cart->save();

            $res = MandrillMailer::send_cart_mail( $cart );

            if ( $res[0]['status'] == 'sent' ) {
                $cart->email_status_sent = 'yes';
                $cart->save();
                // return $res;
            }

        }
        // dd($cart);

        return [
            'type' => 'success',
            'message' => 'Email Successfully Sent'
        ];
    }

    public function showCartMail($cartId)
    {

        $cart = Cart::find($cartId);

        $cart->email_status_show = 'yes';
        $cart->save();

        return $cart;
    }

    public function generateCode()
    {

        $user;

        if ( !Auth::check() ) {
            return 'Invalid';
        } else {
            $user = Auth::user();

            Activity::add( $user->name . ' Subscribe to youtube, generating promocode' , 'PromoCode' );

            if ($user->youtube_subscribe == 'yes') {
                return 'Duplicate';
            }
        }

        $last_action_code = ActionCode::max('id');
        $last_action_code += 1;

        $action_code = Utils::case_gen( $last_action_code );

        $code = ActionCode::create( [
            'case_number'             => $action_code,
            'action_code_type_id'     => 2,
            'action_code_campaign_id' => 0
        ] );

        $type = ["Trademark Study,Trademark Registration,Trademark Registration [Priority],Trademark Monitoring,Other Services"];

        $promo = ActionCodePromo::create( [
            'action_code_id'    => $code->id,
            'user_id'           => $user->id,
            'discount'          => rand(3,10),
            'limit'             => '1',
            'promo_type'        => $type
        ] );

        $usr = User::find($user->id);

        if ($usr) {
            $usr->youtube_subscribe = 'yes';
            $usr->subscribe_promo = $action_code;
            $usr->save();
        }

        return $usr;

    }
}
