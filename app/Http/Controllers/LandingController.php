<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Landing;
use App\Activity;

class LandingController extends Controller
{
    public function landings( Request $request )
    {
        $path = $request->path();
        $hide = true;

        $video = Landing::where('slug', $path)->get();

        if ( count( $video->toArray() ) == 0 ) {

            return redirect('/');
        }

        $video = $video[0];
        $video['embed'] = Landing::parse_video( $video );

        Activity::add( 'watched the video "' . $video['title'] . '"', 'Videos' );

        $id = $video->id;

        $max = Landing::max('id');
        $min = $max - 3;
        $video_append = [];
        $videolist    = [];

        if ( $id == $max ) {

            $id = 0;
        }

        $results = Landing::where('id', '>', $id)->orderByRaw('id asc')->limit(3)->get();

        foreach ( $results  as $key ) {

            $key['thumbnail'] = Landing::get_thumbnail( $key );

            array_push( $videolist, $key );
        }

        if ( $id == ($max - 2) ) {

            $video_append = Landing::find(1);

            $video_append['thumbnail'] = Landing::get_thumbnail( $video_append );

            array_push( $videolist , $video_append);
        }

        if ( $id == ($max - 1) ) {

            $video_append = Landing::where('id', '>', 0)->orderByRaw('id asc')->limit(2)->get();

            foreach ( $video_append  as $key ) {

                $key['thumbnail'] = Landing::get_thumbnail( $key );

                array_push( $videolist, $key );
            }
        }

        return view('videos', compact('video', 'videolist', 'hide'));
    }

    public function landinglist()
    {
        $hide = true;

        $landinglist = Landing::orderByRaw('created_at desc')->get();

        foreach ( $landinglist as $key ) {
            
            $key['thumbnail'] = Landing::get_thumbnail( $key );
        }

        return view('landing_list', compact('landinglist', 'hide'));
    }
}
