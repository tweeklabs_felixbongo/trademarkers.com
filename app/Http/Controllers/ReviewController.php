<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\ActionCode;
use App\Repositories\Utils;
use App\Http\Controllers\Session;

class ReviewController extends Controller
{
  
    public $rpoReview;
    public $reviewService;
    public $metaService;
    
    public function __construct(
        \App\Services\MetaService $metaService,
        \App\Review $rpoReview,
        \App\Services\ReviewService $reviewService
    ) {
        $this->rpoReview = $rpoReview;
        $this->reviewService = $reviewService;

        $this->metaService = $metaService;

        $meta = $this->metaService->getMetaTag();
        
    }

    public function index(Request $request)
    {
        // dd($request);
        // $actionCode = null;

        $user = Auth::user();
        
        $reviews = $this->rpoReview->get()->sortByDesc('created_at');

        if ( count(request()->all()) > 0 ) {
// dd(request()->all());
            $validatedData = $request->validate([
                'user_id'      => 'required',
                'rating'      => 'required',
                'comment'      => 'required'
            ]);

            $review = $this->rpoReview->addReview($request->all());

            if ( $review ) {
                session()->flash('reviewMessage', 'Thank You for rating us!');
            }

            return redirect('/customer-reviews');

        }
        
        // dd($user);
        return view('review', ['reviews' => $reviews, 'user' => $user] );
    }

    

}
