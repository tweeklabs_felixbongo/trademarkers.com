<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ActionRoute extends Model
{

    protected $fillable = [
        'action_code_id',
        'url',
        'related_action_id'
    ];

    public function action()
    {
        return $this->belongsTo( ActionCode::class, 'action_code_id' );
    }

}
