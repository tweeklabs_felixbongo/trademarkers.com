<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cases extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'action_code', 'order_code', 'customer_code', 'hash'
    ];

    public static function check_duplicate_action( $case_number )
    {
    	$case = Cases::where( 'action_code', 'like', '%' . $case_number . '%'  )->get();

    	if ( $case ) {

    		return true;
    	}

    	return false;
    }

    public static function check_duplicate_order( $order_number )
    {
    	$case = Cases::where( 'order_code', 'like', '%' . $order_number . '%'  )->get();

    	if ( $case ) {

    		return true;
    	}

    	return false;
    }

    public static function check_duplicate_customer( $customer_number )
    {
    	$case = Cases::where( 'customer_code', 'like', '%' . $customer_number . '%'  )->get();

    	if ( $case ) {

    		return true;
    	}

    	return false;
    }
}
