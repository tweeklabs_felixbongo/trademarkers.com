<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cohensive\Embed\Facades\Embed;

use App\ActionCodeReferralInvite;
use App\ActionCodeReferralTransaction;

class ActionCodeReferral extends Model
{
    protected $fillable = [
        'user_id',
        'action_code_id',
        'total_points'
        ];

    public function action()
    {
        return $this->belongsTo( ActionCode::class, 'action_code_id' );
    }

    public function invites()
    {
        return $this->hasMany( ActionCodeReferralInvite::class, 'referrals_id');
    }

    public function transactions()
    {
        return $this->hasMany( ActionCodeReferralTransaction::class, 'referrals_id');
    }

    public function user()
    {
        return $this->hasOne( User::class, 'id','user_id');
    }
    
}
