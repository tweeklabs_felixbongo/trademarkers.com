<?php

use Illuminate\Routing\Router;


Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');

    $router->resource('manage/customers', UserController::class);

    $router->resource('manage/classes', ClassController::class);

    $router->resource('manage/description', DescriptionController::class);

    $router->resource('manage/countries', CountryController::class);

    $router->resource('manage/orders', OrderController::class);

    $router->resource('manage/test', AdminTrademarkController::class);

    $router->resource('manage/trademarks', TrademarkController::class);

    $router->resource('manage/prices', PriceController::class);

    $router->resource('manage/promos', PromoController::class);

    $router->resource('manage/feedbacks', FeedbackController::class);

    $router->resource('manage/redirects', RedirectsController::class);

    $router->resource('manage/attachments', AttachmentController::class);

    $router->resource('manage/videos', VideoController::class);

    $router->resource('manage/cart', CartController::class);

    $router->resource('manage/invoices', InvoiceController::class);

    $router->resource('manage/regions', ContinentController::class);

    $router->resource('manage/action_codes', ActionCodeController::class);

    $router->resource('manage/profiles', ProfileController::class);

    $router->resource('manage/payments', PaymentController::class);

    $router->resource('manage/banners', BannerController::class);

    $router->resource('manage/activities', ActivityController::class);

    $router->resource('manage/priority-cost', PriorityCostController::class);

    $router->resource('manage/metatags', MetaTagController::class);

    $router->resource('manage/campaigns', CampaignController::class);

    $router->resource('manage/cases', CaseController::class);

    $router->resource('manage/referral-request', ReferralTransactionController::class);

    $router->resource('manage/store-hours', StoreHourController::class);


    // BRAND MODULE ROUTINGS 
    $router->resource('manage/brands', BrandController::class);

    $router->resource('manage/brand-domains', BrandDomainController::class);

    $router->resource('manage/events', EventController::class);

    $router->resource('manage/brand-leads', BrandLeadController::class);
    
    $router->resource('manage/email-tracking', EmailTrackerController::class);

    // $router->resource('manage/')

    // GLOBAL SETTING OF EVENT TYPES
    $router->resource('manage/event-types', EventTypeController::class);

    // THIS NEW ROUTE IS FOR LANDING PAGES
    $router->resource('manage/landings', LandingController::class);

    $router->resource('manage/blogs', BlogController::class);

    $router->resource('manage/country-discounts', CountryDiscountsController::class);

    $router->resource('manage/action-routes', ActionRouteController::class);

    // API
    $router->get('api/class', 'DescriptionController@getClass' );

    $router->get('api/countries', 'CartController@getClass' );

    $router->get('api/cases', 'PromoController@getClass' );

    $router->get('api/user', 'CartController@getUser' );

    $router->get('api/user/name', 'CartController@getUserName' );

    $router->get('api/profile', 'CartController@getProfile' );

    $router->get('api/countries2', 'ProfileController@getClass' );

    $router->get('api/attorneys', 'TrademarkController@getAttorneys' );

    $router->get('api/paralegals', 'TrademarkController@getParalegals' );

    $router->get('api/trademark', 'TrademarkController@getClass' );

    $router->get('api/leads', 'EventController@getLeads' );

    $router->get('api/domains', 'EventController@getDomain' );

    $router->get('api/brands', 'BrandController@getBrands' );

    $router->get('api/resend-invoice/{id}', 'OrderController@resendInvoice' );

    // $router->get('api/action-terminate', 'ActionCodeController@terminalO' );


    // action upload csv
    Route::get('action', ActionController::class.'@index');


    Route::post('action/upload', ActionController::class.'@upload');

    // DOWNLOAD INVOICE PDF
    Route::get('manage/invoices/pdf/{id}', 'InvoiceController@download_invoice');
    Route::get('manage/invoices/view-pdf/{id}', 'InvoiceController@view_invoice');

});
