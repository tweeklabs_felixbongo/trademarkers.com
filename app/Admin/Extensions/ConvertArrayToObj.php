<?php
namespace App\Admin\Extensions;


use Maatwebsite\Excel\Concerns\FromArray;

class ConvertArrayToObj implements FromArray
{
    protected $obj;

    public function __construct(array $obj)
    {
        $this->obj = $obj;
    }

    public function array(): array
    {
        return $this->obj;
    }
}