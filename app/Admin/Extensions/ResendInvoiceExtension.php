<?php

namespace App\Admin\Extensions;

use Encore\Admin\Admin;

class ResendInvoiceExtension
{
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    protected function script()
    {
        // echo 'this';
        return "$('.send-email-customer').on('click', function () {
                    if (confirm('Are you sure you want to send this email to client?')) {
                       

                        $.ajax({
                            method: 'get',
                            url: '/admin/api/resend-invoice/' + $(this).data('id'),
                            
                            success: function (data) {
                                $.pjax.reload('#pjax-container');
                                if (data.type === 'success'){
                                    toastr.success(data.message);
                                } else {
                                    toastr.error(data.message);
                                }
                            }
                        });
                    }
                });
                ";
    }

    protected function render()
    {
        Admin::script($this->script());

        return "<a class='send-email-customer' href='#' data-id='{$this->id}'><i class='fa fa-envelope'></i></a>";
    }

    public function __toString()
    {
        return $this->render();
    }

    public function updateAndSend($cartId)
    {   
        
    }
}