<?php

namespace App\Admin\Controllers;

use App\Country;
use App\Brand;
use App\BrandDomain;
use App\DomainEventEmail;
use App\Event;
use App\EventType;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Illuminate\Support\Facades\DB;
use Encore\Admin\Form;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Carbon\Carbon;
use Encore\Admin\Grid\Column;
use Illuminate\Support\MessageBag;
use App\Repositories\MandrillMailer;
use Encore\Admin\Auth\Database\Role;
use Encore\Admin\Auth\Database\Administrator;
Use Encore\Admin\Widgets\Table;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Brand);

        $grid->model()->orderBy('created_at', 'desc');
        $grid->brand('Brand');
        $grid->purpose('Purpose');
        $grid->category('Category');
        // $grid->assigned_user_id('Assign to me')->editable('select', [Admin::user()->id => 'Self',null => 'Available']);
        // if ( Admin::user()->inRoles(['event-researcher']) ) {
        //     $grid->model()->where('assigned_user_id', Admin::user()->id )->orWhereNull('assigned_user_id');
        // }
        // Filter depends on user
        // if ( Admin::user()->inRoles(['event-researcher']) ) {  
            // $grid->assigned_user_id('Assign to me')->editable('select', [Admin::user()->id => 'Self',null => 'Available']);
            // $grid->disableCreation();
        // } else {
        //     $grid->assigned_user_id('Assign to')->replace([null => '-']);
        // }
        // $grid->disableCreation();

        $grid->user_id('Added/Updated')->display(function() {
            if ($this->user && $this->user->name) {

                return $this->user->name == 'Administrator' ? 'System Generated' : $this->user->name;
            } 

            return 'System Generated';
            
        });

        // COMMENT SINCE ENABLE TO EDIT ALL 
        // if ( Admin::user()->inRoles(['event-researcher']) ) { 
        //     $grid->column('Edit')->display(function(){
        //         if ( Admin::user()->id == $this->assigned_user_id ) {
        //             return "<a href='/admin/manage/brands/".$this->id."/edit'><i class='fa fa-pencil'></i></a>";
        //         }
        //     });
        // }
        
        // $grid->actions(function ($actions) {
        //     if ( Admin::user()->inRoles(['event-researcher']) ) {  
        //         $actions->disableDelete();
        //         // $actions->disableEdit();
        //     }
        // });

        $grid->filter(function($filter){

            // Remove the default id filter
            $filter->disableIdFilter();

            $filter->like('brand', 'Brand');
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Brand::findOrFail($id));

        $show->brand('Brand');
        $show->notes('Notes');

        return $show;
    }

    
    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Brand);

        
        
        $form->tab('Brand Information', function( $form ) {

            $form->hidden('user_id')->default(Admin::user()->id);

            if ( Admin::user()->inRoles(['event-researcher']) ) {
                $form->hidden('assigned_user_id')->default(Admin::user()->id);
            } else {
                $form->hidden('assigned_user_id')->default(null);
            }
            
            
            $form->text('brand', 'Brand')
                ->creationRules(['required', "unique:brands"])
                ->updateRules(['required', "unique:brands,brand,{{id}}"]);

            $form->text('purpose','Purpose');
            $form->text('category','Category');
            $form->text('value','Value');

            $form->textarea('notes', 'Notes');

            $form->hasMany('domains', 'Brand Domains', function (Form\NestedForm $domain) {
                
                $countries = Country::get();
                $countriesOptions = $countries->pluck('name', 'id');

                $researcherUsers = Administrator::whereHas('roles',  function ($query) {
                    $query->where('slug', ['event-researcher']);
                })->get();
                $researcherOptions = $researcherUsers->pluck('name', 'id');
    
                $domain->select('researcher_id', 'Researcher')->options($researcherOptions);

                $domain->divider("Domain Information");
                $domain->text('registrar', 'Registrar');
                $domain->text('domain', 'Domain');
                $domain->text('domain_ext', 'Domain Extension');
                
                $states = [
                    'on'  => ['value' => 'yes', 'text' => 'For Sale', 'color' => 'success'],
                    'off' => ['value' => 'no', 'text' => 'Not for Sale', 'color' => 'danger'],
                ];
                $domain->switch('for_sale', 'is the domain for sale?')->states($states)->default('no');
                $domain->currency('selling_price');

                $states2 = [
                    'on'  => ['value' => 'yes', 'text' => 'Yes', 'color' => 'success'],
                    'off' => ['value' => 'no', 'text' => 'No', 'color' => 'danger'],
                ];
                $domain->switch('active_website', 'Is the domain name associated with an active website?')->states($states2)->default('no');
                $domain->textarea('active_description');

                $domain->textarea('website_usage', 'Website Usage');


                $domain->text('first_name', 'Registrant First Name');
                $domain->text('last_name', 'Registrant Last Name');
                $domain->text('registrant_organization', 'Registrant Organization');
                $domain->text('registrant_street', 'Registrant Street');
                $domain->text('registrant_city', 'Registrant City');
                $domain->text('registrant_state', 'Registrant State');
                $domain->text('registrant_postcode', 'Registrant Postal Code');
                $domain->text('registrant_phone', 'Registrant Phone');
                $domain->text('registrant_fax', 'Registrant Fax');
                $domain->text('admin_email', 'Registrant/OwnerEmail');
                $domain->select('registrant_country_id', 'Country')->options($countriesOptions);
                // $domain->text('date_registered', 'Date Registered');
                // $domain->text('postal_address', 'Postal Address');
                
                $domain->textarea('registrant_background', 'Registrant Background');


                $domain->textarea('notes', 'Notes');

                $domain->textarea('other_emails', 'Other Emails')->help("<i>Please enter other email addresses here.<br>Comma separated ex. email1@email.com, email2@email.com</i>");
                
                
                // $domain->hasMany('domains.emails', 'Domain email', function (Form\NestedForm $email) {
                //     $email->email('email', 'Email');
                //     $email->text('notes', 'Notes');
    
                // })->mode('table');
                

            });

        });
        
        // ============================================ 
        // Tools    ===================================
        // ============================================ 
        // $form->tools(function (Form\Tools $tools) {
        //     $tools->disableDelete();
        //     $tools->disableView();
        //     // $tools->disableList();
        // });


        return $form;
    }

    // public function getBrands(Request $request)
    // {
    //     // dd($request->attributes);
    //     $q = $request->get('q');

    //     return Brand::where('brand', 'like', "%$q%")->paginate(null, ['id', 'brand'] );
    // }

    public function getBrands( Request $request ) {
        $q = $request->get('q');

        return Brand::where('brand', 'like', "%$q%")->limit(10)->paginate(null, ['id', 'brand as text']);
    }

}
