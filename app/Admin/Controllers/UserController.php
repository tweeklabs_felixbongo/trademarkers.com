<?php

namespace App\Admin\Controllers;

use App\User;
use App\UserRole;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Encore\Admin\Facades\Admin;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Mail;

class UserController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new User);

        $grid->model()->orderBy('id', 'desc');

        $grid->id('Id');
        $grid->name('Name');
        $grid->email('Email');
        $grid->column('role_id','Customer Type')->display( function($roleId) {
            if ($roleId) {
                $role = UserRole::find($roleId);

                if ($role) {
                    return $role->name;
                }
            }
            // $countries = UserRole::find($roleId);
        });
        // $grid->created_at('Created at');
        // $grid->updated_at('Updated at');
        // $grid->column('email_verified_at', 'Date Verified')->display( function( $email_verified_at ) {
        //     $date = Carbon::parse( $email_verified_at );

        //     return $date->format('M d, Y');
        // } );

        $grid->column('Auto Login URL')->display( function(){

            $strPass = str_replace('/','-',$this->password);

            $link = 'https://trademarkers.com/user-login/'.urlencode($strPass).'?email='.$this->email;

            return '<p style="word-break: break-all;">'.$link.'</p>';
        })->width(400);

        $grid->actions(function ($actions) {

            $actions->disableDelete();

            if ( Admin::user()->inRoles(['csr']) ) {
                $actions->disableEdit();
                $actions->disableView();
            }
        });

        $grid->filter(function($filter){

            // Remove the default id filter
            $filter->disableIdFilter();

            $filter->like('Name', 'Name');
            $filter->like('Email', 'Email Address');
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(User::findOrFail($id));

        $show->id('Id');
        $show->name('Name');
        $show->email('Email');
        $show->ipaddress('IP Address');
        // $show->created_at('Created at');
        // $show->updated_at('Updated at');

        // dd($this->id);
        // $show->column('Auto Login Link')->display( function(){
        //     return 'test';
        // });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new User);

        $form->hidden('id');
        $form->text('name', 'Name');
        $form->email('email', 'Email');

        $roles = UserRole::get();
        $roleOptions = $roles->pluck('name', 'id');
        $form->select('role_id', 'Customer Type')->options($roleOptions);

        $form->text('password', 'Password');
        // $form->datetime('email_verified_at', 'Date Verified');
        $form->hidden('ipaddress', 'IP');


        $form->saving( function ( Form $form ) {

            if ($form->id) {

                $user = User::find( $form->id );
         
                if ( $form->password ) {

                    $form->password = bcrypt( $form->password );

                } else {

                    if ( $user ) {

                        $form->password = $user->password;
                    }
                }

                

                $role_id = $form->role_id;
                $new_role = UserRole::find($form->role_id);

                if ( $user->role->id != $form->role_id && $new_role ) {


                    
                    $subject = "Customer status change From " . $user->role->name . " To " .$new_role->name;
                    $res = Mail::send('mail.customer-type-change', ['user' => $user, 'role' => $new_role->name], function ($m) use ($user, $subject) {
                        $m->from('info@trademarkers.net', 'TradeMarkers LLC');
                        $m->to($user->email, $user->name)->subject($subject);
                    });
    
                }

                

            } else {

            }
            $form->ipaddress = \Request::ip();
        } );

        return $form;
    }

 
}
