<?php

namespace App\Admin\Controllers;

use App\CountryDiscount;
use App\Country;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class CountryDiscountsController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new CountryDiscount);

        $grid->column('country.name', 'Country')->sortable();
        $grid->referrer_discount('Affiliate/Referrer Discount %')->sortable();
        $grid->customer_discount('Refered Customer Discount %')->sortable();
        $grid->total_discount('Partner Discount %')->sortable();

        $grid->filter(function($filter){

            // Remove the default id filter
            $filter->disableIdFilter();
            $filter->equal('country_id','Country')->select()->ajax('/admin/api/countries');

        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(CountryDiscount::findOrFail($id));

        $show->country_id('Country')->as(function($countryId){
            $country = Country::find($countryId);
            // dd($country);
            return $country->name ? $country->name : 'N/A';
        });

        $show->referrer_discount('Affiliate/Referrer Discount %');
        $show->customer_discount('Refered Customer Discount %');
        $show->total_discount('Partner Discount %');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new CountryDiscount);

        $countries = Country::get();
        $countriesOptions = $countries->pluck('name', 'id');

        $form->select('country_id', 'Country')->options($countriesOptions);

        $form->number('referrer_discount', 'Affiliate/Referrer Discount %');
        $form->number('customer_discount', 'Refered Customer Discount %');
        $form->number('total_discount', 'Partner Discount %');

        return $form;
    }
}
