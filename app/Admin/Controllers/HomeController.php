<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\Dashboard;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use App\User;
use App\Order;
use App\Activity;
use App\Payment;

class HomeController extends Controller
{
    public function index(Content $content)
    {
        return $content
            ->header('Dashboard')
            ->description('Statistics')
            ->row(function (Row $row) {

                $row->column(3, function (Column $column) {

                    $result = User::today_new_users();

                    $column->row("<div class=\"info-box\"><span class=\"info-box-icon bg-aqua\"><i class=\"fa fa-users\"></i></span><div class=\"info-box-content\"><span class=\"info-box-text\"> Daily New User</span><span class=\"info-box-number\">$result</span></div></div>");
                });

                $row->column(3, function (Column $column) {

                    $result = Order::today_orders();

                    $column->row("<div class=\"info-box\"><span class=\"info-box-icon bg-red\"><i class=\"fa fa-archive\"></i></span><div class=\"info-box-content\"><span class=\"info-box-text\"> Daily Order</span><span class=\"info-box-number\">$result</span></div></div>");
                });

                $row->column(3, function (Column $column) {
                    
                    $result = Activity::today_user_activity();

                    $column->row("<div class=\"info-box\"><span class=\"info-box-icon bg-yellow\"><i class=\"fa fa-list-alt\"></i></span><div class=\"info-box-content\"><span class=\"info-box-text\"> Daily Activity</span><span class=\"info-box-number\">$result</span></div></div>");
                });

                $row->column(3, function (Column $column) {
                    
                    $result = Payment::today_total_payment();

                    $column->row("<div class=\"info-box\"><span class=\"info-box-icon bg-green\"><i class=\"fa fa-money\"></i></span><div class=\"info-box-content\"><span class=\"info-box-text\"> Daily Total Order</span><span class=\"info-box-number\">$result</span></div></div>");
                });
            });
    }
}
