<?php

namespace App\Admin\Controllers;

use App\PriorityCost;
use App\Country;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class PriorityCostController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new PriorityCost);

        $grid->id('Id');
        $grid->column('country_id', 'Country')->display( function () {
           
            if ( !$this->country ) {

                return "N/A";
            }

           $avatar = $this->country->avatar;

            return "<img src='" . asset('/images/'.$avatar) . "'>" . ' ' . $this->country->name;
        });
        $grid->amount('Amount');
        $grid->created_at('Created at');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(PriorityCost::findOrFail($id));

        $show->id('Id');
        $show->country_id('Country id');
        $show->amount('Amount');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new PriorityCost);

        $form->select('country_id','Country Name')->options( function ( $country_id ) {

            $country = Country::find($country_id);

            if ($country) {
                return [$country->id => $country->name];
            }
        })->ajax('/admin/api/countries');

        $form->decimal('amount', 'Amount');

        return $form;
    }
}
