<?php

namespace App\Admin\Controllers;

use App\ActionCode;
use App\ActionCodeCampaign;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ActionCodeController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ActionCode);

        $campaigns = ActionCodeCampaign::all();
        // dd($campaign);
        $grid->model()->orderBy('created_at', 'desc');
        $grid->filter(function($filter){

            $campaigns = ActionCodeCampaign::all();
            $arr = null;
            foreach ($campaigns as $campaign) {
                $arr[$campaign->id] = $campaign->name;
                // array_push();
            }
            // dd($arr);
            // Remove the default id filter
            $filter->disableIdFilter();

            // Add a column filter
            $filter->like('case_number', 'case_number');
            $filter->equal('action_code_type_id','Action Types')->radio([
                ''   => 'All',
                8    => 'unidentified identifier (8)',
                9    => 'Add to Cart Action',
                7    => 'Initial USPTO New Generated Case',
            ]);
            $filter->equal('action_code_campaign_id','Campaign')->radio($arr);
        
        });

        $grid->id('Id');
        $grid->case_number('Case number');
        // ADDED 2 COLUMNS
        $grid->action_code_type_id('Code Status');
        $grid->action_code_campaign_id('Code Campaign Status');
        $grid->column('case.trademark', 'Trademark Name');


        // $grid->euipo_id('Euipo id');
        // $grid->trademark('Trademark');
        // $grid->tm_filing_date('Tm filing date');
        // $grid->tm_filing_no('Tm filing no');
        // $grid->tm_holder('Tm holder');
        // $grid->application_reference('Application reference');
        // $grid->mark_current_status_code('Mark current status code');
        // $grid->mark_current_status_date('Mark current status date');
        // $grid->ref_page_no('Ref page no');
        // $grid->ref_processed_date('Ref processed date');
        // $grid->registration_date('Registration date');
        // $grid->address('Address');
        // $grid->city('City');
        // $grid->city_of_registration('City of registration');
        // $grid->state('State');
        // $grid->class_number('Class number');
        // $grid->class_description('Class description');
        // $grid->representatives('Representatives');
        // $grid->email('Email');
        // $grid->fax_no('Fax no');
        // $grid->telephone_no('Telephone no');
        // $grid->website('Website');
        // $grid->zip('Zip');
        // $grid->country('Country');
        // $grid->created_at('Created at');
        // $grid->updated_at('Updated at');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ActionCode::findOrFail($id));

        $show->id('Id');
        $show->case_number('Case number');
        // ADDED 2 COLUMNS
        $show->action_code_type_id('Code Status');
        $show->action_code_campaign_id('Code Campaign Status');
        // $show->euipo_id('Euipo id');
        // $show->trademark('Trademark');
        // $show->tm_filing_date('Tm filing date');
        // $show->tm_filing_no('Tm filing no');
        // $show->tm_holder('Tm holder');
        // $show->application_reference('Application reference');
        // $show->mark_current_status_code('Mark current status code');
        // $show->mark_current_status_date('Mark current status date');
        // $show->ref_page_no('Ref page no');
        // $show->ref_processed_date('Ref processed date');
        // $show->registration_date('Registration date');
        // $show->address('Address');
        // $show->city('City');
        // $show->city_of_registration('City of registration');
        // $show->state('State');
        // $show->class_number('Class number');
        // $show->class_description('Class description');
        // $show->representatives('Representatives');
        // $show->email('Email');
        // $show->fax_no('Fax no');
        // $show->telephone_no('Telephone no');
        // $show->website('Website');
        // $show->zip('Zip');
        // $show->country('Country');
        // $show->created_at('Created at');
        // $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ActionCode);

        $form->hidden('case_number', 'Case number');

        // ADDED 2 COLUMNS
        $form->number('action_code_type_id','Code Status');
        $form->number('action_code_campaign_id','Code Campaign Status');
        // $form->text('euipo_id', 'Euipo id');
        // $form->text('trademark', 'Trademark');
        // $form->datetime('tm_filing_date', 'Tm filing date')->default(date('Y-m-d H:i:s'));
        // $form->text('tm_filing_no', 'Tm filing no');
        // $form->text('tm_holder', 'Tm holder');
        // $form->text('application_reference', 'Application reference');
        // $form->text('mark_current_status_code', 'Mark current status code');
        // $form->text('mark_current_status_date', 'Mark current status date');
        // $form->text('ref_page_no', 'Ref page no');
        // $form->text('ref_processed_date', 'Ref processed date');
        // $form->text('registration_date', 'Registration date');
        // $form->text('address', 'Address');
        // $form->text('city', 'City');
        // $form->text('city_of_registration', 'City of registration');
        // $form->text('state', 'State');
        // $form->text('class_number', 'Class number');
        // $form->text('class_description', 'Class description');
        // $form->text('representatives', 'Representatives');
        // $form->email('email', 'Email');
        // $form->text('fax_no', 'Fax no');
        // $form->text('telephone_no', 'Telephone no');
        // $form->text('website', 'Website');
        // $form->text('zip', 'Zip');
        // $form->text('country', 'Country');

        return $form;
    }

    
}
