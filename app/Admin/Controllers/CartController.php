<?php

namespace App\Admin\Controllers;

use App\Cart;
use App\User;
use App\Country;
use App\Profile;
use App\Trademark;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Http\Request;

use App\Admin\Extensions\SendMailCustomer;

use App\Repositories\Utils;
use App\ActionCode;
use App\ActionCodeCart;

class CartController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {

        // dd($this->form());
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    public function store(Request $request)
    {
        // $allContent = $request->all();
        // dd($allContent);
        /**
         * Create trademark and generate action code
         */

        $last_action_code = ActionCode::max('id');
        $caseNumber = Utils::case_gen( ++$last_action_code );


        $tradeData = [
            'user_id' => $request->get('user_id'),
            'country_id' => $request->get('country_id'),
            'profile_id' => $request->get('profile_id'),
            'service' => $request->get('service'),
            'type' => $request->get('type'),
            'name' => $request->get('name'),
            'classes' => $request->get('classes'),
            'classes_description' => $request->get('classes_description'),
            'priority_date' => $request->get('priority_date'),
            'priority_country' => $request->get('priority_country'),
            'priority_number' => $request->get('priority_number'),
            'recently_filed' => $request->get('recently_filed'),
            'claim_priority' => $request->get('claim_priority'),
            'commerce' => $request->get('commerce'),
            'relative_trademark_id' => $request->get('relative_trademark_id'),
            'amount' => $request->get('amount'),
            'case_number' => $caseNumber
        ];

        

        $res = Trademark::create($tradeData);

        $actionCreateData = [
            'case_number' => $caseNumber,
            'action_code_type_id' => 9
            // 'action_code_campaign_id' => ,
        ];

        $resActionCode = ActionCode::create($actionCreateData); 
        
        $theForm = $this->form()->store();

        $last_cart_id = Cart::max('id');

        $actionCartData = [
            'user_id' => $request->get('user_id'),
            'action_code_id' => $resActionCode->id,
            'trademark_id' => $res->id,
            'cart_id' => $last_cart_id,
            'status' => $request->get('status') == 'pending' ? 'pending' : 'added' 
            // 'action_code_campaign_id' => ,
        ];
        
        $resActionCodeCart = ActionCodeCart::create($actionCartData);

        // dd($resActionCode);

        return $theForm;
    }    
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Cart);

        $grid->id('Id');
        $grid->model()->orderBy('created_at', 'desc');

        $grid->column('user_id', 'Owner')->display( function () {

            $str = "<a href='#' data-toggle='modal' data-target='#modal". $this->id ."'>". $this->user->name .'</a>';

            $str .= "<div class='modal fade' id='modal".$this->id."' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
                  <div class='modal-dialog' role='document'>
                    <div class='modal-content'>
                      <div class='modal-header'>
                        <h5 class='modal-title' id='exampleModalLabel'>Owner Information</h5>
                        <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                          <span aria-hidden='true'>&times;</span>
                        </button>
                      </div>
                      <div class='modal-body'>
                        <div class='form-group'>
                            <label for='fullname'>User ID</label>
                            <input type='text' class='form-control' id='fullname' value=". $this->user->id ." disabled>
                        </div>
                        <div class='form-group'>
                            <label for='fullname'>Full Name</label>
                            <input type='text' class='form-control' id='fullname' value=". $this->user->name ." disabled>
                        </div>
                        <div class='form-group'>
                            <label for='email'>Email Address</label>
                            <input type='text' class='form-control' id='email' value=". $this->user->email ." disabled>
                        </div>
                      </div>
                      <div class='modal-footer'>
                        <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                      </div>
                    </div>
                  </div>
                </div>";
            return $str;
        } );

        $grid->column('country_id', 'Country')->display( function () {
           
            if ( !$this->country ) {

                return "N/A";
            }

           $avatar = $this->country->avatar;

            return "<img src='" . asset('/images/'.$avatar) . "'>" . ' ' . $this->country->name;
        });
        $grid->column('profile_id', 'Profile')->display( function () {

            $title = "";
            $str   = "";

            if ( $this->profile ) {

                if ( $this->profile->company ) {

                    $title = $this->profile->company;

                } else {

                    $title = $this->profile->first_name . ' ' . $this->profile->last_name;
                }

                $str = "<a href='#' data-toggle='modal' data-target='#profile".$this->id."'>". $title .'</a>';

                $str .= "<div class='modal fade' id='profile".$this->id."' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
                      <div class='modal-dialog' role='document'>
                        <div class='modal-content'>
                          <div class='modal-header'>
                            <h5 class='modal-title' id='exampleModalLabel'>Profile</h5>
                            <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                              <span aria-hidden='true'>&times;</span>
                            </button>
                          </div>
                          <div class='modal-body'>
                            <div class='form-group'>
                                <label for='firstname'>Profile ID</label>
                                <input type='text' class='form-control' id='firstname' value='".$this->profile->id."' readonly>
                            </div>
                            <div class='form-group'>
                                <label for='firstname'>First Name</label>
                                <input type='text' class='form-control' id='firstname' value='".$this->profile->first_name."' readonly>
                            </div>
                            <div class='form-group'>
                                <label for='lastname'>Last Name</label>
                                <input type='text' class='form-control' id='lastname' value='".$this->profile->last_name."' readonly>
                            </div>
                            <div class='form-group'>
                                <label for='nature'>Nature</label>
                                <input type='text' class='form-control' id='nature' value='".$this->profile->nature."' readonly>
                            </div>
                            <div class='form-group'>
                                <label for='company'>Company Name</label>
                                <input type='text' class='form-control' id='company' value='".$this->profile->company."' readonly>
                            </div>
                            <div class='form-group'>
                                <label for='country'>Country</label>
                                <input type='text' class='form-control' id='country' value='".$this->profile->country."' readonly>
                            </div>
                            <div class='form-group'>
                                <label for='nationality'>Nationality</label>
                                <input type='text' class='form-control' id='nationality' value='".$this->profile->nationality."' readonly>
                            </div>
                            <div class='form-group'>
                                <label for='phone'>Phone</label>
                                <input type='text' class='form-control' id='phone' value='".$this->profile->phone_number."' readonly>
                            </div>
                            <div class='form-group'>
                                <label for='email'>Email Address</label>
                                <input type='text' class='form-control' id='email' value='".$this->profile->email."' readonly>
                            </div>
                            <div class='form-group'>
                                <label for='house'>No/House/Apt</label>
                                <input type='text' class='form-control' id='house' value='".$this->profile->house."' readonly>
                            </div>
                            <div class='form-group'>
                                <label for='state'>Street</label>
                                <input type='text' class='form-control' id='street' value='".$this->profile->street."' readonly>
                            </div>
                            <div class='form-group'>
                                <label for='city'>City</label>
                                <input type='text' class='form-control' id='city' value='".$this->profile->city."' readonly>
                            </div>
                            <div class='form-group'>
                                <label for='state'>State</label>
                                <input type='text' class='form-control' id='state' value='".$this->profile->state."' readonly>
                            </div>
                            <div class='form-group'>
                                <label for='zip'>Zip Code</label>
                                <input type='text' class='form-control' id='zip' value='".$this->profile->zip_code."' readonly>
                            </div>
                          </div>
                          <div class='modal-footer'>
                            <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                          </div>
                        </div>
                      </div>
                    </div>";
            }

            return $str;
        } );
        $grid->service('Service');
        $grid->type('Type');
        $grid->name('Name');
        $grid->amount('Amount');
        $grid->created_at('Created At');

        // $grid->column('email_status_sent', 'Email Status')->display( function ($value) {
        //     // dd($value);
        //     if ( $value == 'no') {
        //         $str = "Draft";
        //     } else {
        //         $str = "Email Sent";
        //     }

        //     return $str;
        // });

        $grid->column('action_code', 'Action Code')->display( function () {
            return $this->getActionCode();
        });

        $grid->status('Status');

        // $grid->actions(function ($actions) {
        //     $actions->append(new SendMailCustomer($actions->getKey()));
        // });

        // $grid->filter(function($filter){

        //     // Remove the default id filter
        //     $filter->disableIdFilter();

        //     $filter->like('order_number', 'Order Number');

        // });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Cart::findOrFail($id));

        $show->id('Id');
        $show->user_id('User id');
        $show->country_id('Country id');
        $show->profile_id('Profile id');
        $show->service('Service');
        $show->type('Type');
        $show->name('Name');
        $show->logo('Logo');
        $show->summary('Summary');
        $show->classes('Classes');
        $show->classes_description('Classes description');
        $show->priority_date('Priority date');
        $show->priority_country('Priority country');
        $show->priority_number('Priority number');
        $show->recently_filed('Recently filed');
        $show->claim_priority('Claim priority');
        $show->commerce('Commerce');
        $show->discount('Discount');
        $show->relative_trademark_id('Relative trademark id');
        $show->amount('Amount');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        $show->deatailed_contents('Detailed Contents');
        // $show->email_content('Email Content');

        return $show;
    }

    public function getClass( Request $request ) {
        $q = $request->get('q');

        return Country::where('name', 'like', "%$q%")->paginate(null, ['id', 'name as text']);
    }

    public function getUser( Request $request ) {
        $q = $request->get('q');

        return User::where('email', 'like', "%$q%")->paginate(null, ['id', 'email as text']);
    }

    public function getUserName( Request $request ) {
        $q = $request->get('q');

        return User::where('name', 'like', "%$q%")->paginate(null, ['id', 'name as text']);
    }

    public function getProfile( Request $request ) {
        $q = $request->get('q');

        return Profile::where('email', 'like', "%$q%")->paginate(null, ['id', 'first_name as text']);
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Cart);
        

        return Admin::form(Cart::class, function (Form $form) {

            $form->select('country_id','Country Name')->options( function ( $country_id ) {

                $country = Country::find($country_id);

                if ($country) {
                    return [$country->id => $country->name];
                }
            })->ajax('/admin/api/countries');

            $form->select('user_id','Owner Email')->options( function ( $user_id ) {

                $user = User::find($user_id);

                if ($user) {
                    return [$user->id => $user->email];
                }
            })->ajax('/admin/api/user');

            $form->select('profile_id','Owner Profile')->options( function ( $profile_id ) {

                $profile = Profile::find($profile_id);

                if ($profile) {
                    return [$profile->id => $profile->email . ' [' . $profile->first_name . ' ' . $profile->last_name . ']' ];
                }
            })->ajax('/admin/api/profile');

            $form->select('service', 'Service')->options( 
                [ 
                    "Trademark Study" => "Trademark Study",
                    "Trademark Registration" => "Trademark Registration",
                    "Trademark Registration [Priority]" => "Trademark Registration [Priority]",
                    "Trademark Monitoring" => "Trademark Monitoring",
                    "Other Services" => "Other Services"  
                ] 
            );
            $form->select('type', 'Type')->options( 
                [ "Word-Only" => "Word-Only", 
                  "Design-Only or Stylized Word-Only (Figurative)" => "Design-Only or Stylized Word-Only (Figurative)",
                  "Combined Word and Design" => "Combined Word and Design",
                  "Other" => "Other"
                ]
            );
            $form->text('name', 'Name');
            $form->image('logo', 'Logo')->uniqueName();
            $form->textarea('summary', 'Summary');
            $form->text('classes', 'Classes');
            $form->textarea('classes_description', 'Classes description')->default('N/A');
            $form->text('priority_date', 'Priority date');
            $form->text('priority_country', 'Priority country');
            $form->text('priority_number', 'Priority number');
            $form->text('recently_filed', 'Recently filed');
            $form->text('claim_priority', 'Claim priority');
            $form->text('commerce', 'Commerce');
            $form->text('discount', 'Discount');
            $form->number('relative_trademark_id', 'Relative trademark id');
            $form->number('amount', 'Amount');
            // $form->text('status', 'Status')->default('pending');

            $form->select('status', 'Status')->options( 
                [ "pending" => "Pending - customer cant see this item until confirm", 
                  "active" => "Active - Show in customer cart and selected",
                  "inactive" => "Inactive - Show in customer cart but not selected"
                ]
            )->default('active');
            $form->ckeditor('deatailed_contents', 'Detailed Contents')
                ->default($this->getDefaultEmailContent())
                ->options(['height' => 500])
                ->help('    <p>Please modify this template to explain this cart service.<br>
                            Current content is just a template.<br>
                            This content will show in frontend with action code. ex. <i>https://trademarkers.com/[action-code]</i><br>
                            Action for this cart can be seen in list column after saving this cart item.<br>
                            (IF STATUS OF THIS CART IS NOT SET TO PENDING THEN THIS ACTION CODE WONT SHOW, THIS WILL BE ADDED TO CUSTOMERS CART DIRECTLY)
                            </p>');
            // $form->ckeditor('email_content')->default($this->getDefaultEmailContent());

            
        });

        

        return $form;
    }

    public function getDefaultEmailContent()
    {
        $html = "";

        $html = "<p>This is to inform you that an Office Action has been issued by the U.S. Patent and Trademark Office (USPTO) for the subject trademark. Below is the initial office action analysis by our US attorney for your review and approval. We have until [DATE] to submit the response. To view the office action, please click on this link.</p>
                <p>The trademark examining attorney has searched the Office’s database of registered and pending marks and has found no conflicting marks that would bar registration based on likelihood of confusion. The following issue however must be addressed before the mark can proceed.</p>
                <p><strong>SPECIMEN REFUSAL - CLASS [NUMBER]</strong></p>
                <p>The examiner finds that the specimens previously submitted are insufficient to prove bona fide use of the mark in U.S. commerce.</p>
                <p>Particularly, the examiner finds that the article from [COMPANY NAME] is a promotion by a third party and that the mark being promoted is [OTHER NAME], not [ACTUAL NAME]. The material consisting of an internal presentation or brochure was also found to be insufficient.</p>
                <p><strong><i>Suggested Action</i></strong></p>
                <p>One of the materials suggested by the examiner as a valid specimen is a screenshot of a webpage that shows the mark used in the actual sale, rendering, or advertising of the services. Our research shows that you have a live webiste at [WEBSITE URL] which promotes and gives information about your Class [NUMBER] services. Furthermore, the website shows a the wordings [WORDS] in the mark as connected or in one word (see image below).</p> 
                <p>As such, there is a 80-90% chance that the website will be accepted as sufficient substitute specimen to prove bona fide use of your trademark in the U.S. We, therefore, suggest to submit the attached screenshots of your website as a substitute specimen to overcome this issue. Please advise if you wish to proceed.</p>
                <p><strong>COSTS</strong></p>
                <p>Considering that the office action only requires a non-substantive response, our standard fee of 350 USD will apply. For your convenience, as soon as you confirm the above matters and cost, we will add a service item in your cart for check out and payment.</p>
                <p>We await your response.</p>";
        return $html;
    }


    // public function getDefaultEmailContent()
    // {
    //     $html = "";

    //     $html = "<p>Dear [Client],</p>
    //             <br>
    //             <p>This is to inform you that an Office Action has been issued by the U.S. Patent and Trademark Office (USPTO) for the subject trademark. Below is the initial office action analysis by our US attorney for your review and approval. We have until [DATE] to submit the response. To view the office action, please click on this link.</p>
    //             <p>The trademark examining attorney has searched the Office’s database of registered and pending marks and has found no conflicting marks that would bar registration based on likelihood of confusion. The following issue however must be addressed before the mark can proceed.</p>
    //             <p><strong>SPECIMEN REFUSAL - CLASS [NUMBER]</strong></p>
    //             <p>The examiner finds that the specimens previously submitted are insufficient to prove bona fide use of the mark in U.S. commerce.</p>
    //             <p>Particularly, the examiner finds that the article from [COMPANY NAME] is a promotion by a third party and that the mark being promoted is [OTHER NAME], not [ACTUAL NAME]. The material consisting of an internal presentation or brochure was also found to be insufficient.</p>
    //             <p><strong><i>Suggested Action</i></strong></p>
    //             <p>One of the materials suggested by the examiner as a valid specimen is a screenshot of a webpage that shows the mark used in the actual sale, rendering, or advertising of the services. Our research shows that you have a live webiste at [WEBSITE URL] which promotes and gives information about your Class [NUMBER] services. Furthermore, the website shows a the wordings [WORDS] in the mark as connected or in one word (see image below).</p> 
    //             <p>[IMAGE]</p>
    //             <p>As such, there is a 80-90% chance that the website will be accepted as sufficient substitute specimen to prove bona fide use of your trademark in the U.S. We, therefore, suggest to submit the attached screenshots of your website as a substitute specimen to overcome this issue. Please advise if you wish to proceed.</p>
    //             <p><strong>COSTS</strong></p>
    //             <p>Considering that the office action only requires a non-substantive response, our standard fee of 350 USD will apply. For your convenience, as soon as you confirm the above matters and cost, we will add a service item in your cart for check out and payment.</p>
    //             <p>We await your response.</p><br>
    //             <p>Sincerely,</p><br>
    //             <p>[NAME]</p>
    //             <p>-----</p>
    //             <p>[NAME] - [NAME]@trademarkers.com</p>
    //             <p>Paralegal</p>
    //             <p>TRADEMARKERS LLC</p>
    //             <p>246 West Broadway</p>
    //             <p>New York, NY 10013</p>
    //             <p>212-468-5500 Office</p>
    //             <p>212-504-0888 Fax</p>";
    //     return $html;
    // }


}
