<?php

namespace App\Admin\Controllers;

use App\ClassDescription;
use App\TrademarkClass;
use App\Http\Controllers\Controller;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Http\Request;

class DescriptionController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header(' ')
            ->description(' ')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(ClassDescription::class, function (Grid $grid) {
            
            $grid->id('id')->sortable();

            $grid->trademarkclass()->name('Class Name')->sortable();

            $grid->description()->display( function ( $description ) {
                return str_limit($description, 20, '...');
            } )->sortable();

            $grid->created_at()->sortable();
            $grid->updated_at()->sortable();

            $grid->filter(function($filter){

                // Remove the default id filter
                $filter->disableIdFilter();

                // Add a column filter
                $filter->like('description', 'Description Name');

            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ClassDescription::findOrFail($id));

        $show->id('Id');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    public function getClass( Request $request ) {
        $q = $request->get('q');

        return TrademarkClass::where('name', 'like', "%$q%")->paginate(null, ['id', 'name as text']);
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(ClassDescription::class, function (Form $form) {

            $form->display('id');

            $form->select('trademark_class_id','Class Name')->options(function ($id) {
                
                $class = TrademarkClass::find($id);

                if ($class) {
                    return [$class->id => $class->name];
                }
            })->ajax('/admin/api/class');

            $form->text('description');

            $form->datetime('created_at');
            $form->datetime('updated_at');
        });
    }
}
