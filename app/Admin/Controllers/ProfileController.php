<?php

namespace App\Admin\Controllers;

use App\Profile;
use App\User;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ProfileController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Profile);

        $grid->column('id', 'Profile ID');
        $grid->column('user_id', 'User ID');
        $grid->first_name('First name');
        $grid->last_name('Last name');
        $grid->created_at('Created at');

        $grid->filter(function($filter){

            // Remove the default id filter
            $filter->disableIdFilter();

            $filter->equal('user_id', 'User Id');
            $filter->like('Email', 'Email Address');
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Profile::findOrFail($id));

        $show->id('Id');
        $show->user_id('User id');
        $show->first_name('First name');
        $show->middle_name('Middle name');
        $show->last_name('Last name');
        $show->nature('Nature');
        $show->company('Company');
        $show->country('Country');
        $show->country_origin('Country origin');
        $show->nationality('Nationality');
        $show->city('City');
        $show->state('State');
        $show->street('Street');
        $show->house('House');
        $show->zip_code('Zip code');
        $show->phone_number('Phone number');
        $show->email('Email');
        $show->fax('Fax');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Profile);

        $users = User::get();
        $userOption = $users->pluck('email', 'id');

        $form->select('user_id', 'User id')->options($userOption);
        $form->text('first_name', 'First name');
        $form->text('middle_name', 'Middle name');
        $form->text('last_name', 'Last name');
        $form->text('nature', 'Nature');
        $form->text('company', 'Company');
        $form->text('country', 'Country');
        $form->text('country_origin', 'Country origin');
        $form->text('nationality', 'Nationality');
        $form->text('city', 'City');
        $form->text('state', 'State');
        $form->text('street', 'Street');
        $form->text('house', 'House');
        $form->text('zip_code', 'Zip code');
        $form->text('phone_number', 'Phone number');
        $form->email('email', 'Email');
        $form->text('fax', 'Fax');

        return $form;
    }
}
