<?php

namespace App\Admin\Controllers;

use App\Country;
use App\ActionCode;
use App\ActionCodeCase;
use App\Brand;
use App\BrandLead;
use App\ActionCodeCampaign;
use App\Repositories\Utils;
use App\Services\CommonService;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Illuminate\Support\Facades\DB;
use Encore\Admin\Form;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Carbon\Carbon;
use Encore\Admin\Grid\Column;
use Illuminate\Support\MessageBag;
use App\Repositories\MandrillMailer;
use Encore\Admin\Auth\Database\Role;
use Encore\Admin\Auth\Database\Administrator;
Use Encore\Admin\Widgets\Table;

class BrandLeadController extends Controller
{
    use HasResourceActions;

    public $commonService;

    public function __construct(
        \App\Services\CommonService $commonService) {

        $this->commonService = $commonService;
    }

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Leads List View')
            ->description('List View')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        $case = $this->commonService->getCase($id);

        return $content
            ->header('Edit lead ' . $case->trademark)
            ->description( $case->number . ' ' . $case->tm_holder )
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ActionCodeCase);

        $grid->model()->orderBy('created_at', 'desc');

        // disable actions show only on grid just for reference
        // $grid->disableCreation();
        if ( ! Admin::user()->inRoles(['admin', 'administrator']) ) {
            $grid->disableExport();
        }
        
        $grid->actions(function ($actions) {
            $actions->disableDelete();
            // $actions->disableEdit();
            $actions->disableView();
        });

        $grid->filter(function($filter){

            // Remove the default id filter
            $filter->disableIdFilter();

            $filter->column(1/2, function ($filter) {
                $filter->like('trademark', 'Word Mark');
                $filter->equal('number', 'Trademark Number');
                $filter->equal('registration_number', 'Registration Number');

                $filter->where(function ($query) {

                    $query->where('class_number','LIKE', '%'.$this->input.',%')
                            ->orWhere('class_number','LIKE', '%'.$this->input);
                
                }, 'Class Number');

                $filter->between('tm_filing_date', 'Filing Date Filter')->datetime();
                $filter->between('registration_date', 'Registration Date Filter')->datetime();
                
            });

            $filter->column(1/2, function ($filter) {

                $campaigns = ActionCodeCampaign::orderBy('created_at', 'desc')->get();
                $campaignOptions = $campaigns->pluck('name', 'id');

                $brands = Brand::orderBy('created_at', 'desc')->get();
                $brandOptions = $brands->pluck('brand', 'id');

                $filter->like('tm_holder', 'TM Holder');
                $filter->like('mark_current_status_code', 'Status');
                $filter->equal('action_code_campaign_id','Group Per Campaign')->select($campaignOptions);
                
                $filter->like('country', 'Country');

                $filter->where(function ($query) {

                    $query->whereHas('brand', function ($query) {
                        $query->where('brand_id','LIKE', '%'.$this->input.',%')
                            ->orWhere('brand_id','LIKE', '%'.$this->input);
                    });
                
                }, 'Brand')->select($brandOptions);
            });

            

        });
       
        // $grid->id();
        $grid->number('Trademark Number');
        $grid->trademark();
        $grid->tm_holder('Holder');
        $grid->tm_filing_date('Filing Date');
        $grid->registration_date('Registration Date')
            ->display( function($registration_date) {
                if (strtotime($registration_date) < strtotime('1980-01-01 00:00:00')) {
                    return '-';
                } else {
                    return $registration_date;
                }
            });
        $grid->trademark_office();
        $grid->mark_current_status_code('Current Status');
        $grid->mark_current_status_date('Current Status Date');
        
        $grid->address()->expand(function () {

            $headers = [ 
                'City', 
                'State', 
                'Fax No',
                'Telephone No', 
                'Zip',
                'Country Code',
                'Country',
                'Email'
            ];
            $rows = [
                [
                    $this->city, 
                    $this->state, 
                    $this->fax_no, 
                    $this->telephone_no, 
                    $this->zip, 
                    $this->country_code, 
                    $this->country, 
                    $this->email
                ]
            ];

            $table = new Table($headers, $rows);

            return $table;
        });
        $grid->class_number();
        $grid->representatives();


        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        // return redirect("/admin/manage/brand-leads/{$id}/edit");
        // $show = new Show(Brand::findOrFail($id));

        // $show->brand('Brand');
        // $show->notes('Notes');

        // return $show;
        admin_toastr('update success');
        return redirect("/admin/manage/brand-leads/".$id."/edit");
    }

    
    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ActionCodeCase);

        // $brandList = Brand::get();
        // $brandOptions = $brandList->pluck('brand', 'id');

        $countries = Country::get();
        $countriesOptions = $countries->pluck('name', 'abbr');

        $leadId = request()->segment(4);

        $form->hidden('id');
        $form->hidden('action_code_id');
        $form->hidden('action_code_campaign_id');
        
        // brand
        // TAG BRAND 
        $form->divider("Tag to a Brand"); 
        if ( is_numeric($leadId) ) {
            $form->multipleSelect('brand.brand_id', "Tag To Brand")->options( function ( $brand_id ) {
                $data = [];
  
                if ( is_array($brand_id) ) {
                    

                    foreach ($brand_id as $id) {
                        if ( $id != 0 ) {
                            $brand = Brand::findOrFail($id);

                            if ($brand) 
                            $data[$brand->id] = $brand->brand;
                        }
                        
                    }
                }
                // dd($data);
                return $data;
        
            })->ajax('/admin/api/brands');
            // CREATE BRAND IF NOT EXIST
            $form->text('brand.placeholder', 'New Brand')
                ->help("<i>If this has value this will create brand if not yet created and tag this lead to that brand.</i>");
        } else {
            $form->text('brand.placeholder', 'Brand')
                ->help("<i>Submit this lead before selecting a brand</i>")
                ->disable();
        }
        
        $form->divider("Lead Information");
        // $form->display('id');
        $form->text('number', 'Serial Number');
        $form->text('registration_number', 'Registration Number');
        $form->text('trademark', 'Trademark');
        $form->text('class_number', 'Trademark Class');
        // $form->text('trademark_office', 'Trademark Office');
        $form->text('mark_current_status_code', 'Trademark Status');
        $form->text('mark_current_status_date', 'Trademark Status Date');
        $form->date('tm_filing_date', 'Filling Date');
        $form->text('tm_holder', 'Trademark Holder');
        if ( is_numeric($leadId) ) {

            $lead = ActionCodeCase::find($leadId);
            if ( $lead && strtotime($lead->registration_date) < strtotime('1980-01-01 00:00:0') ) {
                $lead->registration_date = null;
                $lead->save();
            }
            $form->date('registration_date', 'Registration Date');
        }
        $form->select('country_code', 'Country')->options($countriesOptions);
        // $form->text('country_code');

        $form->text('jurisdiction', 'Trademark Jurisdiction');
        
        $states = [
            'on'  => ['value' => 'yes', 'text' => 'Yes', 'color' => 'success'],
            'off' => ['value' => 'no', 'text' => 'No', 'color' => 'danger'],
        ];

        $form->switch('is_priority','Priority?')->states($states)->default('no');
        $form->textarea('notes', 'Priority Details')->placeholder('Please enter priority details');
        

        // LEAD VALUES ===============================
        $form->text('address');
        $form->text('city');
        // $form->text('city_of_registration');
        $form->text('state');
        $form->text('fax_no');
        $form->text('telephone_no');
        $form->text('zip', 'Postal');
        $form->text('brand.atty_name','Attorney Name');
        $form->email('brand.atty_email','Attorney Email');

        

        // $form->text('representatives','Representatives');

        $form->divider("Lead Emails");
        $form->hasMany('emails', 'Lead Emails', function (Form\NestedForm $email) {
            $email->email('email', 'Email');
            $email->text('notes', 'Notes');

        })->mode('table');



        // ============================================ 
        // CALLBACK ===================================
        // ============================================ 
        $form->saving(function (Form $form) {
            
            $leadId = request()->segment(4);

            if ( $leadId && $form->brand['placeholder'] ) {
                
                $res = Brand::where('brand',$form->brand['placeholder'])->first();

                if (!$res) {
                    $data = [
                        'user_id'   => Admin::user()->id,
                        'brand'     => $form->brand['placeholder'],
                        'notes'     => "Created frome lead"
                    ];
                    Brand::create($data);
                } else {
                    admin_toastr('Duplicate', 'error');
                    return redirect("/admin/manage/brand-leads/".$leadId."/edit");
                }

            } 

            if ( !$form->action_code_id ) {

                $last_action_code = ActionCode::max('id');
                $last_action_code += 1;

                $action_code = Utils::case_gen( $last_action_code );

                $code = ActionCode::create( [
                    'case_number'             => $action_code,
                    'action_code_type_id'     => 7,
                    'action_code_campaign_id' => 0,
                ] );

                $form->action_code_id = $code->id;
                $form->action_code_campaign_id = 0;
            }

        });

        $form->submitted(function (Form $form) {
            //...
        });

        $form->saved(function (Form $form) {
            //...
            // dd($form->brand);
            $leadId = request()->segment(4);

            if ( $leadId ) {

                $res = BrandLead::where(
                    [
                        ['lead_id',$leadId]
                    ]
                )->first();
                            // dd($res);
                $brand = Brand::where('brand',$form->brand['placeholder'])->first();
                        // dd($brand);
                if ( $brand && $res) {
                    // dd(0);
                    // $res->brand_id = $brand->id;
                    // array_push($res->brand_id, $brand->id);
                    // dd($res->brand_id);
                    $res->placeholder = '';
                    $res->save();
                }

                $leadID = ActionCodeCase::max('id');
                $leadID = $form->id ? $form->id : $leadID;

                admin_toastr('update success');
                return redirect("/admin/manage/brand-leads/".$leadID."/edit");

            }

        });



        // ============================================ 
        // Tools    ===================================
        // ============================================ 
        // $form->tools(function (Form\Tools $tools) {
        //     $tools->disableDelete();
        //     $tools->disableView();
        //     // $tools->disableList();
        // });

        return $form;
    }


}
