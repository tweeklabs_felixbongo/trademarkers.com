<?php

namespace App\Admin\Controllers;

use App\ActionCodeCase;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use App\ActionCodeCampaign;

use App\Country;

class CaseController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ActionCodeCase);

        $grid->model()->orderBy('id', 'desc');

        $grid->id('Id');
        $grid->action_code_id('action_code_id');
        $grid->number('Serial Number');
        $grid->trademark('Trademark');
        $grid->tm_filing_date('Filling Date');
        $grid->tm_holder('holder');

        $grid->filter(function($filter){

            // Remove the default id filter
            $filter->disableIdFilter();

            $filter->like('trademark', 'Trademark');

            $campaigns = ActionCodeCampaign::all();

            $arr = null;
            foreach ($campaigns as $campaign) {
                $arr[$campaign->id] = $campaign->name;
                // array_push();
            }

            $filter->equal('action_code_campaign_id','Campaign')->radio($arr);

        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ActionCodeCase::findOrFail($id));

        $show->id('Id');
        $show->number('Serial Number');
        $show->trademark('Trademark');
        $show->tm_filing_date('Filling Date');
        $show->tm_holder('TM Holder');
        $show->email('Email');
        $show->fax_no('Fax Number');
        $show->telephone_no('Telephone No');
        $show->application_reference('Application Reference');
        $show->ref_processed_date('Reference Processed Date');
        $show->registration_date('Registration Date');
        $show->address('Address');
        $show->city('City');
        $show->city_of_registration('City Of Registration');
        $show->state('State');
        $show->class_number('Class Number');
        $show->class_description('Class Description');
        $show->representatives('Representatives');
        $show->zip('Zip');
        $show->country('Country');
        $show->website('Website');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ActionCodeCase);

        $form->text('trademark', 'Trademark');

        $form->text('tm_holder', 'Applicant Name');

        $form->datetime('tm_filing_date', 'Filing Date');
        $form->datetime('mark_current_status_code', 'Mark Status');

        return $form;
    }
}
