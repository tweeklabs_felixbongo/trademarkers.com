<?php

namespace App\Admin\Controllers;

use App\Price;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use App\Country;

class PriceController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Price);

        $grid->id('Id');
        $grid->column('country.name', 'Country');
        $grid->service_type('Service type');
        $grid->initial_cost('Initial cost');
        $grid->additional_cost('Additional cost');
        $grid->logo_initial_cost('Logo initial cost');
        $grid->logo_additional_cost('Logo additional cost');

        $grid->filter(function($filter){

            // Remove the default id filter
            $filter->disableIdFilter();

            // Add a column filter
            $countries = Country::get();
            $countriesOptions = $countries->pluck('name', 'id');

            $filter->equal('country_id', 'Country')->select($countriesOptions);
            $filter->like('service_type', 'Service Type')->select(
                [
                    'Study' => 'Study',
                    'Registration' => 'Registration',
                    'Certificate' => 'Certificate',
                    'Office Status - Simple' => 'Office Status - Simple',
                    'Office Status - Complex' => 'Office Status - Complex',
                    'Office Status - Likelihood' => 'Office Status - Likelihood'
                ]
            );

        });

        $grid->actions(function ($actions) {

            $actions->disableDelete();
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Price::findOrFail($id));

        $show->id('Id');
        $show->country_id('Country id');
        $show->service_type('Service type');
        $show->initial_cost('Initial cost');
        $show->additional_cost('Additional cost');
        $show->logo_initial_cost('Logo initial cost');
        $show->logo_additional_cost('Logo additional cost');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Price);

        $countries = Country::get();
        $countriesOptions = $countries->pluck('name', 'id');
        // $form->display('country.name', 'Country Name');
        $form->select('country_id', 'Country')->options($countriesOptions);
        $form->select('service_type', 'Service type')->options([
            'Study' => 'Study',
            'Registration' => 'Registration',
            'Certificate' => 'Certificate',
            'Monitoring' => 'Monitoring',
            'Office Status - Simple' => 'Office Status - Simple',
            'Office Status - Complex' => 'Office Status - Complex',
            'Office Status - Likelihood' => 'Office Status - Likelihood'
        ]);
        $form->decimal('initial_cost', 'Initial cost')->default('0');
        $form->decimal('additional_cost', 'Additional cost')->default('0');
        $form->decimal('logo_initial_cost', 'Logo initial cost')->default('0');
        $form->decimal('logo_additional_cost', 'Logo additional cost')->default('0');

        $form->tools(function (Form\Tools $tools) {

            // Disable `Delete` btn.
            $tools->disableDelete();
        });

        return $form;
    }
}
