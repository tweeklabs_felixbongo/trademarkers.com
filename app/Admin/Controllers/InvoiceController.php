<?php

namespace App\Admin\Controllers;

use App\Invoice;
use App\Order;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class InvoiceController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Invoice);

        $grid->model()->orderBy('created_at', 'desc');

        $grid->reference('Invoice #');
        $grid->column('order.order_number', 'Order #');
        $grid->column('payment.reference_no', 'Payment Reference');
        $grid->column('user.name', 'Owner')->display( function () {

            if ( !$this->user ) {
                return '';
            } 

            $str = "<a href='#' data-toggle='modal' data-target='#modal". $this->id ."'>". $this->user->name .'</a>';

            $str .= "<div class='modal fade' id='modal".$this->id."' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
                  <div class='modal-dialog' role='document'>
                    <div class='modal-content'>
                      <div class='modal-header'>
                        <h5 class='modal-title' id='exampleModalLabel'>Owner Information</h5>
                        <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                          <span aria-hidden='true'>&times;</span>
                        </button>
                      </div>
                      <div class='modal-body'>
                        <div class='form-group'>
                            <label for='fullname'>User ID</label>
                            <input type='text' class='form-control' id='fullname' value=". $this->user->id ." disabled>
                        </div>
                        <div class='form-group'>
                            <label for='fullname'>Full Name</label>
                            <input type='text' class='form-control' id='fullname' value=". $this->user->name ." disabled>
                        </div>
                        <div class='form-group'>
                            <label for='email'>Email Address</label>
                            <input type='text' class='form-control' id='email' value=". $this->user->email ." disabled>
                        </div>
                      </div>
                      <div class='modal-footer'>
                        <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                      </div>
                    </div>
                  </div>
                </div>";
            return $str;
        } );
        $grid->status('Status');
        $grid->created_at('Created at');

        $grid->filter(function($filter){

            // Remove the default id filter
            $filter->disableIdFilter();
            $filter->like('reference', 'Invoice #');
            $filter->like('order.order_number', 'Order #');
        });

        $grid->actions(function ($actions) {
            // $actions->disableEdit();
            $actions->prepend(
                '<a target="_blank" href="'.$actions->getResource().'/view-pdf/'.$actions->getKey().'" class="grid-row-view">
                    <i class="fa fa-file"></i>
                </a>' );

        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Invoice::findOrFail($id));

        $show->order_id('Order id');
        $show->user_id('User id');
        $show->reference('Reference');
        $show->name('Name');
        $show->path('Path');
        $show->status('Status');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Invoice);

        $form->display('order_id', 'Order id');
        $form->display('user_id', 'User id');
        $form->text('reference', 'Reference');
        $form->text('name', 'Name');

        $states = [
            'on'  => ['value' => 'Paid', 'text' => 'Paid', 'color' => 'success'],
            'off' => ['value' => 'Pending', 'text' => 'Pending', 'color' => 'danger'],
        ];

        $form->switch('status', 'Status')->states( $states );

        return $form;
    }

    // DOWNLOAD PDF
    public function download_invoice( $invoice_id )
    {
        $invoice = Invoice::find( $invoice_id );
        $order   = Order::find($invoice->order_id);

        $pdf = app('dompdf.wrapper')->loadView( 'invoice', ['order' => $order, 'status' => $invoice->status] );

        return $pdf->download( $invoice->name . '.pdf');
    }

    public function view_invoice( $invoice_id )
    {
        $invoice = Invoice::find( $invoice_id );
        $order   = Order::find($invoice->order_id);

        // populate some customer details
        $order = Invoice::validate_billing_info( $order );

        $pdf = app('dompdf.wrapper')->loadView( 'invoice', ['order' => $order, 'status' => $invoice->status] );

        return $pdf->stream( $invoice->name . '.pdf');
    }
}
