<?php

namespace App\Admin\Controllers;

use App\StoreHour;

use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Illuminate\Support\Facades\DB;
use Encore\Admin\Form;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Carbon\Carbon;
use Encore\Admin\Grid\Column;
use Illuminate\Support\MessageBag;
use App\Repositories\MandrillMailer;
use Encore\Admin\Auth\Database\Role;
use Encore\Admin\Auth\Database\Administrator;
Use Encore\Admin\Widgets\Table;

class StoreHourController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new StoreHour);


        $grid->range_time("Open")->display(function($open){
            switch($open){
                case 'full' :
                    return "24 hours open";
                break;

                case 'half' :
                    return "Open";
                break;

                case 'close' :
                    return "Closed";
                break;
            }
        });

        $grid->day()->display(function($day){
            switch($day) {
                case  '0':
                    return "Sunday";
                break;
                case  '1':
                    return "Monday";
                break;
                case  '2':
                    return "Tuesday";
                break;
                case  '3':
                    return "Wensday";
                break;
                case  '4':
                    return "Thursday";
                break;
                case  '5':
                    return "Friday";
                break;
                case  '6':
                    return "Saturday";
                break;
            }
        });
        $grid->start_time();
        $grid->end_time();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(StoreHour::findOrFail($id));


        return $show;
    }

    
    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new StoreHour);

        $form->select("day")->options([
            "1" => "Monday",
            "2" => "Tuesday",
            "3" => "Wensday",
            "4" => "Thursday",
            "5" => "Friday",
            "6" => "Saturday",
            "0" => "Sunday"
        ]);

        $form->select("range_time", "Time Range")->options([
            "full" => "24 Hours Open",
            "half" => "Open with specific hour",
            "close" => "Closed"
        ]);

        $form->time("start_time", "Opening Time");
        $form->time("end_time", "Closing Time");




        return $form;
    }

}
