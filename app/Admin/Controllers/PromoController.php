<?php

namespace App\Admin\Controllers;

use App\ActionCodePromo;
use App\ActionCode;
use App\ActionCodeCampaign;
use App\Country;
use App\User;
use App\Profile;
use App\Repositories\Utils;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Http\Request;
use Carbon\Carbon;


class PromoController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ActionCodePromo);
        $grid->model()->orderBy('id', 'desc');
        
        $grid->id('Id');
        $grid->column('action_code.case_number', 'Promo Code');

        $grid->column('user_id', 'User')->display( function () {


            if ( !$this->user ) {

                return "<span style='color:red;'>All Users</span>";
            }

            $str = "<a href='#' data-toggle='modal' data-target='#modal". $this->id ."'>". $this->user->name .'</a>';

            $str .= "<div class='modal fade' id='modal".$this->id."' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
                  <div class='modal-dialog' role='document'>
                    <div class='modal-content'>
                      <div class='modal-header'>
                        <h5 class='modal-title' id='exampleModalLabel'>Owner Information</h5>
                        <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                          <span aria-hidden='true'>&times;</span>
                        </button>
                      </div>
                      <div class='modal-body'>
                        <div class='form-group'>
                            <label for='fullname'>User ID</label>
                            <input type='text' class='form-control' id='fullname' value=". $this->user->id ." disabled>
                        </div>
                        <div class='form-group'>
                            <label for='fullname'>Full Name</label>
                            <input type='text' class='form-control' id='fullname' value=". $this->user->name ." disabled>
                        </div>
                        <div class='form-group'>
                            <label for='email'>Email Address</label>
                            <input type='text' class='form-control' id='email' value=". $this->user->email ." disabled>
                        </div>
                      </div>
                      <div class='modal-footer'>
                        <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                      </div>
                    </div>
                  </div>
                </div>";
            return $str;
        } );

        // $grid->column('case_number', 'Trademark')->display( function () {

        //     if ( !$this->case_number ) {

        //         return "<span style='color:red;'>All Trademarks</span>";
        //     }

        //     return $this->case_number;
        // } );

        $grid->column('country_id', 'Country')->display( function ($country_id) {

            if ( !$this->country ) {

                return "<span style='color:red;'>All Countries</span>";
            }

            $countries = Country::find($country_id);

            $strValue = "";

            foreach ( $countries as $country ) {
                $strValue .= "<p><img src='" . asset('/images/'.$country->avatar) . "'>" . ' ' . $country->name . "</p>";
            }

            return $strValue;

        } );

        $grid->column('promo_type', 'Promo Type')->display( function ($promoTypes) {
            
            $str = "";
            
            foreach ($promoTypes as $promoType) {
                $str .= $promoType . "<br>";
            }

            return $str;

        } );

        $grid->discount('Discount (%)');
        $grid->limit('Limit');
        // $grid->expiry_date('Expiry date');
        $grid->column('expiry_date', 'Date')->display( function( $expiry_date ) {
            if ( $expiry_date ) {
                $date = Carbon::parse( $expiry_date );

                return $date->format('M d, Y');
            } else {
                return "No Expiry";
            }
            // $date = Carbon::parse( $expiry_date );

            // return $date->format('M d, Y');
        } );
        // $grid->supplementary_discount('Supplementary Discount(%)');
        // $grid->supplementary_amount_point('Supplementary Point');
        // $grid->status('Status');
        $grid->campaign_id('Campaign')->display( function() {
            
            // $campaign = ActionCodeCampaign::findOrFail( $this->campaign_id );
            // dd($this->campaign);
            return $this->campaign ? $this->campaign->name : 'N/A';

        });

        $grid->notes('Description');
        
        $grid->filter(function($filter){

            $filter->disableIdFilter();

            $filter->like('action_code.case_number', 'Promo Code');

            $filter->between('expiry_date', 'Expiry Date Filter')->datetime();

            $filter->in('status')->multipleSelect([
                'expired'   => 'Expired',
                'on-going'   => 'On-going',
                'done'   => 'Done'
            ]);

            

        });

        // $grid->created_at('Created at');
        // $grid->updated_at('Updated at');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ActionCodePromo::findOrFail($id));

        $show->id('Id');
        $show->action_codes_id('Action codes id');
        $show->user_id('User id');
        $show->case_number('Promo Code');
        // $show->country_id('Country id');
        $show->country_id('Country')->as(function($countryId){

            $countries = Country::find($countryId);
            $strCountryName = "";

            foreach ( $countries as $country ) {
                $strCountryName .= $country->name . ', ';
            }
      
            return $strCountryName ? $strCountryName : 'N/A';
        });
        $show->discount('Discount');
        $show->limit('Limit');
        $show->expiry_date('Expiry date');
        $show->supplementary_discount('Supplementary Discount(%)');
        $show->supplementary_amount_point('Supplementary Point');
        $show->status('Status');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    public function getClass( Request $request ) {
        // dd(0);
        $q = $request->get('q');

        // return 1;

        return ActionCode::where('case_number', 'like', "%$q%")->paginate(null, ['id', 'case_number as text']);
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ActionCodePromo);

        $last_action_code = ActionCode::max('id');
        $last_action_code += 1;

        $url = url()->current();

        // if ( strpos($url, 'create') !== false ) {

        //     $form->display('action_code_id', 'Case Number')->default( $last_action_code );

        // } else {

        //     $form->hidden('action_code_id', 'Case Number');
        // }
        $campaigns = ActionCodeCampaign::get();
        $campaignOptions = $campaigns->pluck('name', 'id');


        $form->select('campaign_id')->options($campaignOptions);

        $form->hidden('action_code_id', 'Case Number');

        // $form->multipleSelect('country_id','Country')->ajax('/admin/api/countries');

        // $form->hasMany('promoCountry', '', function (Form\NestedForm $form) {
        //     // $form->hidden('admin_user_id')->default( Admin::user()->id );
        //     $form->multipleSelect('country_id','Country')->options( function ( $country_id ) {

        //         $country = Country::find($country_id);

        //         if ($country) {
        //             return [$country->id => $country->name];
        //         }
        //     })->ajax('/admin/api/countries');
        // });
        
        $form->multipleSelect('country_id','Country')->options( function ( $country_id ) {

            $countries = Country::find($country_id);

            if ($countries) {
                return $countries->pluck('name', 'id');
            }
            
            

        })->ajax('/admin/api/countries');


        $form->select('user_id','User')->options( function ( $user_id ) {

            $user = User::find($user_id);

            if ($user) {
                return [$user->id => $user->email];
            }
        })->ajax('/admin/api/user');

        $form->multipleSelect('promo_type', 'Promo Type')->options( 
            [ 
                "Trademark Study" => "Trademark Study",
                "Trademark Registration" => "Trademark Registration",
                "Trademark Registration [Priority]" => "Trademark Registration [Priority]",
                "Trademark Monitoring" => "Trademark Monitoring",
                "Other Services" => "Other Services"  
            ] 
        );
        
        // $form->text('case_number', 'Case Number');

        $form->decimal('discount', 'Discount')->default(1)->rules('required')->help("Set discount to 0 if using random discount");
        $form->decimal('max_discount', 'Max Discount')->help("Set Max Discount else leave it 0")->default(0);
        $form->decimal('limit', 'Limit')->rules('required'); // -1 for unlimited
        $form->date('expiry_date', 'Expiry date');

        // $form->decimal('supplementary_discount', 'Supplementary Discount(%)');
        // $form->decimal('supplementary_amount_point', 'Supplementary Point (Amount)');

        $form->select('status', 'Status')->options(
            [ 'expired' => "Expired", 
              'on-going' => "On-going",
              'done' => "Done",
            ]
        )->default('on-going');

        $form->textarea('notes','Description');

        $form->saving(function (Form $form) {

            // dd($form->promo_type);
            
            if ( $form->expiry_date ) {

                $form->expiry_date = $form->expiry_date . " 23:59:59";
            }

            // dd($form->country_id);

            $form->country_id = implode (", ", $form->country_id);

            // dd($form->country_id);
            $url = url()->previous();

            if ( strpos($url, 'create') !== false ) {

                $last_action_code = ActionCode::max('id');
                $last_action_code += 1;

                $form->action_code_id = $last_action_code;

                $action_code = Utils::case_gen( $last_action_code );

                $code = ActionCode::create( [
                    'case_number'             => $action_code,
                    'action_code_type_id'     => 2,
                    'action_code_campaign_id' => 0,
                ] );
            }

        } );

        return $form;
    }


    // test gen 1000 codes
    public function store(Request $request)
    {
        
        $theForm;

        // dd('sote');
        $theForm = $this->form()->store();

        return $theForm;
    } 
}
