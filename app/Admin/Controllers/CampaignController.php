<?php

namespace App\Admin\Controllers;

use App\ActionCodeCampaign;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

use App\Country;

class CampaignController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ActionCodeCampaign);
        $grid->model()->orderBy('id', 'desc');

        $grid->id('Id');
        $grid->name('File Name');
        $grid->count('Count');
        $grid->register_country_code('Invite to Register');
        $grid->priority('Priority Claim');
        $grid->cron('System Cron');
        // $grid->header_content('Header Content');
        // $grid->footer_content('Footer Content');
        $grid->created_at('Created at');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ActionCodeCampaign::findOrFail($id));

        $show->id('Id');
        $show->name('File Name');
        $show->count('Count');
        $show->header_content('Header Content');
        $show->footer_content('Footer Content');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ActionCodeCampaign);

        $countries = Country::get();
        $countriesOptions = $countries->pluck('name', 'abbr');

        $form->text('name', 'File Name');

        $form->select('register_country_code', 'Invite to Register')->options($countriesOptions)
                        ->help('Select which country to register');

        $form->select('priority', 'Priority Claim')->options([
            'yes' => 'Yes',
            'no' => 'No'
        ]);

        $form->select('cron', 'Auto create events')->options([
            'yes' => 'Yes',
            'no' => 'No',
            'done' => 'Done'
        ])->default('no');

        $form->ckeditor('header_content', 'Header Content')
                        ->options(['height' => 200])
                        ->help('The 1st paragraph is set by default to color red, centered, and, uppercased.');
        $form->ckeditor('footer_content', 'Footer Content')
                        ->options(['height' => 200])
                        ->help('Variables to be fillout by the system: 
                                <ol>
                                    <li>__trademarkName__</li>
                                    <li>__filingDate__</li>
                                    <li>__filingDatePlus6__</li>
                                    <li>__country__</li>
                                </ol>');

        return $form;
    }
}
