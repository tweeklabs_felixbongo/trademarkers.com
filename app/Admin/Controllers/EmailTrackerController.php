<?php

namespace App\Admin\Controllers;

use App\DomainEventEmail;
use App\BrandDomain;
use App\Event;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Illuminate\Support\Facades\DB;
use Encore\Admin\Form;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Carbon\Carbon;
use Encore\Admin\Grid\Column;
use App\Repositories\MandrillMailer;
use Encore\Admin\Auth\Database\Role;
use Encore\Admin\Auth\Database\Administrator;
Use Encore\Admin\Widgets\Table;
use Illuminate\Http\Request;

class EmailTrackerController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Email List')
            ->description('List of Event Domain Emails')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new DomainEventEmail);

        $grid->disableCreation();
        $grid->disableExport();

        $grid->actions(function ($actions) {
            $actions->disableDelete();
            $actions->disableEdit();
            $actions->disableView();
        });

        $grid->filter(function($filter){
            $filter->like('email', 'Email');
            $filter->like('status', 'Status');
        });

        $grid->model()->orderBy('status', 'desc');

        // $grid->user_id('User ID');
        $grid->column('brand_domain_id', 'Domain/Events')->display( function () {
            // return $this->brand_domain_id;
            if ( $this->brand_domain_id ){
                $domain = BrandDomain::find($this->brand_domain_id);
                if ( $domain )
                return $domain->domain;

                return 'N/A';
            } elseif ( $this->event_id ) {
                $event = Event::find($this->event_id);
                if ( $event )
                return $event->type->type;

                return 'N/A';
            } else {
                return 'N/A';
            }
        });
        $grid->email('Email');
        $grid->status('Email Status')->label([
            'pending' => 'default',
            'sent' => 'warning',
            'read' => 'success'
        ]);

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {

        // admin_toastr('update success');
        // return redirect("/admin/manage/events/".$id."/edit");

        // $show = new Show(Event::findOrFail($id));

        // $show->country_id('Country')->as(function($countryId){
        //     $country = Country::find($countryId);
        //     // dd($country);
        //     return $country->name ? $country->name : 'N/A';
        // });


        // $show->notes('Notes');


        // // dd(0);

        // return $show;
    }

    
    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Event);

         
        

        return $form;
    }



}
