<?php

namespace App\Admin\Controllers;

use App\Trademark;
use App\Icomment;
use App\Profile;
use App\Country;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Illuminate\Support\Facades\DB;
use Encore\Admin\Form;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Carbon\Carbon;
use Encore\Admin\Grid\Column;
use App\Repositories\MandrillMailer;
use App\Repositories\MandrillMailerSend;
use Encore\Admin\Auth\Database\Role;
use Encore\Admin\Auth\Database\Administrator;

class TrademarkController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Trademark);
        
        if ( Admin::user()->isRole('paralegal-v2') && !Admin::user()->isRole('admin') ) {

            $grid->model()->where('paralegal_id', Admin::user()->id );
        }

        if ( Admin::user()->isRole('account-executive') && !Admin::user()->isRole('admin') ) {

            $grid->model()->where('rep_id', Admin::user()->id );
        }

        if ( Admin::user()->isRole('attorney') ) {

            $grid->model()->where('atty_id', Admin::user()->id );
        }
        
        $grid->model()->orderBy('created_at', 'desc');

        $grid->id('ID');
        $grid->column('created_at', 'Date')->display( function( $created_at ) {
            $date = Carbon::parse( $created_at );

            return $date->format('M d, Y');
        } );

        if ( !Admin::user()->isRole('attorney') ) {

            $grid->column('user.name', 'Owner')->display( function () {
                if ( !$this->user ) {
                    return 'N/A';
                } else {
                
                    $str = "<a href='#' data-toggle='modal' data-target='#modal". $this->id ."'>". $this->user->name .'</a>';

                    $str .= "<div class='modal fade' id='modal".$this->id."' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
                        <div class='modal-dialog' role='document'>
                            <div class='modal-content'>
                            <div class='modal-header'>
                                <h5 class='modal-title' id='exampleModalLabel'>Owner Information</h5>
                                <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                                </button>
                            </div>
                            <div class='modal-body'>
                                <div class='form-group'>
                                    <label for='fullname'>User ID</label>
                                    <input type='text' class='form-control' id='fullname' value=". $this->user->id ." disabled>
                                </div>
                                <div class='form-group'>
                                    <label for='fullname'>Full Name</label>
                                    <input type='text' class='form-control' id='fullname' value=". $this->user->name ." disabled>
                                </div>
                                <div class='form-group'>
                                    <label for='email'>Email Address</label>
                                    <input type='text' class='form-control' id='email' value=". $this->user->email ." disabled>
                                </div>
                            </div>
                            <div class='modal-footer'>
                                <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                            </div>
                            </div>
                        </div>
                        </div>";
                    return $str;
                }
            } );

            $grid->column('user.email', 'Profile')->display( function () {

                $title = "";
                $str   = "";

                if ( $this->profile ) {

                    if ( $this->profile->company ) {

                        $title = $this->profile->company;

                    } else {

                        $title = $this->profile->first_name . ' ' . $this->profile->last_name;
                    }

                    $str = "<a href='#' data-toggle='modal' data-target='#profile".$this->id."'>". $title .'</a>';

                    $str .= "<div class='modal fade' id='profile".$this->id."' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
                          <div class='modal-dialog' role='document'>
                            <div class='modal-content'>
                              <div class='modal-header'>
                                <h5 class='modal-title' id='exampleModalLabel'>Profile</h5>
                                <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                                  <span aria-hidden='true'>&times;</span>
                                </button>
                              </div>
                              <div class='modal-body'>
                                <div class='form-group'>
                                    <label for='firstname'>Profile ID</label>
                                    <input type='text' class='form-control' id='firstname' value='".$this->profile->id."' readonly>
                                </div>
                                <div class='form-group'>
                                    <label for='firstname'>First Name</label>
                                    <input type='text' class='form-control' id='firstname' value='".$this->profile->first_name."' readonly>
                                </div>
                                <div class='form-group'>
                                    <label for='lastname'>Last Name</label>
                                    <input type='text' class='form-control' id='lastname' value='".$this->profile->last_name."' readonly>
                                </div>
                                <div class='form-group'>
                                    <label for='nature'>Nature</label>
                                    <input type='text' class='form-control' id='nature' value='".$this->profile->nature."' readonly>
                                </div>
                                <div class='form-group'>
                                    <label for='company'>Company Name</label>
                                    <input type='text' class='form-control' id='company' value='".$this->profile->company."' readonly>
                                </div>
                                <div class='form-group'>
                                    <label for='country'>Country</label>
                                    <input type='text' class='form-control' id='country' value='".$this->profile->country."' readonly>
                                </div>
                                <div class='form-group'>
                                    <label for='nationality'>Nationality</label>
                                    <input type='text' class='form-control' id='nationality' value='".$this->profile->nationality."' readonly>
                                </div>
                                <div class='form-group'>
                                    <label for='phone'>Phone</label>
                                    <input type='text' class='form-control' id='phone' value='".$this->profile->phone_number."' readonly>
                                </div>
                                <div class='form-group'>
                                    <label for='email'>Email Address</label>
                                    <input type='text' class='form-control' id='email' value='".$this->profile->email."' readonly>
                                </div>
                                <div class='form-group'>
                                    <label for='house'>No/House/Apt</label>
                                    <input type='text' class='form-control' id='house' value='".$this->profile->house."' readonly>
                                </div>
                                <div class='form-group'>
                                    <label for='state'>Street</label>
                                    <input type='text' class='form-control' id='street' value='".$this->profile->street."' readonly>
                                </div>
                                <div class='form-group'>
                                    <label for='city'>City</label>
                                    <input type='text' class='form-control' id='city' value='".$this->profile->city."' readonly>
                                </div>
                                <div class='form-group'>
                                    <label for='state'>State</label>
                                    <input type='text' class='form-control' id='state' value='".$this->profile->state."' readonly>
                                </div>
                                <div class='form-group'>
                                    <label for='zip'>Zip Code</label>
                                    <input type='text' class='form-control' id='zip' value='".$this->profile->zip_code."' readonly>
                                </div>
                              </div>
                              <div class='modal-footer'>
                                <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                              </div>
                            </div>
                          </div>
                        </div>";
                }

                return $str;
            } );
        }

        $grid->service('Service');
        // $grid->type('Type');
        $grid->name('Name');

        // $grid->color_claim('Color Claim');

        $grid->column('Country')->display( function () {
           
            if ( !$this->country ) {

                return "N/A";
            }

            $avatar = $this->country->avatar;

            return "<img src='" . asset('/images/'.$avatar) . "'>" . ' ' . $this->country->name;
        });

        $grid->column('order.order_number', 'Order#');

        $grid->office_status('Status');

        $grid->office_deadline_date('Deadline Date')->display( function( $office_deadline_date ) {
            
            if ($office_deadline_date) {
                $date = Carbon::parse( $office_deadline_date );
                return $date->format('M d, Y');
            }
            return 'N/A';
        } );

        // if ( !(Admin::user()->isRole('administrator') || Admin::user()->isRole('admin') ) ) {
        //     $grid->disableCreateButton();
        // }

        $grid->actions(function ($actions) {

            $actions->disableDelete();

            if ( Admin::user()->inRoles(['csr']) ) {
                $actions->disableEdit();
                $actions->disableView();
            }
        });

        $grid->filter(function($filter){

            // Remove the default id filter
            $filter->disableIdFilter();

            
            

            $filter->scope('office_status')->where(function ($query) {
                $query->whereNotNull('office_status');
            });

            

            $filter->column(1/2, function ($filter) {

                $filter->like('Name', 'Word Mark');
                $filter->in('office_status')->multipleSelect([
                    'Pending'   => 'Pending',
                    'Filed'   => 'Filed',
                    'Received'   => 'Received',
                    'UnPaid'   => 'UnPaid',
                    'Under Examinations'   => 'Under Examinations',
                    'Under Opposition'   => 'Under Opposition',
                    'Under Cancellation'   => 'Under Cancellation',
                    'Published'   => 'Published',
                    'Study Completed'   => 'Study Completed',
                    'Registered' => 'Registered'
                ]);
                $filter->between('created_at', 'Date Filter')->datetime();
                $filter->between('office_deadline_date', 'Deadline Date Filter')->datetime();

            });

            $filter->column(1/2, function ($filter) {

                $aeUsers = Administrator::whereHas('roles',  function ($query) {
                    $query->where('name', ['account-executive']);
                })->get();
                $aeOptions = $aeUsers->pluck('name', 'id');

                $paralegalUsers = Administrator::whereHas('roles',  function ($query) {
                    $query->where('name', ['paralegal-v2']);
                })->get();
                $paralegalOptions = $paralegalUsers->pluck('name', 'id');

                $attyUsers = Administrator::whereHas('roles',  function ($query) {
                    $query->whereIn('name', ['attorney']);
                })->get();
                $attyOptions = $attyUsers->pluck('name', 'id');

                $filter->equal('user_id','Owners Email')->select()->ajax('/admin/api/user');
                $filter->equal('rep_id','AE')->select($aeOptions);
                $filter->equal('paralegal_id','Paralegal')->select($paralegalOptions);
                $filter->equal('atty_id','Attorney')->select($attyOptions);
                

            });


        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Trademark::findOrFail($id));

        $show->country_id('Country')->as(function($countryId){
            $country = Country::find($countryId);
            // dd($country);
            return $country->name ? $country->name : 'N/A';
        });
        $show->service('Service');
        $show->type('Type');
        $show->name('Name');
        $show->color_claim('Color Claim');
        $show->logo('Logo');
        $show->summary('Summary');
        $show->number('Number');
        $show->filing_number('Filing number');
        $show->registration_number('Registration number');
        $show->international_registration_number('International registration number');
        $show->filing_date('Filing date');
        $show->registration_date('Registration date');
        $show->expiry_date('Expiry date');
        $show->classes('Classes');
        $show->classes_description('Classes description');
        $show->office('Office');
        $show->office_status('Office status')->help('here');
        $show->status_description();
        $show->divider();
        $show->status_description()->as(function ($content) {
            return "<pre>{$content}</pre>";
        });
        $show->brand_name('Brand name');
        $show->purpose('Purpose');
        $show->value('Value');
        $show->category('Category');
        // $show->hvt('Hvt');
        $show->revocation('Revocation');
        $show->priority_date('Priority date');
        $show->priority_country('Priority country');
        $show->priority_number('Priority number');
        $show->recently_filed('Recently filed');
        $show->claim_priority('Claim priority');
        $show->commerce('Commerce');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    public function getAttorneys( Request $request )
    {
        $q = $request->get('q');

        return Admin::where('name', 'like', "%$q%")->paginate(null, ['id', 'name as text']);
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Trademark);


        $form->tab('Trademark Information', function( $form ) {

            $trademarkId = request()->segment(4); 

            $form->hidden('id');
            

            $countries = Country::get();
            $countriesOptions = $countries->pluck('name', 'id');
            
            if ( is_numeric($trademarkId) ) {
                $form->hidden('user.id');
                $form->number('profile_id');
                $form->select('country_id', 'Country')->options($countriesOptions);
                $form->display('service', 'Service');
                
            } else {
                
                $profiles = Profile::get();
                
                $profileOptions = $profiles->pluck('email', 'id');
                
                // dd($profileOptions);

                $form->select('country_id', 'Country')->options($countriesOptions);

                // $form->select('user_id', 'Owner')->ajax('/admin/api/user');
                $form->select('profile_id', 'Owner Profile')->options($profileOptions)->help("<i>Search Profile Email</i>");
                $form->select('service', 'Service')->options( 
                    [ "Trademark Study" => "Trademark Study", 
                     "Trademark Study [Priority]" => "Trademark Study [Priority]", 
                     "Trademark Registration" => "Trademark Registration", 
                     "Trademark Registration [Priority]" => "Trademark Registration [Priority]"
                    ]
                );
            }
            
            $form->select('type', 'Type')->options( 
                [ "Word-Only" => "Word-Only", 
                  "Design-Only or Stylized Word-Only (Figurative)" => "Design-Only or Stylized Word-Only (Figurative)",
                  "Combined Word and Design" => "Combined Word and Design"
                ]
            );
            $form->text('name', 'Name');
            $form->select('color_claim', 'Color Claim')->options( 
                [ "no" => "No", 
                  "yes" => "Yes"
                ]
            );
            $form->image('logo')->uniqueName();
            $form->textarea('summary', 'Summary');
            $form->text('number', 'Number');
            $form->text('filing_number', 'Filing number');
            $form->text('registration_number', 'Registration number');
            $form->number('international_registration_number', 'International registration number');
            $form->datetime('filing_date', 'Filing date');
            $form->datetime('registration_date', 'Registration date');
            $form->datetime('expiry_date', 'Expiry date');
            $form->text('classes', 'Classes');
            $form->textarea('classes_description', 'Classes description');
            $form->text('office', 'Office');

            $form->select('office_status','Status')->options([ 
                'Pending'            => 'Pending', 
                'Received'           => 'Received', 
                'Filed'              => 'Filed', 
                'Registered'         => 'Registered',
                'Under Examinations' => 'Under Examinations',
                'Published'          => 'Published',
                'Under Opposition'   => 'Under Opposition',
                'Registered'         => 'Registered',
                'Under Cancellation' => 'Under Cancellation',
                'For Renewal'        => 'For Renewal',
                'Study Completed'    => 'Study Completed',
                'Cancelled'          => 'Cancelled',
                'Withdrawn'          => 'Withdrawn',
                'Refused'            => 'Refused',
                'UnPaid'             => 'UnPaid',
                'Order Cancelled'    => 'Order Cancelled',
            ]);

            // get comments internal here and add it in help html
            
            // dd($trademarkId);
            if ( is_numeric($trademarkId) ) { 
                
            $helpComment = "";
            if ($trademarkId) {
                $trademark = Trademark::with(['icomments' => function ($q) {
                    $q->orderBy('id', 'DESC');
                }])
                ->findOrFail($trademarkId);  

                $helpComment = "<table class='table'>";
                $helpComment .= "<tr>";
                $helpComment .= "<th style='width:25%'>Date</th>";
                $helpComment .= "<th style='width:25%'>Edited By</th>";
                $helpComment .= "<th>Comment</th>";
                $helpComment .= "</tr>";
                foreach ($trademark->icomments as $icomment) {
   
                    $date = Carbon::parse( $icomment->updated_at );

                    $adminUser = Administrator::findOrFail($icomment->admin_user_id);
                    $helpComment .= "<tr>";
                    $helpComment .= "<td><i>{$date->format('M d, Y g:i a')}</i></td>";
                    $helpComment .= "<td><i>{$adminUser->name}</i></td>";
                    $helpComment .= "<td><pre>{$icomment->description}</pre></td>";
                    $helpComment .= "</tr>";
                }
                $helpComment .= "</table>";
            }

            $form->text('icomment', 'Comments')->placeholder('Add Comments')->help($helpComment);
            // $form->text('icomment', 'Comments')->placeholder('Add Comments');
            } else {
                $form->text('icomment', 'Comments')->placeholder('Add Comments');
            }

            $form->datetime('office_deadline_date', 'Office Deadline Date');
            $form->textarea('status_description', 'Status description');

            // $form->textarea('test comments')->value('asdasd');

            $form->text('brand_name', 'Brand name');
            $form->text('purpose', 'Purpose');
            $form->text('value', 'Value');
            $form->text('category', 'Category');
            // $form->switch('hvt', 'Hvt');
            $form->switch('revocation', 'Revocation');
            $form->text('priority_date', 'Priority date');
            $form->text('priority_country', 'Priority country');
            $form->text('priority_number', 'Priority number');
            $form->text('recently_filed', 'Recently filed');
            $form->text('claim_priority', 'Claim priority');
            $form->text('commerce', 'Commerce');
            
            if ( Admin::user()->inRoles(['administrator','admin','teamlead']) ) {
             
                $aeUsers = Administrator::whereHas('roles',  function ($query) {
                    $query->where('slug', ['account-executive']);
                })->get();
                $aeOptions = $aeUsers->pluck('name', 'id');

                $paralegalUsers = Administrator::whereHas('roles',  function ($query) {
                    $query->where('slug', ['paralegal-v2']);
                })->get();
                $paralegalOptions = $paralegalUsers->pluck('name', 'id');

                $attyUsers = Administrator::whereHas('roles',  function ($query) {
                    $query->whereIn('slug', ['attorney']);
                })->get();
                $attyOptions = $attyUsers->pluck('name', 'id');

                $form->select('rep_id', 'Account Executives')->options($aeOptions);
                $form->select('paralegal_id', 'Paralegal')->options($paralegalOptions);

                $form->hidden('rep_email')->default("");
                $form->hidden('atty_email')->default("");

                $attyOptions[21] = "Rexis (US)";
                    
                $form->select('atty_id', 'Attorneys')->options($attyOptions);
            }

        } );

        if ( Admin::user()->inRoles(['administrator','admin']) ) {  
            $form->tab('Profile', function ( $form ) {
                $form->display('profile.first_name', 'First Name');
                $form->display('profile.last_name', 'Last Name');
                $form->display('profile.nature', 'Nature');
                $form->display('profile.company', 'Company Name');
                $form->display('profile.phone_number', 'Phone Number');
                $form->display('profile.email', 'Email Address');
            } );
        }

        if ( Admin::user()->inRoles(['administrator','admin']) ) {

            $form->tab('Comments', function ( $form ) {
                $form->hasMany('icomments', '', function (Form\NestedForm $form) {
                    $form->hidden('admin_user_id')->default( Admin::user()->id );
                    $form->textarea('description', '')->rows(10);
                });

            } );
        }

        if ( Admin::user()->inRoles(['administrator','admin']) ) {

            $form->tab('Client Comments', function ( $form ) {

                if ( Admin::user()->inRoles(['paralegal', 'administrator','admin']) ) {

                    $form->hasMany('comments', '', function (Form\NestedForm $form) {
                        $form->hidden('user_id')->default(0);
                        $form->textarea('description', '')->rows(10);
                    });
                }

            } );
        }

        $form->tab('Attachments', function ( $form ) {

            $form->hasMany('attachments', function (Form\NestedForm $form) {
                $form->hidden('user_id')->default(0);
                $form->file('name', 'Name')->uniqueName();
                $form->text('description', 'Description')->default('File');
            });
        } );

        $form->tools(function (Form\Tools $tools) {
            // Disable `Delete` btn.
            $tools->disableDelete();
        });

        // callback before save
        if ( is_numeric(request()->segment(4)) ) {
        $form->saving( function ( Form $form ) {

            $id         = $form->id;
            $new_status = $form->office_status;

            $new_tm_name = $form->name;
            // dd($form->icomment);
            $trademark = Trademark::findOrFail($id);

            $internal_comments = count( $trademark->icomments );
            $client_comments   = count( $trademark->comments );

            $old_status  = $trademark->office_status;

            if ( $form->rep_id ) {

                if ( $trademark->rep_id != $form->rep_id ) {
                   
                   $result = DB::table('admin_users')->where('id', $form->rep_id)->get();

                    $form->rep_email = $result[0]->username;

                    MandrillMailerSend::assign_trademark( $trademark, $result[0] );
                }
                
            }

            if ( $form->paralegal_id ) {

                if ( $trademark->paralegal_id != $form->paralegal_id ) {
                   
                   $result = DB::table('admin_users')->where('id', $form->paralegal_id)->get();
                    // dd($result);
                    $form->paralegal_email = $result[0]->username;

                    MandrillMailerSend::assign_trademark( $trademark, $result[0] );
                }
                
            }

            if ( $form->atty_id ) {

                if ( $trademark->atty_id != $form->atty_id) {

                    $result = DB::table('admin_users')->where('id', $form->atty_id)->get();

                    $form->atty_email = $result[0]->username;

                    MandrillMailerSend::assign_trademark( $trademark, $result[0] );
                }
            }

            if ( $old_status != $new_status ) {

                // MandrillMailerSend::trademark_new_status( $trademark, $new_status );
            }

            // COMMENT OUT SINCE THIS WASN'T USED
            // if ( $form->icomments ) {

            //     if ( isset( $form->icomments['new_1'] ) ) {

            //         MandrillMailer::trademark_new_comment( $trademark, Admin::user(), true, true );   
            //     }
                
            // }
            // NEW INTERNAL COMMENT
            // dd($form->icomment);
            if ( $form->icomment ){ 
                $icomment = new Icomment;

                $icomment->admin_user_id = Admin::user()->id;
                $icomment->trademark_id = $form->id;
                $icomment->description = $form->icomment;
                
                $icomment->save();
                // dd('save2');
                $form->icomment = null;
            }
            // dd('wla');

            if ( $form->comments ) {

                if ( isset( $form->comments['new_1'] ) ) {
                    
                    MandrillMailerSend::trademark_new_comment( $trademark, Admin::user(), true, false );   
                }  
            }
        });
        } 
        
        return $form;
    }


    public function store()
    {
        // $allContent = $request->all();
        // dd($allContent);
        /**
         * Create trademark and generate action code
         */

        
        // $this->form->ignore(['icomment']);

        $theForm = $this->form()->store();
        // dd($theForm);

        $trdId = Trademark::max('id');

        $trademark = Trademark::with('profile')->findOrFail($trdId);

        if ( $trademark && !$trademark->user_id ) {
           
            $trademark->user_id = $trademark->profile->user_id;
            $trademark->save();
        }

        

        return $theForm;
    }
}
