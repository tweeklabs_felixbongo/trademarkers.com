<?php

namespace App\Admin\Controllers;

use App\Trademark;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class AdminTrademarkController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Trademark);

        $grid->id('Id');
        $grid->user_id('User id');
        $grid->country_id('Country id');
        $grid->order_id('Order id');
        $grid->profile_id('Profile id');
        $grid->euipo_id('Euipo id');
        $grid->service('Service');
        $grid->type('Type');
        $grid->name('Name');
        $grid->logo('Logo');
        $grid->amount('Amount');
        $grid->created_at('Created at');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Trademark::findOrFail($id));

        $show->id('Id');
        $show->user_id('User id');
        $show->case_number('Case number');
        $show->country_id('Country id');
        $show->order_id('Order id');
        $show->profile_id('Profile id');
        $show->euipo_id('Euipo id');
        $show->service('Service');
        $show->type('Type');
        $show->name('Name');
        $show->logo('Logo');
        $show->logo_description('Logo description');
        $show->summary('Summary');
        $show->filing_date('Filing date');
        $show->filing_number('Filing number');
        $show->app_reference('App reference');
        $show->atty_notes('Atty notes');
        $show->rep_notes('Rep notes');
        $show->client_notes('Client notes');
        $show->registration_date('Registration date');
        $show->expiry_date('Expiry date');
        $show->classes('Classes');
        $show->classes_description('Classes description');
        $show->office('Office');
        $show->office_status('Office status');
        $show->status_description('Status description');
        $show->brand_name('Brand name');
        $show->purpose('Purpose');
        $show->value('Value');
        $show->category('Category');
        $show->hvt('Hvt');
        $show->revocation('Revocation');
        $show->priority_date('Priority date');
        $show->priority_country('Priority country');
        $show->priority_number('Priority number');
        $show->recently_filed('Recently filed');
        $show->claim_priority('Claim priority');
        $show->commerce('Commerce');
        $show->amount('Amount');
        $show->discount('Discount');
        $show->atty_id('Atty id');
        $show->rep_id('Rep id');
        $show->atty_email('Atty email');
        $show->rep_email('Rep email');
        $show->rep_phone('Rep phone');
        $show->rep_fax('Rep fax');
        $show->number('Number');
        $show->relative_trademark_id('Relative trademark id');
        $show->registration_number('Registration number');
        $show->international_registration_number('International registration number');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Trademark);

        $form->number('user_id', 'User id');
        $form->text('case_number', 'Case number');
        $form->number('country_id', 'Country id');
        $form->number('order_id', 'Order id');
        $form->number('profile_id', 'Profile id');
        $form->text('euipo_id', 'Euipo id');
        $form->text('service', 'Service');
        $form->text('type', 'Type');
        $form->text('name', 'Name');
        $form->text('logo', 'Logo');
        $form->text('logo_description', 'Logo description');
        $form->textarea('summary', 'Summary');
        $form->datetime('filing_date', 'Filing date')->default(date('Y-m-d H:i:s'));
        $form->textarea('filing_number', 'Filing number');
        $form->text('app_reference', 'App reference');
        $form->textarea('atty_notes', 'Atty notes');
        $form->textarea('rep_notes', 'Rep notes');
        $form->textarea('client_notes', 'Client notes');
        $form->datetime('registration_date', 'Registration date')->default(date('Y-m-d H:i:s'));
        $form->datetime('expiry_date', 'Expiry date')->default(date('Y-m-d H:i:s'));
        $form->text('classes', 'Classes');
        $form->textarea('classes_description', 'Classes description');
        $form->text('office', 'Office');
        $form->text('office_status', 'Office status');
        $form->textarea('status_description', 'Status description');
        $form->text('brand_name', 'Brand name');
        $form->text('purpose', 'Purpose');
        $form->text('value', 'Value');
        $form->text('category', 'Category');
        $form->switch('hvt', 'Hvt');
        $form->switch('revocation', 'Revocation');
        $form->text('priority_date', 'Priority date');
        $form->text('priority_country', 'Priority country');
        $form->text('priority_number', 'Priority number');
        $form->text('recently_filed', 'Recently filed');
        $form->text('claim_priority', 'Claim priority');
        $form->text('commerce', 'Commerce');
        $form->decimal('amount', 'Amount');
        $form->text('discount', 'Discount');
        $form->number('atty_id', 'Atty id');
        $form->number('rep_id', 'Rep id');
        $form->text('atty_email', 'Atty email');
        $form->text('rep_email', 'Rep email');
        $form->text('rep_phone', 'Rep phone');
        $form->text('rep_fax', 'Rep fax');
        $form->number('number', 'Number');
        $form->number('relative_trademark_id', 'Relative trademark id');
        $form->number('registration_number', 'Registration number');
        $form->number('international_registration_number', 'International registration number');
        $form->datetime('created_at', 'Created date');

        return $form;
    }
}
