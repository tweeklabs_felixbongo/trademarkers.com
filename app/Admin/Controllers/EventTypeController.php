<?php

namespace App\Admin\Controllers;

// use App\Country;
// use App\Brand;
use App\EventType;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Illuminate\Support\Facades\DB;
use Encore\Admin\Form;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Carbon\Carbon;
use Encore\Admin\Grid\Column;
use App\Repositories\MandrillMailer;
use Encore\Admin\Auth\Database\Role;
use Encore\Admin\Auth\Database\Administrator;
Use Encore\Admin\Widgets\Table;

class EventTypeController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new EventType);

        $grid->model()->orderBy('created_at', 'desc');

        $grid->type('Event Type');
        $grid->slug('Event slug');
        $grid->email_subject('Email Subject')->editable();
        $grid->notes('Notes')->editable();

        $grid->event_show('Show In Creating Event')->editable('select',['yes' => 'Show', 'no' => 'Hide']);

        $grid->created_at('Date Added');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(EventType::findOrFail($id));

        // $show->country_id('Country')->as(function($countryId){
        //     $country = Country::find($countryId);
        //     // dd($country);
        //     return $country->name ? $country->name : 'N/A';
        // });

        // $show->filling_number('Filling Number');
        // $show->opposition_period_start('Opposition Start');
        // $show->opposition_period_end('Opposition End');
        // $show->brand('Brand');
        // $show->affected_brand('Affected Brand');
        // $show->notes('Notes');


        

        return $show;
    }

    
    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new EventType);

        $form->text('slug', 'Slug')->disable();
        $form->text('type', 'Event Type');
        $form->text('email_subject', 'Email Subject');
        $form->textarea('notes', 'Notes');

        // $eventTypes = EventType::get();

        $form->select('event_show','Show this in Event')->options([
            'yes' => 'Show',
            'no' => 'Hide',
        ])->default('yes');

        $form->saving( function ( Form $form ) {
            $slug = str_replace(' ', '-', $form->type);

            $form->slug = strtolower($slug);
        });

        return $form;
    }

}
