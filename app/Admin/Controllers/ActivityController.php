<?php

namespace App\Admin\Controllers;

use App\Activity;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ActivityController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Activity);

        $grid->id('Id');

        $grid->model()->orderBy('created_at', 'desc');

        $grid->column('user_id', 'User Details')->display( function () {

            if ( !$this->user ) {

                return $str = "Guest";
            }   

            $str = "<a href='#' data-toggle='modal' data-target='#modal". $this->user->id ."'>". $this->user->name .'</a>';

            $str .= "<div class='modal fade' id='modal".$this->user->id."' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
                  <div class='modal-dialog' role='document'>
                    <div class='modal-content'>
                      <div class='modal-header'>
                        <h5 class='modal-title' id='exampleModalLabel'>Owner Information</h5>
                        <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                          <span aria-hidden='true'>&times;</span>
                        </button>
                      </div>
                      <div class='modal-body'>
                        <div class='form-group'>
                            <label for='fullname'>User ID</label>
                            <input type='text' class='form-control' id='fullname' value=". $this->user->id ." disabled>
                        </div>
                        <div class='form-group'>
                            <label for='fullname'>Full Name</label>
                            <input type='text' class='form-control' id='fullname' value=". $this->user->name ." disabled>
                        </div>
                        <div class='form-group'>
                            <label for='email'>Email Address</label>
                            <input type='text' class='form-control' id='email' value=". $this->user->email ." disabled>
                        </div>
                      </div>
                      <div class='modal-footer'>
                        <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
                      </div>
                    </div>
                  </div>
                </div>";
            return $str;
        } );
        $grid->ip_address('IP Address');
        $grid->description('Description');
        $grid->created_at('Created at');

        $grid->filter(function($filter){

            // Remove the default id filter
            $filter->disableIdFilter();

            $filter->like('description', 'Description');

        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Activity::findOrFail($id));

        $show->id('Id');
        $show->user_id('User id');
        $show->module('Module');
        $show->description('Description');
        $show->ip_address('Ip address');
        $show->browser('Browser');
        $show->os('Os');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Activity);

        $form->number('user_id', 'User id');
        $form->text('module', 'Module');
        $form->textarea('description', 'Description');
        $form->text('ip_address', 'Ip address');
        $form->text('browser', 'Browser');
        $form->text('os', 'Os');

        return $form;
    }
}
