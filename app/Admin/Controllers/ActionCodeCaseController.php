<?php

namespace App\Admin\Controllers;

use App\ActionCodeCase;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ActionCodeCaseController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ActionCodeCase);

        $grid->id('Id');
        $grid->action_code_id('Action code id');
        $grid->action_code_campaign_id('Action code campaign id');
        $grid->number('Number');
        $grid->trademark('Trademark');
        $grid->tm_filing_date('Tm filing date');
        $grid->tm_holder('Tm holder');
        $grid->application_reference('Application reference');
        $grid->mark_current_status_code('Mark current status code');
        $grid->mark_current_status_date('Mark current status date');
        $grid->ref_processed_date('Ref processed date');
        $grid->registration_date('Registration date');
        $grid->address('Address');
        $grid->city('City');
        $grid->city_of_registration('City of registration');
        $grid->state('State');
        $grid->class_number('Class number');
        $grid->class_description('Class description');
        $grid->representatives('Representatives');
        $grid->email('Email');
        $grid->fax_no('Fax no');
        $grid->telephone_no('Telephone no');
        $grid->website('Website');
        $grid->zip('Zip');
        $grid->country('Country');
        $grid->country_code('Country code');
        $grid->discount('Discount');
        $grid->discount_expiry_date('Discount expiry date');
        $grid->expiry_date('Expiry date');
        $grid->created_at('Created at');
        $grid->updated_at('Updated at');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ActionCodeCase::findOrFail($id));

        $show->id('Id');
        $show->action_code_id('Action code id');
        $show->action_code_campaign_id('Action code campaign id');
        $show->number('Number');
        $show->trademark('Trademark');
        $show->tm_filing_date('Tm filing date');
        $show->tm_holder('Tm holder');
        $show->application_reference('Application reference');
        $show->mark_current_status_code('Mark current status code');
        $show->mark_current_status_date('Mark current status date');
        $show->ref_processed_date('Ref processed date');
        $show->registration_date('Registration date');
        $show->address('Address');
        $show->city('City');
        $show->city_of_registration('City of registration');
        $show->state('State');
        $show->class_number('Class number');
        $show->class_description('Class description');
        $show->representatives('Representatives');
        $show->email('Email');
        $show->fax_no('Fax no');
        $show->telephone_no('Telephone no');
        $show->website('Website');
        $show->zip('Zip');
        $show->country('Country');
        $show->country_code('Country code');
        $show->discount('Discount');
        $show->discount_expiry_date('Discount expiry date');
        $show->expiry_date('Expiry date');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ActionCodeCase);

        $form->number('action_code_id', 'Action code id');
        $form->number('action_code_campaign_id', 'Action code campaign id');
        $form->text('number', 'Number');
        $form->textarea('trademark', 'Trademark');
        $form->datetime('tm_filing_date', 'Tm filing date')->default(date('Y-m-d H:i:s'));
        $form->textarea('tm_holder', 'Tm holder');
        $form->text('application_reference', 'Application reference');
        $form->text('mark_current_status_code', 'Mark current status code');
        $form->datetime('mark_current_status_date', 'Mark current status date')->default(date('Y-m-d H:i:s'));
        $form->datetime('ref_processed_date', 'Ref processed date')->default(date('Y-m-d H:i:s'));
        $form->datetime('registration_date', 'Registration date')->default(date('Y-m-d H:i:s'));
        $form->textarea('address', 'Address');
        $form->text('city', 'City');
        $form->text('city_of_registration', 'City of registration');
        $form->text('state', 'State');
        $form->text('class_number', 'Class number');
        $form->textarea('class_description', 'Class description');
        $form->text('representatives', 'Representatives');
        $form->email('email', 'Email');
        $form->text('fax_no', 'Fax no');
        $form->text('telephone_no', 'Telephone no');
        $form->text('website', 'Website');
        $form->text('zip', 'Zip');
        $form->text('country', 'Country');
        $form->text('country_code', 'Country code');
        $form->decimal('discount', 'Discount');
        $form->datetime('discount_expiry_date', 'Discount expiry date')->default(date('Y-m-d H:i:s'));
        $form->datetime('expiry_date', 'Expiry date')->default(date('Y-m-d H:i:s'));

        return $form;
    }
}
