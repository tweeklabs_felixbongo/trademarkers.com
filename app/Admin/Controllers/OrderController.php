<?php

namespace App\Admin\Controllers;

use App\Order;
use App\Cart;
use App\Trademark;
use App\CartCoupon;
use App\ActionCodePromo;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

use App\Admin\Extensions\OrdersExporter;
use App\Admin\Extensions\ResendInvoiceExtension;







class OrderController extends Controller
{
    use HasResourceActions;

    public $cartService;

    // constructor
    public function __construct(
        \App\Services\CartService $cartService
    ) {
        $this->cartService = $cartService;
        
    }

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Order);
        $grid->exporter(new OrdersExporter());
        
        $grid->model()->orderBy('created_at', 'desc');
        $grid->order_number('Order number');
        // $grid->column('user.email', "Owner's Email");
        $grid->column('user.name', 'Owner');
        $grid->column('invoice.reference','Invoice #');
        $grid->total_items('Total items');
        $grid->total_amount('Total amount');
        $grid->created_at('Created at');

        $grid->column('Discounts')->display( function() {

            
            
            $cartCoupons = CartCoupon::where('order_id', $this->id)->get();
            $str = "";
            $code = "";
            $discount = 0;

            if ( count($cartCoupons) > 0 ) {
                
                foreach ($cartCoupons as $cartCoupon) {
                    $promoCode = ActionCodePromo::find( $cartCoupon->promo_code_id );
                    // $str .= "Code : ".$promoCode->action_code->case_number . "<br>";
                    $cart = Cart::find($cartCoupon->cart_id);
                    $discount += $cart->total_discount;
                    if ($promoCode->action_code->case_number) {
                        $code = $promoCode->action_code->case_number;
                    }
                }
                $str = "Code : " . $code . "<br>Discount : " . "$" . number_format($discount,2);

            } else {
                $carts = Cart::where('order_id', $this->id)->get();

                if ( count($carts) > 0 ) {
                    foreach ($carts as $cart) {
                        // Campaign
                        if ( $cart->campaign_id ) {
                            $discount += $cart->total_discount;
                            $str .= "Campaign Discount : " . "$" . $cart->total_discount . "<br>";
                        }
                    }

                    if ($discount == 0) {
                        $str = "Normal Transaction Discount $0";
                    }

                } else {
                    $trademarks = Trademark::where('order_id', $this->id)->get();

                    foreach ($trademarks as $trademark) {
                        $cart = Cart::where([
                            ['user_id', $trademark->user_id],
                            ['profile_id', $trademark->profile_id],
                            ['name', $trademark->name],
                            ['service', $trademark->service],
                            ['type', $trademark->type],
                            ['amount', $trademark->amount]
                        ])->first();

                        if ($cart) {
                            if ( $cart->campaign_id ) {
                                $discount += $cart->total_discount;
                                $str .= "Campaign Discount : " . "$" . $cart->total_discount . "<br>";
                            } else if( count($cart->coupons) > 0 ){
                                $str .= "Coupon Discount : " . "$" . $cart->total_discount . "<br>";
                            } else if ( $cart->total_discount > 0 ){
                                $str .= "unknown discount : " . "$" . $cart->total_discount . " - Cart ID: ". $cart->id . "<br>";
                            } else {
                                $str .= "No Discount - Cart ID: ". $cart->id . "<br>";
                            }
                        } else {
                            $str .= "<i>Can't track cart transactions</i><br>";
                        }
                    }
                    // $str .= "<i>No Discount Found</i>";
                }

            }

            
            return $str;
        });

        // $grid->column('invoice.status','Office Status');
        $grid->column('trademarks.office_status','Office Status')->display( function() {
            
            // $str = "<a href='#' data-toggle='modal' data-target='#modal-transaction-". $this->id ."'>". 'View Office Transactions' .'</a>';
            $str = 'N/A';
            // $str .= "<table class='table'>";
            
            // $str .= "<tr>
            //             <th>Service</th>
            //             <th>Amount</th>
            //             <th>Status</th>
            //         </tr>";

            foreach ( $this->trademarks as $item ) {
              $str = $item->office_status == 'UnPaid' ? 'Unpaid' : 'Paid';
                // $str .= "<tr>
                //             <td>{$item->service}</td>
                //             <td>{$item->amount}</td>
                //             <td>{$payment}</td>
                //         </tr>";
            
            }
                      
            // $str .=     "</table>";

            return $str;
        });

        $grid->actions(function ($actions) {
            $actions->disableDelete();
            $actions->append(new ResendInvoiceExtension($actions->getKey()));
        });
        

        $grid->filter(function($filter){

            // Remove the default id filter
            $filter->disableIdFilter();
            $filter->equal('user_id','Owners Name')->select()->ajax('/admin/api/user/name');
            $filter->like('order_number', 'Order Number');

        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Order::findOrFail($id));

        $show->id('Id');
        $show->order_number('Order number');
        $show->total_items('Total items');
        $show->total_amount('Total amount');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Order);

        $form->tab('Order Information', function ( $form ) {
            // $orderId = request()->segment(4);
            
            // $form->hidden('id', 'ID');
            $form->display('order_number', 'Order number');
            $form->number('total_items', 'Total items');
            $form->decimal('total_amount', 'Total amount');

            // dd($orderId);
            // if ( is_numeric($orderId) ) {
            //     dd(1);
            // }
        } );

        $form->tab('Owner Information', function ( $form ) {
            $form->display('user.name', 'Owner Name');
            $form->display('user.email', 'Owner Email');
        } );

        $form->tab('Payment Information', function ( $form ) {
            $form->display('payment.reference_no', 'Payment Reference');
            $form->display('payment.pay_type', 'Payment Method');
            $form->display('payment.gateway', 'Payment Gateway');
            $form->display('payment.card_type', 'Card Type');
            $form->display('payment.amount', 'Amount');
        } );

        $form->tab('Invoice Information', function ( $form ) {
            $form->display('invoice.reference', 'Invoice Reference');
            $form->display('invoice.name', 'Invoice Name');
            $form->display('invoice.status', 'Invoice Status');
        } );

        $form->tools(function (Form\Tools $tools) {
            // Disable `Delete` btn.
            $tools->disableDelete();
        });

        return $form;
    }

    // API Function
    public function resendInvoice($id)
    {
        $mesage;
        $order = Order::with('items')->find($id);

        if (count($order->items) > 0) {
            $this->cartService->sendNewOrder($order, $order->items);
            $mesage = [
                'type' => 'success',
                'message' => 'Success, Email Sent! ' . $order->order_number,
            ];
        } else {
            $mesage = [
                'type' => 'warning',
                'message' => 'Sorry, Something went wrong.' . $order->order_number,
            ];
        }

        return $mesage;
    }
}
