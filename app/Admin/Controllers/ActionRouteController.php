<?php

namespace App\Admin\Controllers;

use App\ActionCodePromo;
use App\ActionRoute;
use App\ActionCode;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Http\Request;

use App\Repositories\Utils;

class ActionRouteController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ActionRoute);
        $grid->model()->orderBy('id', 'desc');

        $grid->id('Id');
        

        $grid->column('related_action_id', 'Promo Code')->display(function($relatedId){
            $actionC = ActionCodePromo::find($relatedId);
            return ($actionC ? $actionC->action_code->case_number : 'N/A');
        });


        $grid->column('url', 'Redirects To')->display( function ($url){
            return 'https://trademarkers.com/'.$url;
        });


        $grid->column('action.case_number', 'Generated URL')->display( function ($caseNumber){
            return 'https://trademarkers.com/'.$caseNumber;
        });

        $grid->notes('Description');

        


        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ActionRoute::findOrFail($id));

        $show->id('Id');
        

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ActionRoute);

        $form->hidden('action_code_id');
        $form->text('url')->prepend('<i>https://trademarkers.com/</i>');

        $actionPromoCode = ActionCodePromo::get();
        $actionPromoCodeOptions = $actionPromoCode->pluck('promo_code', 'id');

        $form->select('related_action_id', 'Promo Code')->options($actionPromoCodeOptions);

        $form->textarea('notes');

        $form->saving( function ( Form $form ) {

            if (!$form->action_code_id) {

                $last_action_code = ActionCode::max('id');

                $last_action_code++;
                $actionC = Utils::case_gen( $last_action_code );

                $code = ActionCode::create( [
                    'case_number'             => $actionC,
                    'action_code_type_id'     => 12,
                    'action_code_campaign_id' => 0,
                ] );

                $form->action_code_id = $code->id;

            }

        });

        return $form;
    }

    public function getPromo( Request $request ) {
        $q = $request->get('q');

        // $countries = ActionCode::get();
        // $countriesOptions = $countries->pluck('name', 'id');

        $action = ActionCodePromo::where([
            ['promo_code', 'like', "%$q%"]
        ])->paginate(null, ['id', 'promo_code as text']);

        // $actionPluck = $action->pluck('id');
        //     dd($actionPluck);
        // $promos = ActionCodePromo::find($actionPluck)->paginate(null, ['id', 'promo_code as text']);

        // dd(ActionCodePromo::find('1069')->promo_code);


        return $action;

    }

    
}
