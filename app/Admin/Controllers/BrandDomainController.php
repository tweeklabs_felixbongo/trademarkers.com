<?php

namespace App\Admin\Controllers;

use App\Country;
use App\Brand;
use App\BrandDomain;
use App\DomainEventEmail;
use App\Event;
use App\EventType;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Illuminate\Support\Facades\DB;
use Encore\Admin\Form;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Carbon\Carbon;
use Encore\Admin\Grid\Column;
use App\Repositories\MandrillMailer;
use Encore\Admin\Auth\Database\Role;
use Encore\Admin\Auth\Database\Administrator;
Use Encore\Admin\Widgets\Table;

class BrandDomainController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    // public function index(Content $content)
    // {	
    //     $brands = Brand::all();
    //     // $arr = [];

    //     return $content
    //         ->header('Title')
    //         ->description('Description')
    //         ->body(view('admin.domain.index', compact('brands')));
    // }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new BrandDomain);

        // filter if opposition researcher
        // if ( Admin::user()->isRole('opposition-researcher') ) {

        //     $grid->model()->where('user_id', Admin::user()->id );
        // }
        
        
        // $grid->model()->orderBy('created_at', 'desc');

        // if ( Admin::user()->inRoles(['event-researcher']) ) {
        //     $ids = Brand::where('assigned_user_id', Admin::user()->id)->get()->pluck('id');
        //     // dd($domains);
        //     $grid->model()->whereIn('brand_id', $ids);
        // }

        $grid->model()->orderBy('created_at', 'desc');
    
        $grid->column('brand.brand', 'Brand');
        $grid->domain('Domain');

        $grid->user_id('Added/Updated')->display(function() {
            if ($this->user && $this->user->username) {
                return $this->user->name;
            } 

            return 'System Generated';
            
        });

        $grid->researcher_id('Researcher')->display(function() {
            if ($this->researcher && $this->researcher->name) {
                return $this->user->name;
            } 

            return 'N/A';
            
        });

        $grid->column('email', 'Email')->expand(function () {

            $headers = [ 
                'email', 
                'notes', 
                'status'
            ];
            
            $rows = $this->emails->map(function ($emails) {
                return $emails->only(['email', 'notes', 'status']);
            });

            // dd($rows);
            // $rows = $this->emails
            //         ->pluck(
            //             'email',
            //             'notes',
            //             'status'
            //             )
            //         ->toArray();

                        // dd($rows);
            $table = new Table($headers, $rows->toArray());

            return $table;
        });

        // if ( Admin::user()->inRoles(['event-researcher','brand-researcher']) ) {  
        //     // $grid->disableCreation();
        //     $grid->disableExport();
        // }
        

        $grid->actions(function ($actions) {
            // if ( Admin::user()->inRoles(['brand-researcher']) ) {  
                
            //     $actions->disableDelete();
            //     $actions->disableView();
            // }

            // $actions->disableDelete();
        });

        

        // $grid->assigned_user_id('Assign to me')->editable();

        $grid->filter(function($filter){

            // Remove the default id filter
            $filter->disableIdFilter();

            $filter->like('domain', 'Domain');
        });


        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(BrandDomain::findOrFail($id));

        
        $show->brand_id('Brand');
        $show->domain('Domain');
        $show->notes('Notes');

        $show->emails('Emails', function ($email) {

            // $comments->resource('/admin/comments');
        
            // $email->id();
            $email->email();
            $email->notes();
            $email->status();
        
            $email->filter(function ($filter) {
                $filter->like('email');
            });

            $email->actions(function ($actions) {
                $actions->disableDelete();
                $actions->disableEdit();
                $actions->disableView();
            });

            $email->disableCreation();
            $email->disableExport();
        });

        return $show;
    }

    
    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new BrandDomain);

        
        
        $form->tab('Domain Information', function( $form ) {

            $countries = Country::get();
            $countriesOptions = $countries->pluck('name', 'id');

            


            $form->hidden('user_id')->default(Admin::user()->id);

            $researcherUsers = Administrator::whereHas('roles',  function ($query) {
                $query->where('slug', ['event-researcher']);
            })->get();
            $researcherOptions = $researcherUsers->pluck('name', 'id');

            $form->select('researcher_id', 'Researcher')->options($researcherOptions);
            

            // $form->hidden('assigned_user_id')->default(Admin::user()->id);
            // $form->select('country_id', 'Country')->options($countriesOptions);
            // $form->text('domain', 'domain');
            // $form->text('first_name', 'Owner First Name');
            // $form->text('last_name', 'Owner Last Name');
            // $form->email('admin_email', 'Admin/Owner Email');
            // $form->text('postal_address', 'Postal Address');
            // $form->date('date_registered', 'Date Registered');
            // $form->textarea('notes', 'Notes');

            // $countries = Country::get();
            // $countriesOptions = $countries->pluck('name', 'id');

            $form->divider("Domain Information");
            $form->text('registrar', 'Registrar');
            $form->text('domain', 'Domain');
            $form->text('domain_ext', 'Domain Extension');
            
            $states = [
                'on'  => ['value' => 'yes', 'text' => 'For Sale', 'color' => 'success'],
                'off' => ['value' => 'no', 'text' => 'Not for Sale', 'color' => 'danger'],
            ];
            $form->switch('for_sale', 'is the domain for sale?')->states($states)->default('no');
            $form->currency('selling_price');

            $states2 = [
                'on'  => ['value' => 'yes', 'text' => 'Yes', 'color' => 'success'],
                'off' => ['value' => 'no', 'text' => 'No', 'color' => 'danger'],
            ];
            $form->switch('active_website', 'Is the domain name associated with an active website?')->states($states2)->default('no');
            $form->textarea('active_description');

            $form->textarea('website_usage', 'Website Usage');


            $form->text('first_name', 'Registrant First Name');
            $form->text('last_name', 'Registrant Last Name');
            $form->text('registrant_organization', 'Registrant Organization');
            $form->text('registrant_street', 'Registrant Street');
            $form->text('registrant_city', 'Registrant City');
            $form->text('registrant_state', 'Registrant State');
            $form->text('registrant_postcode', 'Registrant Postal Code');
            $form->text('registrant_phone', 'Registrant Phone');
            $form->text('registrant_fax', 'Registrant Fax');
            $form->email('admin_email', 'Registrant/OwnerEmail');
            $form->select('registrant_country_id', 'Country')->options($countriesOptions);
            
            $form->textarea('registrant_background', 'Registrant Background');


            $form->textarea('notes', 'Notes');

            $form->textarea('other_emails', 'Other Emails')->help("<i>Please enter other email addresses here.<br>Comma separated ex. email1@email.com, email2@email.com</i>");



            

        });

        $form->tab('Domain Emails', function( $form ) {
            $form->hasMany('emails', 'Domain email', function (Form\NestedForm $email) {
                $email->email('email', 'Email');
                $email->text('notes', 'Notes');

            })->mode('table');
        });
        

        // ============================================ 
        // Tools    ===================================
        // ============================================ 
        // $form->tools(function (Form\Tools $tools) {
        //     $tools->disableDelete();
        //     $tools->disableView();
        //     // $tools->disableList();
        // });


        return $form;
    }

}
