<?php

namespace App\Admin\Controllers;

use App\Country;
use App\Continent;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class CountryController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Country);

        $grid->column('continent.name', 'Continent');
        $grid->name('Name')->sortable();
        $grid->abbr('Abbr');
        $grid->agency_short('Agency abbr');
        $grid->agency_name('Agency name');
        $grid->opposition_period('Opposition period');
        $grid->registration_period('Registration period');

        $grid->filter(function($filter){

            // Remove the default id filter
            $filter->disableIdFilter();

            // Add a column filter
            $filter->like('Name', 'Country');

            $filter->like('Agency name', 'Agency name');

            $filter->equal('madrid_protocol','Madrid Protocol')->select([
                '0' => 'No',
                '1' => 'Yes'
            ]);

        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Country::findOrFail($id));

        $show->id('Id');
        $show->name('Name');
        $show->abbr('Abbr');
        $show->avatar('Avatar');
        $show->is_feature('Is feature');
        $show->agency_name('Agency name');
        $show->agency_short('Agency short');
        $show->opposition_period('Opposition period');
        $show->registration_period('Registration period');
        $show->madrid_protocol('Madrid protocol');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Country);

        $continent = Continent::get();
        $countries = Country::get();
        
        $continentOptions = $continent->pluck('name', 'id');
        $abbrOptions = $countries->pluck('abbr', 'abbr');

        $form->text('name', 'Name');
        $form->select('continent_id', 'Continent')->options($continentOptions);
        $form->select('abbr', 'Abbr')->options($abbrOptions);
        $form->textarea('avatar', 'Avatar');
        $form->switch('is_feature', 'Is feature');
        $form->text('agency_name', 'Agency name');
        $form->text('agency_short', 'Agency short');
        $form->text('opposition_period', 'Opposition period');
        $form->text('registration_period', 'Registration period');
        
        $form->switch('madrid_protocol', 'Madrid protocol');

        return $form;
    }
}
