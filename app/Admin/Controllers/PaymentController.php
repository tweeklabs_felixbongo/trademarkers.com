<?php

namespace App\Admin\Controllers;

use App\Payment;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class PaymentController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Payment);

        $grid->id('Id');
        $grid->user_id('User id');
        $grid->order_id('Order id');
        $grid->reference_no('Reference no');
        $grid->pay_type('Pay type');
        $grid->gateway('Gateway');
        $grid->card_type('Card type');
        $grid->card_ref('Card ref');
        $grid->currency('Currency');
        $grid->amount('Amount');
        $grid->created_at('Created at');
        $grid->updated_at('Updated at');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Payment::findOrFail($id));

        $show->id('Id');
        $show->user_id('User id');
        $show->order_id('Order id');
        $show->reference_no('Reference no');
        $show->pay_type('Pay type');
        $show->gateway('Gateway');
        $show->card_type('Card type');
        $show->card_ref('Card ref');
        $show->currency('Currency');
        $show->amount('Amount');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Payment);

        $form->number('user_id', 'User id');
        $form->number('order_id', 'Order id');
        $form->text('reference_no', 'Reference no');
        $form->text('pay_type', 'Pay type');
        $form->text('gateway', 'Gateway');
        $form->text('card_type', 'Card type');
        $form->text('card_ref', 'Card ref');
        $form->text('currency', 'Currency');
        $form->decimal('amount', 'Amount');

        return $form;
    }
}
