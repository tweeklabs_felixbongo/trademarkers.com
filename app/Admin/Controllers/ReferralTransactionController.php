<?php

namespace App\Admin\Controllers;

use App\ActionCodeReferralTransaction;
use App\ActionCodeReferral;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Admin\Extensions\SendMailCustomer;

use App\Repositories\Utils;
use App\ActionCode;
use App\ActionCodeCart;

use Mail;

class ReferralTransactionController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {

        // dd($this->form());
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

     
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ActionCodeReferralTransaction);

        // $grid->id('Id');
        $grid->model()->orderBy('created_at', 'desc');
        $grid->model()->where('ref_invite_id', '=', null);

  
        $grid->column('User Name')->display(function(){

            $referral = ActionCodeReferral::find($this->referrals_id);
            // dd($referral->user);
            return $referral->user->name;
        });

        $grid->description('Description');
        $grid->amount('Request Amount');

        $grid->status('Status')->display(function(){
            $str = "";

            if ($this->status == 'processing'){
                $str = "<p style='color:#55abee;font-weight:bold;'><i class='fa fa-spinner'></i> Processing</p>";
            } else {
                $str = "<p style='color:#00a65a;font-weight:bold;'><i class='fa fa-star'></i> Complete</p>";
            }

            return $str;
        });

        $grid->column('created_at', 'Date')->display( function( $created_at ) {
            $date = Carbon::parse( $created_at );

            return $date->format('M d, Y');
        } );

        // $grid->actions(function ($actions) {
        //     $actions->append(new SendMailCustomer($actions->getKey()));
        // });

        // $grid->filter(function($filter){

        //     // Remove the default id filter
        //     $filter->disableIdFilter();

        //     $filter->like('order_number', 'Order Number');

        // });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ActionCodeReferralTransaction::findOrFail($id));

        $show->id('Id');

        $show->info_name('Name');
        $show->info_email('Email');
        $show->information('Information');
        $show->amount('Amount');


        return $show;
    }


    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ActionCodeReferralTransaction);
        

        return Admin::form(ActionCodeReferralTransaction::class, function (Form $form) {

            // $form->text('referral_id')->display(function(){

            //     $referral = ActionCodeReferral::find($this->referrals_id);
            //     // dd($referral->user);
            //     return $referral->user->name;
            // });

            // $referral = ActionCodeReferral::find($form->referrals_id);
            // dd($this->id);
            // $form->divider('Title');
            $form->hidden('id');
            $form->display('info_name', 'Name');
            $form->display('info_email', 'Email');
            $form->textarea('information', 'Information')->readonly();
            $form->currency('amount', 'Requested Amount')->symbol('$');

            $form->select('status', 'Status')->options( 
                [
                  "processing" => "Processing",
                  "done" => "Complete"
                ]
            );
           

            $form->saving( function ( Form $form ) { 
                if ( $form->status == 'done' ) {

                    // fetch user
                    $transaction = ActionCodeReferralTransaction::find($form->id);
                    // dd($transaction);
                    $subject = "Affiliate Payment Request Status";
                    $res = Mail::send('mail.payment-request-status-update', ['transaction' => $transaction], function ($m) use ($transaction, $subject) {
                        $m->from('info@trademarkers.net', 'TradeMarkers LLC');
                        $m->to($transaction->info_email, $transaction->info_name)->subject($subject);
                    });
                }
            });
            
        });

        

        return $form;
    }




}
