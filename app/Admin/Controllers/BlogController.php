<?php

namespace App\Admin\Controllers;

use App\Blog;
use App\MetaTag;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class BlogController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Blog);

        $grid->model()->orderBy('created_at', 'desc');

        $grid->id('Id');
        $grid->slug('Slug');
        $grid->title('Title');
        $grid->status('Status');
    
        $grid->created_at('Created at');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Blog::findOrFail($id));

        $show->id('Id');
        $show->slug('Slug');
        $show->title('Title');
        $show->description('Description');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Blog);

        $form->text('slug', 'Slug')->readonly();
        $form->text('title', 'Title')->required();
        $form->ckeditor('description', 'Content')->required();
        $form->image('featured_img', 'Featured Image')->required();

        $states = [
            'off'  => ['value' => 'draft', 'text' => 'Draft', 'color' => 'info'],
            'on' => ['value' => 'publish', 'text' => 'Publish', 'color' => 'success'],
        ];

        $form->switch('status', 'Status')->states( $states )->default('publish');

        $form->hidden('author_id', 'Author');

        $form->saving( function ( Form $form ) { 
            // $slug = $form->title
            if (!$form->slug) {
                $form->slug = $this->fetchUniqueSlug(str_slug($form->title));

                // create meta links for this article
                MetaTag::create( [
                    'uri'           => '/blog/'.$form->slug,
                    'title'         => $form->title,
                    'description'   => $form->title
                ] );
            }
            

            $form->author_id = ($form->author_id ? $form->author_id : Admin::user()->id);
        });

        return $form;
    }

    public function fetchUniqueSlug($slug, $count = 1){

        $slugFind = ($count > 1 ? $slug .'-'. $count : $slug );
        $blog = Blog::where('slug',$slugFind)->first();

        if (!$blog) {
            return $slugFind;
        } else {
            return $this->fetchUniqueSlug($slug, ++$count);
        }

        // return ;
    }
}
