<?php

namespace App\Admin\Controllers;

use App\Country;
use App\Event;
use App\EventType;
use App\Brand;
use App\BrandLead;
use App\ActionCodeCase;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Illuminate\Support\Facades\DB;
use Encore\Admin\Form;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Carbon\Carbon;
use Encore\Admin\Grid\Column;
use App\Repositories\MandrillMailer;
use Encore\Admin\Auth\Database\Role;
use Encore\Admin\Auth\Database\Administrator;
Use Encore\Admin\Widgets\Table;
use Illuminate\Http\Request;

use Session;

class EventController extends Controller
{
    use HasResourceActions;

    public $notifyService;

    public function __construct(
        \App\Services\CronNotifyService $notifyService) {

        $this->notifyService = $notifyService;
    }

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        if ( Session::has('callService') ){
            $this->notifyService->sendEmailLayout();

            return $content
            ->header('Events List')
            ->description('List of Created Events')
            ->withInfo('Info', 'Event triggered!, sending emails.')
            ->body($this->grid());

            
        }

        return $content
            ->header('Events List')
            ->description('List of Created Events')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        if ( Session::has('message') )
            return $content
            ->header('Edit')
            ->description('description')
            ->withInfo('Info', Session::get('message'))
            ->body($this->form()->edit($id));

        if ( Session::has('errMsg') )
            return $content
            ->header('Edit')
            ->description('description')
            ->withInfo('Error', Session::get('errMsg'))
            ->body($this->form()->edit($id));
        

        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create Event')
            ->description('Select which event type and search from leads to compare')
            ->withInfo('Info', "Please select event type/brand and submit to show necessary leads.")
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Event);

        // filter if opposition researcher
        // if ( Admin::user()->isRole('opposition-researcher') ) {

        //     $grid->model()->where('user_id', Admin::user()->id );
        // }
        
        if ( ! Admin::user()->inRoles(['admin', 'administrator']) ) {
            // $ids = Event::where('user_id', Admin::user()->id)->get()->pluck('id');
            // dd($domains);
            // $grid->model()->where('user_id', Admin::user()->id);
        }
        
        $grid->model()->orderBy('created_at', 'desc');

        $grid->column('brand_id','Brand')
        ->display(function(){
            if (!$this->brand) 
            return 'No Selected Brand';

            return $this->brand->brand;
        })
        ->expand(function ($model) {

            // $brand = Brand::find($brandId);

            // if ($brand) 
            // return $brand->brand;

            // return '<i>No Selected Brand</i>';

            if (!$this->brand || !$this->brand->domains)
            return 'No Domains Found';

            $headers = [ 
                'id', 
                'domain', 
                'created_at'
            ];

            $rows;

            foreach ( $this->brand->domains as $domain ) {
                $rows[] = [
                    $domain->id,
                    $domain->domain,
                    $domain->created_at
                ];
            }

            if ( isset($rows) && $rows ) {
                $table = new Table($headers, $rows);

                return $table;
            } 
            
            return 'No Domains Found';
        });

        $grid->column('event_type_id','Event Type')->display(function($typeId){
            
            $type = EventType::find($typeId);

            if ($type)
            return $type->type;
            
            return 'N/A';
            // return EventType::find($typeId)->type;
        });

        $grid->country_id('Country')->display( function ($country_id) {
           
            if (!$country_id)
            return 'No Selected Country'; 

            $country = Country::find($this->country_id);
            $avatar = $country->avatar;

            return "<img src='" . asset('/images/'.$avatar) . "'>" . ' ' . $country->name;
            
            
        });

        // $grid->trademark('Trademark');
        $grid->column('action_code_case_id', 'ref Mark Lead')->expand(function ($model) {

            if (!$this->case)
            return 'No Selected Lead';
            // dd($this->case->brand);

            $brandStr = "";
            // foreach ($this->case->brand->brand_id as $tagBrand) {
            //     // dd($tagBrand);
            //     $brand = Brand::find($tagBrand);
            //     $brands .= $brand->brand.'';
            // }
            $brands = Brand::find($this->case->brand->brand_id);
            $brandsArr = $brands->pluck('brand')->toArray();
            $brandStr = implode( ',', $brandsArr );

            // dd($brandsArr );
            // dd($brand->pluck('brand'));
            // $eventsOptions = $events->pluck('type', 'id');
            $headers = [ 
                'id', 
                'number', 
                'trademark',
                'tm_holder', 
                'tm_filing_date',
                'registration_date',
                'class_number',
                'tag brands'
            ];
            $rows = [
                [
                    $this->case->id, 
                    $this->case->number, 
                    $this->case->trademark, 
                    $this->case->tm_holder, 
                    $this->case->tm_filing_date, 
                    $this->case->registration_date, 
                    $this->case->class_number, 
                    $brandStr
                ]
            ];

            $table = new Table($headers, $rows);

            return $table;
        });

        // $grid->user_id('User ID');
        $grid->column('caseOwned.trademark', "Owned TM");
        $grid->column('caseOwned.class_number', "Owned TM Class Number");
        $grid->column('caseOwned.tm_filing_date', "Owned TM Filing Date")->display(function ($filingDate){
            if ( $filingDate ) {
                return Carbon::parse($filingDate)->format('M d, Y');
            } 
            return '';
        });
        $grid->column('caseOwned.tm_holder', "TM Holder");
        // $grid->filling_number('Filling number');
        // $grid->tm_holder('TM Holder');
        // $grid->registration_date('Registration date');
        // $grid->tm_filing_date('Filling date');

        $grid->notes('Notes');
        $grid->action('Status');

        $grid->user_id('Added/Updated')->display(function() {
            if ($this->user && $this->user->name) {
                return $this->user->name;
            } 

            return 'System Generated';
            
        });

        $grid->created_at('Created');
        $grid->updated_at('Updated');


        // ==================================================================
        // ============================ FILTERS =============================
        // ==================================================================
        // ==================================================================
        $grid->filter(function($filter){

            // $brands = Brand::orderBy('created_at', 'desc')->get();
            // $brandOptions = $brands->pluck('brand', 'id');

            $events = EventType::getShowEvent()->get();
            $eventsOptions = $events->pluck('type', 'id');

            // Remove the default id filter
            $filter->disableIdFilter();

            $filter->equal('brand_id', 'Brand')->select()->ajax('/admin/api/brands');
            $filter->equal('event_type_id', 'Event')->select($eventsOptions);

            // $filter->where(function ($query) {

            //     $query->whereHas('brand', function ($query) {
            //         $query->where('brand_id','LIKE', '%'.$this->input.',%')
            //             ->orWhere('brand_id','LIKE', '%'.$this->input);
            //     });
            
            // }, 'Brand')->select($brandOptions);


            

        });


        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {

        admin_toastr('update success');
        return redirect("/admin/manage/events/".$id."/edit");

        // $show = new Show(Event::findOrFail($id));

        // $show->country_id('Country')->as(function($countryId){
        //     $country = Country::find($countryId);
        //     // dd($country);
        //     return $country->name ? $country->name : 'N/A';
        // });


        // $show->notes('Notes');


        // // dd(0);

        // return $show;
    }

    
    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Event);

         

        $form->tab('Event Information', function( $form ) {
            
            $eventId = request()->segment(4);
            
            $events = EventType::getShowEvent()->get();
            $eventsOptions = $events->pluck('type', 'id');
            // $brand = Brand::get();
            // $brandOptions = $brand->pluck('brand', 'id');
            $countries = Country::get();
            $countriesOptions = $countries->pluck('name', 'id');

            // populate essential needs for structure
            
            $form->hidden('id');
            $form->hidden('user_id')->default(Admin::user()->id);
            $form->text('name','Event Name');
            $form->select('event_type_id', 'Event Type')->options($eventsOptions);
            $form->select('brand_id', 'Brand')
                    ->options( function ( $brand_id ) {

                        $brand = Brand::find($brand_id);
                        // dd($data);
                        if ($brand) {
                            return $brand->pluck('brand', 'id');
                        }
                        
                        return null;
                
                    })
                    ->ajax('/admin/api/brands')
                    ->load('domain_id','/admin/api/domains')
                    ->help('<i>Select Brand and save to fetch tag leads</i>');

            $form->select('country_id', 'Country')->options($countriesOptions);
            
            if ( is_numeric($eventId) ) {
                // fetch tag leads
                $event = Event::find($eventId);

                if ( $event && $event->type ) {

                    if ( $event->type->slug != 'domain-price-inquiry' ) {
                        if ( $event->type->slug == 'letter-of-protest-for-domain-owners' ) {
                            $leads = BrandLead::with('case')
                                ->where('brand_id','LIKE', '%'.$event->brand_id.',%')
                                ->orWhere('brand_id','LIKE', '%'.$event->brand_id)
                                ->get();
                        } else {
                            $leads = BrandLead::with('case')
                                ->get();
                        }

                        if ($leads) {
                            $data = [];
                            foreach ( $leads as $lead ) {
                                if ($lead->case) 
                                $data[$lead->case->id] =  Carbon::parse($lead->case->tm_filing_date)->format('M d, Y') . ' | ' . $lead->case->number . ' | ' . $lead->case->trademark . ' | ' . $lead->case->class_number . ' | ' . $lead->case->tm_holder;              
                            }

                            $form->select('case_owned_id', 'Earlier Trademark')->options($data);
                            
                            $form->select('action_code_case_id', 'Defending Trademark')->options($data);
                        }
                    } else {
                        
                        if ($event->brand) {
                            $domainOptions = $event->brand->domains->pluck('domain', 'id');
                            $form->select('domain_id', 'Referral Domain')->options($domainOptions);
                        } else {
                            $form->select('domain_id', 'Referral Domain');
                        }
                      
                    }
                    
                    
                   
                } 

            } 

            $form->select('action', 'Event Action Status')->options([
                'draft' => 'Draft',
                'published' => 'Published',
            ])->default('draft');

            $form->textarea('notes', 'Notes');
            
            // $form->select('action_code_case_id', 'Referral Trademark From Leads')->ajax('/admin/api/leads');

        });

        $form->tab('Mark Informations', function( $form ) {

            $eventId = request()->segment(4);

            $event = Event::find($eventId);

            // populate case fields
            if ( $event && $event->caseOwned ) {
                

                $form->divider("Owned Trademark Info");
                // dd($event->caseOwned->emails);
                $emailNotifyStr = '';
                if ( count($event->caseOwned->emails) <= 0 ){
                    $emailNotifyStr = "<br><i>This lead doesn't have email list.</i>";
                }

                $form->display('own_trademark')
                    ->default($event->caseOwned->trademark)
                    ->help('<a href="/admin/manage/brand-leads/'.$event->caseOwned->id.'/edit">Edit Mark Lead</a>'.$emailNotifyStr);

                $form->display('number')->default($event->caseOwned->number);
                $form->display('tm_filing_date')->default(Carbon::parse($event->caseOwned->tm_filing_date)->format('M d, Y'));
                $form->display('tm_holder')->default($event->caseOwned->tm_holder);

                $form->divider("Address");

                $form->display('address')->default($event->caseOwned->address);
                $form->display('city')->default($event->caseOwned->city);
                $form->display('state')->default($event->caseOwned->state);
                $form->display('zip')->default($event->caseOwned->zip);
                $form->display('tm_country')->default($event->caseOwned->country_code);

            }

            if ( $event && $event->case ) {
                

                $form->divider("Defending Trademark Info");

                $emailNotifyStr = '';
                if ( count($event->case->emails) <= 0 ){
                    $emailNotifyStr = "<br><i>This lead doesn't have email list.</i>";
                }

                $form->display('own_trademark')
                    ->default($event->case->trademark)
                    ->help('<a href="/admin/manage/brand-leads/'.$event->case->id.'/edit">Edit Defending Mark Lead</a>'.$emailNotifyStr);

                $form->display('number')->default($event->case->number);
                $form->display('tm_filing_date')->default(Carbon::parse($event->case->tm_filing_date)->format('M d, Y'));
                $form->display('tm_holder')->default($event->case->tm_holder);

                $form->divider("Address");

                $form->display('address')->default($event->case->address);
                $form->display('city')->default($event->case->city);
                $form->display('state')->default($event->case->state);
                $form->display('zip')->default($event->case->zip);
                $form->display('tm_country')->default($event->case->country_code);

            }
            
            
            // $form->has('caseOwned', function (Form\NestedForm $form) {
            //     $form->display('trademark');
            // });
            
            
        
        });

        $form->tab('Attachments', function ( $form ) {

            $form->hasMany('attachments', function (Form\NestedForm $form) {
                $form->hidden('user_id')->default(0);
                $form->file('name', 'Name')->uniqueName();
                $form->textarea('description', 'Description')->default('File');
            })->mode('table');
        } );

        $form->saved(function (Form $form) {
            
            $event = Event::max('id');
            $eventId = $form->id ? $form->id : $event;

            if ( $form->action == 'published' ) {
                
                // $this->notifyService->sendEmailLayout();
                if ( $this->validateEventData($eventId) ) {
                    Session::flash('callService', 'true');
                    admin_toastr('Update success, submitting event..');
                    return redirect("/admin/manage/events/");
                } else {

                    $rpoEvent = Event::find($eventId);
                    $rpoEvent->action = 'draft';
                    $rpoEvent->save();
                    return redirect("/admin/manage/events/".$eventId."/edit");
                }

                
            } else {
                admin_toastr('Updated Record!');
                Session::flash('message', 'Updated Record!, this event will only trigger after setting to pulished.'); 
                return redirect("/admin/manage/events/".$eventId."/edit");
            }

            
        });


        // ============================================ 
        // Tools    ===================================
        // ============================================ 
        // $form->tools(function (Form\Tools $tools) {
        //     $tools->disableDelete();
        //     $tools->disableView();
        //     // $tools->disableList();
        // });
        

        return $form;
    }

    public function getLeads(Request $request)
    {
        // dd($request->attributes);
        $q = $request->get('q');

        return ActionCodeCase::where('trademark', 'like', "%$q%")->paginate(null, ['id', DB::raw("CONCAT(number , ' | ' ,trademark,' | ',tm_holder, ' | ', tm_filing_date) as text")] );
    }

    public function getDomain(Request $request)
    {
        // dd($request->attributes);
        $q = $request->get('q');

        $brand = Brand::find($q);

        // dd($brand);
        return $brand->domains()->get(['id', DB::raw('domain as text')]);
    }

    public function validateEventData( $eventId )
    {
        $rpoEvent = Event::find($eventId);
        // check data if necessary data was selected from each type
        if ( $rpoEvent->type && $rpoEvent->type->slug ) {
            switch ($rpoEvent->type->slug) {

                case 'trademark-registration-invitation-claiming-priority---us-filings':
                    if ( !$rpoEvent->caseOwned ) {
                        admin_toastr('Updated Record!');
                        Session::flash('errMsg', "Updated Record!, But this can't be published since it needs Mark information"); 
                        return false;
                    }
                break;

                case '	letter-of-protest-for-domain-owners':
                    if ( !$rpoEvent->case || !$rpoEvent->brand ) {
                        admin_toastr('Updated Record!');
                        Session::flash('errMsg', "Updated Record!, But this can't be published since it needs Defending Mark information"); 
                        return false;
                    }
                break;

                case 'letter-of-protest-for-prior-registered-tm-holders':
                    if ( !$rpoEvent->case || !$rpoEvent->caseOwned || !$rpoEvent->brand ) {
                        admin_toastr('Updated Record!');
                        Session::flash('errMsg', "Updated Record!, But this can't be published since it needs Defending and Opposing Mark information"); 
                        return false;
                    }
                break;

                case 'trademark-opposition---uspto':
                    if ( !$rpoEvent->case || !$rpoEvent->caseOwned || !$rpoEvent->brand ) {
                        admin_toastr('Updated Record!');
                        Session::flash('errMsg', "Updated Record!, But this can't be published since it needs Defending and Opposing Mark information"); 
                        return false;
                    }
                break;

                case 'trademark-opposition---euipo':
                    if ( !$rpoEvent->case || !$rpoEvent->caseOwned || !$rpoEvent->brand ) {
                        admin_toastr('Updated Record!');
                        Session::flash('errMsg', "Updated Record!, But this can't be published since it needs Defending and Opposing Mark information"); 
                        return false;
                    }
                break;

            } 
        }

        return true;
    }

}
