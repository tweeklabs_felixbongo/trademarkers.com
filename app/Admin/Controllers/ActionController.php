<?php

namespace App\Admin\Controllers;

use App\ActionCode;
use App\ActionCodeCampaign;
use App\ActionCodeCase;
use App\Brand;
use App\BrandDomain;
use App\BrandLead;
use Encore\Admin\Layout\Content;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Repositories\Utils;
use League\Csv\Reader;
use League\Csv\Statement;
use League\Csv\CharsetConverter;
use Carbon\Carbon;

class ActionController extends Controller
{
    public function index(Content $content)
    {	
        $campaigns = ActionCodeCampaign::all();
        $arr = [];

        return $content
            ->header('Title')
            ->description('Description')
            ->body(view('admin.index', compact('campaigns','arr')));
    }

    public function upload( Request $request, Content $content )
    {
        
    	if ( $request->action_file ) {

    		$validatedData = $request->validate( [
                'action_file' => 'required|file',
            ] );

            if ( $request->action_file->getClientOriginalExtension() !== "csv" ) {

            	return back()->withErrors(['action_file' => "The file must be the extension of csv."]);
            }

            $filename = md5( time() ) . '.' . $request->action_file->getClientOriginalExtension();

            $success = ActionCodeCampaign::check_duplicate_campaign( $request->action_file->getClientOriginalName() );

            if ( $success === true ) {

            	return back()->withErrors(['action_file' => "Duplicate file has been uploaded."]);
            } 
            
            $request->action_file->move( public_path('uploads/actions'), $filename );

            $path = public_path('uploads/actions') ."/". $filename;
            $csv = Reader::createFromPath( $path, 'r' );

            $campaign = null;
            
            $input_bom = $csv->getInputBOM();

            if ($input_bom === Reader::BOM_UTF16_LE || $input_bom === Reader::BOM_UTF16_BE) {

                CharsetConverter::addTo($csv, 'utf-16', 'utf-8');
            }

            $count    = 0;
            $success  = 0; //10 is success, if not fail
            $arr      = array();

            
            foreach ( $csv as $data ) {

            	// dd($data);
                
                if ( $count == 0 && $request->type != '13') {
                    
                    $cm = ActionCodeCampaign::create( [
		                'name'      => $request->action_file->getClientOriginalName(),
		                'file_name' => $filename,
		                'count'     => $count 
                    ] );
                    
                    $campaign = $cm;

                } else {

                    $regDateStr = $data[8];
                    $fillingDate = $data[4];
                    $statusDate = $data[7];

                    // SELECT TYPE
                    if ( $request->type == '11' ) {

                        if ( isset($data[8]) ) {
                            $regDateStr = $this->formatStringDate($data[8]);
                        } 

                        if ( isset($data[4]) ) {
                            $fillingDate = $this->formatStringDate($data[4]);
                        } 

                        if ( isset($data[7]) ) {
                            $statusDate = $this->formatStringDate($data[7]);
                        } 

                    } elseif ( $request->type == '12' ) {

                        if ( isset($data[8]) ) {
                            $regDateStr =  substr($data[8],0,4) . '-' . substr($data[8],4,2) . '-' . substr($data[8],6,2);
                        } 

                    } elseif ( $request->type == '13' ) {
                        // dd(0);
                    } else {

                        $last_action_code = ActionCode::max('id');
                        $last_action_code++;
                        $actionC = Utils::case_gen( $last_action_code );
                        $code = ActionCode::create( [

                            'case_number'             => $actionC,
                            'action_code_type_id'     => $request->type,
                            'action_code_campaign_id' => $cm->id,
                        ] );

                    }

                    

                    if ( $request->type == '13' ) {
                        // dd(5);
                        // brand and domain
                        $brand = Brand::where('brand', $data[1])->first();
                        
                        if ( !$brand ) {
                            // create brand
                            $brand = Brand::create([
                                'user_id' => Admin::user()->id,
                                'brand' => $data[1],
                                'notes' => 'CSV Upload',
                                'purpose' => $data[3],
                                'category' => $data[5],
                                'value' => $data[4],
                            ]);
                            // dd($brand);
                        }

                        $brandDomain = BrandDomain::where('domain', $data[0])->first();

                        if ( !$brandDomain ) {
                            // create brand
                            $brand = BrandDomain::create([
                                'user_id' => Admin::user()->id,
                                'brand_id' => $brand->id,
                                'domain' => $data[0],
                                'admin_email' => $data[6],
                                'notes' => 'CSV Upload',
                            ]);
                        }

                        // dd(1);

                    } else {
                        $currentStatus = '';

                        if ( isset($data[6]) && $data[6] ) {
                            $statusArr = explode(',', $data[6]);

                            $ndx = count($statusArr);

                            $currentStatus = $statusArr[($ndx - 1)];
                        }

                        $case = ActionCodeCase::create( [
                            'action_code_id'          => isset($code) ? $code->id : null,
                            'action_code_campaign_id' => $campaign->id,
                            'number'                  => isset($data[0]) ? $data[0] :  null,
                            'trademark'               => isset($data[1]) ? $data[1] :  '',
                            'class_number'            => isset($data[2]) ? $data[2] :  null,
                            'class_description'       => isset($data[3]) ? $data[3] :  'N/A',
                            'tm_filing_date'          => date("Y-m-d H:i:s", strtotime($fillingDate) ),
                            'tm_holder'               => isset($data[5]) ? $data[5] :  '',
                            'mark_current_status_code'=> $currentStatus,
                            'mark_current_status_date'=> date("Y-m-d H:i:s", strtotime($statusDate) ),
                            'registration_date'       => date("Y-m-d H:i:s", strtotime($regDateStr) ),
                            'city_of_registration'    => isset($data[9]) ? $data[9] :  null,
                            'representatives'         => isset($data[10]) ? $data[10] :  null,
                            'address'                 => isset($data[11]) ? $data[11] : '',
                            'city'                    => isset($data[12]) ? $data[12] :  null,
                            'state'                   => isset($data[13]) ? $data[13] :  null,
                            'zip'                     => isset($data[14]) ? $data[14] :  null,
                            'country'                 => isset($data[15]) ? $data[15] :  null,
                            'country_code'            => isset($data[16]) ? $data[16] :  null,
                            'email'                   => isset($data[17]) ? $data[17] : null,
                            'fax_no'                  => isset($data[18]) ? $data[18] :  null,
                            'telephone_no'            => isset($data[19]) ? $data[19] :  null,
                            'application_reference'   => isset($data[20]) ? $data[20] :  null,
                            'ref_processed_date'      => isset($data[21]) ? date("Y-m-d H:i:s", strtotime($data[21]) ) : null,
                            'website'                 => isset($data[22]) ? $data[22] :  null,
                            
                        ] );

                        // CREATE BRAND OR TAG TO EXISTING BRAND
                        $brand = Brand::firstOrCreate(['brand' => $case->trademark]);

                        if ($brand) {
                            BrandLead::create([
                                'brand_id' => [$brand->id],
                                'lead_id' => $case->id
                            ]);
                        }
                    }

                    
                }
                
                $count++;
            }

            if ( $request->type != '13' ) {
                $campaign->count = $count;
                $campaign->save();
            }

            $campaigns = ActionCodeCampaign::all();

            return $content
                ->header('Action Code')
                ->description('View')
                ->body( view('admin.index', compact( 'arr', 'campaigns' )) );
    	}
    }

    public function formatStringDate($str)
    {
        $statDate =  explode("/",trim($str));

        if ( isset($statDate[2]) && isset($statDate[1]) && isset($statDate[0]) ) 
        return $statDate[2] . '-' . $statDate[1] . '-' . $statDate[0]; 

        return '';
    }
}