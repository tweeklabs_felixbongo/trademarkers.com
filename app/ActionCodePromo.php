<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActionCodePromo extends Model
{

    protected $fillable = [
        'action_code_id', 
        'user_id', 
        'discount', 
        'expiry_date', 
        'limit', 
        'status',
        'promo_type'
    ];

    protected $appends = [
        'promo_code'
    ];

    public function getPromoCodeAttribute()
    {
        $actionCodeId = $this->action_code_id;

        $action = ActionCode::find($actionCodeId);
        // dd(0);
        // $available = abs($this->attributes['qty_tabs_dispensed'] - ($this->attributes['total_qty_purchased_pending'] + $this->attributes['total_qty_purchased']));
        return $action->case_number;
    }

    public function action_code()
    {
        return $this->belongsTo( ActionCode::class, 'action_code_id' );
    }

    public function user()
    {
        return $this->belongsTo( User::class );
    }

    public function country()
    {
        return $this->belongsTo( Country::class );
    }

    public function campaign()
    {
        return $this->belongsTo( ActionCodeCampaign::class, 'campaign_id' );
    }

    // mutators
    public function getCountryIdAttribute($value)
    {
        return explode(',', $value);
    }

    public function setCountryIdAttribute($value)
    {
        $this->attributes['country_id'] = implode(',', $value);
    }

    public function getPromoTypeAttribute($value)
    {
        return explode(',', $value);
    }

    public function setPromoTypeAttribute($value)
    {
        $this->attributes['promo_type'] = implode(',', $value);
    }
}
