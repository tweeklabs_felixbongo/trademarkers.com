<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassDescription extends Model
{
    public function trademarkClass()
    {
    	return $this->belongsTo(TrademarkClass::class);
    }

    public static function searchDescription( $description )
    {
    	$result = ClassDescription::where('description', 'like', '%' . $description . '%')->get();

    	return $result;
    }
}
