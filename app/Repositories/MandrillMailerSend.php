<?php

namespace App\Repositories;

use Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class MandrillMailerSend {

    

    public static function sendQuoteRequest( $request )
    {
        // dd($request['attachments']);
        $subject = "Quote Request From " . $request['name'];
        
        // dd($files);
        $res = Mail::send('mail.quote-request', ['data' => $request], function ($m) use ($request , $subject) {
            $m->from('notifications@trademarkers.co.uk', 'TradeMarkers LLC');
            $m->to('info@trademarkers.com', 'TradeMarkers LLC')->subject($subject);

            $files = $request->file('attachments');
            // dd($files);
            if( $files && count($files) > 0 ) {
                foreach($files as $file) {
                    $m->attach($file->getRealPath(), array(
                        'as' => $file->getClientOriginalName(),      
                        'mime' => $file->getMimeType())
                    );
                }
            }
        });

        // auto responder
        $customerSubject = "Trademarkers Request Quotation";
        $res = Mail::send('mail.quote-request-repond', ['data' => $request], function ($m) use ($request, $customerSubject) {
            $m->from('notifications@trademarkers.co.uk', 'TradeMarkers LLC');

            $m->to($request['email'], $request['name'])->subject($customerSubject);
        });

        return $res;

    }

    public static function sendVerificationEmail($user)
    {
        $internalSubject = "Please Verify Email Address";
        $res = Mail::send('mail.verify-email', ['user' => $user], function ($m) use ($internalSubject, $user) {
            $m->from('notifications@trademarkers.com', 'TradeMarkers LLC');

            $m->to($user->email, $user->name)->subject($internalSubject);
        });
    }

    public static function assign_trademark( $trademark, $agent )
    {

        $internalSubject = "New Trademark Assigned";
        $res = Mail::send('mail.assign-trademark', ['trademark' => $trademark, 'agent' => $agent], function ($m) use ($internalSubject, $agent) {
            $m->from('notifications@trademarkers.com', 'TradeMarkers LLC');

            $m->to($agent->username, $agent->name)->subject($internalSubject);
        });
    
         return $res;
    }

    public static function trademark_new_status( $trademark, $status )
    {

        $rcpt_email = $trademark->profile->email;

        if ( $trademark->profile->company != "N/A" ) {

            $rcpt_name  = $trademark->profile->company;
        } else {

            $rcpt_name  = $trademark->profile->first_name;
        }

        $subject = "Trademark Status Updated";
        $res = Mail::send('mail.trademark-status', ['trademark' => $trademark, 'status' => $status, 'rcpt_name' => $rcpt_name], function ($m) use ($subject, $rcpt_email, $rcpt_name) {
            $m->from('notifications@trademarkers.com', 'TradeMarkers LLC');

            $m->to($rcpt_email, $rcpt_name)->subject($subject);
        });
    
        return $res;

    }

    public static function trademark_new_comment( $trademark, $user, $isAdmin, $isInternal )
    {

        $templatename = 'trademark-comment';
        $rcpt_name = "Trademarkers LLC";
        $rcpt_email = "notifications@trademarkers.com";

        if ( $isAdmin === true && $isInternal === false ) {

            $rcpt_email = $trademark->user->email;
            $rcpt_name  = $trademark->user->name;
        }

        else if ( !$trademark->rep_id || !$trademark->atty_id ) {

            return;
        }

        if ( $isAdmin === true && $isInternal === true ) {

            if ( $user->isRole('paralegal') ) {

                $rcpt_email = $trademark->atty_email;

                $res = DB::table('admin_users')->where('id', $trademark->atty_id)->get();

                $rcpt_name  = $res[0]->name;
            }

            if ( $user->isRole('attorney') ) {

                $rcpt_email = $trademark->rep_email;

                $res = DB::table('admin_users')->where('id', $trademark->rep_id)->get();

                $rcpt_name  = $res[0]->name;
            }
            
        }

        if ( $isAdmin === false && $isInternal === false ) {

            $rcpt_email = $trademark->rep_email;

            $res = DB::table('admin_users')->where('id', $trademark->rep_id)->get();

            $rcpt_name  = $res[0]->name;
        }

        $var_content = array(

            array(

                'name'   => 'client_name',
                'content' => $rcpt_name
            ),

            array(

                'name'   => 'trademark_name',
                'content' => $trademark->name
            ),

        );
        
        $message = array(

            'to' => array(

                array(

                    'email' => $rcpt_email,
                    'name'  => $rcpt_name,
                    'type'  => 'to'
                )
            ),

            'merge_language' => 'handlebars',

            'global_merge_vars' => $var_content

        );

        $subject = "Trademark New Comment Added";
        $res = Mail::send('mail.trademark-comment', ['client_name' => $client_name, 'trademark_name' => $trademark->name ], function ($m) use ($subject, $rcpt_email, $rcpt_name) {
            $m->from('notifications@trademarkers.com', 'TradeMarkers LLC');

            $m->to($rcpt_email, $rcpt_name)->subject($subject);
        });

        // $res = \MandrillMail::messages()->sendTemplate( $templatename, [], $message, false, '', ''  );

         return $res;

    }

   

}