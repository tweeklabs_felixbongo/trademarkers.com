<?php

namespace App\Repositories;

use Weblee\Mandrill\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Country;

class MandrillMailer {

    public static function new_user_mail( $name, $email )
    {

        $subject = "New Account Created - TradeMarkers.com";
        $res = Mail::send('new-client-account', ['name' => $name], function ($m) use ($name, $email , $subject) {
            $m->from('info@trademarkers.com', 'TradeMarkers LLC');
            $m->to($email, $name)->subject($subject);
        });

        return $res;

    }

    public static function sendQuoteRequest( $request )
    {
        // dd($request['name']);
        $subject = "Quote Request From " . $request['name'];
        $res = Mail::send('quote-request', ['data' => $request], function ($m) use ($request , $subject) {
            $m->from($request['email'], $request['name']);
            $m->to('info@trademarkers.com', 'TradeMarkers LLC')->subject($subject);
        });

        return $res;

    }

    public static function contact_us_mail( $information )
    {
    	$templatename = 'Contact-us-client';

        $message = array(

            'to' => array(

                array(

                    'email' => $information['email'],
                    'name'  => $information['name'],
                    'type'  => 'to'
                )
            ),

            'merge_vars' => array(
                array(
                    'rcpt' => $information['email'],
                    'vars' => array(
                        array(
                            'name'    => 'NAME',
                            'content' => $information['name']
                        ),

                        array(
                            'name'    => 'email',
                            'content' => $information['email']
                        ),

                        array(
                            'name'    => 'POST_CITY',
                            'content' => $information['city']
                        ),

                        array(
                            'name'    => 'POST_IP',
                            'content' => $information['ipaddress']
                        ),

                        array(
                            'name'    => 'MESSAGE',
                            'content' => $information['message']
                        ),

                        array(
                            'name'    => 'PHONE',
                            'content' => $information['phone']
                        )
                    )
                )
            ),

        );

        MandrillMailer::contact_us_internal_mail( $information );

         $res = \MandrillMail::messages()->sendTemplate( $templatename, [], $message, false, '', ''  );

         return $res;
    }

    public static function contact_us_internal_mail( $information )
    {
    	$templatename = 'Contact-us-internal';

        $message = array(

            'to' => array(

                array(

                    'email' => 'info@trademarkers.com',
                    'name'  => 'Notifications',
                    'type'  => 'to'
                )
            ),

            'merge_vars' => array(
                array(
                    'rcpt' => 'info@trademarkers.com',
                    'vars' => array(
                        array(
                            'name'    => 'NAME',
                            'content' => $information['name']
                        ),

                        array(
                            'name'    => 'EMAIL',
                            'content' => $information['email']
                        ),

                        array(
                            'name'    => 'POST_IP',
                            'content' => $information['ipaddress']
                        ),

                        array(
                            'name'    => 'POST_CITY',
                            'content' => $information['city']
                        ),

                        array(
                            'name'    => 'MESSAGE',
                            'content' => $information['message']
                        ),

                        array(
                            'name'    => 'PHONE',
                            'content' => $information['phone']
                        )
                    )
                )
            ),

        );

         $res = \MandrillMail::messages()->sendTemplate( $templatename, [], $message, false, '', ''  );

         return $res;
    }

    public static function new_order_mail( $order, $contents, $client, $paymentStatus = 'pending', $payment_method = 'Invoice' )
    {
    	$templatename = $client ? 'new-order-client-v2' : 'new-order-internal-v2';

    	if ( $client ) {

    		$rcpt_email = Auth::user()->email;
            $rcpt_name  = Auth::user()->name;
            // $rcpt_email = "felix@trademarkers.com";

    	} else {
    		
    		$rcpt_email = 'info@trademarkers.com';
    		$rcpt_name  = 'Info';
     	}
         

        $lessBalance = $paymentStatus == 'paid' ? number_format( ($order['total_amount'] * -1),2) : number_format(0,2);
        $balance = $paymentStatus == 'paid' ? number_format(0,2) : number_format( ($order['total_amount'] ),2);

    	$var_content = array(

	    	array(

	    		'name'   => 'services',
	    		'content' => array()
	    	),

	    	array(

	    		'name'   => 'ref',
	    		'content' => $order['order_number']
	    	),

	    	array(

	    		'name'   => 'txdate',
	    		'content' => $order['created_at']->toFormattedDateString()
	    	),

	    	array(

	    		'name'   => 'trademarker_url',
	    		'content' => 'www.trademarkers.com'
	    	),

	    	array(

	    		'name'   => 'total',
	    		'content' => number_format($order['total_amount'],2)
	    	),

            array(

                'name'   => 'client_name',
                'content' => Auth::user()->name
            ),

            array(

                'name'   => 'paymentStatus',
                'content' => $paymentStatus
            ),

            array(

                'name'   => 'payment_method',
                'content' => $payment_method
            ),

            array(

                'name'   => 'lessPayment',
                'content' => $lessBalance 
            ),

            array(

                'name'   => 'balance',
                'content' => $balance 
            )


    	);

        $totalDiscount = 0;
        $amountTotal = 0;
    	foreach ( $contents as $content ) {

            $country = Country::find( $content['country_id'] );
            // $amountString = '';
            $amountString = $content['total_discount'] ? ' (-'.$content['total_discount'].')' : '';
    		$x  = array(
    			'service'    => $content['service'],
    			'work_mark'  => $content['name'],
    			'work_type'  => $content['type'],
    			'classes'    => $content['classes'],
    			'country'    => $country ? $country->name : "N/A",
    			'discount'    => number_format($content['total_discount'], 2),
                'amount'     => number_format($content['amount'], 2),
                'totalAmount'     => number_format($content['amount'] - $content['total_discount'], 2)
            );
            $amountTotal += $content['amount'];
            $totalDiscount += $content['total_discount'];

    		array_push( $var_content[0]['content'] , $x);
        }
        
        array_push( $var_content , 
            array(
                'name'   => 'amountTotal',
	    		'content' => number_format($amountTotal,2)
            ),
            array(
                'name'   => 'totalDiscount',
	    		'content' => number_format($totalDiscount,2)
            )
        );

        $message = array(

            'to' => array(

                array(

                    'email' => $rcpt_email,
                    'name'  => $rcpt_name,
                    'type'  => 'to'
                )
            ),

            'merge_language' => 'handlebars',

            'global_merge_vars' => $var_content

        );

         $res = \MandrillMail::messages()->sendTemplate( $templatename, [], $message, false, '', ''  );

         return $res;
    }

    public static function trademark_new_status( $trademark, $status )
    {

        $templatename = 'trademark-status';

        $rcpt_email = $trademark->profile->email;

        if ( $trademark->profile->company != "N/A" ) {

            $rcpt_name  = $trademark->profile->company;
        } else {

            $rcpt_name  = $trademark->profile->first_name;
        }

        $var_content = array(

            array(

                'name'   => 'client_name',
                'content' => $rcpt_name
            ),

            array(

                'name'   => 'trademark_name',
                'content' => $trademark->name
            ),

            array(

                'name'   => 'status',
                'content' => $status
            )

        );
        
        $message = array(

            'to' => array(

                array(

                    'email' => $rcpt_email,
                    'name'  => $rcpt_name,
                    'type'  => 'to'
                )
            ),

            'merge_language' => 'handlebars',

            'global_merge_vars' => $var_content

        );

        $res = \MandrillMail::messages()->sendTemplate( $templatename, [], $message, false, '', ''  );

         return $res;

    }

    public static function trademark_new_comment( $trademark, $user, $isAdmin, $isInternal )
    {

        $templatename = 'trademark-comment';
        $rcpt_name = "Trademarkers LLC";
        $rcpt_email = "notifications@trademarkers.com";

        if ( $isAdmin === true && $isInternal === false ) {

            $rcpt_email = $trademark->user->email;
            $rcpt_name  = $trademark->user->name;
        }

        else if ( !$trademark->rep_id || !$trademark->atty_id ) {

            return;
        }

        if ( $isAdmin === true && $isInternal === true ) {

            if ( $user->isRole('paralegal') ) {

                $rcpt_email = $trademark->atty_email;

                $res = DB::table('admin_users')->where('id', $trademark->atty_id)->get();

                $rcpt_name  = $res[0]->name;
            }

            if ( $user->isRole('attorney') ) {

                $rcpt_email = $trademark->rep_email;

                $res = DB::table('admin_users')->where('id', $trademark->rep_id)->get();

                $rcpt_name  = $res[0]->name;
            }
            
        }

        if ( $isAdmin === false && $isInternal === false ) {

            $rcpt_email = $trademark->rep_email;

            $res = DB::table('admin_users')->where('id', $trademark->rep_id)->get();

            $rcpt_name  = $res[0]->name;
        }

        $var_content = array(

            array(

                'name'   => 'client_name',
                'content' => $rcpt_name
            ),

            array(

                'name'   => 'trademark_name',
                'content' => $trademark->name
            ),

        );
        
        $message = array(

            'to' => array(

                array(

                    'email' => $rcpt_email,
                    'name'  => $rcpt_name,
                    'type'  => 'to'
                )
            ),

            'merge_language' => 'handlebars',

            'global_merge_vars' => $var_content

        );

        $res = \MandrillMail::messages()->sendTemplate( $templatename, [], $message, false, '', ''  );

         return $res;

    }

    public static function assign_trademark( $trademark, $agent )
    {
        $templatename = 'assign-trademark';


        $rcpt_email = $agent->username;
        $rcpt_name  = $agent->name;
        
        $var_content = array(

            array(

                'name'   => 'client_name',
                'content' => $rcpt_name
            ),

            array(

                'name'   => 'trademark_name',
                'content' => $trademark->name
            ),

        );
        
        $message = array(

            'to' => array(

                array(

                    'email' => $rcpt_email,
                    'name'  => $rcpt_name,
                    'type'  => 'to'
                )
            ),

            'merge_language' => 'handlebars',

            'global_merge_vars' => $var_content

        );

        $res = \MandrillMail::messages()->sendTemplate( $templatename, [], $message, false, '', ''  );

         return $res;
    }


    public static function send_cart_mail( $cart )
    {
        $templatename = 'send-cart-mail';
        $from_name = "Trademarkers LLC";
        $from_email = "notifications@trademarkers.com";
        $subject = "Update on Your Trademark Application";

        // dd($cart->user);

    	// $rcpt_email = Auth::user()->email;
    	// $rcpt_name  = Auth::user()->name;

        $message = array(

            'to' => array(

                array(

                    'email' => $cart->user->email,
                    'name'  => $cart->user->name,
                    'type'  => 'to'
                )
            ),

            // 'merge_language' => 'handlebars',
            'subject' => $subject,
            'from_email' => $from_email,
            'from_name' => $from_name,
            // 'global_merge_vars' => $var_content,

            'html' => $cart->email_content

        );


         $res = \MandrillMail::messages()->send( $message, false, '', ''  );

         return $res;
    }

}