<?php

namespace App\Repositories;
use App\Cases;
use App\ActionCode;

use Illuminate\Support\Facades\Auth;

class Utils {

    protected static $alpha = "ABCDEFGHJKLMNPQRSTUVWXYZ";

    /**
     * Generate a case number.
     *
     * @param country code
     * @return case number
     */
    public static function generate_case_number( $trademark_id )
    {
        // Array of uppercase alphabets from A-Z
        $alphabets = [
            'B','D','U','S','Y',
            'F','H','I','J','W',
            'L','A','P','Q','R',
            'V','C','G','E','X',
            'Z','K','T','M','N'
        ];

        // Array of numbers from zero to nine
        $numbers = [
            '3','6','0','9','4',
            '8','7','1','5','2'
        ];

        $alphanumeric = array_merge( $alphabets, $numbers );

        // Append zero in front if single digit ( 1 => 01 )
        $trademark_id = str_pad( $trademark_id, 2, '0', STR_PAD_LEFT );

        // Array of hashes in each number
        $hashes = array(
            '0' => 'A4LE7PODJ6',
            '1' => 'MZCB2CAORA',
            '2' => 'Z2RPQ3FF3K',
            '3' => 'JEP64PUZB0',
            '4' => 'AQCYA7WIDG',
            '5' => 'B7GH0MZAB2',
            '6' => 'U5MLHBTKVP',
            '7' => 'IHYZCTIQ9C',
            '8' => 'XZ8LHVK12C',
            '9' => 'FQQHHND5O4',
        );

        for ( $i = 0; $i < 5; $i++ ) {

            $len = strlen( $trademark_id );
            $x = $y = $z = $w = false;
            $hash = "";
            
            for ( $i = 0; $i < $len; $i++ ) {

                for ( $j = 0; $j < 10; $j++ ) {

                    if ( $trademark_id[$i] == $j ) {

                        $hash .= $hashes[$j];
                    }
                }
            }

            // Convert the given number to md5 hash, md5 will return 32 hexadecimal characters
            $hash = strtoupper( md5( $hash ) );
            
            $prefix = substr( $alphabets[$numbers[rand(0,9)]], 0, 1 ) . '-';

            $multiplier = ($len * 4) / 2 + 1;

            $first_str  = substr( $hash, $multiplier, 3 );

            $second_str = substr( $hash, 0, 4 );

            $second  = $alphanumeric[ rand(0, 35) ];
            $third   = $alphanumeric[ rand(0, 35) ];
            $fourth  = $alphanumeric[ rand(0, 35) ];
            $fifth   = $numbers[ rand( 0,9 ) ];

            $case_number = strtoupper(  $first_str . '-' . $second . $third . $fourth . $fifth );

            if ( strpos( $case_number, 'X' ) !== false ) {

                $x = true;
            }

            if ( strpos( $case_number, '5' ) !== false ) {

                if ( $x === true ) {

                    continue;
                }
            }

            if ( strpos( $case_number, '0' ) !== false ) {

                $z = true;
            }

            if ( strpos( $case_number, 'O' ) !== false ) {

                if ( $z === true ) {

                    continue;
                }
            }

            $result = Cases::where( 'code', 'like', $case_number )->get();

            if ( !$result ) {

                break;
            }
        }

        $result = Cases::create(

                        ['code' => $case_number, 'hash' => '']
                    );

        return $case_number;
    }

    private static function str_reverse( $string )
    {
        $o = '';

        for ( $i = strlen( $string ) - 1; $i >= 0; $i-- ) {

            $o .= $string[$i];
        }

        return $o;
    }

    private static function str_pad( $n, $width )
    {
        $z = 0;
        $n .= '';

        return strlen( $n ) >= $width ? $n : Utils::append_zero( $width - strlen($n) + 1 ) . $n;
    }

    private static function append_zero( $width )
    {
        $str = "";

        for ( $i = 1; $i < $width; $i++ ) {
            $str .= "0";
        }

        return $str;
    }

    public static function gen_case( $number )
    {
        $random = rand( 1, 2 );

        // Array of uppercase alphabets from A-Z without O and X
        $alphabets = [
            'B','D','U','S','Y',
            'F','H','I','J','W',
            'L','A','P','Q','R',
            'V','C','G','E','M',
            'N','Z','K','T',
        ];

        // Array of numbers from zero to nine
        $numbers = [
            '3','6','9','4',
            '8','7','1','2',
        ];

        if ( $random == 1 ) {

            array_push( $alphabets, 'X');
            array_push( $numbers, '0');

        } else {

            array_push( $alphabets, 'O');
            array_push( $numbers, '5');
        }

        $alphanumeric = array_merge( $alphabets, $numbers );

        $case_number  = "E8-L5PU-6";
        $data         = [];

        // for ( $i = 0; $i < 10; $i++ ) {

            $success = true; // 1,676,676,762
            $first   = $alphanumeric[ rand( 0, count( $alphanumeric ) - 1 ) ];
            $second  = $alphanumeric[ rand( 0, count( $alphanumeric ) - 1 ) ] . '-';
            $third   = $alphabets[ rand( 0, count( $alphabets ) - 1 ) ];
            $fourth  = $numbers[ rand( 0, count( $numbers ) - 1 ) ];
            $fifth   = $alphanumeric[ rand( 0, count( $alphanumeric ) - 1 ) ];
            $sixth   = $alphabets[ rand( 0, count( $alphabets ) - 1 ) ] . '-';
            $seventh = $numbers[ rand( 0, count( $numbers ) - 1 ) ];

            $case_number = $first . $second . $third . $fourth . $fifth . $sixth . $seventh;

        //     if ( Cases::check_duplicate_action( $case_number ) ) {

        //         $success = false;
        //     }

        //     if ( $success ) {

        //         break;
        //     }
        // }

        // if ( strlen( $case_number ) > 1 ) {

        //     Cases::create([
        //         'action_code' => $case_number,
        //     ]);
        // }

        return $case_number;
        
    }

    public static function gen_order( $id )
    {
        $random = rand( 1, 2 );

        // Array of uppercase alphabets from A-Z without O and X
        $alphabets = [
            'B','D','U','S','Y',
            'F','H','I','J','W',
            'L','A','P','Q','R',
            'V','C','G','E','M',
            'N','Z','K','T',
        ];

        // Array of numbers from zero to nine
        $numbers = [
            '3','6','9','4',
            '8','7','1','2',
        ];

        if ( $random == 1 ) {

            array_push( $alphabets, 'X');
            array_push( $numbers, '0');

        } else {

            array_push( $alphabets, 'O');
            array_push( $numbers, '5');
        }

        $alphanumeric = array_merge( $alphabets, $numbers );
        $order_number  = "SS62-8M";

        // for ( $i = 0; $i < 10; $i++ ) {

            $success = true; // 76,406,976
            $first   = $alphanumeric[ rand( 0, count( $alphanumeric ) - 1 ) ];
            $second  = $alphanumeric[ rand( 0, count( $alphanumeric ) - 1 ) ];
            $third   = $numbers[ rand( 0, count( $numbers ) - 1 ) ];
            $fourth  = $alphanumeric[ rand( 0, count( $alphanumeric ) - 1 ) ]. '-';
            $fifth   = $numbers[ rand( 0, count( $numbers ) - 1 ) ];
            $sixth   = $alphabets[ rand( 0, count( $alphabets ) - 1 ) ];

            $order_number = $first . $second . $third . $fourth . $fifth . $sixth;

        //     if ( Cases::check_duplicate_order( $order_number ) ) {

        //         $success = false;
        //     }

        //     if ( $success ) {

        //         break;
        //     }
        // }

        // if ( strlen( $order_number ) > 1 ) {
        //     $case = Cases::find( $id );

        //     $case->order_code = $order_number;

        //     $case->save();
        // }

        return $order_number;
    }

    public static function gen_customer( $id )
    {
        $random = rand( 1, 2 );

        // Array of uppercase alphabets from A-Z without O and X
        $alphabets = [
            'B','D','U','S','Y',
            'F','H','I','J','W',
            'L','A','P','Q','R',
            'V','C','G','E','M',
            'N','Z','K','T',
        ];

        // Array of numbers from zero to nine
        $numbers = [
            '3','6','9','4',
            '8','7','1','2',
        ];

        if ( $random == 1 ) {

            array_push( $alphabets, 'X');
            array_push( $numbers, '0');

        } else {

            array_push( $alphabets, 'O');
            array_push( $numbers, '5');
        }

        $alphanumeric = array_merge( $alphabets, $numbers );
        $customer_number  = "ZX-MZE";

        // for ( $i = 0; $i < 10; $i++ ) {

            $success = true; // 39, 135, 393
            $first   = $alphanumeric[ rand( 0, count( $alphanumeric ) - 1 ) ];
            $second  = $alphanumeric[ rand( 0, count( $alphanumeric ) - 1 ) ]. '-';
            $third   = $alphanumeric[ rand( 0, count( $alphanumeric ) - 1 ) ];
            $fourth  = $alphanumeric[ rand( 0, count( $alphanumeric ) - 1 ) ];
            $fifth   = $alphanumeric[ rand( 0, count( $alphanumeric ) - 1 ) ];

            $customer_number = $first . $second . $third . $fourth . $fifth;

        //     if ( Cases::check_duplicate_customer( $customer_number ) ) {

        //         $success = false;
        //     }

        //     if ( $success ) {

        //         break;
        //     }
        // }

        // if ( strlen( $customer_number ) > 1 ) {
        //     $case = Cases::find( $id );

        //     $case->customer_code = $customer_number;

        //     $case->save();
        // }

        return $customer_number;
    }

    public static function case_gen( $number )
    {
        $r   = $number;
        $r   = Utils::str_reverse( Utils::str_pad( $r, 6 ) );

        
        $n   = (int) $r;
        $r1  = $n % 138240;



        $of1 =  ($n - $r1) / 138240;

        $r2  = $r1 % 57600;


        $of2 = ($r1 - $r2) / 57600;

        $r3  = $r2 % 2600;

        $of3 = ($r2 - $r3) / 2600;


        $of1 = substr( self::$alpha, $of1, 1 ) == "" ? rand(0,9) : substr( self::$alpha, $of1, 1 );

        $pad = Utils::str_pad( (string) $r3, 4 );


        $str  = substr( self::$alpha, substr( $pad, 0, 1), 1);
        $str .= substr( $pad, 1, 1);
        $str .= substr( self::$alpha, substr( $pad, 2, 1), 1);
        $str .= substr( $pad, 3, 1);

        $r = substr( self::$alpha, $of3, 1 ) . '-' . $str . '-' . substr( self::$alpha, $of2, 1 ) . $of1;

        $caseNumber = Utils::str_reverse( $r ); 

        // CHECK IF CASE ALREADY EXIST ELSE CALL THIS FUNCTION AGAIN
        $actionCode = ActionCode::where('case_number',$caseNumber)->first();

        if ($actionCode) {
            // dd($actionCode);
            // $this->case_gen($number);
        }

        return $caseNumber;
    }

    public static function order_gen( $number )
    {
        $r   = $number;
        $r   = Utils::str_reverse( Utils::str_pad( $r, 5 ) );
        $n   = (int) $r;
        $r1  = $n % 138240;
        $of1 =  ($n - $r1) / 138240;
        $r2  = $r1 % 57600;
        $of2 = ($r1 - $r2) / 57600;
        $r3  = $r2 % 2600;
        $of3 = ($r2 - $r3) / 2600;

        $order  = substr( self::$alpha, $of3, 1 );
        $order .= substr( Utils::str_pad( (string) $r3, 4 ), 0, 1 );
        $order .= substr( self::$alpha, $of1, 1 );
        $order .= substr( Utils::str_pad( (string) $r3, 4 ), 1, 1 ) . '-';
        $order .= substr( self::$alpha, $of2, 1 );
        $order .= substr( Utils::str_pad( (string) $r3, 4 ), 2, 1 );

        return $order;
    }

    public static function customer_gen( $number )
    {
        $r  = $number;
        $r  = Utils::str_reverse( Utils::str_pad( $r, 4 ) );
        $n  = (int) $r;
        $r1 = $n % 138240;
        $of1 =  ($n - $r1) / 138240;
        $r2 = $r1 % 57600;
        $of2 = ($r1 - $r2) / 57600;
        $r3 = $r2 % 2600;
        $of3 = ($r2 - $r3) / 2600;

        $cus  = substr( self::$alpha, $of2, 1 );
        $cus .= substr( self::$alpha, $of3, 1 ) . '-';
        $cus .= substr( Utils::str_pad( (string) $r3, 4 ), 0, 1 );
        $cus .= substr( self::$alpha, $of1, 1 );
        $cus .= substr( Utils::str_pad( (string) $r3, 4 ), 1, 1 );

        return $cus;
    }

    public static function search_codes( $id )
    {
        $cases = Cases::find($id);

        return $cases;
    }

    public static function check_pattern( $regex, $code )
    {
        preg_match_all( $regex, $code, $matches, PREG_PATTERN_ORDER );

        return $matches;
    }   

    public static function resolve_codes( $code, $type )
    {
        $title = "Case number";

        if ( $type == 1 ) {

            $regex = "/(\w)-(\w+)-(\d)/";

            $res = Utils::check_pattern( "/(\w)-(\w+)-(\d)/", $code );

            if ( count( $res ) < 4 ) {

                return "Invalid code format";
            }

            $result = Cases::where( 'action_code', 'like', '%' . $code . '%' )->get();
        }

        if ( $type == 2 ) {

            $res = Utils::check_pattern( "/(\w+)-(\w+)/", $code );

            if ( count( $res ) < 3 ) {

                return "Invalid code format";
            }
            
            $result = Cases::where( 'order_code', 'like', '%' . $code . '%' )->get();
            $title = "Order Number";
        }

        if ( $type == 3 ) {

            $res = Utils::check_pattern( "/(\w+)-(\w)/", $code );

            if ( count( $res ) < 3 ) {

                return "Invalid code format";
            }
            
            $result = Cases::where( 'customer_code', 'like', '%' . $code . '%' )->get();
            $title = "Customer Number";
        }
        

        if ( count( $result ) > 0 ) {

            return "Sequential ID is " . $result[0]->id;
        }

        return $title . " did not exist!";
    }

    public static function resolve_case( $case )
    {   
        $case = Utils::str_reverse( $case );

        $ret     = "0";
        $regex   = "/(\w)-(\w+)-(\w+)/";

        preg_match_all( $regex, $case, $matches, PREG_PATTERN_ORDER );

        if ( count( $matches[1] ) == 0 ) {

            return $ret = "Invalid case number";
        }

         if ( count( $matches ) > 3 ) {

            try {

                $ofs1 = strpos( (string) self::$alpha, implode("", $matches[1]) );

                $temp = implode("", $matches[3] );

                $ofs2 = strpos( (string) self::$alpha, substr( $temp, 0, 1) );

                if ( substr( $temp, 1, 1) == "" ) {

                    return $ret = "Case number not found!";

                } else {

                    $ofs3 = strpos( (string) self::$alpha, substr( $temp, 1, 1) );
                }

                $str = implode("", $matches[2] );

                $r  = strpos( (string) self::$alpha, substr( $str, 0, 1 ) );
                $r .= substr( $str, 1, 1);
                $r .= strpos( (string) self::$alpha, substr( $str, 2, 1 ) );
                $r .= substr( $str, 3, 1);

                $r = (int) $r;

                $temp = $ofs3 * 138240 + $ofs2 * 57600 + $ofs1 * 2600 + $r;

                $ret = Utils::str_reverse( Utils::str_pad( (string) $temp, 6 ) );

            } catch ( Exception $e ) {

                return "Case number not found!";
            }
         }

         return (int) $ret;
    }

    public static function get_order_number( $order_id )
    {
        $result = Cases::find( $order_id );

        if ( $result ) {

            return $result->order_code;
        }

        return -1;
    }

    public static function get_customer_number( $customer_id )
    {
        $result = Cases::find( $customer_id );

        if ( $result ) {

            return $result->customer_code;
        }

        return -1;
    }
}