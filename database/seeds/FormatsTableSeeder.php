<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class FormatsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('formats')->insert([
        //     'country_id'   => 1,
        //     'input_type'   => 0,
        //     'poa_required' => 0,
        //     'trade_type'   => 'w',
        //     'word_label'   => 'Word Mark',
        //     'commerce_use' => 0,
        //     'has_article'  => 0,
        //     'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
        //     'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        // ]);

        // DB::table('orders')->insert([
        //     'order_number'   => '4W0D-4H',
        //     'total_items'    => 1,
        //     'total_amount'   => '68',
        //     'user_id'       => 1,
        //     'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
        //     'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        // ]);

        // DB::table('trademarks')->insert([
        //     'user_id'      => 1,
        //     'country_id'   => 3,
        //     'order_id'     => 1,
        //     'profile_id'   => 'w',
        //     'word_label'   => 'Word Mark',
        //     'commerce_use' => 0,
        //     'has_article'  => 0,
        //     'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
        //     'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        // ]);
    }
}
