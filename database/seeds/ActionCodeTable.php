<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Repositories\Utils;
use App\ActionCode;

class ActionCodeTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ( range(1,78754) as $index ) {
            
            try {

                $action = ActionCode::find( $index );

                $action->case_number = Utils::case_gen( $index );

                $action->save();

                echo "\nSuccess \t" . $action->case_number . "\t" . $index;

            } catch ( Exception $e ) {

                echo "\nError" . $index;
            }
        }
    }
}
