<?php

use Illuminate\Database\Seeder;

class UserRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_roles')->insert([
            'name' => 'Customer',
            'slug' => 'customer'
        ]);

        DB::table('user_roles')->insert([
            'name' => 'Affiliate',
            'slug' => 'affiliate'
        ]);

        DB::table('user_roles')->insert([
            'name' => 'Partner',
            'slug' => 'partner'
        ]);
    }
}
