<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Repositories\Utils;

class ActionCasesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('action_code_types')->insert([
        	'description'  => 1,
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('action_code_types')->insert([
        	'description'  => 2,
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('action_code_types')->insert([
        	'description'  => 3,
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('action_code_types')->insert([
        	'description'  => 4,
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('action_code_types')->insert([
        	'description'  => 5,
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('action_code_types')->insert([
        	'description'  => 6,
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('action_code_types')->insert([
            'description'  => 'Default or Brexit',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('action_code_types')->insert([
            'description'  => 'Priority Filing',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
