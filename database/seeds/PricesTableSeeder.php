<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PricesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('prices')->insert([
            'country_id'          => 99,
            'service_type'        => 'Study',
            'initial_cost'        => 87.00,
            'additional_cost'     => 87.00,
            'logo_initial_cost'   => 170.00,
            'logo_additional_cost'=> 170.00,
            'tax'                 => 0.00,
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 99,
            'service_type'         => 'Registration',
            'initial_cost'         => 685.00,
            'additional_cost'      => 0.00,
            'logo_initial_cost'    => 685.00,
            'logo_additional_cost' => 0.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 99,
            'service_type'         => 'Certificate',
            'initial_cost'         => 432.00,
            'additional_cost'      => 0.00,
            'logo_initial_cost'    => 432.00,
            'logo_additional_cost' => 0.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 100,
            'service_type'        => 'Study',
            'initial_cost'        => 53.00,
            'additional_cost'     => 53.00,
            'logo_initial_cost'    => 68.00,
            'logo_additional_cost' => 68.00,
            'tax'                 => 0.00,
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 100,
            'service_type'         => 'Registration',
            'initial_cost'         => 543.00,
            'additional_cost'      => 543.00,
            'logo_initial_cost'    => 543.00,
            'logo_additional_cost' => 543.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 100,
            'service_type'         => 'Certificate',
            'initial_cost'         => 0.00,
            'additional_cost'      => 0.00,
            'logo_initial_cost'    => 0.00,
            'logo_additional_cost' => 0.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 101,
            'service_type'        => 'Study',
            'initial_cost'        => 81.00,
            'additional_cost'     => 81.00,
            'logo_initial_cost'    => 144.00,
            'logo_additional_cost' => 144.00,
            'tax'                 => 0.00,
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
           	'country_id'          => 101,
            'service_type'         => 'Registration',
            'initial_cost'         => 427.00,
            'additional_cost'      => 400.00,
            'logo_initial_cost'    => 427.00,
            'logo_additional_cost' => 400.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 101,
            'service_type'         => 'Certificate',
            'initial_cost'         => 0.00,
            'additional_cost'      => 0.00,
            'logo_initial_cost'    => 0.00,
            'logo_additional_cost' => 0.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 114,
            'service_type'        => 'Study',
            'initial_cost'        => 175.00,
            'additional_cost'     => 116.00,
            'logo_initial_cost'    => 175.00,
            'logo_additional_cost' => 116.00,
            'tax'                 => 0.00,
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 114,
            'service_type'         => 'Registration',
            'initial_cost'         => 966.00,
            'additional_cost'      => 419.00,
            'logo_initial_cost'    => 966.00,
            'logo_additional_cost' => 419.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 114,
            'service_type'         => 'Certificate',
            'initial_cost'         => 0.00,
            'additional_cost'      => 0.00,
            'logo_initial_cost'    => 0.00,
            'logo_additional_cost' => 0.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 115,
            'service_type'        => 'Study',
            'initial_cost'        => 243.00,
            'additional_cost'     => 116.00,
            'logo_initial_cost'    => 243.00,
            'logo_additional_cost' => 116.00,
            'tax'                 => 0.00,
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 115,
            'service_type'         => 'Registration',
            'initial_cost'         => 1086.00,
            'additional_cost'      => 138.00,
            'logo_initial_cost'    => 1086.00,
            'logo_additional_cost' => 138.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 115,
            'service_type'         => 'Certificate',
            'initial_cost'         => 0.00,
            'additional_cost'      => 0.00,
            'logo_initial_cost'    => 0.00,
            'logo_additional_cost' => 0.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 116,
            'service_type'        => 'Study',
            'initial_cost'        => 121.00,
            'additional_cost'     => 121.00,
            'logo_initial_cost'    => 121.00,
            'logo_additional_cost' => 121.00,
            'tax'                 => 0.00,
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 116,
            'service_type'         => 'Registration',
            'initial_cost'         => 1150.00,
            'additional_cost'      => 745.00,
            'logo_initial_cost'    => 1150.00,
            'logo_additional_cost' => 745.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 116,
            'service_type'         => 'Certificate',
            'initial_cost'         => 0.00,
            'additional_cost'      => 0.00,
            'logo_initial_cost'    => 0.00,
            'logo_additional_cost' => 0.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 117,
            'service_type'        => 'Study',
            'initial_cost'        => 175.00,
            'additional_cost'     => 116.00,
            'logo_initial_cost'    => 175.00,
            'logo_additional_cost' => 116.00,
            'tax'                 => 0.00,
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 117,
            'service_type'         => 'Registration',
            'initial_cost'         => 1500.00,
            'additional_cost'      => 1380.00,
            'logo_initial_cost'    => 1500.00,
            'logo_additional_cost' => 1380.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 117,
            'service_type'         => 'Certificate',
            'initial_cost'         => 0.00,
            'additional_cost'      => 0.00,
            'logo_initial_cost'    => 0.00,
            'logo_additional_cost' => 0.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 118,
            'service_type'        => 'Study',
            'initial_cost'        => 116.00,
            'additional_cost'     => 97.00,
            'logo_initial_cost'    => 116.00,
            'logo_additional_cost' => 97.00,
            'tax'                 => 0.00,
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 118,
            'service_type'         => 'Registration',
            'initial_cost'         => 667.00,
            'additional_cost'      => 368.00,
            'logo_initial_cost'    => 667.00,
            'logo_additional_cost' => 368.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 118,
            'service_type'         => 'Certificate',
            'initial_cost'         => 0.00,
            'additional_cost'      => 0.00,
            'logo_initial_cost'    => 0.00,
            'logo_additional_cost' => 0.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 119,
            'service_type'        => 'Study',
            'initial_cost'        => 146.00,
            'additional_cost'     => 87.00,
            'logo_initial_cost'    => 146.00,
            'logo_additional_cost' => 87.00,
            'tax'                 => 0.00,
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 119,
            'service_type'         => 'Registration',
            'initial_cost'         => 1113.00,
            'additional_cost'      => 212.00,
            'logo_initial_cost'    => 1113.00,
            'logo_additional_cost' => 212.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 119,
            'service_type'         => 'Certificate',
            'initial_cost'         => 0.00,
            'additional_cost'      => 0.00,
            'logo_initial_cost'    => 0.00,
            'logo_additional_cost' => 0.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 120,
            'service_type'        => 'Study',
            'initial_cost'        => 78.00,
            'additional_cost'     => 58.00,
            'logo_initial_cost'    => 78.00,
            'logo_additional_cost' => 58.00,
            'tax'                 => 0.00,
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 120,
            'service_type'         => 'Registration',
            'initial_cost'         => 455.00,
            'additional_cost'      => 442.00,
            'logo_initial_cost'    => 465.00,
            'logo_additional_cost' => 442.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 120,
            'service_type'         => 'Certificate',
            'initial_cost'         => 0.00,
            'additional_cost'      => 0.00,
            'logo_initial_cost'    => 0.00,
            'logo_additional_cost' => 0.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 121,
            'service_type'        => 'Study',
            'initial_cost'        => 121.00,
            'additional_cost'     => 121.00,
            'logo_initial_cost'    => 121.00,
            'logo_additional_cost' => 121.00,
            'tax'                 => 0.00,
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 121,
            'service_type'         => 'Registration',
            'initial_cost'         => 1168.00,
            'additional_cost'      => 543.00,
            'logo_initial_cost'    => 1168.00,
            'logo_additional_cost' => 543.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 121,
            'service_type'         => 'Certificate',
            'initial_cost'         => 0.00,
            'additional_cost'      => 0.00,
            'logo_initial_cost'    => 0.00,
            'logo_additional_cost' => 0.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 122,
            'service_type'        => 'Study',
            'initial_cost'        => 82.00,
            'additional_cost'     => 82.00,
            'logo_initial_cost'    => 82.00,
            'logo_additional_cost' => 82.00,
            'tax'                 => 0.00,
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 122,
            'service_type'         => 'Registration',
            'initial_cost'         => 432.00,
            'additional_cost'      => 377.00,
            'logo_initial_cost'    => 432.00,
            'logo_additional_cost' => 377.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 122,
            'service_type'         => 'Certificate',
            'initial_cost'         => 0.00,
            'additional_cost'      => 0.00,
            'logo_initial_cost'    => 0.00,
            'logo_additional_cost' => 0.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 123,
            'service_type'        => 'Study',
            'initial_cost'        => 78.00,
            'additional_cost'     => 78.00,
            'logo_initial_cost'    => 78.00,
            'logo_additional_cost' => 78.00,
            'tax'                 => 0.00,
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 123,
            'service_type'         => 'Registration',
            'initial_cost'         => 727.00,
            'additional_cost'      => 460.00,
            'logo_initial_cost'    => 727.00,
            'logo_additional_cost' => 460.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 123,
            'service_type'         => 'Certificate',
            'initial_cost'         => 0.00,
            'additional_cost'      => 0.00,
            'logo_initial_cost'    => 0.00,
            'logo_additional_cost' => 0.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 124,
            'service_type'        => 'Study',
            'initial_cost'        => 146.00,
            'additional_cost'     => 146.00,
            'logo_initial_cost'    => 146.00,
            'logo_additional_cost' => 146.00,
            'tax'                 => 0.00,
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 124,
            'service_type'         => 'Registration',
            'initial_cost'         => 1932.00,
            'additional_cost'      => 1072.00,
            'logo_initial_cost'    => 1932.00,
            'logo_additional_cost' => 1072.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 124,
            'service_type'         => 'Certificate',
            'initial_cost'         => 0.00,
            'additional_cost'      => 0.00,
            'logo_initial_cost'    => 0.00,
            'logo_additional_cost' => 0.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 125,
            'service_type'        => 'Study',
            'initial_cost'        => 78.00,
            'additional_cost'     => 78.00,
            'logo_initial_cost'    => 116.00,
            'logo_additional_cost' => 116.00,
            'tax'                 => 0.00,
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 125,
            'service_type'         => 'Registration',
            'initial_cost'         => 810.00,
            'additional_cost'      => 515.00,
            'logo_initial_cost'    => 810.00,
            'logo_additional_cost' => 515.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 125,
            'service_type'         => 'Certificate',
            'initial_cost'         => 0.00,
            'additional_cost'      => 0.00,
            'logo_initial_cost'    => 0.00,
            'logo_additional_cost' => 0.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 126,
            'service_type'        => 'Study',
            'initial_cost'        => 121.00,
            'additional_cost'     => 121.00,
            'logo_initial_cost'    => 121.00,
            'logo_additional_cost' => 121.00,
            'tax'                 => 0.00,
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 126,
            'service_type'         => 'Registration',
            'initial_cost'         => 626.00,
            'additional_cost'      => 437.00,
            'logo_initial_cost'    => 662.00,
            'logo_additional_cost' => 437.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 126,
            'service_type'         => 'Certificate',
            'initial_cost'         => 0.00,
            'additional_cost'      => 0.00,
            'logo_initial_cost'    => 0.00,
            'logo_additional_cost' => 0.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 127,
            'service_type'        => 'Study',
            'initial_cost'        => 53.00,
            'additional_cost'     => 53.00,
            'logo_initial_cost'    => 131.00,
            'logo_additional_cost' => 131.00,
            'tax'                 => 0.00,
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 127,
            'service_type'         => 'Registration',
            'initial_cost'         => 566.00,
            'additional_cost'      => 566.00,
            'logo_initial_cost'    => 566.00,
            'logo_additional_cost' => 566.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 127,
            'service_type'         => 'Certificate',
            'initial_cost'         => 247.00,
            'additional_cost'      => 247.00,
            'logo_initial_cost'    => 247.00,
            'logo_additional_cost' => 247.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 128,
            'service_type'        => 'Study',
            'initial_cost'        => 121.00,
            'additional_cost'     => 121.00,
            'logo_initial_cost'    => 121.00,
            'logo_additional_cost' => 121.00,
            'tax'                 => 0.00,
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 128,
            'service_type'         => 'Registration',
            'initial_cost'         => 948.00,
            'additional_cost'      => 212.00,
            'logo_initial_cost'    => 948.00,
            'logo_additional_cost' => 212.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 128,
            'service_type'         => 'Certificate',
            'initial_cost'         => 194.00,
            'additional_cost'      => 0.00,
            'logo_initial_cost'    => 194.00,
            'logo_additional_cost' => 0.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 129,
            'service_type'        => 'Study',
            'initial_cost'        => 175.00,
            'additional_cost'     => 116.00,
            'logo_initial_cost'    => 175.00,
            'logo_additional_cost' => 116.00,
            'tax'                 => 0.00,
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 129,
            'service_type'         => 'Registration',
            'initial_cost'         => 791.00,
            'additional_cost'      => 345.00,
            'logo_initial_cost'    => 791.00,
            'logo_additional_cost' => 345.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 129,
            'service_type'         => 'Certificate',
            'initial_cost'         => 0.00,
            'additional_cost'      => 0.00,
            'logo_initial_cost'    => 0.00,
            'logo_additional_cost' => 0.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 130,
            'service_type'        => 'Study',
            'initial_cost'        => 58.00,
            'additional_cost'     => 58.00,
            'logo_initial_cost'    => 68.00,
            'logo_additional_cost' => 68.00,
            'tax'                 => 0.00,
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 130,
            'service_type'         => 'Registration',
            'initial_cost'         => 432.00,
            'additional_cost'      => 313.00,
            'logo_initial_cost'    => 497.00,
            'logo_additional_cost' => 340.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 130,
            'service_type'         => 'Certificate',
            'initial_cost'         => 0.00,
            'additional_cost'      => 0.00,
            'logo_initial_cost'    => 0.00,
            'logo_additional_cost' => 0.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 131,
            'service_type'        => 'Study',
            'initial_cost'        => 87.00,
            'additional_cost'     => 78.00,
            'logo_initial_cost'    => 175.00,
            'logo_additional_cost' => 155.00,
            'tax'                 => 0.00,
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 131,
            'service_type'         => 'Registration',
            'initial_cost'         => 432.00,
            'additional_cost'      => 386.00,
            'logo_initial_cost'    => 432.00,
            'logo_additional_cost' => 386.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 131,
            'service_type'         => 'Certificate',
            'initial_cost'         => 0.00,
            'additional_cost'      => 0.00,
            'logo_initial_cost'    => 0.00,
            'logo_additional_cost' => 0.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 132,
            'service_type'        => 'Study',
            'initial_cost'        => 102.00,
            'additional_cost'     => 82.00,
            'logo_initial_cost'    => 102.00,
            'logo_additional_cost' => 82.00,
            'tax'                 => 0.00,
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 132,
            'service_type'         => 'Registration',
            'initial_cost'         => 593.00,
            'additional_cost'      => 465.00,
            'logo_initial_cost'    => 593.00,
            'logo_additional_cost' => 465.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 132,
            'service_type'         => 'Certificate',
            'initial_cost'         => 102.00,
            'additional_cost'      => 102.00,
            'logo_initial_cost'    => 102.00,
            'logo_additional_cost' => 102.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 133,
            'service_type'        => 'Study',
            'initial_cost'        => 121.00,
            'additional_cost'     => 121.00,
            'logo_initial_cost'    => 121.00,
            'logo_additional_cost' => 121.00,
            'tax'                 => 0.00,
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 133,
            'service_type'         => 'Registration',
            'initial_cost'         => 1086.00,
            'additional_cost'      => 414.00,
            'logo_initial_cost'    => 1086.00,
            'logo_additional_cost' => 414.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 133,
            'service_type'         => 'Certificate',
            'initial_cost'         => 0.00,
            'additional_cost'      => 0.00,
            'logo_initial_cost'    => 0.00,
            'logo_additional_cost' => 0.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 134,
            'service_type'        => 'Study',
            'initial_cost'        => 116.00,
            'additional_cost'     => 78.00,
            'logo_initial_cost'    => 116.00,
            'logo_additional_cost' => 78.00,
            'tax'                 => 0.00,
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 134,
            'service_type'         => 'Registration',
            'initial_cost'         => 869.00,
            'additional_cost'      => 828.00,
            'logo_initial_cost'    => 869.00,
            'logo_additional_cost' => 828.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 134,
            'service_type'         => 'Certificate',
            'initial_cost'         => 0.00,
            'additional_cost'      => 0.00,
            'logo_initial_cost'    => 0.00,
            'logo_additional_cost' => 0.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 135,
            'service_type'        => 'Study',
            'initial_cost'        => 175.00,
            'additional_cost'     => 78.00,
            'logo_initial_cost'    => 175.00,
            'logo_additional_cost' => 78.00,
            'tax'                 => 0.00,
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 135,
            'service_type'         => 'Registration',
            'initial_cost'         => 957.00,
            'additional_cost'      => 322.00,
            'logo_initial_cost'    => 957.00,
            'logo_additional_cost' => 322.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 135,
            'service_type'         => 'Certificate',
            'initial_cost'         => 0.00,
            'additional_cost'      => 0.00,
            'logo_initial_cost'    => 0.00,
            'logo_additional_cost' => 0.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 136,
            'service_type'        => 'Study',
            'initial_cost'        => 175.00,
            'additional_cost'     => 116.00,
            'logo_initial_cost'    => 175.00,
            'logo_additional_cost' => 116.00,
            'tax'                 => 0.00,
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 136,
            'service_type'         => 'Registration',
            'initial_cost'         => 1012.00,
            'additional_cost'      => 120.00,
            'logo_initial_cost'    => 1012.00,
            'logo_additional_cost' => 120.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 136,
            'service_type'         => 'Certificate',
            'initial_cost'         => 0.00,
            'additional_cost'      => 0.00,
            'logo_initial_cost'    => 0.00,
            'logo_additional_cost' => 0.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 137,
            'service_type'        => 'Study',
            'initial_cost'        => 121.00,
            'additional_cost'     => 78.00,
            'logo_initial_cost'    => 121.00,
            'logo_additional_cost' => 78.00,
            'tax'                 => 0.00,
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 137,
            'service_type'         => 'Registration',
            'initial_cost'         => 1693.00,
            'additional_cost'      => 980.00,
            'logo_initial_cost'    => 1693.00,
            'logo_additional_cost' => 980.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 137,
            'service_type'         => 'Certificate',
            'initial_cost'         => 0.00,
            'additional_cost'      => 0.00,
            'logo_initial_cost'    => 0.00,
            'logo_additional_cost' => 0.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 138,
            'service_type'        => 'Study',
            'initial_cost'        => 121.00,
            'additional_cost'     => 121.00,
            'logo_initial_cost'    => 121.00,
            'logo_additional_cost' => 121.00,
            'tax'                 => 0.00,
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 138,
            'service_type'         => 'Registration',
            'initial_cost'         => 1053.00,
            'additional_cost'      => 538.00,
            'logo_initial_cost'    => 1053.00,
            'logo_additional_cost' => 538.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 138,
            'service_type'         => 'Certificate',
            'initial_cost'         => 0.00,
            'additional_cost'      => 0.00,
            'logo_initial_cost'    => 0.00,
            'logo_additional_cost' => 0.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 45,
            'service_type'        => 'Study',
            'initial_cost'        => 78.00,
            'additional_cost'     => 78.00,
            'logo_initial_cost'    => 78.00,
            'logo_additional_cost' => 78.00,
            'tax'                 => 0.00,
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 45,
            'service_type'         => 'Registration',
            'initial_cost'         => 488.00,
            'additional_cost'      => 83.00,
            'logo_initial_cost'    => 488.00,
            'logo_additional_cost' => 83.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 45,
            'service_type'         => 'Certificate',
            'initial_cost'         => 272.00,
            'additional_cost'      => 0.00,
            'logo_initial_cost'    => 272.00,
            'logo_additional_cost' => 0.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 46,
            'service_type'        => 'Study',
            'initial_cost'        => 124.00,
            'additional_cost'     => 76.00,
            'logo_initial_cost'    => 178.00,
            'logo_additional_cost' => 118.00,
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 46,
            'service_type'         => 'Registration',
            'initial_cost'         => 926.00,
            'additional_cost'      => 138.00,
            'logo_initial_cost'    => 926.00,
            'logo_additional_cost' => 138.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 46,
            'service_type'         => 'Certificate',
            'initial_cost'         => 0.00,
            'additional_cost'      => 0.00,
            'logo_initial_cost'    => 0.00,
            'logo_additional_cost' => 0.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 47,
            'service_type'        => 'Study',
            'initial_cost'        => 175.00,
            'additional_cost'     => 97.00,
            'logo_initial_cost'    => 291.00,
            'logo_additional_cost' => 136.00,
            'tax'                 => 0.00,
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 47,
            'service_type'         => 'Registration',
            'initial_cost'         => 580.00,
            'additional_cost'      => 115.00,
            'logo_initial_cost'    => 580.00,
            'logo_additional_cost' => 115.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 47,
            'service_type'         => 'Certificate',
            'initial_cost'         => 417.00,
            'additional_cost'      => 0.00,
            'logo_initial_cost'    => 417.00,
            'logo_additional_cost' => 0.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 48,
            'service_type'        => 'Study',
            'initial_cost'        => 78.00,
            'additional_cost'     => 78.00,
            'logo_initial_cost'    => 136.00,
            'logo_additional_cost' => 136.00,
            'tax'                 => 0.00,
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 48,
            'service_type'         => 'Registration',
            'initial_cost'         => 1794.00,
            'additional_cost'      => 156.00,
            'logo_initial_cost'    => 1794.00,
            'logo_additional_cost' => 156.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 48,
            'service_type'         => 'Certificate',
            'initial_cost'         => 0.00,
            'additional_cost'      => 0.00,
            'logo_initial_cost'    => 0.00,
            'logo_additional_cost' => 0.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 49,
            'service_type'        => 'Study',
            'initial_cost'        => 121.00,
            'additional_cost'     => 121.00,
            'logo_initial_cost'    => 121.00,
            'logo_additional_cost' => 121.00,
            'tax'                 => 0.00,
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 49,
            'service_type'         => 'Registration',
            'initial_cost'         => 699.00,
            'additional_cost'      => 92.00,
            'logo_initial_cost'    => 699.00,
            'logo_additional_cost' => 92.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 49,
            'service_type'         => 'Certificate',
            'initial_cost'         => 500.00,
            'additional_cost'      => 0.00,
            'logo_initial_cost'    => 500.00,
            'logo_additional_cost' => 0.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 50,
            'service_type'        => 'Study',
            'initial_cost'        => 121.00,
            'additional_cost'     => 121.00,
            'logo_initial_cost'    => 121.00,
            'logo_additional_cost' => 121.00,
            'tax'                 => 0.00,
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 50,
            'service_type'         => 'Registration',
            'initial_cost'         => 1012.00,
            'additional_cost'      => 216.00,
            'logo_initial_cost'    => 1012.00,
            'logo_additional_cost' => 216.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 50,
            'service_type'         => 'Certificate',
            'initial_cost'         => 558.00,
            'additional_cost'      => 0.00,
            'logo_initial_cost'    => 558.00,
            'logo_additional_cost' => 0.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

// THIS COUNTRY DOES NOT HAVE INDIVIDUAL TRADEMARKREGISTRATION. PRICES ARE BASED ON BENELUX TREATY.

		DB::table('prices')->insert([
            'country_id'          => 51,
            'service_type'        => 'Study',
            'initial_cost'        => 97.00,
            'additional_cost'     => 97.00,
            'logo_initial_cost'    => 173.00,
            'logo_additional_cost' => 173.00,
            'tax'                 => 0.00,
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 51,
            'service_type'         => 'Registration',
            'initial_cost'         => 545.00,
            'additional_cost'      => 144.00,
            'logo_initial_cost'    => 545.00,
            'logo_additional_cost' => 144.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 51,
            'service_type'         => 'Certificate',
            'initial_cost'         => 0.00,
            'additional_cost'      => 0.00,
            'logo_initial_cost'    => 0.00,
            'logo_additional_cost' => 0.00,
            'tax'                  => 0.00,
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 52,
            'service_type'        => 'Study',
            'initial_cost'        => '97.00',
            'additional_cost'     => '97.00',
            'logo_initial_cost'    => '173.00',
            'logo_additional_cost' => '173.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 52,
            'service_type'         => 'Registration',
            'initial_cost'         => '545.00',
            'additional_cost'      => '144.00',
            'logo_initial_cost'    => '545.00',
            'logo_additional_cost' => '144.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 52,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 53,
            'service_type'        => 'Study',
            'initial_cost'        => '116.00',
            'additional_cost'     => '78.00',
            'logo_initial_cost'    => '378.00',
            'logo_additional_cost' => '49.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 53,
            'service_type'         => 'Registration',
            'initial_cost'         => '690.00',
            'additional_cost'      => '110.00',
            'logo_initial_cost'    => '690.00',
            'logo_additional_cost' => '110.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 53,
            'service_type'         => 'Certificate',
            'initial_cost'         => '1019.00',
            'additional_cost'      => '116.00',
            'logo_initial_cost'    => '1019.00',
            'logo_additional_cost' => '116.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 54,
            'service_type'        => 'Study',
            'initial_cost'        => '76.00',
            'additional_cost'     => '76.00',
            'logo_initial_cost'    => '151.00',
            'logo_additional_cost' => '151.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 54,
            'service_type'         => 'Registration',
            'initial_cost'         => '514.00',
            'additional_cost'      => '66.00',
            'logo_initial_cost'    => '529.00',
            'logo_additional_cost' => '66.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 54,
            'service_type'         => 'Certificate',
            'initial_cost'         => '596.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '596.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 55,
            'service_type'        => 'Study',
            'initial_cost'        => '78.00',
            'additional_cost'     => '78.00',
            'logo_initial_cost'    => '155.00',
            'logo_additional_cost' => '155.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 55,
            'service_type'         => 'Registration',
            'initial_cost'         => '672.00',
            'additional_cost'      => '41.00',
            'logo_initial_cost'    => '672.00',
            'logo_additional_cost' => '41.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 55,
            'service_type'         => 'Certificate',
            'initial_cost'         => '698.00',
            'additional_cost'      => '78.00',
            'logo_initial_cost'    => '698.00',
            'logo_additional_cost' => '78.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 56,
            'service_type'        => 'Study',
            'initial_cost'        => '97.00',
            'additional_cost'     => '76.00',
            'logo_initial_cost'    => '97.00',
            'logo_additional_cost' => '76.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 56,
            'service_type'         => 'Registration',
            'initial_cost'         => '812.00',
            'additional_cost'      => '812.00',
            'logo_initial_cost'    => '812.00',
            'logo_additional_cost' => '812.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 56,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 57,
            'service_type'        => 'Study',
            'initial_cost'        => '78.00',
            'additional_cost'     => '78.00',
            'logo_initial_cost'    => '155.00',
            'logo_additional_cost' => '155.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 57,
            'service_type'         => 'Registration',
            'initial_cost'         => '754.00',
            'additional_cost'      => '124.00',
            'logo_initial_cost'    => '754.00',
            'logo_additional_cost' => '124.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 57,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 58,
            'service_type'        => 'Study',
            'initial_cost'        => '68.00',
            'additional_cost'     => '68.00',
            'logo_initial_cost'    => '116.00',
            'logo_additional_cost' => '116.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 58,
            'service_type'         => 'Registration',
            'initial_cost'         => '580.00',
            'additional_cost'      => '115.00',
            'logo_initial_cost'    => '580.00',
            'logo_additional_cost' => '115.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 58,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 59,
            'service_type'        => 'Study',
            'initial_cost'        => '65.00',
            'additional_cost'     => '65.00',
            'logo_initial_cost'    => '130.00',
            'logo_additional_cost' => '130.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 59,
            'service_type'         => 'Registration',
            'initial_cost'         => '638.00',
            'additional_cost'      => '123.00',
            'logo_initial_cost'    => '638.00',
            'logo_additional_cost' => '123.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'          => 59,
            'service_type'         => 'Certificate',
            'initial_cost'         => '411.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '411.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'          => 60,
            'service_type'        => 'Study',
            'initial_cost'        => '131.00',
            'additional_cost'     => '131.00',
            'logo_initial_cost'    => '335.00',
            'logo_additional_cost' => '335.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 60,
            'service_type'         => 'Registration',
            'initial_cost'         => '1337.00',
            'additional_cost'      => '185.00',
            'logo_initial_cost'    => '1337.00',
            'logo_additional_cost' => '185.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 60,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 61,
            'service_type'        => 'Study',
            'initial_cost'        => '78.00',
            'additional_cost'     => '78.00',
            'logo_initial_cost'    => '121.00',
            'logo_additional_cost' => '121.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 61,
            'service_type'         => 'Registration',
            'initial_cost'         => '1072.00',
            'additional_cost'      => '276.00',
            'logo_initial_cost'    => '1072.00',
            'logo_additional_cost' => '276.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 61,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 62,
            'service_type'        => 'Study',
            'initial_cost'        => '113.00',
            'additional_cost'     => '76.00',
            'logo_initial_cost'    => '162.00',
            'logo_additional_cost' => '124.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 62,
            'service_type'         => 'Registration',
            'initial_cost'         => '663.00',
            'additional_cost'      => '123.00',
            'logo_initial_cost'    => '663.00',
            'logo_additional_cost' => '123.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 62,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 63,
            'service_type'        => 'Study',
            'initial_cost'        => '87.00',
            'additional_cost'     => '58.00',
            'logo_initial_cost'    => '87.00',
            'logo_additional_cost' => '58.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 63,
            'service_type'         => 'Registration',
            'initial_cost'         => '690.00',
            'additional_cost'      => '166.00',
            'logo_initial_cost'    => '690.00',
            'logo_additional_cost' => '166.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 63,
            'service_type'         => 'Certificate',
            'initial_cost'         => '611.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '611.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 64,
            'service_type'        => 'Study',
            'initial_cost'        => '118.00',
            'additional_cost'     => '118.00',
            'logo_initial_cost'    => '243.00',
            'logo_additional_cost' => '245.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 64,
            'service_type'         => 'Registration',
            'initial_cost'         => '616.00',
            'additional_cost'      => '144.00',
            'logo_initial_cost'    => '616.00',
            'logo_additional_cost' => '144.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 64,
            'service_type'         => 'Certificate',
            'initial_cost'         => '130.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '130.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 65,
            'service_type'        => 'Study',
            'initial_cost'        => '146.00',
            'additional_cost'     => '95.00',
            'logo_initial_cost'    => '146.00',
            'logo_additional_cost' => '95.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 65,
            'service_type'         => 'Registration',
            'initial_cost'         => '465.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '465.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 65,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 66,
            'service_type'        => 'Study',
            'initial_cost'        => '76.00',
            'additional_cost'     => '76.00',
            'logo_initial_cost'    => '76.00',
            'logo_additional_cost' => '76.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 66,
            'service_type'         => 'Registration',
            'initial_cost'         => '638.00',
            'additional_cost'      => '169.00',
            'logo_initial_cost'    => '638.00',
            'logo_additional_cost' => '169.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 66,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 67,
            'service_type'        => 'Study',
            'initial_cost'        => '99.00',
            'additional_cost'     => '99.00',
            'logo_initial_cost'    => '217.00',
            'logo_additional_cost' => '217.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 67,
            'service_type'         => 'Registration',
            'initial_cost'         => '842.00',
            'additional_cost'      => '59.00',
            'logo_initial_cost'    => '842.00',
            'logo_additional_cost' => '59.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 67,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 68,
            'service_type'        => 'Study',
            'initial_cost'        => '59.00',
            'additional_cost'     => '59.00',
            'logo_initial_cost'    => '97.00',
            'logo_additional_cost' => '97.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 68,
            'service_type'         => 'Registration',
            'initial_cost'         => '1013.00',
            'additional_cost'      => '375.00',
            'logo_initial_cost'    => '1013.00',
            'logo_additional_cost' => '376.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 68,
            'service_type'         => 'Certificate',
            'initial_cost'         => '346.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '346.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 69,
            'service_type'        => 'Study',
            'initial_cost'        => '78.00',
            'additional_cost'     => '58.00',
            'logo_initial_cost'    => '78.00',
            'logo_additional_cost' => '58.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 69,
            'service_type'         => 'Registration',
            'initial_cost'         => '1311.00',
            'additional_cost'      => '152.00',
            'logo_initial_cost'    => '1311.00',
            'logo_additional_cost' => '152.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 69,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 70,
            'service_type'        => 'Study',
            'initial_cost'        => '124.00',
            'additional_cost'     => '124.00',
            'logo_initial_cost'    => '151.00',
            'logo_additional_cost' => '151.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 70,
            'service_type'         => 'Registration',
            'initial_cost'         => '616.00',
            'additional_cost'      => '211.00',
            'logo_initial_cost'    => '616.00',
            'logo_additional_cost' => '211.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 70,
            'service_type'         => 'Certificate',
            'initial_cost'         => '672.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '672.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 71,
            'service_type'        => 'Study',
            'initial_cost'        => '70.00',
            'additional_cost'     => '70.00',
            'logo_initial_cost'    => '70.00',
            'logo_additional_cost' => '70.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 71,
            'service_type'         => 'Registration',
            'initial_cost'         => '709.00',
            'additional_cost'      => '92.00',
            'logo_initial_cost'    => '709.00',
            'logo_additional_cost' => '92.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 71,
            'service_type'         => 'Certificate',
            'initial_cost'         => '130.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '130.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 72,
            'service_type'        => 'Study',
            'initial_cost'        => '99.00',
            'additional_cost'     => '99.00',
            'logo_initial_cost'    => '99.00',
            'logo_additional_cost' => '99.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 72,
            'service_type'         => 'Registration',
            'initial_cost'         => '1202.00',
            'additional_cost'      => '182.00',
            'logo_initial_cost'    => '1202.00',
            'logo_additional_cost' => '182.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 72,
            'service_type'         => 'Certificate',
            'initial_cost'         => -1,
            'additional_cost'      => -1,
            'logo_initial_cost'    => -1,
            'logo_additional_cost' => -1,
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 73,
            'service_type'        => 'Study',
            'initial_cost'        => '131.00',
            'additional_cost'     => '116.00',
            'logo_initial_cost'    => '131.00',
            'logo_additional_cost' => '116.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 73,
            'service_type'         => 'Registration',
            'initial_cost'         => '534.00',
            'additional_cost'      => '78.00',
            'logo_initial_cost'    => '534.00',
            'logo_additional_cost' => '78.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 73,
            'service_type'         => 'Certificate',
            'initial_cost'         => '42.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '42.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 74,
            'service_type'        => 'Study',
            'initial_cost'        => '124.00',
            'additional_cost'     => '124.00',
            'logo_initial_cost'    => '151.00',
            'logo_additional_cost' => '151.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 74,
            'service_type'         => 'Registration',
            'initial_cost'         => '360.00',
            'additional_cost'      => '98.00',
            'logo_initial_cost'    => '360.00',
            'logo_additional_cost' => '98.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 74,
            'service_type'         => 'Certificate',
            'initial_cost'         => '363.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '363.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 75,
            'service_type'        => 'Study',
            'initial_cost'        => '78.00',
            'additional_cost'     => '78.00',
            'logo_initial_cost'    => '78.00',
            'logo_additional_cost' => '78.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 75,
            'service_type'         => 'Registration',
            'initial_cost'         => '1702.00',
            'additional_cost'      => '147.00',
            'logo_initial_cost'    => '1702.00',
            'logo_additional_cost' => '147.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 75,
            'service_type'         => 'Certificate',
            'initial_cost'         => '359.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '359.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 76,
            'service_type'        => 'Study',
            'initial_cost'        => '86.00',
            'additional_cost'     => '86.00',
            'logo_initial_cost'    => '151.00',
            'logo_additional_cost' => '151.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 76,
            'service_type'         => 'Registration',
            'initial_cost'         => '426.00',
            'additional_cost'      => '92.00',
            'logo_initial_cost'    => '426.00',
            'logo_additional_cost' => '92.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 76,
            'service_type'         => 'Certificate',
            'initial_cost'         => '449.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '449.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 77,
            'service_type'        => 'Study',
            'initial_cost'        => '97.00',
            'additional_cost'     => '97.00',
            'logo_initial_cost'    => '173.00',
            'logo_additional_cost' => '173.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 77,
            'service_type'         => 'Registration',
            'initial_cost'         => '545.00',
            'additional_cost'      => '144.00',
            'logo_initial_cost'    => '545.00',
            'logo_additional_cost' => '144.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 77,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 78,
            'service_type'        => 'Study',
            'initial_cost'        => '121.00',
            'additional_cost'     => '49.00',
            'logo_initial_cost'    => '194.00',
            'logo_additional_cost' => '78.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 78,
            'service_type'         => 'Registration',
            'initial_cost'         => '368.00',
            'additional_cost'      => '46.00',
            'logo_initial_cost'    => '368.00',
            'logo_additional_cost' => '46.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 78,
            'service_type'         => 'Certificate',
            'initial_cost'         => '388.00',
            'additional_cost'      => '49.00',
            'logo_initial_cost'    => '388.00',
            'logo_additional_cost' => '49.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 79,
            'service_type'        => 'Study',
            'initial_cost'        => '76.00',
            'additional_cost'     => '76.00',
            'logo_initial_cost'    => '124.00',
            'logo_additional_cost' => '124.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 79,
            'service_type'         => 'Registration',
            'initial_cost'         => '658.00',
            'additional_cost'      => '658.00',
            'logo_initial_cost'    => '658.00',
            'logo_additional_cost' => '658.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 79,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 80,
            'service_type'        => 'Study',
            'initial_cost'        => '76.00',
            'additional_cost'     => '76.00',
            'logo_initial_cost'    => '157.00',
            'logo_additional_cost' => '157.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 80,
            'service_type'         => 'Registration',
            'initial_cost'         => '915.00',
            'additional_cost'      => '87.00',
            'logo_initial_cost'    => '915.00',
            'logo_additional_cost' => '87.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 80,
            'service_type'         => 'Certificate',
            'initial_cost'         => '471.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '471.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 81,
            'service_type'        => 'Study',
            'initial_cost'        => '124.00',
            'additional_cost'     => '124.00',
            'logo_initial_cost'    => '124.00',
            'logo_additional_cost' => '124.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 81,
            'service_type'         => 'Registration',
            'initial_cost'         => '616.00',
            'additional_cost'      => '98.00',
            'logo_initial_cost'    => '616.00',
            'logo_additional_cost' => '98.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 81,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

//THIS COUNTRY DOES NOT HAVE INDIVIDUAL TRADEMARKREGISTRATION. PRICES ARE BASED ON BENELUX TREATY.

		DB::table('prices')->insert([
            'country_id'           => 82,
            'service_type'        => 'Study',
            'initial_cost'        => '97.00',
            'additional_cost'     => '97.00',
            'logo_initial_cost'    => '173.00',
            'logo_additional_cost' => '173.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 82,
            'service_type'         => 'Registration',
            'initial_cost'         => '545.00',
            'additional_cost'      => '144.00',
            'logo_initial_cost'    => '545.00',
            'logo_additional_cost' => '144.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 82,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 83,
            'service_type'        => 'Study',
            'initial_cost'        => '113.00',
            'additional_cost'     => '113.00',
            'logo_initial_cost'    => '113.00',
            'logo_additional_cost' => '113.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 83,
            'service_type'         => 'Registration',
            'initial_cost'         => '957.00',
            'additional_cost'      => '220.00',
            'logo_initial_cost'    => '957.00',
            'logo_additional_cost' => '220.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 83,
            'service_type'         => 'Certificate',
            'initial_cost'         => '344.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '344.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 84,
            'service_type'        => 'Study',
            'initial_cost'        => '78.00',
            'additional_cost'     => '78.00',
            'logo_initial_cost'    => '155.00',
            'logo_additional_cost' => '155.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 84,
            'service_type'         => 'Registration',
            'initial_cost'         => '796.00',
            'additional_cost'      => '87.00',
            'logo_initial_cost'    => '796.00',
            'logo_additional_cost' => '87.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 84,
            'service_type'         => 'Certificate',
            'initial_cost'         => '543.00',
            'additional_cost'      => '388.00',
            'logo_initial_cost'    => '543.00',
            'logo_additional_cost' => '388.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 85,
            'service_type'        => 'Study',
            'initial_cost'        => '76.00',
            'additional_cost'     => '76.00',
            'logo_initial_cost'    => '118.00',
            'logo_additional_cost' => '118.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 85,
            'service_type'         => 'Registration',
            'initial_cost'         => '442.00',
            'additional_cost'      => '159.00',
            'logo_initial_cost'    => '442.00',
            'logo_additional_cost' => '159.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 85,
            'service_type'         => 'Certificate',
            'initial_cost'         => '189.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '189.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 86,
            'service_type'        => 'Study',
            'initial_cost'        => '86.00',
            'additional_cost'     => '70.00',
            'logo_initial_cost'    => '135.00',
            'logo_additional_cost' => '97.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 86,
            'service_type'         => 'Registration',
            'initial_cost'         => '684.00',
            'additional_cost'      => '354.00',
            'logo_initial_cost'    => '879.00',
            'logo_additional_cost' => '354.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 86,
            'service_type'         => 'Certificate',
            'initial_cost'         => '178.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '178.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 87,
            'service_type'        => 'Study',
            'initial_cost'        => '121.00',
            'additional_cost'     => '121.00',
            'logo_initial_cost'    => '121.00',
            'logo_additional_cost' => '121.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 87,
            'service_type'         => 'Registration',
            'initial_cost'         => '833.00',
            'additional_cost'      => '133.00',
            'logo_initial_cost'    => '833.00',
            'logo_additional_cost' => '133.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 87,
            'service_type'         => 'Certificate',
            'initial_cost'         => '606.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '606.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 88,
            'service_type'        => 'Study',
            'initial_cost'        => '70.00',
            'additional_cost'     => '70.00',
            'logo_initial_cost'    => '70.00',
            'logo_additional_cost' => '70.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 88,
            'service_type'         => 'Registration',
            'initial_cost'         => '694.00',
            'additional_cost'      => '205.00',
            'logo_initial_cost'    => '694.00',
            'logo_additional_cost' => '205.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 88,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 89,
            'service_type'        => 'Study',
            'initial_cost'        => '78.00',
            'additional_cost'     => '63.00',
            'logo_initial_cost'    => '116.00',
            'logo_additional_cost' => '97.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 89,
            'service_type'         => 'Registration',
            'initial_cost'         => '616.00',
            'additional_cost'      => '129.00',
            'logo_initial_cost'    => '616.00',
            'logo_additional_cost' => '129.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 89,
            'service_type'         => 'Certificate',
            'initial_cost'         => '776.00',
            'additional_cost'      => '116.00',
            'logo_initial_cost'    => '776.00',
            'logo_additional_cost' => '116.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 90,
            'service_type'        => 'Study',
            'initial_cost'        => '76.00',
            'additional_cost'     => '76.00',
            'logo_initial_cost'    => '76.00',
            'logo_additional_cost' => '76.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 90,
            'service_type'         => 'Registration',
            'initial_cost'         => '565.00',
            'additional_cost'      => '76.00',
            'logo_initial_cost'    => '565.00',
            'logo_additional_cost' => '76.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 90,
            'service_type'         => 'Certificate',
            'initial_cost'         => '786.00',
            'additional_cost'      => '59.00',
            'logo_initial_cost'    => '786.00',
            'logo_additional_cost' => '59.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 91,
            'service_type'        => 'Study',
            'initial_cost'        => '116.00',
            'additional_cost'     => '78.00',
            'logo_initial_cost'    => '116.00',
            'logo_additional_cost' => '78.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 91,
            'service_type'         => 'Registration',
            'initial_cost'         => '708.00',
            'additional_cost'      => '64.00',
            'logo_initial_cost'    => '708.00',
            'logo_additional_cost' => '64.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 91,
            'service_type'         => 'Certificate',
            'initial_cost'         => -1,
            'additional_cost'      => -1,
            'logo_initial_cost'    => -1,
            'logo_additional_cost' => -1,
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 92,
            'service_type'        => 'Study',
            'initial_cost'        => '76.00',
            'additional_cost'     => '76.00',
            'logo_initial_cost'    => '157.00',
            'logo_additional_cost' => '157.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 92,
            'service_type'         => 'Registration',
            'initial_cost'         => '828.00',
            'additional_cost'      => '201.00',
            'logo_initial_cost'    => '828.00',
            'logo_additional_cost' => '201.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 92,
            'service_type'         => 'Certificate',
            'initial_cost'         => '141.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '141.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 93,
            'service_type'        => 'Study',
            'initial_cost'        => '76.00',
            'additional_cost'     => '76.00',
            'logo_initial_cost'    => '157.00',
            'logo_additional_cost' => '157.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 93,
            'service_type'         => 'Registration',
            'initial_cost'         => '627.00',
            'additional_cost'      => '102.00',
            'logo_initial_cost'    => '627.00',
            'logo_additional_cost' => '102.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 93,
            'service_type'         => 'Certificate',
            'initial_cost'         => '727.00',
            'additional_cost'      => '130.00',
            'logo_initial_cost'    => '727.00',
            'logo_additional_cost' => '130.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 94,
            'service_type'        => 'Study',
            'initial_cost'        => '53.00',
            'additional_cost'     => '53.00',
            'logo_initial_cost'    => '216.00',
            'logo_additional_cost' => '216.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 94,
            'service_type'         => 'Registration',
            'initial_cost'         => '350.00',
            'additional_cost'      => '211.00',
            'logo_initial_cost'    => '350.00',
            'logo_additional_cost' => '211.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 94,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 95,
            'service_type'        => 'Study',
            'initial_cost'        => '78.00',
            'additional_cost'     => '78.00',
            'logo_initial_cost'    => '121.00',
            'logo_additional_cost' => '121.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 95,
            'service_type'         => 'Registration',
            'initial_cost'         => '598.00',
            'additional_cost'      => '120.00',
            'logo_initial_cost'    => '598.00',
            'logo_additional_cost' => '120.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 95,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 96,
            'service_type'        => 'Study',
            'initial_cost'        => '87.00',
            'additional_cost'     => '87.00',
            'logo_initial_cost'    => '87.00',
            'logo_additional_cost' => '87.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 96,
            'service_type'         => 'Registration',
            'initial_cost'         => '764.00',
            'additional_cost'      => '138.00',
            'logo_initial_cost'    => '764.00',
            'logo_additional_cost' => '138.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 96,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 97,
            'service_type'        => 'Study',
            'initial_cost'        => '121.00',
            'additional_cost'     => '121.00',
            'logo_initial_cost'    => '121.00',
            'logo_additional_cost' => '121.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 97,
            'service_type'         => 'Registration',
            'initial_cost'         => '506.00',
            'additional_cost'      => '258.00',
            'logo_initial_cost'    => '506.00',
            'logo_additional_cost' => '258.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 97,
            'service_type'         => 'Certificate',
            'initial_cost'         => '514.00',
            'additional_cost'      => '150.00',
            'logo_initial_cost'    => '514.00',
            'logo_additional_cost' => '150.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 98,
            'service_type'        => 'Study',
            'initial_cost'        => '68.00',
            'additional_cost'     => '68.00',
            'logo_initial_cost'    => '136.00',
            'logo_additional_cost' => '105.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 98,
            'service_type'         => 'Registration',
            'initial_cost'         => '459.00',
            'additional_cost'      => '129.00',
            'logo_initial_cost'    => '459.00',
            'logo_additional_cost' => '129.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 98,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 1,
            'service_type'        => 'Study',
            'initial_cost'        => '146.00',
            'additional_cost'     => '146.00',
            'logo_initial_cost'    => '146.00',
            'logo_additional_cost' => '146.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 1,
            'service_type'         => 'Registration',
            'initial_cost'         => '451.00',
            'additional_cost'      => '414.00',
            'logo_initial_cost'    => '451.00',
            'logo_additional_cost' => '414.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 1,
            'service_type'         => 'Certificate',
            'initial_cost'         => '466.00',
            'additional_cost'      => '437.00',
            'logo_initial_cost'    => '466.00',
            'logo_additional_cost' => '437.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);
 
 		DB::table('prices')->insert([
            'country_id'           => 2,
            'service_type'        => 'Study',
            'initial_cost'        => '121.00',
            'additional_cost'     => '121.00',
            'logo_initial_cost'    => '121.00',
            'logo_additional_cost' => '121.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 2,
            'service_type'         => 'Registration',
            'initial_cost'         => '1408.00',
            'additional_cost'      => '1380.00',
            'logo_initial_cost'    => '1408.00',
            'logo_additional_cost' => '1380.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 2,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 3,
            'service_type'        => 'Study',
            'initial_cost'        => '116.00',
            'additional_cost'     => '78.00',
            'logo_initial_cost'    => '116.00',
            'logo_additional_cost' => '78.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 3,
            'service_type'         => 'Registration',
            'initial_cost'         => '428.00',
            'additional_cost'      => '331.00',
            'logo_initial_cost'    => '428.00',
            'logo_additional_cost' => '331.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 3,
            'service_type'         => 'Certificate',
            'initial_cost'         => '349.00',
            'additional_cost'      => '267.00',
            'logo_initial_cost'    => '349.00',
            'logo_additional_cost' => '267.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 4,
            'service_type'        => 'Study',
            'initial_cost'        => '121.00',
            'additional_cost'     => '121.00',
            'logo_initial_cost'    => '121.00',
            'logo_additional_cost' => '121.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 4,
            'service_type'         => 'Registration',
            'initial_cost'         => '1150.00',
            'additional_cost'      => '644.00',
            'logo_initial_cost'    => '1150.00',
            'logo_additional_cost' => '644.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 4,
            'service_type'         => 'Certificate',
            'initial_cost'         => '301.00',
            'additional_cost'      => '243.00',
            'logo_initial_cost'    => '301.00',
            'logo_additional_cost' => '243.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 5,
            'service_type'        => 'Study',
            'initial_cost'        => '121.00',
            'additional_cost'     => '121.00',
            'logo_initial_cost'    => '121.00',
            'logo_additional_cost' => '121.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 5,
            'service_type'         => 'Registration',
            'initial_cost'         => '552.00',
            'additional_cost'      => '331.00',
            'logo_initial_cost'    => '552.00',
            'logo_additional_cost' => '331.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 5,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 6,
            'service_type'        => 'Study',
            'initial_cost'        => '73.00',
            'additional_cost'     => '73.00',
            'logo_initial_cost'    => '146.00',
            'logo_additional_cost' => '146.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 6,
            'service_type'         => 'Registration',
            'initial_cost'         => '386.00',
            'additional_cost'      => '386.00',
            'logo_initial_cost'    => '386.00',
            'logo_additional_cost' => '386.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 6,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 7,
            'service_type'        => 'Study',
            'initial_cost'        => '68.00',
            'additional_cost'     => '68.00',
            'logo_initial_cost'    => '194.00',
            'logo_additional_cost' => '155.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 7,
            'service_type'         => 'Registration',
            'initial_cost'         => '589.00',
            'additional_cost'      => '313.00',
            'logo_initial_cost'    => '589.00',
            'logo_additional_cost' => '313.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 7,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 8,
            'service_type'        => 'Study',
            'initial_cost'        => '97.00',
            'additional_cost'     => '78.00',
            'logo_initial_cost'    => '136.00',
            'logo_additional_cost' => '116.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 8,
            'service_type'         => 'Registration',
            'initial_cost'         => '359.00',
            'additional_cost'      => '359.00',
            'logo_initial_cost'    => '359.00',
            'logo_additional_cost' => '359.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 8,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 9,
            'service_type'        => 'Study',
            'initial_cost'        => '97.00',
            'additional_cost'     => '97.00',
            'logo_initial_cost'    => '136.00',
            'logo_additional_cost' => '136.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 9,
            'service_type'         => 'Registration',
            'initial_cost'         => '515.00',
            'additional_cost'      => '460.00',
            'logo_initial_cost'    => '515.00',
            'logo_additional_cost' => '460.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 9,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 10,
            'service_type'        => 'Study',
            'initial_cost'        => '279.00',
            'additional_cost'     => '279.00',
            'logo_initial_cost'    => '279.00',
            'logo_additional_cost' => '279.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 10,
            'service_type'         => 'Registration',
            'initial_cost'         => '801.00',
            'additional_cost'      => '383.00',
            'logo_initial_cost'    => '801.00',
            'logo_additional_cost' => '383.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 10,
            'service_type'         => 'Certificate',
            'initial_cost'         => '495.00',
            'additional_cost'      => '147.00',
            'logo_initial_cost'    => '495.00',
            'logo_additional_cost' => '147.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 11,
            'service_type'        => 'Study',
            'initial_cost'        => '76.00',
            'additional_cost'     => '76.00',
            'logo_initial_cost'    => '151.00',
            'logo_additional_cost' => '151.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 11,
            'service_type'         => 'Registration',
            'initial_cost'         => '593.00',
            'additional_cost'      => '414.00',
            'logo_initial_cost'    => '593.00',
            'logo_additional_cost' => '414.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 11,
            'service_type'         => 'Certificate',
            'initial_cost'         => '530.00',
            'additional_cost'      => '423.00',
            'logo_initial_cost'    => '1060.00',
            'logo_additional_cost' => '852.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 12,
            'service_type'        => 'Study',
            'initial_cost'        => '121.00',
            'additional_cost'     => '87.00',
            'logo_initial_cost'    => '194.00',
            'logo_additional_cost' => '155.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 12,
            'service_type'         => 'Registration',
            'initial_cost'         => '423.00',
            'additional_cost'      => '423.00',
            'logo_initial_cost'    => '423.00',
            'logo_additional_cost' => '423.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 12,
            'service_type'         => 'Certificate',
            'initial_cost'         => '388.00',
            'additional_cost'      => '388.00',
            'logo_initial_cost'    => '388.00',
            'logo_additional_cost' => '388.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 13,
            'service_type'        => 'Study',
            'initial_cost'        => '121.00',
            'additional_cost'     => '121.00',
            'logo_initial_cost'    => '121.00',
            'logo_additional_cost' => '121.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 13,
            'service_type'         => 'Registration',
            'initial_cost'         => '810.00',
            'additional_cost'      => '299.00',
            'logo_initial_cost'    => '810.00',
            'logo_additional_cost' => '299.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 13,
            'service_type'         => 'Certificate',
            'initial_cost'         => '631.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '631.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		// DB::table('prices')->insert([
  //           'country_name'        => 'Laos',
  //           'service_type'        => 'Study',
  //           'initial_cost'        => '136.00',
  //           'additional_cost'     => '136.00',
  //           'logo_initial_cost'    => '136.00',
  //           'logo_additional_cost' => '136.00',
  //           'tax'                 => '0.00',
  //           'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
  //           'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
  //       ]);

  //       DB::table('prices')->insert([
  //           'country_name'         => 'Laos',
  //           'service_type'         => 'Registration',
  //           'initial_cost'         => '543.00',
  //           'additional_cost'      => '506.00',
  //           'logo_initial_cost'    => '543.00',
  //           'logo_additional_cost' => '506.00',
  //           'tax'                  => '0.00',
  //           'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
  //           'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
  //       ]);

  //       DB::table('prices')->insert([
  //           'country_name'         => 'Laos',
  //           'service_type'         => 'Certificate',
  //           'initial_cost'         => '0.00',
  //           'additional_cost'      => '0.00',
  //           'logo_initial_cost'    => '0.00',
  //           'logo_additional_cost' => '0.00',
  //           'tax'                  => '0.00',
  //           'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
  //           'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
  //       ]);

		DB::table('prices')->insert([
            'country_id'           => 14,
            'service_type'        => 'Study',
            'initial_cost'        => '78.00',
            'additional_cost'     => '78.00',
            'logo_initial_cost'    => '155.00',
            'logo_additional_cost' => '155.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 14,
            'service_type'         => 'Registration',
            'initial_cost'         => '474.00',
            'additional_cost'      => '474.00',
            'logo_initial_cost'    => '474.00',
            'logo_additional_cost' => '474.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 14,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 15,
            'service_type'        => 'Study',
            'initial_cost'        => '68.00',
            'additional_cost'     => '68.00',
            'logo_initial_cost'    => '68.00',
            'logo_additional_cost' => '68.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 15,
            'service_type'         => 'Registration',
            'initial_cost'         => '396.00',
            'additional_cost'      => '391.00',
            'logo_initial_cost'    => '396.00',
            'logo_additional_cost' => '391.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 15,
            'service_type'         => 'Certificate',
            'initial_cost'         => '485.00',
            'additional_cost'      => '466.00',
            'logo_initial_cost'    => '485.00',
            'logo_additional_cost' => '466.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 16,
            'service_type'        => 'Study',
            'initial_cost'        => '116.00',
            'additional_cost'     => '116.00',
            'logo_initial_cost'    => '116.00',
            'logo_additional_cost' => '116.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 16,
            'service_type'         => 'Registration',
            'initial_cost'         => '1012.00',
            'additional_cost'      => '147.00',
            'logo_initial_cost'    => '1012.00',
            'logo_additional_cost' => '147.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 16,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 17,
            'service_type'        => 'Study',
            'initial_cost'        => '175.00',
            'additional_cost'     => '78.00',
            'logo_initial_cost'    => '175.00',
            'logo_additional_cost' => '78.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 17,
            'service_type'         => 'Registration',
            'initial_cost'         => '501.00',
            'additional_cost'      => '184.00',
            'logo_initial_cost'    => '501.00',
            'logo_additional_cost' => '184.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 17,
            'service_type'         => 'Certificate',
            'initial_cost'         => '432.00',
            'additional_cost'      => '97.00',
            'logo_initial_cost'    => '432.00',
            'logo_additional_cost' => '97.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 18,
            'service_type'        => 'Study',
            'initial_cost'        => '97.00',
            'additional_cost'     => '97.00',
            'logo_initial_cost'    => '97.00',
            'logo_additional_cost' => '97.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 18,
            'service_type'         => 'Registration',
            'initial_cost'         => '460.00',
            'additional_cost'      => '110.00',
            'logo_initial_cost'    => '460.00',
            'logo_additional_cost' => '110.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 18,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 19,
            'service_type'        => 'Study',
            'initial_cost'        => '78.00',
            'additional_cost'     => '78.00',
            'logo_initial_cost'    => '78.00',
            'logo_additional_cost' => '78.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 19,
            'service_type'         => 'Registration',
            'initial_cost'         => '460.00',
            'additional_cost'      => '419.00',
            'logo_initial_cost'    => '460.00',
            'logo_additional_cost' => '419.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 19,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 20,
            'service_type'        => 'Study',
            'initial_cost'        => '121.00',
            'additional_cost'     => '121.00',
            'logo_initial_cost'    => '121.00',
            'logo_additional_cost' => '121.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 20,
            'service_type'         => 'Registration',
            'initial_cost'         => '304.00',
            'additional_cost'      => '285.00',
            'logo_initial_cost'    => '304.00',
            'logo_additional_cost' => '285.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 20,
            'service_type'         => 'Certificate',
            'initial_cost'         => '340.00',
            'additional_cost'      => '320.00',
            'logo_initial_cost'    => '340.00',
            'logo_additional_cost' => '320.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 21,
            'service_type'        => 'Study',
            'initial_cost'        => '97.00',
            'additional_cost'     => '78.00',
            'logo_initial_cost'    => '160.00',
            'logo_additional_cost' => '97.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 21,
            'service_type'         => 'Registration',
            'initial_cost'         => '672.00',
            'additional_cost'      => '478.00',
            'logo_initial_cost'    => '672.00',
            'logo_additional_cost' => '478.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 21,
            'service_type'         => 'Certificate',
            'initial_cost'         => '296.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '296.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 22,
            'service_type'        => 'Study',
            'initial_cost'        => '87.00',
            'additional_cost'     => '87.00',
            'logo_initial_cost'    => '87.00',
            'logo_additional_cost' => '87.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 22,
            'service_type'         => 'Registration',
            'initial_cost'         => '497.00',
            'additional_cost'      => '396.00',
            'logo_initial_cost'    => '497.00',
            'logo_additional_cost' => '396.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 22,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 23,
            'service_type'        => 'Study',
            'initial_cost'        => '146.00',
            'additional_cost'     => '97.00',
            'logo_initial_cost'    => '97.00',
            'logo_additional_cost' => '97.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 23,
            'service_type'         => 'Registration',
            'initial_cost'         => '460.00',
            'additional_cost'      => '460.00',
            'logo_initial_cost'    => '460.00',
            'logo_additional_cost' => '460.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 23,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 24,
            'service_type'        => 'Study',
            'initial_cost'        => '121.00',
            'additional_cost'     => '121.00',
            'logo_initial_cost'    => '121.00',
            'logo_additional_cost' => '121.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 24,
            'service_type'         => 'Registration',
            'initial_cost'         => '451.00',
            'additional_cost'      => '327.00',
            'logo_initial_cost'    => '451.00',
            'logo_additional_cost' => '327.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 24,
            'service_type'         => 'Certificate',
            'initial_cost'         => '223.00',
            'additional_cost'      => '223.00',
            'logo_initial_cost'    => '223.00',
            'logo_additional_cost' => '223.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 25,
            'service_type'        => 'Study',
            'initial_cost'        => '78.00',
            'additional_cost'     => '78.00',
            'logo_initial_cost'    => '78.00',
            'logo_additional_cost' => '78.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 25,
            'service_type'         => 'Registration',
            'initial_cost'         => '653.00',
            'additional_cost'      => '276.00',
            'logo_initial_cost'    => '653.00',
            'logo_additional_cost' => '276.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 25,
            'service_type'         => 'Certificate',
            'initial_cost'         => '825.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '825.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 26,
            'service_type'        => 'Study',
            'initial_cost'        => '121.00',
            'additional_cost'     => '121.00',
            'logo_initial_cost'    => '121.00',
            'logo_additional_cost' => '121.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 26,
            'service_type'         => 'Registration',
            'initial_cost'         => '432.00',
            'additional_cost'      => '368.00',
            'logo_initial_cost'    => '432.00',
            'logo_additional_cost' => '368.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 26,
            'service_type'         => 'Certificate',
            'initial_cost'         => '184.00',
            'additional_cost'      => '175.00',
            'logo_initial_cost'    => '184.00',
            'logo_additional_cost' => '175.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 27,
            'service_type'        => 'Study',
            'initial_cost'        => '-1',
            'additional_cost'     => '-1',
            'logo_initial_cost'    => '-1',
            'logo_additional_cost' => '-1',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 27,
            'service_type'         => 'Registration',
            'initial_cost'         => '1130.00',
            'additional_cost'      => '6.00',
            'logo_initial_cost'    => '1130.00',
            'logo_additional_cost' => '6.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 27,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 28,
            'service_type'        => 'Study',
            'initial_cost'        => '78.00',
            'additional_cost'     => '78.00',
            'logo_initial_cost'    => '78.00',
            'logo_additional_cost' => '78.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 28,
            'service_type'         => 'Registration',
            'initial_cost'         => '690.00',
            'additional_cost'      => '221.00',
            'logo_initial_cost'    => '690.00',
            'logo_additional_cost' => '221.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 28,
            'service_type'         => 'Certificate',
            'initial_cost'         => '626.00',
            'additional_cost'      => '626.00',
            'logo_initial_cost'    => '626.00',
            'logo_additional_cost' => '626.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		// DB::table('prices')->insert([
  //           'country_name'        => 'Uzbekistan',
  //           'service_type'        => 'Study',
  //           'initial_cost'        => '116.00',
  //           'additional_cost'     => '78.00',
  //           'logo_initial_cost'    => '116.00',
  //           'logo_additional_cost' => '78.00',
  //           'tax'                 => '0.00',
  //           'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
  //           'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
  //       ]);

  //       DB::table('prices')->insert([
  //           'country_name'         => 'Uzbekistan',
  //           'service_type'         => 'Registration',
  //           'initial_cost'         => '1394.00',
  //           'additional_cost'      => '276.00',
  //           'logo_initial_cost'    => '1394.00',
  //           'logo_additional_cost' => '276.00',
  //           'tax'                  => '0.00',
  //           'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
  //           'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
  //       ]);

  //       DB::table('prices')->insert([
  //           'country_name'         => 'Uzbekistan',
  //           'service_type'         => 'Certificate',
  //           'initial_cost'         => '1664.00',
  //           'additional_cost'      => '165.00',
  //           'logo_initial_cost'    => '1664.00',
  //           'logo_additional_cost' => '165.00',
 
  //           'tax'                  => '0.00',
  //           'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
  //           'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
  //       ]);

		DB::table('prices')->insert([
            'country_id'           => 29,
            'service_type'        => 'Study',
            'initial_cost'        => '78.00',
            'additional_cost'     => '78.00',
            'logo_initial_cost'    => '121.00',
            'logo_additional_cost' => '121.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 29,
            'service_type'         => 'Registration',
            'initial_cost'         => '359.00',
            'additional_cost'      => '299.00',
            'logo_initial_cost'    => '359.00',
            'logo_additional_cost' => '299.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 29,
            'service_type'         => 'Certificate',
            'initial_cost'         => '175.00',
            'additional_cost'      => '116.00',
            'logo_initial_cost'    => '175.00',
            'logo_additional_cost' => '116.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 30,
            'service_type'        => 'Study',
            'initial_cost'        => '243.00',
            'additional_cost'     => '102.00',
            'logo_initial_cost'    => '243.00',
            'logo_additional_cost' => '102.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 30,
            'service_type'         => 'Registration',
            'initial_cost'         => '685.00',
            'additional_cost'      => '313.00',
            'logo_initial_cost'    => '685.00',
            'logo_additional_cost' => '313.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 30,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 31,
            'service_type'        => 'Study',
            'initial_cost'        => '121.00',
            'additional_cost'     => '121.00',
            'logo_initial_cost'    => '121.00',
            'logo_additional_cost' => '121.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 31,
            'service_type'         => 'Registration',
            'initial_cost'         => '764.00',
            'additional_cost'      => '653.00',
            'logo_initial_cost'    => '764.00',
            'logo_additional_cost' => '653.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 31,
            'service_type'         => 'Certificate',
            'initial_cost'         => '1639.00',
            'additional_cost'      => '1504.00',
            'logo_initial_cost'    => '1639.00',
            'logo_additional_cost' => '1504.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 32,
            'service_type'        => 'Study',
            'initial_cost'        => '121.00',
            'additional_cost'     => '121.00',
            'logo_initial_cost'    => '121.00',
            'logo_additional_cost' => '121.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 32,
            'service_type'         => 'Registration',
            'initial_cost'         => '1224.00',
            'additional_cost'      => '120.00',
            'logo_initial_cost'    => '1224.00',
            'logo_additional_cost' => '120.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 32,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 33,
            'service_type'        => 'Study',
            'initial_cost'        => '121.00',
            'additional_cost'     => '121.00',
            'logo_initial_cost'    => '121.00',
            'logo_additional_cost' => '121.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 33,
            'service_type'         => 'Registration',
            'initial_cost'         => '920.00',
            'additional_cost'      => '644.00',
            'logo_initial_cost'    => '920.00',
            'logo_additional_cost' => '644.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 33,
            'service_type'         => 'Certificate',
            'initial_cost'         => '369.00',
            'additional_cost'      => '257.00',
            'logo_initial_cost'    => '369.00',
            'logo_additional_cost' => '257.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 34,
            'service_type'        => 'Study',
            'initial_cost'        => '121.00',
            'additional_cost'     => '121.00',
            'logo_initial_cost'    => '121.00',
            'logo_additional_cost' => '121.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 34,
            'service_type'         => 'Registration',
            'initial_cost'         => '635.00',
            'additional_cost'      => '566.00',
            'logo_initial_cost'    => '635.00',
            'logo_additional_cost' => '566.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 34,
            'service_type'         => 'Certificate',
            'initial_cost'         => '640.00',
            'additional_cost'      => '601.00',
            'logo_initial_cost'    => '640.00',
            'logo_additional_cost' => '601.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 35,
            'service_type'        => 'Study',
            'initial_cost'        => '121.00',
            'additional_cost'     => '121.00',
            'logo_initial_cost'    => '121.00',
            'logo_additional_cost' => '121.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 35,
            'service_type'         => 'Registration',
            'initial_cost'         => '1564.00',
            'additional_cost'      => '1527.00',
            'logo_initial_cost'    => '1564.00',
            'logo_additional_cost' => '1527.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 35,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 36,
            'service_type'        => 'Study',
            'initial_cost'        => '121.00',
            'additional_cost'     => '78.00',
            'logo_initial_cost'    => '121.00',
            'logo_additional_cost' => '78.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 36,
            'service_type'         => 'Registration',
            'initial_cost'         => '1012.00',
            'additional_cost'      => '294.00',
            'logo_initial_cost'    => '1012.00',
            'logo_additional_cost' => '294.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 36,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 37,
            'service_type'        => 'Study',
            'initial_cost'        => '78.00',
            'additional_cost'     => '78.00',
            'logo_initial_cost'    => '78.00',
            'logo_additional_cost' => '78.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 37,
            'service_type'         => 'Registration',
            'initial_cost'         => '644.00',
            'additional_cost'      => '644.00',
            'logo_initial_cost'    => '644.00',
            'logo_additional_cost' => '644.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 37,
            'service_type'         => 'Certificate',
            'initial_cost'         => '621.00',
            'additional_cost'      => '582.00',
            'logo_initial_cost'    => '621.00',
            'logo_additional_cost' => '582.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 38,
            'service_type'        => 'Study',
            'initial_cost'        => '272.00',
            'additional_cost'     => '243.00',
            'logo_initial_cost'    => '272.00',
            'logo_additional_cost' => '243.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 38,
            'service_type'         => 'Registration',
            'initial_cost'         => '865.00',
            'additional_cost'      => '828.00',
            'logo_initial_cost'    => '865.00',
            'logo_additional_cost' => '828.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 38,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 39,
            'service_type'        => 'Study',
            'initial_cost'        => '121.00',
            'additional_cost'     => '121.00',
            'logo_initial_cost'    => '121.00',
            'logo_additional_cost' => '121.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 39,
            'service_type'         => 'Registration',
            'initial_cost'         => '1288.00',
            'additional_cost'      => '1288.00',
            'logo_initial_cost'    => '1288.00',
            'logo_additional_cost' => '1288.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 39,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 40,
            'service_type'        => 'Study',
            'initial_cost'        => '121.00',
            'additional_cost'     => '121.00',
            'logo_initial_cost'    => '121.00',
            'logo_additional_cost' => '121.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 40,
            'service_type'         => 'Registration',
            'initial_cost'         => '1518.00',
            'additional_cost'      => '1440.00',
            'logo_initial_cost'    => '1518.00',
            'logo_additional_cost' => '1440.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 40,
            'service_type'         => 'Certificate',
            'initial_cost'         => '1668.00',
            'additional_cost'      => '1668.00',
            'logo_initial_cost'    => '1668.00',
            'logo_additional_cost' => '1668.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 41,
            'service_type'        => 'Study',
            'initial_cost'        => '78.00',
            'additional_cost'     => '78.00',
            'logo_initial_cost'    => '121.00',
            'logo_additional_cost' => '121.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 41,
            'service_type'         => 'Registration',
            'initial_cost'         => '515.00',
            'additional_cost'      => '230.00',
            'logo_initial_cost'    => '515.00',
            'logo_additional_cost' => '230.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 41,
            'service_type'         => 'Certificate',
            'initial_cost'         => '543.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '543.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 42,
            'service_type'        => 'Study',
            'initial_cost'        => '121.00',
            'additional_cost'     => '121.00',
            'logo_initial_cost'    => '121.00',
            'logo_additional_cost' => '121.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 42,
            'service_type'         => 'Registration',
            'initial_cost'         => '1739.00',
            'additional_cost'      => '1412.00',
            'logo_initial_cost'    => '1739.00',
            'logo_additional_cost' => '1412.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 42,
            'service_type'         => 'Certificate',
            'initial_cost'         => '3220.00',
            'additional_cost'      => '3220.00',
            'logo_initial_cost'    => '3220.00',
            'logo_additional_cost' => '3220.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 43,
            'service_type'        => 'Study',
            'initial_cost'        => '121.00',
            'additional_cost'     => '121.00',
            'logo_initial_cost'    => '121.00',
            'logo_additional_cost' => '121.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 43,
            'service_type'         => 'Registration',
            'initial_cost'         => '1150.00',
            'additional_cost'      => '920.00',
            'logo_initial_cost'    => '1150.00',
            'logo_additional_cost' => '920.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 43,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 44,
            'service_type'        => 'Study',
            'initial_cost'        => '175.00',
            'additional_cost'     => '175.00',
            'logo_initial_cost'    => '175.00',
            'logo_additional_cost' => '175.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 44,
            'service_type'         => 'Registration',
            'initial_cost'         => '368.00',
            'additional_cost'      => '41.00',
            'logo_initial_cost'    => '368.00',
            'logo_additional_cost' => '41.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 44,
            'service_type'         => 'Certificate',
            'initial_cost'         => '437.00',
            'additional_cost'      => '44.00',
            'logo_initial_cost'    => '437.00',
            'logo_additional_cost' => '44.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 102,
            'service_type'        => 'Study',
            'initial_cost'        => '78.00',
            'additional_cost'     => '68.00',
            'logo_initial_cost'    => '78.00',
            'logo_additional_cost' => '68.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 102,
            'service_type'         => 'Registration',
            'initial_cost'         => '506.00',
            'additional_cost'      => '465.00',
            'logo_initial_cost'    => '506.00',
            'logo_additional_cost' => '465.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 102,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 103,
            'service_type'        => 'Study',
            'initial_cost'        => '87.00',
            'additional_cost'     => '68.00',
            'logo_initial_cost'    => '97.00',
            'logo_additional_cost' => '78.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 103,
            'service_type'         => 'Registration',
            'initial_cost'         => '708.00',
            'additional_cost'      => '626.00',
            'logo_initial_cost'    => '708.00',
            'logo_additional_cost' => '626.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 103,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 104,
            'service_type'        => 'Study',
            'initial_cost'        => '78.00',
            'additional_cost'     => '78.00',
            'logo_initial_cost'    => '155.00',
            'logo_additional_cost' => '116.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 104,
            'service_type'         => 'Registration',
            'initial_cost'         => '543.00',
            'additional_cost'      => '543.00',
            'logo_initial_cost'    => '543.00',
            'logo_additional_cost' => '543.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 104,
            'service_type'         => 'Certificate',
            'initial_cost'         => '669.00',
            'additional_cost'      => '669.00',
            'logo_initial_cost'    => '669.00',
            'logo_additional_cost' => '669.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 105,
            'service_type'        => 'Study',
            'initial_cost'        => '49.00',
            'additional_cost'     => '49.00',
            'logo_initial_cost'    => '49.00',
            'logo_additional_cost' => '49.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 105,
            'service_type'         => 'Registration',
            'initial_cost'         => '414.00',
            'additional_cost'      => '322.00',
            'logo_initial_cost'    => '414.00',
            'logo_additional_cost' => '322.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 105,
            'service_type'         => 'Certificate',
            'initial_cost'         => '243.00',
            'additional_cost'      => '243.00',
            'logo_initial_cost'    => '243.00',
            'logo_additional_cost' => '243.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 106,
            'service_type'        => 'Study',
            'initial_cost'        => '57.00',
            'additional_cost'     => '57.00',
            'logo_initial_cost'    => '115.00',
            'logo_additional_cost' => '113.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 106,
            'service_type'         => 'Registration',
            'initial_cost'         => '626.00',
            'additional_cost'      => '488.00',
            'logo_initial_cost'    => '626.00',
            'logo_additional_cost' => '488.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 106,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 107,
            'service_type'        => 'Study',
            'initial_cost'        => '63.00',
            'additional_cost'     => '63.00',
            'logo_initial_cost'    => '63.00',
            'logo_additional_cost' => '63.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 107,
            'service_type'         => 'Registration',
            'initial_cost'         => '524.00',
            'additional_cost'      => '524.00',
            'logo_initial_cost'    => '524.00',
            'logo_additional_cost' => '524.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 107,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 108,
            'service_type'        => 'Study',
            'initial_cost'        => '194.00',
            'additional_cost'     => '194.00',
            'logo_initial_cost'    => '194.00',
            'logo_additional_cost' => '194.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 108,
            'service_type'         => 'Registration',
            'initial_cost'         => '598.00',
            'additional_cost'      => '598.00',
            'logo_initial_cost'    => '598.00',
            'logo_additional_cost' => '598.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 108,
            'service_type'         => 'Certificate',
            'initial_cost'         => '243.00',
            'additional_cost'      => '243.00',
            'logo_initial_cost'    => '243.00',
            'logo_additional_cost' => '243.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 109,
            'service_type'        => 'Study',
            'initial_cost'        => '49.00',
            'additional_cost'     => '44.00',
            'logo_initial_cost'    => '49.00',
            'logo_additional_cost' => '44.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 109,
            'service_type'         => 'Registration',
            'initial_cost'         => '359.00',
            'additional_cost'      => '304.00',
            'logo_initial_cost'    => '363.00',
            'logo_additional_cost' => '308.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 109,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 110,
            'service_type'        => 'Study',
            'initial_cost'        => '63.00',
            'additional_cost'     => '58.00',
            'logo_initial_cost'    => '121.00',
            'logo_additional_cost' => '112.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 110,
            'service_type'         => 'Registration',
            'initial_cost'         => '511.00',
            'additional_cost'      => '474.00',
            'logo_initial_cost'    => '547.00',
            'logo_additional_cost' => '497.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 110,
            'service_type'         => 'Certificate',
            'initial_cost'         => '131.00',
            'additional_cost'      => '131.00',
            'logo_initial_cost'    => '131.00',
            'logo_additional_cost' => '131.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 111,
            'service_type'        => 'Study',
            'initial_cost'        => '272.00',
            'additional_cost'     => '272.00',
            'logo_initial_cost'    => '272.00',
            'logo_additional_cost' => '272.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 111,
            'service_type'         => 'Registration',
            'initial_cost'         => '736.00',
            'additional_cost'      => '644.00',
            'logo_initial_cost'    => '736.00',
            'logo_additional_cost' => '644.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 111,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 112,
            'service_type'        => 'Study',
            'initial_cost'        => '107.00',
            'additional_cost'     => '78.00',
            'logo_initial_cost'    => '107.00',
            'logo_additional_cost' => '78.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 112,
            'service_type'         => 'Registration',
            'initial_cost'         => '644.00',
            'additional_cost'      => '566.00',
            'logo_initial_cost'    => '842.00',
            'logo_additional_cost' => '593.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 112,
            'service_type'         => 'Certificate',
            'initial_cost'         => '121.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '121.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 113,
            'service_type'        => 'Study',
            'initial_cost'        => '97.00',
            'additional_cost'     => '97.00',
            'logo_initial_cost'    => '97.00',
            'logo_additional_cost' => '97.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 113,
            'service_type'         => 'Registration',
            'initial_cost'         => '727.00',
            'additional_cost'      => '727.00',
            'logo_initial_cost'    => '727.00',
            'logo_additional_cost' => '727.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 113,
            'service_type'         => 'Certificate',
            'initial_cost'         => '2192.00',
            'additional_cost'      => '2192.00',
            'logo_initial_cost'    => '2192.00',
            'logo_additional_cost' => '2192.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 139,
            'service_type'        => 'Study',
            'initial_cost'        => '466.00',
            'additional_cost'     => '466.00',
            'logo_initial_cost'    => '466.00',
            'logo_additional_cost' => '466.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 139,
            'service_type'         => 'Registration',
            'initial_cost'         => '1233.00',
            'additional_cost'      => '1233.00',
            'logo_initial_cost'    => '1233.00',
            'logo_additional_cost' => '1233.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 139,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 140,
            'service_type'        => 'Study',
            'initial_cost'        => '107.00',
            'additional_cost'     => '53.00',
            'logo_initial_cost'    => '136.00',
            'logo_additional_cost' => '68.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 140,
            'service_type'         => 'Registration',
            'initial_cost'         => '475.00',
            'additional_cost'      => '475.00',
            'logo_initial_cost'    => '475.00',
            'logo_additional_cost' => '475.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 140,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 141,
            'service_type'        => 'Study',
            'initial_cost'        => '175.00',
            'additional_cost'     => '116.00',
            'logo_initial_cost'    => '175.00',
            'logo_additional_cost' => '116.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 141,
            'service_type'         => 'Registration',
            'initial_cost'         => '1412.00',
            'additional_cost'      => '681.00',
            'logo_initial_cost'    => '1412.00',
            'logo_additional_cost' => '681.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 141,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 142,
            'service_type'        => 'Study',
            'initial_cost'        => '116.00',
            'additional_cost'     => '116.00',
            'logo_initial_cost'    => '155.00',
            'logo_additional_cost' => '155.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 142,
            'service_type'         => 'Registration',
            'initial_cost'         => '566.00',
            'additional_cost'      => '271.00',
            'logo_initial_cost'    => '566.00',
            'logo_additional_cost' => '271.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 142,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 143,
            'service_type'        => 'Study',
            'initial_cost'        => '175.00',
            'additional_cost'     => '116.00',
            'logo_initial_cost'    => '175.00',
            'logo_additional_cost' => '116.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 143,
            'service_type'         => 'Registration',
            'initial_cost'         => '1334.00',
            'additional_cost'      => '819.00',
            'logo_initial_cost'    => '1334.00',
            'logo_additional_cost' => '819.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 143,
            'service_type'         => 'Certificate',
            'initial_cost'         => '679.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '679.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 144,
            'service_type'        => 'Study',
            'initial_cost'        => '108.00',
            'additional_cost'     => '108.00',
            'logo_initial_cost'    => '108.00',
            'logo_additional_cost' => '108.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 144,
            'service_type'         => 'Registration',
            'initial_cost'         => '-1.00',
            'additional_cost'      => '-1.00',
            'logo_initial_cost'    => '-1.00',
            'logo_additional_cost' => '-1.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 144,
            'service_type'         => 'Certificate',
            'initial_cost'         => '-1.00',
            'additional_cost'      => '-1.00',
            'logo_initial_cost'    => '-1.00',
            'logo_additional_cost' => '-1.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('prices')->insert([
            'country_id'           => 145,
            'service_type'        => 'Study',
            'initial_cost'        => '175.00',
            'additional_cost'     => '116.00',
            'logo_initial_cost'    => '175.00',
            'logo_additional_cost' => '175.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 145,
            'service_type'         => 'Registration',
            'initial_cost'         => '920.00',
            'additional_cost'      => '497.00',
            'logo_initial_cost'    => '920.00',
            'logo_additional_cost' => '497.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 145,
            'service_type'         => 'Certificate',
            'initial_cost'         => '-1',
            'additional_cost'      => '-1',
            'logo_initial_cost'    => '-1',
            'logo_additional_cost' => '-1',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 146,
            'service_type'        => 'Study',
            'initial_cost'        => '262.00',
            'additional_cost'     => '58.00',
            'logo_initial_cost'    => '262.00',
            'logo_additional_cost' => '58.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 146,
            'service_type'         => 'Registration',
            'initial_cost'         => '598.00',
            'additional_cost'      => '184.00',
            'logo_initial_cost'    => '598.00',
            'logo_additional_cost' => '184.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 146,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 147,
            'service_type'        => 'Study',
            'initial_cost'        => '124.00',
            'additional_cost'     => '124.00',
            'logo_initial_cost'    => '124.00',
            'logo_additional_cost' => '124.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 147,
            'service_type'         => 'Registration',
            'initial_cost'         => '828.00',
            'additional_cost'      => '719.00',
            'logo_initial_cost'    => '828.00',
            'logo_additional_cost' => '719.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 147,
            'service_type'         => 'Certificate',
            'initial_cost'         => '411.00',
            'additional_cost'      => '357.00',
            'logo_initial_cost'    => '411.00',
            'logo_additional_cost' => '357.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 148,
            'service_type'        => 'Study',
            'initial_cost'        => '175.00',
            'additional_cost'     => '116.00',
            'logo_initial_cost'    => '175.00',
            'logo_additional_cost' => '116.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 148,
            'service_type'         => 'Registration',
            'initial_cost'         => '874.00',
            'additional_cost'      => '350.00',
            'logo_initial_cost'    => '874.00',
            'logo_additional_cost' => '350.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 148,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 149,
            'service_type'        => 'Study',
            'initial_cost'        => '406.00',
            'additional_cost'     => '103.00',
            'logo_initial_cost'    => '406.00',
            'logo_additional_cost' => '103.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 149,
            'service_type'         => 'Registration',
            'initial_cost'         => '1126.00',
            'additional_cost'      => '257.00',
            'logo_initial_cost'    => '1126.00',
            'logo_additional_cost' => '257.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 149,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 150,
            'service_type'        => 'Study',
            'initial_cost'        => '243.00',
            'additional_cost'     => '68.00',
            'logo_initial_cost'    => '243.00',
            'logo_additional_cost' => '68.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 150,
            'service_type'         => 'Registration',
            'initial_cost'         => '1288.00',
            'additional_cost'      => '152.00',
            'logo_initial_cost'    => '1288.00',
            'logo_additional_cost' => '152.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 150,
            'service_type'         => 'Certificate',
            'initial_cost'         => '1358.00',
            'additional_cost'      => '601.00',
            'logo_initial_cost'    => '1358.00',
            'logo_additional_cost' => '601.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 151,
            'service_type'        => 'Study',
            'initial_cost'        => '406.00',
            'additional_cost'     => '103.00',
            'logo_initial_cost'    => '406.00',
            'logo_additional_cost' => '103.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 151,
            'service_type'         => 'Registration',
            'initial_cost'         => '1126.00',
            'additional_cost'      => '257.00',
            'logo_initial_cost'    => '1126.00',
            'logo_additional_cost' => '257.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 151,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 152,
            'service_type'        => 'Study',
            'initial_cost'        => '121.00',
            'additional_cost'     => '121.00',
            'logo_initial_cost'    => '121.00',
            'logo_additional_cost' => '121.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 152,
            'service_type'         => 'Registration',
            'initial_cost'         => '1320.00',
            'additional_cost'      => '92.00',
            'logo_initial_cost'    => '1320.00',
            'logo_additional_cost' => '92.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 152,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 153,
            'service_type'        => 'Study',
            'initial_cost'        => '406.00',
            'additional_cost'     => '103.00',
            'logo_initial_cost'    => '406.00',
            'logo_additional_cost' => '103.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 153,
            'service_type'         => 'Registration',
            'initial_cost'         => '1126.00',
            'additional_cost'      => '257.00',
            'logo_initial_cost'    => '1126.00',
            'logo_additional_cost' => '257.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 153,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 154,
            'service_type'        => 'Study',
            'initial_cost'        => '121.00',
            'additional_cost'     => '121.00',
            'logo_initial_cost'    => '121.00',
            'logo_additional_cost' => '121.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 154,
            'service_type'         => 'Registration',
            'initial_cost'         => '736.00',
            'additional_cost'      => '432.00',
            'logo_initial_cost'    => '736.00',
            'logo_additional_cost' => '432.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 154,
            'service_type'         => 'Certificate',
            'initial_cost'         => '514.00',
            'additional_cost'      => '514.00',
            'logo_initial_cost'    => '514.00',
            'logo_additional_cost' => '514.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 155,
            'service_type'        => 'Study',
            'initial_cost'        => '406.00',
            'additional_cost'     => '103.00',
            'logo_initial_cost'    => '406.00',
            'logo_additional_cost' => '103.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 155,
            'service_type'         => 'Registration',
            'initial_cost'         => '1126.00',
            'additional_cost'      => '257.00',
            'logo_initial_cost'    => '1126.00',
            'logo_additional_cost' => '257.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 155,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 156,
            'service_type'        => 'Study',
            'initial_cost'        => '406.00',
            'additional_cost'     => '103.00',
            'logo_initial_cost'    => '406.00',
            'logo_additional_cost' => '103.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 156,
            'service_type'         => 'Registration',
            'initial_cost'         => '1126.00',
            'additional_cost'      => '257.00',
            'logo_initial_cost'    => '1126.00',
            'logo_additional_cost' => '257.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 156,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 157,
            'service_type'        => 'Study',
            'initial_cost'        => '406.00',
            'additional_cost'     => '103.00',
            'logo_initial_cost'    => '406.00',
            'logo_additional_cost' => '103.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 157,
            'service_type'         => 'Registration',
            'initial_cost'         => '1126.00',
            'additional_cost'      => '257.00',
            'logo_initial_cost'    => '1126.00',
            'logo_additional_cost' => '257.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 157,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 158,
            'service_type'        => 'Study',
            'initial_cost'        => '406.00',
            'additional_cost'     => '103.00',
            'logo_initial_cost'    => '406.00',
            'logo_additional_cost' => '103.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 158,
            'service_type'         => 'Registration',
            'initial_cost'         => '1126.00',
            'additional_cost'      => '257.00',
            'logo_initial_cost'    => '1126.00',
            'logo_additional_cost' => '257.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 158,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        // DB::table('prices')->insert([
        //     'country_name'        => 'Democratic Republic of the Congo',
        //     'service_type'        => 'Study',
        //     'initial_cost'        => '175.00',
        //     'additional_cost'     => '116.00',
        //     'logo_initial_cost'    => '175.00',
        //     'logo_additional_cost' => '116.00',
        //     'tax'                 => '0.00',
        //     'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
        //     'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        // ]);

        // DB::table('prices')->insert([
        //     'country_name'         => 'Democratic Republic of the Congo',
        //     'service_type'         => 'Registration',
        //     'initial_cost'         => '1076.00',
        //     'additional_cost'      => '432.00',
        //     'logo_initial_cost'    => '1076.00',
        //     'logo_additional_cost' => '432.00',
        //     'tax'                  => '0.00',
        //     'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
        //     'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        // ]);

        // DB::table('prices')->insert([
        //     'country_name'         => 'Democratic Republic of the Congo',
        //     'service_type'         => 'Certificate',
        //     'initial_cost'         => '0.00',
        //     'additional_cost'      => '0.00',
        //     'logo_initial_cost'    => '0.00',
        //     'logo_additional_cost' => '0.00',
 
        //     'tax'                  => '0.00',
        //     'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
        //     'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        // ]);

        DB::table('prices')->insert([
            'country_id'           => 159,
            'service_type'        => 'Study',
            'initial_cost'        => '406.00',
            'additional_cost'     => '103.00',
            'logo_initial_cost'    => '406.00',
            'logo_additional_cost' => '103.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 159,
            'service_type'         => 'Registration',
            'initial_cost'         => '1126.00',
            'additional_cost'      => '257.00',
            'logo_initial_cost'    => '1126.00',
            'logo_additional_cost' => '257.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 159,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 160,
            'service_type'        => 'Study',
            'initial_cost'        => '291.00',
            'additional_cost'     => '204.00',
            'logo_initial_cost'    => '291.00',
            'logo_additional_cost' => '204.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 160,
            'service_type'         => 'Registration',
            'initial_cost'         => '1987.00',
            'additional_cost'      => '644.00',
            'logo_initial_cost'    => '1987.00',
            'logo_additional_cost' => '644.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 160,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 161,
            'service_type'        => 'Study',
            'initial_cost'        => '107.00',
            'additional_cost'     => '73.00',
            'logo_initial_cost'    => '107.00',
            'logo_additional_cost' => '73.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 161,
            'service_type'         => 'Registration',
            'initial_cost'         => '304.00',
            'additional_cost'      => '258.00',
            'logo_initial_cost'    => '304.00',
            'logo_additional_cost' => '258.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 161,
            'service_type'         => 'Certificate',
            'initial_cost'         => '126.00',
            'additional_cost'      => '92.00',
            'logo_initial_cost'    => '126.00',
            'logo_additional_cost' => '92.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 162,
            'service_type'        => 'Study',
            'initial_cost'        => '406.00',
            'additional_cost'     => '103.00',
            'logo_initial_cost'    => '406.00',
            'logo_additional_cost' => '103.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 162,
            'service_type'         => 'Registration',
            'initial_cost'         => '1126.00',
            'additional_cost'      => '257.00',
            'logo_initial_cost'    => '1126.00',
            'logo_additional_cost' => '257.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 162,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 163,
            'service_type'        => 'Study',
            'initial_cost'        => '-1',
            'additional_cost'     => '-1',
            'logo_initial_cost'    => '-1',
            'logo_additional_cost' => '-1',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 163,
            'service_type'         => 'Registration',
            'initial_cost'         => '1182.00',
            'additional_cost'      => '598.00',
            'logo_initial_cost'    => '1182.00',
            'logo_additional_cost' => '598.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 163,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 164,
            'service_type'        => 'Study',
            'initial_cost'        => '223.00',
            'additional_cost'     => '223.00',
            'logo_initial_cost'    => '223.00',
            'logo_additional_cost' => '223.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 164,
            'service_type'         => 'Registration',
            'initial_cost'         => '1771.00',
            'additional_cost'      => '506.00',
            'logo_initial_cost'    => '2553.00',
            'logo_additional_cost' => '506.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 164,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 165,
            'service_type'        => 'Study',
            'initial_cost'        => '406.00',
            'additional_cost'     => '103.00',
            'logo_initial_cost'    => '406.00',
            'logo_additional_cost' => '103.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 165,
            'service_type'         => 'Registration',
            'initial_cost'         => '1126.00',
            'additional_cost'      => '257.00',
            'logo_initial_cost'    => '1126.00',
            'logo_additional_cost' => '257.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 165,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 166,
            'service_type'        => 'Study',
            'initial_cost'        => '175.00',
            'additional_cost'     => '116.00',
            'logo_initial_cost'    => '175.00',
            'logo_additional_cost' => '116.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 166,
            'service_type'         => 'Registration',
            'initial_cost'         => '1012.00',
            'additional_cost'      => '1012.00',
            'logo_initial_cost'    => '1012.00',
            'logo_additional_cost' => '1012.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 166,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 167,
            'service_type'        => 'Study',
            'initial_cost'        => '175.00',
            'additional_cost'     => '116.00',
            'logo_initial_cost'    => '175.00',
            'logo_additional_cost' => '116.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 167,
            'service_type'         => 'Registration',
            'initial_cost'         => '782.00',
            'additional_cost'      => '644.00',
            'logo_initial_cost'    => '782.00',
            'logo_additional_cost' => '644.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 167,
            'service_type'         => 'Certificate',
            'initial_cost'         => '388.00',
            'additional_cost'      => '369.00',
            'logo_initial_cost'    => '388.00',
            'logo_additional_cost' => '369.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 168,
            'service_type'        => 'Study',
            'initial_cost'        => '406.00',
            'additional_cost'     => '103.00',
            'logo_initial_cost'    => '406.00',
            'logo_additional_cost' => '103.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 168,
            'service_type'         => 'Registration',
            'initial_cost'         => '1126.00',
            'additional_cost'      => '257.00',
            'logo_initial_cost'    => '1126.00',
            'logo_additional_cost' => '257.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 168,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 169,
            'service_type'        => 'Study',
            'initial_cost'        => '406.00',
            'additional_cost'     => '103.00',
            'logo_initial_cost'    => '406.00',
            'logo_additional_cost' => '103.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 169,
            'service_type'         => 'Registration',
            'initial_cost'         => '1126.00',
            'additional_cost'      => '257.00',
            'logo_initial_cost'    => '1126.00',
            'logo_additional_cost' => '257.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 169,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 170,
            'service_type'        => 'Study',
            'initial_cost'        => '121.00',
            'additional_cost'     => '121.00',
            'logo_initial_cost'    => '121.00',
            'logo_additional_cost' => '121.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 170,
            'service_type'         => 'Registration',
            'initial_cost'         => '883.00',
            'additional_cost'      => '534.00',
            'logo_initial_cost'    => '883.00',
            'logo_additional_cost' => '534.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 170,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 171,
            'service_type'        => 'Study',
            'initial_cost'        => '116.00',
            'additional_cost'     => '116.00',
            'logo_initial_cost'    => '116.00',
            'logo_additional_cost' => '116.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 171,
            'service_type'         => 'Registration',
            'initial_cost'         => '-1',
            'additional_cost'      => '-1',
            'logo_initial_cost'    => '-1',
            'logo_additional_cost' => '-1',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 171,
            'service_type'         => 'Certificate',
            'initial_cost'         => '-1',
            'additional_cost'      => '-1',
            'logo_initial_cost'    => '-1',
            'logo_additional_cost' => '-1',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 172,
            'service_type'        => 'Study',
            'initial_cost'        => '78.00',
            'additional_cost'     => '78.00',
            'logo_initial_cost'    => '116.00',
            'logo_additional_cost' => '116.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 172,
            'service_type'         => 'Registration',
            'initial_cost'         => '1895.00',
            'additional_cost'      => '1868.00',
            'logo_initial_cost'    => '1895.00',
            'logo_additional_cost' => '1868.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 172,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 173,
            'service_type'        => 'Study',
            'initial_cost'        => '121.00',
            'additional_cost'     => '121.00',
            'logo_initial_cost'    => '121.00',
            'logo_additional_cost' => '121.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 173,
            'service_type'         => 'Registration',
            'initial_cost'         => '920.00',
            'additional_cost'      => '566.00',
            'logo_initial_cost'    => '920.00',
            'logo_additional_cost' => '566.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 173,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 174,
            'service_type'        => 'Study',
            'initial_cost'        => '108.00',
            'additional_cost'     => '108.00',
            'logo_initial_cost'    => '108.00',
            'logo_additional_cost' => '108.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 174,
            'service_type'         => 'Registration',
            'initial_cost'         => '791.00',
            'additional_cost'      => '87.00',
            'logo_initial_cost'    => '791.00',
            'logo_additional_cost' => '87.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 174,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 175,
            'service_type'        => 'Study',
            'initial_cost'        => '243.00',
            'additional_cost'     => '243.00',
            'logo_initial_cost'    => '243.00',
            'logo_additional_cost' => '243.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 175,
            'service_type'         => 'Registration',
            'initial_cost'         => '1996.00',
            'additional_cost'      => '1886.00',
            'logo_initial_cost'    => '1996.00',
            'logo_additional_cost' => '1886.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 175,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 176,
            'service_type'        => 'Study',
            'initial_cost'        => '406.00',
            'additional_cost'     => '103.00',
            'logo_initial_cost'    => '406.00',
            'logo_additional_cost' => '103.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 176,
            'service_type'         => 'Registration',
            'initial_cost'         => '1126.00',
            'additional_cost'      => '257.00',
            'logo_initial_cost'    => '1126.00',
            'logo_additional_cost' => '257.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);
 
        DB::table('prices')->insert([
            'country_id'           => 176,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 177,
            'service_type'        => 'Study',
            'initial_cost'        => '406.00',
            'additional_cost'     => '103.00',
            'logo_initial_cost'    => '406.00',
            'logo_additional_cost' => '103.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 177,
            'service_type'         => 'Registration',
            'initial_cost'         => '1126.00',
            'additional_cost'      => '257.00',
            'logo_initial_cost'    => '1126.00',
            'logo_additional_cost' => '257.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);
 
        DB::table('prices')->insert([
            'country_id'           => 177,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 178,
            'service_type'        => 'Study',
            'initial_cost'        => '78.00',
            'additional_cost'     => '78.00',
            'logo_initial_cost'    => '78.00',
            'logo_additional_cost' => '78.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 178,
            'service_type'         => 'Registration',
            'initial_cost'         => '653.00',
            'additional_cost'      => '138.00',
            'logo_initial_cost'    => '653.00',
            'logo_additional_cost' => '138.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);
 
        DB::table('prices')->insert([
            'country_id'           => 178,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 179,
            'service_type'        => 'Study',
            'initial_cost'        => '65.00',
            'additional_cost'     => '65.00',
            'logo_initial_cost'    => '65.00',
            'logo_additional_cost' => '65.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 179,
            'service_type'         => 'Registration',
            'initial_cost'         => '689.00',
            'additional_cost'      => '46.00',
            'logo_initial_cost'    => '689.00',
            'logo_additional_cost' => '46.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);
 
        DB::table('prices')->insert([
            'country_id'           => 179,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 180,
            'service_type'        => 'Study',
            'initial_cost'        => '121.00',
            'additional_cost'     => '78.00',
            'logo_initial_cost'    => '121.00',
            'logo_additional_cost' => '78.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 180,
            'service_type'         => 'Registration',
            'initial_cost'         => '736.00',
            'additional_cost'      => '644.00',
            'logo_initial_cost'    => '736.00',
            'logo_additional_cost' => '644.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);
 
        DB::table('prices')->insert([
            'country_id'           => 180,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 181,
            'service_type'        => 'Study',
            'initial_cost'        => '72.00',
            'additional_cost'     => '72.00',
            'logo_initial_cost'    => '72.00',
            'logo_additional_cost' => '72.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 181,
            'service_type'         => 'Registration',
            'initial_cost'         => '519.00',
            'additional_cost'      => '476.00',
            'logo_initial_cost'    => '1039.00',
            'logo_additional_cost' => '950.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);
 
        DB::table('prices')->insert([
            'country_id'           => 181,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 182,
            'service_type'        => 'Study',
            'initial_cost'        => '97.00',
            'additional_cost'     => '97.00',
            'logo_initial_cost'    => '97.00',
            'logo_additional_cost' => '97.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 182,
            'service_type'         => 'Registration',
            'initial_cost'         => '745.00',
            'additional_cost'      => '653.00',
            'logo_initial_cost'    => '745.00',
            'logo_additional_cost' => '653.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);
 
        DB::table('prices')->insert([
            'country_id'           => 182,
            'service_type'         => 'Certificate',
            'initial_cost'         => '378.00',
            'additional_cost'      => '340.00',
            'logo_initial_cost'    => '378.00',
            'logo_additional_cost' => '340.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 183,
            'service_type'        => 'Study',
            'initial_cost'        => '97.00',
            'additional_cost'     => '97.00',
            'logo_initial_cost'    => '97.00',
            'logo_additional_cost' => '97.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 183,
            'service_type'         => 'Registration',
            'initial_cost'         => '745.00',
            'additional_cost'      => '653.00',
            'logo_initial_cost'    => '745.00',
            'logo_additional_cost' => '653.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);
 
        DB::table('prices')->insert([
            'country_id'           => 183,
            'service_type'         => 'Certificate',
            'initial_cost'         => '378.00',
            'additional_cost'      => '340.00',
            'logo_initial_cost'    => '378.00',
            'logo_additional_cost' => '340.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 184,
            'service_type'        => 'Study',
            'initial_cost'        => '78.00',
            'additional_cost'     => '78.00',
            'logo_initial_cost'    => '78.00',
            'logo_additional_cost' => '78.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 184,
            'service_type'         => 'Registration',
            'initial_cost'         => '1780.00',
            'additional_cost'      => '658.00',
            'logo_initial_cost'    => '1780.00',
            'logo_additional_cost' => '658.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);
 
        DB::table('prices')->insert([
            'country_id'           => 184,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 185,
            'service_type'        => 'Study',
            'initial_cost'        => '121.00',
            'additional_cost'     => '121.00',
            'logo_initial_cost'    => '121.00',
            'logo_additional_cost' => '121.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 185,
            'service_type'         => 'Registration',
            'initial_cost'         => '984.00',
            'additional_cost'      => '359.00',
            'logo_initial_cost'    => '984.00',
            'logo_additional_cost' => '359.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);
 
        DB::table('prices')->insert([
            'country_id'           => 185,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 186,
            'service_type'        => 'Study',
            'initial_cost'        => '243.00',
            'additional_cost'     => '68.00',
            'logo_initial_cost'    => '243.00',
            'logo_additional_cost' => '68.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 186,
            'service_type'         => 'Registration',
            'initial_cost'         => '1288.00',
            'additional_cost'      => '152.00',
            'logo_initial_cost'    => '1288.00',
            'logo_additional_cost' => '152.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);
 
        DB::table('prices')->insert([
            'country_id'           => 186,
            'service_type'         => 'Certificate',
            'initial_cost'         => '1358.00',
            'additional_cost'      => '601.00',
            'logo_initial_cost'    => '1358.00',
            'logo_additional_cost' => '601.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 187,
            'service_type'        => 'Study',
            'initial_cost'        => '406.00',
            'additional_cost'     => '103.00',
            'logo_initial_cost'    => '406.00',
            'logo_additional_cost' => '103.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 187,
            'service_type'         => 'Registration',
            'initial_cost'         => '1126.00',
            'additional_cost'      => '257.00',
            'logo_initial_cost'    => '1126.00',
            'logo_additional_cost' => '257.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);
 
        DB::table('prices')->insert([
            'country_id'           => 187,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 188,
            'service_type'        => 'Study',
            'initial_cost'        => '121.00',
            'additional_cost'     => '121.00',
            'logo_initial_cost'    => '121.00',
            'logo_additional_cost' => '121.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 188,
            'service_type'         => 'Registration',
            'initial_cost'         => '754.00',
            'additional_cost'      => '727.00',
            'logo_initial_cost'    => '754.00',
            'logo_additional_cost' => '727.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);
 
        DB::table('prices')->insert([
            'country_id'           => 188,
            'service_type'         => 'Certificate',
            'initial_cost'         => '388.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '388.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 189,
            'service_type'        => 'Study',
            'initial_cost'        => '116.00',
            'additional_cost'     => '78.00',
            'logo_initial_cost'    => '116.00',
            'logo_additional_cost' => '78.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 189,
            'service_type'         => 'Registration',
            'initial_cost'         => '856.00',
            'additional_cost'      => '534.00',
            'logo_initial_cost'    => '856.00',
            'logo_additional_cost' => '534.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);
 
        DB::table('prices')->insert([
            'country_id'           => 189,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 190,
            'service_type'        => 'Study',
            'initial_cost'        => '184.00',
            'additional_cost'     => '184.00',
            'logo_initial_cost'    => '184.00',
            'logo_additional_cost' => '184.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 190,
            'service_type'         => 'Registration',
            'initial_cost'         => '635.00',
            'additional_cost'      => '635.00',
            'logo_initial_cost'    => '635.00',
            'logo_additional_cost' => '635.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);
 
        DB::table('prices')->insert([
            'country_id'           => 190,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 191,
            'service_type'        => 'Study',
            'initial_cost'        => '243.00',
            'additional_cost'     => '68.00',
            'logo_initial_cost'    => '243.00',
            'logo_additional_cost' => '68.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 191,
            'service_type'         => 'Registration',
            'initial_cost'         => '1288.00',
            'additional_cost'      => '152.00',
            'logo_initial_cost'    => '1288.00',
            'logo_additional_cost' => '152.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);
 
        DB::table('prices')->insert([
            'country_id'           => 191,
            'service_type'         => 'Certificate',
            'initial_cost'         => '1358.00',
            'additional_cost'      => '601.00',
            'logo_initial_cost'    => '1358.00',
            'logo_additional_cost' => '601.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 192,
            'service_type'        => 'Study',
            'initial_cost'        => '121.00',
            'additional_cost'     => '121.00',
            'logo_initial_cost'    => '121.00',
            'logo_additional_cost' => '121.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 192,
            'service_type'         => 'Registration',
            'initial_cost'         => '842.00',
            'additional_cost'      => '685.00',
            'logo_initial_cost'    => '842.00',
            'logo_additional_cost' => '685.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);
 
        DB::table('prices')->insert([
            'country_id'           => 192,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 193,
            'service_type'        => 'Study',
            'initial_cost'        => '175.00',
            'additional_cost'     => '116.00',
            'logo_initial_cost'    => '175.00',
            'logo_additional_cost' => '175.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 193,
            'service_type'         => 'Registration',
            'initial_cost'         => '920.00',
            'additional_cost'      => '497.00',
            'logo_initial_cost'    => '920.00',
            'logo_additional_cost' => '685.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);
 
        DB::table('prices')->insert([
            'country_id'           => 193,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 194,
            'service_type'        => 'Study',
            'initial_cost'        => '78.00',
            'additional_cost'     => '78.00',
            'logo_initial_cost'    => '78.00',
            'logo_additional_cost' => '78.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 194,
            'service_type'         => 'Registration',
            'initial_cost'         => '598.00',
            'additional_cost'      => '129.00',
            'logo_initial_cost'    => '598.00',
            'logo_additional_cost' => '129.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);
 
        DB::table('prices')->insert([
            'country_id'           => 194,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 195,
            'service_type'        => 'Study',
            'initial_cost'        => '121.00',
            'additional_cost'     => '121.00',
            'logo_initial_cost'    => '121.00',
            'logo_additional_cost' => '121.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 195,
            'service_type'         => 'Registration',
            'initial_cost'         => '902.00',
            'additional_cost'      => '805.00',
            'logo_initial_cost'    => '902.00',
            'logo_additional_cost' => '805.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);
 
        DB::table('prices')->insert([
            'country_id'           => 195,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 196,
            'service_type'        => 'Study',
            'initial_cost'        => '121.00',
            'additional_cost'     => '121.00',
            'logo_initial_cost'    => '121.00',
            'logo_additional_cost' => '121.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 196,
            'service_type'         => 'Registration',
            'initial_cost'         => '1040.00',
            'additional_cost'      => '920.00',
            'logo_initial_cost'    => '1040.00',
            'logo_additional_cost' => '920.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);
 
        DB::table('prices')->insert([
            'country_id'           => 196,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 197,
            'service_type'        => 'Study',
            'initial_cost'        => '78.00',
            'additional_cost'     => '58.00',
            'logo_initial_cost'    => '78.00',
            'logo_additional_cost' => '58.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 197,
            'service_type'         => 'Registration',
            'initial_cost'         => '819.00',
            'additional_cost'      => '331.00',
            'logo_initial_cost'    => '819.00',
            'logo_additional_cost' => '331.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);
 
        DB::table('prices')->insert([
            'country_id'           => 197,
            'service_type'         => 'Certificate',
            'initial_cost'         => '184.00',
            'additional_cost'      => '126.00',
            'logo_initial_cost'    => '184.00',
            'logo_additional_cost' => '126.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 198,
            'service_type'        => 'Study',
            'initial_cost'        => '243.00',
            'additional_cost'     => '68.00',
            'logo_initial_cost'    => '243.00',
            'logo_additional_cost' => '68.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 198,
            'service_type'         => 'Registration',
            'initial_cost'         => '1288.00',
            'additional_cost'      => '152.00',
            'logo_initial_cost'    => '1288.00',
            'logo_additional_cost' => '152.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);
 
        DB::table('prices')->insert([
            'country_id'           => 198,
            'service_type'         => 'Certificate',
            'initial_cost'         => '1358.00',
            'additional_cost'      => '601.00',
            'logo_initial_cost'    => '1358.00',
            'logo_additional_cost' => '601.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 199,
            'service_type'        => 'Study',
            'initial_cost'        => '406.00',
            'additional_cost'     => '103.00',
            'logo_initial_cost'    => '406.00',
            'logo_additional_cost' => '103.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 199,
            'service_type'         => 'Registration',
            'initial_cost'         => '1126.00',
            'additional_cost'      => '257.00',
            'logo_initial_cost'    => '1126.00',
            'logo_additional_cost' => '257.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);
 
        DB::table('prices')->insert([
            'country_id'           => 199,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 200,
            'service_type'        => 'Study',
            'initial_cost'        => '116.00',
            'additional_cost'     => '78.00',
            'logo_initial_cost'    => '116.00',
            'logo_additional_cost' => '78.00',
            'tax'                 => '0.00',
            'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'          => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('prices')->insert([
            'country_id'           => 200,
            'service_type'         => 'Registration',
            'initial_cost'         => '741.00',
            'additional_cost'      => '400.00',
            'logo_initial_cost'    => '471.00',
            'logo_additional_cost' => '400.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);
 
        DB::table('prices')->insert([
            'country_id'           => 200,
            'service_type'         => 'Certificate',
            'initial_cost'         => '0.00',
            'additional_cost'      => '0.00',
            'logo_initial_cost'    => '0.00',
            'logo_additional_cost' => '0.00',
            'tax'                  => '0.00',
            'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
