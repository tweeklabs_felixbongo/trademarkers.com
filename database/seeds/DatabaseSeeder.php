<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
	        ContinentsTableSeeder::class,
	        CountriesTableSeeder::class,
	        PricesTableSeeder::class,
	        FormatsTableSeeder::class,
            ClassesTableSeeder::class,
            ClassesDescriptionTableSeeder::class,
            UsersTableSeeder::class,
            ActionCasesSeeder::class,
	    ]);
    }
}
