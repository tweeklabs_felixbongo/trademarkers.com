<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\User;
use App\Profile;
use App\Order;
use App\Payment;
use App\Trademark;

class OldDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('users')->insert([
			"email" => "mdakssa7@gmail.com",
			"name"  => "UVUKILE UMALAMBANE CLOTHING",
			"ipaddress" => "154.69.12.203",
			"password" => "Oj2GPvgkgd8519b87ec9d98b0439e90ff69f0fdf1",
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "alintintila@yahoo.com",
			"name"  => "Alin",
			"ipaddress" => "86.20.63.61",
			"password" => "vdQdqxAFr5ce20db3c7598114bf4d28d30f008869",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "jacobthompson369@gmail.com",
			"name"  => "Jacob Thompson",
			"ipaddress" => "173.80.195.120",
			"password" => "o4axYbebpdd8063db64e48e568f59345fc7d31e17",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "matthewbrain26@gmail.com",
			"name"  => "Matthew Brain",
			"ipaddress" => "70.69.122.203",
			"password" => "ELvd6lzSN96cb551d79f1c9937d6b0c666603eb72",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			
			"email" => "xolanisibongiseni@gamil",
			"name"  => "Xolani",
			"password" => "JkrlQHLbh0bb097787387345b8b62f00001cd861e",
			"ipaddress" => "41.115.84.42",
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "iamjaccuzi@gmail.com",
			"name"  => "Hall of fame Record label",
			"ipaddress" => "168.195.94.58",
			"password" => "BdDnzkEPYb5bc6ad36088ece08c5a6ca2904e7a5c",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "foreign@51ip.com.cn",
			"name"  => "51ipcomcn",
			"ipaddress" => "119.98.116.254",
			"password" => "9cZh8KgFK48368c9bf68a4ddd84fd5bb00bf2267b",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "thedolledupbeauty@gmail.com",
			"name"  => "Shakeda",
			"ipaddress" => "174.236.149.180",
			"password" => "xxUVfQ9vb3443869b149d4b6977b8f180e95f5f92",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "victor2014kai@icloud.com",
			"name"  => "Kai yan Liao",
			"ipaddress" => "27.253.105.174",
			"password" => "qGF06YBJWf7f80647294d41e77fcd672eb352957f",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "robydagan@gmail.com",
			"name"  => "nanomass technologuy srl",
			"password" => "lKZg5jEsbe8d8ec4dcdb5c5cacc2f3b9c273265f4",
			"ipaddress" => "5.29.180.74",
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "judyraby@jarbarre.com",
			"name"  => "JAR Barre",
			"ipaddress" => "24.211.225.5",
			"password" => "VtWtVqfXEf6d8561e15b457fe97a83ef4f51240d3",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "Lesegoleseane@gmail.com",
			"name"  => "Lesego Leseane",
			"ipaddress" => "102.252.127.174",
			"password" => "uarJWtzWu3bd90e71cf5dc9c93861de5a7ba4e158",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "nancymia54@yahoo.com",
			"name"  => "Nancy Redmond",
			"ipaddress" => "97.73.97.23",
			"password" => "FGOuX6rOXbc6ad1f145321fc18ff51f813a4f2d3d",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "slachup@hotmail.com",
			"name"  => "Sylvain lachpelle",
			"ipaddress" => "96.22.22.40",
			"password" => "UMa7NJfGwdd90aa20078ea23ebc8080b67aa79a80",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			
			"email" => "cbourne1970@gmail.com",
			"name"  => "Claire",
			"password" => "lA7ooTcfgb9ff6250fe534608dd5c1f72dc01b9f6",
			"ipaddress" => "5.80.202.9",
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "kgotsomoroke02@gmail.com",
			"name"  => "MOROKE’s INQUISITIVE_SA APPAREL",
			"ipaddress" => "154.69.178.161",
			"password" => "bjXqEjiIb54434897011227379de82c3502d107ec",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "successigrab@gmail.com",
			"name"  => "Gregjanae brooks",
			"ipaddress" => "108.223.174.91",
			"password" => "rbds7Qbbe529e0e139940d1c961710bd5ff52f8ed",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "galegro@hotmail.com",
			"name"  => "George Moran",
			"ipaddress" => "137.186.103.66",
			"password" => "XqE5uQ0tv9d5b66fa225d4d256976a81c217d7579",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "juangmotta@aol.com",
			"name"  => "Juan Motta",
			"ipaddress" => "201.17.157.194",
			"password" => "ffyLPkA0Ya63863163329e4290382e2c7f38a162c",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "spmthembu@ithala.co.za",
			"name"  => "Skhona Mthembu",
			"ipaddress" => "41.13.136.77",
			"password" => "BVH3eFQ5u6291f25398a617175162c9b24a6108ab",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "lordbyronsbackribs@gmail.com",
			"name"  => "Lord Byron BackRibs Grille",
			"ipaddress" => "222.119.248.61",
			"password" => "oelUuYDS73c46f943806b3f9f582522515e74420c",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "Batiegreene@gmaol.com",
			"name"  => "Regina Batie Greene",
			"ipaddress" => "71.199.254.71",
			"password" => "dRB0U0Ncg3272e74a36f64bad46cddf13a907e0c5",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "carlringer419@gmail.com",
			"name"  => "WaTseba Clothing",
			"ipaddress" => "165.255.195.95",
			"password" => "ajiJM42Sj46e7a2e4f3b09b28ea0325cff95fe922",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "siyandanombiba@gmail.com",
			"name"  => "Bright Sewing Ideas",
			"ipaddress" => "105.227.220.40",
			"password" => "TJigTVvcwdf863d2f888483aeed1cb1a5f09742bd",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "janneke@whalewatchcabo.com",
			"name"  => "Whale Watch Cabo",
			"ipaddress" => "200.68.149.92",
			"password" => "sh7viwXzg0bc34bfb37ff29c52b4e976cff292d4e",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "tshepzen235@gmail.com",
			"name"  => "Tshepiso Sedupane",
			"ipaddress" => "105.4.5.19",
			"password" => "q0fhmk1BK964465f06affbd71ccd9609c4f81ca9b",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "amzsalescd@gmail.com",
			"name"  => "C. BOROWSKI LLC",
			"ipaddress" => "201.6.134.190",
			"password" => "u8t84SyUW5d60035d4738fd94a38a5860f16ce8e5",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "gcgomezverea@gmail.com",
			"name"  => "Gilberto",
			"ipaddress" => "189.168.165.17",
			"password" => "SPiI1K3RX3adc8d0b9b25cf5474802fb962d9b960",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "Amarinio@live.fr",
			"name"  => "Come \u0026 Go",
			"ipaddress" => "50.7.114.12",
			"password" => "RQXQFJOnU2a5316113382ffd756dd32eed21e1cf3",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "jhon@trademarkers.com",
			"name"  => "Jhon Pie",
			"ipaddress" => "203.177.94.36",
			"password" => "bZAeKF2Kud5edc937092d23af4e0d7869d97b2970",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "stefanie@peduti.com.br",
			"name"  => "Smiley Company",
			"ipaddress" => "191.162.239.33",
			"password" => "12UUURNUT0cb7905c9cfa84e5cf4359544d835e18",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "Kidhazard100@gmail.com",
			"name"  => "Deeq mohammed",
			"ipaddress" => "83.254.200.121",
			"password" => "wyuXzFq5zea0607342f7fb384a75cb78f435dad2c",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "saraflack@gmail.com",
			"name"  => "Sara Flack",
			"ipaddress" => "77.127.117.125",
			"password" => "yy2sgMury8ba2afaa93442af8b8d0407307ede8f0",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "info@eco-sol.nl",
			"name"  => "Ecological Solutions B.V.",
			"ipaddress" => "85.144.16.162",
			"password" => "9juPSbylOc71f5d6e4c93cd9957561454de1046f3",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "dr.leocouto71@gmsil.com",
			"name"  => "Oca Cafe",
			"ipaddress" => "70.68.171.242",
			"password" => "2WYEq0cOZc6d6421ad4a5ad21d4ada50af393fb62",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "KDellensen@gmail.com",
			"name"  => "KAAJ",
			"ipaddress" => "77.160.159.129",
			"password" => "UNhUCGnap6aa96246d09f98ba8d3429ffb057194c",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "Springfieldsx10@gmail.com",
			"name"  => "Pari capital limited",
			"ipaddress" => "182.239.115.111",
			"password" => "Fu7h2dGRP73a763d003cd4147f669b27c6d3c2800",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "officialcollaboza@gmail.con",
			"name"  => "Luyanda",
			"ipaddress" => "41.13.92.55",
			"password" => "KVxQRtbOx791599d2a69e17a3f07200fee353659d",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "mgrimes01@charter.net",
			"name"  => "Michelle Grimes",
			"ipaddress" => "172.58.143.137",
			"password" => "ISqgdsP8N11e738d07da479538874eb921c343748",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			
			"email" => "MARIO.ADAME@HOTMAIL.COM",
			"name"  => "MARIO ADAME",
			"password" => "E4v9BUvex7b278658d397c9db142a2d64028a94b7",
			"ipaddress" => "201.146.79.123",
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "adriangarzabenavides@gmail.com",
			"name"  => "Adrian",
			"ipaddress" => "189.152.203.82",
			"password" => "QsOjgdyVad09c904c9b08f450a1ce79770e6a6b72",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "alexander.n.lebed@gmail.com",
			"name"  => "Alexander",
			"ipaddress" => "31.4.230.45",
			"password" => "WYLSY21Vr2c4e979b1752ed813b76a86a12fbecf9",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "maureenvillarino@gmail.com",
			"name"  => "Resti Tito H.  Villarino",
			"ipaddress" => "110.54.243.161",
			"password" => "CcDO0u4WJ68c46d891879bf5d5b5b2044649ff7d4",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "sevriugin@gmail.com",
			"name"  => "Sergei Sevriugin",
			"ipaddress" => "62.105.140.86",
			"password" => "1aDnZlbd690693ddff20cfb6cb2febed0d228470a",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "sportscartoy@usa.com",
			"name"  => "simon harding",
			"ipaddress" => "42.241.243.121",
			"password" => "BwETOs8rQ8b1842f8e664be359f7ed0619a782318",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "omarlindsay@hotmail.co.uk",
			"name"  => "omar lindsay",
			"ipaddress" => "92.40.248.46",
			"password" => "ILGkHEeYef687d2219488f9d6ad091edc4724f06e",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "konahcrusoe5@gmail.com",
			"name"  => "Konah Crusoe",
			"ipaddress" => "209.107.214.87",
			"password" => "WYjgg7okg4ce845c4d9844c205eaf4a081611fdc7",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			
			"email" => "info@abmlondon.co.uk",
			"name"  => "Mahmut Tuyluoglu",
			"password" => "jTQcufKCu9f6e0948922ae9e83b10d1b0ee62b5c3",
			"ipaddress" => "31.55.250.51",
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "info@canadaibs.ca",
			"name"  => "Canada IBS laser Science and Technology Co., Ltd.",
			"ipaddress" => "108.174.166.200",
			"password" => "IYEvz31Th7d4731068ff5f0e127cac745d845d8f0",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "solroberts1@gmail.com",
			"name"  => "Sol",
			"ipaddress" => "94.14.216.253",
			"password" => "t40xdimiu6b61bf7c822e8530eb646b9e83c8f75a",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "fhfaisal2016@gmail.com",
			"name"  => "faruk",
			"ipaddress" => "103.15.245.58",
			"password" => "FiZfuB6Yz0a43aa50e663da2b07222e9316169345",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "hady@mazayainc.com",
			"name"  => "hady",
			"ipaddress" => "89.148.42.12",
			"password" => "0g1tZCNHZ162833330fb977a8d5bae68ae358cce1",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "Dijkarbeyaz@gmail.com",
			"name"  => "Eylem Dogan",
			"ipaddress" => "82.42.163.236",
			"password" => "1nwfdVOPA2e6fb7c6b41bff493122d8a9510b5ec4",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "ava@rics.ch",
			"name"  => "RICS Gmbh",
			"ipaddress" => "178.197.235.83",
			"password" => "TEx3K3FDd885be5258154d4399d9fa650a6107f75",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "salazarivyjoy@live.com",
			"name"  => "Ivyjoy Salazar",
			"ipaddress" => "92.97.152.134",
			"password" => "yFU4RkNEH41620536691329936f05a23cbe48cff7",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "prostoyaric@gmail.com",
			"name"  => "Etriy",
			"ipaddress" => "91.76.169.41",
			"password" => "0KpkMIasb1c2348e8c42bfe24df7d421935c29953",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			
			"email" => "alexandregja@gmail.com",
			"name"  => "Alexandre Fernandes Andrade",
			"password" => "k1qfXrKfZ471d2eda7c3c75572aa3dcd4fd2b6c63",
			"ipaddress" => "187.74.165.198",
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "lrslatoya17@yahoo.com",
			"name"  => "Latoya McGee",
			"ipaddress" => "98.239.25.39",
			"password" => "MFVBx5WFi32f250292775a6934659cd1cf5908d38",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "nickstekovic@hotmail.com",
			"name"  => "korgix300",
			"ipaddress" => "184.147.23.31",
			"password" => "1wTUeBXrQ3a0656556c52d6f8cca9bca57a065c9f",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "info@choose2be.ca",
			"name"  => "Veronique",
			"ipaddress" => "96.54.162.7",
			"password" => "ExnmOJdnG74c9c6a1f36c2d036766b5aa63ba7703",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "gianfrancoderosa69@gmail.com",
			"name"  => "Gianfranco De Rosa",
			"ipaddress" => "93.32.79.190",
			"password" => "p86GDLEvc6adc86f9d4460d285d7b729f039ab6da",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "jryme369@gmail.com",
			"name"  => "Justin",
			"ipaddress" => "73.217.207.120",
			"password" => "5KbS6F8Jk020918ac7a87efe4a53f73c877f69f2b",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "mdiazorlando@gmail.com",
			"name"  => "Mario Diaz",
			"ipaddress" => "97.103.43.86",
			"password" => "nMPMm8auHa9c8242c938713cd436e0205ff4df295",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			
			"email" => "lighthouseproductionmalta@gmail.com",
			"name"  => "Jorge",
			"password" => "BpwZMRRiF2e9e2692de29ca684a647531e923d5f9",
			"ipaddress" => "37.75.54.19",
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "Michaellusk@gmail.com",
			"name"  => "Michael G Lusk",
			"ipaddress" => "107.77.229.174",
			"password" => "6NiGaJnu765a441278090777c4c172daf3b808bc1",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "nivraimonodidepse24@gmail.com",
			"name"  => "Raymond arvin",
			"ipaddress" => "110.54.140.141",
			"password" => "m1WlPlBrS2a7b7a42f8e0ff56f6dda648a02b2c3f",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "oliverderrickcowley@hotmail.com",
			"name"  => "Oliver Cowley",
			"ipaddress" => "86.134.235.220",
			"password" => "a9B3LtrUZd1f742edc0f310560b8c5b42e870ec6b",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "drummike75@gmail.com",
			"name"  => "Michael Wake",
			"ipaddress" => "88.14.200.202",
			"password" => "fPMuMu10ud2513cfe91d170f8eb32640e4069c4b0",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			
			"email" => "deejayanapaula@gmail.com",
			"name"  => "Ana Paula Magalhaes Carneiro",
			"password" => "NbYjj7jAJ3e709dc88a0864e11d5d065572537bd8",
			"ipaddress" => "79.152.18.137",
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "shaunalumpkin@aol.com",
			"name"  => "shauna lumpkin",
			"ipaddress" => "62.196.178.41",
			"password" => "5qAhWzKjw491aa742cc37a04f277dada7e5fa2938",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "urinboyevvv@mail.ru",
			"name"  => "URINBOEV ABBOSBEK",
			"ipaddress" => "223.62.179.33",
			"password" => "1dV0l2z9ce831d76c8b2036612081633a4a6cbb6d",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "antheathemis@gmail.com",
			"name"  => "Anthea Antoniou",
			"ipaddress" => "94.224.35.103",
			"password" => "dajrGqUsUd9e3a0f19fe70d38abfb992654d5997e",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "fg942697@gmail.com",
			"name"  => "Fernando",
			"ipaddress" => "184.4.125.83",
			"password" => "Ua9OZoukH53eea31360886616f94028e807951863",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "john_salen@hotmail.com",
			"name"  => "John Salen",
			"ipaddress" => "189.50.12.34",
			"password" => "MFDGaKjPi0d363d6cbe24be0f8ddde7a1ef54baa7",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "crouch0192@yahoo.com",
			"name"  => "Chris crouch",
			"ipaddress" => "174.235.13.181",
			"password" => "ONuTeSAqQ11c72e30542df0eadfd7babdc1b297c3",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "adamharrisgolf@yahoo.co.uk",
			"name"  => "Adam Harris",
			"ipaddress" => "85.255.234.146",
			"password" => "dzqnTSEkobc05fbadcd08feda805dd3b59d022bce",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "purehustleent215@gmail.com",
			"name"  => "Pure Hustle Entertainment LLC",
			"ipaddress" => "173.49.9.13",
			"password" => "HUDRRvCS997a7ab422836228e34f5d7024b750de1",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "clowrie@clearlysupplements.com",
			"name"  => "Chris Lowrie",
			"ipaddress" => "82.18.56.216",
			"password" => "XWvweWii2f067c0219abb6e2e6972af9f9fce6a27",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "admin@nesteggenterprise.com",
			"name"  => "Nestegg Enterprise pty ltd",
			"ipaddress" => "49.183.148.169",
			"password" => "h5CADGGbN2e9a3bbf06c596f3bdd9f617b5868fcc",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "fashionistainme18@gmail.com",
			"name"  => "Cristian cuevas",
			"ipaddress" => "72.143.197.66",
			"password" => "awTUzayiS03e3fb928e9191f939f4f7644b15592f",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			
			"email" => "Yatulbarokahriski@gmail.com",
			"name"  => "Riski Art",
			"password" => "lqpIxD1U8f219b860f420c925e46c2d67355a2bef",
			"ipaddress" => "120.188.7.131",
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "candylesufi300@gmail.com",
			"name"  => "300 Gang",
			"ipaddress" => "102.250.4.168",
			"password" => "9CKmUwhySfa1b2db2c6e0bf6b9659505178dd94d0",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "anthony@trademarkers.com",
			"name"  => "Anthony Yuji",
			"ipaddress" => "203.177.94.36",
			"password" => "JyfUbiqTX0f23aa040a864e79a1da466453e58479",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "marittes@trademarkers.com",
			"name"  => "Marie",
			"ipaddress" => "203.177.94.36",
			"password" => "6v63Ftg5G7e0dca3eb62d3d643ab1a9cf6726ca59",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "janette.lazaraga@asia.bigfoot.com",
			"name"  => "Jan",
			"ipaddress" => "203.177.94.36",
			"password" => "pPYPHh3xWd23121451f2a01d9199d219616298f5b",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			
			"email" => "hector@mavhgroup.com",
			"name"  => "Said Henaine Quintanilla",
			"password" => "UNiqSwYMy99c960fbf4a72eecb5bc7d30bc2c8029",
			"ipaddress" => "189.236.61.179",
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "alshabazhaider99@yahoo.com",
			"name"  => "madanistar electronics",
			"ipaddress" => "2.50.8.98",
			"password" => "QwPYWKNe4670c87af4261478533ad4b8272f62f4f",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "christiaan.schlebusch@gmail.com",
			"name"  => "Christiaan Schlebusch",
			"ipaddress" => "196.36.167.46",
			"password" => "Eml4KFtK58b5a59c86245b226b79835333b67aa8e",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "tameisha5915@gmail.com",
			"name"  => "Tameisha Williams",
			"ipaddress" => "66.87.152.216",
			"password" => "NH98S8kKJ600dfa79a8b77ddbbb604c1e19f52724",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "Kabelomazibuko.km@gmail.com",
			"name"  => "Crusty.af",
			"ipaddress" => "41.13.8.28",
			"password" => "Hs9A6gblJ212a610d9de9aa2e18c0a59f0e5bc259",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "ibrahimbellobature1@gmail.com",
			"name"  => "ibrahim bello bature",
			"ipaddress" => "197.210.11.4",
			"password" => "kM2xwTGknb83d027243317bbba6f521099bcca0af",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			
			"email" => "2516275594@qq.com",
			"name"  => "王宪竹",
			"password" => "3EF0fkpw4188fe2872b7b0ebcb2ed7e5d5f8d6391",
			"ipaddress" => "112.9.222.71",
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "djafromotxo@gmail.com",
			"name"  => "michael",
			"ipaddress" => "105.247.25.122",
			"password" => "TEbQPcU8l754bdccf2df1b6cf3d79cfe104516bc2",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "779958459@qq.com",
			"name"  => "御医",
			"ipaddress" => "206.189.93.136",
			"password" => "jYX4YWeLG5c4f13f1fb3562deddbc4125bce202cc",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "ruba_ansari@hotmail.com",
			"name"  => "Délicieux",
			"ipaddress" => "82.42.187.9",
			"password" => "lnP582hTwcfe2c4806a83ef4a1a06822d3f8d9ecc",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "fis_001@live.cn",
			"name"  => "fisinsky2019",
			"ipaddress" => "27.72.173.142",
			"password" => "uvNKiu21Vc7da3bb0569e26639dd3910e73a09168",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "tailo2701973@gmail.com",
			"name"  => "Lo van tai",
			"ipaddress" => "113.185.11.108",
			"password" => "UmSopWxu37e0702507060238a12e356e71feec0a3",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "khan0322@naver.com",
			"name"  => "khan kim",
			"ipaddress" => "223.62.190.208",
			"password" => "n88dXiXqf07e66b23469433ad61be3eeb4f747746",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "angelgoal77@gmail.com",
			"name"  => "miguel",
			"ipaddress" => "191.89.247.10",
			"password" => "s0VGa4O5k683478d7923f3bb3fe5f7a997989c054",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "455894173@qq.com",
			"name"  => "Song",
			"ipaddress" => "150.129.56.253",
			"password" => "PUCxGbduh3c5fd18dded3b20eccac229eb155b808",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "meikewerk@t-online.de",
			"name"  => "Meike Werk",
			"ipaddress" => "79.192.243.245",
			"password" => "qbY3MAPQNabbc118601b1dcc3f0569ad49b72b378",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "george.abbott@hotmail.com",
			"name"  => "George",
			"ipaddress" => "203.177.94.36",
			"password" => "WEjTKbbtM9265d3aaaba11f506ebe6d60ce9b0401",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "jihadalzeod@yahoo.com",
			"name"  => "jihad",
			"ipaddress" => "31.215.164.120",
			"password" => "YDBtKhJdA91d6c1ce456aa1ca0bb18923e6c4427a",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "amazon@rock-black.com",
			"name"  => "Manhattan DJ Shop",
			"ipaddress" => "141.226.24.61",
			"password" => "7pUhrunPU3f58c02971a94cd8e8da81123f17a738",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "vramirez@live.cl",
			"name"  => "Cleanhub",
			"ipaddress" => "64.66.18.55",
			"password" => "yTUj4U5yL08e7d7d482a8833bbe1ded0de2f6744d",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "luka.bottega@gmail.com",
			"name"  => "Luca",
			"ipaddress" => "91.253.161.124",
			"password" => "HORwvIEVI1eb7d50f4c492798354ade7a5915305c",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "susilawatimaisis29@gmail.com",
			"name"  => "Maisis",
			"ipaddress" => "112.215.245.179",
			"password" => "BqU3Uu5oqdcd753b7a5db42d2b02c41b8bc1da4d2",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "CEO@SHOPFOCALLURE.COM",
			"name"  => "A AND A SUCCESS LLC",
			"ipaddress" => "213.57.144.185",
			"password" => "Qluouauw1f5f884c6a1cc98d98ba290529aca3d3b",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "J13977jj@gmail.com",
			"name"  => "James Singleton",
			"ipaddress" => "122.210.69.1",
			"password" => "h0L7fVwiH32bdbdd6fb877c121ee2e304e16d840f",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "Caotrongtai@gmail.com",
			"name"  => "Tai",
			"ipaddress" => "45.126.99.115",
			"password" => "JgryWH06uf345ac1fbd8adb686c427633abc86cbf",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "keeleylockett@hotmail.com",
			"name"  => "Keeley Lockett",
			"ipaddress" => "92.236.126.69",
			"password" => "UBmLkY37o5329696b74a16a6273685437e77afe1b",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "rayhan09ahmed@gmail.com",
			"name"  => "Rayhan Ahmed Bijoy",
			"ipaddress" => "202.134.9.141",
			"password" => "gaBWEWuvXfd534d1cf314413be503329559662968",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "jabella@cblawparis.com",
			"name"  => "Julien Abella",
			"ipaddress" => "90.63.238.175",
			"password" => "4fbw7cqQOe79bc13b7f8567dcde414a786d448352",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			
			"email" => "tbarber@tdwatts.com",
			"name"  => "Tony Barber",
			"password" => "Hy3HmN1XM9da16cde67bb44ff96f0f6bfdaa286ec",
			"ipaddress" => "73.212.132.183",
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "kgotso1366@gmail.com",
			"name"  => "Master kaygee",
			"ipaddress" => "149.56.181.1",
			"password" => "SHBxkf3VJebacb9694c9855d541d3a26b1ecff27d",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "amandamaryanne.m@gmail.com",
			"name"  => "Amanda",
			"ipaddress" => "168.245.239.175",
			"password" => "NhnnUHqLudbe55fc8a6f1dc9fddfe8de2f75c2f54",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "sheenalyn@trademarkers.com",
			"name"  => "SheenalynAlivio",
			"ipaddress" => "203.177.94.36",
			"password" => "MzxSZCZFX1bc791ba9b115ef31a33c25f05156c6e",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "julie50160@yahoo.com",
			"name"  => "Julie Poulos",
			"ipaddress" => "73.209.147.178",
			"password" => "K40RkRJiG16abfaf24a3804b77b35b761e2366b0a",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "jan@gmail.com",
			"name"  => "Jan",
			"ipaddress" => "117.20.116.3",
			"password" => "w1VkklJgq660b4645d37a80e44f6d27ef4256fcbb",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "tevernKamogelo@yahoo.com",
			"name"  => "Kamogelo Tevern Angel Tlhako",
			"ipaddress" => "41.114.109.116",
			"password" => "FRYDcPERS6d3905afb83b8cf4b2a702236ef5bc5a",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "Crystalwiltshire@yahoo.com",
			"name"  => "Crystal R Scott",
			"ipaddress" => "172.58.224.114",
			"password" => "8IdbFSnw759f047a49751693147e554f7cb9b7c47",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "ace@trademarkers.com",
			"name"  => "Ace Villacarillo",
			"ipaddress" => "203.177.94.36",
			"password" => "gdfJtisfIcf846a3ca0d8f1664f2c3c991a3f1af9",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "john.lumongsud@trademarkers.com",
			"name"  => "John Ramah Lumongsud",
			"ipaddress" => "203.177.94.36",
			"password" => "e1LS05WU76fe6cf1291b302eebfafbe7426c09f0e",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "susan@trademarkers.com",
			"name"  => "Jane De Asis",
			"ipaddress" => "203.177.94.36",
			"password" => "sr3uNAwIef9625deaf7e2251e452d2bd8abe2ee84",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "pinkelephantfoodtruck@gmail.com",
			"name"  => "Natasha  Andrews",
			"ipaddress" => "73.186.106.63",
			"password" => "TLb2DrBrA8e8d60385c30ad3d39ad1bf6fe8226eb",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "roman.cywinski@btinternet.com",
			"name"  => "Roman Cywinski",
			"ipaddress" => "82.45.112.35",
			"password" => "1oMGdjpae6d254579669ef723944350fe2a2797de",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "roger@vickys-ie.com",
			"name"  => "Roger Jung",
			"ipaddress" => "175.156.219.139",
			"password" => "p9DQrXUke6fb81499c85bb315f8c5489e173e48d5",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "geoff@skitv.tv",
			"name"  => "geoffrey harrison",
			"ipaddress" => "77.251.36.6",
			"password" => "UQJbNkmuYb873896e2b6caa51e29f3f93f5387268",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "antwoord10@gmail.com",
			"name"  => "antwoord",
			"ipaddress" => "41.13.92.199",
			"password" => "iCkKbFw8Qb078ca0e18661986e8eee7148a8d6be2",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "edasol.us@gmail.com",
			"name"  => "DASOL",
			"ipaddress" => "183.107.117.211",
			"password" => "AvuOgIdu5dcb020e944d60e950c06d8642269c832",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "13125157787@163.com",
			"name"  => "yuewei",
			"ipaddress" => "128.199.249.206",
			"password" => "fn7UTM2OJ47654768b5dc97f34d12e02bb99f0cd0",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "maria.comfort@rocsolutions.org",
			"name"  => "Maria Comfort",
			"ipaddress" => "188.14.193.135",
			"password" => "wtU81Xutn4bad7bcad16ab262f127972f032ccc10",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "sales@chelseycomfort.com",
			"name"  => "Maria Comfort",
			"ipaddress" => "188.14.193.135",
			"password" => "7wiMGGfLU4f57b57c8427b050acdf9e1182b3f64b",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "sean.burke80@yahoo.com",
			"name"  => "Sean Burke",
			"ipaddress" => "87.69.73.241",
			"password" => "zbLZr5kSd0ec7977e94c1bb94ca2c3a8048de89c9",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "mbedzihulisani95@gmail.com",
			"name"  => "Boys hard",
			"ipaddress" => "102.250.2.41",
			"password" => "N2f3lkaXUc729ae135922b303b307e0b34bd8d9df",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "1556287402@qq.com",
			"name"  => "ke zhi cheng",
			"ipaddress" => "52.179.103.139",
			"password" => "uRsp20PJYa37d64d1f4fe0d7d34a213e2ed86a351",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "stanleymo@hotmail.co.uk",
			"name"  => "Stanley Mo",
			"ipaddress" => "88.130.59.11",
			"password" => "lCubvU94fab8ea245b0adce334ee3d4d67c1d37dd",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "trankhanhloc@gmail.com",
			"name"  => "Loc Tran Khanh",
			"ipaddress" => "113.185.15.49",
			"password" => "jl9KmTvRx7810bb059b599f08350cbbf259bfdc8f",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "nomathembahlongwane03@gmail.com",
			"name"  => "Bunny Pledge",
			"ipaddress" => "41.13.66.92",
			"password" => "UZ6hk3MZn716f96498229454efce15c8dd2ecaa14",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			
			"email" => "Ediwardiansyah@mail.com",
			"name"  => "Edi eror",
			"password" => "oLqgKOAeQ268ec720d36b33a3d4e8f1b24daa8153",
			"ipaddress" => "114.124.149.215",
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			
			"email" => "serenity.venturesinc@gmail.com",
			"name"  => "Jason",
			"password" => "PNu4OUqfM6f576c3c1ab818f639f8788332b7212f",
			"ipaddress" => "142.179.236.242",
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "kiesha@trademarkers.com",
			"name"  => "Kiesha Bayocboc",
			"ipaddress" => "122.52.119.55",
			"password" => "q1vTMrN5Kca0a2c40fd5dabf4cfa6bee99b1f2985",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "7gproductions80@gmail.com",
			"name"  => "Sean",
			"ipaddress" => "87.69.73.241",
			"password" => "qd2GRwcUncbb75f5361d1e67ac94fb2803328c073",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "xinpu7964062raoli@163.com",
			"name"  => "liukang",
			"ipaddress" => "173.24.116.97",
			"password" => "TZUPjgSXz45653b3aeed4c12192de67009817e244",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			
			"email" => "mokhonazikp@yahoo.com",
			"name"  => "Khomotso",
			"password" => "yqtXERQIw5c3d857c88062bd87563f58c0d0c18f3",
			"ipaddress" => "41.13.88.59",
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "anhnangcuaemzz@gmail.com",
			"name"  => "chisusu",
			"ipaddress" => "171.253.38.141",
			"password" => "SxsgERnvk160e30d030a73f2fa0bbfa0bc59adb99",
			
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "sahily@cristal.hlg.sld.cu",
			"name"  => "Sahily",
			"ipaddress" => "181.225.242.246",
			"password" => "XwU2PT5uX2b099b5af2660dac333213c395d4cbff",
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "killy.wang@powervision.me",
			"name"  => "Killy.wang",
			"ipaddress" => "124.65.65.190",
			"password" => "4UOPDaP5Mfd8c6e1094982d277f7d0ee1102d71b9",
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "cocoriang@naver.com",
			"name"  => "Sung yoon Lee",
			"ipaddress" => "119.193.237.32",
			"password" => "TgU9WUiNh39bb169088a29ebc8e4053744baff24c",
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "Rickeymitchell300@yahoo.com",
			"name"  => "Rickey Mitchell",
			"ipaddress" => "99.203.26.33",
			"password" => "zZQCi0kxR150092fb1926adc9637dea74fbf44a77",
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "hthantsha@gmail.com",
			"name"  => "Blessing",
			"password" => "aqPBQGU2l9be945275a8d5ebadee77f97bbaee47b",
			"ipaddress" => "102.250.4.71",
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "ativarsoft@gmail.com",
			"name"  => "Joaquin Alcocer",
			"ipaddress" => "187.155.84.223",
			"password" => "sWHlpwd0J6aede38d9b206cb0b0d53626fc327a24",
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "shuvosa421@gmail.com",
			"name"  => "sashuvo",
			"ipaddress" => "202.134.13.142",
			"password" => "60tWakF0Ef53e81b164db5a8c56b932215f30f93b",
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "mdunanalisakhanya@gmail.com",
			"name"  => "Lisakhanya Mdunana",
			"ipaddress" => "154.69.80.100",
			"password" => "HXuV50uuX5486830c9f21cc762a9fbca1e0c1c904",
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "nana.wati09@gmail.com",
			"name"  => "bastian nugroho",
			"ipaddress" => "114.124.199.74",
			"password" => "4vBTja1UDb48c71e749459a71a4fb3d9ca3fa834e",
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "jerome.tenchavez@trademarkers.com",
			"name"  => "Jerome Tenchavez",
			"ipaddress" => "203.177.94.36",
			"password" => "VkmuIcZwk88bd0c049b02664cde7d4d2568180c88",
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "david.wisecracks@gmail.com",
			"name"  => "David Pieterse",
			"ipaddress" => "72.38.13.52",
			"password" => "rgxeNBAb1314aaded9a09bb3ae21932a474aaac71",
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			
			"email" => "ryaniswara30@gmail.com",
			"name"  => "Ryan Iswara",
			"password" => "BuIBtNTzyf5bacf88ddcbf0bab1023446899b5a67",
			"ipaddress" => "202.67.35.12",
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "irjnaeem@gmail.com",
			"name"  => "naeem",
			"ipaddress" => "123.108.246.223",
			"password" => "vRE1HmXxJ00897caa92117661014ca3681265e90e",
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "lauren@trademarkers.com",
			"name"  => "lauren",
			"ipaddress" => "213.125.232.2",
			"password" => "9DUETTLBN81c9fe5b76b5a63ef275472ef4b9f7cd",
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);

		DB::table('users')->insert([
			"email" => "fkriting75@gmail.com",
			"name"  => "Kriting gaming",
			"ipaddress" => "114.142.168.35",
			"password" => "WFAvxUXtW738e4ae382099f9e76360c80cdc198b5",
			'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
			'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
		]);
    }
}
