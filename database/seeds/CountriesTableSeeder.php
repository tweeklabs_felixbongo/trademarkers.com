<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->insert([
            'continent_id' => 2,
            'name' => 'Bangladesh',
            'abbr' => 'BD',
            'agency_name' => 'Department of Patents Designs and Trademarks Ministry of Industries',
            'agency_short' => 'DPDT',
            'opposition_period' => '2 months',
            'registration_period' => '15 months',
            'madrid_protocol' => 0,
            'is_feature' => 0,
            'avatar' => 'countries/bd.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 2,
            'name' => 'Bermuda',
            'abbr' => 'BM',
            'agency_name' => 'Intellectual Property Office of Bermuda',
            'agency_short' => 'IPO',
            'opposition_period' => '2 months',
            'registration_period' => '12 to 18 months',
            'madrid_protocol' => 0,
            'is_feature' => 0,
            'avatar' => 'countries/bm.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 2,
            'name' => 'Bhutan',
            'abbr' => 'BT',
            'agency_name' => 'Industrial Property Division  Ministry of Economic Affairs',
            'agency_short' => 'IPD',
            'opposition_period' => '3 months',
            'registration_period' => 'within 9 to 12 months',
            'madrid_protocol' => 1,
            'is_feature' => 0,
            'avatar' => 'countries/bt.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 2,
            'name' => 'Brunei Darussalam',
            'abbr' => 'BN',
            'agency_name' => ' Brunei Darussalam Intellectual Property Office Ministry of Energy, Manpower and Industry',
            'agency_short' => ' BruIPO',
            'opposition_period' => '3 months',
            'registration_period' => 'within 1-2 years',
            'madrid_protocol' => 0,
            'is_feature' => 0,
            'avatar' => 'countries/bn.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 2,
            'name' => 'Cambodia',
             'abbr' => 'KH',
            'agency_name' => 'Department of Industrial Property, Ministry of Industry and Handicraft',
            'agency_short'=> 'DIP',
            'opposition_period' => '3 months',
            "registration_period" => "9 months to 1 year",
            'madrid_protocol'=> 0,
                    'is_feature' => 0,
            'avatar' => 'countries/kh.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 2,
            'name' => 'China',
            'abbr' => 'CN',
            'agency_name' => 'National Intellectual Property Administration, PRC',
            'agency_short' => ' NCAC',
            'opposition_period' => '3 months',
            'registration_period' => '15 months',
            'madrid_protocol' => 1,
            'is_feature' => 1,
            'avatar' => 'countries/cn.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 2,
            'name' => 'Hong Kong',
            'abbr' => 'HK',
            'agency_name' => 'Intellectual Property Department',
            'agency_short' => 'IPD',
            'opposition_period' => '3 months',
            'registration_period' => '9 to 12 months',
            'madrid_protocol' => 0,
            'is_feature' => 1,
            'avatar' => 'countries/hk.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 2,
            'name' => 'India',
            'abbr' => 'IN',
            'agency_name' => 'Controller General of Patents Designs & Trade Marks',
            'agency_short' => 'Controller General of Patents Designs & Trade Marks',
            'opposition_period' => '4 months',
            'registration_period' => '12 to 14 months',
            'madrid_protocol' => 0,
            'is_feature' => 0,
            'avatar' => 'countries/in.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 2,
            'name' => 'Indonesia',
            'abbr' => 'IN',
            'agency_name' => 'Directorate General of Intellectual Property Rights Ministry of Law and Human Rights',
            'agency_short' => 'DGIP',
            'opposition_period' => '3 months',
            'registration_period' => '1 year and 6 months',
            'madrid_protocol' => 0,
            'is_feature' => 0,
            'avatar' => 'countries/id.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 2,
            'name' => 'Iran',
            'abbr' => 'IR',
            'agency_name' => 'Industrial Property Office',
            'agency_short' => 'Industrial Property Office',
            'opposition_period' => '1 month',
            'registration_period' => '8 months',
            'madrid_protocol' => 1,
            'is_feature' => 0,
            'avatar' => 'countries/ir.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 2,
            'name' => 'Japan',
            'abbr' => 'JP',
            'agency_name' => 'International Affairs Division General Affairs Department Japan Patent Office',
            'agency_short' => 'JPO',
            'opposition_period' => '2 months',
            'registration_period' => '7-8 months',
            'madrid_protocol' => 1,
            'is_feature' => 0,
            'avatar' => 'countries/jp.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 2,
            'name' => 'South Korea',
            'abbr' => 'KR',
            'agency_name' => 'Korean Intellectual Property Office',
            'agency_short' => 'KIPO',
            'opposition_period' => '2 months',
            'registration_period' => '9-12 months',
            'madrid_protocol' => 1,
            'is_feature' => 0,
            'avatar' => 'countries/kr.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 2,
            'name' => 'Kyrgyzstan',
            'abbr' => 'KG',
            'agency_name' => 'Kyrgyzpatent',
            'agency_short' => 'Kyrgyzpatent',
            'opposition_period' => '?',
            'registration_period' => '6-14 months',
            'madrid_protocol' => 1,
            'is_feature' => 0,
            'avatar' => 'countries/kg.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 2,
            'name' => 'Macau',
            'abbr' => 'MO',
            'agency_name' => 'Macau Economic Services - Intellectual Property Department',
            'agency_short' => 'Macau Economic Services - Intellectual Property Department',
            'opposition_period' => '2 months',
            'registration_period' => '6-8 months',
            'madrid_protocol' => 1,
            'is_feature' => 0,
            'avatar' => 'countries/mo.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 2,
            'name' => 'Malaysia',
            'abbr' => 'MY',
            'agency_name' => 'Intellectual Property Corporation of Malaysia',
            'agency_short' => 'MyIPO',
            'opposition_period' => '2 months',
            'registration_period' => '9 to 12 months',
            'madrid_protocol' => 0,
            'is_feature' => 0,
            'avatar' => 'countries/my.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 2,
            'name' => 'Maldives',
            'abbr' => 'MV',
            'agency_name' => 'Intellectual Property Unit Ministry of Economic Development',
            'agency_short' => 'Intellectual Property Unit Ministry of Economic Development',
            'opposition_period' => '?',
            'registration_period' => '3 to 4 weeks',
            'madrid_protocol' => 0,
            'is_feature' => 0,
            'avatar' => 'countries/mv.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 2,
            'name' => 'Mongolia',
            'abbr' => 'MN',
            'agency_name' => 'Intellectual Property Office Implementing Agency of the Government of Mongolia',
            'agency_short' => 'IPOM',
            'opposition_period' => '1 year',
            'registration_period' => '6-9 months',
            'madrid_protocol' => 1,
            'is_feature' => 0,
            'avatar' => 'countries/mn.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 2,
            'name' => 'Myanmar',
            'abbr' => 'MM',
            'agency_name' => 'Department of Technical and Vocational Education Ministry of Science and Technology',
            'agency_short' => 'Department of Technical and Vocational Education Ministry of Science and Technology',
            'opposition_period' => '?',
            'registration_period' => '3 months',
            'madrid_protocol' => 1,
            'is_feature' => 0,
            'avatar' => 'countries/mm.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 2,
            'name' => 'Nepal',
            'abbr' => 'NP',
            'agency_name' => 'Nepal Copyright Registrar\'s Office Ministry of Culture, Tourism and Civil Aviation',
            'agency_short' => 'Department of Technical and Vocational Education Ministry of Science and Technology',
            'opposition_period' => '?',
            'registration_period' => '3 months',
            'madrid_protocol' => 1,
            'is_feature' => 0,
            'avatar' => 'countries/mm.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 2,
            'name' => 'Pakistan',
            'abbr' => 'PK',
            'agency_name' => 'Intellectual Property Organization of Pakistan (IPO-Pakistan)',
            'agency_short' => 'IPO-Pakistan',
            'opposition_period' => '2 months',
            'registration_period' => '9 to 12 months',
            'madrid_protocol' => 1,
            'is_feature' => 0,
            'avatar' => 'countries/pk.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 2,
            'name' => 'Philippines',
            'abbr' => 'PH',
            'agency_name' => 'Intellectual Property Office of the Philippines',
            'agency_short' => 'IPOPHL',
            'opposition_period' => '30 days',
            'registration_period' => '3 to 6 months',
            'madrid_protocol' => 0,
            'is_feature' => 1,
            'avatar' => 'countries/ph.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 2,
            'name' => 'Singapore',
            'abbr' => 'SG',
            'agency_name' => 'Intellectual Property Office of Singapore',
            'agency_short' => 'IPOS',
            'opposition_period' => '2 to 4 months',
            'registration_period' => '6 to 12 months',
            'madrid_protocol' => 1,
            'is_feature' => 1,
            'avatar' => 'countries/sg.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 2,
            'name' => 'Sri Lanka',
            'abbr' => 'LK',
            'agency_name' => 'National Intellectual Property Office of Sri Lanka',
            'agency_short' => 'National Intellectual Property Office of Sri Lanka',
            'opposition_period' => '3 months',
            'registration_period' => '8 years',
            'madrid_protocol' => 0,
            'is_feature' => 0,
            'avatar' => 'countries/lk.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 2,
            'name' => 'Taiwan',
            'abbr' => 'TW',
            'agency_name' => 'Taiwan Intellectual Property Office',
            'agency_short' => 'Taiwan Intellectual Property Office',
            'opposition_period' => '3 months',
            'registration_period' => '10 to 12 months',
            'madrid_protocol' => 0,
            'is_feature' => 1,
            'avatar' => 'countries/tw.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 2,
            'name' => 'Tajikistan',
            'abbr' => 'TJ',
            'agency_name' => 'Agency of Copyright and Related Rights, Ministry of Culture',
            'agency_short' => 'Agency of Copyright and Related Rights, Ministry of Culture',
            'opposition_period' => 'no opposition period',
            'registration_period' => '16 to 25 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/tj.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 2,
            'name' => 'Thailand',
            'abbr' => 'TH',
            'agency_name' => 'Agency of Copyright and Related Rights',
            'agency_short' => 'Agency of Copyright and Related Rights',
            'opposition_period' => '2 months',
            'registration_period' => '12 to 18 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/th.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 2,
            'name' => 'East Timor',
            'abbr' => 'TL',
            'agency_name' => 'Intellectual Property Office of East Timor',
            'agency_short' => 'IPO',
            'opposition_period' => '1 month',
            'registration_period' => '8 to 12 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/tl.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 2,
            'name' => 'Turkmenistan',
            'abbr' => 'TM',
            'agency_name' => 'State Service on Intellectual Property of Ministry of Finance and Economy of Turkmenistan' ,
            'agency_short' => 'State Service on Intellectual Property of Ministry of Finance and Economy of Turkmenistan',
            'opposition_period' => '?',
            'registration_period' => '26 to 29 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/tm.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 2,
            'name' => 'Vietnam',
            'abbr' => 'VN',
            'agency_name' => 'Intellectual Property Office of Viet Nam',
            'agency_short' => 'Intellectual Property Office of Viet Nam',
            'opposition_period' => '12 months',
            'registration_period' => '12 to 15 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/vn.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 1,
            'name' => 'Afghanistan',
            'abbr' => 'AF',
            'agency_name' => 'Afghanistan Central Business Registry and Intellectual Property ',
            'agency_short' => 'Afghanistan Central Business Registry and Intellectual Property ',
            'opposition_period' => '1 month',
            'registration_period' => '5 to 6 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/af.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 1,
            'name' => 'Bahrain',
            'abbr' => 'BH',
            'agency_name' => 'Industrial Property Directorate',
            'agency_short' => 'Industrial Property Directorate',
            'opposition_period' => '2 months',
            'registration_period' => '5 to 7 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/bh.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 1,
            'name' => 'Iraq',
            'abbr' => 'IQ',
            'agency_name' => 'Industrial Property Department ',
            'agency_short' => 'Industrial Property Department ',
            'opposition_period' => '3 months',
            'registration_period' => '1.5 to 2 years',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/iq.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 1,
            'name' => 'Israel',
            'abbr' => 'IL',
            'agency_name' => 'Israel Intellectual Property Office',
            'agency_short' => 'Israel Intellectual Property Office',
            'opposition_period' => '3 months',
            'registration_period' => '17 to 21 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/il.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 1,
            'name' => 'Jordan',
            'abbr' => 'JO',
            'agency_name' => 'Industrial Property Protection Directorate',
            'agency_short' => 'Industrial Property Protection Directorate',
            'opposition_period' => '3 months',
            'registration_period' => '9 to 11 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/jo.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 1,
            'name' => 'Kuwait',
            'abbr' => 'KW',
            'agency_name' => 'Ministry of Trade and Industry Trademarks and Patents Department',
            'agency_short' => 'Ministry of Trade and Industry Trademarks and Patents Department',
            'opposition_period' => '2 months',
            'registration_period' => '8 to 10 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/kw.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 1,
            'name' => 'Lebanon',
            'abbr' => 'LB',
            'agency_name' => 'Office of Intellectual Property of Lebanon',
            'agency_short' => 'Office of Intellectual Property of Lebanon',
            'opposition_period' => 'no opposition period',
            'registration_period' => '4 to 6 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/lb.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 1,
            'name' => 'Oman',
            'abbr' => 'OM',
            'agency_name' => 'Intellectual Property Department Ministry of Commerce and Industry',
            'agency_short' => 'Intellectual Property Department Ministry of Commerce and Industry',
            'opposition_period' => '2 months',
            'registration_period' => '10 to 12  months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/om.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 1,
            'name' => 'Palestine',
            'abbr' => 'PS',
            'agency_name' => 'Palestine Intellectual Property Office',
            'agency_short' => 'IPO',
            'opposition_period' => '?',
            'registration_period' => '?',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/ps.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 1,
            'name' => 'Qatar',
            'abbr' => 'QA',
            'agency_name' => 'Trademarks, Industrial Designs, Geographical Indication, Commercial Registration and Licensing',
            'agency_short' => 'Intellectual Property Department',
            'opposition_period' => '4 months',
            'registration_period' => '8 to 12 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/qa.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 1,
            'name' => 'Saudi Arabia',
            'abbr' => 'SA',
            'agency_name' => 'General Administration of Trademarks Ministry of Commerce and Investment',
            'agency_short' => 'General Administration of Trademarks Ministry of Commerce and Investment',
            'opposition_period' => '2 months',
            'registration_period' => '3 to 5 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/sa.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 1,
            'name' => 'Turkey',
            'abbr' => 'TR',
            'agency_name' => 'Turkish Patent and Trademark Office',
            'agency_short' => 'Turkish Patent and Trademark Office',
            'opposition_period' => '2 months',
            'registration_period' =>'4 to 10 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/tr.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 1,
            'name' => 'United Arab Emirates',
            'abbr' => 'AE',
            'agency_name' => 'Trademark Department of UAE',
            'agency_short' => 'Trademark Department of UAE',
            'opposition_period' => '1 month',
            'registration_period' => '10 to 12 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/ae.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 1,
            'name' => 'Yemen',
            'abbr' => 'YE',
            'agency_name' => 'Ministry of Industry and Trade, General Department for Intellectual Property Protection',
            'agency_short' => 'Ministry of Industry and Trade, General Department for Intellectual Property Protection',
            'opposition_period' => '3 months',
            'registration_period' => '7 to 9 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/ye.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 1,
            'name' => 'Kurdistan Region',
            'abbr' => 'krd',
            'agency_name' => 'Intellectual Property Office',
            'agency_short' => 'IPO',
            'opposition_period' => '1 month',
            'registration_period' => '8 to 12 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/krd.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Albania',
            'abbr' => 'AL',
            'agency_name' => 'General Directorate of Industrial Property',
            'agency_short' => 'GDIP ',
            'opposition_period' => '3 months',
            'registration_period' => '6 to 9 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/al.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Andorra',
            'abbr' => 'AD',
            'agency_name' => 'Trademarks Office of the Principality of Andorra',
            'agency_short' => 'Trademarks Office of the Principality of Andorra',
            'opposition_period' => '?',
            'registration_period' => '3 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/ad.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Armenia',
            'abbr' => 'AM',
            'agency_name' => 'Intellectual Property Agency of the Republic of Armenia, Ministry of Economic           Development and Investments',
            'agency_short' => 'Intellectual Property Agency of the Republic of Armenia, Ministry of Economic           Development and Investments',
            'opposition_period' => '2 months',
            'registration_period' => '10 to 12 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/am.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Austria',
            'abbr' => 'AT',
            'agency_name' => 'Intellectual Property Agency of the Republic of Armenia, Ministry of Economic           Development and Investments',
            'agency_short' => 'OPA',
            'opposition_period' => '2 to 3 months',
            'registration_period' => '3 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/at.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Azerbaijan',
            'abbr' => 'AZ',
            'agency_name' => 'Intellectual Property Agency of the Republic of Azerbaijan',
            'agency_short' => 'IPOA',
            'opposition_period' => '2 months',
            'registration_period' => '26 to 29 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/az.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Belarus',
            'abbr' => 'BY',
            'agency_name' => 'National Center of Intellectual Property',
            'agency_short' => 'NCIP',
            'opposition_period' => '?',
            'registration_period' => '20 to 24 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/by.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Belgium',
            'abbr' => 'BE',
            'agency_name' => '',
            'agency_short' => '',
            'opposition_period' => '?',
            'registration_period' =>'',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/be.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Benelux',
            'abbr' => 'BX',
            'agency_name' => 'Benelux Office for Intellectual Property',
            'agency_short' => 'BOIP',
            'opposition_period' => '2 months',
            'registration_period' => '3 to 4 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'is_treaty' => 1, 
            'avatar' => 'countries/bx.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Bosnia and Herzegovina',
            'abbr' => 'BA',
            'agency_name' => 'Institute for Intellectual Property of Bosnia and Herzegovina',
            'agency_short' => 'IIP-BIH',
            'opposition_period' => '3 months',
            'registration_period' => '1 to 2 years',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/ba.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Bulgaria',
            'abbr' => 'BG',
            'agency_name' => 'Patent Office of the Republic of Bulgaria',
            'agency_short' => 'BPO',
            'opposition_period' => '3 months',
            'registration_period' => '6 to 9 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/bg.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Croatia',
            'abbr' => 'HR',
            'agency_name' => 'State Intellectual Property Office of the Republic of Croatia',
            'agency_short' => 'DZIV',
            'opposition_period' => '3 months',
            'registration_period' => '9 to 12 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/hr.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Cyprus',
            'abbr' => 'CY',
            'agency_name' => 'Department of Registrar of Companies and Official Receiver',
            'agency_short' => 'DRCOR',
            'opposition_period' => '2 months',
            'registration_period' => '6 to 9 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/cy.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Czech Republic',
            'abbr' => 'CZ',
            'agency_name' => 'Intellectual Property Office',
            'agency_short' => 'IPOCZ',
            'opposition_period' => '2 months',
            'registration_period' => '6 to 9 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/cz.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Denmark',
            'abbr' => 'DK',
            'agency_name' => 'Danish Patent and Trademark Office',
            'agency_short' => 'DKPTO',
            'opposition_period' => '2 months',
            'registration_period' => '3 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/dk.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Estonia',
            'abbr' => 'EE',
            'agency_name' => 'The Estonian Patent Office',
            'agency_short' => 'EPA',
            'opposition_period' => '2 months',
            'registration_period' => '11 to 13 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/ee.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'European Union',
            'abbr' => 'EM',
            'agency_name' => 'European Union Intellectual Property Office',
            'agency_short' => 'EUIPO',
            'opposition_period' => '3 months',
            'registration_period' => '7 months',
            'madrid_protocol' => 1,
            'is_feature' => 1, 
            'is_treaty' => 1, 
            'avatar' => 'countries/em.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Finland',
            'abbr' => 'FI',
            'agency_name' => 'Finnish Patent and Registration Office',
            'agency_short' => 'PRH',
            'opposition_period' => '2 months',
            'registration_period' => '4 to 5 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/fi.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'France',
            'abbr' => 'FR',
            'agency_name' => 'National Institute of Industrial Property',
            'agency_short' => 'I.N.P.I',
            'opposition_period' => '2 months',
            'registration_period' => '4 to 6 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/fr.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Georgia',
            'abbr' => 'GE',
            'agency_name' => 'National Intellectual Property Center (Sakpatenti)',
            'agency_short' => 'NIPCG',
            'opposition_period' => '3 months',
            'registration_period' => '14 to 17 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/ge.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Germany',
            'abbr' => 'DE',
            'agency_name' => 'German Patent and Trade Mark Office',
            'agency_short' => 'DPMA',
            'opposition_period' => '3 months',
            'registration_period' => '6-9 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/de.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Gibraltar',
            'abbr' => 'GI',
            'agency_name' => 'Intellectual Property Office',
            'agency_short' => 'IPO',
            'opposition_period' => '?',
            'registration_period' => '?',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/gi.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Greece',
            'abbr' => 'GR',
            'agency_name' => 'Industrial Property Organization',
            'agency_short' => 'GGE',
            'opposition_period' => '3 months',
            'registration_period' => '6 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/gr.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Guernsey',
            'abbr' => 'GG',
            'agency_name' => 'Intellectual Property Office of Guernsey',
            'agency_short' => 'IPO',
            'opposition_period' => '?',
            'registration_period' => '?',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/gg.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Hungary',
            'abbr' => 'HU',
            'agency_name' => 'Hungarian Intellectual Property Office',
            'agency_short' => 'HIPO',
            'opposition_period' => '3 months',
            'registration_period' => '7 to 9 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/hu.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Iceland',
            'abbr' => 'IS',
            'agency_name' => 'Icelandic Patent Office',
            'agency_short' => 'ELS-IPO',
            'opposition_period' => '2 months',
            'registration_period' => '2 to 4months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/is.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Ireland',
            'abbr' => 'IE',
            'agency_name' => 'Patents Office - Department of Jobs, Enterprise & Innovation',
            'agency_short' => 'IEIPO',
            'opposition_period' => '3 months',
            'registration_period' => '9 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/ie.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Italy',
            'abbr' => 'IT',
            'agency_name' => 'Italian Patent and Trademark Office',
            'agency_short' => 'UIBM',
            'opposition_period' => '3 months',
            'registration_period' => '12 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/it.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Jersey',
            'abbr' => 'JE',
            'agency_name' => 'Intellectual Property Office',
            'agency_short' => 'IPO',
            'opposition_period' => '1 month',
            'registration_period' => '8 to 12 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/je.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Kosovo',
            'abbr' => 'KS',
            'agency_name' => 'Intellectual Property Office',
            'agency_short' => 'IPO',
            'opposition_period' => '3 months',
            'registration_period' => '12 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/ks.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Latvia',
            'abbr' => 'LV',
            'agency_name' => 'Patent Office of the Republic of Latvia',
            'agency_short' => 'LRPV',
            'opposition_period' => '3 months',
            'registration_period' => '6 to 8 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/lv.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Liechtenstein',
            'abbr' => 'LI',
            'agency_name' => 'Amt für Volkswirtschaft',
            'agency_short' => 'AVW',
            'opposition_period' => 'no opposition period',
            'registration_period' => '3 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/li.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Lithuania',
            'abbr' => 'LT',
            'agency_name' => 'State Patent Bureau of the Republic of Lithuania',
            'agency_short' => 'VPB',
            'opposition_period' => '3 months',
            'registration_period' => '5 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/lt.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Luxembourg',
            'abbr' => 'LU',
            'agency_name' => 'Intellectual Property Office',
            'agency_short' => 'IPO',
            'opposition_period' => '1 month',
            'registration_period' => '8 to 12 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/lu.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Macedonia',
            'abbr' => 'MK',
            'agency_name' => 'State Office of Industrial Property',
            'agency_short' => 'SOIP',
            'opposition_period' => '3 months',
            'registration_period' => '6 to 12 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/mk.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Malta',
            'abbr' => 'MT',
            'agency_name' => 'Industrial Property Registrations Directorate',
            'agency_short' => 'CD-IPRD',
            'opposition_period' => '9 months',
            'registration_period' => '9 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/mt.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Moldova',
            'abbr' => 'MD',
            'agency_name' => 'State Agency on Intellectual Property',
            'agency_short' => 'AGEPI',
            'opposition_period' => '3 months',
            'registration_period' => '12 to 14 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/md.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Monaco',
            'abbr' => 'MC',
            'agency_name' => 'Intellectual Property Division',
            'agency_short' => 'MIPRO',
            'opposition_period' => 'no opposition period',
            'registration_period' => '3 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/mc.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Netherlands',
            'abbr' => 'NL',
            'agency_name' => 'Intellectual Property Office',
            'agency_short' => 'IPO',
            'opposition_period' => '1 month',
            'registration_period' => '8 to 12 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/nl.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Norway',
            'abbr' => 'NO',
            'agency_name' => 'Norwegian Industrial Property Office',
            'agency_short' => 'NIPO',
            'opposition_period' => '3 months',
            'registration_period' => '3 to 6 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/no.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Poland',
            'abbr' => 'PL',
            'agency_name' => 'Patent Office of the Republic of Poland',
            'agency_short' => 'PPO',
            'opposition_period' => '3 months',
            'registration_period' => '6 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/pl.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Portugal',
            'abbr' => 'PT',
            'agency_name' => 'Portuguese Institute of Industrial Property',
            'agency_short' => 'INPIPT',
            'opposition_period' => '2 months',
            'registration_period' => '4 to 6 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/pt.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Romania',
            'abbr' => 'RO',
            'agency_name' => 'State Office for Inventions and Trademarks',
            'agency_short' => 'OSIM',
            'opposition_period' => '2 months',
            'registration_period' => '6 to 12 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/ro.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Russian Federation',
            'abbr' => 'RU',
            'agency_name' => 'Federal Service for Intellectual Property',
            'agency_short' => 'ROSPATENT',
            'opposition_period' => '5 years',
            'registration_period' => '12 to 14 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/ru.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'San Marino',
            'abbr' => 'SM',
            'agency_name' => 'Department of External Affairs',
            'agency_short' => 'USBM',
            'opposition_period' => '3 months',
            'registration_period' => '12 to 18 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/sm.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Serbia',
            'abbr' => 'RS',
            'agency_name' => 'Intellectual Property Office of the Republic of Serbia',
            'agency_short' => 'IPORS',
            'opposition_period' => '3 months',
            'registration_period' => '12 to 18 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/rs.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Montenegro',
            'abbr' => 'ME',
            'agency_name' => 'Intellectual Property Office of Montenegro',
            'agency_short' => 'IPOM',
            'opposition_period' => '3 months',
            'registration_period' => '12 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/me.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Sint Maarten',
            'abbr' => 'SX',
            'agency_name' => 'Bureau for Intellectual Property Sint Maarten',
            'agency_short' => 'IPO',
            'opposition_period' => '1 month',
            'registration_period' => '8 to 12 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/sx.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Slovakia',
            'abbr' => 'SK',
            'agency_name' => 'Industrial Property Office of the Slovak Republic',
            'agency_short' => 'SKIPO',
            'opposition_period' => '3 months',
            'registration_period' => '4 to 5 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/sk.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Slovenia',
            'abbr' => 'SI',
            'agency_name' => 'Slovenian Intellectual Property Office',
            'agency_short' => 'SIPO',
            'opposition_period' => '3 months',
            'registration_period' => '5 to 12 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/si.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Spain',
            'abbr' => 'ES',
            'agency_name' => 'Spanish Patent and Trademark Office O.A.',
            'agency_short' => 'OEPM',
            'opposition_period' => '2 months',
            'registration_period' => '6 to 12 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/es.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Sweden',
            'abbr' => 'SE',
            'agency_name' => 'Swedish Patent and Registration Office (SPRO)',
            'agency_short' => 'PRV',
            'opposition_period' => '3 months',
            'registration_period' => '3 to 6 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/se.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Switzerland',
            'abbr' => 'CH',
            'agency_name' => 'Swiss Federal Institute of Intellectual Property',
            'agency_short' => 'IGE-IPI',
            'opposition_period' => '3 months',
            'registration_period' => '3 to 6 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/ch.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'Ukraine',
            'abbr' => 'UA',
            'agency_name' => 'Ministry of Economic Development and Trade of Ukraine',
            'agency_short' => 'UIPV',
            'opposition_period' => '12 to 15 months',
            'registration_period' => '12 to 15 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/ua.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 6,
            'name' => 'United Kingdom',
            'abbr' => 'GB',
            'agency_name' => 'United Kingdom Intellectual Property Office',
            'agency_short' => 'UKIPO',
            'opposition_period' => '2 months',
            'registration_period' => '4 to 6 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/gb.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 3,
            'name' => 'Canada',
            'abbr' => 'CA',
            'agency_name' => 'Canadian Intellectual Property Office',
            'agency_short' => 'CIPO',
            'opposition_period' => '2 months',
            'registration_period' => '14 to 20 months',
            'madrid_protocol' => 0,
            'is_feature' => 1, 
            'avatar' => 'countries/ca.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 3,
            'name' => 'Mexico',
            'abbr' => 'MX',
            'agency_name' => 'Mexican Institute of Industrial Property',
            'agency_short' => 'IMPI',
            'opposition_period' => '10 business days',
            'registration_period' => '8 to 10 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/mx.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 3,
            'name' => 'United States',
            'abbr' => 'US',
            'agency_name' => 'United States Patent and Trademark Office',
            'agency_short' => 'USPTO',
            'opposition_period' => ' 30 days',
            'registration_period' => '9 to 16 months',
            'madrid_protocol' => 1,
            'is_feature' => 1, 
            'avatar' => 'countries/us.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 7,
            'name' => 'Argentina',
            'abbr' => 'AR',
            'agency_name' => 'National Institute of Industrial Property',
            'agency_short' => 'INPI',
            'opposition_period' => ' 30 days',
            'registration_period' => '12 to 18 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/ar.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 7,
            'name' => 'Bolivia',
            'abbr' => 'BO',
            'agency_name' => 'National Intellectual Property Service, Ministry of Productive Development and Plural Economy',
            'agency_short' => 'NIPS',
            'opposition_period' => ' 30 days',
            'registration_period' => '12 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/bo.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 7,
            'name' => 'Brazil',
            'abbr' => 'BR',
            'agency_name' => 'National Institute of Industrial Property',
            'agency_short' => 'INPI',
            'opposition_period' => ' 60 days',
            'registration_period' => '24 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/br.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 7,
            'name' => 'Chile',
            'abbr' => 'CL',
            'agency_name' => 'National Institute of Industrial Property',
            'agency_short' => 'INAPI',
            'opposition_period' => ' 30 days',
            'registration_period' => '4 to 12 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/cl.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 7,
            'name' => 'Colombia',
            'abbr' => 'CO',
            'agency_name' => 'Superintendence of Industry and Commerce',
            'agency_short' => 'SIC',
            'opposition_period' => ' 30 days',
            'registration_period' => '6 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/co.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 7,
            'name' => 'Ecuador',
            'abbr' => 'EC',
            'agency_name' => 'Instituto Ecuatoriano de Propiedad Industrial Intelectual',
            'agency_short' => 'IEPI',
            'opposition_period' => ' 30 days',
            'registration_period' => '6 to 8 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/ec.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 7,
            'name' => 'Guyana',
            'abbr' => 'GY',
            'agency_name' => 'Deeds and Commercial Registries',
            'agency_short' => 'IPO',
            'opposition_period' => ' 30 days',
            'registration_period' => '12 to 18 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/gy.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 7,
            'name' => 'Paraguay',
            'abbr' => 'PY',
            'agency_name' => 'National Directorate of Intellectual Property',
            'agency_short' => 'NDIP',
            'opposition_period' => ' 60 days',
            'registration_period' => '8 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/py.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 7,
            'name' => 'Peru',
            'abbr' => 'PE',
            'agency_name' => 'National Institute for the Defense of Competition and Protection of Intellectual Property',
            'agency_short' => 'INDECOPI',
            'opposition_period' => ' 30 days',
            'registration_period' => '5 to 6 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/pe.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 7,
            'name' => 'Suriname',
            'abbr' => 'SR',
            'agency_name' => 'Intellectual Property Office of Suriname',
            'agency_short' => 'IPO',
            'opposition_period' => '6 months',
            'registration_period' => '3 years',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/sr.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 7,
            'name' => 'Uruguay',
            'abbr' => 'UY',
            'agency_name' => 'National Directorate of Industrial Property',
            'agency_short' => 'MIEM-DNPI',
            'opposition_period' => '30 days',
            'registration_period' => '14 to 18 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/uy.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 7,
            'name' => 'Venezuela',
            'abbr' => 'VE',
            'agency_name' => 'Autonomous Service of Intellectual Property',
            'agency_short' => 'SAPI',
            'opposition_period' => '30 days',
            'registration_period' => '12 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/ve.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 8,
            'name' => 'Antigua and Barbuda',
            'abbr' => 'AG',
            'agency_name' => 'Antigua and Barbuda Intellectual Property & Commerce Office',
            'agency_short' => 'ABIPCO',
            'opposition_period' => '3 months',
            'registration_period' => '3 to 4 years',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/ag.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 8,
            'name' => 'Aruba',
            'abbr' => 'AW',
            'agency_name' => 'Bureau of Intellectual Property in Aruba',
            'agency_short' => 'IPO',
            'opposition_period' => '2 months',
            'registration_period' => '6 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/ag.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 8,
            'name' => 'Bahamas',
            'abbr' => 'BS',
            'agency_name' => 'Registrar General\'s Department',
            'agency_short' => 'IPO',
            'opposition_period' => '30 days',
            'registration_period' => '4 years',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/bs.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 8,
            'name' => 'Barbados',
            'abbr' => 'BB',
            'agency_name' => 'Corporate Affairs and Intellectual Property Office',
            'agency_short' => 'CAIPO',
            'opposition_period' => '?',
            'registration_period' => '?',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/bb.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 8,
            'name' => 'Belize',
            'abbr' => 'BZ',
            'agency_name' => 'Belize Intellectual Property Office',
            'agency_short' => 'BELIPO',
            'opposition_period' => '3 months',
            'registration_period' => '8 to 12 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/bz.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 8,
            'name' => 'Cayman Islands',
            'abbr' => 'KY',
            'agency_name' => 'Intellectual Property Office',
            'agency_short' => 'IPO',
            'opposition_period' => '?',
            'registration_period' => '?',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/ky.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 8,
            'name' => 'Costa Rica',
            'abbr' => 'CR',
            'agency_name' => 'Register of Industrial Property',
            'agency_short' => 'RNPCR',
            'opposition_period' => '2 months',
            'registration_period' => '5 to 6 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/cr.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 8,
            'name' => 'Dominica',
            'abbr' => 'DM',
            'agency_name' => 'Companies and Intellectual Property Office',
            'agency_short' => 'RNPCR',
            'opposition_period' => '2 months',
            'registration_period' => '6 to 12 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/dm.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 8,
            'name' => 'Dominican Republic',
            'abbr' => 'DO',
            'agency_name' => 'National Office of Industrial Property',
            'agency_short' => 'NOIP',
            'opposition_period' => '45 days',
            'registration_period' => '30 days',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/do.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 8,
            'name' => 'El Salvador',
            'abbr' => 'SV',
            'agency_name' => 'National Center of Registries',
            'agency_short' => 'CNR',
            'opposition_period' => '2 months',
            'registration_period' => '7 to 9 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/sv.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 8,
            'name' => 'Grenada',
            'abbr' => 'GD',
            'agency_name' => 'Corporate Affairs and Intellectual Property Office',
            'agency_short' => 'CAIPO',
            'opposition_period' => '1 month',
            'registration_period' => '1 year',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/gd.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 8,
            'name' => 'Guatemala',
            'abbr' => 'GT',
            'agency_name' => 'Registry of Intellectual Property',
            'agency_short' => 'IPO',
            'opposition_period' => '2 months',
            'registration_period' => '2 to 3 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/gt.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 8,
            'name' => 'Haiti',
            'abbr' => 'HT',
            'agency_name' => 'Haitian Copyright Office',
            'agency_short' => 'BHDA',
            'opposition_period' => '2 months',
            'registration_period' => '8 to 12 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/ht.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 8,
            'name' => 'Honduras',
            'abbr' => 'HN',
            'agency_name' => 'Directorate General of Intellectual Property',
            'agency_short' => 'DIGEPIH',
            'opposition_period' => '1 month',
            'registration_period' => '3 to 4 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/hn.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 8,
            'name' => 'Jamaica',
            'abbr' => 'JM',
            'agency_name' => 'Jamaica Intellectual Property Office',
            'agency_short' => 'JIPO',
            'opposition_period' => '2 months',
            'registration_period' => '12 to 18 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/jm.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 8,
            'name' => 'Montserrat',
            'abbr' => 'MS',
            'agency_name' => 'Intellectual Property Office',
            'agency_short' => 'IPO',
            'opposition_period' => '?',
            'registration_period' =>'?',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/ms.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 8,
            'name' => 'Nicaragua',
            'abbr' => 'NI',
            'agency_name' => 'Intellectual Property Registry',
            'agency_short' => 'RPI',
            'opposition_period' => '2 months',
            'registration_period' =>'10 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/ni.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 8,
            'name' => 'Panama',
            'abbr' => 'PA',
            'agency_name' => 'Directorate General of the Industrial Property Registry',
            'agency_short' => 'DIGERPI',
            'opposition_period' => '2 months',
            'registration_period' =>'3 to 6 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/pa.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 8,
            'name' => 'Puerto Rico',
            'abbr' => 'PR',
            'agency_name' => 'The Department of Economic development and Commerce',
            'agency_short' => 'IPO',
            'opposition_period' => '1 month',
            'registration_period' =>'2 years',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/pr.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 8,
            'name' => 'Saint Kitts and Nevis',
            'abbr' => 'KN',
            'agency_name' => 'Intellectual Property Office',
            'agency_short' => 'IPO',
            'opposition_period' => '?',
            'registration_period' =>'?',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/kn.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 8,
            'name' => 'Saint Lucia',
            'abbr' => 'LC',
            'agency_name' => 'Intellectual Property Office',
            'agency_short' => 'IPO',
            'opposition_period' => '?',
            'registration_period' =>'?',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/lc.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 8,
            'name' => 'Saint Vincent and the Grenadines',
            'abbr' => 'VC',
            'agency_name' => 'Commerce and Intellectual Property Office',
            'agency_short' => 'IPO',
            'opposition_period' => '3 months',
            'registration_period' =>'12 to 18 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/vc.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 8,
            'name' => 'Trinidad and Tobago',
            'abbr' => 'TT',
            'agency_name' => 'Intellectual Property Office',
            'agency_short' => 'IPO',
            'opposition_period' => '3 months',
            'registration_period' => '8 months to 2 years',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/tt.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 8,
            'name' => 'Turks and Caicos Islands',
            'abbr' => 'TC',
            'agency_name' => 'Intellectual Property Office',
            'agency_short' => 'IPO',
            'opposition_period' => '1 month',
            'registration_period' =>' 4 to 6 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/tt.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 8,
            'name' => 'British Virgin Islands',
            'abbr' => 'VG',
            'agency_name' => 'Intellectual Property Office',
            'agency_short' => 'IPO',
            'opposition_period' => '3 months',
            'registration_period' => '8 to 12 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/vg.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 4,
            'name' => 'American Samoa',
            'abbr' => 'AS',
            'agency_name' => 'Intellectual Property Office',
            'agency_short' => 'IPO',
            'opposition_period' => '?',
            'registration_period' => '?',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/as.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')

        ]);

        DB::table('countries')->insert([
            'continent_id' => 4,
            'name' => 'Australia',
            'abbr' => 'AU',
            'agency_name' => 'IP Australia Department of Industry',
            'agency_short' => 'IPO',
            'opposition_period' => '2 months',
            'registration_period' => '7 to 24 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/au.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 4,
            'name' => 'Fiji',
            'abbr' => 'FJ',
            'agency_name' => 'Office of the Attorney-General',
            'agency_short' => 'IPO',
            'opposition_period' => '3 months',
            'registration_period' => '12 to 18 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/fj.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 4,
            'name' => 'New Zealand',
            'abbr' => 'NZ',
            'agency_name' => 'New Zealand Intellectual Property Office',
            'agency_short' => 'IPONZ',
            'opposition_period' => '3 months',
            'registration_period' => '6 to 12 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/nz.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 4,
            'name' => 'Samoa',
            'abbr' => 'WS',
            'agency_name' => 'Registries of Companies and Intellectual Property Division',
            'agency_short' => 'RCIP',
            'opposition_period' => '3 months',
            'registration_period' => '1 to 2 years',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/ws.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 4,
            'name' => 'Solomon Islands',
            'abbr' => 'SB',
            'agency_name' => 'Registrar General\'s Office',
            'agency_short' => 'RGO',
            'opposition_period' => 'no opposition period',
            'registration_period' => '2 to 3 years',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/sb.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 4,
            'name' => 'Tonga',
            'abbr' => 'TO',
            'agency_name' => 'Registry & Intellectual Property Office',
            'agency_short' => 'RIPO',
            'opposition_period' => '3 months',
            'registration_period' => '1 to 2 years',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/to.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Algeria',
            'abbr' => 'DZ',
            'agency_name' => 'Algerian National Institute of Industrial Property',
            'agency_short' => 'INAPI',
            'opposition_period' => 'no opposition period',
            'registration_period' => '2 to 15 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/dz.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Angola',
            'abbr' => 'AO',
            'agency_name' => 'Angolan Institute of Industrial Property',
            'agency_short' => 'AIIP',
            'opposition_period' => '2 months',
            'registration_period' => '2 to 4 years',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/ao.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Anguilla',
            'abbr' => 'AI',
            'agency_name' => 'Intellectual Property Office',
            'agency_short' => 'IPO',
            'opposition_period' => '?',
            'registration_period' => '?',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/ai.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Benin',
            'abbr' => 'BJ',
            'agency_name' => 'Beninese Copyright Office',
            'agency_short' =>  'BUBEDRA',
            'opposition_period' => '?',
            'registration_period' => '?',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/bj.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Botswana',
            'abbr' => 'BW',
            'agency_name' => 'Companies and Intellectual Property Authority',
            'agency_short' =>  'CIPA',
            'opposition_period' => '?',
            'registration_period' => '?',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/bw.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Burkina Faso',
            'abbr' => 'BF',
            'agency_name' => 'General Directorate of Industrial Property',
            'agency_short' =>  'DGPI',
            'opposition_period' => '?',
            'registration_period' => '?',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/bf.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Burundi',
            'abbr' => 'BI',
            'agency_name' => 'General Directorate of Industrial Property',
            'agency_short' =>  'DGPI',
            'opposition_period' => '?',
            'registration_period' => '?',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/bi.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Cameroon',
            'abbr' => 'CM',
            'agency_name' => 'Directorate of Technological Development and Industrial Property',
            'agency_short' =>  '?',
            'opposition_period' => '?',
            'registration_period' => '?',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/cm.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Cape Verde',
            'abbr' => 'CV',
            'agency_name' => 'Industrial Property Institute',
            'agency_short' =>  ' IPICV',
            'opposition_period' => '3 months',
            'registration_period' => '6 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/cv.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Central African Republic',
            'abbr' => 'CF',
            'agency_name' => 'National Industrial Property Service',
            'agency_short' =>  'SNL',
            'opposition_period' => '?',
            'registration_period' => '?',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/cf.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Chad',
            'abbr' => 'TD',
            'agency_name' => 'Intellectual Property Office',
            'agency_short' =>  'IPO',
            'opposition_period' => '?',
            'registration_period' => '?',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/td.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Comoros',
            'abbr' => 'KM',
            'agency_name' => 'Intellectual Property Office',
            'agency_short' =>  'IPO',
            'opposition_period' => '?',
            'registration_period' => '?',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/km.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Republic of the Congo',
            'abbr' => 'CG',
            'agency_name' => 'Congolese Copyright Office',
            'agency_short' =>  'BCDA',
            'opposition_period' => '?',
            'registration_period' => '?',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/cg.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Ivory Coast',
            'abbr' => 'CI',
            'agency_name' => 'Intellectual Property Office',
            'agency_short' =>  'IPO',
            'opposition_period' => '?',
            'registration_period' => '?',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/ci.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Djibouti',
            'abbr' => 'DJ',
            'agency_name' => 'Office of Industrial Property and Commerce',
            'agency_short' =>  'OIPC',
            'opposition_period' => '?',
            'registration_period' => '?',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/dj.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 2,
            'name' => 'Egypt',
            'abbr' => 'EG',
            'agency_name' => 'Trademarks and Industrial Designs Office',
            'agency_short' =>  'TIDO',
            'opposition_period' => '2 months',
            'registration_period' => '18 to 24 months',
            'madrid_protocol' => 1,
            'is_feature' => 0, 
            'avatar' => 'countries/eg.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Equatorial Guinea',
            'abbr' => 'GQ',
            'agency_name' => 'Council of Scientific and Tecnological Research',
            'agency_short' =>  'IPO',
            'opposition_period' => '?',
            'registration_period' => '?',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/gq.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Eritrea',
            'abbr' => 'ER',
            'agency_name' => 'Domestic Trade and Intellectual Property',
            'agency_short' =>  'IPO',
            'opposition_period' => '?',
            'registration_period' => '?',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/er.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Ethiopia',
            'abbr' => 'ET',
            'agency_name' => 'Ethiopian Intellectual Property Office',
            'agency_short' =>  'EIPO',
            'opposition_period' => '2 months',
            'registration_period' => '5 to 7 months',
            'madrid_protocol' => 0,
            'is_feature' => 0, 
            'avatar' => 'countries/et.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Gabon',
            'abbr' => 'GA',
            'agency_name' => 'Intellectual Property Office',
            'agency_short' =>  'IPO',
            'opposition_period' => '?',
            'registration_period' => '?',
            'madrid_protocol' => 0,     
            'is_feature' => 0, 
            'avatar' => 'countries/ga.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Gambia',
            'abbr' => 'GM',
            'agency_name' => 'National Centre for Arts and Culture',
            'agency_short' =>  'IPO',
            'opposition_period' => '?',
            'registration_period' => '?',
            'madrid_protocol' => 0,     
            'is_feature' => 0, 
            'avatar' => 'countries/gm.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Ghana',
            'abbr' => 'GH',
            'agency_name' => 'Registrar General\'s Department Ministry of Justice',
            'agency_short' =>  'IPO',
            'opposition_period' => '2 months',
            'registration_period' => '1 year',
            'madrid_protocol' => 1,     
            'is_feature' => 0, 
            'avatar' => 'countries/gh.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Guinea',
            'abbr' => 'GN',
            'agency_name' => 'National Service of Industrial Property',
            'agency_short' =>  'NSIP',
            'opposition_period' => '?',
            'registration_period' => '?',
            'madrid_protocol' => 0,     
            'is_feature' => 0, 
            'avatar' => 'countries/gn.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Guinea-Bissau',
            'abbr' => 'GW',
            'agency_name' => 'General Directorate of Industrial Property',
            'agency_short' =>  'GDIP',
            'opposition_period' => '?',
            'registration_period' => '?',
            'madrid_protocol' => 0,     
            'is_feature' => 0, 
            'avatar' => 'countries/gw.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Kenya',
            'abbr' => 'KE',
            'agency_name' => 'Kenya Industrial Property Institute',
            'agency_short' =>  'KIPI',
            'opposition_period' => '2 months',
            'registration_period' => '15 months',
            'madrid_protocol' => 1,     
            'is_feature' => 0, 
            'avatar' => 'countries/ke.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Lesotho',
            'abbr' => 'LS',
            'agency_name' => 'Ministry of Law and Constitutional Affairs',
            'agency_short' =>  '?',
            'opposition_period' => '3 months',
            'registration_period' => '24 months',
            'madrid_protocol' => 1,     
            'is_feature' => 0, 
            'avatar' => 'countries/ls.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Liberia',
            'abbr' => 'LR',
            'agency_name' => 'Liberia Intellectual Property Office',
            'agency_short' =>  'LIPO',
            'opposition_period' => '?',
            'registration_period' =>'?',
            'madrid_protocol' => 1,     
            'is_feature' => 0, 
            'avatar' => 'countries/lr.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Libya',
            'abbr' => 'LY',
            'agency_name' => 'National Authority for Scientific Research',
            'agency_short' =>  'NASR',
            'opposition_period' => '3 months',
            'registration_period' =>'3 years',
            'madrid_protocol' => 0,     
            'is_feature' => 0, 
            'avatar' => 'countries/ly.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Madagascar',
            'abbr' => 'MG',
            'agency_name' => 'Malagasy Industrial Property Office',
            'agency_short' =>  'MIPO',
            'opposition_period' => 'no opposition period',
            'registration_period' =>'10 months',
            'madrid_protocol' => 1,     
            'is_feature' => 0, 
            'avatar' => 'countries/mg.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Malawi',
            'abbr' => 'MW',
            'agency_name' => 'Intellectual Property Office',
            'agency_short' =>  'IPO',
            'opposition_period' => '?',
            'registration_period' => '?',
            'madrid_protocol' => 0, 
            'is_feature' => 0, 
            'avatar' => 'countries/mg.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Mali',
            'abbr' => 'ML',
            'agency_name' => 'Malian Centre for the Promotion of Industrial Property',
            'agency_short' =>  'CEMAPI',
            'opposition_period' => '?',
            'registration_period' => '?',
            'madrid_protocol' => 0, 
            'is_feature' => 0, 
            'avatar' => 'countries/ml.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Mauritania',
            'abbr' => 'MR',
            'agency_name' => 'Directorate of Industry',
            'agency_short' =>  '?',
            'opposition_period' => '?',
            'registration_period' => '?',
            'madrid_protocol' => 0, 
            'is_feature' => 0, 
            'avatar' => 'countries/mr.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Mauritius',
            'abbr' => 'MU',
            'agency_name' => 'Industrial Property Office',
            'agency_short' =>  'IPO',
            'opposition_period' => '2 months',
            'registration_period' => '1 year',
            'madrid_protocol' => 0, 
            'is_feature' => 0, 
            'avatar' => 'countries/mu.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Morocco',
            'abbr' => 'MA',
            'agency_name' => 'Moroccan Industrial and Commercial Property Office',
            'agency_short' =>  'MICPO',
            'opposition_period' => '2 months',
            'registration_period' => '4 to 6 months',
            'madrid_protocol' => 1, 
            'is_feature' => 0, 
            'avatar' => 'countries/ma.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Mozambique',
            'abbr' => 'MZ',
            'agency_name' => 'Industrial Property Institute',
            'agency_short' =>  'IPI',
            'opposition_period' => '30 days',
            'registration_period' => '6 months',
            'madrid_protocol' => 1, 
            'is_feature' => 0, 
            'avatar' => 'countries/mz.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Namibia',
            'abbr' => 'NA',
            'agency_name' => 'Business and Intellectual Property Authority',
            'agency_short' =>  'BIPA',
            'opposition_period' => '2 months',
            'registration_period' => '3 years',
            'madrid_protocol' => 1, 
            'is_feature' => 0, 
            'avatar' => 'countries/na.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Niger',
            'abbr' => 'NE',
            'agency_name' => 'Intellectual Property Office',
            'agency_short' =>  'IPO',
            'opposition_period' => '?',
            'registration_period' => '?',
            'madrid_protocol' => 0, 
            'is_feature' => 0, 
            'avatar' => 'countries/ne.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Nigeria',
            'abbr' => 'NG',
            'agency_name' => 'Commercial Law Department of Trademarks, Patents and Designs',
            'agency_short' =>  '?',
            'opposition_period' => '2 months',
            'registration_period' => '2 months',
            'madrid_protocol' => 0, 
            'is_feature' => 0, 
            'avatar' => 'countries/ng.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Papua New Guinea',
            'abbr' => 'PG',
            'agency_name' => 'Intellectual Property Office of Papua New Guinea',
            'agency_short' =>  'IPOPNG',
            'opposition_period' => '?',
            'registration_period' => '?',
            'madrid_protocol' => 0, 
            'is_feature' => 0, 
            'avatar' => 'countries/pg.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Rwanda',
            'abbr' => 'RW',
            'agency_name' => 'Rwanda Development Board',
            'agency_short' =>  'RDB',
            'opposition_period' => '2 months',
            'registration_period' => '3 months',
            'madrid_protocol' => 0, 
            'is_feature' => 0, 
            'avatar' => 'countries/rw.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Sao Tome and Principe',
            'abbr' => 'ST',
            'agency_name' => 'Industrial Property National Service',
            'agency_short' =>  'SENAPI',
            'opposition_period' => '3 months',
            'registration_period' => '3 to 12 months',
            'madrid_protocol' => 0, 
            'is_feature' => 0, 
            'avatar' => 'countries/rw.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Senegal',
            'abbr' => 'SN',
            'agency_name' => 'Senegalese Agency of Industrial Property and Innovation Technologique',
            'agency_short' =>  'ASPIT',
            'opposition_period' => '?',
            'registration_period' => '?',
            'madrid_protocol' => 0, 
            'is_feature' => 0, 
            'avatar' => 'countries/sn.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Seychelles',
            'abbr' => 'SC',
            'agency_name' => 'Department of Legal Affairs - President\'s Office',
            'agency_short' =>  '?',
            'opposition_period' => '2 months',
            'registration_period' => '1 to 2 years',
            'madrid_protocol' => 0, 
            'is_feature' => 0, 
            'avatar' => 'countries/sc.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Sierra Leone',
            'abbr' => 'SL',
            'agency_name' => 'National Registry for industrial property and copyright',
            'agency_short' =>  '?',
            'opposition_period' => '?',
            'registration_period' => '?',
            'madrid_protocol' => 1, 
            'is_feature' => 0, 
            'avatar' => 'countries/sl.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'South Africa',
            'abbr' => 'ZA',
            'agency_name' => 'Companies and Intellectual Property Commission',
            'agency_short' =>  'CIPC',
            'opposition_period' => '3 months',
            'registration_period' => '2 years',
            'madrid_protocol' => 0, 
            'is_feature' => 0, 
            'avatar' => 'countries/za.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Swaziland',
            'abbr' => 'SZ',
            'agency_name' => 'Ministry of Commerce Industry and Trade',
            'agency_short' =>  '?',
            'opposition_period' => '8 months',
            'registration_period' => '9 to 12 months',
            'madrid_protocol' => 1, 
            'is_feature' => 0, 
            'avatar' => 'countries/sz.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Tanzania',
            'abbr' => 'TZ',
            'agency_name' => 'Business Registrations and Licensing Agency',
            'agency_short' =>  'BRELA',
            'opposition_period' => '2 months',
            'registration_period' => '6 months',
            'madrid_protocol' => 1, 
            'is_feature' => 0, 
            'avatar' => 'countries/tz.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Togo',
            'abbr' => 'TG',
            'agency_name' => 'National Institute for Industrial Property and Technology',
            'agency_short' =>  'INPIT',
            'opposition_period' => '?',
            'registration_period' => '?',
            'madrid_protocol' => 0, 
            'is_feature' => 0, 
            'avatar' => 'countries/tg.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Tunisia',
            'abbr' => 'TN',
            'agency_name' => 'National Institute for Standardization and Industrial Property',
            'agency_short' =>  'NISIP',
            'opposition_period' => '2 months',
            'registration_period' => '12 to 14 months',
            'madrid_protocol' => 0, 
            'is_feature' => 0, 
            'avatar' => 'countries/tn.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Uganda',
            'abbr' => 'UG',
            'agency_name' => 'Uganda Registration Services Bureau',
            'agency_short' =>  'URSB',
            'opposition_period' => '2 months',
            'registration_period' => '3 months',
            'madrid_protocol' => 0, 
            'is_feature' => 0, 
            'avatar' => 'countries/ug.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Zambia',
            'abbr' => 'ZM',
            'agency_name' => 'Patents and Companies Registration Agency',
            'agency_short' =>  'PCRA',
            'opposition_period' => '?',
            'registration_period' => '?',
            'madrid_protocol' => 1, 
            'is_feature' => 0, 
            'avatar' => 'countries/zm.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Zimbabwe',
            'abbr' => 'ZW',
            'agency_name' => 'Zimbabwe Intellectual Property Office',
            'agency_short' =>  'ZIPO',
            'opposition_period' => '2 months',
            'registration_period' => '8 to 16 months',
            'madrid_protocol' => 0, 
            'is_feature' => 0, 
            'avatar' => 'countries/zw.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'ARIPO Treaty',
            'abbr' => 'ARIPO',
            'agency_name' => 'African Regional Intellectual Property Organization',
            'agency_short' =>  'ARIPO',
            'opposition_period' => '3 months',
            'registration_period' => '12 months',
            'madrid_protocol' => 0, 
            'is_feature' => 0, 
            'is_treaty' => 1, 
            'avatar' => 'countries/aripo.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'OAPI Treaty',
            'abbr' => 'OAPI',
            'agency_name' => 'African Intellectual Property Organization',
            'agency_short' =>  'OAPI',
            'opposition_period' => '6 months',
            'registration_period' => '9 to 12 months',
            'madrid_protocol' => 0, 
            'is_feature' => 0,
            'is_treaty' => 1,
            'avatar' => 'countries/oapi.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('countries')->insert([
            'continent_id' => 5,
            'name' => 'Zanzibar',
            'abbr' => 'EAZ',
            'agency_name' => 'Intellectual Property Office',
            'agency_short' =>  'IPO',
            'opposition_period' => '1 month',
            'registration_period' => '8 to 12 months',
            'madrid_protocol' => 0, 
            'is_feature' => 0, 
            'avatar' => 'countries/eaz.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
