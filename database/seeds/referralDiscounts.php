<?php

use Illuminate\Database\Seeder;

class CountryReferralDiscounts extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('country_referral_discounts')->insert([
            'country_id' => '45',
            'total_discount' => '10',
            'referrer_discount' => '5',
            'customer_discount' => '5'
        ]);

        DB::table('country_referral_discounts')->insert([
            'country_id' => '146',
            'total_discount' => '10',
            'referrer_discount' => '5',
            'customer_discount' => '5'
        ]);

        DB::table('country_referral_discounts')->insert([
            'country_id' => '46',
            'total_discount' => '15',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);

        DB::table('country_referral_discounts')->insert([
            'country_id' => '102',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);

        DB::table('country_referral_discounts')->insert([
            'country_id' => '198',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);

        DB::table('country_referral_discounts')->insert([
            'country_id' => '140',
            'total_discount' => '10',
            'referrer_discount' => '5',
            'customer_discount' => '5'
        ]);

        DB::table('country_referral_discounts')->insert([
            'country_id' => '48',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);

        DB::table('country_referral_discounts')->insert([
            'country_id' => '49',
            'total_discount' => '20',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);

        DB::table('country_referral_discounts')->insert([
            'country_id' => '1',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);

        DB::table('country_referral_discounts')->insert([
            'country_id' => '50',
            'total_discount' => '15',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);

        DB::table('country_referral_discounts')->insert([
            'country_id' => '52',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);

        DB::table('country_referral_discounts')->insert([
            'country_id' => '103',
            'total_discount' => '10',
            'referrer_discount' => '5',
            'customer_discount' => '5'
        ]);

        DB::table('country_referral_discounts')->insert([
            'country_id' => '53',
            'total_discount' => '10',
            'referrer_discount' => '5',
            'customer_discount' => '5'
        ]);

        DB::table('country_referral_discounts')->insert([
            'country_id' => '104',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);

        DB::table('country_referral_discounts')->insert([
            'country_id' => '54',
            'total_discount' => '10',
            'referrer_discount' => '5',
            'customer_discount' => '5'
        ]);

        DB::table('country_referral_discounts')->insert([
            'country_id' => '152',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);

        DB::table('country_referral_discounts')->insert([
            'country_id' => '5',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);

        DB::table('country_referral_discounts')->insert([
            'country_id' => '99',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);

        DB::table('country_referral_discounts')->insert([
            'country_id' => '105',
            'total_discount' => '5',
            'referrer_discount' => '2',
            'customer_discount' => '3'
        ]);

        DB::table('country_referral_discounts')->insert([
            'country_id' => '6',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);

        DB::table('country_referral_discounts')->insert([
            'country_id' => '120',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);

        DB::table('country_referral_discounts')->insert([
            'country_id' => '55',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);

        DB::table('country_referral_discounts')->insert([
            'country_id' => '56',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);

        DB::table('country_referral_discounts')->insert([
            'country_id' => '57',
            'total_discount' => '5',
            'referrer_discount' => '2',
            'customer_discount' => '3'
        ]);

        DB::table('country_referral_discounts')->insert([
            'country_id' => '160',
            'total_discount' => '20',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);

        DB::table('country_referral_discounts')->insert([
            'country_id' => '121',
            'total_discount' => '20',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);

        DB::table('country_referral_discounts')->insert([
            'country_id' => '164',
            'total_discount' => '10',
            'referrer_discount' => '5',
            'customer_discount' => '5'
        ]);

        DB::table('country_referral_discounts')->insert([
            'country_id' => '60',
            'total_discount' => '5',
            'referrer_discount' => '2',
            'customer_discount' => '3'
        ]);

        DB::table('country_referral_discounts')->insert([
            'country_id' => '141',
            'total_discount' => '20',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);

        DB::table('country_referral_discounts')->insert([
            'country_id' => '61',
            'total_discount' => '15',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);

        DB::table('country_referral_discounts')->insert([
            'country_id' => '62',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);

        DB::table('country_referral_discounts')->insert([
            'country_id' => '166',
            'total_discount' => '10',
            'referrer_discount' => '5',
            'customer_discount' => '5'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '64',
            'total_discount' => '5',
            'referrer_discount' => '2',
            'customer_discount' => '3'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '65',
            'total_discount' => '20',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '108',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '68',
            'total_discount' => '15',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '8',
            'total_discount' => '5',
            'referrer_discount' => '2',
            'customer_discount' => '3'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '32',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '70',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '71',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '128',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '11',
            'total_discount' => '15',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '73',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '44',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '74',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '172',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '76',
            'total_discount' => '20',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '14',
            'total_discount' => '10',
            'referrer_discount' => '5',
            'customer_discount' => '5'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '15',
            'total_discount' => '5',
            'referrer_discount' => '2',
            'customer_discount' => '3'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '100',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '80',
            'total_discount' => '15',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '17',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '90',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);

        DB::table('country_referral_discounts')->insert([
            'country_id' => '179',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '18',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '19',
            'total_discount' => '15',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '142',
            'total_discount' => '15',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '183',
            'total_discount' => '5',
            'referrer_discount' => '2',
            'customer_discount' => '3'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '199',
            'total_discount' => '15',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '20',
            'total_discount' => '5',
            'referrer_discount' => '2',
            'customer_discount' => '3'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '38',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '131',
            'total_discount' => '5',
            'referrer_discount' => '2',
            'customer_discount' => '3'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '21',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '84',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '85',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '86',
            'total_discount' => '5',
            'referrer_discount' => '2',
            'customer_discount' => '3'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '87',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '185',
            'total_discount' => '5',
            'referrer_discount' => '2',
            'customer_discount' => '3'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '89',
            'total_discount' => '15',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '189',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '22',
            'total_discount' => '10',
            'referrer_discount' => '5',
            'customer_discount' => '5'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '92',
            'total_discount' => '20',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '93',
            'total_discount' => '10',
            'referrer_discount' => '5',
            'customer_discount' => '5'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '190',
            'total_discount' => '10',
            'referrer_discount' => '5',
            'customer_discount' => '5'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '12',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '23',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '133',
            'total_discount' => '20',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '95',
            'total_discount' => '20',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '24',
            'total_discount' => '20',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '192',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '26',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '41',
            'total_discount' => '20',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '28',
            'total_discount' => '5',
            'referrer_discount' => '2',
            'customer_discount' => '3'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '195',
            'total_discount' => '5',
            'referrer_discount' => '2',
            'customer_discount' => '3'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '42',
            'total_discount' => '15',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '98',
            'total_discount' => '5',
            'referrer_discount' => '2',
            'customer_discount' => '3'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '101',
            'total_discount' => '20',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '112',
            'total_discount' => '10',
            'referrer_discount' => '5',
            'customer_discount' => '5'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '29',
            'total_discount' => '15',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '43',
            'total_discount' => '25',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '196',
            'total_discount' => '15',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '200',
            'total_discount' => '15',
            'referrer_discount' => '5',
            'customer_discount' => '10'
        ]);
        DB::table('country_referral_discounts')->insert([
            'country_id' => '197',
            'total_discount' => '10',
            'referrer_discount' => '5',
            'customer_discount' => '5'
        ]);



    }
}
