<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ContinentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('continents')->insert([
            'name'         => 'Middle East',
            'abbr'         => 'ME',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        
        DB::table('continents')->insert([
            'name'    	   => 'Asia',
            'abbr'         => 'AS',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('continents')->insert([
            'name'    	   => 'North America',
            'abbr'         => 'NA',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('continents')->insert([
            'name'    	   => 'Ocenia',
            'abbr'         => 'OC',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('continents')->insert([
            'name'    	   => 'Africa',
            'abbr'         => 'AF',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('continents')->insert([
            'name'    	   => 'Europe',
            'abbr'         => 'EU',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('continents')->insert([
            'name'    	   => 'South America',
            'abbr'         => 'SA',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('continents')->insert([
            'name'    	   => 'Central America',
            'abbr'         => 'CA',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
