<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\EventType;

class EventTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

		EventType::create([
			'slug' => "trademark-study",
			'type'=> "Trademark Study",
			'notes' => "System Generated"
		]);

		EventType::create([
			'slug' => "trademark-registration",
			'type'=> "Trademark Registration",
			'notes' => "System Generated"
		]);

		EventType::create([
			'slug' => "office-action-response",
			'type'=> "Office Action Response",
			'notes' => "System Generated"
		]);

		EventType::create([
			'slug' => "appeal-a-final-refusal",
			'type'=> "Appeal a Final Refusal",
			'notes' => "System Generated"
		]);

		EventType::create([
			'slug' => "trademark-statement-of-use",
			'type'=> "Trademark Statement of Use",
			'notes' => "System Generated"
		]);

		EventType::create([
			'slug' => "revive-an-abandoned-application",
			'type'=> "Revive an Abandoned Application",
			'notes' => "System Generated"
		]);

		EventType::create([
			'slug' => "international-trademark-application",
			'type'=> "International Trademark Application",
			'notes' => "System Generated"
		]);

		EventType::create([
			'slug' => "trademark-renewal",
			'type'=> "Trademark Renewal",
			'notes' => "System Generated"
		]);

		EventType::create([
			'slug' => "change-or-update-trademark-owner",
			'type'=> "Change or Update Trademark Owner",
			'notes' => "System Generated"
		]);

		EventType::create([
			'slug' => "trademark-monitoring",
			'type'=> "Trademark Monitoring",
			'notes' => "System Generated"
		]);

		EventType::create([
			'slug' => "letter-of-protest",
			'type'=> "Letter of Protest",
			'notes' => "System Generated"
		]);

		EventType::create([
			'slug' => "negotiate-a-settlement",
			'type'=> "Negotiate a Settlement",
			'notes' => "System Generated"
		]);

		
    }
}
