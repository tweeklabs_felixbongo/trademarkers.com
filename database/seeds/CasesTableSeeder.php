<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Repositories\Utils;
use App\ActionCode;
use Illuminate\Support\Facades\DB;
use Spatie\Sitemap\Sitemap;

class CasesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // ini_set('memory_limit', '1024M');

        // $database  = DB::connection('sitemap');
        $count = 0;
        // $error = 0;

        // $file_name ="-details" . ".xml";

        // $sitemap   = Sitemap::create();

        // $result    = $database->table('details')->whereBetween('reg_date',['2017-08-15', '2018-01-22'])->get();

        // $ch = curl_init(); 

        // if ( count( $result ) > 0 ) {

        //     foreach ( $result as $res ) {

        //         $new_name  = $this->escape_characters( $res->name );
        //         $new_orga  = $this->escape_characters( $res->applicant_orga );

        //         if ( strlen( $new_name ) == 0 || strlen( $new_orga ) == 0 ) {
        //             $error++;

        //             echo "\t" . $res->number . "\t" . $error . "\n" ;

        //             continue;
        //         }
                
        //         if ( strlen( $res->number ) != 9 ) {

        //             $res->number = "0" . $res->number; 
        //         }

        //         $url_name  = "https://trademarkers.com/EUIPO/" . $res->number . "/";

        //         $url_name .= preg_replace( "/(.)\\1+/", "$1", "trademark-" . $new_name  . "-" );
                
        //         $url_name .= preg_replace( "/(.)\\1+/", "$1", "granted-to-" . $new_orga );

        //         $url_name    = rtrim( $url_name,'-' );

        //         // $length = $this->review_url( $url_name, $ch );

        //         // if ( $length < 20000 ) {

        //         //     $error++;

        //         //     echo "\t" . $res->number . "\t" . $error . "\n" ;

        //         //     continue;
        //         // }

        //         $count++;

        //         echo $count . "\n";

        //         $sitemap->add( $url_name );
        //     }

        //     curl_close($ch);

        //     // $sitemap->writeToFile( base_path("sitemaps/" . $file_name) );

        //     // return "Successfully generated sitemap on " . $date1->format('Y-m-d');
        // }

        foreach (range(1,1000000) as $index) {

            try {

                DB::table('cases')->insert([
                    'action_code'   => Utils::gen_case( 1 ),
                    'order_code'    => Utils::gen_order( 1 ),
                    'customer_code' => Utils::gen_customer( 1 ),
                    'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
                ]);

            } catch ( Exception $e ) {
            	
                echo $index . "\t" . $count++ . "\n";
            }  
        }
    }

    public function escape_characters( $string )
    {
        $clean    = preg_replace('/[^\w\p{L}\p{N}]/u', '-' , $string);
        $initial  = preg_replace("/(.)\\1+/", "$1", $clean);
        $final    = rtrim( $initial,'-' );

        return $final;
    }

    public function review_url( $url ,$ch )
    {
        curl_setopt($ch, CURLOPT_URL, $url); 

        curl_setopt($ch, CURLOPT_HEADER, TRUE);

        curl_setopt($ch, CURLOPT_NOBODY, TRUE);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 

        curl_exec($ch); 

        $length = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD); 

        return $length;
    }
}
