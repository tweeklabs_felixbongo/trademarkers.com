<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ClassesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('trademark_classes')->insert([
            'name'    	   => 'Chemicals',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Paints, Coatings & Pigments',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Cleaning Products, Bleaching & Abrasives, Cosmetics',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Fuels, Industrial Oils and Greases, Illuminates',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Pharmaceutical, Veterinary Products, Dietetic',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Metals, metal castings, Locks, Safes, Hardware',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Machines and Machine Tools, Parts',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Hand Tools and implements, Cutlery',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Computers, Software, Electronic instruments, & Scientific appliances',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Medical, Dental Instruments and Apparatus',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Appliances, Lighting, Heating, Sanitary Installations',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Vehicles',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Firearms, Explosives and Projectiles',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Precious Metal ware, Jewellery',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Musical Instruments and supplies',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Paper, Items made of Paper, Stationary items',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Rubber, Asbestos, Plastic Items',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Leather and Substitute Goods',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Construction Materials (building - non metallic)',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Furniture, Mirrors',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Crockery, Containers, Utensils, Brushes, Cleaning Implements',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Cordage, Ropes, Nets, Awnings, Sacks, Padding',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Yarns, Threads',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Fabrics, Blankets, Covers, Textile',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Clothing, Footwear and Headgear',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Sewing Notions, Fancy Goods, Lace and Embroidery',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Carpets, Linoleum, Wall and Floor Coverings (non textile)',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Games, Toys, Sports Equipment',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Foods - Dairy, Meat, Fish, Processed & Preserved Foods',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Foods - Spices, Bakery Goods, Ice, Confectionery',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Fresh Fruit & Vegetables, Live Animals',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Beer, Ales, Soft Drinks, Carbonated Waters',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Wines, Spirits, Liqueurs',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Tobacco, Smokers Requisites & Matches',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Advertising, Business Consulting',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Insurance, Financial',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Construction, Repair, Cleaning',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Communications',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Transport, Utilities, Storage & Warehousing',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Materials Treatment, Working',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Education, Amusement, Entertainment, Reproduction',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Scientific and technological services and research and design relating thereto',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Services for providing food and drink; temporary accommodations',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Medical services; veterinary services; hygienic and beauty care for human beings or animals',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('trademark_classes')->insert([
            'name'    	   => 'Personal and social services rendered by others to meet the needs of individuals',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
