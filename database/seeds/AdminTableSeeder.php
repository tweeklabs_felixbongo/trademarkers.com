<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('admin_users')->insert([
        	'username'     => 'administrator',
        	'password'     => bcrypt('bigfoot123!'),
            'name'    	   => 'Manager',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('admin_users')->insert([
        	'username'     => 'ashley@trademarkers.com',
        	'password'     => bcrypt('bigfoot123!'),
            'name'    	   => 'Ashley Patalingjug',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('admin_users')->insert([
        	'username'     => 'bonn@trademarkers.com',
        	'password'     => bcrypt('bigfoot123!'),
            'name'    	   => 'Bonn Clark Rivera',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('admin_users')->insert([
        	'username'     => 'ivy@trademarkers.com',
        	'password'     => bcrypt('bigfoot123!'),
            'name'    	   => 'Ivy Villarta',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('admin_users')->insert([
        	'username'     => 'jenny@trademarkers.com',
        	'password'     => bcrypt('bigfoot123!'),
            'name'    	   => 'Jenny Daño',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('admin_users')->insert([
        	'username'     => 'luigi@trademarkers.com',
        	'password'     => bcrypt('bigfoot123!'),
            'name'    	   => 'Luigi Tumulak',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('admin_users')->insert([
        	'username'     => 'lanie@trademarkers.com',
        	'password'     => bcrypt('bigfoot123!'),
            'name'    	   => 'Lanie Tribunal',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('admin_users')->insert([
        	'username'     => 'marittes@trademarkers.com',
        	'password'     => bcrypt('bigfoot123!'),
            'name'    	   => 'Marittes',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('admin_users')->insert([
        	'username'     => 'anthony@trademarkers.com',
        	'password'     => bcrypt('bigfoot123!'),
            'name'    	   => 'Anthony',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('admin_users')->insert([
        	'username'     => 'sheenalyn@trademarkers.com',
        	'password'     => bcrypt('bigfoot123!'),
            'name'    	   => 'Sheenalyn',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('admin_users')->insert([
        	'username'     => 'jhon@trademarkers.com',
        	'password'     => bcrypt('bigfoot123!'),
            'name'    	   => 'Jhon',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('admin_users')->insert([
        	'username'     => 'jucelf@trademarkers.com',
        	'password'     => bcrypt('bigfoot123!'),
            'name'    	   => 'Jucelf',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('admin_users')->insert([
        	'username'     => 'arviejell@trademarkers.com',
        	'password'     => bcrypt('bigfoot123!'),
            'name'    	   => 'Arviejell',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('admin_users')->insert([
        	'username'     => 'us.atty@trademarkers.com',
        	'password'     => bcrypt('bigfoot123!'),
            'name'    	   => 'US',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('admin_users')->insert([
        	'username'     => 'nl.atty@trademarkers.com',
        	'password'     => bcrypt('bigfoot123!'),
            'name'    	   => 'NL',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('admin_users')->insert([
        	'username'     => 'uk.atty@trademarkers.com',
        	'password'     => bcrypt('bigfoot123!'),
            'name'    	   => 'UK',
            'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
