<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateActionCodePromosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('action_code_promos', function (Blueprint $table) {
            $table->integer('limit')->default(0); // 0 for no limit
            $table->enum('status', ['expired','on-going','done'])->default('on-going');
            $table->decimal('supplementary_discount')->nullable();
            $table->decimal('supplementary_amount_point')->nullable();
            $table->integer('country_id')->nullable();
            $table->integer('case_number')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('action_code_promos', function (Blueprint $table) {
            $table->dropColumn('supplementary_discount');
            $table->dropColumn('supplementary_amount_point');
            
            $table->dropColumn('status');
            $table->dropColumn('limit');
            $table->dropColumn('country_id');
            $table->dropColumn('case_number');
        });
    }
}
