<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEnumValuesPriceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // ENUM NOT SUPPORTED
        
        // DB::statement("ALTER TABLE prices CHANGE COLUMN service_type values enum('Study','Registration','Certificate','Office Status - Simple','Office Status - Complex','Office Status - Likelihood') NOT NULL DEFAULT 'Study'");
        // Schema::table('prices', function (Blueprint $table) { 
        //     $table->enum('service_type', ['Study','Registration','Certificate','Office Status - Simple'])->default('Study')->change(); 
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // DB::statement("ALTER TABLE prices CHANGE COLUMN service_type values enum('Study','Registration','Certificate')");
    }
}
