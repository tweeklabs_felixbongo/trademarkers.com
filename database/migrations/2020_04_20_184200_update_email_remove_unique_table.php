<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEmailRemoveUniqueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('domain_event_emails', function (Blueprint $table) {
            $table->dropUnique('domain_event_emails_email_unique');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('domain_event_emails', function (Blueprint $table) {
        //     $table->dropColumn('email_subject');
        // });

    }
}
