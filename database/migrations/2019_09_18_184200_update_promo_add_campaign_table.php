<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePromoAddCampaignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('action_code_promos', function (Blueprint $table) {
            $table->integer('campaign_id')->nullable();
        });

        Schema::table('carts', function (Blueprint $table) {
            $table->integer('campaign_id')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('action_code_promos', function (Blueprint $table) {
            $table->dropColumn('campaign_id');
        });

        Schema::table('carts', function (Blueprint $table) {
            $table->dropColumn('campaign_id');
        });

    }
}
