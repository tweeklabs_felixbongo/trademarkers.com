<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('carts', function ($table) {
            $table->text('email_content');
            $table->enum('email_status_sent', ['yes','no'])->default('no');
            $table->enum('email_status_show', ['yes','no'])->default('no');
            $table->enum('status', ['active','inactive','completed','pending'])->default('active');
            $table->float('total_discount')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('carts', function (Blueprint $table) {
            $table->dropColumn('email_content');
            $table->dropColumn('email_status_sent');
            $table->dropColumn('email_status_show');
            $table->dropColumn('status');
            $table->dropColumn('total_discount');
        });
    }
}
