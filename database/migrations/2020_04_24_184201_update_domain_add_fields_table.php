<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDomainAddFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('brand_domains', function (Blueprint $table) {
            $table->text('website_usage')->nullable();
            $table->text('registrant_background')->nullable();
            $table->float('selling_price')->nullable();
            $table->enum('for_sale',['yes','no'])->default('no');
            $table->enum('active_website',['yes','no'])->default('no');
            $table->text('active_description')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('brand_domains', function (Blueprint $table) {
            $table->dropColumn('website_usage');
            $table->dropColumn('registrant_background');
            $table->dropColumn('selling_price');
            $table->dropColumn('for_sale');
            $table->dropColumn('active_website');
            $table->dropColumn('active_description');
        });


    }
}
