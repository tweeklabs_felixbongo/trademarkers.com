<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCaseJurisdictionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('action_code_cases', function (Blueprint $table) {
            $table->text('jurisdiction')->nullable();
            $table->enum('is_priority',['yes','no'])->default('no');
            $table->text('notes')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('action_code_cases', function (Blueprint $table) {
            $table->dropColumn('jurisdiction');
            $table->dropColumn('is_priority');
            $table->dropColumn('notes');
        });

    }
}
