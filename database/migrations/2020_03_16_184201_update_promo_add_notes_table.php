<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePromoAddNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('action_code_promos', function (Blueprint $table) {
            $table->text('notes')->nullable();
            // $table->decimal('assigned_discount')->default(0);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('action_code_promos', function (Blueprint $table) {
            $table->dropColumn('notes');
            // $table->dropColumn('assigned_discount');
        });

    }
}
