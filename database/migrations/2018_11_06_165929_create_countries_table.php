<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('continent_id');
            $table->string('name');
            $table->string('abbr');
            $table->text('avatar');
            $table->boolean('is_feature');
            $table->string('agency_name');
            $table->string('agency_short');
            $table->string('opposition_period');
            $table->string('registration_period');
            $table->boolean('madrid_protocol');
            $table->integer('is_treaty')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
