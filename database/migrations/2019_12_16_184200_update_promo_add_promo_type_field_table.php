<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePromoAddPromoTypeFieldTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('action_code_promos', function (Blueprint $table) {
            $table->string('promo_type')->default('Trademark Registration')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('action_code_promos', function (Blueprint $table) {
            $table->dropColumn('promo_type');
        });

    }
}
