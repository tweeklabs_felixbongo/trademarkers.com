<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrademarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trademarks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->string('case_number')->nullable();
            $table->integer('country_id')->nullable();
            $table->integer('order_id')->nullable();
            $table->integer('profile_id')->nullable();
            $table->string('euipo_id')->nullable();
            $table->string('service')->nullable();
            $table->string('type')->nullable();
            $table->string('name')->nullable();
            $table->string('logo')->nullable();
            $table->string('logo_description')->nullable();
            $table->text('summary')->nullable();
            $table->datetime('filing_date')->nullable();
            $table->text('filing_number')->nullable();
            $table->string('app_reference')->nullable();
            $table->text('atty_notes')->nullable();
            $table->text('rep_notes')->nullable();
            $table->text('client_notes')->nullable();
            $table->datetime('registration_date')->nullable();
            $table->datetime('expiry_date')->nullable();
            $table->string('classes')->nullable();
            $table->text('classes_description');
            $table->string('office')->nullable();
            $table->string('office_status')->nullable();
            $table->text('status_description')->nullable();
            $table->string('brand_name')->nullable();
            $table->string('purpose')->nullable();
            $table->string('value')->nullable();
            $table->string('category')->nullable();
            $table->boolean('hvt')->nullable();
            $table->boolean('revocation')->nullable();
            $table->string('priority_date')->nullable();
            $table->string('priority_country')->nullable();
            $table->string('priority_number')->nullable();
            $table->string('recently_filed')->nullable();
            $table->string('claim_priority')->nullable();
            $table->string('commerce')->nullable();
            $table->double('amount')->nullable();
            $table->string('discount')->nullable();
            $table->integer('atty_id')->nullable();
            $table->integer('rep_id')->nullable();
            $table->string('atty_email')->nullable();
            $table->string('rep_email')->nullable();
            $table->string('rep_phone')->nullable();
            $table->string('rep_fax')->nullable();
            $table->integer('number')->nullable();
            $table->integer('relative_trademark_id')->nullable();
            $table->integer('registration_number')->nullable();
            $table->integer('international_registration_number')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trademarks');
    }
}
