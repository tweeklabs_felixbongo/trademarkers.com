<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailTrackerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_trackers', function ($table) {
            $table->increments('id');
            $table->integer('email_id');
            $table->integer('event_id');
            $table->integer('domain_id');
            $table->enum('status',['sent','bounced','read'])->default('sent');
            $table->timestamps();
        });

        Schema::table('domain_event_emails', function (Blueprint $table) {
            $table->enum('flag',['yes','no'])->default('yes');
        });

        Schema::table('events', function (Blueprint $table) {
            $table->string('name');
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_trackers');

        Schema::table('domain_event_emails', function (Blueprint $table) {
            $table->dropColumn('flag');
        });

        Schema::table('events', function (Blueprint $table) {
            $table->dropColumn('name');
        });

    }
}
