<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadBrandPointTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brand_leads', function ($table) {
            $table->increments('id');
            $table->integer('brand_id')->nullable();
            $table->integer('lead_id')->nullable();
            // $table->text('email_address')->nullable();
            $table->timestamps();
        });

        Schema::table('domain_event_emails', function (Blueprint $table) {
            $table->integer('action_code_case_id')->nullable();
        });

        Schema::table('brand_domains', function (Blueprint $table) {
            $table->integer('action_code_case_id')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brand_leads');

        Schema::table('domain_event_emails', function (Blueprint $table) {
            $table->dropColumn('action_code_case_id');
        });

        Schema::table('brand_domains', function (Blueprint $table) {
            $table->dropColumn('action_code_case_id');
        });

    }
}
