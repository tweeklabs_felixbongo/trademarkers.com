<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBrandDomainAddInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('brand_domains', function (Blueprint $table) {
            $table->text('first_name')->nullable();
            $table->text('last_name')->nullable();
            $table->text('date_registered')->nullable();
            $table->text('admin_email')->nullable();
            $table->text('postal_address')->nullable();
            $table->datetime('last_crawl')->nullable();
        });

        Schema::table('events', function (Blueprint $table) {
            $table->enum('email_flag',['yes','no'])->default('no');
            $table->datetime('last_crawl')->nullable();
        });

        Schema::table('event_types', function (Blueprint $table) {
            $table->enum('event_show',['yes','no'])->default('yes');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('brand_domains', function (Blueprint $table) {
            $table->dropColumn('first_name');
            $table->dropColumn('last_name');
            $table->dropColumn('date_registered');
            $table->dropColumn('admin_email');
            $table->dropColumn('postal_address');
            $table->dropColumn('last_crawl');
        });

        Schema::table('events', function (Blueprint $table) {
            $table->dropColumn('email_flag');
            $table->dropColumn('last_crawl');
        });

        Schema::table('event_types', function (Blueprint $table) {
            $table->dropColumn('event_show');
        });

    }
}
