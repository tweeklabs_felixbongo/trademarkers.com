<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('blogs', function ($table) {
            $table->increments('id');
            $table->string('slug')->unique();
            $table->string('title');
            $table->mediumText('description');
            $table->string('featured_img')->nullable();
            $table->string('author_id');
            $table->enum('status', ['draft','publish'])->default('publish');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
