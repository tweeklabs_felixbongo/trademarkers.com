<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCartTrademarkAddColorClaimFieldTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('carts', function (Blueprint $table) {
            $table->enum('color_claim',['no','yes'])->default('no');
        });

        Schema::table('trademarks', function (Blueprint $table) {
            $table->enum('color_claim',['no','yes'])->default('no');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('carts', function (Blueprint $table) {
            $table->dropColumn('color_claim');
        });

        Schema::table('trademarks', function (Blueprint $table) {
            $table->dropColumn('color_claim');
        });


    }
}
