<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBrandAddExtraFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('brands', function (Blueprint $table) {
            $table->text('purpose')->nullable();
            $table->text('category')->nullable();
            $table->text('value')->nullable();
        });

        Schema::table('brand_domains', function (Blueprint $table) {
            $table->text('registrar')->nullable();
            $table->text('registrant_name')->nullable();
            $table->text('registrant_organization')->nullable();
            $table->text('registrant_street')->nullable();
            $table->text('registrant_city')->nullable();
            $table->text('registrant_state')->nullable();
            $table->text('registrant_postcode')->nullable();
            $table->integer('registrant_country_id')->nullable();
            $table->text('registrant_phone')->nullable();
            $table->text('registrant_fax')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('brands', function (Blueprint $table) {
            $table->dropColumn('purpose');
            $table->dropColumn('category');
            $table->dropColumn('value');
        });

        Schema::table('brand_domains', function (Blueprint $table) {
            $table->dropColumn('registrar');
            $table->dropColumn('registrant_name');
            $table->dropColumn('registrant_organization');
            $table->dropColumn('registrant_street');
            $table->dropColumn('registrant_city');
            $table->dropColumn('registrant_state');
            $table->dropColumn('registrant_postcode');
            $table->dropColumn('registrant_country_id');
            $table->dropColumn('registrant_phone');
            $table->dropColumn('registrant_fax');
        });

    }
}
