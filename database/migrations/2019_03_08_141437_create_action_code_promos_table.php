<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionCodePromosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('action_code_promos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('action_code_id')->unique();
            $table->integer('user_id')->nullable(); // 0 for all
            $table->integer('case_number_id')->nullable(); // 0 for all cases
            $table->integer('country_id')->nullable();// 0 for all countries
            $table->decimal('discount');
            $table->integer('limit'); // 0 for no limit
            $table->datetime('expiry_date')->nullable();
            $table->enum('status', ['expired','on-going','done'])->default('on-going');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('action_code_promos');
    }
}
