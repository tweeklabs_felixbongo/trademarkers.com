<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionCodeReferralTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('action_code_referrals', function ($table) {
            $table->increments('id');
            $table->integer('user_id')->unique();
            $table->integer('action_code_id');
            $table->float('total_points')->default(0);
            $table->timestamps();
        });

        Schema::create('action_code_referral_invites', function ($table) {
            $table->increments('id');

            $table->integer('referrals_id');

            $table->integer('invited_user_id');

            $table->integer('order_id')->nullable();

            $table->enum('apply',['yes','no'])->default('no');
            
            $table->timestamps();
        });

        Schema::create('action_code_referral_transactions', function ($table) {
            $table->increments('id');
            
            $table->integer('referrals_id');

            $table->integer('ref_invite_id')->nullable();

            $table->float('amount')->default(0);
            $table->text('description')->nullable();

            $table->text('info_name')->nullable();
            $table->text('info_email')->nullable();
            $table->text('information')->nullable();
            $table->text('payment_method')->nullable();

            $table->enum('status', ['credit','processing','done'])->default('credit');

            $table->timestamps();
        });

        Schema::create('country_referral_discounts', function ($table) {
            $table->increments('id');
            
            $table->integer('country_id');

            $table->float('total_discount')->default(0);
            $table->float('referrer_discount')->default(0);
            $table->float('customer_discount')->default(0);

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
 
        Schema::dropIfExists('action_code_referral_invites');
        Schema::dropIfExists('action_code_referral_transactions');
        Schema::dropIfExists('action_code_referrals');
        Schema::dropIfExists('country_referral_discounts');
        
        Schema::enableForeignKeyConstraints();
    }
}
