<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionCodeCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('action_code_cases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('action_code_id')->unique();
            $table->integer('action_code_campaign_id');
            $table->string('number')->nullable();
            $table->text('trademark')->nullable();
            $table->datetime('tm_filing_date')->nullable();
            $table->text('tm_holder')->nullable();
            $table->string('application_reference')->nullable();
            $table->string('mark_current_status_code')->nullable();
            $table->datetime('mark_current_status_date')->nullable();
            $table->datetime('ref_processed_date')->nullable();
            $table->datetime('registration_date')->nullable();
            $table->text('address')->nullable();
            $table->string('city')->nullable();
            $table->string('city_of_registration')->nullable();
            $table->string('state')->nullable();
            $table->string('class_number')->nullable();
            $table->mediumText('class_description')->nullable();
            $table->string('representatives')->nullable();
            $table->string('email')->nullable();
            $table->string('fax_no')->nullable();
            $table->string('telephone_no')->nullable();
            $table->string('website')->nullable();
            $table->string('zip')->nullable();
            $table->string('country')->nullable();
            $table->string('country_code')->nullable();
            $table->double('discount')->nullable();
            $table->datetime('discount_expiry_date')->nullable();
            $table->datetime('expiry_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('action_code_cases');
    }
}
