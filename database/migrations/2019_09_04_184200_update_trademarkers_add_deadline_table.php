<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTrademarkersAddDeadlineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trademarks', function (Blueprint $table) {
            $table->datetime('office_deadline_date')->nullable();
            $table->integer('paralegal_id')->nullable();
            $table->integer('paralegal_email')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trademarks', function (Blueprint $table) {
            $table->dropColumn('office_deadline_date');
            $table->dropColumn('paralegal_id');
            $table->dropColumn('paralegal_email');
        });

    }
}
