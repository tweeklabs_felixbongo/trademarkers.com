<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateActionCodeCasesAddHeaderFooterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('action_code_campaigns', function (Blueprint $table) {
            $table->text('header_content')->nullable();
            $table->text('footer_content')->nullable();
            $table->string('register_country_code')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('action_code_campaigns', function (Blueprint $table) {
            $table->dropColumn('header_content');
            $table->dropColumn('footer_content');
            $table->dropColumn('register_country_code');
        });

    }
}
