<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('country_id')->nullable();
            $table->integer('profile_id');
            $table->string('service');
            $table->string('type')->nullable();
            $table->string('name')->nullable();
            $table->string('logo')->nullable();
            $table->text('summary')->nullable();
            $table->string('classes')->nullable();
            $table->text('classes_description');
            $table->string('priority_date')->nullable();
            $table->string('priority_country')->nullable();
            $table->string('priority_number')->nullable();
            $table->string('recently_filed')->nullable();
            $table->string('claim_priority')->nullable();
            $table->string('commerce')->nullable();
            $table->string('discount')->nullable();
            $table->integer('relative_trademark_id')->nullable();
            $table->string('amount')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carts');
    }
}
