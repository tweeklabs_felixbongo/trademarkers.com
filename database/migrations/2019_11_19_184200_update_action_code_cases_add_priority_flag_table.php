<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateActionCodeCasesAddPriorityFlagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('action_code_campaigns', function (Blueprint $table) {
            $table->enum('priority',['yes','no'])->default('no');
        });

        Schema::table('action_code_cases', function (Blueprint $table) {
            $table->enum('is_purchase',['yes','no'])->default('no');
        });

        Schema::table('carts', function (Blueprint $table) {
            $table->integer('case_id')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('action_code_campaigns', function (Blueprint $table) {
            $table->dropColumn('priority');
        });

        Schema::table('carts', function (Blueprint $table) {
            $table->dropColumn('case_id');
        });

        Schema::table('action_code_cases', function (Blueprint $table) {
            $table->dropColumn('is_purchase');
        });

    }
}
