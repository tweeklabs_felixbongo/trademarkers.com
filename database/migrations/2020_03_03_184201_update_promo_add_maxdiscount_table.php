<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePromoAddMaxdiscountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('action_code_promos', function (Blueprint $table) {
            $table->decimal('max_discount')->default(0);
            // $table->decimal('assigned_discount')->default(0);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('action_code_promos', function (Blueprint $table) {
            $table->dropColumn('max_discount');
            // $table->dropColumn('assigned_discount');
        });

    }
}
