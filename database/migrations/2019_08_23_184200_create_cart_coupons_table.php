<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_coupons', function ($table) {
            $table->increments('id');
            $table->integer('promo_code_id')->nullable();
            $table->integer('trademark_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('cart_id')->nullable();
            $table->integer('order_id')->nullable();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_coupons');

    }
}
