<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrandTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('brands', function ($table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('assigned_user_id')->nullable();
            // $table->integer('country_id');
            // $table->string('filling_number')->nullable();
            $table->string('brand')->nullable();
            $table->text('notes')->nullable();

            $table->timestamps();
        });

        Schema::create('domain_event_emails', function ($table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->integer('brand_domain_id')->nullable();
            $table->integer('events_id')->nullable();
            // $table->morphs('email');
            $table->string('email')->unique();
            $table->text('notes')->nullable();
            $table->enum('status',['pending','send','sent','bounced','read'])->default('pending');

            $table->timestamps();
        });

        Schema::create('brand_domains', function ($table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->integer('brand_id');
            $table->string('domain')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
        });

        Schema::create('events', function ($table) {
            $table->increments('id');
            $table->integer('brand_id')->nullable();
            $table->integer('country_id')->nullable();
            $table->integer('event_type_id')->nullable();
            $table->integer('action_code_case_id')->nullable();

            $table->text('trademark')->nullable();
            $table->datetime('tm_filing_date')->nullable();
            $table->string('tm_holder')->nullable();
            $table->datetime('registration_date')->nullable();
            $table->text('address')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('class_number')->nullable();
            $table->string('class_description')->nullable();
            $table->string('email')->nullable();
            $table->string('fax_no')->nullable();
            $table->string('website')->nullable();
            $table->string('zip')->nullable();
            $table->string('notes')->nullable();
            $table->timestamps();
        });

        Schema::create('event_types', function ($table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('type');
            $table->text('notes')->nullable();
            $table->timestamps();
        });

        Schema::create('event_attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->integer('event_id');
            $table->string('name');
            $table->string('description');
            $table->string('file_size')->nullable();
            $table->string('file_type')->nullable();
            $table->timestamps();
        });

        Schema::dropIfExists('oppositions');
        Schema::dropIfExists('opposition_emails');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brands');
        Schema::dropIfExists('domain_event_emails');
        Schema::dropIfExists('brand_domains');
        Schema::dropIfExists('events');
        Schema::dropIfExists('event_types');
        Schema::dropIfExists('event_attachments');
    }
}
