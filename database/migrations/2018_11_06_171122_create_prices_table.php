<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id');
            $table->enum('service_type', ['Study', 'Registration', 'Certificate']);
            $table->double('initial_cost', 8, 2);
            $table->double('additional_cost', 8, 2);
            $table->double('logo_initial_cost', 8, 2);
            $table->double('logo_additional_cost', 8, 2);
            $table->double('tax', 8, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prices');
    }
}
