<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreHourTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_hours', function ($table) {
            $table->increments('id');
            $table->integer('day')->unique();
            $table->time('start_time')->nullable();
            $table->time('end_time')->nullable();
            $table->enum('range_time',['full','half','close'])->default('full');
            $table->timestamps();
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_hours');
    }
}
