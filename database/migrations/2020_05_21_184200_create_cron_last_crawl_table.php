<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCronLastCrawlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cron_last_crawls', function ($table) {
            $table->increments('id');

            $table->string('name');
            $table->string('last_page');

            $table->timestamps();
        });

        Schema::table('action_code_campaigns', function (Blueprint $table) {
            $table->enum('cron',['yes','no','done'])->default('no');
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cron_last_crawls');

        Schema::table('action_code_campaigns', function (Blueprint $table) {
            $table->dropColumn('cron');
        });

    }
}
