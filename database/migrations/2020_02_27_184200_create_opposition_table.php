<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOppositionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('oppositions', function ($table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('country_id');
            $table->string('filling_number')->nullable();
            
            $table->datetime('opposition_period_start')->nullable();
            $table->datetime('opposition_period_end')->nullable();
            $table->string('brand')->nullable();
            $table->string('affected_brand')->nullable();
            $table->text('notes')->nullable();

            $table->timestamps();
        });

        Schema::create('opposition_emails', function ($table) {
            $table->increments('id');
            $table->integer('opposition_id');
            $table->string('email')->nullable();
            $table->text('notes')->nullable();
            $table->enum('status',['send','sent','bounced','read'])->default('send');

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oppositions');
        Schema::dropIfExists('opposition_emails');
    }
}
