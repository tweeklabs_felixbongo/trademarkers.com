<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->text('code');
            $table->text('description')->nullable();
            $table->decimal('discount');
            $table->decimal('supplementary_discount');
            $table->decimal('supplementary_amount_point');
            $table->integer('limit'); // 0 for no limit
            $table->datetime('expiry_date')->nullable();
            $table->enum('status', ['expired','on-going','done'])->default('on-going');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
