<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionCodeCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('action_code_carts');

        Schema::create('action_code_carts', function ($table) {
            $table->increments('id');
            $table->integer('action_code_id');
            $table->integer('trademark_id');
            $table->integer('user_id');
            $table->integer('cart_id');
            $table->enum('status', ['pending','added'])->default('pending');
            $table->timestamps();
        });

        Schema::table('carts', function ($table) {
            $table->text('deatailed_contents')->nullable();
            $table->float('total_discount')->default('0');
            $table->enum('status', ['active','inactive','completed','pending'])->default('active');
            $table->integer('order_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('action_code_carts');

        Schema::table('carts', function (Blueprint $table) {
            $table->dropColumn('deatailed_contents');
            $table->dropColumn('total_discount');
            $table->dropColumn('status');
            $table->dropColumn('order_id');
        });
    }
}
